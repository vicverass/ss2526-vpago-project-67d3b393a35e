package vpago.monitor.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Clase persistente para representar una Regla de Producto
 */
@Entity
@DiscriminatorValue("product")
public class RuleProduct extends Rule {
	
	public RuleProduct() {
		super();
	}

	/** Identificador del Producto */
	private BankCardProduct bankProduct;

	/**
	 * @return the bankProduct
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id")
	public BankCardProduct getBankProduct() {
		return bankProduct;
	}

	/**
	 * @param bankProduct the bankProduct to set
	 */
	public void setBankProduct(BankCardProduct bankProduct) {
		this.bankProduct = bankProduct;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProduct [id=" + id + ", acquirer=" + acquirer + ", bankProduct="
				+ bankProduct + "]";
	}
}
