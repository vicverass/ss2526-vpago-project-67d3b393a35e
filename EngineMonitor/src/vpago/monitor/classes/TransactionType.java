package vpago.monitor.classes;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vpago.monitor.classes.types.BatchOperation;



/**
 * Clase persistente para representar un Tipo de Transaccion
 */
@Entity
@Table(name="transaction_type")
public class TransactionType {
	
	public TransactionType() {
		super();
	}
	
	public TransactionType(String name, String description, TemplateMessage templateMessage, IsoTemplate isoTemplate,
			String methodName, double timeout, boolean retry, int retryAttempts,
			TransactionType transactionResponseType, boolean inverseTrxType, boolean blocksOnConf, boolean blocksOnInv,
			TransactionType transactionInverseType, boolean confirmationTrxType,
			TransactionType transactionConfirmationType, boolean reversableType, double retryDelay, boolean confirmableType,
			boolean autoInverse, BatchOperation batchOperation, boolean settlementTrxType, boolean statePrint, boolean uploadableType,
			TransactionType transactionBatchUploadType, TransactionType transactionTrailerSettleType, boolean annulmentType) {
		super();
		this.name = name;
		this.description = description;
		this.templateMessage = templateMessage;
		this.isoTemplate = isoTemplate;
		this.methodName = methodName;
		this.timeout = timeout;
		this.retry = retry;
		this.retryAttempts = retryAttempts;
		this.transactionResponseType = transactionResponseType;
		this.inverseTrxType = inverseTrxType;
		this.blocksOnConf = blocksOnConf;
		this.blocksOnInv = blocksOnInv;
		this.transactionInverseType = transactionInverseType;
		this.confirmationTrxType = confirmationTrxType;
		this.transactionConfirmationType = transactionConfirmationType;
		this.reversableType = reversableType;
		this.retryDelay = retryDelay;
		this.confirmableType = confirmableType;
		this.autoInverse = autoInverse;
		this.batchOperation = batchOperation;
		this.settlementTrxType = settlementTrxType;
		this.statePrint = statePrint;
		this.uploadableType = uploadableType;
		this.transactionBatchUploadType = transactionBatchUploadType;
		this.transactionTrailerSettleType = transactionTrailerSettleType;
		this.annulmentType = annulmentType;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del metodo */
	private String name;
	/** Descripcion opcional del metodo */
	private String description;
	/** Plantilla de Mensajes a la que esta asociado el metodo */
	private TemplateMessage templateMessage;
	/** Plantilla para construir el mensaje iso asociado al metodo */
	private IsoTemplate isoTemplate;
	/** Nombre de la primitiva a invocar en el codigo */
	private String methodName;
	/** Tiempo maximo de espera en segundos */
	private double timeout;
	/** Si se puede reintentar la transaccion dada la ocurrencia de timeout */
	private boolean retry;
	/** Numero de maximo de reintentos si ocurre timeout */
	private int retryAttempts;
	/** Tipo de transaccion que debe recibir como respuesta */
	private TransactionType transactionResponseType;
	/** Indica si la transaccion es de tipo inversa */
	private boolean inverseTrxType;
	/** Indica si la transaccion debe bloquearse en espera de confirmacion */
	private boolean blocksOnConf;
	/** Indica si la transaccion debe bloquearse en espera de inversa */
	private boolean blocksOnInv;
	/** Tipo de transaccion inversa a invocar en caso de fallo o no respuesta */
	private TransactionType transactionInverseType;
	/** Indica si la transaccion es de tipo confirmacion */
	private boolean confirmationTrxType;
	/** Tipo de transaccion de confirmacion si es requerida **/
	private TransactionType transactionConfirmationType;
	/** Indica si la transaccion es de tipo reversible **/
	private boolean reversableType;
	/** Tiempo de espera antes de realizar un reintento */
	private double retryDelay;
	/** Indica si la transaccion es de tipo confirmable **/
	private boolean confirmableType;
	/** Indica si el tipo de transaccion ejecutara la inversa automatica **/
	private boolean autoInverse;
	/** Indica el tipo de operacion de lote/cierre que se le aplica **/
	private BatchOperation batchOperation;
	/** Indica si la transaccion es de tipo cierre */
	private boolean settlementTrxType;
	/** Estado de impresión*/
	protected boolean statePrint;
	/** Indica si el tipo de transaccion se reenvia en batch upload **/
	private boolean uploadableType;
	/** Tipo de transaccion batch upload asociada **/
	private TransactionType transactionBatchUploadType;
	/** Tipo de transaccion de cierre trailer **/
	private TransactionType transactionTrailerSettleType;
	/** Indica si el tipo anula transacciones **/
	private boolean annulmentType;
	/** Lista de elementos de voucher adicionales **/
	private List<AdditionalVoucherFields> voucherFields;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_type_id_seq")
	@SequenceGenerator(name="transaction_type_id_seq", sequenceName = "transaction_type_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the templateMessage
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "template_message_id")
	public TemplateMessage getTemplateMessage() {
		return templateMessage;
	}

	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(TemplateMessage templateMessage) {
		this.templateMessage = templateMessage;
	}

	/**
	 * @return the isoTemplate
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "iso_template_id")
	public IsoTemplate getIsoTemplate() {
		return isoTemplate;
	}

	/**
	 * @param isoTemplate the isoTemplate to set
	 */
	public void setIsoTemplate(IsoTemplate isoTemplate) {
		this.isoTemplate = isoTemplate;
	}

	/**
	 * @return the methodName
	 */
	@Column(name="method_name")
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the timeout
	 */
	public double getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(double timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the retry
	 */
	public boolean isRetry() {
		return retry;
	}

	/**
	 * @param retry the retry to set
	 */
	public void setRetry(boolean retry) {
		this.retry = retry;
	}

	/**
	 * @return the retryAttempts
	 */
	@Column(name="retry_attempts")
	public int getRetryAttempts() {
		return retryAttempts;
	}

	/**
	 * @param retryAttempts the retryAttempts to set
	 */
	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}
	
	/**
	 * @return the transactionResponseType
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transaction_response_type")
	public TransactionType getTransactionResponseType() {
		return transactionResponseType;
	}

	/**
	 * @param transactionResponseType the transactionResponseType to set
	 */
	public void setTransactionResponseType(TransactionType transactionResponseType) {
		this.transactionResponseType = transactionResponseType;
	}
	
	/**
	 * @return the transactionInverseType
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transaction_inverse_type")
	public TransactionType getTransactionInverseType() {
		return transactionInverseType;
	}

	/**
	 * @param transactionInverseType the transactionInverseType to set
	 */
	public void setTransactionInverseType(TransactionType transactionInverseType) {
		this.transactionInverseType = transactionInverseType;
	}//##################################
	
	/**
	 * @return the inverseTrxType
	 */
	@Column(name="inverse_trx_type")
	public boolean isInverseTrxType() {
		return inverseTrxType;
	}

	/**
	 * @param inverseTrxType the inverseTrxType to set
	 */
	public void setInverseTrxType(boolean inverseTrxType) {
		this.inverseTrxType = inverseTrxType;
	}

	/**
	 * @return the blocksOnConf
	 */
	@Column(name="blocks_on_conf")
	public boolean isBlocksOnConf() {
		return blocksOnConf;
	}

	/**
	 * @param blocksOnConf the blocksOnConf to set
	 */
	public void setBlocksOnConf(boolean blocksOnConf) {
		this.blocksOnConf = blocksOnConf;
	}

	/**
	 * @return the blocksOnInv
	 */
	@Column(name="block_on_inv")
	public boolean isBlocksOnInv() {
		return blocksOnInv;
	}

	/**
	 * @param blocksOnInv the blocksOnInv to set
	 */
	public void setBlocksOnInv(boolean blocksOnInv) {
		this.blocksOnInv = blocksOnInv;
	}

	/**
	 * @return the confirmationTrxType
	 */
	@Column(name="confirmation_trx_type")
	public boolean isConfirmationTrxType() {
		return confirmationTrxType;
	}

	/**
	 * @param confirmationTrxType the confirmationTrxType to set
	 */
	public void setConfirmationTrxType(boolean confirmationTrxType) {
		this.confirmationTrxType = confirmationTrxType;
	}

	/**
	 * @return the transactionConfirmationType
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transaction_conf_type")
	public TransactionType getTransactionConfirmationType() {
		return transactionConfirmationType;
	}

	/**
	 * @param transactionConfirmationType the transactionConfirmationType to set
	 */
	public void setTransactionConfirmationType(TransactionType transactionConfirmationType) {
		this.transactionConfirmationType = transactionConfirmationType;
	}

	/**
	 * @return the reversableType
	 */
	@Column(name="reversable_type")
	public boolean isReversableType() {
		return reversableType;
	}

	/**
	 * @param reversableType the reversableType to set
	 */
	public void setReversableType(boolean reversableType) {
		this.reversableType = reversableType;
	}

	/**
	 * @return the retryDelay
	 */
	@Column(name="retry_delay")
	public double getRetryDelay() {
		return retryDelay;
	}

	/**
	 * @param retryDelay the retryDelay to set
	 */
	public void setRetryDelay(double retryDelay) {
		this.retryDelay = retryDelay;
	}

	/**
	 * @return the confirmableType
	 */
	@Column(name="confirmable_type")
	public boolean isConfirmableType() {
		return confirmableType;
	}

	/**
	 * @param confirmableType the confirmableType to set
	 */
	public void setConfirmableType(boolean confirmableType) {
		this.confirmableType = confirmableType;
	}
	
	/**
	 * @return the autoInverse
	 */
	@Column(name="auto_inverse")
	public boolean isAutoInverse() {
		return autoInverse;
	}

	/**
	 * @param autoInverse the autoInverse to set
	 */
	public void setAutoInverse(boolean autoInverse) {
		this.autoInverse = autoInverse;
	}
	
	/**
	 * @return the batchOperation
	 */
	@Column(name="batch_operation")
	public BatchOperation getBatchOperation() {
		return batchOperation;
	}

	/**
	 * @param batchOperation the batchOperation to set
	 */
	public void setBatchOperation(BatchOperation batchOperation) {
		this.batchOperation = batchOperation;
	}
	
	/**
	 * @return the settlementTrxType
	 */
	@Column(name="settlement_trx_type")
	public boolean isSettlementTrxType() {
		return settlementTrxType;
	}

	/**
	 * @param settlementTrxType the settlementTrxType to set
	 */
	public void setSettlementTrxType(boolean settlementTrxType) {
		this.settlementTrxType = settlementTrxType;
	}

	@Column(name="state_print")
	public boolean isStatePrint() {
		return statePrint;
	}

	public void setStatePrint(boolean statePrint) {
		this.statePrint = statePrint;
	}
	
	/**
	 * @return the uploadableType
	 */
	@Column(name="uploadable_type")
	public boolean isUploadableType() {
		return uploadableType;
	}

	/**
	 * @param uploadableType the uploadableType to set
	 */
	public void setUploadableType(boolean uploadableType) {
		this.uploadableType = uploadableType;
	}

	/**
	 * @return the batchUploadType
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "batch_upload_type")
	public TransactionType getTransactionBatchUploadType() {
		return transactionBatchUploadType;
	}

	/**
	 * @param batchUploadType the batchUploadType to set
	 */
	public void setTransactionBatchUploadType(TransactionType transactionBatchUploadType) {
		this.transactionBatchUploadType = transactionBatchUploadType;
	}
	
	/**
	 * @return the trailerSettleType
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "closing_trailer_type")
	public TransactionType getTransactionTrailerSettleType() {
		return transactionTrailerSettleType;
	}

	/**
	 * @param trailerSettleType the trailerSettleType to set
	 */
	public void setTransactionTrailerSettleType(TransactionType transactionTrailerSettleType) {
		this.transactionTrailerSettleType = transactionTrailerSettleType;
	}
	
	/**
	 * @return the annulmentType
	 */
	@Column(name="annulment_type")
	public boolean isAnnulmentType() {
		return annulmentType;
	}

	/**
	 * @param annulmentType the annulmentType to set
	 */
	public void setAnnulmentType(boolean annulmentType) {
		this.annulmentType = annulmentType;
	}
	
	/**
	 * @return the voucherFields
	 */
	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="transaction_type_id")
	public List<AdditionalVoucherFields> getVoucherFields() {
		return voucherFields;
	}

	/**
	 * @param voucherFields the voucherFields to set
	 */
	public void setVoucherFields(List<AdditionalVoucherFields> voucherFields) {
		this.voucherFields = voucherFields;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((isoTemplate == null) ? 0 : isoTemplate.hashCode());
		result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((templateMessage == null) ? 0 : templateMessage.hashCode());
		result = prime * result + (retry ? 1231 : 1237);
		result = prime * result + retryAttempts;
		long temp;
		temp = Double.doubleToLongBits(timeout);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((transactionResponseType == null) ? 0 : transactionResponseType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionType other = (TransactionType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (isoTemplate == null) {
			if (other.isoTemplate != null)
				return false;
		} else if (!isoTemplate.equals(other.isoTemplate))
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (templateMessage == null) {
			if (other.templateMessage != null)
				return false;
		} else if (!templateMessage.equals(other.templateMessage))
			return false;
		if (retry != other.retry)
			return false;
		if (retryAttempts != other.retryAttempts)
			return false;
		if (Double.doubleToLongBits(timeout) != Double.doubleToLongBits(other.timeout))
			return false;
		if (transactionResponseType == null) {
			if (other.transactionResponseType != null)
				return false;
		} else if (!transactionResponseType.equals(other.transactionResponseType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionType [id=" + id + ", name=" + name + ", description=" + description + ", templateMessage="
				+ templateMessage + ", isoTemplate=" + isoTemplate + ", methodName=" + methodName + ", timeout="
				+ timeout + ", retry=" + retry + ", retryAttempts=" + retryAttempts + ", transactionResponseType="
				+ transactionResponseType + "]";
	}

}
