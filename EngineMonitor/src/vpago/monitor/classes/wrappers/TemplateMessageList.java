package vpago.monitor.classes.wrappers;

import java.util.List;

import vpago.monitor.classes.TemplateMessage;

public class TemplateMessageList {
	
	public TemplateMessageList() {
		super();
	}

	public TemplateMessageList(List<TemplateMessage> templateMessage) {
		super();
		this.templateMessage = templateMessage;
	}

	/** Lista de Plantillas de Mensajes **/
	private List<TemplateMessage> templateMessage;

	/**
	 * @return the templateMessage
	 */
	public List<TemplateMessage> getTemplateMessage() {
		return templateMessage;
	}

	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(List<TemplateMessage> templateMessage) {
		this.templateMessage = templateMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateMessageList [templateMessage=" + templateMessage + "]";
	}
	
	
}
