package vpago.monitor.classes.wrappers;

import java.util.List;

import vpago.monitor.classes.TransactionType;

public class TransactionTypeList {
	
	public TransactionTypeList() {
		super();
	}

	public TransactionTypeList(List<TransactionType> transactionType) {
		super();
		this.transactionType = transactionType;
	}

	/** Lista de tipos de transacciones **/
	private List<TransactionType> transactionType;

	/**
	 * @return the transactionType
	 */
	public List<TransactionType> getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(List<TransactionType> transactionType) {
		this.transactionType = transactionType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionTypeList [transactionType=" + transactionType + "]";
	}
	
	
}
