package vpago.monitor.classes.wrappers;

import java.util.List;

import vpago.monitor.classes.IsoTemplate;

public class IsoTemplateList {
	
	public IsoTemplateList() {
		super();
	}

	public IsoTemplateList(List<IsoTemplate> isoTemplate) {
		super();
		this.isoTemplate = isoTemplate;
	}

	/** Lista de Plantillas ISO **/
	private List<IsoTemplate> isoTemplate;

	/**
	 * @return the isoTemplate
	 */
	public List<IsoTemplate> getIsoTemplate() {
		return isoTemplate;
	}

	/**
	 * @param isoTemplate the isoTemplate to set
	 */
	public void setIsoTemplate(List<IsoTemplate> isoTemplate) {
		this.isoTemplate = isoTemplate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IsoTemplateList [isoTemplate=" + isoTemplate + "]";
	}
	
	
}
