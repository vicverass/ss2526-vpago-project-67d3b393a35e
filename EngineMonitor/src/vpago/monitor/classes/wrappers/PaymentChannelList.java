package vpago.monitor.classes.wrappers;

import java.util.List;

import vpago.monitor.classes.PaymentChannel;

public class PaymentChannelList {
	
	public PaymentChannelList() {
		super();
	}

	public PaymentChannelList(List<PaymentChannel> paymentChannel) {
		super();
		this.paymentChannel = paymentChannel;
	}

	/** Lista de Canales de Pago **/
	private List<PaymentChannel> paymentChannel;

	/**
	 * @return the paymentChannel
	 */
	public List<PaymentChannel> getPaymentChannel() {
		return paymentChannel;
	}

	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(List<PaymentChannel> paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentChannelList [paymentChannel=" + paymentChannel + "]";
	}
	
}
