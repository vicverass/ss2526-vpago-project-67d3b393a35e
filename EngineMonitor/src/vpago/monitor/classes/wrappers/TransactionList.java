package vpago.monitor.classes.wrappers;

import java.util.List;

import vpago.monitor.classes.minified.TransactionMin;

public class TransactionList {

	public TransactionList() {
		super();
	}
	
	public TransactionList(List<TransactionMin> transaction) {
		super();
		this.transaction = transaction;
	}
	
	/** Lista de Transacciones */
	private List<TransactionMin> transaction;

	/**
	 * @return the transaction
	 */
	public List<TransactionMin> getTransaction() {
		return transaction;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(List<TransactionMin> transaction) {
		this.transaction = transaction;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionList [transaction=" + transaction + "]";
	}
	

}
