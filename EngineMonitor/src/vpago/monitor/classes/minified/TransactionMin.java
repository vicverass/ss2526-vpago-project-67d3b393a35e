package vpago.monitor.classes.minified;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.monitor.classes.GeneralCardProduct;
import vpago.monitor.classes.Terminal;
import vpago.monitor.classes.TransactionParameter;
import vpago.monitor.classes.TransactionResponse;
import vpago.monitor.classes.TransactionResponseParameter;
import vpago.monitor.classes.TransactionType;
import vpago.monitor.classes.types.TransactionStatus;

public class TransactionMin {
	private static final Logger logger = LoggerFactory.getLogger(TransactionMin.class);

	public TransactionMin() {
		super();
	}
	
	public TransactionMin(int id, String hashCode, Terminal terminal, double amount, Calendar txnDate, String controlToken, 
			TransactionStatus transactionStatus, TransactionType transactionType, TransactionResponse transactionResponse,
			boolean retried, int retryAttempts, String secToken) {
		super();
		logger.trace("Constructor para transaccion {}", id);
		this.id = id;
		this.hashCode = hashCode;
		this.terminal = terminal;
		this.terminalGuid = terminal.getTerminalGuid();
		this.terminalBankId = null;
		this.merchantIdCode = null;
		this.amount = amount;
		this.txnDate = txnDate;
		this.date = (new SimpleDateFormat("dd/MM/yyyy hh:mm:ss")).format(txnDate.getTime()) ;
		this.controlToken = controlToken;
		this.transactionStatus = transactionStatus;
		this.transactionType = transactionType;
		this.transactionTypeDescription = transactionType.getDescription();
		this.transactionResponse = transactionResponse;
		this.transactionResponseDesc = transactionResponse.getCode() + ", " + transactionResponse.getName();
		this.cardProduct = null;
		this.retried = retried;
		this.retryAttempts = retryAttempts;
		this.secToken = secToken;
	}
	
	public TransactionMin(int id, Set<TransactionParameter> parameters, Set<TransactionResponseParameter> responseParameters) {
		this.id = id;
		this.parameters = parameters;
		this.responseParameters = responseParameters;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Codigo único generado para la transaccion */
	private String hashCode;
	/** Terminal que genero la transaccion */
	private Terminal terminal;
	/** Identificador del terminal que genero la transaccion */
	private String terminalGuid;
	/** Campo 41 ISO8583 */
	private String terminalBankId;
	/** Campo 42 ISO8583 */
	private String merchantIdCode;
	/** Monto de la transacción */
	private double amount;
	/** Fecha en que se realizo la transaccion */
	private Calendar txnDate;
	/** Fecha en que se realizo la transaccion con formato */
	private String date;
	/** Código de control enviado por el terminal */
	private String controlToken;
	/** Estado de la transaccion */
	private TransactionStatus transactionStatus;
	/** Identificador del tipo de transaccion */
	private TransactionType transactionType;
	/** Nombre del tipo de transaccion **/
	private String transactionTypeDescription;
	/** Respuesta de la transaccion */
	private TransactionResponse transactionResponse;
	/** Descripcion de la respuesta **/
	private String transactionResponseDesc;
	/** Marca de tarjeta */
	private GeneralCardProduct cardProduct;
	/** Si se vencio el timeout para recibir respuesta a la transaccion */
	private boolean retried;
	/** Numero de veces que se reintento realizar la transaccion */
	private int retryAttempts;
	/** Firma de seguridad para validacion de integridad y evitar duplicacion */
	private String secToken;
	/** Conjunto de parametros asociados (solicitud) */
	private Set<TransactionParameter> parameters; 
	/** Conjunto de parametros asociados (respuesta) */
	private Set<TransactionResponseParameter> responseParameters; 
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the hashCode
	 */
	public String getHashCode() {
		return hashCode;
	}

	/**
	 * @param hashCode the hashCode to set
	 */
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	/**
	 * @return the terminal
	 */
	public Terminal getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}
	
	/**
	 * @return the terminalGuid
	 */
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	public void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the terminalBankId
	 */
	public String getTerminalBankId() {
		return terminalBankId;
	}

	/**
	 * @param terminalBankId the terminalBankId to set
	 */
	public void setTerminalBankId(String terminalBankId) {
		this.terminalBankId = terminalBankId;
	}

	/**
	 * @return the merchantIdCode
	 */
	public String getMerchantIdCode() {
		return merchantIdCode;
	}

	/**
	 * @param merchantIdCode the merchantIdCode to set
	 */
	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the txnDate
	 */
	public Calendar getTxnDate() {
		return txnDate;
	}

	/**
	 * @param txnDate the txnDate to set
	 */
	public void setTxnDate(Calendar txnDate) {
		this.txnDate = txnDate;
	}
	
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the controlToken
	 */
	public String getControlToken() {
		return controlToken;
	}

	/**
	 * @param controlToken the controlToken to set
	 */
	public void setControlToken(String controlToken) {
		this.controlToken = controlToken;
	}

	/**
	 * @return the transactionStatus
	 */
	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	/**
	 * @return the transactionType
	 */
	public TransactionType getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	/**
	 * @return the transactionTypeName
	 */
	public String getTransactionTypeDescription() {
		return transactionTypeDescription;
	}

	/**
	 * @param transactionTypeName the transactionTypeName to set
	 */
	public void setTransactionTypeDescription(String transactionTypeName) {
		this.transactionTypeDescription = transactionTypeName;
	}

	/**
	 * @return the transactionResponse
	 */
	public TransactionResponse getTransactionResponse() {
		return transactionResponse;
	}

	/**
	 * @param transactionResponse the transactionResponse to set
	 */
	public void setTransactionResponse(TransactionResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}

	/**
	 * @return the transactionResponseDesc
	 */
	public String getTransactionResponseDesc() {
		return transactionResponseDesc;
	}

	/**
	 * @param transactionResponseDesc the transactionResponseDesc to set
	 */
	public void setTransactionResponseDesc(String transactionResponseDesc) {
		this.transactionResponseDesc = transactionResponseDesc;
	}

	/**
	 * @return the cardProduct
	 */
	public GeneralCardProduct getCardProduct() {
		return cardProduct;
	}

	/**
	 * @param cardProduct the cardProduct to set
	 */
	public void setCardProduct(GeneralCardProduct cardProduct) {
		this.cardProduct = cardProduct;
	}

	/**
	 * @return the retried
	 */
	public boolean isRetried() {
		return retried;
	}

	/**
	 * @param retried the retried to set
	 */
	public void setRetried(boolean retried) {
		this.retried = retried;
	}

	/**
	 * @return the retryAttempts
	 */
	public int getRetryAttempts() {
		return retryAttempts;
	}

	/**
	 * @param retryAttempts the retryAttempts to set
	 */
	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}

	/**
	 * @return the secToken
	 */
	public String getSecToken() {
		return secToken;
	}

	/**
	 * @param secToken the secToken to set
	 */
	public void setSecToken(String secToken) {
		this.secToken = secToken;
	}
	
	/**
	 * @return the parameters
	 */
	public Set<TransactionParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Set<TransactionParameter> parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the responseParameters
	 */
	public Set<TransactionResponseParameter> getResponseParameters() {
		return responseParameters;
	}

	/**
	 * @param responseParameters the responseParameters to set
	 */
	public void setResponseParameters(Set<TransactionResponseParameter> responseParameters) {
		this.responseParameters = responseParameters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionMin [id=" + id + ", hashCode=" + hashCode + ", terminal=" + terminal + ", terminalBankId="
				+ terminalBankId + ", merchantIdCode=" + merchantIdCode + ", amount=" + amount + ", txnDate=" + txnDate
				+ ", controlToken=" + controlToken + ", transactionStatus=" + transactionStatus + ", transactionType="
				+ transactionType + ", transactionResponse=" + transactionResponse + ", cardProduct=" + cardProduct
				+ ", retried=" + retried + ", retryAttempts=" + retryAttempts + ", secToken=" + secToken + "]";
	}
	

}
