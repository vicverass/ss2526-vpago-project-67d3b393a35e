package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/**
 * Clase persistente para representar una Regla
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="rule_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("rule")
public class Rule {
	
	public Rule() {
		super();
	}
	
	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Adquiriente */
	protected Acquirer acquirer;
	/** Afiliacion a la que esta asociada la Regla */
	protected Affiliation affiliation;
	/** Tipo de Regla */
	private String ruleType;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="rule_id_seq")
	@SequenceGenerator(name="rule_id_seq", sequenceName = "rule_id_seq")
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the acquirer
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "acquirer_id", insertable=false, updatable=false)
	public Acquirer getAcquirer() {
		return acquirer;
	}
	
	/**
	 * @param acquirer the acquirer to set
	 */
	public void setAcquirer(Acquirer acquirer) {
		this.acquirer = acquirer;
	}
	
	/**
	 * @return the affiliation
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}
	
	/**
	 * @return the ruleType
	 */
	@Column(name="rule_type", insertable=false, updatable=false)
	public String getRuleType() {
		return ruleType;
	}
	
	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Rule [id=" + id  + ", acquirer=" + acquirer + ", rule_type=" + ruleType
				+ "]";
	}
	
}
