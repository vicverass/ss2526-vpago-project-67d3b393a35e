package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="batch_type")
public class BatchType {

	public BatchType() {
		super();
	}
	
	public BatchType(PaymentChannel paymentChannel, IsoTemplate isoTemplate, String paramName, String paramValue,
			String typeName, String typeDesc) {
		super();
		this.paymentChannel = paymentChannel;
		this.isoTemplate = isoTemplate;
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.typeName = typeName;
		this.typeDesc = typeDesc;
	}

	/** Identificador generado por el motor base de datos */
	private Integer id;
	/** Canal de pago **/
	private PaymentChannel paymentChannel;
	/** Plantilla ISO **/
	private IsoTemplate isoTemplate;
	/** Asociacion a parametro de plantilla **/
	private String paramName;
	/** Valor del parametro de lote **/
	private String paramValue;
	/** Nombre del tipo de lote **/
	private String typeName;
	/** Descripcion del tipo de lote **/
	private String typeDesc;

	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="batch_type_id_seq")
	@SequenceGenerator(name="batch_type_id_seq", sequenceName = "batch_type_id_seq")
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the paymentChannel
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}
	
	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the isoTemplate
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "iso_template_id")
	public IsoTemplate getIsoTemplate() {
		return isoTemplate;
	}
	
	/**
	 * @param isoTemplate the isoTemplate to set
	 */
	public void setIsoTemplate(IsoTemplate isoTemplate) {
		this.isoTemplate = isoTemplate;
	}
	
	/**
	 * @return the paramName
	 */
	@Column(name="param_name")
	public String getParamName() {
		return paramName;
	}
	
	/**
	 * @param paramName the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	/**
	 * @return the paramValue
	 */
	@Column(name="param_value")
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * @param paramValue the paramValue to set
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	/**
	 * @return the typeName
	 */
	@Column(name="batch_type_name")
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the typeDesc
	 */
	@Column(name="batch_type_desc")
	public String getTypeDesc() {
		return typeDesc;
	}

	/**
	 * @param typeDesc the typeDesc to set
	 */
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchType [id=" + id + ", paymentChannel=" + paymentChannel + ", isoTemplate=" + isoTemplate
				+ ", paramName=" + paramName + ", paramValue=" + paramValue + ", typeName=" + typeName + ", typeDesc="
				+ typeDesc + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
//	@Override
//	public String toString() {
//		return "BatchType [id=" + id + ", paymentChannel=" + paymentChannel + ", isoTemplate=" + isoTemplate
//				+ ", paramName=" + paramName + "]";
//	}
	
	

}
