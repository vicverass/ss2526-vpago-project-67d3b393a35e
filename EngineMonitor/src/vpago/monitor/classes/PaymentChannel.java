package vpago.monitor.classes;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import vpago.monitor.classes.types.DeviceValidation;


/**
 * Clase persistente para representar un Canal de Pago
 */
@Entity
@Table(name="payment_channel")
public class PaymentChannel {
	
	public PaymentChannel() {
		super();
	}

	public PaymentChannel(String name, String description, String codeName, 
			PaymentProcessor paymentProcessor, DeviceValidation deviceValidation) {
		super();		
		this.name = name;
		this.description = description;
		this.codeName = codeName;
		this.paymentProcessor = paymentProcessor;
		this.deviceValidation = deviceValidation;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del Canal de Pago */
	private String name;
	/** Descripción del Canal de Pago */
	private String description;
	/** Codigo que sera enviado por el Terminal */
	private String codeName;
	/** Plantillas de mensajes asociada al Canal de Pago */
	private List<TemplateMessage> templateMessage;
	/** Posibles mensajes de respuestas para las transacciones aasociadas */
	private Map<String, TransactionResponse> transactionRespTypes;
	/** Procesador de pagos **/
	private PaymentProcessor paymentProcessor;
	/** Lista de tipos de lotes asociados al canal de pago **/
	private Set<BatchType> batchTypes;
	/** Tipo de validacion de dispositivo **/
	private DeviceValidation deviceValidation;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="payment_channel_id_seq")
	@SequenceGenerator(name="payment_channel_id_seq", sequenceName = "payment_channel_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the codeName
	 */
	@Column(name="code_name")
	public String getCodeName() {
		return codeName;
	}

	/**
	 * @param codeName the codeName to set
	 */
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	
	/**
	 * @return the templateMessage
	 */
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="payment_channel_id")
	public List<TemplateMessage> getTemplateMessage() {
		return templateMessage;
	}

	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(List<TemplateMessage> templateMessage) {
		this.templateMessage = templateMessage;
	}
	
	/**
	 * @return the transactionRespTypes
	 */
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="payment_channel_id")
	@javax.persistence.MapKey(name="code")
	@OrderBy("id ASC")
	public Map<String, TransactionResponse> getTransactionRespTypes() {
		return transactionRespTypes;
	}

	/**
	 * @param transactionRespTypes the transactionRespTypes to set
	 */
	public void setTransactionRespTypes(Map<String, TransactionResponse> transactionRespTypes) {
		this.transactionRespTypes = transactionRespTypes;
	}
	
	/**
	 * @return the paymentProcessor
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_processor_id")
	public PaymentProcessor getPaymentProcessor() {
		return paymentProcessor;
	}

	/**
	 * @param paymentProcessor the paymentProcessor to set
	 */
	public void setPaymentProcessor(PaymentProcessor paymentProcessor) {
		this.paymentProcessor = paymentProcessor;
	}
	
	/**
	 * @return the batchTypes
	 */
	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="payment_channel_id")
	public Set<BatchType> getBatchTypes() {
		return batchTypes;
	}

	/**
	 * @param batchTypes the batchTypes to set
	 */
	public void setBatchTypes(Set<BatchType> batchTypes) {
		this.batchTypes = batchTypes;
	}
	
	/**
	 * @return the deviceValidation
	 */
	@Column(name="device_validation")
	public DeviceValidation getDeviceValidation() {
		return deviceValidation;
	}

	/**
	 * @param deviceValidation the deviceValidation to set
	 */
	public void setDeviceValidation(DeviceValidation deviceValidation) {
		this.deviceValidation = deviceValidation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentChannel other = (PaymentChannel) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentChannel [id=" + id + ", name=" + name + ", description=" + description + ", codeName=" + codeName
				+ "]";
	}
}
