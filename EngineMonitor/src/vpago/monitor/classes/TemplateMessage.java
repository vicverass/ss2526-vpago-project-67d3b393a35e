package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



/**
 * Clase persistente para representar una Plantilla de Mensajes
 */
@Entity
@Table(name="template_message")
public class TemplateMessage {
	
	public TemplateMessage() {
		super();
	}
	
	public TemplateMessage(int id, String terminalParamName) {
		super();
		this.id = id;
		this.terminalParamName = terminalParamName;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Canal de pago asociado **/
	private PaymentChannel paymentChannel;
	/** Nombre del parametro para los terminales bancarios asociados a este canal de pago */ 
	private String terminalParamName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="template_message_id_seq")
	@SequenceGenerator(name="template_message_id_seq", sequenceName = "template_message_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the paymentChannel
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}

	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	/**
	 * @return the terminalParamName
	 */
	@Column(name="terminal_param_name")
	public String getTerminalParamName() {
		return terminalParamName;
	}
	
	/**
	 * @param terminalParamName the terminalParamName to set
	 */
	public void setTerminalParamName(String terminalParamName) {
		this.terminalParamName = terminalParamName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateMessage [id=" + id + ", terminalParamName=" + terminalParamName + "]";
	}
	
}
