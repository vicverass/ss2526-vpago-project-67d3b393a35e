package vpago.monitor.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.annotations.DiscriminatorOptions;

/**
 * Clase persistente para representar un Parametro de Respuesta de la Transaccion
 */
@Entity
@DiscriminatorValue("response")
@DiscriminatorOptions(force = true)
public class TransactionResponseParameter extends TransactionParameter {

	public TransactionResponseParameter() {
		super();
	}

	public TransactionResponseParameter(Transaction transaction, String paramName, String paramValue) {
		super(transaction, paramName, paramValue);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionResponseParameter [id=" + id + ", paramName=" + paramName + ", paramValue=" + paramValue
				+ ", paramType=" + paramType + "]";
	}
	
}
