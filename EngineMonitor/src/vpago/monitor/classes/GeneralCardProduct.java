package vpago.monitor.classes;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar un Producto de Tarjeta General
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="product_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("general")
@Table(name="card_product")
public class GeneralCardProduct {

	public GeneralCardProduct() {
		super();
	}
	
	public GeneralCardProduct(CardFranchise cardFranchise, CardType cardType, Bin productBin) {
		super();
		this.cardFranchise = cardFranchise;
		this.cardType = cardType;
		this.productBin = productBin;
	}
	
	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Franquicia de la tarjeta */
	protected CardFranchise cardFranchise;
	/** Tipo de tarjeta */
	protected CardType cardType;
	/** Rango de bin para identificar el tipo de tarjeta */
	protected Bin productBin;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="card_product_id_seq")
	@SequenceGenerator(name="card_product_id_seq", sequenceName = "card_product_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cardFranchise
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "card_franchise_id")
	public CardFranchise getCardFranchise() {
		return cardFranchise;
	}

	/**
	 * @param cardFranchise the cardFranchise to set
	 */
	public void setCardFranchise(CardFranchise cardFranchise) {
		this.cardFranchise = cardFranchise;
	}

	/**
	 * @return the cardType
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "card_type_id")
	public CardType getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}
	
	/**
	 * @return the productBin
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "bin_id")
	public Bin getProductBin() {
		return productBin;
	}

	/**
	 * @param productBin the productBin to set
	 */
	public void setProductBin(Bin productBin) {
		this.productBin = productBin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardFranchise == null) ? 0 : cardFranchise.hashCode());
		result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneralCardProduct other = (GeneralCardProduct) obj;
		if (cardFranchise == null) {
			if (other.cardFranchise != null)
				return false;
		} else if (!cardFranchise.equals(other.cardFranchise))
			return false;
		if (cardType == null) {
			if (other.cardType != null)
				return false;
		} else if (!cardType.equals(other.cardType))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GeneralCardProduct [id=" + id + ", cardFranchise=" + cardFranchise + ", cardType=" + cardType
				+ ", productBin=" + productBin + "]";
	}
}
