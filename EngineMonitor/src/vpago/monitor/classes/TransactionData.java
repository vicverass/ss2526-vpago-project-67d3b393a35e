package vpago.monitor.classes;

import java.util.Map;

import javax.persistence.Transient;

/**
 * Clase que representa la estructura de Datos de Transaccion utilizada para proporcionar a TransactionHandler toda la informacion recibida en una invocacion al servicio
 * web que le es relevante
 */
public class TransactionData {
	
	public TransactionData() {
		super();
	}
	
	public TransactionData(String terminalId, String channelCode, String transactionType, String controlToken,
			String secToken, String cardNumber, Map<String, String> transactionParameters) {
		super();
		this.terminalId = terminalId;
		this.channelCode = channelCode;
		this.transactionType = transactionType;
		this.controlToken = controlToken;
		this.secToken = secToken;
		this.cardNumber = cardNumber;
		this.transactionParameters = transactionParameters;
	}

	/** Identificador generado por el motor base de datos */
	private String terminalId;
	/** Codigo del canal de pago */
	private String channelCode;
	/** Tipo de transaccion */
	private String transactionType;
	/** Numero de control enviado por el terminal */
	private String controlToken;
	/** Token de seguridad */
	private String secToken;
	/** Numero de la tarjeta */
	private String cardNumber;
	/** Elementos dentro de TransactionParameters en el formato de servicio web transaccional
	 * donde key: nombre del elemento y value: valor que contiene el elemento
	 */
	private Map<String, String> transactionParameters;
		
	/**
	 * @return the terminalId
	 */
	public String getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the channelCode
	 */
	public String getChannelCode() {
		return channelCode;
	}

	/**
	 * @param channelCode the channelCode to set
	 */
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the controlToken
	 */
	public String getControlToken() {
		return controlToken;
	}

	/**
	 * @param controlToken the controlToken to set
	 */
	public void setControlToken(String controlToken) {
		this.controlToken = controlToken;
	}

	/**
	 * @return the secToken
	 */
	public String getSecToken() {
		return secToken;
	}

	/**
	 * @param secToken the secToken to set
	 */
	public void setSecToken(String secToken) {
		this.secToken = secToken;
	}

	/**
	 * @return the cardNumber
	 */
	@Transient
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the transactionParameters
	 */
	public Map<String, String> getTransactionParameters() {
		return transactionParameters;
	}

	/**
	 * @param transactionParameters the transactionParameters to set
	 */
	public void setTransactionParameters(Map<String, String> transactionParameters) {
		this.transactionParameters = transactionParameters;
	}
	
	/**
	 * Inserta un par clave-valor en el conjunto de parametros de una Transaccion 
	 * 
	 * @param key Clave que identifica el parametro
	 * 
	 * @param value Valor del parametro
	 */
	public void insertTransactionField(String key, String value) {
		this.transactionParameters.put(key, value);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionData [terminalId=" + terminalId + ", channelCode=" + channelCode + ", transactionType="
				+ transactionType + ", controlToken=" + controlToken + ", secToken=" + secToken + ", transactionFields="
				+ transactionParameters + "]";
	}
}
