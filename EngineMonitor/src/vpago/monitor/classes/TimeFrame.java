package vpago.monitor.classes;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import vpago.monitor.classes.types.WeekDay;

/**
 *  Clase persistente para representar un Rango de Tiempo para la aplicacion de una Regla
 */
@Entity
@Table(name="time_frame")
public class TimeFrame {
	
	public TimeFrame() {
		super();
	}
	
	public TimeFrame(WeekDay weekDay, Calendar start, Calendar end) {
		super();
		this.weekDay = weekDay;
		this.start = start;
		this.end = end;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Dia de la semana */
	private WeekDay weekDay;
	/** Hora inicio */
	private Calendar start;
	/** Hora fin */
	private Calendar end;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="time_frame_id_seq")
	@SequenceGenerator(name="time_frame_id_seq", sequenceName = "time_frame_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the weekDay
	 */
	@Column(name="week_day")
	public WeekDay getWeekDay() {
		return weekDay;
	}

	/**
	 * @param weekDay the weekDay to set
	 */
	public void setWeekDay(WeekDay weekDay) {
		this.weekDay = weekDay;
	}

	/**
	 * @return the start
	 */
	@Column(name="start_time")
	public Calendar getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Calendar start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	@Column(name="end_time")
	public Calendar getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Calendar end) {
		this.end = end;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + id;
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		result = prime * result + ((weekDay == null) ? 0 : weekDay.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeFrame other = (TimeFrame) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (id != other.id)
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		if (weekDay != other.weekDay)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimeFrame [id=" + id + ", weekDay=" + weekDay + ", start=" + start + ", end=" + end + "]";
	}
	
}
