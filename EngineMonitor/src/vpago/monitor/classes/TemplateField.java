package vpago.monitor.classes;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.solab.iso8583.IsoType;

import vpago.monitor.classes.types.FieldLoadType;

/** Clase persistente que representa una Plantilla de Campo con la descripcion de un campo transaccional
 * en la plantilla de un esquema almacenado en el motor
 */
@Entity
@Table(name = "template_field")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	    name="field_level",
	    discriminatorType=DiscriminatorType.STRING
	)
@DiscriminatorValue("Field")
public class TemplateField {
	
	public TemplateField() {
		super();
	}
	
	public TemplateField(int fieldId, IsoType fieldType, int fieldLen, FieldLoadType loadType, String encoder,
			String decoder, String loadFunc, String storeFunc, boolean storable, boolean returnable, boolean mandatory,
			String fixedValue, String acquirerAttr, String className, IsoTemplate isoTemplate) {
		super();
		this.fieldId = fieldId;
		this.fieldType = fieldType;
		this.fieldLen = fieldLen;
		this.loadType = loadType;
		this.encoder = encoder;
		this.decoder = decoder;
		this.loadFunc = loadFunc;
		this.storeFunc = storeFunc;
		this.storable = storable;
		this.returnable = returnable;
		this.mandatory = mandatory;
		this.fixedValue = fixedValue;
		this.acquirerAttr = acquirerAttr;
		this.className = className;
		this.isoTemplate = isoTemplate;
	}

	/** Identificador generado por el motor de base de datos */
	protected int id;
	/** Identificador del campo ISO */
	protected int fieldId;
	/** Tipo de campo ISO de acuerdo a la libreria j8583 */
	protected IsoType fieldType;
	/** Longitud del campo ISO */
	protected int fieldLen;
	/** Tipo de Carga del parametro */
	protected FieldLoadType loadType;
	/** Nombre de la funcion codificadora del subcampo */
	protected String encoder;
	/** Nombre de la funcion decodificadora del campo */
	protected String decoder;
	/** Nombre de la funcion a aplicar durante la carga del parametro */
	protected String loadFunc;
	/** Nombre de la funcion a aplicar durante el almacenamiento del parametro */
	protected String storeFunc;
	/** Indica si el parametro debe almacenarse en la base de datos */
	protected boolean storable;
	/** Indica si el parametro debe retornarse en la respuesta de la solicitud */
	protected boolean returnable;
	/** Indica si es obligatoria la presencia del parametro en el mensaje */
	protected boolean mandatory;
	/** Almacena el valor del parametro en caso de que sea tipo FIXED */
	protected String fixedValue;
	/** Plantilla iso a la que esta asociado */
	protected IsoTemplate isoTemplate;
	/** Nombre del atributo de la clase Acquirer al que esta asociado en caso de ser de tipo RULE */
	protected String acquirerAttr;
	/** Nombre de la clase que contiene los metodos para manejar el campo */
	protected String className;
	/** Lista de subcampos */
	private List<TemplateSubField> subFields;

	/**
	 * @return the id
	 */
	@Id @GeneratedValue(strategy = GenerationType.AUTO, generator="template_field_id_seq")
	@SequenceGenerator(name="template_field_id_seq", sequenceName = "template_field_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the fieldId
	 */
	@Column(name="field_id")
	public int getFieldId() {
		return fieldId;
	}

	/**
	 * @param fieldId the fieldId to set
	 */
	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	/**
	 * @return the fieldType
	 */
	@Column(name="field_type")
	public IsoType getFieldType() {
		return fieldType;
	}

	/**
	 * @param fieldType the fieldType to set
	 */
	public void setFieldType(IsoType fieldType) {
		this.fieldType = fieldType;
	}

	/**
	 * @return the fieldLen
	 */
	@Column(name="field_len")
	public int getFieldLen() {
		return fieldLen;
	}

	/**
	 * @param fieldLen the fieldLen to set
	 */
	public void setFieldLen(int fieldLen) {
		this.fieldLen = fieldLen;
	}
	
	/**
	 * @return the loadType
	 */
	@Column(name="load_type")
	public FieldLoadType getLoadType() {
		return loadType;
	}

	/**
	 * @param loadType the loadType to set
	 */
	public void setLoadType(FieldLoadType loadType) {
		this.loadType = loadType;
	}
	
	/**
	 * @return the encoder
	 */
	public String getEncoder() {
		return encoder;
	}

	/**
	 * @param encoder the encoder to set
	 */
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}

	/**
	 * @return the decoder
	 */
	public String getDecoder() {
		return decoder;
	}

	/**
	 * @param decoder the decoder to set
	 */
	public void setDecoder(String decoder) {
		this.decoder = decoder;
	}

	/**
	 * @return the loadFunc
	 */
	@Column(name="load_func")
	public String getLoadFunc() {
		return loadFunc;
	}

	/**
	 * @param loadFunc the loadFunc to set
	 */
	public void setLoadFunc(String loadFunc) {
		this.loadFunc = loadFunc;
	}

	/**
	 * @return the storeFunc
	 */
	@Column(name="store_func")
	public String getStoreFunc() {
		return storeFunc;
	}

	/**
	 * @param storeFunc the storeFunc to set
	 */
	public void setStoreFunc(String storeFunc) {
		this.storeFunc = storeFunc;
	}

	/**
	 * @return the storable
	 */
	public boolean isStorable() {
		return storable;
	}

	/**
	 * @param storable the storable to set
	 */
	public void setStorable(boolean storable) {
		this.storable = storable;
	}
	
	/**
	 * @return the returnable
	 */
	public boolean isReturnable() {
		return returnable;
	}

	/**
	 * @param returnable the returnable to set
	 */
	public void setReturnable(boolean returnable) {
		this.returnable = returnable;
	}

	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the fixedValue
	 */
	@Column(name="fixed_value")
	public String getFixedValue() {
		return fixedValue;
	}

	/**
	 * @param fixedValue the fixedValue to set
	 */
	public void setFixedValue(String fixedValue) {
		this.fixedValue = fixedValue;
	}

	/**
	 * @return the isoTemplate
	 */
	@ManyToOne
	@JoinColumn(name="iso_template_id")
	public IsoTemplate getIsoTemplate() {
		return isoTemplate;
	}

	/**
	 * @param isoTemplate the isoTemplate to set
	 */
	public void setIsoTemplate(IsoTemplate isoTemplate) {
		this.isoTemplate = isoTemplate;
	}
	
	/**
	 * @return the accountAttr
	 */
	@Column(name="acquirer_attr")
	public String getAcquirerAttr() {
		return acquirerAttr;
	}

	/**
	 * @param acquirerAttr the acquirerAttr to set
	 */
	public void setAcquirerAttr(String acquirerAttr) {
		this.acquirerAttr = acquirerAttr;
	}

	/**
	 * @return the className
	 */
	@Column(name="class_name")
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the subFields
	 */
	@OneToMany(mappedBy="parentField", cascade=CascadeType.ALL) 
	@OrderBy("subfield_id ASC")
	public List<TemplateSubField> getSubFields() {
		return subFields;
	}

	/**
	 * @param subFields the subFields to set
	 */
	public void setSubFields(List<TemplateSubField> subFields) {
		this.subFields = subFields;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateField [id=" + id + ", fieldId=" + fieldId + ", fieldType=" + fieldType + ", fieldLen="
				+ fieldLen + ", loadType=" + loadType + ", encoder=" + encoder + ", decoder=" + decoder + ", loadFunc="
				+ loadFunc + ", storeFunc=" + storeFunc + ", storable=" + storable + ", returnable=" + returnable
				+ ", mandatory=" + mandatory + ", fixedValue=" + fixedValue + ", acquirerAttr=" + acquirerAttr
				+ ", className=" + className + "]";
	}
	
}
