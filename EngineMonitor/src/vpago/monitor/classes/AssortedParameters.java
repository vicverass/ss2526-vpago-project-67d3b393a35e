package vpago.monitor.classes;

import java.util.HashMap;
import java.util.Map;

/**
 * Clase para almacenar de manera clasificada los parametros de las transacciones
 */
public class AssortedParameters {
	
	public AssortedParameters() {
		super();
		this.returnableParams = new HashMap<String, String>();
		this.storableParams = new HashMap<String, String>();
	}
	
	public AssortedParameters(Map<String, String> returnableParams, Map<String, String> storableParams) {
		super();
		this.returnableParams = returnableParams;
		this.storableParams = storableParams;
	}

	/** Parametros a ser devueltos en la respuesta del servicio web */
	private Map<String, String> returnableParams;
	/** Parametros a almacenar en la base de datos */
	private Map<String, String> storableParams;
	
	/**
	 * @return the returnableParams
	 */
	public Map<String, String> getReturnableParams() {
		return returnableParams;
	}
	
	/**
	 * @param returnableParams the returnableParams to set
	 */
	public void setReturnableParams(Map<String, String> returnableParams) {
		this.returnableParams = returnableParams;
	}
	
	/**
	 * @return the storableParams
	 */
	public Map<String, String> getStorableParams() {
		return storableParams;
	}
	
	/**
	 * @param storableParams the storableParams to set
	 */
	public void setStorableParams(Map<String, String> storableParams) {
		this.storableParams = storableParams;
	}
	
	

}
