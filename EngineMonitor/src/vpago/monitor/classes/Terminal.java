package vpago.monitor.classes;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * Clase persistente para representar un Terminal
 *
 */
@Entity
@Table(name="terminal")
public class Terminal {

	public Terminal() {
		super();
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador unico del terminal en la base de datos */
	private String terminalGuid;
	/** Llave para autenticar al terminal */
	private String accessKey;
	/** Terminal activo */
	private boolean active;
	/** Afiliacion a la que esta asociado el Terminal */
	private Affiliation affiliation;
	/** Lista de terminales bancarios */
	private List<TerminalParameter> terminalParameters;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="terminal_id_seq")
	@SequenceGenerator(name="terminal_id_seq", sequenceName = "terminal_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the terminalGuid
	 */
	@Column(name="terminal_guid")
	@NotNull
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	@SuppressWarnings("unused")
	private void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the accessKey
	 */
	@Column(name="access_key")
	@NotNull
	public String getAccessKey() {
		return accessKey;
	}

	/**
	 * @param accessKey the accessKey to set
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	/**
	 * @return the active
	 */
	@NotNull
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the affiliation
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}
	
	/**
	 * @return the terminalParameters
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="terminal_id")
	public List<TerminalParameter> getTerminalParameters() {
		return terminalParameters;
	}

	/**
	 * @param terminalParameters the terminalParameters to set
	 */
	public void setTerminalParameters(List<TerminalParameter> terminalParameters) {
		this.terminalParameters = terminalParameters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Terminal [id=" + id + ", terminalGuid=" + terminalGuid + ", accessKey=" + accessKey + ", active="
				+ active + "]";
	}
	
	
}
