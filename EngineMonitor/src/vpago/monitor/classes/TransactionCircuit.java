package vpago.monitor.classes;

import java.util.List;
import java.util.Map;

public class TransactionCircuit {
	
	public TransactionCircuit() {
		super();
	}

	public TransactionCircuit(PaymentChannel paymentChannel, TemplateMessage templateMessage,
			List<TransactionType> transactionTypes, Map<String, TransactionResponse>  transactionResponses,
			PaymentProcessor paymentProcessor) {
		super();
		this.paymentChannel = paymentChannel;
		this.templateMessage = templateMessage;
		this.transactionTypes = transactionTypes;
		this.transactionResponses = transactionResponses;
		this.paymentProcessor = paymentProcessor;
	}

	private PaymentChannel paymentChannel;
	private TemplateMessage templateMessage;
	private List<TransactionType> transactionTypes;
	private Map<String, TransactionResponse> transactionResponses;
	private PaymentProcessor paymentProcessor;
	
	/**
	 * @return the paymentChannel
	 */
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}
	
	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the templateMessage
	 */
	public TemplateMessage getTemplateMessage() {
		return templateMessage;
	}
	
	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(TemplateMessage templateMessage) {
		this.templateMessage = templateMessage;
	}

	/**
	 * @return the transactionTypes
	 */
	public List<TransactionType> getTransactionTypes() {
		return transactionTypes;
	}

	/**
	 * @param transactionTypes the transactionTypes to set
	 */
	public void setTransactionTypes(List<TransactionType> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	/**
	 * @return the transactionResponses
	 */
	public Map<String, TransactionResponse> getTransactionResponses() {
		return transactionResponses;
	}

	/**
	 * @param transactionResponses the transactionResponses to set
	 */
	public void setTransactionResponses(Map<String, TransactionResponse>  transactionResponses) {
		this.transactionResponses = transactionResponses;
	}

	/**
	 * @return the paymentProcessor
	 */
	public PaymentProcessor getPaymentProcessor() {
		return paymentProcessor;
	}

	/**
	 * @param paymentProcessor the paymentProcessor to set
	 */
	public void setPaymentProcessor(PaymentProcessor paymentProcessor) {
		this.paymentProcessor = paymentProcessor;
	}
	
	
	
}
