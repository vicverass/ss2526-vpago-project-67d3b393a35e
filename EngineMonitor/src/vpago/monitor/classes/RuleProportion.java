package vpago.monitor.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Clase persistente para representar una Regla por Proporcion
 */
@Entity
@DiscriminatorValue("proportion")
public class RuleProportion extends Rule {
	
	public RuleProportion() {
		super();
	}

	/** Proporcion */
	private int proportion;

	/**
	 * @return the proportion
	 */
	public int getProportion() {
		return proportion;
	}

	/**
	 * @param proportion the proportion to set
	 */
	public void setProportion(int proportion) {
		this.proportion = proportion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProportion [id=" + id + ", acquirer=" + acquirer + ", proportion="
				+ proportion + "]";
	}
	
}
