package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Clase persistente para representar un Banco
 */
@Entity
@Table(name="bank")
public class Bank {
	
	public Bank() {
		super();
	}
	
	public Bank(String name, String rif, String acquirerCode) {
		super();
		this.name = name;
		this.rif = rif;
		this.acquirerCode = acquirerCode;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre para mostrar */
	private String name;
	/** Registro de Informacion Fiscal */
	private String rif;
	/** Codigo adquiriente */
	private String acquirerCode;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="bank_id_seq")
	@SequenceGenerator(name="bank_id_seq", sequenceName = "bank_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rif
	 */
	@NotNull
	public String getRif() {
		return rif;
	}

	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	/**
	 * @return the acquirerCode
	 */
	@Column(name="acquirer_code")
	public String getAcquirerCode() {
		return acquirerCode;
	}

	/**
	 * @param acquirerCode the acquirerCode to set
	 */
	public void setAcquirerCode(String acquirerCode) {
		this.acquirerCode = acquirerCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rif == null) ? 0 : rif.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bank other = (Bank) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rif == null) {
			if (other.rif != null)
				return false;
		} else if (!rif.equals(other.rif))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bank [id=" + id + ", name=" + name + ", rif=" + rif + ", acquirerCode=" + acquirerCode + "]";
	}
	
	
}
