package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Clase persistente para representar una Respuesta de Transaccion
 */
@Entity
@Table(name="transaction_response")
public class TransactionResponse {
	
	public TransactionResponse() {
		super();
	}
	
	public TransactionResponse(String code, String name, PaymentChannel paymentChannel, String description,
			boolean approvedCode, TransactionType transactionType, boolean unbalanceCode) {
		super();
		this.code = code;
		this.name = name;
		this.paymentChannel = paymentChannel;
		this.description = description;
		this.approvedCode = approvedCode;
		this.transactionType = transactionType;
		this.unbalanceCode = unbalanceCode;
	}
	
	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Codigo de respuesta de acuerdo al campo 39 ISO8583  */
	protected String code;
	/** Nombre de la respuesta  */
	protected String name;
	/** Canal de Pago al que esta asociado este tipo de respuesta  */
	protected PaymentChannel paymentChannel;
	/** Breve descripcion de la respuesta  */
	protected String description;
	/** Indica si el codigo es de aprobacion **/
	protected boolean approvedCode;
	/** Indica si el codigo es de aprobacion para una transaccion determinada **/
	protected TransactionType transactionType;
	/** Indica si el tipo de respuesta es debido a un descuadre **/
	protected boolean unbalanceCode;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_response_id_seq")
	@SequenceGenerator(name="transaction_response_id_seq", sequenceName = "transaction_response_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the paymentChannel
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}

	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	

	/**
	 * @return the approvedCode
	 */
	@Column(name="approved_code")
	public boolean isApprovedCode() {
		return approvedCode;
	}

	/**
	 * @param approvedCode the approvedCode to set
	 */
	public void setApprovedCode(boolean approvedCode) {
		this.approvedCode = approvedCode;
	}

	/**
	 * @return the transactionType
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction_type")
	public TransactionType getTransactionType() {
		return transactionType;
	}

	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	/**
	 * @return the unbalanceCode
	 */
	@Column(name="unbalance_code")
	public boolean isUnbalanceCode() {
		return unbalanceCode;
	}

	/**
	 * @param unbalanceCode the unbalanceCode to set
	 */
	public void setUnbalanceCode(boolean unbalanceCode) {
		this.unbalanceCode = unbalanceCode;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((paymentChannel == null) ? 0 : paymentChannel.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionResponse other = (TransactionResponse) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (paymentChannel == null) {
			if (other.paymentChannel != null)
				return false;
		} else if (!paymentChannel.equals(other.paymentChannel))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionResponse [id=" + id + ", code=" + code + ", name=" + name + ", paymentChannel="
				+ paymentChannel + ", description=" + description + "]";
	}
	
}
