package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar un Parametro de Terminal
 */
@Entity
@Table(name="terminal_parameter")
public class TerminalParameter {
	
	public TerminalParameter() {
		super();
	}
	
	public TerminalParameter(Terminal terminal, Acquirer acquirer, String name, String value) {
		super();
		this.terminal = terminal;
		this.acquirer = acquirer;
		this.name = name;
		this.value = value;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Terminal al que esta asociado el Parametro de Terminal */
	private Terminal terminal;
	/** Adquiriente al que esta asociado el Parametro de Terminal */
	private Acquirer acquirer;
	/** Nombre del Parametro de Terminal */
	private String name;
	/** Valor del Parametro de Terminal */
	private String value;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="terminal_parameter_id_seq")
	@SequenceGenerator(name="terminal_parameter_id_seq", sequenceName = "terminal_parameter_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the terminal
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	/**
	 * @return the acquirer
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "acquirer_id")
	public Acquirer getAcquirer() {
		return acquirer;
	}

	/**
	 * @param acquirer the acquirer to set
	 */
	public void setAcquirer(Acquirer acquirer) {
		this.acquirer = acquirer;
	}

	/**
	 * @return the name
	 */
	@Column(name="parameter_name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	@Column(name="parameter_value")
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalParameter [id=" + id + ", terminal=" + terminal + ", acquirer=" + acquirer + ", name=" + name
				+ ", value=" + value + "]";
	}
	
}
