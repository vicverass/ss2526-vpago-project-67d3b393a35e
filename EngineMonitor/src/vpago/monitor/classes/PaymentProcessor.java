package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="payment_processor")
public class PaymentProcessor {
	
	public PaymentProcessor() {
		super();
	}
	
	public PaymentProcessor(String name, String connectionMethod, String connectionIp, String connectionPort) {
		super();
		this.name = name;
		this.connectionMethod = connectionMethod;
		this.connectionIp = connectionIp;
		this.connectionPort = connectionPort;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del Procesador de tarjetas */
	private String name;
	/** Nombre del metodo de conexion para enviar transacciones **/
	private String connectionMethod;
	/** Ip de conexion **/
	private String connectionIp;
	/** Puerto de conexion **/
	private String connectionPort;

	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="payment_processor_id_seq")
	@SequenceGenerator(name="payment_processor_id_seq", sequenceName = "payment_processor_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the connectionMethod
	 */
	@Column(name="connection_method")
	public String getConnectionMethod() {
		return connectionMethod;
	}

	/**
	 * @param connectionMethod the connectionMethod to set
	 */
	public void setConnectionMethod(String connectionMethod) {
		this.connectionMethod = connectionMethod;
	}
	
	

	/**
	 * @return the connectionIp
	 */
	@Column(name="connection_ip")
	public String getConnectionIp() {
		return connectionIp;
	}

	/**
	 * @param connectionIp the connectionIp to set
	 */
	public void setConnectionIp(String connectionIp) {
		this.connectionIp = connectionIp;
	}

	/**
	 * @return the connectionPort
	 */
	@Column(name="connection_port")
	public String getConnectionPort() {
		return connectionPort;
	}

	/**
	 * @param connectionPort the connectionPort to set
	 */
	public void setConnectionPort(String connectionPort) {
		this.connectionPort = connectionPort;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentProcessor [id=" + id + ", name=" + name + ", connectionMethod=" + connectionMethod
				+ ", connectionIp=" + connectionIp + ", connectionPort=" + connectionPort + "]";
	}

	
	
	
}
