package vpago.monitor.classes.types;

public enum DeviceValidation {

	NONE, /*No requiere validacion, canal de pago no utiliza dispositivo*/
	SERIAL; /*Requiere validacion, canal de pago utiliza dispositivo*/
	
}
