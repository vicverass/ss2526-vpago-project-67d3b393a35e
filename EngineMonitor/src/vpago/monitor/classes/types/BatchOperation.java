package vpago.monitor.classes.types;

/**
 * Indica el tipo de operacion de lote/cierre que se le aplica a un tipo de transaccion
 */
public enum BatchOperation {
	NONE, //No se toma en cuenta para operaciones de manejo de lotes/cierres
	ADD, //Se toma en cuenta para sumar valores del lote/cierre
	SUBTRACT, //Se toma en cuenta para restar valores del lote/cierre
	SETTLEMENT //Se toma en cuenta para realizar cierres
}
