package vpago.monitor.classes.types;

/**
 * Tipo enumerado para representar los dias de la semana
 */
public enum WeekDay {
	LUNES(2), MARTES(3), MIERCOLES(4), JUEVES(5), VIERNES(6), SABADO(7), DOMINGO(1)
	;
	
	/** Entero que identifica a cada elemento del enumerado */
	private final int dayNumber;
	
	/**
	 * Constructor de un elemento del enumerado
	 * 
	 * @param dayNumber Entero para identificar un elemento
	 */
	WeekDay(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	
	/**
	 * Accede sl entero que identifica al elemento del enumerado
	 * 
	 * @return El entero que identifica al elemento
	 */
	public int getDayNumber() {
		return this.dayNumber;
	}
}
