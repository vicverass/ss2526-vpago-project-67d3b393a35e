package vpago.monitor.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar un Adquiriente
 */
@Entity
@Table(name="additional_voucher_fields")
public class AdditionalVoucherFields {
	
	public AdditionalVoucherFields() {
		super();
	}
	
	public AdditionalVoucherFields(String tableName, String fieldName, String message, boolean startReceipt,
			boolean endReceipt, int orderField, TransactionType transactionType, BatchType batchType, boolean active) {
		super();
		this.tableName = tableName;
		this.fieldName = fieldName;
		this.message = message;
		this.startReceipt = startReceipt;
		this.endReceipt = endReceipt;
		this.orderField = orderField;
		this.transactionType = transactionType;
		this.batchType = batchType;
		this.active = active;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre de la tabla */
	private String tableName;
	/** Campo de la tabla */
	private String fieldName;
	/** Mensaje adicional (Opcional) */
	private String message;
	/** Identificador de la posición del valor (Inicio del vouche) */
	private boolean startReceipt;
	/** Identificador de la posición del valor (Final del vouche) */
	private boolean endReceipt;
	/** Orden de impresión del valor en el vouche */
	private int orderField;	
	/** Tipo de Transaccion */
	private TransactionType transactionType;
	/** Tipo de Tarjeta */
	private BatchType batchType;
	/** Estado del elemento */
	private boolean active;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="additional_voucher_fields_seq")
	@SequenceGenerator(name="additional_voucher_fields_seq", sequenceName = "additional_voucher_fields_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="table_name")
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	@Column(name="field_name")
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Column(name="message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name="start_receipt")
	public boolean isStartReceipt() {
		return startReceipt;
	}

	public void setStartReceipt(boolean startReceipt) {
		this.startReceipt = startReceipt;
	}
	
	@Column(name="end_receipt")
	public boolean isEndReceipt() {
		return endReceipt;
	}

	public void setEndReceipt(boolean endReceipt) {
		this.endReceipt = endReceipt;
	}
	
	@Column(name="order_field")
	public int getOrderField() {
		return orderField;
	}

	public void setOrderField(int orderField) {
		this.orderField = orderField;
	}
	
	@Column(name="active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction_type_id")
	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "batch_type_id")
	public BatchType getBatchType() {
		return batchType;
	}

	public void setBatchType(BatchType batchType) {
		this.batchType = batchType;
	}

	@Override
	public String toString() {
		return "AdditionalVoucherFields [id=" + id + ", tableName=" + tableName + ", fieldName=" + fieldName
				+ ", message=" + message + ", startReceipt=" + startReceipt + ", endReceipt=" + endReceipt
				+ ", orderField=" + orderField + ", transactionType=" + transactionType + ", batchType=" + batchType
				+ ", active=" + active + "]";
	}

}