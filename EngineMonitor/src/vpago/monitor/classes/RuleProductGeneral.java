package vpago.monitor.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Clase persistente para representar una Regla General
 */
@Entity
@DiscriminatorValue("general")
public class RuleProductGeneral extends Rule {
	
	public RuleProductGeneral() {
		super();
	}
	
	/** Identificador del Producto General */
	private GeneralCardProduct generalProduct;

	/**
	 * @return the generalProduct
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "general_product_id")
	public GeneralCardProduct getGeneralProduct() {
		return generalProduct;
	}
	
	/**
	 * @param generalProduct the generalProduct to set
	 */
	public void setGeneralProduct(GeneralCardProduct generalProduct) {
		this.generalProduct = generalProduct;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProductGeneral [id=" + id  + ", acquirer=" + acquirer
				+ ", generalProduct=" + generalProduct + "]";
	}
}
