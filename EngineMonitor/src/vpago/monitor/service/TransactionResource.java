package vpago.monitor.service;

import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.glassfish.jersey.server.mvc.Viewable;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.monitor.classes.IsoTemplate;
import vpago.monitor.classes.PaymentChannel;
import vpago.monitor.classes.Transaction;
import vpago.monitor.classes.TransactionCircuit;
import vpago.monitor.classes.TransactionType;
import vpago.monitor.classes.wrappers.IsoTemplateList;
import vpago.monitor.classes.wrappers.PaymentChannelList;
import vpago.monitor.classes.wrappers.TemplateMessageList;
import vpago.monitor.classes.wrappers.TransactionList;
import vpago.monitor.classes.wrappers.TransactionTypeList;
import vpago.monitor.db.HibernateUtil;


@Path("/monitor")
public class TransactionResource {
	private static final Logger logger = LoggerFactory.getLogger(TransactionResource.class);
	
	@Path("/home")
	@GET
	@Produces("text/html")
	public Viewable getHome(@Context HttpServletRequest request){
		Session session = null;
		try {
			return new Viewable("/WEB-INF/jsp/Home");
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}

	@Path("/transaction")
	@GET
	@Produces("text/html")
	public Viewable getTransactionList(@Context HttpServletRequest request){
		Session session = null;
		try {
			TransactionList transactionList = null;
			
			session = HibernateUtil.getSessionFactory().openSession();
			//String hsqlquery = "select new vpago.monitor.classes.minified.TransactionMin(id, hashCode, terminal, terminalBankId, merchantIdCode, amount, txnDate, controlToken, transactionStatus, transactionType, transactionResponse, cardProduct, retried, retryAttempts, secToken) from Transaction as transaction";
			String hsqlquery = "select new vpago.monitor.classes.minified.TransactionMin(transaction.id, transaction.hashCode, "
					+ "transaction.terminal, transaction.amount , transaction.txnDate, transaction.controlToken, transaction.transactionStatus, "
					+ "transaction.transactionType, transaction.transactionResponse, transaction.retried, transaction.retryAttempts, "
					+ "transaction.secToken) "
					+ "from Transaction as transaction order by transaction.id desc";		
			if(session != null) transactionList = new TransactionList(session.createQuery(hsqlquery).list());
			else logger.debug("Session de Hibernate es null");
			transactionList.getTransaction().forEach((trxMin)->{
				//logger.debug("trxMin {} {}", trxMin.getId(), trxMin);
			});
			return new Viewable("/WEB-INF/jsp/TransactionList", transactionList);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/transaction/{transaction_id: \\d+ }")
	@GET
	@Produces("text/html")
	public Viewable getTransaction(@Context HttpServletRequest request, @PathParam("transaction_id") int transactionId){
		Session session = null;
		try {
			Transaction transaction = null;
			session = HibernateUtil.getSessionFactory().openSession();
			if(session != null) transaction = (Transaction) session.get(Transaction.class, transactionId);
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/TransactionDetail", transaction);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/paymentChannel")
	@GET
	@Produces("text/html")
	public Viewable getPaymentChannelList(@Context HttpServletRequest request){
		Session session = null;
		try {
			PaymentChannelList paymentChannelList = null;			
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from PaymentChannel as paymentChannel order by paymentChannel.id asc";		
			if(session != null) paymentChannelList = new PaymentChannelList(session.createQuery(hsqlquery).list());
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/PaymentChannelList", paymentChannelList);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/tempMsg")
	@GET
	@Produces("text/html")
	public Viewable getTemplateMessageList(@Context HttpServletRequest request){
		Session session = null;
		try {
			TemplateMessageList tempMsgList = null;			
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from TemplateMessage as tempMsg order by tempMsg.id asc";		
			if(session != null) tempMsgList = new TemplateMessageList(session.createQuery(hsqlquery).list());
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/TemplateMessageList", tempMsgList);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/isoTemp")
	@GET
	@Produces("text/html")
	public Viewable getIsoTemplateList(@Context HttpServletRequest request){
		Session session = null;
		try {
			IsoTemplateList isoTempList = null;			
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from IsoTemplate as isoTemp order by isoTemp.id asc";		
			if(session != null) isoTempList = new IsoTemplateList(session.createQuery(hsqlquery).list());
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/IsoTemplateList", isoTempList);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/isoTemp/{iso_template_id: \\d+ }")
	@GET
	@Produces("text/html")
	public Viewable getIsoTemplateParams(@Context HttpServletRequest request, @PathParam("iso_template_id") int isoTempId){
		Session session = null;
		try {
			IsoTemplate isoTemp = null;			
			session = HibernateUtil.getSessionFactory().openSession();	
			if(session != null) isoTemp = (IsoTemplate) session.get(IsoTemplate.class, isoTempId);
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/IsoTemplateParam", isoTemp);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/trxType")
	@GET
	@Produces("text/html")
	public Viewable getTransactionTypeList(@Context HttpServletRequest request){
		Session session = null;
		try {
			TransactionTypeList trxTypeList = null;			
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from TransactionType as trxType order by trxType.id asc";		
			if(session != null) trxTypeList = new TransactionTypeList(session.createQuery(hsqlquery).list());
			else logger.debug("Session de Hibernate es null");
			return new Viewable("/WEB-INF/jsp/TransactionTypeList", trxTypeList);
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}
	
	@Path("/script/{payment_channel_id: \\d+ }")
	@GET
	@Produces("text/html")
	public Viewable getTransactionCircuit(@Context HttpServletRequest request, @PathParam("payment_channel_id") int paymentChannelId){
		Session session = null;
		try {
			PaymentChannel paymentChannel = null;
			List<TransactionType> transactionTypes = null;
			session = HibernateUtil.getSessionFactory().openSession();	
			if(session != null) {
				paymentChannel = (PaymentChannel) session.get(PaymentChannel.class, paymentChannelId);
				String hsqlquery = "from TransactionType as transactionType where transactionType.templateMessage=:templateMessage";
				transactionTypes = session.createQuery(hsqlquery).setEntity("templateMessage", paymentChannel.getTemplateMessage().get(0)).list();
			}
			else logger.debug("Session de Hibernate es null");
//			paymentChannel.getTransactionRespTypes().forEach((key,item)->{
//				logger.debug("{}", item);
//			});
			transactionTypes.sort((tt1, tt2)->{
//			    	if((tt1.getTransactionResponseType() != null ? tt1.getTransactionResponseType().equals(tt2.getTransactionResponseType()) : tt1.getTransactionResponseType() == tt2.getTransactionResponseType()) &&
//			    	   (tt1.getTransactionInverseType() != null ? tt1.getTransactionInverseType().equals(tt2.getTransactionInverseType()) : tt1.getTransactionInverseType() == tt2.getTransactionInverseType()) &&
//			    	   (tt1.getTransactionConfirmationType() != null ? tt1.getTransactionConfirmationType().equals(tt2.getTransactionConfirmationType()) : tt1.getTransactionConfirmationType() == tt2.getTransactionConfirmationType()) &&
//			    	   (tt1.getTransactionBatchUploadType() != null ? tt1.getTransactionBatchUploadType().equals(tt2.getTransactionBatchUploadType()) : tt1.getTransactionBatchUploadType() == tt2.getTransactionBatchUploadType()) &&
//			    	   (tt1.getTransactionTrailerSettleType() != null ? tt1.getTransactionTrailerSettleType().equals(tt2.getTransactionTrailerSettleType()) : tt1.getTransactionTrailerSettleType() == tt2.getTransactionTrailerSettleType())	)
//			    	{
//			    		System.out.println(tt1.getName() + " == " + tt2.getName());
//			    		return 0;
//			    	}
//			    	
//			    	if((tt1.getTransactionResponseType() != null ? tt1.getTransactionResponseType().equals(tt2) : false) ||
//			    	   (tt1.getTransactionInverseType() != null ? tt1.getTransactionInverseType().equals(tt2) : false) ||
//			    	   (tt1.getTransactionConfirmationType() != null ? tt1.getTransactionConfirmationType().equals(tt2) : false) ||
//			    	   (tt1.getTransactionBatchUploadType() != null ? tt1.getTransactionBatchUploadType().equals(tt2) : false) ||
//			           (tt1.getTransactionTrailerSettleType() != null ? tt1.getTransactionTrailerSettleType().equals(tt2) : false) ) 
//			        {
//			    		System.out.println(tt1.getName() + " < " + tt2.getName());
//			    		return -1;
//			        }
//			    	System.out.println(tt1.getName() + " > " + tt2.getName());
//			    	return 1;
				System.out.println("Comparing " + tt1.getId() + " and " + tt2.getId());
				//Si son iguales todos los tipos dependientes
		    	if((tt1.getTransactionResponseType() == tt2.getTransactionResponseType()) &&
		    	   (tt1.getTransactionInverseType() == tt2.getTransactionInverseType()) &&
		    	   (tt1.getTransactionConfirmationType() == tt2.getTransactionConfirmationType()) &&
		    	   (tt1.getTransactionBatchUploadType() == tt2.getTransactionBatchUploadType()) &&
		    	   (tt1.getTransactionTrailerSettleType() == tt2.getTransactionTrailerSettleType())	)
		    	{
		    		System.out.println(tt1.getName() + " == " + tt2.getName());
		    		return 0;
		    	}
		    	
		    	//Si son iguales todos los tipos dependientes
		    	if((tt1.getTransactionInverseType() == tt2.getTransactionInverseType()) &&
		    	   (tt1.getTransactionConfirmationType() == tt2.getTransactionConfirmationType()) &&
		    	   (tt1.getTransactionBatchUploadType() == tt2.getTransactionBatchUploadType()) &&
		    	   (tt1.getTransactionTrailerSettleType() == tt2.getTransactionTrailerSettleType())	)
		    	{
		    		System.out.println(tt1.getName() + " == " + tt2.getName());
		    		return 0;
		    	}
		    	
		    	//Si es respuesta de alguna transaccion debe ir primero, por lo que es menor
		    	if((tt1.getTransactionResponseType() == null) && (tt2.getTransactionResponseType() != null))
		        {
		    		System.out.println(tt1.getName() + " < " + tt2.getName());
		    		return -1;
		        }
		    	
		    	//Si es respuesta de alguna transaccion debe ir primero, por lo que es menor
		    	if((tt2.getTransactionResponseType() == null) && (tt1.getTransactionResponseType() != null))
		        {
		    		System.out.println(tt2.getName() + " < " + tt1.getName());
		    		return 1;
		        }
		    	
		    	//Si tt1 depende de tt2 entonces tt1 va despues, por lo que es mayor
		    	if((tt1.getTransactionResponseType() != null ? tt1.getTransactionResponseType().getId() == tt2.getId() : false) ||
		    	   (tt1.getTransactionInverseType() != null ? tt1.getTransactionInverseType().getId() == tt2.getId() : false) ||
		    	   (tt1.getTransactionConfirmationType() != null ? tt1.getTransactionConfirmationType().getId() == tt2.getId() : false) ||
		    	   (tt1.getTransactionBatchUploadType() != null ? tt1.getTransactionBatchUploadType().getId() == tt2.getId() : false) ||
		           (tt1.getTransactionTrailerSettleType() != null ? tt1.getTransactionTrailerSettleType().getId() == tt2.getId() : false)) 
		    	{
		    		System.out.println(tt1.getName() + " > " + tt2.getName());
			    	return 1;
		    	}
		    	
		    	//Si tt2 depende de tt1 entonces tt2 va despues, por lo que es mayor
		    	if((tt2.getTransactionResponseType() != null ? tt2.getTransactionResponseType().getId() == tt1.getId() : false) ||
		    	   (tt2.getTransactionInverseType() != null ? tt2.getTransactionInverseType().getId() == tt1.getId() : false) ||
		    	   (tt2.getTransactionConfirmationType() != null ? tt2.getTransactionConfirmationType().getId() == tt1.getId() : false) ||
		    	   (tt2.getTransactionBatchUploadType() != null ? tt2.getTransactionBatchUploadType().getId() == tt1.getId() : false) ||
		           (tt2.getTransactionTrailerSettleType() != null ? tt2.getTransactionTrailerSettleType().getId() == tt1.getId() : false)) 
		    	{
		    		System.out.println(tt2.getName() + " > " + tt1.getName());
			    	return -1;
		    	}

		    	System.out.println(tt1.getName() + " ? " + tt2.getName() + " *");
		    	return 0;

			    }

			);
			transactionTypes.forEach(item->{
				logger.debug("{}", item);
			});
			paymentChannel.getTransactionRespTypes().forEach((key, item)->{
				logger.trace("{}", item);
			});
			return new Viewable("/WEB-INF/jsp/PaymentCircuit", new TransactionCircuit(
					paymentChannel, 
					paymentChannel.getTemplateMessage().get(0), 
					transactionTypes, 
					paymentChannel.getTransactionRespTypes(),
					paymentChannel.getPaymentProcessor()));
//			return new Viewable("/WEB-INF/jsp/PaymentCircuitNoId", new TransactionCircuit(
//					paymentChannel, 
//					paymentChannel.getTemplateMessage().get(0), 
//					transactionTypes, 
//					paymentChannel.getTransactionRespTypes(),
//					paymentChannel.getPaymentProcessor()));
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Viewable("/WEB-INF/jsp/Error");
		} finally {
			if(session != null) session.close();
		}
	}

}
