package vpago.monitor.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.monitor.classes.Acquirer;
import vpago.monitor.classes.AdditionalVoucherFields;
import vpago.monitor.classes.Affiliation;
import vpago.monitor.classes.Bank;
import vpago.monitor.classes.BankCardProduct;
import vpago.monitor.classes.BatchType;
import vpago.monitor.classes.Bin;
import vpago.monitor.classes.CardFranchise;
import vpago.monitor.classes.GeneralCardProduct;
import vpago.monitor.classes.CardType;
import vpago.monitor.classes.IsoTemplate;
import vpago.monitor.classes.Merchant;
import vpago.monitor.classes.PaymentChannel;
import vpago.monitor.classes.PaymentProcessor;
import vpago.monitor.classes.Rule;
import vpago.monitor.classes.RuleProduct;
import vpago.monitor.classes.RuleProductGeneral;
import vpago.monitor.classes.RuleProportion;
import vpago.monitor.classes.RuleSet;
import vpago.monitor.classes.TemplateField;
import vpago.monitor.classes.TemplateMessage;
import vpago.monitor.classes.TemplateSubField;
import vpago.monitor.classes.Terminal;
import vpago.monitor.classes.TerminalParameter;
import vpago.monitor.classes.TimeFrame;
import vpago.monitor.classes.Transaction;
import vpago.monitor.classes.TransactionParameter;
import vpago.monitor.classes.TransactionResponse;
import vpago.monitor.classes.TransactionResponseParameter;
import vpago.monitor.classes.TransactionType;

/**
 * Clase para el manejo de la configuracion y funcionamiento del framework Hibernate
 */
public class HibernateUtil
{
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    static
    {
        try
        {
//          Configuration configuration = new Configuration();
//            Configuration configuration = new Configuration().configure();
            Configuration cfg = new Configuration()
       				//HSQLDB
            		.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect")
    				.setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver")
    				.setProperty("hibernate.connection.username", "SA")
    				.setProperty("hibernate.connection.password", "")
    				.setProperty("hibernate.connection.url", "jdbc:hsqldb:hsql://localhost/engine_db")
    				.setProperty("hibernate.show_sql", "false")
    				.setProperty("hibernate.format_sql.password", "true")
    				
    				.setProperty("hibernate.c3p0.min_size", "1")
    				.setProperty("hibernate.c3p0.max_size", "100")
    				.setProperty("hibernate.c3p0.timeout", "0")
    				.setProperty("hibernate.c3p0.max_statements", "50")
    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
    				
	    			//ORACLE PRODUCCION
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINECONSULT")
//    				.setProperty("hibernate.connection.password", "agNG2011__")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.98:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "20")
//    				.setProperty("hibernate.c3p0.timeout", "3000")
//    				.setProperty("hibernate.c3p0.max_statements","100")
//    				.setProperty("hibernate.c3p0.idle_test_period", "20")
    				
    				//ORACLE QA
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINETRANSACTION")
//    				.setProperty("hibernate.connection.password", "enginetransactionss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@10.20.184.172:1521:PROD")
////    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "100")
//    				//.setProperty("hibernate.c3p0.timeout", "0")
//    				.setProperty("hibernate.c3p0.max_statements", "50")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
    				
    				//ORACLE DESARROLLO
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINEDEVELOPMENT")
//    				.setProperty("hibernate.connection.password", "enginedevelopmentss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "100")
//    				.setProperty("hibernate.c3p0.timeout", "0")
//    				.setProperty("hibernate.c3p0.max_statements", "50")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
            		
    				//POSTGRES
//					.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect")
//    				.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver")
////    				.setProperty("hibernate.connection.username", "postgres")
////    				.setProperty("hibernate.connection.password", "vpagoss2526qa")
////    				.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost/engine_db")
//    				.setProperty("hibernate.connection.username", "vfinancialu")
//    				.setProperty("hibernate.connection.password", "vfinancialu")
//    				.setProperty("hibernate.connection.url", "jdbc:postgresql://192.168.1.120/vfinancialenginedb")
//    				.setProperty("hibernate.show_sql", "true")
//    				.setProperty("hibernate.format_sql.password", "true")
    				
   				
    				.addAnnotatedClass(Transaction.class)
    				.addAnnotatedClass(Terminal.class)
    				.addAnnotatedClass(Merchant.class)
    				.addAnnotatedClass(Rule.class)
    				.addAnnotatedClass(RuleSet.class)
    				.addAnnotatedClass(Acquirer.class)
    				.addAnnotatedClass(TimeFrame.class)
    				.addAnnotatedClass(CardFranchise.class)
    				.addAnnotatedClass(CardType.class)
    				.addAnnotatedClass(GeneralCardProduct.class)
    				.addAnnotatedClass(BankCardProduct.class)
    				.addAnnotatedClass(Bank.class)
    				.addAnnotatedClass(Bin.class)
    				.addAnnotatedClass(PaymentChannel.class)
    				.addAnnotatedClass(TransactionType.class)
    				.addAnnotatedClass(IsoTemplate.class)
    				.addAnnotatedClass(TemplateField.class)
    				.addAnnotatedClass(TemplateSubField.class)
    				.addAnnotatedClass(TransactionResponse.class)
    				.addAnnotatedClass(TransactionParameter.class)
    				.addAnnotatedClass(TransactionResponseParameter.class)
    				.addAnnotatedClass(RuleProduct.class)
    				.addAnnotatedClass(RuleProductGeneral.class)
    				.addAnnotatedClass(RuleProportion.class)
    				.addAnnotatedClass(TerminalParameter.class)
    				.addAnnotatedClass(Affiliation.class)
    				.addAnnotatedClass(TemplateMessage.class)
    				.addAnnotatedClass(BatchType.class)
    				.addAnnotatedClass(PaymentProcessor.class)
    				.addAnnotatedClass(AdditionalVoucherFields.class)
    				;

            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
    				cfg.getProperties()).build();
            sessionFactory = cfg.buildSessionFactory(serviceRegistry);
        } catch (Exception he) {
            logger.error("Error creando sesion de Hibernate: {}", he.getMessage());
            throw new ExceptionInInitializerError(he);
        }
    }

    /**
     * Retorna una entidad necesaria para construir una sesion de Hibernate
     * 
     * @return La entidad SessionFactory
     */
    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    } 
}