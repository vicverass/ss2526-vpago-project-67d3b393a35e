<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/error.css">

	<title>Error</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      
      <div class="row">
       <div class="col-md-10 col-md-offset-1 content-back"> 
          
        <div class="row">
          <hr>
            <div class="col-md-12">
            
            <div class="page-header">
                <h1>
                  Lo sentimos...
                </h1>
            </div>
            </div>
           
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-danger" role="alert"><p class="error-message"><i class="fa fa-exclamation-triangle fa-lg"></i> 
              <c:choose>
                <c:when test="${ model == null }">
                    Pero ha ocurrido un error inesperado, estamos trabajando para solucionarlo.
                </c:when>
                <c:otherwise>
                    ${ model.toString() }
                </c:otherwise>
            </c:choose> 
            </p> 
           </div>
          </div>
        </div>

        </div>
      </div>


		</div>


	</body>
</html>