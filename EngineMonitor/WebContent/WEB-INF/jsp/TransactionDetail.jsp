<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Transacciones</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Parametros de transaccion ${ model.getId() }
                			</h1>
            			</div>
        			</div>
					<div class="row">	
					<table class="table table-hover">
						<thead>
							<th class="info">Id</th>
							<th class="info">Hash</th>
							<th class="info">Terminal</th>
							<th class="info">Fecha</th>
							<th class="info">Token de control</th>
							<th class="info">Estado</th>
							<th class="info">Tipo de transaccion</th>
							<th class="info">Reintentada</th>
							<th class="info">Intentos</th>
							<th class="info">Token de seguridad</th>
							<th class="info">Respuesta</th>
						</thead>
						<tbody>
							<tr>
								<td><c:out value="${model.getId()}"/></td>
								<td><c:out value="${model.getHashCode()}"/></td>
								<td><a href="/EngineMonitor/monitor/transaction/${transaction.getId()}"><c:out value="${transaction.getTerminalGuid()}"/></a></td>
								<td><fmt:formatDate value="${model.getTxnDate().getTime()}"/></td>
								<td><c:out value="${model.getControlToken()}"/></td>
								<c:choose>
									<c:when test="${model.getTransactionStatus() == 'CLOSED'}"><td><span class="label label-primary">CERRADA</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'COMPLETED'}"><td><span class="label label-success">COMPLETADA</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'PENDING'}"><td><span class="label label-default">PENDIENTE</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'REVERSED'}"><td><span class="label label-warning">REVERSADA</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'TIMED_OUT'}"><td><span class="label label-danger">TIME OUT</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'CANCELED'}"><td><span class="label label-danger">CANCELADA</span></td></c:when>
									<c:when test="${model.getTransactionStatus() == 'ANULLED'}"><td><span class="label label-warning">ANULADA</span></td></c:when>
								</c:choose>
								<td><c:out value="${model.getTransactionType().getDescription()}"/></td>
								<c:choose>
									<c:when test="${model.isRetried() == true}"><td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></c:when>
									<c:when test="${model.isRetried() == false}"><td><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td></c:when>
								</c:choose>
								<td><c:out value="${model.getRetryAttempts()}"/></td>
								<td><c:out value="${model.getSecToken()}"/></td>
								<td><c:out value="${model.getTransactionResponse().getCode()}  ${model.getTransactionResponse().getDescription()}"/></td>
							
							</tr>
						</tbody>
					</table>
					</div>
        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Nombre</th>
								<th class="info">Valor</th>
								<th class="info">Tipo</th>
							</thead>
							<tbody>
								<c:forEach var="rparam" items="${ model.getParameters() }" >
									<tr>
										<td><c:out value="${ rparam.getParamName() }"/></td>
										<td><c:out value="${ rparam.getParamValue() }"/></td>
										<td><c:out value="${ rparam.getParamType() }"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>