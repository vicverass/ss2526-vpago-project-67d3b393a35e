<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Plantillas de mensajes</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Plantillas de mensajes
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Id</th>
								<th class="info">Canal de pago</th>
								<th class="info">Nombre del parametro de terminal</th>
							</thead>
							<tbody>
								<c:forEach var="tempMsg" items="${model.getTemplateMessage()}" >
									<tr>
										<td><c:out value="${tempMsg.getId()}"/></td>
										<td><c:out value="${tempMsg.getPaymentChannel().getName()}"/></td>								
										<td><c:out value="${tempMsg.getTerminalParamName()}"/></td>							
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>