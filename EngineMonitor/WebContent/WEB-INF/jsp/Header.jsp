<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<!-- jQuery CDN (necessary for Bootstrap's JavaScript plugins) -->
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		
		<!-- jQuery UI -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/jquery-ui/jquery-ui.min.css">
		
    	<!-- Bootstrap -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

		<!-- Custom CSS -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">

		<!-- Fontawesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    	
    	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
      	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    	<![endif]-->