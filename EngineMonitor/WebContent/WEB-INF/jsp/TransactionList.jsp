<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Transacciones</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Transacciones
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Id</th>
								<th class="info">Hash</th>
								<th class="info">Terminal</th>
								<th class="info">Fecha</th>
								<th class="info">Token de control</th>
								<th class="info">Monto</th>
								<th class="info">Estado</th>
								<th class="info">Tipo de transaccion</th>
								<th class="info">Reintentada</th>
								<th class="info">Intentos</th>
								<th class="info">Token de seguridad</th>
								<th class="info">Respuesta</th>
							</thead>
							<tbody>
								<c:forEach var="transaction" items="${model.getTransaction()}" >
									<tr>
										<td><a href="/EngineMonitor/monitor/transaction/${transaction.getId()}"><c:out value="${transaction.getId()}"/></a></td>
										<td><c:out value="${transaction.getHashCode()}"/></td>
										<td><c:out value="${transaction.getTerminalGuid()}"/></td>
										<td><c:out value="${transaction.getDate()}"/></td>
										<td><c:out value="${transaction.getControlToken()}"/></td>
										<td><c:out value="${transaction.getAmount()}"/></td>
										<c:choose>
											<c:when test="${transaction.getTransactionStatus() == 'CLOSED'}"><td><span class="label label-primary">CERRADA</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'COMPLETED'}"><td><span class="label label-success">COMPLETADA</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'PENDING'}"><td><span class="label label-default">PENDIENTE</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'REVERSED'}"><td><span class="label label-warning">REVERSADA</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'TIMED_OUT'}"><td><span class="label label-danger">TIME OUT</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'CANCELED'}"><td><span class="label label-danger">CANCELADA</span></td></c:when>
											<c:when test="${transaction.getTransactionStatus() == 'ANULLED'}"><td><span class="label label-warning">ANULADA</span></td></c:when>
										</c:choose>
										<td><c:out value="${transaction.getTransactionTypeDescription()}"/></td>
										<c:choose>
											<c:when test="${transaction.isRetried() == true}"><td><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></c:when>
											<c:when test="${transaction.isRetried() == false}"><td><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></td></c:when>
										</c:choose>
										<td><c:out value="${transaction.getRetryAttempts()}"/></td>
										<td><c:out value="${transaction.getSecToken()}"/></td>
										<td><c:out value="${transaction.getTransactionResponseDesc()}"/></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>