<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Plantillas ISO</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Plantillas ISO
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Id</th>
								<th class="info">Tipo de transaccion </th>
								<th class="info">Header</th>
								<th class="info">Nombre de sub paquete</th>
								<th class="info">Nombre de la clase</th>
								<th class="info">Funcion de pre-procesamiento</th>
								<th class="info">Funcion de validacion de reverso</th>
								<th class="info">Funcion de desbloqueo por confirmacion</th>
							</thead>
							<tbody>
								<c:forEach var="isoTemplate" items="${model.getIsoTemplate()}" >
									<tr>
										<td><a href="/EngineMonitor/monitor/isoTemp/${isoTemplate.getId()}"><c:out value="${isoTemplate.getId()}"/></a></td>
										<td><c:out value="0${Integer.toHexString(isoTemplate.getTxnType())}"/></td>	
										<td><c:out value="${isoTemplate.getHeader()}"/></td>
										<td><c:out value="${isoTemplate.getSubpackageName()}"/></td>
										<td><c:out value="${isoTemplate.getClassName()}"/></td>
										<td><c:out value="${isoTemplate.getPreProcFunction()}"/></td>	
										<td><c:out value="${isoTemplate.getValRevFunction()}"/></td>
										<td><c:out value="${isoTemplate.getTermUnblockConf()}"/></td>								
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>