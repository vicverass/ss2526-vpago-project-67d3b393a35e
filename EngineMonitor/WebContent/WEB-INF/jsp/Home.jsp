<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Inicio</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Enlaces
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Enlace</th>
							</thead>
							<tbody>						
								<tr><td><a href="/EngineMonitor/monitor/transaction">Lista de Transacciones</a></td></tr>
								<tr><td><a href="/EngineMonitor/monitor/paymentChannel">Lista de Canales de pago</a></td></tr>
								<tr><td><a href="/EngineMonitor/monitor/tempMsg">Lista de Plantillas de mensajes</a></td></tr>
								<tr><td><a href="/EngineMonitor/monitor/isoTemp">Lista de Plantillas ISO</a></td></tr>
								<tr><td><a href="/EngineMonitor/monitor/trxType">Lista de Tipos de transacciones</a></td></tr>							
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>