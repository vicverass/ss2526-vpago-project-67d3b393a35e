<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Canales de pago</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Canales de pago
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Id</th>
								<th class="info">Nombre </th>
								<th class="info">Descripci�n</th>
								<th class="info">C�digo</th>
							</thead>
							<tbody>
								<c:forEach var="paymentChannel" items="${model.getPaymentChannel()}" >
									<tr>
										<td><c:out value="${paymentChannel.getId()}"/></td>
										<td><c:out value="${paymentChannel.getName()}"/></td>								
										<td><c:out value="${paymentChannel.getDescription()}"/></td>
										<td><c:out value="${paymentChannel.getCodeName()}"/></td>								
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>