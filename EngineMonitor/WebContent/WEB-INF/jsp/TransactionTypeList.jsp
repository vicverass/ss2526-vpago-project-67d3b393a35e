<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Tipos de transacciones</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Tipos de transacciones
                			</h1>
            			</div>
        			</div>

        			<div class="row">	
						<table class="table table-hover">
							<thead>
								<th class="info">Id</th>
								<th class="info">Nombre</th>
								<th class="info">Descripcion</th>
								<th class="info">Plantilla ISO</th>
								<th class="info">Nombre de la primitiva</th>
								<th class="info">Timeout de espera</th>
								<th class="info">�Admite reintentos?</th>
								<th class="info">Numero de reintentos</th>
								<th class="info">Delay entre reintentos</th>
								<th class="info">Tipo de transaccion de respuesta</th>
								<th class="info">Plantilla de mensaje</th>
								<th class="info">Tipo de transaccion de confirmacion</th>
								<th class="info">Tipo de transaccion inversa</th>
								<th class="info">�Es de tipo confirmacion?</th>
								<th class="info">�Es de tipo inversa?</th>
								<th class="info">�Se bloquea en confirmacion?</th>
								<th class="info">�Se bloquea en inversa?</th>
								<th class="info">�Es de tipo inversible?</th>
								<th class="info">�Es de tipo confirmable?</th>
							</thead>
							<tbody>
								<c:forEach var="transactionType" items="${model.getTransactionType()}" >
									<tr>
										<td><c:out value="${transactionType.getId()}"/></td>
										<td><c:out value="${transactionType.getName()}"/></td>
										<td><c:out value="${transactionType.getDescription()}"/></td>
										<td><c:out value="${transactionType.getIsoTemplate().getId()}"/></td>
										<td><c:out value="${transactionType.getMethodName()}"/></td>
										<td><c:out value="${transactionType.getTimeout()}"/></td>
										<td><c:out value="${transactionType.isRetry()}"/></td>
										<td><c:out value="${transactionType.getRetryAttempts()}"/></td>
										<td><c:out value="${transactionType.getRetryDelay()}"/></td>
										<td><c:out value="${transactionType.getTransactionResponseType().getName()}"/></td>
										<td><c:out value="${transactionType.getTemplateMessage().getId()}"/></td>
										<td><c:out value="${transactionType.getTransactionConfirmationType().getName()}"/></td>
										<td><c:out value="${transactionType.getTransactionInverseType().getName()}"/></td>
										<td><c:out value="${transactionType.isConfirmationTrxType()}"/></td>
										<td><c:out value="${transactionType.isInverseTrxType()}"/></td>
										<td><c:out value="${transactionType.isBlocksOnConf()}"/></td>
										<td><c:out value="${transactionType.isBlocksOnInv()}"/></td>
										<td><c:out value="${transactionType.isReversableType()}"/></td>
										<td><c:out value="${transactionType.isConfirmableType()}"/></td>							
									</tr>
								</c:forEach>
							</tbody>
						</table>
	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>