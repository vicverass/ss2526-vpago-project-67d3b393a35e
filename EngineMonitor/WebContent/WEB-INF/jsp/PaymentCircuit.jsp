<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<jsp:include page="Header.jsp"></jsp:include>

	<title>Script de circuito</title>
	</head>
	
	<body>		
		<div class="container-fluid">
      		<div class="row">
       			<div class="col-md-10 col-md-offset-1 content-back"> 

        			<div class="row">
        				<hr>
        	 			<div class="page-header">
                			<h1>
                				Script de circuito
                			</h1>
            			</div>
        			</div>
					<div class="row">
						
						<p>
							<h1>Llenar manualmente los IsoTemplate de los tipos de lotes</h1>
						</p>
						
						<p>
						//Procesador<br>
						Integer paymentProcessorId = ${model.getPaymentProcessor().getId()};<br>
						System.out.println("Id de procesador: " + paymentProcessorId);<br>
						Session session = HibernateUtil.getSessionFactory().openSession();<br>
						PaymentProcessor paymentProcessor = (PaymentProcessor)session.get(PaymentProcessor.class, paymentProcessorId);<br>
						if(paymentProcessor == null) {<br>
							paymentProcessor = new PaymentProcessor(<c:out value='"${model.getPaymentProcessor().getName()}"'/>, <c:out value='"${model.getPaymentProcessor().getConnectionMethod()}"'/>, <c:out value='"${model.getPaymentProcessor().getConnectionIp()}"'/>, <c:out value='"${model.getPaymentProcessor().getConnectionPort()}"'/>);<br>
							session.beginTransaction();<br>
							paymentProcessorId = (Integer) session.save(paymentProcessor);<br>
							session.getTransaction().commit();<br>
							System.out.println("Id de nuevo procesador: " + paymentProcessorId);<br>
						}
						session.close();<br>
						</p>
						<p>
						//Canal de pago<br>
						PaymentChannel paymentChannel = new PaymentChannel(
						<c:out value='"${model.getPaymentChannel().getName()}"'/>, 
						<c:out value='"${model.getPaymentChannel().getDescription()}"'/>, 
						<c:out value='"${model.getPaymentChannel().getCodeName()}"'/>, 
						paymentProcessor,
						DeviceValidation.${model.getPaymentChannel().getDeviceValidation()});<br>
						session = HibernateUtil.getSessionFactory().openSession();<br>
						session.beginTransaction();<br>
						Integer paymentChannelId = (Integer) session.save(paymentChannel);<br>
						session.getTransaction().commit();<br>
						session.close();<br>
						System.out.println("Id de nuevo canal de pago: " + paymentChannelId);<br>
						</p>
						
						<p>
						//Guardar Tipos de lotes<br>
						session = HibernateUtil.getSessionFactory().openSession();
						List&#60;BatchType&#62; batchTypes = new ArrayList&#60;BatchType&#62;();<br>
						HashMap&#60;String, BatchType&#62; batchTypesMap = new HashMap&#60;String, BatchType&#62;();<br>
						<c:forEach var="batchType" items="${model.getPaymentChannel().getBatchTypes()}" >
						paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);<br>
						batchTypes.add( new BatchType(
					    paymentChannel,
						null,
						<c:choose>
						<c:when test="${batchType.getParamName() != null}">"${batchType.getParamName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						<c:choose>
						<c:when test="${batchType.getParamValue() != null}">"${batchType.getParamValue()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						<c:choose>
						<c:when test="${batchType.getTypeName() != null}">"${batchType.getTypeName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						<c:choose>
						<c:when test="${batchType.getTypeDesc() != null}">"${batchType.getTypeDesc()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>
						));<br>
						</c:forEach>
						for(BatchType batchTypeE : batchTypes){<br>
							Session sessionbt = HibernateUtil.getSessionFactory().openSession();<br>
							sessionbt.beginTransaction();<br>
							sessionbt.save(batchTypeE);<br>
							sessionbt.getTransaction().commit();<br>
							sessionbt.close();<br>
							batchTypesMap.put(batchTypeE.getTypeName(), batchTypeE);<br>
						};<br>
						session.close();<br>
						</p>
						
						<p>
						//Plantilla de mensaje<br>
						session = HibernateUtil.getSessionFactory().openSession();<br>
						paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);<br>
						TemplateMessage templateMessage = new TemplateMessage(paymentChannel,<c:out value='"${model.getTemplateMessage().getTerminalParamName()}"'/>);<br>
						session.beginTransaction();<br>
						Integer templateMessageId = (Integer) session.save(templateMessage);<br>
						paymentChannel.getTemplateMessage().add(templateMessage);<br>
						session.update(paymentChannel);<br>
						session.update(templateMessage);<br>
						session.getTransaction().commit();<br>
						session.close();<br>
						System.out.println("Id de nueva plantilla de mensaje: " + templateMessageId);<br>
						</p>
						
						<p>
						//Guardar Tipos de transaccion con parametros<br>
						Integer transactionTypeId = 0;<br>
						Integer isoTemplateId = 0;<br>
						TransactionType transactionType = null;<br>
						IsoTemplate isoTemplate = null;<br>
						TransactionType transactionResponseType = null;<br>
						TransactionType transactionInverseType = null;<br>
						TransactionType transactionConfirmationType = null;<br>
						TransactionType transactionBatchUploadType = null;<br>
						TransactionType transactionTrailerSettleType = null;<br>
						AdditionalVoucherFields addVoucherF = null;<br>
						TransactionType voucherTransactionType = null;<br>
						BatchType voucherBatchType = null;<br>
						HashMap&#60;String, TransactionType&#62; transactionTypeMap = new HashMap&#60;String, TransactionType&#62;();<br>
						HashMap&#60;String, IsoTemplate&#62; isoTemplateMap = new HashMap&#60;String, IsoTemplate&#62;();<br>
						<br>
						<c:forEach var="transactionType" items="${model.getTransactionTypes()}" >
						//####################################################################################################<br>
						isoTemplate = new IsoTemplate(
						0x${Integer.toHexString(transactionType.getIsoTemplate().getTxnType())},
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getHeader() != null}">"${transactionType.getIsoTemplate().getHeader()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getSubpackageName() != null}">"${transactionType.getIsoTemplate().getSubpackageName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getPreProcFunction() != null}">"${transactionType.getIsoTemplate().getPreProcFunction()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getClassName() != null}">"${transactionType.getIsoTemplate().getClassName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getValRevFunction() != null}">"${transactionType.getIsoTemplate().getValRevFunction()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${transactionType.getIsoTemplate().getTermUnblockConf() != null}">"${transactionType.getIsoTemplate().getTermUnblockConf()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						${transactionType.getIsoTemplate().isDatetimeField()},
						${transactionType.getIsoTemplate().getHeaderLength()}
						);
						<br>
						session = HibernateUtil.getSessionFactory().openSession();<br>
						session.beginTransaction();<br>
						isoTemplateId = (Integer) session.save(isoTemplate);<br>
						session.getTransaction().commit();<br>
						session.close();<br>
						isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);<br>
						System.out.println("Id de nueva plantilla iso: " + isoTemplateId);<br>
						<br>
						session = HibernateUtil.getSessionFactory().openSession();<br>						
						transactionResponseType = 
						<c:choose>
						<c:when test="${transactionType.getTransactionResponseType() != null}"> transactionTypeMap.get("${transactionType.getTransactionResponseType().getName()}")</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>;<br>
						transactionInverseType = 
						<c:choose>
						<c:when test="${transactionType.getTransactionInverseType() != null}">transactionTypeMap.get("${transactionType.getTransactionInverseType().getName()}")</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>;<br>
						transactionConfirmationType = 
						<c:choose>
						<c:when test="${transactionType.getTransactionConfirmationType() != null}">transactionTypeMap.get("${transactionType.getTransactionConfirmationType().getName()}")</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>;<br>
						transactionBatchUploadType = 
						<c:choose>
						<c:when test="${transactionType.getTransactionBatchUploadType() != null}">transactionTypeMap.get("${transactionType.getTransactionBatchUploadType().getName()}")</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>;<br>
						transactionTrailerSettleType = 
						<c:choose>
						<c:when test="${transactionType.getTransactionTrailerSettleType() != null}">transactionTypeMap.get("${transactionType.getTransactionTrailerSettleType().getName()}")</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>;<br>				
						transactionType = new TransactionType(
						<c:choose>
						<c:when test="${transactionType.getName() != null}">"${transactionType.getName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						<c:choose>
						<c:when test="${transactionType.getDescription() != null}">"${transactionType.getDescription()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						templateMessage, 
						isoTemplate, 
						<c:choose>
						<c:when test="${transactionType.getMethodName() != null}">"${transactionType.getMethodName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						${transactionType.getTimeout()},
						${transactionType.isRetry()},
						${transactionType.getRetryAttempts()},
						transactionResponseType,
						${transactionType.isInverseTrxType()}, 
						${transactionType.isBlocksOnConf()}, 
						${transactionType.isBlocksOnInv()}, 
						transactionInverseType, 
						${transactionType.isConfirmationTrxType()}, 
						transactionConfirmationType, 
						${transactionType.isReversableType()}, 
						${transactionType.getRetryDelay()}, 
						${transactionType.isConfirmableType()},
						${transactionType.isAutoInverse()},
						BatchOperation.${transactionType.getBatchOperation()},
						${transactionType.isSettlementTrxType()},
						${transactionType.isStatePrint()},
						${transactionType.isUploadableType()},
						transactionBatchUploadType,
						transactionTrailerSettleType,
						${transactionType.isAnnulmentType()}
						);<br>			
						session.beginTransaction();<br>
						transactionTypeId = (Integer) session.save(transactionType);<br>
						session.getTransaction().commit();<br>
						session.close();<br>
						transactionTypeMap.put(transactionType.getName(), transactionType);<br>
						<br>
						//Campos de la plantilla ISO<br>
						session = HibernateUtil.getSessionFactory().openSession();<br>
						isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);<br>
						<c:forEach var="templateField" items="${transactionType.getIsoTemplate().getTemplate_field()}">
						isoTemplate.insertIsoField(
						<c:choose>
						<c:when test="${templateField.getKey() != null}">"${templateField.getKey()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						${templateField.getValue().getFieldId()}, 
						IsoType.${templateField.getValue().getFieldType()}, 
						${templateField.getValue().getFieldLen()}, 
						FieldLoadType.${templateField.getValue().getLoadType()}, 
						<c:choose>
						<c:when test="${templateField.getValue().getEncoder() != null}">"${templateField.getValue().getEncoder()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getDecoder() != null}">"${templateField.getValue().getDecoder()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getLoadFunc() != null}">"${templateField.getValue().getLoadFunc()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getStoreFunc() != null}">"${templateField.getValue().getStoreFunc()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						${templateField.getValue().isStorable()}, 
						${templateField.getValue().isReturnable()}, 
						${templateField.getValue().isMandatory()}, 
						<c:choose>
						<c:when test="${templateField.getValue().getFixedValue() != null}">"${templateField.getValue().getFixedValue()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getAcquirerAttr() != null}">"${templateField.getValue().getAcquirerAttr()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getClassName() != null}">"${templateField.getValue().getClassName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>);<br>
						</c:forEach>
						<br>
						session.beginTransaction();<br>
						session.update(isoTemplate);<br>
						session.getTransaction().commit();<br>
						<br>
						//SubCampos de la plantilla ISO<br>
						<c:forEach var="templateField" items="${transactionType.getIsoTemplate().getTemplate_subfield()}">
						isoTemplate.insertIsoSubField(
						<c:choose>
						<c:when test="${templateField.getKey() != null}">"${templateField.getKey()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						isoTemplate.getIsoIdMap().get(${templateField.getValue().getParentField().getFieldId()}),
						IsoType.${templateField.getValue().getFieldType()}, 
						${templateField.getValue().getFieldLen()}, 
						FieldLoadType.${templateField.getValue().getLoadType()}, 
						<c:choose>
						<c:when test="${templateField.getValue().getEncoder() != null}">"${templateField.getValue().getEncoder()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getDecoder() != null}">"${templateField.getValue().getDecoder()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getLoadFunc() != null}">"${templateField.getValue().getLoadFunc()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getStoreFunc() != null}">"${templateField.getValue().getStoreFunc()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						${templateField.getValue().isStorable()}, 
						${templateField.getValue().isReturnable()}, 
						${templateField.getValue().isMandatory()}, 
						<c:choose>
						<c:when test="${templateField.getValue().getFixedValue() != null}">"${templateField.getValue().getFixedValue()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getAcquirerAttr() != null}">"${templateField.getValue().getAcquirerAttr()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${templateField.getValue().getClassName() != null}">"${templateField.getValue().getClassName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>,
						${templateField.getValue().getSubfieldId()});<br>
						</c:forEach>
						<br>
						session.beginTransaction();<br>
						session.update(isoTemplate);<br>
						session.getTransaction().commit();<br>
						<br>
						//Elementos de voucher adicionales<br>
						addVoucherF = null;<br>
						voucherTransactionType = null;<br>
						voucherBatchType = null;<br>
						<c:forEach var="voucherElem" items="${transactionType.getVoucherFields()}">
						voucherTransactionType =  
						<c:choose>
							<c:when test="${voucherElem.getTransactionType() != null}">
								transactionTypeMap.get("${voucherElem.getTransactionType().getName()}");
							</c:when>
							<c:otherwise>
    							null;
  							</c:otherwise>
						</c:choose><br>
						voucherBatchType = 
						<c:choose>
							<c:when test="${voucherElem.getBatchType() != null}">
								(BatchType)session.load(BatchType.class, ${voucherElem.getBatchType().getId()});
							</c:when>
							<c:otherwise>
    							null;
  							</c:otherwise>
						</c:choose><br>
						
						addVoucherF = new AdditionalVoucherFields(
						<c:choose>
						<c:when test="${voucherElem.getTableName() != null}">"${voucherElem.getTableName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${voucherElem.getFieldName() != null}">"${voucherElem.getFieldName()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						<c:choose>
						<c:when test="${voucherElem.getMessage() != null}">"${voucherElem.getMessage()}"</c:when>
						<c:otherwise>null</c:otherwise>
						</c:choose>, 
						${voucherElem.isStartReceipt()},
						${voucherElem.isEndReceipt()},
						${voucherElem.getOrderField()},
						voucherTransactionType,
						voucherBatchType,
						${voucherElem.isActive()}
						);<br>
						session.beginTransaction();<br>
						session.save(addVoucherF);<br>
						session.getTransaction().commit();<br>
						</c:forEach>
						
						</c:forEach>
						
						</p>
	
						<p>
						//Respuestas de transacciones<br>
						List&#60;TransactionResponse&#62; transactionResponse = new ArrayList&#60;TransactionResponse&#62;();<br>
						TransactionType transactionRType = null;<br>
						<c:forEach var="transactionResponse" items="${model.getTransactionResponses()}">
							transactionRType =
							<c:choose>
								<c:when test="${transactionResponse.getValue().getTransactionType() != null}">
									(TransactionType)session.load(TransactionType.class, ${transactionResponse.getValue().getTransactionType().getId()});
								</c:when>
								<c:otherwise>
    								null;
  								</c:otherwise>
							</c:choose>
							<br>
							transactionResponse.add(new TransactionResponse(<c:out value='"${transactionResponse.getValue().getCode()}"'/>, <c:out value='"${transactionResponse.getValue().getName()}"'/>, paymentChannel, 
							<c:choose>
								<c:when test="${transactionResponse.getValue().getDescription() != null}">
									<c:out value='"${transactionResponse.getValue().getDescription()}"'/>			
								</c:when>
								<c:otherwise>
    								null
  								</c:otherwise>
							</c:choose>
							, 
							<c:out value='${transactionResponse.getValue().isApprovedCode()}'/>,
							transactionRType,
							<c:out value='${transactionResponse.getValue().isUnbalanceCode()}'/>
							));<br>
						</c:forEach>
						transactionResponse.forEach((transactionR) -> {<br>
							Session sessionr = HibernateUtil.getSessionFactory().openSession();<br>
							sessionr.beginTransaction();<br>
							sessionr.save(transactionR);<br>
							sessionr.getTransaction().commit();<br>
							sessionr.close();<br>
						});<br>
						</p>
					</div>
        			<div class="row">	

	   				</div>
      			</div>
      		</div>
		</div>
	</body>
</html>