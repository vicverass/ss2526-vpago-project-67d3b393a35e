package vpago.dataapi.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EngineProperties {
	private static final Logger logger = LoggerFactory.getLogger(EngineProperties.class);
	 
	private static Boolean autoAffiliation;
	private static Boolean sendVadmin;
	private static String vadminIp;
	private static String vadminPort;
	private static String vauthIp;
	private static String vauthPort;
	private static String propertyFile = "/main/resources/dataaccessapi-development.properties";
	
	public static void loadProperties() {
		Properties engProperties = new Properties();
		InputStream inputStream = null;
		
		try {	
			inputStream = EngineProperties.class.getResourceAsStream(propertyFile);
			// load a properties file
			if(inputStream != null) engProperties.load(inputStream);

			// get the property value and print it out
			autoAffiliation = engProperties.getProperty("engine.affiliation.auto", "false").equals("true") ? true : false;
			logger.debug("engine.affiliation.auto={}", autoAffiliation.toString());
			sendVadmin = engProperties.getProperty("engine.affiliation.send-vadmin", "false").equals("true") ? true : false;
			logger.debug("engine.affiliation.send-vadmin={}", sendVadmin.toString());
			vadminIp = engProperties.getProperty("engine.connections.vadmin-ip", "localhost");
			logger.debug("engine.connections.vadmin-ip={}", vadminIp);
			vadminPort = engProperties.getProperty("engine.connections.vadmin-port");
			logger.debug("engine.connections.vadmin-port={}", vadminPort);
			vauthIp = engProperties.getProperty("engine.connections.vauth-ip", "localhost");
			logger.debug("engine.connections.vauth-ip={}", vauthIp);
			vauthPort = engProperties.getProperty("engine.connections.vauth-port", "8083");
			logger.debug("engine.connections.vauth-port={}", vauthPort);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @return the autoAffiliation
	 */
	public static Boolean getAutoAffiliation() {
		return autoAffiliation;
	}

	/**
	 * @param autoAffiliation the autoAffiliation to set
	 */
	public static void setAutoAffiliation(Boolean autoAffiliation) {
		EngineProperties.autoAffiliation = autoAffiliation;
	}

	/**
	 * @return the sendVadmin
	 */
	public static Boolean getSendVadmin() {
		return sendVadmin;
	}

	/**
	 * @param sendVadmin the sendVadmin to set
	 */
	public static void setSendVadmin(Boolean sendVadmin) {
		EngineProperties.sendVadmin = sendVadmin;
	}

	/**
	 * @return the vadminIp
	 */
	public static String getVadminIp() {
		return vadminIp;
	}

	/**
	 * @param vadminIp the vadminIp to set
	 */
	public static void setVadminIp(String vadminIp) {
		EngineProperties.vadminIp = vadminIp;
	}

	/**
	 * @return the vadminPort
	 */
	public static String getVadminPort() {
		return vadminPort;
	}

	/**
	 * @param vadminPort the vadminPort to set
	 */
	public static void setVadminPort(String vadminPort) {
		EngineProperties.vadminPort = vadminPort;
	}
	
	public static String getVauthIp() {
		return vauthIp;
	}

	public static void setVauthIp(String vauthIp) {
		EngineProperties.vauthIp = vauthIp;
	}

	public static String getVauthPort() {
		return vauthPort;
	}

	public static void setVauthPort(String vauthPort) {
		EngineProperties.vauthPort = vauthPort;
	}
	
}
