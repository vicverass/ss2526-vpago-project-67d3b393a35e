package vpago.dataapi.types;

/**
 * Tipo enumerado para representar los Tipos de Respuesta
 */
public enum ResponseType {
	
	/** Indica que el mensaje informa de un solicitud con resultado exitoso */
	SUCCESS,
	/** Indica que el Comercio al que esta asociado la solicitud no existe */
	INEXISTENTMERCHANT,
	/** El recurso solicitado no existe */
	RESOURCENOTFOUND,
	/** Error general ocurrido durante la solicitud */
	GENERALERROR,
	/** Error ocurrido durante una operacion en la base de datos */
	DATABASEERROR,
	/** Error en el formato de una solicitud */
	FORMATERROR,
	/** Recurso no autorizado o relacionado a la solicitud */
	INVALIDRESOURCE,
	;
}
