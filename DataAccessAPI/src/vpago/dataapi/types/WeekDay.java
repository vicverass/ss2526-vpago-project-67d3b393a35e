package vpago.dataapi.types;

/**
 * Tipo enumerado para representar los dias de la semana
 */
public enum WeekDay {
	LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO
}
