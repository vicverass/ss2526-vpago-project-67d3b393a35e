package vpago.dataapi.types;

/**
 * Tipo enumerado para representar los Tipos de Carga de Parametros para una transaccion
 */
public enum FieldLoadType {
	/** Obtenido de la base de datos a traves de TransactionParameter y TransactionResponseParameter */
	DATABASE,		
	/** Enviado en la invocacion al servicio web correspondiente */
	WEBSERVICE,		
	/** Almacenado como valor fijo en la base de datos a traves TemplateField o TemplateSubField */
	FIXED,			 
	/** Calculado u obtenido utilizando una funcion determinada */
	COMPUTED,		
	/** Carga delegada a los subcampos */
	SUBFIELD,		
	/** Campos relacionados al manejo de reglas del comercio */
	RULE,			
	/** Ignorar en el momento de la carga */
	NONE			
}
