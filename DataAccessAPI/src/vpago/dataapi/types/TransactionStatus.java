package vpago.dataapi.types;

/**
 * Tipo enumerado para representar un Tipo de Estado de Transaccion
 */
public enum TransactionStatus {
	/** Transaccion completada pero no cerrada */
	COMPLETED("Transaccion completada pero no cerrada"),
	/** Tiempo de espera vencido */
	TIMED_OUT("Tiempo de espera vencido"),
	/** Transaccion reversada */
	REVERSED("Transaccion reversada"),
	/** Transaccion completada y cerrada */
	CLOSED("Transaccion completada y cerrada"),
	/** Transaccion pendiente */
	PENDING("Transaccion pendiente");
	
	/** Descripcion del Tipo de Estado de la Transaccion */
	@SuppressWarnings("unused")
	private final String description; 
	
	TransactionStatus(String description) {
		this.description = description;
	}
}
