package vpago.dataapi.migrate;

import org.flywaydb.core.Flyway;

/**
 * Metodos para la ejecucion de las migraciones en la base de datos
 */
public class DBControl {

	public static void main(String[] args) {
		migrateDB();
	}
	
	/**
	 * Aplica una migracion a la base de datos
	 */
	public static void migrateDB() {
		// Create the Flyway instance
        Flyway flyway = new Flyway();
        
        // Point it to the database
        //HSQLDB
        flyway.setLocations("db/migration/hsqldb");
        flyway.setDataSource("jdbc:hsqldb:hsql://localhost/engine_db", "SA", null); //Local
        //flyway.setDataSource("jdbc:hsqldb:hsql://172.28.111.130/engine_db", "SA", null); //Pruebas desarrollo Dayco
        flyway.setOutOfOrder(true); //Para ejecutar migraciones con fechas anteriores a la ultima aplicada
        //PostgreSQL
        //flyway.setLocations("db/migration/postgresql");
        //flyway.setDataSource("jdbc:postgresql://localhost/engine_db", "engine_db", "engine_db");
        // Start the migration
        flyway.repair();
        flyway.migrate();
        //flyway.clean();
	}

}
