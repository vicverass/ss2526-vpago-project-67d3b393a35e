package vpago.dataapi.classes.forms;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
@XmlType(propOrder={"active", "inactive", "allMerchant", "allMerchantAffiliationInac"})
public class DashboardMerchant extends APIResponse {
	
	public DashboardMerchant() {
		super();
	}

	public DashboardMerchant(Integer active, Integer inactive, Integer allMerchant,
			Integer allMerchantAffiliationInac) {
		super();
		this.active = active;
		this.inactive = inactive;
		this.allMerchant = allMerchant;
		this.allMerchantAffiliationInac = allMerchantAffiliationInac;
	}

	private Integer active;
	private Integer inactive;
	private Integer allMerchant;
	private Integer allMerchantAffiliationInac;
	public Integer getActive() {
		return active;
	}
	
	public void setActive(Integer active) {
		this.active = active;
	}
	
	public Integer getInactive() {
		return inactive;
	}
	
	public void setInactive(Integer inactive) {
		this.inactive = inactive;
	}
	
	public Integer getAllMerchant() {
		return allMerchant;
	}
	
	public void setAllMerchant(Integer allMerchant) {
		this.allMerchant = allMerchant;
	}

	public Integer getAllMerchantAffiliationInac() {
		return allMerchantAffiliationInac;
	}

	public void setAllMerchantAffiliationInac(Integer allMerchantAffiliationInac) {
		this.allMerchantAffiliationInac = allMerchantAffiliationInac;
	}

	@Override
	public String toString() {
		return "DashboardMerchant [active=" + active + ", inactive=" + inactive + ", allMerchant=" + allMerchant
				+ ", allMerchantAffiliationInac=" + allMerchantAffiliationInac + "]";
	}

	
	
}
