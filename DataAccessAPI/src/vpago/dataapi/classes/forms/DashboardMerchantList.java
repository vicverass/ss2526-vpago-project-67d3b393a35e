package vpago.dataapi.classes.forms;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
public class DashboardMerchantList extends APIResponse {
	public ArrayList<DashboardMerchant> termList = new ArrayList<DashboardMerchant>();
}
