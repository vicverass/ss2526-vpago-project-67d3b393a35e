package vpago.dataapi.classes.forms;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MerchantTerminalList {
	public ArrayList<MerchantTerminal> termList = new ArrayList<MerchantTerminal>();
}
