package vpago.dataapi.classes.forms;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
public class DashboardTerminalList extends APIResponse {
	public ArrayList<DashboardTerminal> termList = new ArrayList<DashboardTerminal>();
}
