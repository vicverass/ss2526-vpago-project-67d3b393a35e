package vpago.dataapi.classes.forms;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder={"merchantGuid", "affiliationCode", "terminalGuid", "terminalCode"})
public class MerchantTerminal {
	
	public MerchantTerminal() {
		super();
	}
	
	public MerchantTerminal(String merchantGuid, String affiliationCode, String terminalGuid, String terminalCode) {
		super();
		this.merchantGuid = merchantGuid;
		this.affiliationCode = affiliationCode;
		this.terminalGuid = terminalGuid;
		this.terminalCode = terminalCode;
	}

	private Integer id;
	private String merchantGuid;
	private String affiliationCode;
	private String terminalGuid;
	private String terminalCode;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the merchantGuid
	 */
	public String getMerchantGuid() {
		return merchantGuid;
	}
	
	/**
	 * @param merchantGuid the merchantGuid to set
	 */
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	/**
	 * @return the affiliationCode
	 */
	public String getAffiliationCode() {
		return affiliationCode;
	}
	
	/**
	 * @param affiliationCode the affiliationCode to set
	 */
	public void setAffiliationCode(String affiliationCode) {
		this.affiliationCode = affiliationCode;
	}
	
	/**
	 * @return the terminalGuid
	 */
	public String getTerminalGuid() {
		return terminalGuid;
	}
	
	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	public void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}
	
	/**
	 * @return the terminalCode
	 */
	public String getTerminalCode() {
		return terminalCode;
	}
	
	/**
	 * @param terminalCode the terminalCode to set
	 */
	public void setTerminalCode(String terminalCode) {
		this.terminalCode = terminalCode;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MerchantTerminal [id=" + id + ", merchantGuid=" + merchantGuid + ", affiliationCode=" + affiliationCode
				+ ", terminalGuid=" + terminalGuid + ", terminalCode=" + terminalCode + "]";
	}

}
