package vpago.dataapi.classes.forms;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
public class DashboardList extends APIResponse {
	public ArrayList<Dashboard> termList = new ArrayList<Dashboard>();
}
