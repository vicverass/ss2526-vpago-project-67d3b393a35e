package vpago.dataapi.classes.forms;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
@XmlType(propOrder={"active", "inactive", "allTerminal"})
public class DashboardTerminal extends APIResponse {
	
	public DashboardTerminal() {
		super();
	}

	public DashboardTerminal(Integer active, Integer inactive, Integer allTerminal) {
		super();
		this.active = active;
		this.inactive = inactive;
		this.allTerminal = allTerminal;
	}

	private Integer active;
	private Integer inactive;
	private Integer allTerminal;
	
	public Integer getActive() {
		return active;
	}
	
	public void setActive(Integer active) {
		this.active = active;
	}
	
	public Integer getInactive() {
		return inactive;
	}
	
	public void setInactive(Integer inactive) {
		this.inactive = inactive;
	}
	
	public Integer getAllTerminal() {
		return allTerminal;
	}
	
	public void setAllTerminal(Integer allTerminal) {
		this.allTerminal = allTerminal;
	}

	@Override
	public String toString() {
		return "Terminal [active=" + active + ", inactive=" + inactive + ", allTerminal=" + allTerminal + "]";
	}
	
	
}
