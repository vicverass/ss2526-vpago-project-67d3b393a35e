package vpago.dataapi.classes.forms;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NewMerchantTerminalList {
	public ArrayList<NewMerchantTerminal> termList = new ArrayList<NewMerchantTerminal>();
}
