package vpago.dataapi.classes.forms;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
@XmlType(propOrder={"corporation", "merchant", "multimerchant", "terminalActive",  "terminalInactive", "transactionDay", "transactionWeek", "transactionMonth", "amountTransactionOfDay", "amountTransactionOfWeek", "amountTransactionOfMonth"})
public class Dashboard extends APIResponse {
	
	public Dashboard() {
		super();
	}

	public Dashboard(Integer corporation, Integer merchant, Integer multimerchant, Integer terminalActive, 
			Integer terminalInactive, Integer transactionDay, Integer transactionWeek, Integer transactionMonth, double amountTransactionOfDay,
			double amountTransactionOfWeek, double amountTransactionOfMonth) {
		super();
		this.corporation = corporation;
		this.merchant = merchant;
		this.multimerchant = multimerchant;
		this.terminalActive = terminalActive;
		this.terminalActive = terminalInactive;
		this.transactionDay = transactionDay;
		this.transactionWeek = transactionWeek;
		this.transactionMonth = transactionMonth;
		this.amountTransactionOfDay = amountTransactionOfDay;
		this.amountTransactionOfWeek = amountTransactionOfWeek;
		this.amountTransactionOfMonth = amountTransactionOfMonth;
	}


	private Integer corporation;
	private Integer merchant;
	private Integer multimerchant;
	private Integer terminalActive;
	private Integer terminalInactive;	
	private Integer transactionDay;
	private Integer transactionWeek;
	private Integer transactionMonth;
	private double amountTransactionOfDay;
	private double amountTransactionOfWeek;
	private double amountTransactionOfMonth;
	
	public Integer getCorporation() {
		return corporation;
	}

	public void setCorporation(Integer corporation) {
		this.corporation = corporation;
	}

	public Integer getMerchant() {
		return merchant;
	}

	public void setMerchant(Integer merchant) {
		this.merchant = merchant;
	}

	public Integer getMultimerchant() {
		return multimerchant;
	}

	public void setMultimerchant(Integer multimerchant) {
		this.multimerchant = multimerchant;
	}

	public Integer getTerminalActive() {
		return terminalActive;
	}

	public void setTerminalActive(Integer terminalActive) {
		this.terminalActive = terminalActive;
	}

	public Integer getTransactionDay() {
		return transactionDay;
	}

	public void setTransactionDay(Integer transactionDay) {
		this.transactionDay = transactionDay;
	}

	public Integer getTransactionWeek() {
		return transactionWeek;
	}

	public void setTransactionWeek(Integer transactionWeek) {
		this.transactionWeek = transactionWeek;
	}

	public Integer getTransactionMonth() {
		return transactionMonth;
	}

	public void setTransactionMonth(Integer transactionMonth) {
		this.transactionMonth = transactionMonth;
	}

	public double getAmountTransactionOfDay() {
		return amountTransactionOfDay;
	}

	public void setAmountTransactionOfDay(double amountTransactionOfDay) {
		this.amountTransactionOfDay = amountTransactionOfDay;
	}

	public double getAmountTransactionOfWeek() {
		return amountTransactionOfWeek;
	}

	public void setAmountTransactionOfWeek(double amountTransactionOfWeek) {
		this.amountTransactionOfWeek = amountTransactionOfWeek;
	}

	public double getAmountTransactionOfMonth() {
		return amountTransactionOfMonth;
	}

	public void setAmountTransactionOfMonth(double amountTransactionOfMonth) {
		this.amountTransactionOfMonth = amountTransactionOfMonth;
	}

	public Integer getTerminalInactive() {
		return terminalInactive;
	}

	public void setTerminalInactive(Integer terminalInactive) {
		this.terminalInactive = terminalInactive;
	}

	@Override
	public String toString() {
		return "Dashboard [corporation=" + corporation + ", merchant=" + merchant + ", multimerchant=" + multimerchant
				+ ", terminalActive=" + terminalActive + ", terminalInactive=" + terminalInactive + ", transactionDay="
				+ transactionDay + ", transactionWeek=" + transactionWeek + ", transactionMonth=" + transactionMonth
				+ ", amountTransactionOfDay=" + amountTransactionOfDay + ", amountTransactionOfWeek="
				+ amountTransactionOfWeek + ", amountTransactionOfMonth=" + amountTransactionOfMonth + "]";
	}

	
	
}
