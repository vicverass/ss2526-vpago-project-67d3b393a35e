package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Parroquia
 */
@XmlRootElement
@XmlType(propOrder={"id", "municipalityId", "parishName"})
@Entity
@Table(name="parish")
public class Parish extends APIResponse {
	
	public Parish() {
		super();
	}
	
	public Parish(Municipality municipality, Integer municipalityId, String parishName) {
		super();
		this.municipality = municipality;
		this.municipalityId = municipalityId;
		this.parishName = parishName;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Municipio al que esta asociada la parroquia */
	private Municipality municipality;
	/** Identificador del municipio al que esta asociada la parroquia */
	private Integer municipalityId;
	/** Nombre de la parroquia */
	private String parishName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="parish_id_seq")
	@SequenceGenerator(name="parish_id_seq", sequenceName = "parish_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the municipality
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "municipality_id")
	public Municipality getMunicipality() {
		return municipality;
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}

	/**
	 * @return the municipalityId
	 */
	@Column(name="municipality_id", updatable=false, insertable=false)
	public Integer getMunicipalityId() {
		return municipalityId;
	}

	/**
	 * @param municipalityId the municipalityId to set
	 */
	public void setMunicipalityId(Integer municipalityId) {
		this.municipalityId = municipalityId;
	}

	/**
	 * @return the parishName
	 */
	@Column(name="parish")
	public String getParishName() {
		return parishName;
	}

	/**
	 * @param parishName the parishName to set
	 */
	public void setParishName(String parishName) {
		this.parishName = parishName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((municipality == null) ? 0 : municipality.hashCode());
		result = prime * result + ((municipalityId == null) ? 0 : municipalityId.hashCode());
		result = prime * result + ((parishName == null) ? 0 : parishName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parish other = (Parish) obj;
		if (id != other.id)
			return false;
		if (municipality == null) {
			if (other.municipality != null)
				return false;
		} else if (!municipality.equals(other.municipality))
			return false;
		if (municipalityId == null) {
			if (other.municipalityId != null)
				return false;
		} else if (!municipalityId.equals(other.municipalityId))
			return false;
		if (parishName == null) {
			if (other.parishName != null)
				return false;
		} else if (!parishName.equals(other.parishName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Parish [id=" + id + ", municipality=" + municipality + ", municipalityId=" + municipalityId
				+ ", parishName=" + parishName + "]";
	}
	
	
}
