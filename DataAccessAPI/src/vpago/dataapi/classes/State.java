package vpago.dataapi.classes;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Estado
 */
@XmlRootElement
@XmlType(propOrder={"id", "countryId", "stateName", "iso31662Code"})
@Entity
@Table(name="state")
public class State extends APIResponse {
	
	public State() {
		super();
	}
	
	public State(Country country, Integer countryId, String stateName, String iso31662Code) {
		super();
		this.country = country;
		this.countryId = countryId;
		this.stateName = stateName;
		this.iso31662Code = iso31662Code;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Pais al que esta asociado el estado */
	private Country country;
	/** Identificador del pais al que esta asociado el estado */
	private Integer countryId;
	/** Nombre del estado */
	private String stateName;
	/** Codigo ISO 3166-2 */
	private String iso31662Code;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="state_id_seq")
	@SequenceGenerator(name="state_id_seq", sequenceName = "state_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the country
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id")
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @return the countryId
	 */
	@Column(name="country_id", updatable=false, insertable=false)
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the stateName
	 */
	@Column(name="state")
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the iso31662Code
	 */
	@Column(name="iso_3166_2")
	public String getIso31662Code() {
		return iso31662Code;
	}

	/**
	 * @param iso31662Code the iso31662Code to set
	 */
	public void setIso31662Code(String iso31662Code) {
		this.iso31662Code = iso31662Code;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + id;
		result = prime * result + ((iso31662Code == null) ? 0 : iso31662Code.hashCode());
		result = prime * result + ((stateName == null) ? 0 : stateName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (id != other.id)
			return false;
		if (iso31662Code == null) {
			if (other.iso31662Code != null)
				return false;
		} else if (!iso31662Code.equals(other.iso31662Code))
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "State [id=" + id + ", country=" + country + ", countryId=" + countryId + ", stateName=" + stateName
				+ ", iso31662Code=" + iso31662Code + "]";
	}
	
	
}