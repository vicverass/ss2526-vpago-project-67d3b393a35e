package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 Clase persistente para almacenar parametros asociados a las transacciones
 */
@XmlRootElement
@XmlType(propOrder={"id", "transaction", "paramName", "paramValue", "paramType"})
@XmlSeeAlso({TransactionResponseParameter.class})
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="param_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("request")
@Table(name="transaction_parameters")
public class TransactionParameter extends APIResponse {
	
	public TransactionParameter() {
		super();
	}
	
	public TransactionParameter(Transaction transaction, String paramName, String paramValue) {
		super();
		this.transaction = transaction;
		this.paramName = paramName;
		this.paramValue = paramValue;
	}
	
	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Transaccion a la que esta asociado el parametro */
	protected Transaction transaction;
	/** Nombre del parametro */
	protected String paramName;
	/** Valor del parametro */
	protected String paramValue;
	/** Tipo de parametro (solicitud o respuesta) */
	protected String paramType;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_parameters_id_seq")
	@SequenceGenerator(name="transaction_parameters_id_seq", sequenceName = "transaction_parameters_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the transaction
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction_id")
	public Transaction getTransaction() {
		return transaction;
	}
	
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	/**
	 * @return the paramName
	 */
	@Column(name="param_name")
	public String getParamName() {
		return paramName;
	}
	
	/**
	 * @param paramName the paramName to set
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	/**
	 * @return the paramValue
	 */
	@Column(name="param_value")
	public String getParamValue() {
		return paramValue;
	}
	
	/**
	 * @param paramValue the paramValue to set
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	
	/**
	 * @return the paramType
	 */
	@Column(name="param_type", insertable=false, updatable=false)
	public String getParamType() {
		return paramType;
	}
	
	/**
	 * @param paramType the paramType to set
	 */
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	@Override
	public String toString() {
		return "TransactionParameter [id=" + id + ", transaction=" + transaction + ", paramName=" + paramName
				+ ", paramValue=" + paramValue + ", paramType=" + paramType + "]";
	}

}
