package vpago.dataapi.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Afiliacion
 */
@XmlRootElement
@XmlType(propOrder={"id", "merchantId", "paymentChannelId", "active"})
@Entity
@Table(name="affiliation")
public class Affiliation extends APIResponse {
	
	public Affiliation() {
		super();
	}
	
	public Affiliation(Merchant merchant, PaymentChannel paymentChannel) {
		super();
		this.merchantId = merchant.getId();
		this.merchant = merchant;
		this.paymentChannelId = paymentChannel.getId();
		this.paymentChannel = paymentChannel;
		this.active = true;
		this.activeRuleSet = null;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del Comercio afiliado */
	private Integer merchantId;
	/** Comercio afiliado */
	private Merchant merchant;
	/** Identificador del Canal de Pago al cual esta afiliado el Comercio */
	private Integer paymentChannelId;
	/** Canal de Pago al cual esta afiliado el Comercio */
	private PaymentChannel paymentChannel;
	/** Indica si el Comercio esta activo para este Canal de Pago */
	private boolean active;
	/** Conjunto de Reglas activo del Comercio para esta Afiliacion */
	private RuleSet activeRuleSet;
	/** Conjunto de Adquirientes asociados a la Afiliacion */
	private List<Acquirer> acquirers;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="affiliation_id_seq")
	@SequenceGenerator(name="affiliation_id_seq", sequenceName = "affiliation_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the merchantId
	 */
	@Column(name="merchant_id", insertable=false, updatable=false)
	public Integer getMerchantId() {
		return merchantId;
	}
	
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	
	/**
	 * @return the merchant
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "merchant_id")
	public Merchant getMerchant() {
		return merchant;
	}
	
	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
	/**
	 * @return the paymentChannelId
	 */
	@Column(name="payment_channel_id", insertable=false, updatable=false)
	public Integer getPaymentChannelId() {
		return paymentChannelId;
	}
	
	/**
	 * @param paymentChannelId the paymentChannelId to set
	 */
	public void setPaymentChannelId(Integer paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}
	
	/**
	 * @return the paymentChannel
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}
	
	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the active
	 */
	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the activeRuleSet
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "active_rule_set_id")
	public RuleSet getActiveRuleSet() {
		return activeRuleSet;
	}
	
	/**
	 * @param activeRuleSet the activeRuleSet to set
	 */
	public void setActiveRuleSet(RuleSet activeRuleSet) {
		this.activeRuleSet = activeRuleSet;
	}
	
	/**
	 * @return the acquirers
	 */
	@XmlTransient
	@OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="affiliation_id")
	public List<Acquirer> getAcquirers() {
		return acquirers;
	}

	/**
	 * @param acquirers the acquirers to set
	 */
	public void setAcquirers(List<Acquirer> acquirers) {
		this.acquirers = acquirers;
	}

	@Override
	public String toString() {
		return "Affiliation [id=" + id + ", merchantId=" + merchantId + ", paymentChannelId=" + paymentChannelId
				+ ", active=" + active + "]";
	}
	
}
