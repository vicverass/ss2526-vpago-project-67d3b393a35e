package vpago.dataapi.classes;

import java.util.Calendar;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Clase persistente para representar un Multicomercio
 */
@XmlRootElement
@Entity
@DiscriminatorValue("multi")
public class Multimerchant extends Merchant {
	
	public Multimerchant() {
		super();
	}

	public Multimerchant(String name, String rif, Location location, String fantasyName, boolean active,
			IndustryType industryType, Corporation corporation, Calendar createDate) {
		super(name, rif, location, fantasyName, true, industryType, null, corporation, createDate);
	}
	
	/** Lista de Comercios asociados al Multicomercio */
	private List<Merchant> merchants;

	/**
	 * @return the merchants
	 */
	@XmlTransient
	@OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="parent_merchant_id")
	public List<Merchant> getMerchants() {
		return merchants;
	}

	/**
	 * @param merchants the merchants to set
	 */
	public void setMerchants(List<Merchant> merchants) {
		this.merchants = merchants;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Multimerchant [merchants=" + merchants + ", id=" + id + ", merchantGuid=" + merchantGuid + ", name="
				+ name + ", rif=" + rif + ", location=" + location + ", fantasyName=" + fantasyName + ", active="
				+ active + ", industryTypeId=" + industryTypeId + ", industryType=" + industryType
				+ "]";
	}
}
