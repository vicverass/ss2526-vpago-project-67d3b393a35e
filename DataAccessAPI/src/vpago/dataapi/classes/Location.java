package vpago.dataapi.classes;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Clase persistente para representar una Ubicacion
 */
@XmlType(propOrder={"address", "contact", "phone", "email"})
@Entity
@Table(name="location")
public class Location {
	private static final Logger logger = LoggerFactory.getLogger(Location.class);
	
	public Location() {
		this(new Address(), new Contact(), "", "");
	}
	
	public Location(Address address, Contact contact, String phone, String email) {
		this.address = address == null ? new Address() : address;
		this.contact = contact == null ? new Contact() : contact;
		this.phone = phone.isEmpty() || phone == null ? "" : phone;
		this.email = email.isEmpty() || email == null ? "" : email;
	}
	
	public void update(Location other) {
		logger.debug("Update location");
		this.address.update(other.address);
		this.contact.update(other.contact);
		this.phone = this.phone != other.phone ? other.phone : this.phone;
		this.email = this.email != other.email ? other.email : this.email;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Direccion de la Ubicacion */
	private Address address;
	/** Contacto de la Ubicacion */
	private Contact contact;
	/** Numero de telefono de la Ubicacion */
	private String phone;
	/** Direccion de correo electronico de la Ubicacion */
	private String email;
	
	/**
	 * @return the id
	 */
	@XmlTransient
	@Id @GeneratedValue(generator="location_id_seq")
	@SequenceGenerator(name="location_id_seq", sequenceName = "location_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the address
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "address_id")
	@NotNull
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the contact
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "contact_id")
	@NotNull
	public Contact getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Location [address=" + address + ", contact=" + contact + ", phone=" + phone + ", email=" + email + "]";
	}

}
