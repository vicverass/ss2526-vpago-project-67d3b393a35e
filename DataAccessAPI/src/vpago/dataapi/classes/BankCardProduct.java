package vpago.dataapi.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Producto de Tarjeta de Banco
 */
@XmlRootElement
@XmlType(propOrder={
		"cardBank", 
		"cardBankId"})
@Entity
@DiscriminatorValue("bank")
public class BankCardProduct extends GeneralCardProduct {

	public BankCardProduct() {
		super();
	}

	public BankCardProduct(CardFranchise cardFranchise, CardType cardType, Bin productBin, Bank cardBank) {
		super(cardFranchise, cardType, productBin);
		this.cardBank = cardBank;
	}
	
	
	public void update(BankCardProduct other) {
		this.setCardFranchise(other.getCardFranchise());
		this.setCardType(other.getCardType());
		this.setProductBin(other.getProductBin());
		this.setCardBank(other.getCardBank());
	}
	
	/** Banco emisor de la tarjeta */
	private Bank cardBank;
	/** Indice del Banco emisor de la tarjeta */
	private Integer cardBankId;

	/**
	 * @return the cardBank
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bank_id")
	public Bank getCardBank() {
		return cardBank;
	}

	/**
	 * @param cardBank the cardBank to set
	 */
	public void setCardBank(Bank cardBank) {
		this.cardBank = cardBank;
	}
	
	@Transient
	public Integer getCardBankId() {
		return cardBankId;
	}

	public void setCardBankId(Integer cardBankId) {
		this.cardBankId = cardBankId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BankCardProduct [id=" + id + ", cardFranchise=" + cardFranchise + ", cardType=" + cardType
				+ ", productBin=" + productBin + ", cardBank=" + cardBank + "]";
	}

}
