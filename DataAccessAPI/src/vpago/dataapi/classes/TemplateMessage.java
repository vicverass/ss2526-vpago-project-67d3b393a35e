package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Plantilla de Mensajes
 */
@XmlRootElement
@XmlType(propOrder={"id", "terminalParamName"})
@Entity
@Table(name="template_message")
public class TemplateMessage extends APIResponse{
	
	public TemplateMessage() {
		super();
	}
	
	public TemplateMessage(int id, String terminalParamName) {
		super();
		this.id = id;
		this.terminalParamName = terminalParamName;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre del parametro para los terminales bancarios asociados a este canal de pago */ 
	private String terminalParamName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="template_message_id_seq")
	@SequenceGenerator(name="template_message_id_seq", sequenceName = "template_message_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the terminalParamName
	 */
	@Column(name="terminal_param_name")
	public String getTerminalParamName() {
		return terminalParamName;
	}
	
	/**
	 * @param terminalParamName the terminalParamName to set
	 */
	public void setTerminalParamName(String terminalParamName) {
		this.terminalParamName = terminalParamName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateMessage [id=" + id + ", terminalParamName=" + terminalParamName + "]";
	}
	
}
