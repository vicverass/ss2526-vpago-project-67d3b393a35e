package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos para la consulta de las transacciones
 */
@XmlRootElement
@XmlType(name="transactionConsultMin", propOrder={"txnDate", "bankName", "bankRif", "merchantName", 
		"merchantRif", "terminal", "affiliatedId", 
"pan" , "authCode", "loteHist", "loteCurr", "ref", "trace", "amount", "ansCode", "cardTypeHist"})//TODO agregar cardType y lote
public class TransactionConsultMin {
	
	public TransactionConsultMin() {
		super();
	}	


	public TransactionConsultMin(String txnDate, String bankName, String bankRif, String merchantName,
			String merchantRif, String terminal, String affiliatedId, String pan, String authCode, String loteHist,
			String loteCurr, String ref, String trace, String amount, String ansCode) {
		super();
		this.txnDate = txnDate;
		this.bankName = bankName;
		this.bankRif = bankRif;
		this.merchantName = merchantName;
		this.merchantRif = merchantRif;
		this.terminal = terminal;
		this.affiliatedId = affiliatedId;
		this.pan = pan;
		this.authCode = authCode;
		this.loteHist = loteHist;
		this.loteCurr = loteCurr;
		this.ref = ref;
		this.trace = trace;
		this.amount = amount;
		this.ansCode = ansCode;
	}


	private String txnDate;
	private String bankName;
	private String bankRif;
	private String merchantName;
	private String merchantRif;
	private String terminal;
	private String affiliatedId;
	private String cardTypeHist;
//	private String cardTypeCurr;
	private String pan;
	private String authCode;
	private String loteHist;
	private String loteCurr;
	private String ref;
	private String trace;
	private String amount;
	private String ansCode;
	
	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankRif() {
		return bankRif;
	}

	public void setBankRif(String bankRif) {
		this.bankRif = bankRif;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantRif() {
		return merchantRif;
	}

	public void setMerchantRif(String merchantRif) {
		this.merchantRif = merchantRif;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getAffiliatedId() {
		return affiliatedId;
	}

	public void setAffiliatedId(String affiliatedId) {
		this.affiliatedId = affiliatedId;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getLoteHist() {
		return loteHist;
	}

	public void setLoteHist(String loteHist) {
		this.loteHist = loteHist;
	}

	public String getLoteCurr() {
		return loteCurr;
	}

	public void setLoteCurr(String loteCurr) {
		this.loteCurr = loteCurr;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAnsCode() {
		return ansCode;
	}

	public void setAnsCode(String ansCode) {
		this.ansCode = ansCode;
	}

	public String getCardTypeHist() {
		return cardTypeHist;
	}

	public void setCardTypeHist(String cardTypeHist) {
		this.cardTypeHist = cardTypeHist;
	}


	@Override
	public String toString() {
		return "TransactionConsultMin [txnDate=" + txnDate + ", bankName=" + bankName + ", bankRif=" + bankRif
				+ ", merchantName=" + merchantName + ", merchantRif=" + merchantRif + ", terminal=" + terminal
				+ ", affiliatedId=" + affiliatedId + ", cardTypeHist=" + cardTypeHist + ", pan=" + pan + ", authCode="
				+ authCode + ", loteHist=" + loteHist + ", loteCurr=" + loteCurr + ", ref=" + ref + ", trace=" + trace
				+ ", amount=" + amount + ", ansCode=" + ansCode + "]";
	}

/*	public String getCardTypeCurr() {
		return cardTypeCurr;
	}

	public void setCardTypeCurr(String cardTypeCurr) {
		this.cardTypeCurr = cardTypeCurr;
	}
*/



}
