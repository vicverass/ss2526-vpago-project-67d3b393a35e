package vpago.dataapi.classes.minified;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Terminal
 */
@XmlRootElement
@XmlType(name="simpleTerminal", propOrder={"id", "terminalGuid", "active", "affiliationId", "createDate"})
public class TerminalMin {
	
	public TerminalMin() {
		super();
	}
	
	public TerminalMin(int id, String terminalGuid, boolean active, int affiliationId, Calendar createDate) {
		super();
		this.id = id;
		this.terminalGuid = terminalGuid;
		this.active = active;
		this.affiliationId = affiliationId;
		this.createDate = createDate;
	}

	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Identificador unico del Comercio en la base de datos */
	private String terminalGuid;
	/** Indica si el Comercio se encuentra activo */
	private boolean active;
	/** Afiliacion a la que esta asociado el Terminal */
	private int affiliationId;
	private Calendar createDate;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the terminalGuid
	 */
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	public void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the affiliationId
	 */
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}
	

	public Calendar getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalMin [id=" + id + ", terminalGuid=" + terminalGuid + ", active=" + active + ", affiliationId="
				+ affiliationId + ", createDate=" + createDate + "]";
	}
	
}
