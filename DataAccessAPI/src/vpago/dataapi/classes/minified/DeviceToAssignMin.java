package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Device;
import vpago.dataapi.classes.DeviceType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Dispositivo a asignar
 */
@XmlRootElement
@XmlType(name="simpleDeviceAssign", propOrder={"id", "deviceId", "deviceName", "deviceTypeId", "deviceTypeName", "deviceSerial", "available"})
public class DeviceToAssignMin extends APIResponse {
	
	public DeviceToAssignMin() {
		super();
	}
	
	public DeviceToAssignMin(int id, Device device, String deviceSerial, boolean available) {
		super();
		this.id = id;
		this.deviceId = device != null ? device.getId() : null;
		this.deviceName = device != null ? device.getDisplayName() : null;
		DeviceType deviceType = device != null ? device.getType(): null;
		this.deviceTypeId = deviceType != null ? deviceType.getId() : null;
		this.deviceTypeName = deviceType != null ? deviceType.getName() : null;
		this.deviceSerial = deviceSerial;
		this.available = available;
	}
	
	/** Id en la base de datos */
	private int id;
	/** Id de Dispositivo **/
	private Integer deviceId;
	/** Nombre de Dispositivo **/
	private String deviceName;
	/** Id de Tipo de Dispositivo **/
	private Integer deviceTypeId;
	/** Nombre de Tipo de Dispositivo **/
	private String deviceTypeName;
	/** Serial */
	private String deviceSerial;
	/** Disponible **/
	private boolean available;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}

	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}

	public String getDeviceTypeName() {
		return deviceTypeName;
	}

	public void setDeviceTypeName(String deviceTypeName) {
		this.deviceTypeName = deviceTypeName;
	}

	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}
	
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return "DeviceToAssignMin [id=" + id + ", deviceId=" + deviceId + ", deviceName=" + deviceName
				+ ", deviceTypeId=" + deviceTypeId + ", deviceTypeName=" + deviceTypeName + ", deviceSerial="
				+ deviceSerial + ", available=" + available + "]";
	}


	
	
}
