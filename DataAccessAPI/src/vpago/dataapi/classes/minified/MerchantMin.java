package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Comercio
 */
@XmlRootElement
@XmlType(name="simpleMerchant", propOrder={"id", "merchantGuid", "fantasyName", "active", "name", "rif", "multi"})
public class MerchantMin {
	
	public MerchantMin() {
		super();
	}
	
	public MerchantMin(int id, String merchantGuid, String name, String rif, String fantasyName, boolean active, String merchantType) {
		super();
		this.id = id;
		this.merchantGuid = merchantGuid;
		this.name = name;
		this.rif = rif;
		this.fantasyName = fantasyName;
		this.active = active;
		this.multi = merchantType.compareTo("multi") == 0 ? true : false;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Identificador unico del Comercio en la base de datos */
	private String merchantGuid;
	/** Nombre del Comercio */
	private String name;
	/** Registro de Identificacion Fiscal del Comercio **/
	private String rif;
	/** Nombre fantasia */
	private String fantasyName;
	/** Comercio activo */
	private boolean active;
	/** Si es Multicomercio */
	private boolean multi;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the merchantGuid
	 */
	public String getMerchantGuid() {
		return merchantGuid;
	}

	/**
	 * @param merchantGuid the merchantGuid to set
	 */
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rif
	 */
	public String getRif() {
		return rif;
	}

	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}

	/**
	 * @return the fantasyName
	 */
	public String getFantasyName() {
		return fantasyName;
	}

	/**
	 * @param fantasyName the fantasyName to set
	 */
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the multi
	 */
	public boolean isMulti() {
		return multi;
	}

	/**
	 * @param multi the multi to set
	 */
	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Merchant [id=" + id + ", merchantGuid=" + merchantGuid + "]";
	}
}
