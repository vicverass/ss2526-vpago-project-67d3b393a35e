package vpago.dataapi.classes.minified;

import java.math.BigInteger;
import java.util.Calendar;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos para la consulta de los lotes cerrados
 */
@XmlRootElement
@XmlType(name="batchSummaryMin", propOrder={"product", "transactions", "totalAmount"})
public class BatchSummaryConsultMin {
	
	public BatchSummaryConsultMin() {
		super();
	}	

	public BatchSummaryConsultMin(String product, BigInteger transactions, double totalAmount) {
		super();
		this.product = product;
		this.transactions = transactions;
		this.totalAmount = totalAmount;
	}

	private String product;
	private BigInteger transactions;
	private double totalAmount;
	
	public String getProduct() {
		return product;
	}
	
	public void setProduct(String product) {
		this.product = product;
	}
	
	public BigInteger getTransactions() {
		return transactions;
	}
	
	public void setTransactions(BigInteger transactions) {
		this.transactions = transactions;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "BatchSummaryConsultMin [product=" + product + ", transactions=" + transactions + ", totalAmount="
				+ totalAmount + "]";
	}
	
}
