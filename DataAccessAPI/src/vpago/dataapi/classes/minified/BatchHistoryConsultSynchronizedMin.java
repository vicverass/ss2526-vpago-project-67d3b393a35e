package vpago.dataapi.classes.minified;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
@XmlType(propOrder={"batchId", "terminalId", "merchantGuid", "bankName", "merchantName", "rif", "merchantIdCode",
		"terminalBank", "batchType", "batchNumber", "totalAmount", "duplicate"})
public class BatchHistoryConsultSynchronizedMin extends APIResponse {

	public BatchHistoryConsultSynchronizedMin() {
		super();
	}
	
	public BatchHistoryConsultSynchronizedMin(BigDecimal batchId, BigDecimal terminalId, String merchantGuid, String bankName,
			String merchantName, String rif, String merchantIdCode, String terminalBank, String batchType,
			BigDecimal batchNumber, BigDecimal totalAmount, BigDecimal duplicate) {
		super();
		this.batchId = batchId;
		this.terminalId = terminalId;
		this.merchantGuid = merchantGuid;
		this.bankName = bankName;
		this.merchantName = merchantName;
		this.rif = rif;
		this.merchantIdCode = merchantIdCode;
		this.terminalBank = terminalBank;
		this.batchType = batchType;
		this.batchNumber = batchNumber;
		this.totalAmount = totalAmount;
		this.duplicate = duplicate;
	}

	private BigDecimal batchId;
	private BigDecimal terminalId;
	private String merchantGuid;
	private String bankName;
	private String merchantName;
	private String rif;
	private String merchantIdCode;
	private String terminalBank;
	private String batchType;
	private BigDecimal batchNumber;
	private BigDecimal totalAmount;
	private BigDecimal duplicate;
	
	public BigDecimal getBatchId() {
		return batchId;
	}
	
	public void setBatchId(BigDecimal batchId) {
		this.batchId = batchId;
	}
	public BigDecimal getTerminalId() {
		return terminalId;
	}
	
	public void setTerminalId(BigDecimal terminalId) {
		this.terminalId = terminalId;
	}
	
	public String getMerchantGuid() {
		return merchantGuid;
	}
	
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	public String getBankName() {
		return bankName;
	}
	
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public String getRif() {
		return rif;
	}
	
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	public String getMerchantIdCode() {
		return merchantIdCode;
	}
	
	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}
	
	public String getTerminalBank() {
		return terminalBank;
	}
	
	public void setTerminalBank(String terminalBank) {
		this.terminalBank = terminalBank;
	}
	
	public String getBatchType() {
		return batchType;
	}
	
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	
	public BigDecimal getBatchNumber() {
		return batchNumber;
	}
	
	public void setBatchNumber(BigDecimal batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public BigDecimal getDuplicate() {
		return duplicate;
	}
	
	public void setDuplicate(BigDecimal duplicate) {
		this.duplicate = duplicate;
	}

	@Override
	public String toString() {
		return "BatchHistoryConsultSynchronizedMin [batchId=" + batchId + ", terminalId=" + terminalId
				+ ", merchantGuid=" + merchantGuid + ", bankName=" + bankName + ", merchantName=" + merchantName
				+ ", rif=" + rif + ", merchantIdCode=" + merchantIdCode + ", terminalBank=" + terminalBank
				+ ", batchType=" + batchType + ", batchNumber=" + batchNumber + ", totalAmount=" + totalAmount
				+ ", duplicate=" + duplicate + "]";
	}
	
}
