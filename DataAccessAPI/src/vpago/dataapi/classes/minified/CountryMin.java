package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Pais  
 */
@XmlRootElement
@XmlType(name="simpleCountry", propOrder={"id", "countryName", "iso31661Code"})
public class CountryMin {
	
	public CountryMin() {
		super();
	}
	
	public CountryMin(int id, String countryName, String iso31661Code) {
		super();
		this.id = id;
		this.countryName = countryName;
		this.iso31661Code = iso31661Code;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del pais */
	private String countryName;
	/** Codigo ISO 3166-1 */
	private String iso31661Code;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the iso31661Code
	 */
	public String getIso31661Code() {
		return iso31661Code;
	}

	/**
	 * @param iso31661Code the iso31661Code to set
	 */
	public void setIso31661Code(String iso31661Code) {
		this.iso31661Code = iso31661Code;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountryMin [id=" + id + ", countryName=" + countryName + ", iso31661Code=" + iso31661Code + "]";
	}

}
