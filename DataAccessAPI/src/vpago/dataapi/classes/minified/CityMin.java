package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Ciudad  
 */
@XmlRootElement
@XmlType(name="simpleCity", propOrder={"id", "stateId", "cityName", "capital"})
public class CityMin extends APIResponse {
	
	public CityMin() {
		super();
	}
	
	public CityMin(int id, Integer stateId, String cityName, boolean capital) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.cityName = cityName;
		this.capital = capital;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del estado al que esta asociada la ciudad */
	private Integer stateId;
	/** Nombre de la ciudad */
	private String cityName;
	/** Indica si la ciudad es una capital */
	private boolean capital;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the capital
	 */
	public boolean isCapital() {
		return capital;
	}

	/**
	 * @param capital the capital to set
	 */
	public void setCapital(boolean capital) {
		this.capital = capital;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CityMin [id=" + id + ", stateId=" + stateId + ", cityName=" + cityName + ", capital=" + capital + "]";
	}
	
	
}
