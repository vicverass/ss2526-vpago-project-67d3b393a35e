package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Bin;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad producto general
 */
@XmlRootElement
@XmlType(name="simpleGeneralCardProduct", propOrder={
		"id", 
		"cardFranchiseId", 
		"cardFranchise", 
		"cardTypeId", 
		"cardType", 
		"productBin"})
public class GeneralCardProductMin extends APIResponse {
	
	public GeneralCardProductMin() {
		super();
	}
	
	public GeneralCardProductMin(int id, Integer cardFranchiseId, String cardFranchise, Integer cardTypeId,
			String cardType, Bin productBin) {
		super();
		this.id = id;
		this.cardFranchiseId = cardFranchiseId;
		this.cardFranchise = cardFranchise;
		this.cardTypeId = cardTypeId;
		this.cardType = cardType;
		this.productBin = productBin;
	}



	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Id de Franquicia de la tarjeta */
	protected Integer cardFranchiseId;
	/** Franquicia de la tarjeta */
	protected String cardFranchise;
	/** Id de Tipo de tarjeta */
	protected Integer cardTypeId;
	/** Tipo de tarjeta */
	protected String cardType;
	/** Bin de tarjeta */
	protected Bin productBin;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cardFranchiseId
	 */
	public Integer getCardFranchiseId() {
		return cardFranchiseId;
	}

	/**
	 * @param cardFranchiseId the cardFranchiseId to set
	 */
	public void setCardFranchiseId(Integer cardFranchiseId) {
		this.cardFranchiseId = cardFranchiseId;
	}

	/**
	 * @return the cardFranchise
	 */
	public String getCardFranchise() {
		return cardFranchise;
	}



	/**
	 * @param cardFranchise the cardFranchise to set
	 */
	public void setCardFranchise(String cardFranchise) {
		this.cardFranchise = cardFranchise;
	}

	/**
	 * @return the cardTypeId
	 */
	public Integer getCardTypeId() {
		return cardTypeId;
	}

	/**
	 * @param cardTypeId the cardTypeId to set
	 */
	public void setCardTypeId(Integer cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the productBin
	 */
	public Bin getProductBin() {
		return productBin;
	}

	/**
	 * @param productBin the productBin to set
	 */
	public void setProductBin(Bin productBin) {
		this.productBin = productBin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GeneralCardProductMin [id=" + id + ", cardFranchise=" + cardFranchise + ", cardType=" + cardType + "]";
	}
	
	

}
