package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.Transaction;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Transaction Parameters
 */
@XmlRootElement
@XmlType(propOrder={"id", "transaction", "paramName", "paramValue", "paramType" })
public class TransactionParametersMin {
	
	public TransactionParametersMin() {
		super();
	}	
	

	public TransactionParametersMin(int id, Transaction transaction, String paramName, String paramValue,
			String paramType) {
		super();
		this.id = id;
		this.transaction = transaction;
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.paramType = paramType;
	}


	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Transaccion a la que esta asociado el parametro */
	protected Transaction transaction;
	/** Nombre del parametro */
	protected String paramName;
	/** Valor del parametro */
	protected String paramValue;
	/** Tipo de parametro (solicitud o respuesta) */
	protected String paramType;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	@Override
	public String toString() {
		return "TransactionParametersMin [id=" + id + ", transaction=" + transaction + ", paramName=" + paramName
				+ ", paramValue=" + paramValue + ", paramType=" + paramType + "]";
	}

	
}
