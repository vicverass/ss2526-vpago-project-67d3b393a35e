package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Parroquia  
 */
@XmlRootElement
@XmlType(name="simpleParish", propOrder={"id", "municipalityId", "parishName"})
public class ParishMin extends APIResponse {
	
	public ParishMin() {
		super();
	}
	
	public ParishMin(int id, Integer municipalityId, String parishName) {
		super();
		this.id = id;
		this.municipalityId = municipalityId;
		this.parishName = parishName;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del municipio al que esta asociada la parroquia */
	private Integer municipalityId;
	/** Nombre de la parroquia */
	private String parishName;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the municipalityId
	 */
	public Integer getMunicipalityId() {
		return municipalityId;
	}

	/**
	 * @param municipalityId the municipalityId to set
	 */
	public void setMunicipalityId(Integer municipalityId) {
		this.municipalityId = municipalityId;
	}

	/**
	 * @return the parishName
	 */
	public String getParishName() {
		return parishName;
	}

	/**
	 * @param parishName the parishName to set
	 */
	public void setParishName(String parishName) {
		this.parishName = parishName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParishMin [id=" + id + ", municipalityId=" + municipalityId + ", parishName=" + parishName + "]";
	}

	
}
