package vpago.dataapi.classes.minified;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad TerminalSummary
 */
@XmlRootElement
@XmlType(name="terminalDevice", propOrder={"terminalId", "terminalGuid", "terminalBankId" ,"terminalBank", 
		"assignedDeviceId", "deviceId", "serialNumberDevice", "deviceName", "acquirerId",
		"active", "paymentChannelId", "paymentChannelName", "serialNumberSimCard", "operatorNameSimCard", 
		"merchantRif", "merchantName", "merchantGuid", "merchantIdCode", "bankName", "dayActive", "accumulatedAmount", "accumulatedTrx",
		"phone", "contact", "state", "city", "location", "property", "industryType", "merchantEmail",
		"dateAssignment", "startDateOperations", "serialUpdateDate", "lastTransactionDate", "averageTicket", "classification"})
public class TerminalDeviceMin {
	
	public TerminalDeviceMin() {
		super();
	}

	public TerminalDeviceMin(Integer terminalId, String terminalGuid, Integer terminalBankId, String terminalBank,
			Integer assignedDeviceId, Integer deviceId, String serialNumberDevice, String deviceName,
			Integer acquirerId, boolean active, Integer paymentChannelId, String paymentChannelName,
			String serialNumberSimCard, String operatorNameSimCard, String merchantRif, String merchantName,
			String merchantGuid, String merchantIdCode, String bankName, String dayActive, BigInteger accumulatedTrx,
			BigDecimal accumulatedAmount, String phone, String contact, String state, String city, String location,
			String property, String industryType, String merchantEmail, String dateAssignment,
			String startDateOperations, String serialUpdateDate, String lastTransactionDate, BigDecimal averageTicket,
			String classification) {
		super();
		this.terminalId = terminalId;
		this.terminalGuid = terminalGuid;
		this.terminalBankId = terminalBankId;
		this.terminalBank = terminalBank;
		this.assignedDeviceId = assignedDeviceId;
		this.deviceId = deviceId;
		this.serialNumberDevice = serialNumberDevice;
		this.deviceName = deviceName;
		this.acquirerId = acquirerId;
		this.active = active;
		this.paymentChannelId = paymentChannelId;
		this.paymentChannelName = paymentChannelName;
		this.serialNumberSimCard = serialNumberSimCard;
		this.operatorNameSimCard = operatorNameSimCard;
		this.merchantRif = merchantRif;
		this.merchantName = merchantName;
		this.merchantGuid = merchantGuid;
		this.merchantIdCode = merchantIdCode;
		this.bankName = bankName;
		this.dayActive = dayActive;
		this.accumulatedTrx = accumulatedTrx;
		this.accumulatedAmount = accumulatedAmount;
		this.phone = phone;
		this.contact = contact;
		this.state = state;
		this.city = city;
		this.location = location;
		this.property = property;
		this.industryType = industryType;
		this.merchantEmail = merchantEmail;
		this.dateAssignment = dateAssignment;
		this.startDateOperations = startDateOperations;
		this.serialUpdateDate = serialUpdateDate;
		this.lastTransactionDate = lastTransactionDate;
		this.averageTicket = averageTicket;
		this.classification = classification;
	}

	private Integer terminalId;
	private String terminalGuid;
	private Integer terminalBankId;
	private String terminalBank;
	private Integer assignedDeviceId;
	private Integer deviceId;
	private String serialNumberDevice;
	private String deviceName;
	private Integer acquirerId;
	private boolean active;
	private Integer paymentChannelId;
	private String paymentChannelName;
	private String serialNumberSimCard;
	private String operatorNameSimCard;
	private String merchantRif;
	private String merchantName;
	private String merchantGuid;
	private String merchantIdCode;
	private String bankName;
	private String dayActive;
	private BigInteger accumulatedTrx;
	private BigDecimal accumulatedAmount;
	private String phone;
	private String contact;
	private String state;
	private String city;
	private String location;
	private String property;
	private String industryType;
	private String merchantEmail;
	private String dateAssignment;
	private String startDateOperations;
	private String serialUpdateDate;
	private String lastTransactionDate;
	private BigDecimal averageTicket;
	private String classification;
	
	/**
	 * @return the terminalId
	 */
	public Integer getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the terminalGuid
	 */
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	public void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the terminalBankId
	 */
	public Integer getTerminalBankId() {
		return terminalBankId;
	}

	/**
	 * @param terminalBankId the terminalBankId to set
	 */
	public void setTerminalBankId(Integer terminalBankId) {
		this.terminalBankId = terminalBankId;
	}

	/**
	 * @return the terminalBank
	 */
	public String getTerminalBank() {
		return terminalBank;
	}

	/**
	 * @param terminalBank the terminalBank to set
	 */
	public void setTerminalBank(String terminalBank) {
		this.terminalBank = terminalBank;
	}

	/**
	 * @return the assignedDeviceId
	 */
	public Integer getAssignedDeviceId() {
		return assignedDeviceId;
	}

	/**
	 * @param assignedDeviceId the assignedDeviceId to set
	 */
	public void setAssignedDeviceId(Integer assignedDeviceId) {
		this.assignedDeviceId = assignedDeviceId;
	}

	/**
	 * @return the deviceId
	 */
	public Integer getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the serialNumberDevice
	 */
	public String getSerialNumberDevice() {
		return serialNumberDevice;
	}

	/**
	 * @param serialNumberDevice the serialNumberDevice to set
	 */
	public void setSerialNumberDevice(String serialNumberDevice) {
		this.serialNumberDevice = serialNumberDevice;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the acquirerId
	 */
	public Integer getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(Integer acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the paymentChannelId
	 */
	public Integer getPaymentChannelId() {
		return paymentChannelId;
	}

	/**
	 * @param paymentChannelId the paymentChannelId to set
	 */
	public void setPaymentChannelId(Integer paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}

	/**
	 * @return the paymentChannelName
	 */
	public String getPaymentChannelName() {
		return paymentChannelName;
	}

	/**
	 * @param paymentChannelName the paymentChannelName to set
	 */
	public void setPaymentChannelName(String paymentChannelName) {
		this.paymentChannelName = paymentChannelName;
	}

	/**
	 * @return the serialNumberSimCard
	 */
	public String getSerialNumberSimCard() {
		return serialNumberSimCard;
	}

	/**
	 * @param serialNumberSimCard the serialNumberSimCard to set
	 */
	public void setSerialNumberSimCard(String serialNumberSimCard) {
		this.serialNumberSimCard = serialNumberSimCard;
	}

	/**
	 * @return the operatorNameSimCard
	 */
	public String getOperatorNameSimCard() {
		return operatorNameSimCard;
	}

	/**
	 * @param operatorNameSimCard the operatorNameSimCard to set
	 */
	public void setOperatorNameSimCard(String operatorNameSimCard) {
		this.operatorNameSimCard = operatorNameSimCard;
	}

	/**
	 * @return the merchantRif
	 */
	public String getMerchantRif() {
		return merchantRif;
	}

	/**
	 * @param merchantRif the merchantRif to set
	 */
	public void setMerchantRif(String merchantRif) {
		this.merchantRif = merchantRif;
	}

	/**
	 * @return the merchantName
	 */
	public String getMerchantName() {
		return merchantName;
	}

	/**
	 * @param merchantName the merchantName to set
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	/**
	 * @return the merchantGuid
	 */
	public String getMerchantGuid() {
		return merchantGuid;
	}

	/**
	 * @param merchantGuid the merchantGuid to set
	 */
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}

	public String getMerchantIdCode() {
		return merchantIdCode;
	}

	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}
	
	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getDayActive() {
		return dayActive;
	}

	public void setDayActive(String dayActive) {
		this.dayActive = dayActive;
	}

	public BigInteger getAccumulatedTrx() {
		return accumulatedTrx;
	}

	public void setAccumulatedTrx(BigInteger accumulatedTrx) {
		this.accumulatedTrx = accumulatedTrx;
	}

	public BigDecimal getAccumulatedAmount() {
		return accumulatedAmount;
	}

	public void setAccumulatedAmount(BigDecimal accumulatedAmount) {
		this.accumulatedAmount = accumulatedAmount;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getMerchantEmail() {
		return merchantEmail;
	}

	public void setMerchantEmail(String merchantEmail) {
		this.merchantEmail = merchantEmail;
	}

	public String getDateAssignment() {
		return dateAssignment;
	}

	public void setDateAssignment(String dateAssignment) {
		this.dateAssignment = dateAssignment;
	}

	public String getStartDateOperations() {
		return startDateOperations;
	}

	public void setStartDateOperations(String startDateOperations) {
		this.startDateOperations = startDateOperations;
	}

	public String getSerialUpdateDate() {
		return serialUpdateDate;
	}

	public void setSerialUpdateDate(String serialUpdateDate) {
		this.serialUpdateDate = serialUpdateDate;
	}

	public String getLastTransactionDate() {
		return lastTransactionDate;
	}

	public void setLastTransactionDate(String lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	public BigDecimal getAverageTicket() {
		return averageTicket;
	}

	public void setAverageTicket(BigDecimal averageTicket) {
		this.averageTicket = averageTicket;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		return "TerminalDeviceMin [terminalId=" + terminalId + ", terminalGuid=" + terminalGuid + ", terminalBankId="
				+ terminalBankId + ", terminalBank=" + terminalBank + ", assignedDeviceId=" + assignedDeviceId
				+ ", deviceId=" + deviceId + ", serialNumberDevice=" + serialNumberDevice + ", deviceName=" + deviceName
				+ ", acquirerId=" + acquirerId + ", active=" + active + ", paymentChannelId=" + paymentChannelId
				+ ", paymentChannelName=" + paymentChannelName + ", serialNumberSimCard=" + serialNumberSimCard
				+ ", operatorNameSimCard=" + operatorNameSimCard + ", merchantRif=" + merchantRif + ", merchantName="
				+ merchantName + ", merchantGuid=" + merchantGuid + ", merchantIdCode=" + merchantIdCode + ", bankName="
				+ bankName + ", dayActive=" + dayActive + ", accumulatedTrx=" + accumulatedTrx + ", accumulatedAmount="
				+ accumulatedAmount + ", phone=" + phone + ", contact=" + contact + ", state=" + state + ", city="
				+ city + ", location=" + location + ", property=" + property + ", industryType=" + industryType
				+ ", merchantEmail=" + merchantEmail + ", dateAssignment=" + dateAssignment + ", startDateOperations="
				+ startDateOperations + ", serialUpdateDate=" + serialUpdateDate + ", lastTransactionDate="
				+ lastTransactionDate + ", averageTicket=" + averageTicket + ", classification=" + classification + "]";
	}

}
