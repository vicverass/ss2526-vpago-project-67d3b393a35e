package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Zona Postal  
 */
@XmlRootElement
@XmlType(name="simplePostalZona", propOrder={"id", "parishId", "townName", "postalCode"})
public class PostalZoneMin extends APIResponse {

	public PostalZoneMin() {
		super();
	}
	
	public PostalZoneMin(int id, Integer parishId, String townName, String postalCode) {
		super();
		this.id = id;
		this.parishId = parishId;
		this.townName = townName;
		this.postalCode = postalCode;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador de la parroquia a la que esta asociado el codigo postal */
	private Integer parishId;
	/** Nombre de la poblacion */
	private String townName;
	/** Codigo postal */
	private String postalCode;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the parishId
	 */
	public Integer getParishId() {
		return parishId;
	}

	/**
	 * @param parishId the parishId to set
	 */
	public void setParishId(Integer parishId) {
		this.parishId = parishId;
	}

	/**
	 * @return the townName
	 */
	public String getTownName() {
		return townName;
	}

	/**
	 * @param townName the townName to set
	 */
	public void setTownName(String townName) {
		this.townName = townName;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostalZoneMin [id=" + id + ", parishId=" + parishId + ", townName=" + townName + ", postalCode="
				+ postalCode + "]";
	}
	
	
}
