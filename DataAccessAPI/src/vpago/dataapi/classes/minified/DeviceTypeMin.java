package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Tipo de Dispositivo
 */
@XmlRootElement
@XmlType(name="simpleDeviceType", propOrder={"id", "name"})
public class DeviceTypeMin {
	
	public DeviceTypeMin() {
		super();
	}
	
	public DeviceTypeMin(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre del Tipo de Dispositivo */
	private String name;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
