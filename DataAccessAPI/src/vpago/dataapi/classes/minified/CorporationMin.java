package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Corporacion
 */
@XmlRootElement
@XmlType(name="simpleCorporation", propOrder={"id", "name", "fantasyName", "rif"})
public class CorporationMin {
	
	public CorporationMin() {
		super();
	}
	
	public CorporationMin(int id, String name, String fantasyName, String rif) {
		super();
		this.id = id;
		this.name = name;
		this.fantasyName = fantasyName;
		this.rif = rif;
	}
	
	/** Identificador generado por la base de datos */
	protected int id;
	/** Nombre o razon social */
	protected String name;
	/** Nombre fantasia */
	protected String fantasyName;
	/** Registro de identificacion fiscal */
	protected String rif;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fantasyName
	 */
	public String getFantasyName() {
		return fantasyName;
	}

	/**
	 * @param fantasyName the fantasyName to set
	 */
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	/**
	 * @return the rif
	 */
	public String getRif() {
		return rif;
	}

	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CorporationMin [id=" + id + ", name=" + name + ", fantasyName=" + fantasyName + ", rif=" + rif + "]";
	}
	
}
