package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.QueryException;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Terminal
 */
@XmlRootElement
@XmlType(propOrder={"datePeriod", "average"})
public class DashboardLatestTicketMin {
	
	public DashboardLatestTicketMin() {
		super();
	}
	
	/*Constructor con el promedio*/
	public DashboardLatestTicketMin(double average) {
		super();
		this.average = average;		
	}
	
	private String datePeriod;
	private double average;
	
	public String getDatePeriod() {
		return datePeriod;
	}
	public void setDatePeriod(String datePeriod) {
		this.datePeriod = datePeriod;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}

	@Override
	public String toString() {
		return "DashboardLatestTicketMin [datePeriod=" + datePeriod + ", average=" + average + "]";
	}
	
}
