package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad TerminalSummary
 */
@XmlRootElement
@XmlType(name="terminalSummary", propOrder={"terminalId", "terminalGuid", "terminalBankId" ,"terminalBank", 
		"assignedDeviceId", "deviceId", "serialNumberDevice", "deviceName", "acquirerId",
		"active", "paymentChannelId", "paymentChannelName", "serialNumberSimCard", "operatorNameSimCard"})
public class TerminalSummaryMin {
	
	public TerminalSummaryMin() {
		super();
	}
	
	public TerminalSummaryMin(Integer terminalId, String terminalGuid, Integer terminalBankId, String terminalBank,
			Integer assignedDeviceId, Integer deviceId, String serialNumberDevice, String deviceName,
			Integer acquirerId, boolean active, Integer paymentChannelId, String paymentChannelName,
			String serialNumberSimCard, String operatorNameSimCard) {
		super();
		this.terminalId = terminalId;
		this.terminalGuid = terminalGuid;
		this.terminalBankId = terminalBankId;
		this.terminalBank = terminalBank;
		this.assignedDeviceId = assignedDeviceId;
		this.deviceId = deviceId;
		this.serialNumberDevice = serialNumberDevice;
		this.deviceName = deviceName;
		this.acquirerId = acquirerId;
		this.active = active;
		this.paymentChannelId = paymentChannelId;
		this.paymentChannelName = paymentChannelName;
		this.serialNumberSimCard = serialNumberSimCard;
		this.operatorNameSimCard = operatorNameSimCard;
	}

	private Integer terminalId;
	private String terminalGuid;
	private Integer terminalBankId;
	private String terminalBank;
	private Integer assignedDeviceId;
	private Integer deviceId;
	private String serialNumberDevice;
	private String deviceName;
	private Integer acquirerId;
	private boolean active;
	private Integer paymentChannelId;
	private String paymentChannelName;
	private String serialNumberSimCard;
	private String operatorNameSimCard;
	
	public Integer getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}
	public String getTerminalGuid() {
		return terminalGuid;
	}
	public void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}
	public Integer getTerminalBankId() {
		return terminalBankId;
	}
	public void setTerminalBankId(Integer terminalBankId) {
		this.terminalBankId = terminalBankId;
	}
	public String getTerminalBank() {
		return terminalBank;
	}
	public void setTerminalBank(String terminalBank) {
		this.terminalBank = terminalBank;
	}
	public Integer getAssignedDeviceId() {
		return assignedDeviceId;
	}
	public void setAssignedDeviceId(Integer assignedDeviceId) {
		this.assignedDeviceId = assignedDeviceId;
	}
	public Integer getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	public String getSerialNumberDevice() {
		return serialNumberDevice;
	}
	public void setSerialNumberDevice(String serialNumberDevice) {
		this.serialNumberDevice = serialNumberDevice;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public Integer getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(Integer acquirerId) {
		this.acquirerId = acquirerId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Integer getPaymentChannelId() {
		return paymentChannelId;
	}

	public void setPaymentChannelId(Integer paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}

	public String getPaymentChannelName() {
		return paymentChannelName;
	}

	public void setPaymentChannelName(String paymentChannelName) {
		this.paymentChannelName = paymentChannelName;
	}

	public String getSerialNumberSimCard() {
		return serialNumberSimCard;
	}

	public void setSerialNumberSimCard(String serialNumberSimCard) {
		this.serialNumberSimCard = serialNumberSimCard;
	}

	public String getOperatorNameSimCard() {
		return operatorNameSimCard;
	}

	public void setOperatorNameSimCard(String operatorNameSimCard) {
		this.operatorNameSimCard = operatorNameSimCard;
	}

	@Override
	public String toString() {
		return "TerminalSummaryMin [terminalId=" + terminalId + ", terminalGuid=" + terminalGuid + ", terminalBankId="
				+ terminalBankId + ", terminalBank=" + terminalBank + ", assignedDeviceId=" + assignedDeviceId
				+ ", deviceId=" + deviceId + ", serialNumberDevice=" + serialNumberDevice + ", deviceName=" + deviceName
				+ ", acquirerId=" + acquirerId + ", active=" + active + ", paymentChannelId=" + paymentChannelId
				+ ", paymentChannelName=" + paymentChannelName + ", serialNumberSimCard=" + serialNumberSimCard
				+ ", operatorNameSimCard=" + operatorNameSimCard + "]";
	}

	
}
