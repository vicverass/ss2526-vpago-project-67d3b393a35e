package vpago.dataapi.classes.minified;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Regla
 */
@XmlRootElement
@XmlType(name="simpleRule", propOrder={"id", "displayName", "acquirerId", "ruleType"})
public class RuleMin {
	
	public RuleMin() {
		super();
	}
	
	public RuleMin(int id, String displayName, int acquirerId, String ruleType) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.acquirerId = acquirerId;
		this.ruleType = ruleType;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Identificador del Adquiriente */
	private int acquirerId;
	/** Tipo de Regla */
	private String ruleType;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the acquirerId
	 */
	@Column(name="aquirer_id")
	public int getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(int acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the ruleType
	 */
	@Column(name="rule_type", insertable=false, updatable=false)
	public String getRuleType() {
		return ruleType;
	}

	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleMin [id=" + id + ", displayName=" + displayName + ", acquirerId=" + acquirerId + ", ruleType="
				+ ruleType + "]";
	}
	
}
