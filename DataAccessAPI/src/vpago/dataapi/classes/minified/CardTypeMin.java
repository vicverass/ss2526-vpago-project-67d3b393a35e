package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de tipo de tarjetas
 */
@XmlRootElement
@XmlType(name="simpleCardType", propOrder={"id", "name"})
public class CardTypeMin {


	public CardTypeMin() {
		super();
	}
		
	public CardTypeMin (int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String name;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "CardTypeMin [id=" + id + ", name=" + name + "]";
	}
}
