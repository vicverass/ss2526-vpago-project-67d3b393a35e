package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos para la consulta de los lotes cerrados
 */
@XmlRootElement
@XmlType(name="TerminalAffiliationMin", propOrder={"terminalParameterId", "parameterValue"})
public class TerminalAffiliationMin {
	
	public TerminalAffiliationMin() {
		super();
	}	

	public TerminalAffiliationMin(Integer terminalParameterId, String parameterValue) {
		super();
		this.terminalParameterId = terminalParameterId;
		this.parameterValue = parameterValue;
	}

	private Integer terminalParameterId;
	private String parameterValue;
	/**
	 * @return the terminalParameterId
	 */
	public Integer getTerminalParameterId() {
		return terminalParameterId;
	}
	/**
	 * @param terminalParameterId the terminalParameterId to set
	 */
	public void setTerminalParameterId(Integer terminalParameterId) {
		this.terminalParameterId = terminalParameterId;
	}
	/**
	 * @return the parameterValue
	 */
	public String getParameterValue() {
		return parameterValue;
	}
	/**
	 * @param parameterValue the parameterValue to set
	 */
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalAffiliationMin [terminalParameterId=" + terminalParameterId + ", parameterValue="
				+ parameterValue + "]";
	}
	
}
