package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Municipio  
 */
@XmlRootElement
@XmlType(name="simpleMunicipality", propOrder={"id", "stateId", "municipalityName"})
public class MunicipalityMin extends APIResponse {
	
	public MunicipalityMin() {
		super();
	}
	
	public MunicipalityMin(int id, Integer stateId, String municipalityName) {
		super();
		this.id = id;
		this.stateId = stateId;
		this.municipalityName = municipalityName;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del estado al que esta asociado el municipio */
	private Integer stateId;
	/** Nombre del municipio */
	private String municipalityName;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the stateId
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the municipalityName
	 */
	public String getMunicipalityName() {
		return municipalityName;
	}

	/**
	 * @param municipalityName the municipalityName to set
	 */
	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MunicipalityMin [id=" + id + ", stateId=" + stateId + ", municipalityName=" + municipalityName + "]";
	}
	
	
}
