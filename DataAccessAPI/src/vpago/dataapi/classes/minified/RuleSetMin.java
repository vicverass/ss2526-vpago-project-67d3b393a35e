package vpago.dataapi.classes.minified;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Conjunto de Reglas
 */
@XmlRootElement
@XmlType(name="simpleRuleSet", propOrder={"id", "displayName", "description", "affiliationId"})
public class RuleSetMin {
	
	public RuleSetMin() {
		super();
	}
	
	public RuleSetMin(int id, String displayName, String description, int affiliationId) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.description = description;
		this.affiliationId = affiliationId;
	}

	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Descripcion */
	private String description;
	/** Identificador del Comercio al que esta asociado el Conjunto de Reglas */
	private int affiliationId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleSetMin [id=" + id + ", displayName=" + displayName + ", description=" + description
				+ ", affiliationId=" + affiliationId + "]";
	}

}
