package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;

@XmlRootElement
@XmlType(propOrder={"merchantGuid", "bankId", "totalAmount", "terminalBank", "batchNumber"})
public class BatchHistorySynchronizedMin extends APIResponse {

	public BatchHistorySynchronizedMin() {
		super();
	}
	
	public BatchHistorySynchronizedMin(String merchantGuid, Integer bankId, double totalAmount, String terminalBank,
			Integer batchNumber) {
		super();
		this.merchantGuid = merchantGuid;
		this.bankId = bankId;
		this.totalAmount = totalAmount;
		this.terminalBank = terminalBank;
		this.batchNumber = batchNumber;
	}

	private String merchantGuid;
	private Integer bankId;
	private double totalAmount;
	private String terminalBank;
	private Integer batchNumber;
	
	public String getMerchantGuid() {
		return merchantGuid;
	}
	
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	public Integer getBankId() {
		return bankId;
	}
	
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTerminalBank() {
		return terminalBank;
	}

	public void setTerminalBank(String terminalBank) {
		this.terminalBank = terminalBank;
	}

	public Integer getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(Integer batchNumber) {
		this.batchNumber = batchNumber;
	}

	@Override
	public String toString() {
		return "BatchHistorySynchronized [merchantGuid=" + merchantGuid + ", bankId=" + bankId + ", totalAmount="
				+ totalAmount + ", terminalBank=" + terminalBank + ", batchNumber=" + batchNumber + "]";
	}	
	
}
