package vpago.dataapi.classes.minified;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos para la consulta de los lotes cerrados
 */
@XmlRootElement
@XmlType(name="batchConsultMin", propOrder={"id", "affiliateNumber", "batchNumber", "terminalBank", "batchType", "batchDate",
		"paymentChannel", "totalTrx", "bankId", "bank", "merchantId", "merchantGuid", "merchantRif", "merchantName", "totalAmount"})
public class BatchConsultMin {
	
	public BatchConsultMin() {
		super();
	}	

	public BatchConsultMin(Integer id, String affiliateNumber, Integer batchNumber, String terminalBank,
			String batchType, String batchDate, String paymentChannel, Integer totalTrx, Integer bankId, String bank,
			Integer merchantId, String merchantGuid, String merchantRif, String merchantName, double totalAmount) {
		super();
		this.id = id;
		this.affiliateNumber = affiliateNumber;
		this.batchNumber = batchNumber;
		this.terminalBank = terminalBank;
		this.batchType = batchType;
		this.batchDate = batchDate;
		this.paymentChannel = paymentChannel;
		this.totalTrx = totalTrx;
		this.bankId = bankId;
		this.bank = bank;
		this.merchantId = merchantId;
		this.merchantGuid = merchantGuid;
		this.merchantRif = merchantRif;
		this.merchantName = merchantName;
		this.totalAmount = totalAmount;
	}

	private Integer id;
	private String affiliateNumber;
	private Integer batchNumber;
	private String terminalBank;
	private String batchType;
	private String batchDate;
	private String paymentChannel;
	private Integer totalTrx;
	private Integer bankId;
	private String bank;
	private Integer merchantId;
	private String merchantGuid;
	private String merchantRif;
	private String merchantName;
	private double totalAmount;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getAffiliateNumber() {
		return affiliateNumber;
	}
	
	public void setAffiliateNumber(String affiliateNumber) {
		this.affiliateNumber = affiliateNumber;
	}
	
	public Integer getBatchNumber() {
		return batchNumber;
	}
	
	public void setBatchNumber(Integer batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	public String getTerminalBank() {
		return terminalBank;
	}
	
	public void setTerminalBank(String terminalBank) {
		this.terminalBank = terminalBank;
	}
	
	public String getBatchType() {
		return batchType;
	}
	
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	
	public String getPaymentChannel() {
		return paymentChannel;
	}
	
	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	public Integer getTotalTrx() {
		return totalTrx;
	}
	
	public void setTotalTrx(Integer totalTrx) {
		this.totalTrx = totalTrx;
	}
	
	public Integer getBankId() {
		return bankId;
	}
	
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	
	public String getBank() {
		return bank;
	}
	
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	public Integer getMerchantId() {
		return merchantId;
	}
	
	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	
	public String getMerchantGuid() {
		return merchantGuid;
	}
	
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	public String getMerchantRif() {
		return merchantRif;
	}

	public void setMerchantRif(String merchantRif) {
		this.merchantRif = merchantRif;
	}

	public String getMerchantName() {
		return merchantName;
	}
	
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public String getBatchDate() {
		return batchDate;
	}
	
	public void setBatchDate(String batchDate) {
		this.batchDate = batchDate;
	}
	
	@Override
	public String toString() {
		return "BatchConsultMin [id=" + id + ", affiliateNumber=" + affiliateNumber + ", batchNumber=" + batchNumber
				+ ", terminalBank=" + terminalBank + ", batchType=" + batchType + ", batchDate=" + batchDate
				+ ", paymentChannel=" + paymentChannel + ", totalTrx=" + totalTrx + ", bankId=" + bankId + ", bank="
				+ bank + ", merchantId=" + merchantId + ", merchantGuid=" + merchantGuid + ", merchantRif="
				+ merchantRif + ", merchantName=" + merchantName + ", totalAmount=" + totalAmount + "]";
	}
}
