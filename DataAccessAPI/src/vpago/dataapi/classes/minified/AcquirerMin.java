package vpago.dataapi.classes.minified;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Adquiriente
 */
@XmlRootElement
@XmlType(name="simpleAcquirer", propOrder={"id", "displayName", "bankId", "affiliationId", "merchantId", "merchantIdCode"})
public class AcquirerMin {
	
	public AcquirerMin() {
		super();
	}

	public AcquirerMin(int id, String displayName, int bankId, int affiliationId) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.bankId = bankId;
		this.affiliationId = affiliationId;
		this.merchantId = null;
	}
	
	public AcquirerMin(int id, String displayName, int bankId, int affiliationId, Integer merchantId, String merchantIdCode) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.bankId = bankId;
		this.affiliationId = affiliationId;
		this.merchantId = merchantId;
		this.merchantIdCode = merchantIdCode;
	}
	
	public AcquirerMin(int id, String displayName, int bankId, int affiliationId, String merchantIdCode) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.bankId = bankId;
		this.affiliationId = affiliationId;
		this.merchantId = null;
		this.merchantIdCode = merchantIdCode;
	}

	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Identificador del Banco asociado al Adquiriente */
	private int bankId;
	/** Identificador de la Afiliacion a la que esta asociado el Adquiriente */
	private int affiliationId;
	/** Identificador del Comercio al que pertenece el Adquiriente */
	private Integer merchantId;
	private String merchantIdCode;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the bankId
	 */
	@Column(name="bank_id")
	public int getBankId() {
		return bankId;
	}

	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/**
	 * @return the merchantId
	 */
	public Integer getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(Integer merchantId) {
		this.merchantId = merchantId;
	}
	
	public String getMerchantIdCode() {
		return merchantIdCode;
	}

	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}

	@Override
	public String toString() {
		return "AcquirerMin [id=" + id + ", displayName=" + displayName + ", bankId=" + bankId + ", affiliationId="
				+ affiliationId + ", merchantId=" + merchantId + ", merchantIdCode=" + merchantIdCode + "]";
	}

	
}
