package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Banco
 */
@XmlRootElement
@XmlType(name="simpleBank", propOrder={"id", "name", "rif"})
public class BankMin {

	public BankMin() {
		super();
	}
	
	public BankMin(int id, String name, String rif) {
		super();
		this.id = id;
		this.name = name;
		this.rif = rif;
	}

	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String name;
	/** Registro de Informacion Fiscal */
	private String rif;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rif
	 */
	public String getRif() {
		return rif;
	}

	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BankMin [id=" + id + ", name=" + name + ", rif=" + rif + "]";
	}
	
}
