package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.Affiliation;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Afiliacion
 */
@XmlRootElement
@XmlType(name="simpleAffiliation", propOrder={"id", "paymentChannelId", "paymentChannelName", "active"})
public class AffiliationMin {

	public AffiliationMin() {
		super();
	}
	
	public AffiliationMin(int id, Affiliation affiliation, boolean active) {
		super();
		this.id = id;
		this.paymentChannelId = affiliation.getPaymentChannelId();
		this.paymentChannelName = affiliation.getPaymentChannel().getName();
		this.active = active;
	}
	
	private int id;
	/** Identificador del Canal de Pago al cual esta afiliado el Comercio */
	private Integer paymentChannelId;
	/** Canal de pago al cual esta afiliado el comercio **/
	private String paymentChannelName;
	/** Indica si el Comercio esta activo para este Canal de Pago */
	private boolean active;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the paymentChannelId
	 */
	public Integer getPaymentChannelId() {
		return paymentChannelId;
	}

	/**
	 * @param paymentChannelId the paymentChannelId to set
	 */
	public void setPaymentChannelId(Integer paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}
	
	

	/**
	 * @return the paymentChannelName
	 */
	public String getPaymentChannelName() {
		return paymentChannelName;
	}

	/**
	 * @param paymentChannelName the paymentChannelName to set
	 */
	public void setPaymentChannelName(String paymentChannelName) {
		this.paymentChannelName = paymentChannelName;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
