package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Terminal
 */
@XmlRootElement
@XmlType(propOrder={"name", "countMerchant"})
public class DashboardMerchantByAcquirerMin {
	
	public DashboardMerchantByAcquirerMin() {
		super();
	}
	
	
	public DashboardMerchantByAcquirerMin(String name, Long countMerchant) {
		super();
		this.name = name;
		this.countMerchant = countMerchant;
	}

	private String name;
	private Long countMerchant;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCountMerchant() {
		return countMerchant;
	}
	public void setCountMerchant(Long countMerchant) {
		this.countMerchant = countMerchant;
	}

	@Override
	public String toString() {
		return "DashboardMerchantByAcquirerMin [name=" + name + ", countMerchant=" + countMerchant + "]";
	}
	
}
