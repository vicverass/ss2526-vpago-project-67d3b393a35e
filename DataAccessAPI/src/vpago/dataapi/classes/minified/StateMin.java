package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Estado  
 */
@XmlRootElement
@XmlType(name="simpleState", propOrder={"id", "countryId", "stateName", "iso31662Code"})
public class StateMin {
	
	public StateMin() {
		super();
	}
	
	public StateMin(int id, Integer countryId, String stateName, String iso31662Code) {
		super();
		this.id = id;
		this.countryId = countryId;
		this.stateName = stateName;
		this.iso31662Code = iso31662Code;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del pais al que esta asociado el estado */
	private Integer countryId;
	/** Nombre del estado */
	private String stateName;
	/** Codigo ISO 3166-2 */
	private String iso31662Code;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the countryId
	 */
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the iso31662Code
	 */
	public String getIso31662Code() {
		return iso31662Code;
	}

	/**
	 * @param iso31662Code the iso31662Code to set
	 */
	public void setIso31662Code(String iso31662Code) {
		this.iso31662Code = iso31662Code;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StateMin [id=" + id + ", countryId=" + countryId + ", stateName=" + stateName + ", iso31662Code="
				+ iso31662Code + "]";
	}

}
