package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Dispositivo
 */
@XmlRootElement
@XmlType(name="simpleDevice", propOrder={"id", "displayName", "deviceTypeId"})
public class DeviceMin {

	public DeviceMin() {
		super();
	}

	public DeviceMin(int id, String displayName, Integer deviceTypeId) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.deviceTypeId = deviceTypeId;
	}

	/** Identificador generado por el motor de base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Tipo de Dispositivo */
	private Integer deviceTypeId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	/**
	 * @return the deviceTypeId
	 */
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	
	/**
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
}
