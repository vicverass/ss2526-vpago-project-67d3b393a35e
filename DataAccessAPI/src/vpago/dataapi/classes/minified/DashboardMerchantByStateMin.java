package vpago.dataapi.classes.minified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase utilitaria para mostrar un subconjunto de los datos de la entidad Terminal
 */
@XmlRootElement
@XmlType(propOrder={"stateName", "countMerchant"})
public class DashboardMerchantByStateMin {
	
	public DashboardMerchantByStateMin() {
		super();
	}
	
	
	public DashboardMerchantByStateMin(String stateName, Long countMerchant) {
		super();
		this.stateName = stateName;
		this.countMerchant = countMerchant;
	}

	private String stateName;
	private Long countMerchant;
	
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public Long getCountMerchant() {
		return countMerchant;
	}
	public void setCountMerchant(Long countMerchant) {
		this.countMerchant = countMerchant;
	}

	@Override
	public String toString() {
		return "DashboardMerchantByStateMin [stateName=" + stateName + ", countMerchant=" + countMerchant + "]";
	}
	
}
