package vpago.dataapi.classes;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Conjunto de Reglas
 */
@XmlRootElement
@XmlType(propOrder={"id", "displayName", "description", "timeFrame", "affiliationId"})
@Entity
@Table(name="rule_set")
public class RuleSet extends APIResponse {
	
	public RuleSet() {
		super();
	}
	
	public RuleSet(String displayName, String description, TimeFrame timeFrame, Affiliation affiliation) {
		super();
		this.displayName = displayName;
		this.description = description;
		this.timeFrame = timeFrame;
		this.affiliationId = affiliation.getId();
		this.affiliation = affiliation;
		this.rules = new HashSet<Rule>();
	}
	
	public void update(RuleSet other) {
		this.displayName = other.displayName;
		this.description = other.description;
		this.timeFrame = other.timeFrame;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Descripcion */
	private String description;
	/** Rango de tiempo */
	protected TimeFrame timeFrame;
	/** Identificador de la Afiliacion a la que esta asociado el Conjunto de Reglas */
	private int affiliationId;
	/** Afiliacion a la que esta asociado el Conjunto de Reglas */
	private Affiliation affiliation;
	/** Reglas asociadas al Conjunto de Reglas */
	private Set<Rule> rules;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="rule_set_id_seq")
	@SequenceGenerator(name="rule_set_id_seq", sequenceName = "rule_set_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the timeFrame
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "time_frame_id")
	public TimeFrame getTimeFrame() {
		return timeFrame;
	}
	
	/**
	 * @param timeFrame the timeFrame to set
	 */
	public void setTimeFrame(TimeFrame timeFrame) {
		this.timeFrame = timeFrame;
	}
	
	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/**
	 * @return the affiliation
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		if(affiliation != null) { this.affiliationId = affiliation.getId(); }
		this.affiliation = affiliation;
	}

	/**
	 * @return the rules
	 */
	@XmlTransient
	@ManyToMany(
		targetEntity=Rule.class,
		cascade={CascadeType.PERSIST, CascadeType.MERGE}
	)
	@JoinTable(
		name="rule_set_rule",
		joinColumns= @JoinColumn(name="rule_set_id"),
		inverseJoinColumns=@JoinColumn(name="rule_id")
	)
	public Set<Rule> getRules() {
		return rules;
	}
	
	/**
	 * @param rules the rules to set
	 */
	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}
	
	public void addRule(Rule rule) {
		this.rules.add(rule);
	}
	
	public void deleteRule(Rule rule) {
		this.rules.remove(rule);
	}
	
	public boolean hasRule(Rule rule) {
		return this.rules.contains(rule);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + id;
		result = prime * result + ((affiliation == null) ? 0 : affiliation.hashCode());
		result = prime * result + affiliationId;
		result = prime * result + ((rules == null) ? 0 : rules.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleSet other = (RuleSet) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (id != other.id)
			return false;
		if (affiliation == null) {
			if (other.affiliation != null)
				return false;
		} else if (!affiliation.equals(other.affiliation))
			return false;
		if (affiliationId != other.affiliationId)
			return false;
		if (rules == null) {
			if (other.rules != null)
				return false;
		} else if (!rules.equals(other.rules))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleSet [id=" + id + ", displayName=" + displayName + ", description=" + description
				+ ", affiliationId=" + affiliationId + "]";
	}
	
}
