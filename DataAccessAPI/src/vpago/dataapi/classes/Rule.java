package vpago.dataapi.classes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.persistence.DiscriminatorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Regla
 */
@XmlRootElement
@XmlType(propOrder={"id", "displayName", "description", "acquirerId", "affiliationId"})
@XmlSeeAlso({RuleProduct.class, RuleProductGeneral.class, RuleProportion.class})
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="rule_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("rule")
public class Rule extends APIResponse {
	
	public Rule() {
		super();
	}
	
	public Rule(String displayName, String description, Acquirer acquirer,
			Affiliation affiliation) {
		super();
		this.displayName = displayName;
		this.description = description;
		this.acquirerId = acquirer.getId();
		this.acquirer = acquirer;
		this.affiliationId = affiliation.getId();
		this.affiliation = affiliation;
	}
	
	public void update(Rule other) {
		this.displayName = other.displayName;
		this.description = other.description;
	}

	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Nombre para mostrar */
	protected String displayName;
	/** Descripcion */
	protected String description;
	/** Identificador de Adquiriente */
	protected int acquirerId;
	/** Adquiriente */
	protected Acquirer acquirer;
	/** Identificador de la afiliacion a la que esta asociada la Regla */
	protected int affiliationId;
	/** Afiliacion a la que esta asociada la Regla */
	protected Affiliation affiliation;
	/** Conjunto de Reglas a los que esta asociada la Regla */
	protected Set<RuleSet> ruleSets;
	/** Tipo de Regla */
	private String ruleType;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="rule_id_seq")
	@SequenceGenerator(name="rule_id_seq", sequenceName = "rule_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	@NotNull
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	/**
	 * @return the acquirerId
	 */
	@Column(name="acquirer_id")
	@NotNull
	public int getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(int acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the acquirer
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "acquirer_id", insertable=false, updatable=false)
	public Acquirer getAcquirer() {
		return acquirer;
	}

	/**
	 * @param acquirer the acquirer to set
	 */
	public void setAcquirer(Acquirer acquirer) {
		this.acquirer = acquirer;
		if(acquirer != null) this.acquirerId = acquirer.getId();
	}

	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/**
	 * @return the affiliation
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * @return the ruleSets
	 */
	@XmlTransient
	@ManyToMany(
		cascade = {CascadeType.PERSIST, CascadeType.MERGE},
		mappedBy = "rules",
		targetEntity = RuleSet.class
	)
	public Set<RuleSet> getRuleSets() {
		return ruleSets;
	}
	
	/**
	 * @param ruleSets the ruleSets to set
	 */
	public void setRuleSets(Set<RuleSet> ruleSets) {
		this.ruleSets = ruleSets;
	}

	/**
	 * @return the ruleType
	 */
	@XmlTransient
	@Column(name = "rule_type", insertable=false, updatable=false)
	public String getRuleType() {
		return ruleType;
	}

	/**
	 * @param ruleType the ruleType to set
	 */
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acquirer == null) ? 0 : acquirer.hashCode());
		result = prime * result + acquirerId;
		result = prime * result + ((affiliation == null) ? 0 : affiliation.hashCode());
		result = prime * result + affiliationId;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + id;
		result = prime * result + ((ruleType == null) ? 0 : ruleType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rule other = (Rule) obj;
		if (acquirer == null) {
			if (other.acquirer != null)
				return false;
		} else if (!acquirer.equals(other.acquirer))
			return false;
		if (acquirerId != other.acquirerId)
			return false;
		if (affiliation == null) {
			if (other.affiliation != null)
				return false;
		} else if (!affiliation.equals(other.affiliation))
			return false;
		if (affiliationId != other.affiliationId)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (id != other.id)
			return false;
		if (ruleType == null) {
			if (other.ruleType != null)
				return false;
		} else if (!ruleType.equals(other.ruleType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Rule [id=" + id + ", displayName=" + displayName + ", description=" + description + ", acquirerId=" + acquirerId + ", affiliationId=" + affiliationId + ", ruleType="
				+ ruleType + "]";
	}
	
}
