package vpago.dataapi.classes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.types.TransactionStatus;



/**
 * Clase persistente para representar una Transaccion
 */
@XmlRootElement
@XmlType(propOrder={
		"id", "hashCode", "terminalId", 
		"terminalBankId", "merchantIdCode", "amount", 
		"txnDate", "controlToken", "transactionStatus", 
		"transactionTypeId", "transactionResponseId", "cardProductId", 
		"retried", "retryAttempts", "secToken"})
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Table(name="transaction")
public class Transaction extends APIResponse {

	public Transaction() {
		super(); 
	}
	
	public Transaction(String hashCode, Terminal terminalId, String terminalBankId, String merchantIdCode,
			double amount, Calendar txnDate, String controlToken, Integer transactionStatus, TransactionType transactionTypeId,
			Integer transactionResponseId, Integer cardProductId, boolean retried, Integer retryAttempts,
			String secToken) {
		super();
		this.id = null;
		this.hashCode = hashCode;
		this.terminalId = terminalId;
		this.terminalBankId = terminalBankId;
		this.merchantIdCode = merchantIdCode;
		this.amount = amount;
		this.txnDate = txnDate;
		this.controlToken = controlToken;
		this.transactionStatus = transactionStatus;
		this.transactionTypeId = transactionTypeId;
		this.transactionResponseId = transactionResponseId;
		this.cardProductId = cardProductId;
		this.retried = retried;
		this.retryAttempts = retryAttempts;
		this.secToken = secToken;
	}

	/** Identificador generado por el motor base de datos */
	private Integer id;
	/** Identificador unico de la transaccion */
	private String hashCode;
	/** Identificador del terminal */
	private Terminal terminalId;
	/** Identificador del terminal bancario */
	private String terminalBankId;
	/** Identificador del comercio */
	private String merchantIdCode;
	/** Monto tranzado */
	private double amount;
	/** Fecha de la transaccion */
	private Calendar txnDate;
	/** Numero del token de control */
	private String controlToken;
	/** Estado de la transaccion */
	private Integer transactionStatus;
	/** Identificador del tipo de transaccion */
	private TransactionType transactionTypeId;
	/** Identificador de la respuesta */
	private Integer transactionResponseId;
	/** Identificador del producto */
	private Integer cardProductId;
	/** Reintentos */
	private boolean retried;
	/** Numero de reintentos */
	private int retryAttempts;
	/** Secuencia de tokens */
	private String secToken;
	/** Conjunto de parametros asociados (solicitud) */
	private Set<TransactionParameter> parameters; 
	/** Conjunto de parametros asociados (respuesta) */
	//private Set<TransactionResponseParameter> responseParameters;
	/**
	 * 
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_id_seq")
	@SequenceGenerator(name="transaction_id_seq", sequenceName = "transaction_id_seq")
	public Integer getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the hashCode
	 */
	@XmlElement
	@Column(name="hash_code")
	public String getHashCode() {
		return hashCode;
	}

	/**
	 * 
	 * @param hashCode
	 */
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	/**
	 * 
	 * @return the terminalId
	 */
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminalId() {
		return terminalId;
	}

	/**
	 * 
	 * @param terminalId
	 */
	public void setTerminalId(Terminal terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * 
	 * @return the terminalBankId
	 */
	@Column(name="terminal_bank_id")
	public String getTerminalBankId() {
		return terminalBankId;
	}

	/**
	 * 
	 * @param terminalBankId
	 */
	public void setTerminalBankId(String terminalBankId) {
		this.terminalBankId = terminalBankId;
	}

	/**
	 * 
	 * @return the merchantIdCode
	 */
	@Column(name="merchant_id_code")
	public String getMerchantIdCode() {
		return merchantIdCode;
	}

	/**
	 * 
	 * @param merchantIdCode
	 */
	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}

	/**	
	 * 
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	/**
	 * 
	 * @return the txnDate
	 */
	@Column(name="txn_date")
	public Calendar getTxnDate() {
		return txnDate;
	}

	/**
	 * 
	 * @param txnDate
	 */
	public void setTxnDate(Calendar txnDate) {
		this.txnDate = txnDate;
	}

	/**
	 * 
	 * @return the controlToken
	 */
	@Column(name="control_token")
	public String getControlToken() {
		return controlToken;
	}

	/**
	 * 
	 * @param controlToken
	 */
	public void setControlToken(String controlToken) {
		this.controlToken = controlToken;
	}
	
	/**
	 * 
	 * @return transactionStatus
	 */
	@Column(name="transaction_status")
	public Integer getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * 
	 * @param transactionStatus
	 */
	public void setTransactionStatus(Integer transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	/**
	 * 
	 * @return transactionTypeId
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "transaction_type_id")
	//@Column(name="transaction_type_id")
	public TransactionType getTransactionTypeId() {
		return transactionTypeId;
	}

	/**
	 * 
	 * @param transactionTypeId
	 */
	public void setTransactionTypeId(TransactionType transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	
	
	/**
	 * 
	 * @return transactionResponseId
	 */
	/*@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "transaction_response_id")*/
	@Column(name="transaction_response_id")
	public Integer getTransactionResponseId() {
		return transactionResponseId;
	}

	/**
	 * 
	 * @param transactionResponseId
	 */
	public void setTransactionResponseId(Integer transactionResponseId) {
		this.transactionResponseId = transactionResponseId;
	}

	/**
	 * 
	 * @return cardProductId
	 */
	/*@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "card_product_id")*/
	@Column(name="card_product_id")
	public Integer getCardProductId() {
		return cardProductId;
	}

	/**
	 * 
	 * @param cardProductId
	 */
	public void setCardProductId(Integer cardProductId) {
		this.cardProductId = cardProductId;
	}

	/**
	 * 
	 * @return the retried
	 */
	public boolean isRetried() {
		return retried;
	}

	/**
	 * 
	 * @param retried
	 */
	public void setRetried(boolean retried) {
		this.retried = retried;
	}

	/**
	 * 
	 * @return retryAttempts
	 */
	@Column(name="retry_attempts")
	public Integer getRetryAttempts() {
		return retryAttempts;
	}

	/**
	 * 
	 * @param retryAttempts
	 */
	public void setRetryAttempts(Integer retryAttempts) {
		this.retryAttempts = retryAttempts;
	}

	/**
	 * 
	 * @return secToken
	 */
	@Column(name="sec_token")
	public String getSecToken() {
		return secToken;
	}

	/**
	 * 
	 * @param secToken
	 */
	public void setSecToken(String secToken) {
		this.secToken = secToken;
	}

	/**
	 * @return the parameters
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="transaction_id")
	@XmlTransient
	public Set<TransactionParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Set<TransactionParameter> parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * @return the responseParameters
	 *//*
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="transaction_id")
	public Set<TransactionResponseParameter> getResponseParameters() {
		return responseParameters;
	}

	*//**
	 * @param responseParameters the responseParameters to set
	 *//*
	public void setResponseParameters(Set<TransactionResponseParameter> responseParameters) {
		this.responseParameters = responseParameters;
	}
	*/
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", hashCode=" + hashCode + ", terminalId=" + terminalId + ", terminalBankId="
				+ terminalBankId + ", merchantIdCode=" + merchantIdCode + ", amount=" + amount + ", txnDate=" + txnDate
				+ ", controlToken=" + controlToken + ", transactionStatus=" + transactionStatus + ", transactionTypeId="
				+ transactionTypeId + ", transactionResponseId=" + transactionResponseId + ", cardProductId="
				+ cardProductId + ", retried=" + retried + ", retryAttempts=" + retryAttempts + ", secToken=" + secToken
				+ "]";
	}
	
}
