package vpago.dataapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.dataapi.classes.forms.BatchHistorySynchronized;
import vpago.dataapi.classes.forms.Dashboard;
import vpago.dataapi.classes.forms.DashboardList;
import vpago.dataapi.classes.forms.DashboardTerminal;
import vpago.dataapi.classes.forms.DashboardTerminalList;
import vpago.dataapi.classes.minified.AffiliationMin;
import vpago.dataapi.classes.minified.BankCardProductMin;
import vpago.dataapi.classes.minified.BatchConsultMin;
import vpago.dataapi.classes.minified.BatchHistoryConsultSynchronizedMin;
import vpago.dataapi.classes.minified.BatchHistorySynchronizedMin;
import vpago.dataapi.classes.minified.BatchSummaryConsultMin;
import vpago.dataapi.classes.minified.CardFranchiseMin;
import vpago.dataapi.classes.minified.CardTypeMin;
import vpago.dataapi.classes.minified.CityMin;
import vpago.dataapi.classes.minified.CountryMin;
import vpago.dataapi.classes.minified.DashboardMerchantByAcquirerMin;
import vpago.dataapi.classes.minified.DashboardMerchantByStateMin;
import vpago.dataapi.classes.minified.DashboardTerminalMin;
import vpago.dataapi.classes.minified.DeviceMin;
import vpago.dataapi.classes.minified.DeviceToAssignMin;
import vpago.dataapi.classes.minified.DeviceTypeMin;
import vpago.dataapi.classes.minified.GeneralCardProductMin;
import vpago.dataapi.classes.minified.MunicipalityMin;
import vpago.dataapi.classes.minified.ParishMin;
import vpago.dataapi.classes.minified.PaymentChannelMin;
import vpago.dataapi.classes.minified.PostalZoneMin;
import vpago.dataapi.classes.minified.StateMin;
import vpago.dataapi.classes.minified.TerminalAffiliationMin;
import vpago.dataapi.classes.minified.TerminalDeviceMin;
import vpago.dataapi.classes.minified.TerminalMin;
import vpago.dataapi.classes.minified.TerminalSummaryMin;
import vpago.dataapi.classes.minified.TransactionConsultMin;
import vpago.dataapi.classes.minified.TransactionParametersMin;
import vpago.dataapi.classes.wrappers.AcquirerList;
import vpago.dataapi.classes.wrappers.AffiliationList;
import vpago.dataapi.classes.wrappers.BankCardProductList;
import vpago.dataapi.classes.wrappers.BankList;
import vpago.dataapi.classes.wrappers.BatchConsultList;
import vpago.dataapi.classes.wrappers.BatchHistoryConsultSynchronizedList;
import vpago.dataapi.classes.wrappers.BatchHistorySynchronizedList;
import vpago.dataapi.classes.wrappers.BatchSummaryConsultList;
import vpago.dataapi.classes.wrappers.BinList;
import vpago.dataapi.classes.wrappers.CardFranchiseList;
import vpago.dataapi.classes.wrappers.CardTypeList;
import vpago.dataapi.classes.wrappers.CityList;
import vpago.dataapi.classes.wrappers.CorporationList;
import vpago.dataapi.classes.wrappers.CountryList;
import vpago.dataapi.classes.wrappers.DashboardMerchantByAcquirerList;
import vpago.dataapi.classes.wrappers.DashboardMerchantByStateList;
import vpago.dataapi.classes.wrappers.DeviceList;
import vpago.dataapi.classes.wrappers.DeviceToAssignList;
import vpago.dataapi.classes.wrappers.DeviceTypeList;
import vpago.dataapi.classes.wrappers.GeneralCardProductList;
import vpago.dataapi.classes.wrappers.IndustryTypeList;
import vpago.dataapi.classes.wrappers.MerchantList;
import vpago.dataapi.classes.wrappers.MunicipalityList;
import vpago.dataapi.classes.wrappers.ParishList;
import vpago.dataapi.classes.wrappers.PaymentChannelList;
import vpago.dataapi.classes.wrappers.PostalZoneList;
import vpago.dataapi.classes.wrappers.RuleList;
import vpago.dataapi.classes.wrappers.RuleSetList;
import vpago.dataapi.classes.wrappers.RuleSetRules;
import vpago.dataapi.classes.wrappers.SectorList;
import vpago.dataapi.classes.wrappers.StateList;
import vpago.dataapi.classes.wrappers.TerminalAffiliationList;
import vpago.dataapi.classes.wrappers.TerminalDeviceList;
import vpago.dataapi.classes.wrappers.TerminalList;
import vpago.dataapi.classes.wrappers.TerminalParametersList;
import vpago.dataapi.classes.wrappers.TerminalSummaryList;
import vpago.dataapi.classes.wrappers.TransactionConsultList;
import vpago.dataapi.classes.wrappers.TransactionParametersList;

/**
 * Clase para aplicar formato a las entidades que seran devueltas por las funciones de servicios web
 */
@XmlRootElement
@XmlSeeAlso({Merchant.class,
			Multimerchant.class,
			MerchantList.class,
			Response.class,
			Device.class,
			DeviceType.class,
			AssignedDevice.class,
			Terminal.class, 
			TerminalList.class,
			Rule.class,
			RuleProduct.class,
			RuleProductGeneral.class,
			RuleProportion.class,
			RuleList.class,
			RuleSet.class,
			RuleSetList.class,
			TimeFrame.class,
			Acquirer.class,
			AcquirerList.class,
			Bank.class,
			BankList.class,
			RuleSetRules.class,
			Sector.class,
			IndustryType.class,
			SectorList.class,
			IndustryTypeList.class,
			Corporation.class,
			CorporationList.class,
			TerminalParametersList.class,
			PaymentChannel.class,
			PaymentChannelMin.class,
			PaymentChannelList.class,
			Affiliation.class,
			DeviceTypeMin.class,
			DeviceTypeList.class,
			DeviceMin.class,
			DeviceList.class,
			AffiliationMin.class,
			AffiliationList.class,
			BankCardProductMin.class,
			GeneralCardProductMin.class,
			BankCardProductList.class,
			GeneralCardProductList.class,
			GeneralCardProduct.class,
			BankCardProduct.class,
			CardType.class,
			CardTypeMin.class,
			CardTypeList.class,
			CardFranchise.class,
			CardFranchiseMin.class,
			CardFranchiseList.class,
			Country.class,
			CountryMin.class,
			CountryList.class,
			State.class,
			StateMin.class,
			StateList.class,
			Municipality.class,
			MunicipalityMin.class,
			MunicipalityList.class,
			Parish.class,
			ParishMin.class,
			ParishList.class,
			City.class,
			CityMin.class,
			CityList.class,
			PostalZone.class,
			PostalZoneMin.class,
			PostalZoneList.class,
			DeviceToAssign.class,
			DeviceToAssignMin.class,
			DeviceToAssignList.class,
			BinList.class,
			Bin.class,
			Transaction.class,
			TransactionParameter.class,
			TransactionParametersMin.class,
			TransactionParametersList.class,
			TransactionResponse.class,
			TransactionResponseParameter.class,
			TransactionType.class,
			TemplateMessage.class,
			DashboardList.class,
			Dashboard.class,
			DashboardTerminal.class,
			DashboardTerminalList.class,
			DashboardTerminalMin.class,
			DashboardTerminalList.class,
			DashboardMerchantByStateMin.class,
			DashboardMerchantByStateList.class,
			DashboardMerchantByAcquirerMin.class,
			DashboardMerchantByAcquirerList.class,
			TransactionConsultMin.class,
			TransactionConsultList.class,
			TerminalMin.class,
			TerminalSummaryMin.class,
			TerminalSummaryList.class,
			BatchConsultMin.class,
			BatchConsultList.class,
			BatchSummaryConsultMin.class,
			BatchSummaryConsultList.class,
			TerminalAffiliationMin.class,
			TerminalAffiliationList.class,
			TerminalDeviceMin.class,
			BatchHistorySynchronized.class,
			BatchHistorySynchronizedMin.class,
			BatchHistorySynchronizedList.class,
			BatchHistory.class,
			BatchHistoryConsultSynchronizedMin.class,
			BatchHistoryConsultSynchronizedList.class,
			TerminalDeviceList.class
			})
public class APIResponse {
	protected static final Logger logger = LoggerFactory.getLogger(APIResponse.class);
}
