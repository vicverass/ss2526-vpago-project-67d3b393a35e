package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Regla de Producto
 */
@XmlRootElement
@XmlType(propOrder={"productId"})
@Entity
@DiscriminatorValue("product")
public class RuleProduct extends Rule {
	
	public RuleProduct() {
		super();
	}

//	public RuleProduct(String displayName, String description, Acquirer acquirer,
//			Affiliation affiliation, BankCardProduct bankProduct) {
//		super(displayName, description, acquirer, affiliation);
//		this.bankProduct = bankProduct;
//		this.productId = bankProduct.getId();
//	}
	
	public RuleProduct(String displayName, String description, Acquirer acquirer,
			Affiliation affiliation, int productId) {
		super(displayName, description, acquirer, affiliation);
		this.productId = productId;
	}
		
	public void update(RuleProduct other) {
		this.update((Rule)other);
//		this.bankProduct = other.bankProduct;
		this.productId = other.productId;
	}
	
//	/** Producto */
//	private BankCardProduct bankProduct;
	/** Identificador del Producto */
	private int productId;

//	/**
//	 * @return the bankProduct
//	 */
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "product_id")
//	public BankCardProduct getBankProduct() {
//		return bankProduct;
//	}
//
//	/**
//	 * @param bankProduct the bankProduct to set
//	 */
//	public void setBankProduct(BankCardProduct bankProduct) {
//		this.bankProduct = bankProduct;
//	}

	/**
	 * @return the productId
	 */
	@Column(name="product_id")
	public int getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProduct [productId=" + productId + ", id=" + id + ", displayName=" + displayName + ", description="
				+ description  + ", acquirer=" + acquirer + ", affiliation=" + affiliation
				+ "]";
	}
}
