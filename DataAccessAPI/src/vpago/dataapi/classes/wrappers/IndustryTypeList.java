package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.IndustryType;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Tipo de Industria
 */
@XmlRootElement
public class IndustryTypeList extends APIResponse {
	
	public IndustryTypeList() {
		super();
	}

	public IndustryTypeList(List<IndustryType> industryType) {
		super();
		this.industryType = industryType;
	}

	/** Lista de Tipos de Industrias */
	private List<IndustryType> industryType;

	/**
	 * @return the industryType
	 */
	public List<IndustryType> getIndustryType() {
		return industryType;
	}

	/**
	 * @param industryType the industryType to set
	 */
	public void setIndustryType(List<IndustryType> industryType) {
		this.industryType = industryType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndustryTypeList [industryType=" + industryType + "]";
	}
	
	
}
