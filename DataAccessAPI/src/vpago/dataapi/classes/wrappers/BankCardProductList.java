package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BankCardProductMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase producto de banco
 */
@XmlRootElement
public class BankCardProductList extends APIResponse {
	
	public BankCardProductList() {
		super();
	}

	public BankCardProductList(List<BankCardProductMin> bankCardProduct) {
		super();
		this.bankCardProduct = bankCardProduct;
	}

	/** Lista de productos de bancos **/
	private List<BankCardProductMin> bankCardProduct;

	/**
	 * @return the bankCardProduct
	 */
	public List<BankCardProductMin> getBankCardProduct() {
		return bankCardProduct;
	}

	/**
	 * @param bankCardProduct the bankCardProduct to set
	 */
	public void setBankCardProduct(List<BankCardProductMin> bankCardProduct) {
		this.bankCardProduct = bankCardProduct;
	}
	
}
