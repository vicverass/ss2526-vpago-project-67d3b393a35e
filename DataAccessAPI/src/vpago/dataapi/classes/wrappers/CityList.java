package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.CityMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Ciudad
 */
@XmlRootElement
public class CityList extends APIResponse {
	
	public CityList() {
		super();
	}

	public CityList(List<CityMin> city) {
		super();
		this.city = city;
	}

	/** Lista de Ciudades **/
	private List<CityMin> city;

	/**
	 * @return the city
	 */
	public List<CityMin> getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(List<CityMin> city) {
		this.city = city;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CityList [city=" + city + "]";
	}

}
