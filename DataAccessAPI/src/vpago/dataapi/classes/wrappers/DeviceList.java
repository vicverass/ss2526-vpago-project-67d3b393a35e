package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DeviceMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Dispositivo
 */
@XmlRootElement
public class DeviceList extends APIResponse {
	
	public DeviceList() {
		super();
	}

	public DeviceList(List<DeviceMin> device) {
		super();
		this.device = device;
	}
	
	/** Lista de Dispositivos */
	private List<DeviceMin> device;

	/**
	 * @return the device
	 */
	public List<DeviceMin> getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(List<DeviceMin> device) {
		this.device = device;
	}
	
}
