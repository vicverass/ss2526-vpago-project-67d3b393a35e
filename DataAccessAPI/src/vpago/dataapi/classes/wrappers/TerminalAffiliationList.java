package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BatchSummaryConsultMin;
import vpago.dataapi.classes.minified.TerminalAffiliationMin;

/**
 * Clase utilitaria para construir la lista de los lotes cerrados
 */
@XmlRootElement
public class TerminalAffiliationList extends APIResponse {
	
	public TerminalAffiliationList() {
		super();
	}

	public TerminalAffiliationList(List<TerminalAffiliationMin> terminalAffiliationMin) {
		super();
		this.terminalAffiliationMin = terminalAffiliationMin;
	}

	/** Lista de Parametros de transacciones */
	private List<TerminalAffiliationMin> terminalAffiliationMin;

	/**
	 * @return the transaction
	 */
	public List<TerminalAffiliationMin> getTerminalAffiliationMin() {
		return terminalAffiliationMin;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTerminalAffiliationMin(List<TerminalAffiliationMin> terminalAffiliationMin) {
		this.terminalAffiliationMin = terminalAffiliationMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalAffiliationList [TerminalAffiliationMin=" + terminalAffiliationMin + "]";
	}

}
