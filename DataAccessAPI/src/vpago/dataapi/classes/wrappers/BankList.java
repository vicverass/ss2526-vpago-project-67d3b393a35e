package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BankMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Banco
 */
@XmlRootElement
public class BankList extends APIResponse {
	
	public BankList() {
		super();
	}

	public BankList(List<BankMin> bank) {
		super();
		this.bank = bank;
	}

	/** Lista de Bancos */
	private List<BankMin> bank;

	/**
	 * @return the bank
	 */
	public List<BankMin> getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(List<BankMin> bank) {
		this.bank = bank;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BankList [bank=" + bank + "]";
	}
	
}
