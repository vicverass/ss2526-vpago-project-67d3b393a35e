package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.MunicipalityMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Municipio
 */
@XmlRootElement
public class MunicipalityList extends APIResponse {
	
	public MunicipalityList() {
		super();
	}

	public MunicipalityList(List<MunicipalityMin> municipality) {
		super();
		this.municipality = municipality;
	}

	/** Lista de Municipios **/
	private List<MunicipalityMin> municipality;

	/**
	 * @return the municipality
	 */
	public List<MunicipalityMin> getMunicipality() {
		return municipality;
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(List<MunicipalityMin> municipality) {
		this.municipality = municipality;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MunicipalityList [municipality=" + municipality + "]";
	}
	
}
