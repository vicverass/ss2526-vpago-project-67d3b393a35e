package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.GeneralCardProductMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase producto general
 */
@XmlRootElement
public class GeneralCardProductList extends APIResponse {
	
	public GeneralCardProductList() {
		super();
	}

	public GeneralCardProductList(List<GeneralCardProductMin> generalCardProduct) {
		super();
		this.generalCardProduct = generalCardProduct;
	}

	/** Lista de productos generales **/
	private List<GeneralCardProductMin> generalCardProduct;

	/**
	 * @return the generalCardProduct
	 */
	public List<GeneralCardProductMin> getGeneralCardProduct() {
		return generalCardProduct;
	}

	/**
	 * @param generalCardProduct the generalCardProduct to set
	 */
	public void setGeneralCardProduct(List<GeneralCardProductMin> generalCardProduct) {
		this.generalCardProduct = generalCardProduct;
	}

}
