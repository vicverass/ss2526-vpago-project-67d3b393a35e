package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.ParishMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Parroquia
 */
@XmlRootElement
public class ParishList extends APIResponse {
	
	public ParishList() {
		super();
	}

	public ParishList(List<ParishMin> parish) {
		super();
		this.parish = parish;
	}

	/** Lista de Parroquias **/
	private List<ParishMin> parish;

	/**
	 * @return the parish
	 */
	public List<ParishMin> getParish() {
		return parish;
	}

	/**
	 * @param parish the parish to set
	 */
	public void setParish(List<ParishMin> parish) {
		this.parish = parish;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParishList [parish=" + parish + "]";
	}
	
	
}
