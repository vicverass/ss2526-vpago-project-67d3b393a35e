package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DeviceTypeMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Tipo de Dispositivo
 */
@XmlRootElement
public class DeviceTypeList extends APIResponse {
	
	public DeviceTypeList() {
		super();
	}

	public DeviceTypeList(List<DeviceTypeMin> deviceType) {
		super();
		this.deviceType = deviceType;
	}
	
	/** Lista de Tipos de Dispositivos */
	private List<DeviceTypeMin> deviceType;

	/**
	 * @return the deviceType
	 */
	public List<DeviceTypeMin> getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(List<DeviceTypeMin> deviceType) {
		this.deviceType = deviceType;
	}
	
}
