package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.CardTypeMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase tipo de tarjetas (CardType)
 */
@XmlRootElement
public class CardTypeList extends APIResponse {

	public CardTypeList() {
		super();
	}

	public CardTypeList(List<CardTypeMin> cardType) {
		super();
		this.cardType = cardType;
	}

	/** Lista de tipo de tarjetas */
	private List<CardTypeMin> cardType;

	/**
	 * @return the cardType
	 */
	public List<CardTypeMin> getCardType() {
		return cardType;
	}

	/**
	 * @param cardType the cardType to set
	 */
	public void setCardType(List<CardTypeMin> bank) {
		this.cardType = cardType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CardTypeList [cardType=" + cardType + "]";
	}
	
}
