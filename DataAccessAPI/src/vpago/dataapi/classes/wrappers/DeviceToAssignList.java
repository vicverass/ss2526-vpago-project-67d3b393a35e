package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DeviceToAssignMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Dispositivo a asignar
 */
@XmlRootElement
public class DeviceToAssignList extends APIResponse {
	
	public DeviceToAssignList() {
		super();
	}
	
	public DeviceToAssignList(List<DeviceToAssignMin> deviceToAssign) {
		super();
		this.deviceToAssign = deviceToAssign;
	}

	/** Lista de Dispositivos a asignar */
	private List<DeviceToAssignMin> deviceToAssign;

	public List<DeviceToAssignMin> getDeviceToAssign() {
		return deviceToAssign;
	}

	public void setDeviceToAssign(List<DeviceToAssignMin> deviceToAssign) {
		this.deviceToAssign = deviceToAssign;
	}


	
	
}
