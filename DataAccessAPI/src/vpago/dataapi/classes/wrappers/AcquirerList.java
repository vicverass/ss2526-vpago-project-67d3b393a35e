package vpago.dataapi.classes.wrappers;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.minified.AcquirerMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Adquiriente
 */
@XmlRootElement
public class AcquirerList extends APIResponse {

	public AcquirerList() {
		super();
	}

	public AcquirerList(List<AcquirerMin> acquirer) {
		super();
		this.acquirer = acquirer;
	}

	/** Lista de Adquirientes */
	private List<AcquirerMin> acquirer;

	/**
	 * @return the acquirer
	 */
	public List<AcquirerMin> getAcquirer() {
		return acquirer;
	}

	/**
	 * @param acquirer the acquirer to set
	 */
	public void setAcquirer(List<AcquirerMin> acquirer) {
		this.acquirer = acquirer;
	}
	
	/**
	 * Transforma el parametro List de Acquirer a un List de AcquirerMin
	 * 
	 * @param acquirer Lista de elementos de tipo Acquirer
	 */
	public void setFromAcquirerList(List<Acquirer> acquirer) {
		Acquirer acquirerToMin = null;
		this.acquirer = new ArrayList<AcquirerMin>();
		for (int i = 0; i < acquirer.size(); i++) {
			acquirerToMin = acquirer.get(i);
			this.acquirer.add(new AcquirerMin(acquirerToMin.getId(), 
					acquirerToMin.getDisplayName(),
					acquirerToMin.getBankId(),
					acquirerToMin.getAffiliationId(),
					acquirerToMin.getAffiliation().getMerchantId(),
					acquirerToMin.getMerchantIdCode()));
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AcquirerList [acquirer=" + acquirer + "]";
	}
	
}
