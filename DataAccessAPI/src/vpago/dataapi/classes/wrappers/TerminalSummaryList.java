package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.TerminalSummaryMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase TerminalSummary
 */
@XmlRootElement
public class TerminalSummaryList extends APIResponse {
	
	public TerminalSummaryList() {
		super();
	}

	public TerminalSummaryList(List<TerminalSummaryMin> terminal) {
		super();
		this.terminal = terminal;
	}

	/** Lista de Comercios */
	private List<TerminalSummaryMin> terminal;

	/**
	 * @return the terminal
	 */
	public List<TerminalSummaryMin> getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(List<TerminalSummaryMin> terminal) {
		this.terminal = terminal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalList [terminal=" + terminal + "]";
	}
}
