package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.AffiliationMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Afiliacion
 */
@XmlRootElement
public class AffiliationList extends APIResponse {
	
	public AffiliationList() {
		super();
	}

	public AffiliationList(List<AffiliationMin> affiliation) {
		super();
		this.affiliation = affiliation;
	}
	
	/** Lista de Afiliaciones */
	private List<AffiliationMin> affiliation;

	/**
	 * @return the affiliation
	 */
	public List<AffiliationMin> getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(List<AffiliationMin> affiliation) {
		this.affiliation = affiliation;
	}
	
	
}
