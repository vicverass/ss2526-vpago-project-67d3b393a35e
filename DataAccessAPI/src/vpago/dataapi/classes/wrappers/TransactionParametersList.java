package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.TransactionParametersMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Parametros de Terminal
 */
@XmlRootElement
@XmlType(propOrder={"transactionParameters"})
public class TransactionParametersList extends APIResponse {
	
	public TransactionParametersList() {
		super();
	}

	public TransactionParametersList(List<TransactionParametersMin> transactionParameters) {
		super();
		this.transactionParameters = transactionParameters;
	}

	/** Lista de Parametros de transacciones */
	private List<TransactionParametersMin> transactionParameters;

	/**
	 * @return the transaction
	 */
	public List<TransactionParametersMin> getTransactionParameters() {
		return transactionParameters;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransactionParameters(List<TransactionParametersMin> transactionParameters) {
		this.transactionParameters = transactionParameters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "transactionList [transaction=" + transactionParameters + "]";
	}

}
