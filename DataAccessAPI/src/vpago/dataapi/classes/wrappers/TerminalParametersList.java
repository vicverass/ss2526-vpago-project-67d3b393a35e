package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.TerminalParameter;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Parametros de Terminal
 */
@XmlRootElement
public class TerminalParametersList extends APIResponse {
	
	public TerminalParametersList() {
		super();
	}

	public TerminalParametersList(List<TerminalParameter> terminalParameters) {
		super();
		this.terminalParameters = terminalParameters;
	}

	/** Lista de Parametros de Terminales */
	private List<TerminalParameter> terminalParameters;

	/**
	 * @return the terminalParameters
	 */
	public List<TerminalParameter> getTerminalParameters() {
		return terminalParameters;
	}

	/**
	 * @param terminalParameters the terminalParameters to set
	 */
	public void setTerminalParameters(List<TerminalParameter> terminalParameters) {
		this.terminalParameters = terminalParameters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalParametersList [terminalParameters=" + terminalParameters + "]";
	}

}
