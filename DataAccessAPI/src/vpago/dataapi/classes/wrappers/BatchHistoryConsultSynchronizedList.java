package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BatchHistoryConsultSynchronizedMin;

@XmlRootElement
public class BatchHistoryConsultSynchronizedList extends APIResponse {
	
	public BatchHistoryConsultSynchronizedList() {
		super();
	}

	public BatchHistoryConsultSynchronizedList(List<BatchHistoryConsultSynchronizedMin> batchHistoryConsultSynchronizedMin) {
		super();
		this.batchHistoryConsultSynchronizedMin = batchHistoryConsultSynchronizedMin;
	}

	private List<BatchHistoryConsultSynchronizedMin> batchHistoryConsultSynchronizedMin;


	public List<BatchHistoryConsultSynchronizedMin> getBatchHistorySynchronized() {
		return batchHistoryConsultSynchronizedMin;
	}

	public void setBatchHistorySynchronized(List<BatchHistoryConsultSynchronizedMin> batchHistoryConsultSynchronizedMin) {
		this.batchHistoryConsultSynchronizedMin = batchHistoryConsultSynchronizedMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchHistorySynchronizedList [batchHistoryConsultSynchronizedMin=" + batchHistoryConsultSynchronizedMin + "]";
	}

	
}
