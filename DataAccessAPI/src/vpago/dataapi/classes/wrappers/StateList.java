package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.StateMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Estado
 */
@XmlRootElement
public class StateList extends APIResponse {

	public StateList() {
		super();
	}

	public StateList(List<StateMin> state) {
		super();
		this.state = state;
	}

	/** Lista de Estados */
	private List<StateMin> state;

	/**
	 * @return the state
	 */
	public List<StateMin> getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(List<StateMin> state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StateList [state=" + state + "]";
	}
	
	

}
