package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DashboardMerchantByAcquirerMin;

@XmlRootElement
public class DashboardMerchantByAcquirerList extends APIResponse {
	
	public DashboardMerchantByAcquirerList() {
		super();
	}

	public DashboardMerchantByAcquirerList(List<DashboardMerchantByAcquirerMin> dashboardMerchantByAcquirerMin) {
		super();
		this.dashboardMerchantByAcquirerMin = dashboardMerchantByAcquirerMin;
	}

	/** Lista de Comercios */
	private List<DashboardMerchantByAcquirerMin> dashboardMerchantByAcquirerMin;

	/**
	 * @return the terminal
	 */
	public List<DashboardMerchantByAcquirerMin> getDashboardMerchantByAcquirer() {
		return dashboardMerchantByAcquirerMin;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setDashboardMerchantByAcquirer(List<DashboardMerchantByAcquirerMin> dashboardMerchantByAcquirerMin) {
		this.dashboardMerchantByAcquirerMin = dashboardMerchantByAcquirerMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DashboardMerchantByAcquirerList [dashboardMerchantByAcquirerMin=" + dashboardMerchantByAcquirerMin + "]";
	}

	
}
