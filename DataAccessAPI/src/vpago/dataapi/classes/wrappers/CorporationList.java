package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.CorporationMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Corporacion
 */
@XmlRootElement
public class CorporationList extends APIResponse {

	public CorporationList() {
		super();
	}
		
	public CorporationList(List<CorporationMin> corporation) {
		super();
		this.corporation = corporation;
	}
	
	/** Lista de Corporaciones */
	private List<CorporationMin> corporation;

	/**
	 * @return the corporation
	 */
	public List<CorporationMin> getCorporation() {
		return corporation;
	}

	/**
	 * @param corporation the corporation to set
	 */
	public void setCorporation(List<CorporationMin> corporation) {
		this.corporation = corporation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CorporationList [corporations=" + corporation + "]";
	}
	
}
