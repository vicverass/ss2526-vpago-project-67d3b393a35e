package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.TerminalDeviceMin;
import vpago.dataapi.classes.minified.TerminalSummaryMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase TerminalSummary
 */
@XmlRootElement
public class TerminalDeviceList extends APIResponse {
	
	public TerminalDeviceList() {
		super();
	}

	public TerminalDeviceList(List<TerminalDeviceMin> terminal) {
		super();
		this.terminal = terminal;
	}

	/** Lista de Comercios */
	private List<TerminalDeviceMin> terminal;

	/**
	 * @return the terminal
	 */
	public List<TerminalDeviceMin> getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(List<TerminalDeviceMin> terminal) {
		this.terminal = terminal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalList [terminal=" + terminal + "]";
	}
}
