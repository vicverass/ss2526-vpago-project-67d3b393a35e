package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.TransactionConsultMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Parametros de Terminal
 */
@XmlRootElement
public class TransactionConsultList extends APIResponse {
	
	public TransactionConsultList() {
		super();
	}

	public TransactionConsultList(List<TransactionConsultMin> transactionConsult) {
		super();
		this.transactionConsult = transactionConsult;
	}

	/** Lista de Parametros de transacciones */
	private List<TransactionConsultMin> transactionConsult;

	/**
	 * @return the transaction
	 */
	public List<TransactionConsultMin> getTransactionConsult() {
		return transactionConsult;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransactionConsult(List<TransactionConsultMin> transactionConsult) {
		this.transactionConsult = transactionConsult;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "transactionConsultList [transactionConsult=" + transactionConsult + "]";
	}

}
