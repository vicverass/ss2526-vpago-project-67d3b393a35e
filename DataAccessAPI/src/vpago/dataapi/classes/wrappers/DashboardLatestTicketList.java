package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DashboardLatestTicketMin;

@XmlRootElement
public class DashboardLatestTicketList extends APIResponse {
	
	public DashboardLatestTicketList() {
		super();
	}

	public DashboardLatestTicketList(List<DashboardLatestTicketMin> dashboardLatestTicketMin) {
		super();
		this.dashboardLatestTicketMin = dashboardLatestTicketMin;
	}

	/** Lista de Comercios */
	private List<DashboardLatestTicketMin> dashboardLatestTicketMin;

	/**
	 * @return the terminal
	 */
	public List<DashboardLatestTicketMin> getDashboardLatestTicket() {
		return dashboardLatestTicketMin;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setDashboardLatestTicket(List<DashboardLatestTicketMin> dashboardLatestTicketMin) {
		this.dashboardLatestTicketMin = dashboardLatestTicketMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DashboardLatestTicketList [DashboardLatestTicketMin=" + dashboardLatestTicketMin + "]";
	}

	
}
