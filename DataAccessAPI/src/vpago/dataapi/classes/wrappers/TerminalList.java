package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.TerminalMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Terminal
 */
@XmlRootElement
public class TerminalList extends APIResponse {
	
	public TerminalList() {
		super();
	}

	public TerminalList(List<TerminalMin> terminal) {
		super();
		this.terminal = terminal;
	}

	/** Lista de Comercios */
	private List<TerminalMin> terminal;

	/**
	 * @return the terminal
	 */
	public List<TerminalMin> getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(List<TerminalMin> terminal) {
		this.terminal = terminal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalList [terminal=" + terminal + "]";
	}
}
