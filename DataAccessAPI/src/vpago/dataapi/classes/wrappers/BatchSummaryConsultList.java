package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BatchSummaryConsultMin;

/**
 * Clase utilitaria para construir la lista de los lotes cerrados
 */
@XmlRootElement
public class BatchSummaryConsultList extends APIResponse {
	
	public BatchSummaryConsultList() {
		super();
	}

	public BatchSummaryConsultList(List<BatchSummaryConsultMin> batchSummaryConsultMin) {
		super();
		this.batchSummaryConsultMin = batchSummaryConsultMin;
	}

	/** Lista de Parametros de transacciones */
	private List<BatchSummaryConsultMin> batchSummaryConsultMin;

	/**
	 * @return the transaction
	 */
	public List<BatchSummaryConsultMin> getBatchSummaryConsultMin() {
		return batchSummaryConsultMin;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setBatchSummaryConsultMin(List<BatchSummaryConsultMin> batchSummaryConsultMin) {
		this.batchSummaryConsultMin = batchSummaryConsultMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchSummaryConsultList [BatchSummaryConsultMin=" + batchSummaryConsultMin + "]";
	}

}
