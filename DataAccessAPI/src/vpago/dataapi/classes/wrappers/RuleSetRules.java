package vpago.dataapi.classes.wrappers;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Rule;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Reglas asociadas a un Conjunto de Reglas
 */
@XmlRootElement
public class RuleSetRules extends APIResponse {
	
	public RuleSetRules() {
		super();
	}

	public RuleSetRules(Collection<Rule> rule) {
		super();
		this.rule = rule;
	}

	/** Lista de Reglas */
	private Collection<Rule> rule;

	/**
	 * @return the rule
	 */
	public Collection<Rule> getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(Collection<Rule> rule) {
		this.rule = rule;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleSetRules [rule=" + rule + "]";
	}
}
