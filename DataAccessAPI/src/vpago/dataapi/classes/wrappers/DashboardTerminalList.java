package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DashboardTerminalMin;
import vpago.dataapi.classes.minified.TransactionParametersMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Parametros de Terminal
 */
@XmlRootElement
@XmlType(propOrder={"dashboardTerminal"})
public class DashboardTerminalList extends APIResponse {
	
	public DashboardTerminalList() {
		super();
	}

	public DashboardTerminalList(List<DashboardTerminalMin> dashboardTerminal) {
		super();
		this.dashboardTerminal = dashboardTerminal;
	}

	/** Lista de terminales para los graficos */
	private List<DashboardTerminalMin> dashboardTerminal;

	/**
	 * @return the dashboardTerminal
	 */
	public List<DashboardTerminalMin> getDashboardTerminalMin() {
		return dashboardTerminal;
	}

	/**
	 * 
	 * @param dashboardTerminal
	 */
	public void setDashboardTerminalMin(List<DashboardTerminalMin> dashboardTerminal) {
		this.dashboardTerminal = dashboardTerminal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DashboardTerminalList [dashboardTerminal=" + dashboardTerminal + "]";
	}

}
