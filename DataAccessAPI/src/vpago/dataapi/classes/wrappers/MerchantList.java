package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.MerchantMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Comercio
 */
@XmlRootElement
public class MerchantList extends APIResponse {

	public MerchantList() {
		super();
	}
		
	public MerchantList(List<MerchantMin> merchant) {
		super();
		this.merchant = merchant;
	}
	
	/** Lista de Comercios */
	private List<MerchantMin> merchant;

	/**
	 * @return the merchant
	 */
	public List<MerchantMin> getMerchant() {
		return merchant;
	}

	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(List<MerchantMin> merchant) {
		this.merchant = merchant;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MerchantList [merchants=" + merchant + "]";
	}
}
