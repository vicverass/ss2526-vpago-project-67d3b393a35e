package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.PostalZoneMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Zona Postal
 */
@XmlRootElement
public class PostalZoneList extends APIResponse {
	
	public PostalZoneList() {
		super();
	}

	public PostalZoneList(List<PostalZoneMin> postalZone) {
		super();
		this.postalZone = postalZone;
	}

	/** Lista de Zonas Postales**/
	private List<PostalZoneMin> postalZone;

	/**
	 * @return the postalZone
	 */
	public List<PostalZoneMin> getPostalZone() {
		return postalZone;
	}

	/**
	 * @param postalZone the postalZone to set
	 */
	public void setPostalZone(List<PostalZoneMin> postalZone) {
		this.postalZone = postalZone;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostalZoneList [postalZone=" + postalZone + "]";
	}
	
	
}
