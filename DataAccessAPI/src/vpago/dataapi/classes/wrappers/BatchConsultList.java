package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BatchConsultMin;

/**
 * Clase utilitaria para construir la lista de los lotes cerrados
 */
@XmlRootElement
public class BatchConsultList extends APIResponse {
	
	public BatchConsultList() {
		super();
	}

	public BatchConsultList(List<BatchConsultMin> batchConsultMin) {
		super();
		this.batchConsultMin = batchConsultMin;
	}

	/** Lista de Parametros de transacciones */
	private List<BatchConsultMin> batchConsultMin;

	/**
	 * @return the transaction
	 */
	public List<BatchConsultMin> getBatchConsultMin() {
		return batchConsultMin;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setBatchConsultMin(List<BatchConsultMin> batchConsultMin) {
		this.batchConsultMin = batchConsultMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchConsultList [batchConsultMin=" + batchConsultMin + "]";
	}

}
