package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.PaymentChannelMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Canal de Pago
 */
@XmlRootElement
public class PaymentChannelList extends APIResponse {
	
	public PaymentChannelList() {
		super();
	}

	public PaymentChannelList(List<PaymentChannelMin> paymentChannel) {
		super();
		this.paymentChannel = paymentChannel;
	}

	/** Lista de Canales de Pago */
	private List<PaymentChannelMin> paymentChannel;

	/**
	 * @return the paymentChannel
	 */
	public List<PaymentChannelMin> getPaymentChannel() {
		return paymentChannel;
	}

	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(List<PaymentChannelMin> paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	
}
