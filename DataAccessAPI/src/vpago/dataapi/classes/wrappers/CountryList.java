package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.CountryMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Pais
 */
@XmlRootElement
public class CountryList extends APIResponse {
	
	
	public CountryList() {
		super();
	}

	public CountryList(List<CountryMin> country) {
		super();
		this.country = country;
	}

	/** Lista de Paises */
	private List<CountryMin> country;

	/**
	 * @return the country
	 */
	public List<CountryMin> getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(List<CountryMin> country) {
		this.country = country;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountryList [country=" + country + "]";
	}
	
}
