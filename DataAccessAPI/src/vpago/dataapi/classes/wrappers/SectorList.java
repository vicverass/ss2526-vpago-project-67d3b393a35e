package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Sector;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Sector
 */
@XmlRootElement
public class SectorList extends APIResponse {
	
	public SectorList() {
		super();
	}

	public SectorList(List<Sector> sector) {
		super();
		this.sector = sector;
	}

	/** Lista de Sectores */
	private List<Sector> sector;

	/**
	 * @return the sector
	 */
	public List<Sector> getSector() {
		return sector;
	}

	/**
	 * @param sector the sector to set
	 */
	public void setSector(List<Sector> sector) {
		this.sector = sector;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SectorList [sector=" + sector + "]";
	}
	
	
}
