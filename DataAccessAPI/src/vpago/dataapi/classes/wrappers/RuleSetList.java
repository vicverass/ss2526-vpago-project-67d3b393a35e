package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.RuleSetMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Conjunto de Reglas
 */
@XmlRootElement
public class RuleSetList extends APIResponse {
	
	public RuleSetList() {
		super();
	}

	public RuleSetList(List<RuleSetMin> ruleSet) {
		super();
		this.ruleSet = ruleSet;
	}

	/** Lista de Conjuntos de Reglas */
	private List<RuleSetMin> ruleSet;

	/**
	 * @return the ruleSet
	 */
	public List<RuleSetMin> getRuleSet() {
		return ruleSet;
	}

	/**
	 * @param ruleSet the ruleSet to set
	 */
	public void setRuleSet(List<RuleSetMin> ruleSet) {
		this.ruleSet = ruleSet;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleSetList [ruleSet=" + ruleSet + "]";
	}
}
