package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.RuleMin;

/**
 * Clase utilitaria para construir la lista de las entidades de la clase Regla
 */
@XmlRootElement
public class RuleList extends APIResponse {
	
	public RuleList() {
		super();
	}

	public RuleList(List<RuleMin> rule) {
		super();
		this.rule = rule;
	}

	/** Lista de Reglas */
	private List<RuleMin> rule;

	/**
	 * @return the rule
	 */
	public List<RuleMin> getRule() {
		return rule;
	}

	/**
	 * @param rule the rule to set
	 */
	public void setRule(List<RuleMin> rule) {
		this.rule = rule;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleList [rule=" + rule + "]";
	}
	
}
