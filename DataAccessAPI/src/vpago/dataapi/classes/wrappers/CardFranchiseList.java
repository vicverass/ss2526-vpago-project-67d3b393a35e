package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.CardFranchiseMin;


/**
 * Clase utilitaria para construir la lista de las entidades de la clase tipo de tarjetas (CardType)
 */
@XmlRootElement
public class CardFranchiseList extends APIResponse {

	public CardFranchiseList() {
		super();
	}

	public CardFranchiseList(List<CardFranchiseMin> cardFranchise) {
		super();
		this.cardFranchise = cardFranchise;
	}

	/** Lista de franquicias */
	private List<CardFranchiseMin> cardFranchise;

	/**
	 * @return the cardFranchise
	 */
	public List<CardFranchiseMin> getCardFranchise() {
		return cardFranchise;
	}

	/**
	 * @param cardFranchise the cardFranchise to set
	 */
	public void setCardFranchise(List<CardFranchiseMin> cardFranchise) {
		this.cardFranchise = cardFranchise;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CardFranchise [cardFranchise=" + cardFranchise + "]";
	}
	
}