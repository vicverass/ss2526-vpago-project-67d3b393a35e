package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.BatchHistorySynchronizedMin;
import vpago.dataapi.classes.minified.DashboardLatestTicketMin;

@XmlRootElement
public class BatchHistorySynchronizedList extends APIResponse {
	
	public BatchHistorySynchronizedList() {
		super();
	}

	public BatchHistorySynchronizedList(List<BatchHistorySynchronizedMin> batchHistorySynchronizedMin) {
		super();
		this.batchHistorySynchronizedMin = batchHistorySynchronizedMin;
	}

	private List<BatchHistorySynchronizedMin> batchHistorySynchronizedMin;


	public List<BatchHistorySynchronizedMin> getBatchHistorySynchronized() {
		return batchHistorySynchronizedMin;
	}

	public void setBatchHistorySynchronized(List<BatchHistorySynchronizedMin> batchHistorySynchronizedMin) {
		this.batchHistorySynchronizedMin = batchHistorySynchronizedMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchHistorySynchronizedList [BatchHistorySynchronizedMin=" + batchHistorySynchronizedMin + "]";
	}

	
}
