package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Bin;

@XmlRootElement
public class BinList extends APIResponse {
	
	public BinList() {
		super();
	}

	public BinList(List<Bin> bin) {
		super();
		this.bin = bin;
	}

	private List<Bin> bin;

	/**
	 * @return the bin
	 */
	public List<Bin> getBin() {
		return bin;
	}

	/**
	 * @param bin the bin to set
	 */
	public void setBin(List<Bin> bin) {
		this.bin = bin;
	}
	
	
}
