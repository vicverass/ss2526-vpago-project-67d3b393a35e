package vpago.dataapi.classes.wrappers;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.minified.DashboardMerchantByStateMin;

@XmlRootElement
public class DashboardMerchantByStateList extends APIResponse {
	
	public DashboardMerchantByStateList() {
		super();
	}

	public DashboardMerchantByStateList(List<DashboardMerchantByStateMin> dashboardMerchantByStateMin) {
		super();
		this.dashboardMerchantByStateMin = dashboardMerchantByStateMin;
	}

	/** Lista de Comercios */
	private List<DashboardMerchantByStateMin> dashboardMerchantByStateMin;

	/**
	 * @return the terminal
	 */
	public List<DashboardMerchantByStateMin> getDashboardMerchantByState() {
		return dashboardMerchantByStateMin;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setDashboardMerchantByState(List<DashboardMerchantByStateMin> dashboardMerchantByStateMin) {
		this.dashboardMerchantByStateMin = dashboardMerchantByStateMin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DashboardMerchantByStateList [dashboardMerchantByStateMin=" + dashboardMerchantByStateMin + "]";
	}

	
}
