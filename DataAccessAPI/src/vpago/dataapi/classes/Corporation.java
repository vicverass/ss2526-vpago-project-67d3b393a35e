package vpago.dataapi.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Corporacion
 */
@XmlRootElement
@XmlType(propOrder={"id", "name", "fantasyName", "rif"})
@Entity
@Table(name="corporation")
public class Corporation extends APIResponse {
	
	public Corporation() {
		super();
	}
	
	public Corporation(String name, String fantasyName, String rif) {
		super();
		this.name = name;
		this.fantasyName = fantasyName;
		this.rif = rif;
	}
	
	public void update(Corporation other) {
		this.setName(other.getName());
		this.setFantasyName(other.getFantasyName());;
		this.setRif(other.getRif());
	}
	
	/** Identificador generado por la base de datos */
	protected int id;
	/** Nombre o razon social */
	protected String name;
	/** Nombre fantasia */
	protected String fantasyName;
	/** Registro de identificacion fiscal */
	protected String rif;
	/** Lista de comercios asociados **/
	protected List<Merchant> merchants;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="corporation_id_seq")
	@SequenceGenerator(name="corporation_id_seq", sequenceName = "corporation_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fantasyName
	 */
	@Column(name="fantasy_name")
	public String getFantasyName() {
		return fantasyName;
	}

	/**
	 * @param fantasyName the fantasyName to set
	 */
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	/**
	 * @return the rif
	 */
	public String getRif() {
		return rif;
	}

	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	/**
	 * @return the merchants
	 */
	@XmlTransient
	@OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="corporation_id")
	public List<Merchant> getMerchants() {
		return merchants;
	}

	/**
	 * @param merchants the merchants to set
	 */
	public void setMerchants(List<Merchant> merchants) {
		this.merchants = merchants;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Corporation [id=" + id + ", name=" + name + ", fantasyName=" + fantasyName + ", rif=" + rif + "]";
	}

}
