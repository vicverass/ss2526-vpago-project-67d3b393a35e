package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Parametro de Terminal
 */
@XmlRootElement
@XmlType(propOrder={"id", "terminalId", "acquirerId", "name", "value"})
@Entity
@Table(name="terminal_parameter")
public class TerminalParameter {
	
	public TerminalParameter() {
		super();
	}
	
	public TerminalParameter(Terminal terminal, Acquirer acquirer, String name, String value) {
		super();
		this.terminal = terminal;
		this.acquirer = acquirer;
		this.name = name;
		this.value = value;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador de Terminal al que esta asociado el Parametro de Terminal */
	private Integer terminalId;
	/** Terminal al que esta asociado el Parametro de Terminal */
	private Terminal terminal;
	/** Identificador del Adquiriente al que esta asociado el Parametro de Terminal */
	private Integer acquirerId;
	/** Adquiriente al que esta asociado el Parametro de Terminal */
	private Acquirer acquirer;
	/** Nombre del Parametro de Terminal */
	private String name;
	/** Valor del Parametro de Terminal */
	private String value;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="terminal_parameter_id_seq")
	@SequenceGenerator(name="terminal_parameter_id_seq", sequenceName = "terminal_parameter_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the terminalId
	 */
	@Transient
	public Integer getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(Integer terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the terminal
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	/**
	 * @return the acquirerId
	 */
	@Transient
	public Integer getAcquirerId() {
		return acquirerId;
	}

	/**
	 * @param acquirerId the acquirerId to set
	 */
	public void setAcquirerId(Integer acquirerId) {
		this.acquirerId = acquirerId;
	}

	/**
	 * @return the acquirer
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "acquirer_id")
	public Acquirer getAcquirer() {
		return acquirer;
	}

	/**
	 * @param acquirer the acquirer to set
	 */
	public void setAcquirer(Acquirer acquirer) {
		this.acquirer = acquirer;
	}

	/**
	 * @return the name
	 */
	@Column(name="parameter_name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	@Column(name="parameter_value")
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TerminalParameter [id=" + id + ", terminalId=" + terminalId + ", acquirerId=" + acquirerId + ", name="
				+ name + ", value=" + value + "]";
	}
	
}
