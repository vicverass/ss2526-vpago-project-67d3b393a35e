package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Dispositivo por asignar
 */
@XmlRootElement
@XmlType(propOrder={"id", "deviceId", "deviceTypeId", "deviceSerial", "available", "active"})
@Entity
@Table(name="device_to_assign")
public class DeviceToAssign extends APIResponse {
	
	public DeviceToAssign() {
		super();
	}
	
	public DeviceToAssign(Device device, DeviceType deviceType, String deviceSerial, boolean available,
			boolean active) {
		super();
		this.device = device;
		this.deviceId = device != null ? device.getId() : null;
		this.deviceType = deviceType;
		this.deviceTypeId = deviceType != null ? deviceType.getId() : null;
		this.deviceSerial = deviceSerial;
		this.available = available;
		this.active = active;
	}
	

	public DeviceToAssign(int id, Device device, DeviceType deviceType, String deviceSerial, boolean available,
			boolean active) {
		super();
		this.id = id;
		this.device = device;
		this.deviceType = deviceType;
		this.deviceSerial = deviceSerial;
		this.available = available;
		this.active = active;
	}


	/** Id en la base de datos */
	private int id;
	/** Dispositivo **/
	private Device device;
	/** Id de Dispositivo **/
	private Integer deviceId;
	/** Tipo de Dispositivo */
	private DeviceType deviceType;
	/** Id de Tipo de Dispositivo **/
	private Integer deviceTypeId;
	/** Serial */
	private String deviceSerial;
	/** Disponible para su asignacion */
	private boolean available;
	/** Activo */
	private boolean active;
	
	@Id @GeneratedValue(generator="device_to_assign_id_seq")
	@SequenceGenerator(name="device_to_assign_id_seq", sequenceName = "device_to_assign_id_seq")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@XmlTransient
	@ManyToOne( fetch=FetchType.EAGER )
	@JoinColumn(name = "device_id")
	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
	
	@Column(name="device_id", insertable=false, updatable=false)
	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}
	
	@XmlTransient
	@ManyToOne( fetch=FetchType.EAGER )
	@JoinColumn(name = "device_type_id")
	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	@Column(name="device_type_id", insertable=false, updatable=false)
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}

	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}

	@Column(name="serial_number")
	public String getDeviceSerial() {
		return deviceSerial;
	}

	public void setDeviceSerial(String deviceSerial) {
		this.deviceSerial = deviceSerial;
	}

	@Column(name="device_available")
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Column(name="active")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "DeviceToAssign [id=" + id + ", device=" + device + ", deviceType=" + deviceType + ", deviceSerial="
				+ deviceSerial + ", available=" + available + ", active=" + active + "]";
	}
	
	
}
