package vpago.dataapi.classes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase persistente para representar un Contacto
 */
@XmlType(propOrder={"name", "lastname", "phone1", "phone2", "email"})
@Entity
@Table(name="contact")
public class Contact {
	private static final Logger logger = LoggerFactory.getLogger(Contact.class);
	
	public Contact() {
		this("", "", "", "", "");
	}
	
	public Contact(String name, String lastname, String phone1, String phone2, String email) {
		this.name = name.isEmpty() || name == null ? "" : name;
		this.lastname = lastname.isEmpty() || lastname == null ? "" : lastname;
		this.phone1 = phone1.isEmpty() || phone1 == null ? "" : phone1;
		this.phone2 = phone2.isEmpty() || phone2 == null ? "" : phone2;
		this.email = email.isEmpty() || email == null ? "" : email;
	}
	
	public void update(Contact other) {
		logger.debug("Update contact");
		this.name = this.name != other.name ? other.name : this.name;
		this.lastname = this.lastname != other.lastname ? other.lastname : this.lastname;
		this.phone1 = this.phone1 != other.phone1 ? other.phone1 : this.phone1;
		this.phone2 = this.phone2 != other.phone2 ? other.phone2 : this.phone2;
		this.email = this.email != other.email ? other.email : this.email;
		logger.debug("this.email={} other.email={}", this.email, other.email);
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del contacto */
	private String name;
	/** Apellido del contacto */
	private String lastname;
	/** Numero de telefono del contacto */
	private String phone1;
	/** Numero de telefono alternativo del contacto */
	private String phone2;
	/** Direccion de correo electronico del contacto */
	private String email;
	
	/**
	 * @return the id
	 */
	@XmlTransient
	@Id @GeneratedValue(generator="contact_id_seq")
	@SequenceGenerator(name="contact_id_seq", sequenceName = "contact_id_seq")	
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return phone1;
	}
	
	/**
	 * @param phone1 the phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	
	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return phone2;
	}
	
	/**
	 * @param phone2 the phone2 to set
	 */
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone1 == null) ? 0 : phone1.hashCode());
		result = prime * result + ((phone2 == null) ? 0 : phone2.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone1 == null) {
			if (other.phone1 != null)
				return false;
		} else if (!phone1.equals(other.phone1))
			return false;
		if (phone2 == null) {
			if (other.phone2 != null)
				return false;
		} else if (!phone2.equals(other.phone2))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Contact [name=" + name + ", lastname=" + lastname + ", phone1=" + phone1 + ", phone2=" + phone2
				+ ", email=" + email + "]";
	}
	
}
