package vpago.dataapi.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Parametro de Respuesta de la Transaccion
 */
@XmlRootElement
@Entity
@DiscriminatorValue("response")
public class TransactionResponseParameter extends TransactionParameter {

	public TransactionResponseParameter() {
		super();
	}

	public TransactionResponseParameter(Transaction transaction, String paramName, String paramValue) {
		super(transaction, paramName, paramValue);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionResponseParameter [id=" + id + ", paramName=" + paramName + ", paramValue=" + paramValue
				+ ", paramType=" + paramType + "]";
	}
	
}
