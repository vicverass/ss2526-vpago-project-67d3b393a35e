package vpago.dataapi.classes;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



/**
 * Clase persistente para representar un Canal de Pago
 */
@Entity
@Table(name="payment_channel")
public class PaymentChannel {
	
	public PaymentChannel() {
		super();
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del Canal de Pago */
	private String name;
	/** Descripción del Canal de Pago */
	private String description;
	/** Codigo que sera enviado por el Terminal */
	private String codeName;
	/** Plantillas de mensajes asociada al Canal de Pago */
	private List<TemplateMessage> templateMessage;
	/** Lista de tipos de lotes asociados al canal de pago **/
	private Set<BatchType> batchTypes;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="payment_channel_id_seq")
	@SequenceGenerator(name="payment_channel_id_seq", sequenceName = "payment_channel_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the codeName
	 */
	@Column(name="code_name")
	public String getCodeName() {
		return codeName;
	}

	/**
	 * @param codeName the codeName to set
	 */
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	/**
	 * @return the batchTypes
	 */
	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="payment_channel_id")
	public Set<BatchType> getBatchTypes() {
		return batchTypes;
	}

	/**
	 * @param batchTypes the batchTypes to set
	 */
	public void setBatchTypes(Set<BatchType> batchTypes) {
		this.batchTypes = batchTypes;
	}
	
	/**
	 * @return the templateMessage
	 */
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="payment_channel_id")
	public List<TemplateMessage> getTemplateMessage() {
		return templateMessage;
	}

	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(List<TemplateMessage> templateMessage) {
		this.templateMessage = templateMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentChannel [id=" + id + ", name=" + name + ", description=" + description + ", codeName=" + codeName
				+ "]";
	}

}
