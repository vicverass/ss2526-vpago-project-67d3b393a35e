package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Pais
 */
@XmlRootElement
@XmlType(propOrder={"id", "countryName", "iso31661Code", "iso31662Code", "iso3166NumCode"})
@Entity
@Table(name="country")
public class Country extends APIResponse {
	
	
	public Country() {
		super();
	}
	
	public Country(String countryName, String iso31661Code, String iso31662Code, String iso3166NumCode) {
		super();
		this.countryName = countryName;
		this.iso31661Code = iso31661Code;
		this.iso31662Code = iso31662Code;
		this.iso3166NumCode = iso3166NumCode;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del pais */
	private String countryName;
	/** Codigo ISO 3166-1 */
	private String iso31661Code;
	/** Codigo ISO 3166-2 */
	private String iso31662Code;
	/** Codigo ISO 3166-num */
	private String iso3166NumCode;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="country_id_seq")
	@SequenceGenerator(name="country_id_seq", sequenceName = "country_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the countryName
	 */
	@Column(name="country")
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the iso31661Code
	 */
	@Column(name="iso_3166_1")
	public String getIso31661Code() {
		return iso31661Code;
	}

	/**
	 * @param iso31661Code the iso31661Code to set
	 */
	public void setIso31661Code(String iso31661Code) {
		this.iso31661Code = iso31661Code;
	}

	/**
	 * @return the iso31662Code
	 */
	@Column(name="iso_3166_2")
	public String getIso31662Code() {
		return iso31662Code;
	}

	/**
	 * @param iso31662Code the iso31662Code to set
	 */
	public void setIso31662Code(String iso31662Code) {
		this.iso31662Code = iso31662Code;
	}

	/**
	 * @return the iso3166NumCode
	 */
	@Column(name="iso_3166_num")
	public String getIso3166NumCode() {
		return iso3166NumCode;
	}

	/**
	 * @param iso3166NumCode the iso3166NumCode to set
	 */
	public void setIso3166NumCode(String iso3166NumCode) {
		this.iso3166NumCode = iso3166NumCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result + id;
		result = prime * result + ((iso31661Code == null) ? 0 : iso31661Code.hashCode());
		result = prime * result + ((iso31662Code == null) ? 0 : iso31662Code.hashCode());
		result = prime * result + ((iso3166NumCode == null) ? 0 : iso3166NumCode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (countryName == null) {
			if (other.countryName != null)
				return false;
		} else if (!countryName.equals(other.countryName))
			return false;
		if (id != other.id)
			return false;
		if (iso31661Code == null) {
			if (other.iso31661Code != null)
				return false;
		} else if (!iso31661Code.equals(other.iso31661Code))
			return false;
		if (iso31662Code == null) {
			if (other.iso31662Code != null)
				return false;
		} else if (!iso31662Code.equals(other.iso31662Code))
			return false;
		if (iso3166NumCode == null) {
			if (other.iso3166NumCode != null)
				return false;
		} else if (!iso3166NumCode.equals(other.iso3166NumCode))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Country [id=" + id + ", countryName=" + countryName + ", iso31661Code=" + iso31661Code
				+ ", iso31662Code=" + iso31662Code + ", iso3166NumCode=" + iso3166NumCode + "]";
	}
	
}
