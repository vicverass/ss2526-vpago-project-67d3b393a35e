package vpago.dataapi.classes;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import vpago.dataapi.db.HibernateUtil;

/**
 * Clase persistente para representar un Terminal
 */
@XmlRootElement
@XmlType(propOrder={"id", "terminalGuid", "accessKey", "active", "assignedDevice", "terminalParameters", "assignedDeviceId", "affiliationId"})
@Entity
@Table(name="terminal")
public class Terminal extends APIResponse {

	public Terminal() {
		super();
	}
	
	public Terminal(Affiliation affiliation) {
		super();
		if(affiliation == null) {
			throw new NullPointerException("Parametro affiliation no puede ser null");
		}
		this.active = false;
		this.affiliationId = affiliation.getId();
		this.affiliation = affiliation;
		this.generateGroupIndex();
		this.generateTerminalGuid();
		// TODO Generar el access key luego
		this.accessKey = this.terminalGuid;
		this.terminalParameters = new ArrayList<TerminalParameter>();
		this.traceCounter = 0;
		this.createDate = Calendar.getInstance();
	}
	
	/** Identificador generado por el motor base de datos */
	private Integer id;
	/** Identificador unico del terminal en la base de datos */
	private String terminalGuid;
	/** Llave para autenticar al terminal */
	private String accessKey;
	/** Terminal activo */
	private boolean active;
	/** Numero de Terminal dentro del Comercio */
	private int groupIndex;
	/** Identificador de la Afiliacion a la que esta asociado el Terminal */
	private int affiliationId;
	/** Afiliacion a la que esta asociado el Terminal */
	private Affiliation affiliation;
	/** Identificador de Dispositivo Asignado */
	private Integer assignedDeviceId;
	/** Dispositivo Asignado */
	private AssignedDevice assignedDevice;
	/** Lista de terminales bancarios */
	private List<TerminalParameter> terminalParameters;
	private Integer traceCounter;
	/** Lotes actuales **/
	private List<CurrentBatch> currentBatchs;
	/** Fecha de creacion */
	private Calendar createDate;
	/** Fecha de asignacion del dispositivo */
	private Calendar dateAssignment;
	/** Fecha de inicio de operaciones */
	private Calendar startDateOperations;
	/** Fecha de cambio de serial */
	private Calendar serialUpdateDate;
	/** Fecha de ultima transaccion **/
	private Calendar lastTransactionDate;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="terminal_id_seq")
	@SequenceGenerator(name="terminal_id_seq", sequenceName = "terminal_id_seq")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the terminalGuid
	 */
	@XmlElement
	@Column(name="terminal_guid")
	@NotNull
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	@SuppressWarnings("unused")
	private void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the accessKey
	 */
	@Column(name="access_key")
	public String getAccessKey() {
		return accessKey;
	}

	/**
	 * @param accessKey the accessKey to set
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	/**
	 * @return the active
	 */
	@NotNull
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the groupIndex
	 */
	@XmlTransient
	@Column(name="group_index")
	@NotNull
	public int getGroupIndex() {
		return groupIndex;
	}

	/**
	 * @param groupIndex the groupIndex to set
	 */
	public void setGroupIndex(int groupIndex) {
		this.groupIndex = groupIndex;
	}

	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/**
	 * @return the affiliation
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	@NotNull
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliationId = affiliation.getId();
		this.affiliation = affiliation;
	}

	/**
	 * @return the assignedDeviceId
	 */
	@Column(name="assigned_device_id")
	public Integer getAssignedDeviceId() {
		return assignedDeviceId;
	}

	/**
	 * @param assignedDeviceId the assignedDeviceId to set
	 */
	public void setAssignedDeviceId(Integer assignedDeviceId) {
		this.assignedDeviceId = assignedDeviceId;
	}

	/**
	 * @return the assignedDevice
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "assigned_device_id", insertable=false, updatable=false)
	public AssignedDevice getAssignedDevice() {
		return assignedDevice;
	}

	/**
	 * @param assignedDevice the assignedDevice to set
	 */
	public void setAssignedDevice(AssignedDevice assignedDevice) {
		this.assignedDevice = assignedDevice;
		assignedDeviceId = assignedDevice != null ? this.assignedDeviceId = assignedDevice.getId() : null;
	}
	
	/**
	 * @return the terminalParameter
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="terminal_id")
	public List<TerminalParameter> getTerminalParameters() {
		return terminalParameters;
	}

	/**
	 * @param terminalParameter the terminalParameter to set
	 */
	public void setTerminalParameters(List<TerminalParameter> terminalParameter) {
		this.terminalParameters = terminalParameter;
	}
	
	/**
	 * Inserta un terminal bancario en la lista del terminal
	 * 
	 * @param acquirer Adquiriente
	 * @param name Nombre del Parametro de Terminal
	 * @param value Valor del Parametro de Terminal
	 */
	public void addTerminalParameter(Acquirer acquirer, String name, String value) {
		this.terminalParameters.add(new TerminalParameter(this, acquirer, name, value));
	}

	/**
	 * 
	 * @return traceCounter
	 */
	@XmlTransient
	@Column(name="trace_counter")
	public Integer getTraceCounter() {
		return traceCounter;
	}

	/**
	 * 
	 * @param traceCounter
	 */
	public void setTraceCounter(Integer traceCounter) {
		this.traceCounter = traceCounter;
	}
	
	/**
	 * @return the currentBatchs
	 */
	@XmlTransient
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="terminal_id")
	public List<CurrentBatch> getCurrentBatchs() {
		return currentBatchs;
	}

	/**
	 * @param currentBatchs the currentBatchs to set
	 */
	public void setCurrentBatchs(List<CurrentBatch> currentBatchs) {
		this.currentBatchs = currentBatchs;
	}
	
	/**
	 * 
	 * @return the createDate
	 */
	@XmlTransient
	@Column(name="create_date")
	public Calendar getCreateDate() {
		return createDate;
	}

	/**
	 * 
	 * @param createDate
	 */
	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	/**
	 * 
	 * @return the dateAssignment
	 */
	@XmlTransient
	@Column(name="date_assignment")
	public Calendar getDateAssignment() {
		return dateAssignment;
	}

	/**
	 * 
	 * @param dateAssignment
	 */
	public void setDateAssignment(Calendar dateAssignment) {
		this.dateAssignment = dateAssignment;
	}
	
	/**
	 * 
	 * @return the startDateOperations
	 */
	@XmlTransient
	@Column(name="start_date_operations")
	public Calendar getStartDateOperations() {
		return startDateOperations;
	}

	/**
	 * 
	 * @param startDateOperations
	 */
	public void setStartDateOperations(Calendar startDateOperations) {
		this.startDateOperations = startDateOperations;
	}

	/**
	 * 
	 * @return the serialUpdateDate
	 */
	@XmlTransient
	@Column(name="serial_update_date")
	public Calendar getSerialUpdateDate() {
		return serialUpdateDate;
	}

	/**
	 * 
	 * @param serialUpdateDate
	 */
	public void setSerialUpdateDate(Calendar serialUpdateDate) {
		this.serialUpdateDate = serialUpdateDate;
	}

	/**
	 * 
	 * @return the lastTransactionDate
	 */
	@XmlTransient
	@Column(name = "last_transaction_date")
	public Calendar getLastTransactionDate() {
		return lastTransactionDate;
	}

	/**
	 * 
	 * @param lastTransactionDate
	 */
	public void setLastTransactionDate(Calendar lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	@Override
	public String toString() {
		return "Terminal [id=" + id + ", terminalGuid=" + terminalGuid + ", accessKey=" + accessKey + ", active="
				+ active + ", groupIndex=" + groupIndex + ", affiliationId=" + affiliationId + ", affiliation="
				+ affiliation + ", assignedDeviceId=" + assignedDeviceId + ", assignedDevice=" + assignedDevice
				+ ", terminalParameters=" + terminalParameters + ", traceCounter=" + traceCounter + ", currentBatchs="
				+ currentBatchs + ", createDate=" + createDate + ", dateAssignment=" + dateAssignment
				+ ", startDateOperations=" + startDateOperations + ", serialUpdateDate=" + serialUpdateDate + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessKey == null) ? 0 : accessKey.hashCode());
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((assignedDevice == null) ? 0 : assignedDevice.hashCode());
		result = prime * result + ((assignedDeviceId == null) ? 0 : assignedDeviceId.hashCode());
		result = prime * result + groupIndex;
		if(id != null) result = prime * result + id;
		result = prime * result + ((affiliation == null) ? 0 : affiliation.hashCode());
		result = prime * result + affiliationId;
		result = prime * result + ((terminalGuid == null) ? 0 : terminalGuid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Terminal other = (Terminal) obj;
		if (accessKey == null) {
			if (other.accessKey != null)
				return false;
		} else if (!accessKey.equals(other.accessKey))
			return false;
		if (active != other.active)
			return false;
		if (assignedDevice == null) {
			if (other.assignedDevice != null)
				return false;
		} else if (!assignedDevice.equals(other.assignedDevice))
			return false;
		if (assignedDeviceId == null) {
			if (other.assignedDeviceId != null)
				return false;
		} else if (!assignedDeviceId.equals(other.assignedDeviceId))
			return false;
		if (groupIndex != other.groupIndex)
			return false;
		if (id != other.id)
			return false;
		if (affiliation == null) {
			if (other.affiliation != null)
				return false;
		} else if (!affiliation.equals(other.affiliation))
			return false;
		if (affiliationId != other.affiliationId)
			return false;
		if (terminalGuid == null) {
			if (other.terminalGuid != null)
				return false;
		} else if (!terminalGuid.equals(other.terminalGuid))
			return false;
		return true;
	}



	/**
	 * Genera el indice entero positivo incremental de un terminal para una determinada afiliacion del comercio
	 * 
	 * @return Un dato booleano que indica el resultado de la funcion: true si se ejecuto correctamente y false en caso contrario
	 */
	private boolean generateGroupIndex() {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Iterator<?> result = session.createQuery("select count(terminal) from Terminal as terminal where terminal.affiliationId = :affiliationId")
					.setInteger("affiliationId", this.affiliationId).iterate();
			Random rand = new Random();
			Long r = (long) -1; 
			if (result.hasNext()) r = (long) result.next();
			this.groupIndex = r != null ? r.intValue() + 1 : rand.nextInt(500 + 1) + 500;
			return true;
		} catch (HibernateException e) {
			logger.error(e.toString());
			return false;
		} finally {
			if(session != null) session.close();
		}
	}
	
	/**
	 * Genera un guid (global unique identifier) unico para identificar un Terminal
	 * 
	 * @return Un dato booleano que indica el resultado de la funcion: true si se ejecuto correctamente y false en caso contrario
	 */
	public boolean generateTerminalGuid() {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(this.affiliation.getMerchant().getName().getBytes());
			md.update(this.getAffiliation().getMerchant().getRif().getBytes());
			md.update((byte) this.groupIndex);
			md.update(this.id != null ? this.id.byteValue() : ((Integer)ThreadLocalRandom.current().nextInt(0, 10000)).byteValue());
			String guid = DatatypeConverter.printHexBinary(md.digest());
			this.terminalGuid = guid.substring(0, guid.length()/4);
			return true;
		} catch (Exception e) {
			logger.error(e.toString());
			return false;
		}
	}
}
