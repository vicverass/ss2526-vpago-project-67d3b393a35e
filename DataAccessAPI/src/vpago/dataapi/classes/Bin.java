package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar los rangos numericos que identifican tipos de tarjetas
 */
@XmlRootElement
@XmlType(propOrder={"id", "binStart", "binEnd"})
@Entity
@Table(name="bin")
public class Bin {

	public Bin() {
		super();
	}
	
	public Bin(String binStart, String binEnd) {
		super();
		this.binStart = binStart;
		this.binEnd = binEnd;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Comienzo del rango  */
	private String binStart;
	/** Fin del rango */
	private String binEnd;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="bin_id_seq")
	@SequenceGenerator(name="bin_id_seq", sequenceName = "bin_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the binStart
	 */
	@Column(name="bin_start")
	public String getBinStart() {
		return binStart;
	}

	/**
	 * @param binStart the binStart to set
	 */
	public void setBinStart(String binStart) {
		this.binStart = binStart;
	}

	/**
	 * @return the binEnd
	 */
	@Column(name="bin_end")
	public String getBinEnd() {
		return binEnd;
	}

	/**
	 * @param binEnd the binEnd to set
	 */
	public void setBinEnd(String binEnd) {
		this.binEnd = binEnd;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bin [id=" + id + ", bin_start=" + binStart + ", bin_end=" + binEnd + "]";
	}


}
