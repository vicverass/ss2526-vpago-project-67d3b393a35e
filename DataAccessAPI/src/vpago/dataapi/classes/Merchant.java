package vpago.dataapi.classes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Comercio
 */
@XmlRootElement
@XmlType(propOrder={"id", "merchantGuid", "fantasyName", "active", "name", "rif", "sectorName", 
		"industryTypeName", "parentMerchantId", "corporationId", "industryTypeId", "location", 
		"industryType"})
@XmlSeeAlso({Multimerchant.class})
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="merchant_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("simple")
@Table(name="merchant")
public class Merchant extends APIResponse {

	public Merchant() {
		super(); 
	}
	
	public Merchant(String name, String rif, Location location, String fantasyName, boolean active, IndustryType industryType, Multimerchant parentMerchant, Corporation corporation, Calendar  createDate) {
		//Verificamos que ninguno de los parametros sea null
		if(name == null || rif == null || location == null) {
			String param = name == null ? "name": rif == null ? "rif" : "location";
			throw new NullPointerException("Par�metro " + param + " no puede ser null");
		}
		//Verificamos que los strings no esten vacios
		if(name.isEmpty() || rif.isEmpty()) throw new IllegalArgumentException("Ninguno de los argumentos puede estar vac�o");
		this.id = null;
		this.name = name;
		this.rif = rif;
		this.location = location;
		this.fantasyName = fantasyName;
		this.active = true;
		this.industryTypeId = industryType.getId();
		this.industryType = industryType;
		this.parentMerchantId = parentMerchant != null ? parentMerchant.getId() : null;
		this.parentMerchant = parentMerchant;
		this.corporationId = corporation != null ? corporation.getId() : null;
		this.corporation = corporation;
		this.sectorName = industryType.getSector().getName();
		this.industryTypeName = industryType.getName();
		this.createDate = createDate;
		//Generamos el identificador de comercio
		this.generateMerchantGuid(); 
	}
	
	public void update(Merchant other){
		logger.trace("{}={}", this.merchantGuid, other.merchantGuid);
		if (this.merchantGuid.equals(other.merchantGuid)){
			logger.debug("Update merchant");
			this.name = other.name;
			this.fantasyName = other.fantasyName;
			this.industryTypeId = other.industryTypeId;
			this.rif = other.rif;
			this.location.update(other.location);			
		}
	}
	
	public void updateData(Merchant other){
		logger.trace("{}={}", this.merchantGuid, other.merchantGuid);
		if (this.merchantGuid.equals(other.merchantGuid)){
			logger.debug("Update merchant");
			this.name = other.name;
			this.fantasyName = other.fantasyName;
			this.rif = other.rif;			
		}
	}

	/** Identificador generado por el motor base de datos */
	protected Integer id;
	/** Identificador unico del Comercio en la base de datos */
	protected String merchantGuid;
	/** Nombre del comercio */
	protected String name;
	/** Registro de Identificacion Fiscal del Comercio **/
	protected String rif;
	/** Ubicacion del Comercio */
	protected Location location;
	/** Nombre fantasia */
	protected String fantasyName;
	/** Comercio activo */
	protected boolean active;
	/** Identificador de Tipo de Industria del Comercio */
	protected int industryTypeId;
	/** Tipo de Industria del Comercio */
	protected IndustryType industryType;
	/** Identificador del Multicomercio al cual pertenece el Comercio */
	protected Integer parentMerchantId;
	/** Multicomercio al cual pertenece el Comercio */
	protected Multimerchant parentMerchant;
	/** Tipo de Comercio */
	protected String merchantType;
	/** Identificador de la Corporacion a la cual pertenece el Comercio */
	protected Integer corporationId;
	/** Corporacion a la cual pertenece el Comercio */
	protected Corporation corporation;
	/** Afiliaciones del Comercio */
	protected List<Affiliation> affiliations;
	/** Nombre de sector al que pertenece **/
	protected String sectorName;
	/** Nombre del tipo de industria al que pertencene **/
	protected String industryTypeName;
	/** Fecha de creacion del comercio **/
	protected Calendar createDate;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="merchant_id_seq")
	@SequenceGenerator(name="merchant_id_seq", sequenceName = "merchant_id_seq")
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the merchantGuid
	 */
	@XmlElement
	@Column(name="merchant_guid")
	@NotNull
	public String getMerchantGuid() {
		return merchantGuid;
	}
	
	/**
	 * @param merchantGuid the merchantGuid to set
	 */
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	/**
	 * @return the name
	 */
	@NotNull
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rif
	 */
	@NotNull
	public String getRif() {
		return rif;
	}
	
	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}

	/**
	 * @return the location
	 */
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "location_id")
	@NotNull
	public Location getLocation() {
		return location;
	}
	
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
	
	/**
	 * @return the fantasyName
	 */
	@Column(name="fantasy_name")
	public String getFantasyName() {
		return fantasyName;
	}

	/**
	 * @param fantasyName the fantasyName to set
	 */
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the industryTypeId
	 */
	@Column(name="industry_type_id")
	public int getIndustryTypeId() {
		return industryTypeId;
	}

	/**
	 * @param industryTypeId the industryTypeId to set
	 */
	public void setIndustryTypeId(int industryTypeId) {
		this.industryTypeId = industryTypeId;
	}

	/**
	 * @return the industryType
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "industry_type_id", insertable=false, updatable=false)
	public IndustryType getIndustryType() {
		return industryType;
	}

	/**
	 * @param industryType the industryType to set
	 */
	public void setIndustryType(IndustryType industryType) {
		if(industryType != null) this.industryTypeId = industryType.getId();
		this.industryType = industryType;
	}
	
	/**
	 * @return the parentMerchantId
	 */
	@Column(name="parent_merchant_id")
	public Integer getParentMerchantId() {
		return parentMerchantId;
	}

	/**
	 * @param parentMerchantId the parentMerchantId to set
	 */
	public void setParentMerchantId(Integer parentMerchantId) {
		this.parentMerchantId = parentMerchantId;
	}

	/**
	 * @return the parentMerchant
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_merchant_id", insertable=false, updatable=false)
	public Multimerchant getParentMerchant() {
		return parentMerchant;
	}

	/**
	 * @param parentMerchant the parentMerchant to set
	 */
	public void setParentMerchant(Multimerchant parentMerchant) {
		this.parentMerchantId = parentMerchant != null ? parentMerchant.getId() : null;
		this.parentMerchant = parentMerchant;
	}
	
	/**
	 * @return the merchantType
	 */
	@XmlTransient
	@Column(name="merchant_type", insertable=false, updatable=false)
	public String getMerchantType() {
		return merchantType;
	}

	/**
	 * @param merchantType the merchantType to set
	 */
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	/**
	 * @return the corporationId
	 */
	@Column(name="corporation_id")
	public Integer getCorporationId() {
		return corporationId;
	}

	/**
	 * @param corporationId the corporationId to set
	 */
	public void setCorporationId(Integer corporationId) {
		this.corporationId = corporationId;
	}

	/**
	 * @return the corporation
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "corporation_id", insertable=false, updatable=false)
	public Corporation getCorporation() {
		return corporation;
	}

	/**
	 * @param corporation the corporation to set
	 */
	public void setCorporation(Corporation corporation) {
		if(corporation != null) this.corporationId = corporation.getId();
		this.corporation = corporation;
	}
	
	/**
	 * @return the affiliations
	 */
	@XmlTransient
	@OneToMany(fetch=FetchType.EAGER)
    @JoinColumn(name="merchant_id")
	public List<Affiliation> getAffiliations() {
		return affiliations;
	}

	/**
	 * @param affiliations the affiliations to set
	 */
	public void setAffiliations(List<Affiliation> affiliations) {
		this.affiliations = affiliations;
	}
	
	/**
	 * @return the sectorName
	 */
	@Transient
	public String getSectorName() {
		return sectorName;
	}

	/**
	 * @param sectorName the sectorName to set
	 */
	public void setSectorName(String sectorName) {
		this.sectorName = sectorName;
	}

	/**
	 * @return the industryTypeName
	 */
	@Transient
	public String getIndustryTypeName() {
		return industryTypeName;
	}

	/**
	 * @param industryTypeName the industryTypeName to set
	 */
	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}

	@Column(name = "create_date")
	@XmlTransient
	/**
	 * 
	 * @return the createDate
	 */
	public Calendar getCreateDate() {
		return createDate;
	}

	/**
	 * 
	 * @param createDate
	 */
	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if(id != null) result = prime * result + id;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((merchantGuid == null) ? 0 : merchantGuid.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((rif == null) ? 0 : rif.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Merchant other = (Merchant) obj;
		if (id != other.id)
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (merchantGuid == null) {
			if (other.merchantGuid != null)
				return false;
		} else if (!merchantGuid.equals(other.merchantGuid))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (rif == null) {
			if (other.rif != null)
				return false;
		} else if (!rif.equals(other.rif))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Merchant [id=" + id + ", merchantGuid=" + merchantGuid + ", name=" + name + ", rif=" + rif
				+ ", location=" + location + ", fantasyName=" + fantasyName + ", active=" + active + ", industryTypeId="
				+ industryTypeId + ", industryType=" + industryType + ", parentMerchantId=" + parentMerchantId
				+ ", parentMerchant=" + parentMerchant + ", merchantType=" + merchantType + ", corporationId="
				+ corporationId + ", corporation=" + corporation + ", affiliations=" + affiliations + ", sectorName="
				+ sectorName + ", industryTypeName=" + industryTypeName + ", createDate=" + createDate + "]";
	}
	
	/**
	 * Genera un guid (global unique identifier) unico para identificar un Comercio
	 * 
	 * @return Un dato booleano que indica el resultado de la funcion: true si se ejecuto correctamente y false en caso contrario
	 */
	public boolean generateMerchantGuid() {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(this.name.getBytes());
			md.update(this.rif.getBytes());
			md.update(this.id != null ? this.id.byteValue() : ((Integer)ThreadLocalRandom.current().nextInt(0, 10000)).byteValue());
			String guid = DatatypeConverter.printHexBinary(md.digest());
			this.merchantGuid = guid.substring(0, guid.length()/2);
			return true;
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.toString());
			return false;
		} catch (Exception e) {
			logger.error(e.toString());
			return false;
		}
	}
}
