package vpago.dataapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase para generar el formato de comercio para enviar al vadmin
 * @author Emily
 *
 */
@XmlRootElement
@XmlType(name="merchant" ,propOrder={"merchantGuid", "merchantType", "corporationName", "parentMerchantId", "industryTypeName", 
		"taxIdentifierValue", "merchantName", "fantasyName", "merchantActive", "countryName", "cityName", "adminEntityLevel1Name", 
		"address"})
public class SimpleMerchant {
	
	public SimpleMerchant() {
		super();
	}
	
	public SimpleMerchant(String merchantGuid, String merchantType, String corporationName, Integer parentMerchantId,
			String industryTypeName, String taxIdentifierValue, String merchantName, String fantasyName,
			boolean merchantActive, String countryName, String cityName, String adminEntityLevel1Name, String address) {
		super();
		this.merchantGuid = merchantGuid;
		this.merchantType = merchantType;
		this.corporationName = corporationName;
		this.parentMerchantId = parentMerchantId;
		this.industryTypeName = industryTypeName;
		this.taxIdentifierValue = taxIdentifierValue;
		this.merchantName = merchantName;
		this.fantasyName = fantasyName;
		this.merchantActive = merchantActive;
		this.countryName = countryName;
		this.cityName = cityName;
		this.adminEntityLevel1Name = adminEntityLevel1Name;
		this.address = address;
	}
	
	private String merchantGuid;
	private String merchantType;
	private String corporationName;
	private Integer parentMerchantId;
	private String industryTypeName;
	private String taxIdentifierValue;
	private String merchantName;
	private String fantasyName;
	private boolean merchantActive;
	private String countryName;
	private String cityName;
	private String adminEntityLevel1Name;
	private String address;
	
	public String getMerchantGuid() {
		return merchantGuid;
	}

	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getCorporationName() {
		return corporationName;
	}

	public void setCorporationName(String corporationName) {
		this.corporationName = corporationName;
	}

	public Integer getParentMerchantId() {
		return parentMerchantId;
	}

	public void setParentMerchantId(Integer parentMerchantId) {
		this.parentMerchantId = parentMerchantId;
	}

	public String getIndustryTypeName() {
		return industryTypeName;
	}

	public void setIndustryTypeName(String industryTypeName) {
		this.industryTypeName = industryTypeName;
	}

	public String getTaxIdentifierValue() {
		return taxIdentifierValue;
	}

	public void setTaxIdentifierValue(String taxIdentifierValue) {
		this.taxIdentifierValue = taxIdentifierValue;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getFantasyName() {
		return fantasyName;
	}

	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	public boolean isMerchantActive() {
		return merchantActive;
	}

	public void setMerchantActive(boolean merchantActive) {
		this.merchantActive = merchantActive;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAdminEntityLevel1Name() {
		return adminEntityLevel1Name;
	}

	public void setAdminEntityLevel1Name(String adminEntityLevel1Name) {
		this.adminEntityLevel1Name = adminEntityLevel1Name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "SimpleMerchant [merchantGuid=" + merchantGuid + ", merchantType=" + merchantType + ", corporationName="
				+ corporationName + ", parentMerchantId=" + parentMerchantId + ", industryTypeName=" + industryTypeName
				+ ", taxIdentifierValue=" + taxIdentifierValue + ", merchantName=" + merchantName + ", fantasyName="
				+ fantasyName + ", merchantActive=" + merchantActive + ", countryName=" + countryName + ", cityName="
				+ cityName + ", adminEntityLevel1Name=" + adminEntityLevel1Name + ", address=" + address + "]";
	}
	
	
}
