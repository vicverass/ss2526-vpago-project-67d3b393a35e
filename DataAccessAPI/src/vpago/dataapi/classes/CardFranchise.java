package vpago.dataapi.classes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una franquicia procesadora de tarjetas
 */
@XmlRootElement
@XmlType(name="cardFranchise", propOrder={"id", "name"})
@Entity
@Table(name="card_franchise")
public class CardFranchise {
	
	public CardFranchise() {
		super();
	}
	
	public CardFranchise(String name) {
		super();
		this.name = name;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre de la franquicia */ 
	private String name;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="card_franchise_id_seq")
	@SequenceGenerator(name="card_franchise_id_seq", sequenceName = "card_franchise_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardFranchise other = (CardFranchise) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CardFranchise [id=" + id + ", name=" + name + "]";
	}
	
}
