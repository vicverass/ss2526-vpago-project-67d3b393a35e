package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Dispositivo asignado a un Terminal
 */
@XmlRootElement
@XmlType(propOrder={"id","serialNumberDevice", "deviceId", "serialNumberSimCard", "operatorName"})
@Entity
@Table(name="assigned_device")
public class AssignedDevice extends APIResponse {
	
	public AssignedDevice() {
		super();
	}
	
	public AssignedDevice(String serialNumberDevice, int deviceId, Device device, String serialNumberSimCard,
			String operatorName) {
		super();
		this.serialNumberDevice = serialNumberDevice;
		this.deviceId = device.getId();
		this.device = device;
		this.serialNumberSimCard = serialNumberSimCard;
		this.operatorName = operatorName;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Serial del Dispositivo Asignado */
	private String serialNumberDevice;
	/** Identificador de Dispositivo Asignado */
	private int deviceId;
	/** Dispositivo Asignado */
	private Device device;
	/** Serial del Sim Card Asignado */
	private String serialNumberSimCard;
	/** Nombre de la operadora a la que pertenece el Sim Card Asignado */
	private String operatorName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="assigned_device_id_seq")
	@SequenceGenerator(name="assigned_device_id_seq", sequenceName = "assigned_device_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the serialNumber
	 */
	@Column(name="serial_number")
	@NotNull
	public String getSerialNumberDevice() {
		return serialNumberDevice;
	}

	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumberDevice(String serialNumberDevice) {
		this.serialNumberDevice = serialNumberDevice;
	}

	/**
	 * @return the deviceId
	 */
	@Column(name="device_id")
	public int getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the device
	 */
	@XmlTransient
	@ManyToOne( fetch=FetchType.EAGER )
	@JoinColumn(name = "device_id", insertable=false, updatable=false)
	public Device getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
		if(device != null) this.deviceId = device.getId();
	}

	/**
	 * 
	 * @return serialNumberSimCard
	 */
	@Column(name="serial_number_simcard")
	public String getSerialNumberSimCard() {
		return serialNumberSimCard;
	}

	/**
	 * 
	 * @param serialNumberSimCard
	 */
	public void setSerialNumberSimCard(String serialNumberSimCard) {
		this.serialNumberSimCard = serialNumberSimCard;
	}

	/**
	 * 
	 * @return operatorName
	 */
	@Column(name="operator_name")
	public String getOperatorName() {
		return operatorName;
	}

	/**
	 * 
	 * @param operatorName
	 */
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AssignedDevice [id=" + id + ", serialNumberDevice=" + serialNumberDevice + ", device=" + device + "]";
	}
	
}
