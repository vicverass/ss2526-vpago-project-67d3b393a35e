package vpago.dataapi.classes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar las respuestas de las transacciones
 */
@XmlRootElement
@XmlType(propOrder={"id", "code", "name", "paymentChannelId", "description"})
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@Table(name="transaction_response")
public class TransactionResponse extends APIResponse {

	public TransactionResponse() {
		super(); 
	}

	public TransactionResponse(String code, String name, Integer paymentChannelId, String description) {
		super();
		this.id = null;
		this.code = code;
		this.name = name;
		this.paymentChannelId = paymentChannelId;
		this.description = description;
	}

	/** Identificador generado por el motor base de datos */
	protected Integer id;
	/** Identificador unico de la transaccion */
	protected String code;
	/** nombre del parametro de la transaccion */
	protected String name;
	/** Valor del parametro de la transaccion */
	protected Integer paymentChannelId;
	/** Tipo del parametro de la transaccion */
	protected String description;
	
	/**
	 * 
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_response_id_seq")
	@SequenceGenerator(name="transaction_response_id_seq", sequenceName = "transaction_response_id_seq")
	public Integer getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return the code
	 */
	@XmlElement
	@Column(name="code")
	public String getCode() {
		return code;
	}
	
	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * 
	 * @return the name
	 */
	@XmlElement
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return the paymentChannelId
	 */
	@XmlElement
	@Column(name="payment_channel_id")
	public Integer getPaymentChannelId() {
		return paymentChannelId;
	}
	
	/**
	 * 
	 * @param paymentChannelId
	 */
	public void setPaymentChannelId(Integer paymentChannelId) {
		this.paymentChannelId = paymentChannelId;
	}
	
	/**
	 * 
	 * @return the description
	 */
	@XmlElement
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	
	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "TransactionResponse [id=" + id + ", code=" + code + ", name=" + name + ", paymentChannelId="
				+ paymentChannelId + ", description=" + description + "]";
	}	

}
