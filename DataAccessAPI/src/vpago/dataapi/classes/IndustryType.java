package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Tipo de Industria segun el Banco Central de Venezuela
 */
@XmlRootElement
@XmlType(propOrder={"id", "number", "name", "sectorId"})
@Entity
@Table(name="industry_type")
public class IndustryType  extends APIResponse {
	
	public IndustryType() {
		super();
	}
	
	public IndustryType(String number, String name, Sector sector) {
		super();
		this.number = number;
		this.name = name;
		this.sectorId = sector.getId();
		this.sector = sector;
	}
	
	public IndustryType(int id, String number, String name, Sector sector) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.sectorId = sector.getId();
		this.sector = sector;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Numero */
	private String number;
	/** Nombre del Tipo de Industria */
	private String name;
	/** Identificador del Sector al que esta asociado el Tipo de Industria */
	private int sectorId;
	/** Sector al que esta asociado el Tipo de Industria */
	private Sector sector;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="industry_type_id_seq")
	@SequenceGenerator(name="industry_type_id_seq", sequenceName = "industry_type_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the number
	 */
	@Column(name="industry_type_number")
	public String getNumber() {
		return number;
	}
	
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	
	/**
	 * @return the name
	 */
	@Column(name="industry_type_name")
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the sectorId
	 */
	@Column(name="sector_id")
	public int getSectorId() {
		return sectorId;
	}

	/**
	 * @param sectorId the sectorId to set
	 */
	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	/**
	 * @return the sector
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sector_id", insertable=false, updatable=false)
	public Sector getSector() {
		return sector;
	}
	
	/**
	 * @param sector the sector to set
	 */
	public void setSector(Sector sector) {
		this.sectorId = sector.getId();
		this.sector = sector;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((sector == null) ? 0 : sector.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndustryType other = (IndustryType) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (sector == null) {
			if (other.sector != null)
				return false;
		} else if (!sector.equals(other.sector))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IndustryType [id=" + id + ", number=" + number + ", name=" + name + ", sector=" + sector + "]";
	}
	
}
