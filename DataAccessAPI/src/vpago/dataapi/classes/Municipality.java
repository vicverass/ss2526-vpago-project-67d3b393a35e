package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Municipio
 */
@XmlRootElement
@XmlType(propOrder={"id", "stateId", "municipalityName"})
@Entity
@Table(name="municipality")
public class Municipality extends APIResponse {
	
	public Municipality() {
		super();
	}
	
	public Municipality(State state, Integer stateId, String municipalityName) {
		super();
		this.state = state;
		this.stateId = stateId;
		this.municipalityName = municipalityName;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Estado al que esta asociado el municipio */
	private State state;
	/** Identificador del estado al que esta asociado el municipio */
	private Integer stateId;
	/** Nombre del municipio */
	private String municipalityName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="municipality_id_seq")
	@SequenceGenerator(name="municipality_id_seq", sequenceName = "municipality_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the state
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "state_id")
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the stateId
	 */
	@Column(name="state_id", updatable=false, insertable=false)
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the municipalityName
	 */
	@Column(name="municipality")
	public String getMunicipalityName() {
		return municipalityName;
	}

	/**
	 * @param municipalityName the municipalityName to set
	 */
	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((municipalityName == null) ? 0 : municipalityName.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((stateId == null) ? 0 : stateId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipality other = (Municipality) obj;
		if (id != other.id)
			return false;
		if (municipalityName == null) {
			if (other.municipalityName != null)
				return false;
		} else if (!municipalityName.equals(other.municipalityName))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (stateId == null) {
			if (other.stateId != null)
				return false;
		} else if (!stateId.equals(other.stateId))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Municipality [id=" + id + ", state=" + state + ", stateId=" + stateId + ", municipalityName="
				+ municipalityName + "]";
	}
	
	
}
