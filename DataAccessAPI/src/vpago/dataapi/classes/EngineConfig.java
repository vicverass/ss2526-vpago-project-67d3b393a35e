package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="engine_config")
public class EngineConfig {
	
	public EngineConfig() {
		super();
	}
	
	private Integer id;
	private String affiliatedCounter;
	private String terminalCounter;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="engine_config_id_seq")
	@SequenceGenerator(name="engine_config_id_seq", sequenceName = "engine_config_id_seq")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the affiliatedCounter
	 */
	@Column(name="affiliated_counter")
	public String getAffiliatedCounter() {
		return affiliatedCounter;
	}
	
	/**
	 * @param affiliatedCounter the affiliatedCounter to set
	 */
	public void setAffiliatedCounter(String affiliatedCounter) {
		this.affiliatedCounter = affiliatedCounter;
	}
	
	/**
	 * @return the terminalCounter
	 */
	@Column(name="terminal_counter")
	public String getTerminalCounter() {
		return terminalCounter;
	}
	
	/**
	 * @param terminalCounter the terminalCounter to set
	 */
	public void setTerminalCounter(String terminalCounter) {
		this.terminalCounter = terminalCounter;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EngineConfig [id=" + id + ", affiliatedCounter=" + affiliatedCounter + ", terminalCounter="
				+ terminalCounter + "]";
	}
	
	
}
