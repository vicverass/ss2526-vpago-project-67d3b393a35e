package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Adquiriente
 */
@XmlRootElement
@XmlType(propOrder={"id", "displayName", "merchantIdCode", "affiliationId", "bankId", "bank"})
@Entity
@Table(name="acquirer")
public class Acquirer extends APIResponse {
	
	public Acquirer() {
		super();
	}
	
	public Acquirer(String merchantIdCode, Affiliation affiliation,
			Bank bank) {
		super();
		this.displayName =  affiliation.getMerchant().getRif() + " " + affiliation.getMerchant().getName() + " " + bank.getName();
		this.merchantIdCode = merchantIdCode;
		this.affiliationId = affiliation.getId();
		this.affiliation = affiliation;
		this.bankId = bank.getId();
		this.bank = bank;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Campo identificador del Comercio en el Banco (42 ISO8583) */
	private String merchantIdCode;
	/** Identificador del Comercio al que esta asociado el Adquiriente */
	private int affiliationId;
	/** Comercio al que esta asociado el Adquiriente */
	private Affiliation affiliation;
	/** Identificador de Banco asociado al Adquiriente */
	private int bankId;
	/** Banco asociado al Adquiriente */
	private Bank bank;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="acquirer_id_seq")
	@SequenceGenerator(name="acquirer_id_seq", sequenceName = "acquirer_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	@NotNull
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the merchantIdCode
	 */
	@Column(name="merchant_id_code")
	public String getMerchantIdCode() {
		return merchantIdCode;
	}

	/**
	 * @param merchantIdCode the merchantIdCode to set
	 */
	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}

	/**
	 * @return the affiliationId
	 */
	@Column(name="affiliation_id")
	public int getAffiliationId() {
		return affiliationId;
	}

	/**
	 * @param affiliationId the affiliationId to set
	 */
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}

	/**
	 * @return the affiliation
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}

	/**
	 * @return the bankId
	 */
	@Column(name="bank_id")
	public int getBankId() {
		return bankId;
	}

	/**
	 * @param bankId the bankId to set
	 */
	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	/**
	 * @return the bank
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "bank_id", insertable=false, updatable=false)
	public Bank getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(Bank bank) {
		this.bank = bank;
		if(bank != null) this.bankId = bank.getId();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((affiliation == null) ? 0 : affiliation.hashCode());
		result = prime * result + affiliationId;
		result = prime * result + ((bank == null) ? 0 : bank.hashCode());
		result = prime * result + bankId;
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + id;
		result = prime * result + ((merchantIdCode == null) ? 0 : merchantIdCode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acquirer other = (Acquirer) obj;
		if (affiliation == null) {
			if (other.affiliation != null)
				return false;
		} else if (!affiliation.equals(other.affiliation))
			return false;
		if (affiliationId != other.affiliationId)
			return false;
		if (bank == null) {
			if (other.bank != null)
				return false;
		} else if (!bank.equals(other.bank))
			return false;
		if (bankId != other.bankId)
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (id != other.id)
			return false;
		if (merchantIdCode == null) {
			if (other.merchantIdCode != null)
				return false;
		} else if (!merchantIdCode.equals(other.merchantIdCode))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Acquirer [id=" + id + ", displayName=" + displayName + ", merchantIdCode=" + merchantIdCode
				+ ", affiliation=" + affiliation + ", bank=" + bank + "]";
	}
}
