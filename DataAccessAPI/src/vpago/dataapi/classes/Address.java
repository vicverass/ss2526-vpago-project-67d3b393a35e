package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase persistente para representar una Direccion
 */
@XmlType(propOrder={"name", "location", "property", "postalZoneId", "postalZoneName", "parishId", "parishName", "municipalityId",  "municipalityName", "cityId", "cityName", "stateId", "stateName", "countryId", "countryName"})
@Entity
@Table(name="address")
public class Address {
	private static final Logger logger = LoggerFactory.getLogger(Address.class);
	
	public Address() {
		super();
	}
	
	public Address(String name, String location, String property, Parish parish, Municipality municipality, City city, State state, Country country, PostalZone postalZone) {
		this.name = name.isEmpty() || name == null ? "" : name;
		this.location = location.isEmpty() || location == null ? "" : location;
		this.property = property.isEmpty() || property == null ? "" : property;
		this.parish = parish;
		this.municipality = municipality;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalZone = postalZone;
	}
	
	public void update(Address other) {
		logger.trace("Update address");
		this.name = this.name != other.name ? other.name : this.name;
		this.location = this.location != other.location ? other.location : this.location;
		this.property = this.property != other.property ? other.property : this.property;
		this.parish = this.parish != other.parish ? other.parish : this.parish;
		this.municipality = this.municipality != other.municipality ? other.municipality : this.municipality;
		this.city = this.city != other.city ? other.city : this.city;
		this.state = this.state != other.state ? other.state : this.state;
		this.country = this.country != other.country ? other.country : this.country;
		this.postalZone = this.postalZone != other.postalZone ? other.postalZone : this.postalZone;
//		this.setPostalZone(other.getPostalZone());
//		this.setName(other.getName());
//		this.setLocation(other.getLocation());
//		this.setProperty(other.getProperty());
//		this.setParish(other.getParish());
//		this.setMunicipality(other.getMunicipality());
//		this.setCity(other.getCity());
//		this.setState(other.getState());
//		this.setCountry(other.getCountry());
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre y Apellido o Razon Social */
	private String name;
	/** Localidad */
	private String location;
	/** Inmueble */
	private String property;
	/** Parroquia  */
	private Parish parish;
	/** Identificador de la parroquia **/
	private Integer parishId;
	/** Municipio  */
	private Municipality municipality;
	/** Identificador del municipio **/
	private Integer municipalityId;
	/** Ciudad */
	private City city;
	/** Identificador de la ciudad **/
	private Integer cityId;
	/** Estado */
	private State state;
	/** Identificador del estado **/
	private Integer stateId;
	/** Pais */
	private Country country;
	/** Identificador del pais **/
	private Integer countryId;
	/** Zona Postal */
	private PostalZone postalZone;
	/** Identificador de la zona postal **/
	private Integer postalZoneId;
	
	/**
	 * @return the id
	 */
	@XmlTransient
	@Id @GeneratedValue(generator="address_id_seq")
	@SequenceGenerator(name="address_id_seq", sequenceName = "address_id_seq")	
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @param property the property to set
	 */
	public void setProperty(String property) {
		this.property = property;
	}

	/**
	 * @return the parish
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parish_id")
	public Parish getParish() {
		return parish;
	}

	/**
	 * @param parish the parish to set
	 */
	public void setParish(Parish parish) {
		this.parish = parish;
	}

	/**
	 * @return the municipality
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "municipality_id")
	public Municipality getMunicipality() {
		return municipality;
	}

	/**
	 * @param municipality the municipality to set
	 */
	public void setMunicipality(Municipality municipality) {
		this.municipality = municipality;
	}

	/**
	 * @return the city
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id")
	public City getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(City city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "state_id")
	public State getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @return the country
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country_id")
	public Country getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}

	/**
	 * @return the postalZone
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "postal_zone_id")
	public PostalZone getPostalZone() {
		return postalZone;
	}

	/**
	 * @param postalZone the postalZone to set
	 */
	public void setPostalZone(PostalZone postalZone) {
		this.postalZone = postalZone;
	}

	/**
	 * @return the parishId
	 */
	@Column(name="parish_id", updatable=false, insertable=false)
	public Integer getParishId() {
		return parishId;
	}

	/**
	 * @param parishId the parishId to set
	 */
	public void setParishId(Integer parishId) {
		this.parishId = parishId;
	}

	/**
	 * @return the municipalityId
	 */
	@Column(name="municipality_id", updatable=false, insertable=false)
	public Integer getMunicipalityId() {
		return municipalityId;
	}

	/**
	 * @param municipalityId the municipalityId to set
	 */
	public void setMunicipalityId(Integer municipalityId) {
		this.municipalityId = municipalityId;
	}

	/**
	 * @return the cityId
	 */
	@Column(name="city_id", updatable=false, insertable=false)
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the stateId
	 */
	@Column(name="state_id", updatable=false, insertable=false)
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the countryId
	 */
	@Column(name="country_id", updatable=false, insertable=false)
	public Integer getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the postalZoneId
	 */
	@Column(name="postal_zone_id", updatable=false, insertable=false)
	public Integer getPostalZoneId() {
		return postalZoneId;
	}

	/**
	 * @param postalZoneId the postalZoneId to set
	 */
	public void setPostalZoneId(Integer postalZoneId) {
		this.postalZoneId = postalZoneId;
	}
	
	/**
	 * @return the postalZoneName
	 */
	@Transient
	public String getPostalZoneName() {
		return this.postalZone.getTownName() + " - " + this.getPostalZone().getPostalCode();
	}

	/**
	 * @param postalZoneName the postalZoneName to set
	 */
	public void setPostalZoneName(String postalZoneName) {
		return;
		//this.postalZoneName = this.postalZone.getTownName() + " - " + this.postalZone.getPostalCode();
	}

	/**
	 * @return the parishName
	 */
	@Transient
	public String getParishName() {
		return this.parish.getParishName();
	}

	/**
	 * @param parishName the parishName to set
	 */
	public void setParishName(String parishName) {
		return;
//		this.parishName = this.parish.getParishName();
	}

	/**
	 * @return the muncipalityName
	 */
	@Transient
	public String getMunicipalityName() {
		return this.municipality.getMunicipalityName();
	}

	/**
	 * @param muncipalityName the muncipalityName to set
	 */
	public void setMunicipalityName(String municipalityName) {
		return;
//		this.municipalityName = this.municipality.getMunicipalityName();
	}

	/**
	 * @return the cityName
	 */
	@Transient
	public String getCityName() {
		return this.city.getCityName();
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		return;
//		this.cityName = this.city.getCityName();
	}

	/**
	 * @return the stateName
	 */
	@Transient
	public String getStateName() {
		return this.state.getStateName();
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		return;
//		this.stateName = this.state.getStateName();
	}

	/**
	 * @return the countryName
	 */
	@Transient
	public String getCountryName() {
		return this.country.getCountryName();
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		return;
//		this.countryName = this.country.getCountryName();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + id;
		result = prime * result + ((parish == null) ? 0 : parish.hashCode());
		result = prime * result + ((municipality == null) ? 0 : municipality.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
//		if (id != other.id)
//			return false;
		if (parish == null) {
			if (other.parish != null)
				return false;
		} else if (!parish.equals(other.parish))
			return false;
		if (municipality == null) {
			if (other.municipality != null)
				return false;
		} else if (!municipality.equals(other.municipality))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Address [id=" + id + ", name=" + name + ", location=" + location + ", property=" + property
				+ ", parish=" + parish + ", parishId=" + parishId + ", municipality=" + municipality
				+ ", municipalityId=" + municipalityId + ", city=" + city + ", cityId=" + cityId + ", state=" + state
				+ ", stateId=" + stateId + ", country=" + country + ", countryId=" + countryId + ", postalZone="
				+ postalZone + ", postalZoneId=" + postalZoneId + "]";
	}


	
}
