package vpago.dataapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.dataapi.types.ResponseType;

/**
 * Clase para el envio de mensajes en la respuesta
 */
@XmlRootElement
@XmlType(name="", propOrder={"type", "name", "description"})
public class Response extends APIResponse{
	
	public Response() { 
		super(); 
	}
	
	public Response(ResponseType type, String name, String description) {
		super();
		this.type = type;
		this.name = name;
		this.description = description;
	}
	
	/** Tipo de Respuesta */
	private ResponseType type;
	/** Nombre del mensaje */
	private String name;
	/** Descripcion del mensaje */
	private String description;
	
	/**
	 * @return the type
	 */
	public ResponseType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ResponseType type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Response [type=" + type + ", name=" + name + ", description=" + description + "]";
	}
	
}
