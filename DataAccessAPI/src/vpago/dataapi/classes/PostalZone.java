package vpago.dataapi.classes;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Codigo Postal
 */
@XmlRootElement
@XmlType(propOrder={"id", "parishId", "townName", "postalCode"})
@Entity
@Table(name="postal_zone")
public class PostalZone extends APIResponse {
	
	public PostalZone() {
		super();
	}
	
	public PostalZone(Parish parish, Integer parishId, String townName, String postalCode) {
		super();
		this.parish = parish;
		this.parishId = parishId;
		this.townName = townName;
		this.postalCode = postalCode;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Parroquia a la que esta asociado el codigo postal */
	private Parish parish;
	/** Identificador de la parroquia a la que esta asociado el codigo postal */
	private Integer parishId;
	/** Nombre de la poblacion */
	private String townName;
	/** Codigo postal */
	private String postalCode;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="postal_zone_id_seq")
	@SequenceGenerator(name="postal_zone_id_seq", sequenceName = "postal_zone_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the parish
	 */
	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parish_id")
	public Parish getParish() {
		return parish;
	}

	/**
	 * @param parish the parish to set
	 */
	public void setParish(Parish parish) {
		this.parish = parish;
	}

	/**
	 * @return the parishId
	 */
	@Column(name="parish_id", updatable=false, insertable=false)
	public Integer getParishId() {
		return parishId;
	}

	/**
	 * @param parishId the parishId to set
	 */
	public void setParishId(Integer parishId) {
		this.parishId = parishId;
	}

	/**
	 * @return the townName
	 */
	@Column(name="town")
	public String getTownName() {
		return townName;
	}

	/**
	 * @param townName the townName to set
	 */
	public void setTownName(String townName) {
		this.townName = townName;
	}

	/**
	 * @return the postalCode
	 */
	@Column(name="postal_code")
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((parish == null) ? 0 : parish.hashCode());
		result = prime * result + ((parishId == null) ? 0 : parishId.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((townName == null) ? 0 : townName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostalZone other = (PostalZone) obj;
		if (id != other.id)
			return false;
		if (parish == null) {
			if (other.parish != null)
				return false;
		} else if (!parish.equals(other.parish))
			return false;
		if (parishId == null) {
			if (other.parishId != null)
				return false;
		} else if (!parishId.equals(other.parishId))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (townName == null) {
			if (other.townName != null)
				return false;
		} else if (!townName.equals(other.townName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostalZone [id=" + id + ", parish=" + parish + ", parishId=" + parishId + ", townName=" + townName
				+ ", postalCode=" + postalCode + "]";
	}
	
	
}
