package vpago.dataapi.classes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar un Tipo de Transaccion
 */
@XmlRootElement
@XmlType(propOrder={"id","name", "description", "templateMessage", "isoTemplate", "methodName", 
		"timeout", "retry", "retryAttempts", "transactionResponseType", "transactionInverseType", "reportValidation"})
@Entity
@Table(name="transaction_type")
public class TransactionType extends APIResponse {
	
	public TransactionType() {
		super();
	}
	
	public TransactionType(String name, String description, Integer templateMessage, Integer isoTemplate,
			String methodName, double timeout, boolean retry, int retryAttempts, Integer transactionResponseType,
			Integer transactionInverseType, boolean reportValidation) {
		super();
		this.name = name;
		this.description = description;
		this.templateMessage = templateMessage;
		this.isoTemplate = isoTemplate;
		this.methodName = methodName;
		this.timeout = timeout;
		this.retry = retry;
		this.retryAttempts = retryAttempts;
		this.transactionResponseType = transactionResponseType;
		this.transactionInverseType = transactionInverseType;
		this.reportValidation = reportValidation;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del metodo */
	private String name;
	/** Descripcion opcional del metodo */
	private String description;
	/** Plantilla de Mensajes a la que esta asociado el metodo */
	private Integer templateMessage;
	/** Plantilla para construir el mensaje iso asociado al metodo */
	private Integer isoTemplate;
	/** Nombre de la primitiva a invocar en el codigo */
	private String methodName;
	/** Tiempo maximo de espera en segundos */
	private double timeout;
	/** Si se puede reintentar la transaccion dada la ocurrencia de timeout */
	private boolean retry;
	/** Numero de maximo de reintentos si ocurre timeout */
	private int retryAttempts;
	/** Tipo de transaccion que debe recibir como respuesta */
	private Integer transactionResponseType;
	/** Tipo de transaccion inversa a invocar en caso de fallo o no respuesta */
	private Integer transactionInverseType;
	private boolean reportValidation;
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_type_id_seq")
	@SequenceGenerator(name="transaction_type_id_seq", sequenceName = "transaction_type_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the templateMessage
	 */
	/*@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "template_message_id")*/
	@Column(name = "template_message_id")
	public Integer getTemplateMessage() {
		return templateMessage;
	}

	/**
	 * @param templateMessage the templateMessage to set
	 */
	public void setTemplateMessage(Integer templateMessage) {
		this.templateMessage = templateMessage;
	}

	/**
	 * @return the isoTemplate
	 */
	/*@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "iso_template_id")*/
	@Column(name = "iso_template_id")
	public Integer getIsoTemplate() {
		return isoTemplate;
	}

	/**
	 * @param isoTemplate the isoTemplate to set
	 */
	public void setIsoTemplate(Integer isoTemplate) {
		this.isoTemplate = isoTemplate;
	}

	/**
	 * @return the methodName
	 */
	@Column(name="method_name")
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the timeout
	 */
	public double getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(double timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the retry
	 */
	public boolean isRetry() {
		return retry;
	}

	/**
	 * @param retry the retry to set
	 */
	public void setRetry(boolean retry) {
		this.retry = retry;
	}

	/**
	 * @return the retryAttempts
	 */
	@Column(name="retry_attempts")
	public int getRetryAttempts() {
		return retryAttempts;
	}

	/**
	 * @param retryAttempts the retryAttempts to set
	 */
	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}
	
	/**
	 * @return the transactionResponseType
	 */
	/*@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transaction_response_type")*/
	@Column(name = "transaction_response_type")
	public Integer getTransactionResponseType() {
		return transactionResponseType;
	}

	/**
	 * @param transactionResponseType the transactionResponseType to set
	 */
	public void setTransactionResponseType(Integer transactionResponseType) {
		this.transactionResponseType = transactionResponseType;
	}
	
	/**
	 * @return the transactionInverseType
	 */
	/*@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "transaction_inverse_type")*/
	@Column(name = "transaction_inverse_type")
	public Integer getTransactionInverseType() {
		return transactionInverseType;
	}

	/**
	 * @param transactionInverseType the transactionInverseType to set
	 */
	public void setTransactionInverseType(Integer transactionInverseType) {
		this.transactionInverseType = transactionInverseType;
	}
	
	
	/**
	 * 
	 * @return reportValidation
	 */
	//@XmlTransient
	@Column(name ="report_validation")
	public boolean isReportValidation() {
		return reportValidation;
	}

	public void setReportValidation(boolean reportValidation) {
		this.reportValidation = reportValidation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((isoTemplate == null) ? 0 : isoTemplate.hashCode());
		result = prime * result + ((methodName == null) ? 0 : methodName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((templateMessage == null) ? 0 : templateMessage.hashCode());
		result = prime * result + (retry ? 1231 : 1237);
		result = prime * result + retryAttempts;
		long temp;
		temp = Double.doubleToLongBits(timeout);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((transactionResponseType == null) ? 0 : transactionResponseType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransactionType other = (TransactionType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (isoTemplate == null) {
			if (other.isoTemplate != null)
				return false;
		} else if (!isoTemplate.equals(other.isoTemplate))
			return false;
		if (methodName == null) {
			if (other.methodName != null)
				return false;
		} else if (!methodName.equals(other.methodName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (templateMessage == null) {
			if (other.templateMessage != null)
				return false;
		} else if (!templateMessage.equals(other.templateMessage))
			return false;
		if (retry != other.retry)
			return false;
		if (retryAttempts != other.retryAttempts)
			return false;
		if (Double.doubleToLongBits(timeout) != Double.doubleToLongBits(other.timeout))
			return false;
		if (transactionResponseType == null) {
			if (other.transactionResponseType != null)
				return false;
		} else if (!transactionResponseType.equals(other.transactionResponseType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TransactionType [id=" + id + ", name=" + name + ", description=" + description + ", templateMessage="
				+ templateMessage + ", isoTemplate=" + isoTemplate + ", methodName=" + methodName + ", timeout="
				+ timeout + ", retry=" + retry + ", retryAttempts=" + retryAttempts + ", transactionResponseType="
				+ transactionResponseType + ", transactionInverseType=" + transactionInverseType + ", reportValidation="
				+ reportValidation + "]";
	}

}
