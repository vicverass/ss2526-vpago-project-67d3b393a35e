package vpago.dataapi.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Regla por Proporcion
 */
@XmlRootElement
@XmlType(propOrder={"proportion"})
@Entity
@DiscriminatorValue("proportion")
public class RuleProportion extends Rule {
	
	public RuleProportion() {
		super();
	}

	public RuleProportion(String displayName, String description, Acquirer acquirer,
			Affiliation affiliation, int proportion) {
		super(displayName, description, acquirer, affiliation);
		this.proportion = proportion;
	}

	public void update(RuleProportion other) {
		this.update((Rule)other);
		this.proportion = other.proportion;
	}
	
	/** Proporcion */
	private int proportion;

	/**
	 * @return the proportion
	 */
	public int getProportion() {
		return proportion;
	}

	/**
	 * @param proportion the proportion to set
	 */
	public void setProportion(int proportion) {
		this.proportion = proportion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProportion [proportion=" + proportion + ", id=" + id + ", displayName=" + displayName
				+ ", description=" + description + ", acquirer=" + acquirer
				+ ", affiliation=" + affiliation + "]";
	}
	
}
