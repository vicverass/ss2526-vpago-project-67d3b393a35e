package vpago.dataapi.classes;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="current_batch")
public class CurrentBatch {
	
	public CurrentBatch() {
		super();
	}
	
	public CurrentBatch(Terminal terminal, Integer batchNumber, Double totalAmount, Integer totalTrx,
			BatchType batchType, Integer referenceCounter) {
		super();
		this.terminal = terminal;
		this.batchNumber = batchNumber;
		this.totalAmount = totalAmount;
		this.totalTrx = totalTrx;
		this.batchType = batchType;
		this.referenceCounter = referenceCounter;
	}

	/** Identificador generado por el motor base de datos */
	private Integer id;
	/** Terminal **/
	private Terminal terminal;
	/** Numero de lote **/
	private Integer batchNumber;
	/** Monto total **/
	private Double totalAmount;
	/** Numero total de transacciones **/
	private Integer totalTrx;
	/** Tipo de lote **/
	private BatchType batchType;
	/** Lista de transacciones asociadas al lote actual **/
	private List<Transaction> transactions;
	/** Contador de referencias **/
	private Integer referenceCounter;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="current_batch_id_seq")
	@SequenceGenerator(name="current_batch_id_seq", sequenceName = "current_batch_id_seq")
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * @return the terminal
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminal() {
		return terminal;
	}
	
	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}
	
	/**
	 * @return the batchNumber
	 */
	@Column(name="batch_number")
	public Integer getBatchNumber() {
		return batchNumber;
	}
	
	/**
	 * @param batchNumber the batchNumber to set
	 */
	public void setBatchNumber(Integer batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	/**
	 * @return the totalAmount
	 */
	@Column(name="total_amount")
	public Double getTotalAmount() {
		return totalAmount;
	}
	
	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	/**
	 * @return the totalTrx
	 */
	@Column(name="total_trx")
	public Integer getTotalTrx() {
		return totalTrx;
	}
	
	/**
	 * @param totalTrx the totalTrx to set
	 */
	public void setTotalTrx(Integer totalTrx) {
		this.totalTrx = totalTrx;
	}
	
	/**
	 * @return the batchType
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "batch_type_id")
	public BatchType getBatchType() {
		return batchType;
	}
	
	/**
	 * @param batchType the batchType to set
	 */
	public void setBatchType(BatchType batchType) {
		this.batchType = batchType;
	}
	
	/**
	 * @return the transactions
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="current_batch_id")
	public List<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	/**
	 * @return the referenceCounter
	 */
	@Column(name="reference_counter")
	public Integer getReferenceCounter() {
		return referenceCounter;
	}

	/**
	 * @param referenceCounter the referenceCounter to set
	 */
	public void setReferenceCounter(Integer referenceCounter) {
		this.referenceCounter = referenceCounter;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CurrentBatch [id=" + id + ", terminal=" + terminal + ", batchNumber=" + batchNumber + ", totalAmount="
				+ totalAmount + ", totalTrx=" + totalTrx + ", batchType=" + batchType 
				+ ", referenceCounter=" + referenceCounter + "]";
	}
	
}
