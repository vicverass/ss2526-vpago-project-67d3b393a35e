package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase persistente para representar una Regla General
 */
@XmlRootElement
@XmlType(propOrder={"generalProductId"})
@Entity
@DiscriminatorValue("general")
public class RuleProductGeneral extends Rule {
	
	public RuleProductGeneral() {
		super();
	}

//	public RuleProductGeneral(String displayName, String description, Acquirer acquirer,
//			Affiliation affiliation, GeneralCardProduct generalProduct) {
//		super(displayName, description, acquirer, affiliation);
//		this.generalProduct = generalProduct;
//		this.generalProductId = generalProduct.getId(); 
//	}
	
	public RuleProductGeneral(String displayName, String description, Acquirer acquirer,
			Affiliation affiliation, int generalProductId) {
		super(displayName, description, acquirer, affiliation);
		this.generalProductId = generalProductId;
	}

	public void update(RuleProductGeneral other) {
		this.update((Rule)other);
//		this.generalProduct = other.generalProduct;
		this.generalProductId = other.generalProductId;
	}
	
	/** Producto General */
	private GeneralCardProduct generalProduct;
	/** Identificador del Producto General */
	private int generalProductId;

//	/**
//	 * @return the generalProduct
//	 */
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "general_product_id")
//	public GeneralCardProduct getGeneralProduct() {
//		return generalProduct;
//	}
//
//	/**
//	 * @param generalProduct the generalProduct to set
//	 */
//	public void setGeneralProduct(GeneralCardProduct generalProduct) {
//		this.generalProduct = generalProduct;
//	}
	
	/**
	 * @return the generalProductId
	 */
	@Column(name="general_product_id")
	public int getGeneralProductId() {
		return generalProductId;
	}

	/**
	 * @param generalProductId the generalProductId to set
	 */
	public void setGeneralProductId(int generalProductId) {
		this.generalProductId = generalProductId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleProductGeneral [generalProductId=" + generalProductId + ", id=" + id + ", displayName="
				+ displayName + ", description=" + description + ", acquirer=" + acquirer
				+ ", affiliation=" + affiliation + "]";
	}
}
