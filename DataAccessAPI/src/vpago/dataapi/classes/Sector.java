package vpago.dataapi.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/** 
 * Clase persistente para representar un Sector economico que agrupa Tipos de Industrias segun el Banco Central de Venezuela 
 */
@XmlRootElement
@XmlType(propOrder={"id", "number", "name"})
@Entity
@Table(name="sector")
public class Sector extends APIResponse {
	
	public Sector() {
		super();
	}
	
	public Sector(String number, String name) {
		super();
		this.number = number;
		this.name = name;
	}
	
	public Sector(int id, String number, String name) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Numero del Sector */
	private String number;
	/** Nombre del Sector */
	private String name;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="sector_id_seq")
	@SequenceGenerator(name="sector_id_seq", sequenceName = "sector_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the number
	 */
	@Column(name="sector_number")
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the name
	 */
	@Column(name="sector_name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sector other = (Sector) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sector [id=" + id + ", number=" + number + ", name=" + name + "]";
	}
	
}
