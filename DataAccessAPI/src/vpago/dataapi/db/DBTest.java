package vpago.dataapi.db;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.dataapi.classes.Corporation;

/**
 * Metodos para pruebas con persistencia de objetos en la base de datos
 */
public class DBTest {
	private static final Logger logger = LoggerFactory.getLogger(DBTest.class);
	
	public static void main(String[] args) {
	}
	
	/**
	 * Inserta una entidad Corporacion en la base de datos
	 */
	public static void insertCorporation() {
		Session session = null;
		Corporation corporation = new Corporation(null, "Corporacion de Prueba", null);
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			if(session != null) {
				session.beginTransaction();
				session.save(corporation);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			if(session != null) session.close();
		}
	}
	
}
