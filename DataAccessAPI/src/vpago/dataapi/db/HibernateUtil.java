package vpago.dataapi.db;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.Address;
import vpago.dataapi.classes.Affiliation;
import vpago.dataapi.classes.AssignedDevice;
import vpago.dataapi.classes.Bank;
import vpago.dataapi.classes.BankCardProduct;
import vpago.dataapi.classes.BatchType;
import vpago.dataapi.classes.Bin;
import vpago.dataapi.classes.CardFranchise;
import vpago.dataapi.classes.CardType;
import vpago.dataapi.classes.City;
import vpago.dataapi.classes.Contact;
import vpago.dataapi.classes.Corporation;
import vpago.dataapi.classes.Country;
import vpago.dataapi.classes.CurrentBatch;
import vpago.dataapi.classes.Device;
import vpago.dataapi.classes.DeviceToAssign;
import vpago.dataapi.classes.DeviceType;
import vpago.dataapi.classes.EngineConfig;
import vpago.dataapi.classes.GeneralCardProduct;
import vpago.dataapi.classes.IndustryType;
import vpago.dataapi.classes.Location;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Multimerchant;
import vpago.dataapi.classes.Municipality;
import vpago.dataapi.classes.Parish;
import vpago.dataapi.classes.PaymentChannel;
import vpago.dataapi.classes.PostalZone;
import vpago.dataapi.classes.Rule;
import vpago.dataapi.classes.RuleProduct;
import vpago.dataapi.classes.RuleProductGeneral;
import vpago.dataapi.classes.RuleProportion;
import vpago.dataapi.classes.RuleSet;
import vpago.dataapi.classes.Sector;
import vpago.dataapi.classes.State;
import vpago.dataapi.classes.TemplateMessage;
import vpago.dataapi.classes.Terminal;
import vpago.dataapi.classes.TerminalParameter;
import vpago.dataapi.classes.TimeFrame;
import vpago.dataapi.classes.Transaction;
import vpago.dataapi.classes.TransactionParameter;
import vpago.dataapi.classes.TransactionType;
import vpago.dataapi.classes.minified.TerminalMin;

/**
 * Clase para el manejo de la configuracion y funcionamiento del framework Hibernate
 */
public class HibernateUtil
{
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    static
    {
        try
        {
//          Configuration configuration = new Configuration();
//            Configuration configuration = new Configuration().configure();
        	//jdbc:postgresql
            Configuration cfg = new Configuration()
       				//HSQLDB
            		.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect")
    				.setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver")
    				.setProperty("hibernate.connection.username", "SA")
    				.setProperty("hibernate.connection.password", "")
    				.setProperty("hibernate.connection.url", "jdbc:hsqldb:hsql://localhost/engine_db")
    				.setProperty("hibernate.show_sql", "false")
    				.setProperty("hibernate.format_sql.password", "true")
    				
    				.setProperty("hibernate.c3p0.min_size", "1")
    				.setProperty("hibernate.c3p0.max_size", "100")
    				.setProperty("hibernate.c3p0.timeout", "0")
    				.setProperty("hibernate.c3p0.max_statements", "50")
    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
    				
	    			//ORACLE PRODUCCION
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINECONSULT")
//    				.setProperty("hibernate.connection.password", "agNG2011__")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.98:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "20")
//    				.setProperty("hibernate.c3p0.timeout", "3000")
//    				.setProperty("hibernate.c3p0.max_statements","100")
//    				.setProperty("hibernate.c3p0.idle_test_period", "20")
    				
//    				//ORACLE QA
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINETRANSACTION")
//    				.setProperty("hibernate.connection.password", "enginetransactionss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@10.20.184.172:1521:PROD")
////    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "100")
//    				//.setProperty("hibernate.c3p0.timeout", "0")
//    				.setProperty("hibernate.c3p0.max_statements", "50")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
    				
    				//ORACLE DESARROLLO
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINEDEVELOPMENT")
//    				.setProperty("hibernate.connection.password", "enginedevelopmentss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "100")
//    				.setProperty("hibernate.c3p0.timeout", "0")
//    				.setProperty("hibernate.c3p0.max_statements", "50")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
            		
    				//POSTGRES
//					.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect")
//    				.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver")
////    				.setProperty("hibernate.connection.username", "postgres")
////    				.setProperty("hibernate.connection.password", "vpagoss2526qa")
////    				.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost/engine_db")
//    				.setProperty("hibernate.connection.username", "vfinancialu")
//    				.setProperty("hibernate.connection.password", "vfinancialu")
//    				.setProperty("hibernate.connection.url", "jdbc:postgresql://192.168.1.120/vfinancialenginedb")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "true")
    							
    				.addAnnotatedClass(Merchant.class)
    				.addAnnotatedClass(Multimerchant.class)
    				.addAnnotatedClass(Location.class)
    				.addAnnotatedClass(Address.class)
    				.addAnnotatedClass(Contact.class)
		            .addAnnotatedClass(Device.class)
					.addAnnotatedClass(DeviceType.class)
					.addAnnotatedClass(AssignedDevice.class)
					.addAnnotatedClass(Terminal.class)
					.addAnnotatedClass(Rule.class)
					.addAnnotatedClass(RuleSet.class)
					.addAnnotatedClass(Acquirer.class)
					.addAnnotatedClass(Bank.class)
					.addAnnotatedClass(TimeFrame.class)
					.addAnnotatedClass(RuleProduct.class)
					.addAnnotatedClass(RuleProductGeneral.class)
					.addAnnotatedClass(RuleProportion.class)
					.addAnnotatedClass(Sector.class)
					.addAnnotatedClass(IndustryType.class)
					.addAnnotatedClass(Corporation.class)
					.addAnnotatedClass(TerminalParameter.class)
					.addAnnotatedClass(Affiliation.class)
					.addAnnotatedClass(PaymentChannel.class)
					.addAnnotatedClass(TemplateMessage.class)
					.addAnnotatedClass(BankCardProduct.class)
					.addAnnotatedClass(Bin.class)
					.addAnnotatedClass(CardFranchise.class)
					.addAnnotatedClass(CardType.class)
					.addAnnotatedClass(GeneralCardProduct.class)
					.addAnnotatedClass(Country.class)
					.addAnnotatedClass(State.class)
					.addAnnotatedClass(Municipality.class)
					.addAnnotatedClass(Parish.class)
					.addAnnotatedClass(City.class)
					.addAnnotatedClass(PostalZone.class)
					.addAnnotatedClass(DeviceToAssign.class)
					.addAnnotatedClass(EngineConfig.class)
					.addAnnotatedClass(Transaction.class)
					.addAnnotatedClass(TransactionParameter.class)
					.addAnnotatedClass(TransactionType.class)
					.addAnnotatedClass(BatchType.class)
					.addAnnotatedClass(CurrentBatch.class)
					;
            
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
    				cfg.getProperties()).build();
            sessionFactory = cfg.buildSessionFactory(serviceRegistry);
        }
        catch (HibernateException he)
        {
            logger.error("Error creando sesion de Hibernate: ", he.getMessage());
            throw new ExceptionInInitializerError(he);
        }
    }

    /**
     * Retorna una entidad necesaria para construir una sesion de Hibernate
     * 
     * @return La entidad SessionFactory
     */
    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    } 
}