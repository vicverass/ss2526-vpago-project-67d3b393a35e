package vpago.dataapi.db;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.dataapi.classes.IndustryType;
import vpago.dataapi.classes.Sector;

/**
 * Metodos para inicializar tablas de la base de datos
 */
public class DBInit {
	private static final Logger logger = LoggerFactory.getLogger(DBInit.class);
	
	public static void main(String[] args) {
	}
	
	/**
	 * Inicializa los Sectores
	 */
	public static void insertMerchantSectors() {
		logger.info("Inicializando Sectores");
		Sector[] sectors = new Sector[10];
		sectors[0] = new Sector("1", "Servicios sin fines de lucro");
		sectors[1] = new Sector("2", "Alimentos");
		sectors[2] = new Sector("3", "Servicios públicos básicos");
		sectors[3] = new Sector("4", "Salud");
		sectors[4] = new Sector("5", "Educación");
		sectors[5] = new Sector("6", "Vestido y calzado");
		sectors[6] = new Sector("7", "Equipamiento del hogar");
		sectors[7] = new Sector("8", "Entretenimiento");
		sectors[8] = new Sector("9", "Bienes y servicios financieros y no financieros");
		sectors[9] = new Sector("10", "Bienes y servicios suntuarios");
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			if(session != null) {
				session.beginTransaction();
				for(int i=0; i<10; i++) session.save(sectors[i]);
				session.getTransaction().commit();
			}
		logger.info("Sectores inicializados");
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			if(session != null) session.close();
		}
	}
	
	/**
	 * Inicializa los Tipos de Industrias
	 */
	public static void insertMerchantIndustryTypes() {
		logger.info("Inicializando Tipos de Industrias");
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Sector[] sectors = new Sector[10];
			for(int i=0; i<10; i++) sectors[i] = (Sector)session.load(Sector.class, i+1);
			
			IndustryType[] types = new IndustryType[129];
			
			types[0] = new IndustryType("1.1", "Organizaciones sin fines de lucro, beneficencias, MERCAL, PDVAL y similares", sectors[0]);
			
			types[1] = new IndustryType("2.1", "Abastos", sectors[1]);
			types[2] = new IndustryType("2.2", "Supermercados", sectors[1]);
			types[3] = new IndustryType("2.3", "Hipermercados", sectors[1]);
			types[4] = new IndustryType("2.4", "Panaderías, pastelerías y cafés", sectors[1]);
			types[5] = new IndustryType("2.5", "Charcuterías, carnicerías y pescaderías", sectors[1]);
			types[6] = new IndustryType("2.6", "Comida rápida", sectors[1]);
			types[7] = new IndustryType("2.7", "Dulcerías, bombonerías, heladerías", sectors[1]);
			types[8] = new IndustryType("2.8", "Restaurant sin bar", sectors[1]);
			types[9] = new IndustryType("2.9", "Fuentes de Soda", sectors[1]);
			types[10] = new IndustryType("2.10", "Churrasquerías", sectors[1]);
			types[11] = new IndustryType("2.11", "Polleras", sectors[1]);
			types[12] = new IndustryType("2.12", "Pizería", sectors[1]);
			types[13] = new IndustryType("2.13", "Otros", sectors[1]);
			
			types[14] = new IndustryType("3.1", "Agua", sectors[2]);
			types[15] = new IndustryType("3.2", "Luz", sectors[2]);
			types[16] = new IndustryType("3.3", "Gas", sectors[2]);
			types[17] = new IndustryType("3.4", "Teléfono", sectors[2]);
			types[18] = new IndustryType("3.5", "Pago de impuestos", sectors[2]);
			
			types[19] = new IndustryType("4.1", "Hospitales y clínicas", sectors[3]);
			types[20] = new IndustryType("4.2", "Droguerías y farmacias", sectors[3]);
			types[21] = new IndustryType("4.3", "Laboratorios médicos, odontológicos y ópticas", sectors[3]);
			types[22] = new IndustryType("4.4", "Equipos de salud", sectors[3]);
			types[23] = new IndustryType("4.5", "Servicios de ambulancia", sectors[3]);
			types[24] = new IndustryType("4.6", "Servicios médicos en general", sectors[3]);
			types[25] = new IndustryType("4.7", "Otros", sectors[3]);
			
			types[26] = new IndustryType("5.1", "Escuelas y colegios", sectors[4]);
			types[27] = new IndustryType("5.2", "Universidades", sectors[4]);
			types[28] = new IndustryType("5.3", "Institutos tecnológicos", sectors[4]);
			types[29] = new IndustryType("5.4", "Academias", sectors[4]);
			types[30] = new IndustryType("5.5", "Librerías, papelerías, útiles escolares", sectors[4]);
			types[31] = new IndustryType("5.6", "Otros", sectors[4]);
			
			types[32] = new IndustryType("6.1", "Zapatería", sectors[5]);
			types[33] = new IndustryType("6.2", "Zapateros", sectors[5]);
			types[34] = new IndustryType("6.3", "Artículos de zapatería", sectors[5]);
			types[35] = new IndustryType("6.4", "Tiendas de descuento", sectors[5]);
			types[36] = new IndustryType("6.5", "Tiendas por departamento", sectors[5]);
			types[37] = new IndustryType("6.6", "Ropa masculina y femenina", sectors[5]);
			types[38] = new IndustryType("6.7", "Accesorios y artículos femeninos", sectors[5]);
			types[39] = new IndustryType("6.8", "Ropa de bebes y niños", sectors[5]);
			types[40] = new IndustryType("6.9", "Ropa para toda la familia", sectors[5]);
			types[41] = new IndustryType("6.10", "Tienda de ropa deportiva", sectors[5]);
			types[42] = new IndustryType("6.11", "Sastres, modistas, arreglos y similares", sectors[5]);
			types[43] = new IndustryType("6.12", "Artículos de cuero", sectors[5]);
			types[44] = new IndustryType("6.13", "Otros", sectors[5]);
			
			types[45] = new IndustryType("7.1", "Muebles y accesorios del hogar", sectors[6]);
			types[46] = new IndustryType("7.2", "Lencería", sectors[6]);
			types[47] = new IndustryType("7.3", "Toldos, techos de lona y similares", sectors[6]);
			types[48] = new IndustryType("7.4", "Tiendas de antiguedades", sectors[6]);
			types[49] = new IndustryType("7.5", "Cristalería", sectors[6]);
			types[50] = new IndustryType("7.6", "Tiendas de pintura, barniz, papel tapiz y similares", sectors[6]);
			types[51] = new IndustryType("7.7", "Chimeneas y accesorios", sectors[6]);
			types[52] = new IndustryType("7.8", "Alquiler de equipos y herramientas", sectors[6]);
			types[53] = new IndustryType("7.9", "Otros", sectors[6]);
			
			types[54] = new IndustryType("8.1", "Artículos para artes plásticas", sectors[7]);
			types[55] = new IndustryType("8.2", "Academias de danza y artes marciales", sectors[7]);
			types[56] = new IndustryType("8.3", "Salas de cine, teatros y museos", sectors[7]);
			types[57] = new IndustryType("8.4", "Discotiendas, ventas de instrumentos musicales y similares", sectors[7]);
			types[58] = new IndustryType("8.5", "Tiendas de artículos deportivos y similares", sectors[7]);
			types[59] = new IndustryType("8.6", "Jugueterías, fototiendas, alquiler de vídeos y similares", sectors[7]);
			types[60] = new IndustryType("8.7", "Agencias de viaje", sectors[7]);
			types[61] = new IndustryType("8.8", "Salones de juego", sectors[7]);
			types[62] = new IndustryType("8.9", "Atracciones turísticas y similares", sectors[7]);
			types[63] = new IndustryType("8.10", "Otros", sectors[7]);
			
			types[64] = new IndustryType("9.1", "Hipertiendas", sectors[8]);
			types[65] = new IndustryType("9.2", "Venta de teléfonos", sectors[8]);
			types[66] = new IndustryType("9.3", "Equipo de telecomunicaciones", sectors[8]);
			types[67] = new IndustryType("9.4", "Servicios de telecomunicaciones nacional e internacional", sectors[8]);
			types[68] = new IndustryType("9.5", "Servicios de telefonía celular", sectors[8]);
			types[69] = new IndustryType("9.6", "Venta de vehículos, camiones, motos y similares", sectors[8]);
			types[70] = new IndustryType("9.7", "Venta de repuestos para vehiculos, camiones, motos y similares", sectors[8]);
			types[71] = new IndustryType("9.8", "Taller de mecánica, latonería, pintura y similares", sectors[8]);
			types[72] = new IndustryType("9.9", "Venta de cauchos para vehículos, camiones, motos y similares", sectors[8]);
			types[73] = new IndustryType("9.10", "Alquiler de vehículos, camiones, motos y similares", sectors[8]);
			types[74] = new IndustryType("9.11", "Transporte de pasajeros (terrestre y marítimo)", sectors[8]);
			types[75] = new IndustryType("9.12", "Líneas aéreas", sectors[8]);
			types[76] = new IndustryType("9.13", "TV por cable y similar", sectors[8]);
			types[77] = new IndustryType("9.14", "Internet", sectors[8]);
			types[78] = new IndustryType("9.15", "Hoteles (hasta 3 estrellas)", sectors[8]);
			types[79] = new IndustryType("9.16", "Posadas", sectors[8]);
			types[80] = new IndustryType("9.17", "Hosterías", sectors[8]);
			types[81] = new IndustryType("9.18", "Condominios residenciales (Serv. Público)", sectors[8]);
			types[82] = new IndustryType("9.19", "Productos químicos y afines", sectors[8]);
			types[83] = new IndustryType("9.20", "Productos petrolíferos y afines", sectors[8]);
			types[84] = new IndustryType("9.21", "Filatelia y numismática", sectors[8]);
			types[85] = new IndustryType("9.22", "Impresión de sellos de goma", sectors[8]);
			types[86] = new IndustryType("9.23", "Tiendas de mascotas", sectors[8]);
			types[87] = new IndustryType("9.24", "Viveros, equipos de jardinería y similares", sectors[8]);
			types[88] = new IndustryType("9.25", "Sistemas de seguridad", sectors[8]);
			types[89] = new IndustryType("9.26", "Plomería, construcción, carpintería, aire acondicionado y similares", sectors[8]);
			types[90] = new IndustryType("9.27", "Reparación de artículos electrónicos y similares", sectors[8]);
			types[91] = new IndustryType("9.28", "Reparación y tapizado de muebles", sectors[8]);
			types[92] = new IndustryType("9.29", "Lavandería y limpieza", sectors[8]);
			types[93] = new IndustryType("9.30", "Limpieza de alfombras y tapicería", sectors[8]);
			types[94] = new IndustryType("9.31", "Limpieza y lavado de ventanas", sectors[8]);
			types[95] = new IndustryType("9.32", "Servicios de fumigación", sectors[8]);
			types[96] = new IndustryType("9.33", "Servicios de piscina", sectors[8]);
			types[97] = new IndustryType("9.34", "Veterinarios", sectors[8]);
			types[98] = new IndustryType("9.35", "Mudanzas", sectors[8]);
			types[99] = new IndustryType("9.36", "Correo aéreo y terrestre", sectors[8]);
			types[100] = new IndustryType("9.37", "Funerarias", sectors[8]);
			types[101] = new IndustryType("9.38", "Servicios de acompañantes", sectors[8]);
			types[102] = new IndustryType("9.39", "Asesoría profesional", sectors[8]);
			types[103] = new IndustryType("9.40", "Salones de belleza, peluquerías, manicuristas, pedicuristas y similares", sectors[8]);
			types[104] = new IndustryType("9.41", "Perfumerías, maquillaje y cosméticos", sectors[8]);
			types[105] = new IndustryType("9.42", "Gimnasios, salones de masaje y similares", sectors[8]);
			types[106] = new IndustryType("9.43", "Servicios de niñera", sectors[8]);
			types[107] = new IndustryType("9.44", "Agencias de publicidad", sectors[8]);
			types[108] = new IndustryType("9.45", "Fotocopiado y reproducciones", sectors[8]);
			types[109] = new IndustryType("9.46", "Servicios de computación", sectors[8]);
			types[110] = new IndustryType("9.47", "Servicios de cobranzas", sectors[8]);
			types[111] = new IndustryType("9.48", "Servicios de gruas", sectors[8]);
			types[112] = new IndustryType("9.49", "Casas de cambio", sectors[8]);
			types[113] = new IndustryType("9.50", "Seguros", sectors[8]);
			types[114] = new IndustryType("9.51", "Floristería", sectors[8]);
			types[115] = new IndustryType("9.52", "Tiendas de variedades", sectors[8]);
			types[116] = new IndustryType("9.53", "Servicios no clasificados", sectors[8]);
			types[117] = new IndustryType("9.54", "Servicios profesionales no incluidos", sectors[8]);
			types[118] = new IndustryType("9.55", "Detal artículos especiales y venta", sectors[8]);
			types[119] = new IndustryType("9.56", "Agencias de empleo", sectors[8]);
			types[120] = new IndustryType("9.57", "Otros comercios mercadeo directo", sectors[8]);
			
			types[121] = new IndustryType("10.1", "Hoteles 4 y 5 estrellas", sectors[9]);
			types[122] = new IndustryType("10.2", "Tiendas de delicateses", sectors[9]);
			types[123] = new IndustryType("10.3", "Club, resort, campo de golf y similares", sectors[9]);
			types[124] = new IndustryType("10.4", "Alquiler de trajes, uniformes o similares", sectors[9]);
			types[125] = new IndustryType("10.5", "Tiendas duty free", sectors[9]);
			types[126] = new IndustryType("10.6", "Joyerías y galerías de arte", sectors[9]);
			types[127] = new IndustryType("10.7", "Restaurantes de lujo (con bar)", sectors[9]);
			types[128] = new IndustryType("10.8", "Bebidas alcohólicas (discotecas, bares, licorerías, etc), tabaco y juegos (loterías, casinos, etc)", sectors[9]);
			
			if(session != null) {
				session.beginTransaction();
				for(int i=0; i<129; i++) session.save(types[i]);
				session.getTransaction().commit();
			}
			logger.info("Tipos de Industrias inicializados");
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			if(session != null) session.close();
		}
	}
}
