package vpago.dataapi.util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.dataapi.classes.EngineConfig;
import vpago.dataapi.db.HibernateUtil;

public class AutoAffiliation {
	protected static final Logger logger = LoggerFactory.getLogger(AutoAffiliation.class);
	
	public static void main(String[] args) {
		getAffiliationNumber();
	}
	
	public static String getAffiliationNumber() {
		logger.debug("getAffiliationNumber");
		Session session = null;
		String affiliatedC = "";
		Integer affiliatedInt = 0;
		List<EngineConfig> engineConL = new ArrayList<EngineConfig>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from EngineConfig as engineConfig";
			engineConL = session.createQuery(hsqlquery).list();
			if(engineConL.size() > 0) {
				EngineConfig engineCon = engineConL.get(0);
				affiliatedInt = Integer.parseInt(engineCon.getAffiliatedCounter());
				affiliatedInt++;
				affiliatedC = String.format("%010d", affiliatedInt);
				logger.debug("{}", affiliatedC);
				engineCon.setAffiliatedCounter(affiliatedC);
				session.beginTransaction();
				session.update(engineCon);
				session.getTransaction().commit();
			}
		} catch (HibernateException e) {
			logger.error(e.toString());
		}
		return affiliatedC;
	}
	
	public static String getTerminalNumber() {
		logger.debug("getTerminalNumber");
		Session session = null;
		String terminalC = "";
		Integer terminalInt = 0;
		List<EngineConfig> engineConL = new ArrayList<EngineConfig>();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from EngineConfig as engineConfig";
			engineConL = session.createQuery(hsqlquery).list();
			if(engineConL.size() > 0) {
				EngineConfig engineCon = engineConL.get(0);
				logger.debug("{}", engineCon);
				terminalInt = Integer.parseInt(engineCon.getTerminalCounter());
				terminalInt++;
				terminalC = String.format("%08d", terminalInt);
				logger.debug("{}", terminalC);
				engineCon.setTerminalCounter(terminalC);
				session.beginTransaction();
				session.update(engineCon);
				session.getTransaction().commit();
			}
		} catch (HibernateException e) {
			logger.error(e.toString());
		}
		return terminalC;
	}
	
}
