package vpago.dataapi.util;


import java.util.List;

import org.hibernate.Session;

import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.Bank;
import vpago.dataapi.classes.CurrentBatch;
import vpago.dataapi.classes.BatchHistory;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Multimerchant;
import vpago.dataapi.classes.Terminal;
import vpago.dataapi.classes.TerminalParameter;
import vpago.dataapi.db.HibernateUtil;

/**
 * Contiene metodos para consultar recursos comunes en las funciones de la API
 */
public class ResourceQuery {
	
	/**
	 * Retorna el objeto de tipo Merchant que posee el guid especificado
	 * 
	 * @param guid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return Datos de la entidad de tipo Comercio (Merchant) identificada por <strong>guid</strong>
	 */
	public static Merchant searchMerchant(String guid){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Merchant merchant = (Merchant)session.createQuery("from Merchant as merchant where merchant.merchantGuid = :guid")
				.setString("guid", guid)
				.uniqueResult();
		session.close();
		return merchant;
	}

	/**
	 * Retorna el objeto de tipo Multimerchant que posee el guid especificado
	 * 
	 * @param guid Identificador del recurso de tipo Multicomercio (Multimerchant)
	 * 
	 * @return Datos de la entidad de tipo Multicomercio (Multimerchant) identificada por <strong>guid</strong>
	 */
	public static Multimerchant searchMultimerchant(String guid){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Multimerchant multiMerchant = (Multimerchant)session.createQuery("from Multimerchant as multimerchant where multimerchant.merchantGuid = :guid")
				.setString("guid", guid)
				.uniqueResult();
		session.close();
		return multiMerchant;
	}
	
	/**
	 * Retorna el objeto de tipo Merchant asociado al objeto Terminal que posee el guid especificado
	 * 
	 * @param guid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Datos de la entidad de tipo Comercio (Merchant) asociada al Terminal (Terminal) identificado por <strong>guid</strong>
	 */
	public static Merchant searchMerchantByTerminal(String guid){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Merchant merchant = ((Terminal)session.createQuery("from Terminal as terminal where terminal.terminalGuid = :guid")
				.setString("guid", guid)
				.uniqueResult()).getAffiliation().getMerchant() ;
		session.close();
		return merchant;
	}
	
	/**
	 * Retorna el objeto de tipo Terminal (Terminal )que posee el guid especificado
	 * 
	 * @param guid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Datos de la entidad de tipo Terminal (Terminal) identificada por <strong>guid</strong>
	 */
	public static Terminal searchTerminal(String guid){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Terminal terminal = (Terminal)session.createQuery("from Terminal as terminal where terminal.terminalGuid = :guid")
				.setString("guid", guid)
				.uniqueResult();
		session.close();
		return terminal;
	}
	
	public static TerminalParameter searchTerminalParameter(Integer terminalParameterId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		TerminalParameter TerminalParameter = (TerminalParameter)session.createQuery("from TerminalParameter as terminalParameter where terminalParameter.id = :terminalParameterId")
				.setInteger("terminalParameterId", terminalParameterId)
				.uniqueResult();
		session.close();
		return TerminalParameter;
	}
	
	public static Acquirer searchAcquirer(Integer acquirerId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Acquirer acquirer = (Acquirer)session.createQuery("from Acquirer as acquirer where acquirer.id = :acquirerId")
				.setInteger("acquirerId", acquirerId)
				.uniqueResult();
		session.close();
		return acquirer;
	}

	public static Bank searchBank(Integer bankId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Bank bank = (Bank)session.createQuery("from Bank as bank where bank.id = :bankId")
				.setInteger("bankId", bankId)
				.uniqueResult();
		session.close();
		return bank;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Terminal> searchTerminalAf(Integer affiliationId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Terminal> terminalList = (List<Terminal>)session.createQuery("from Terminal as terminal where terminal.affiliation.id = :affiliationId order by terminal.id")
				.setInteger("affiliationId", affiliationId)
				.list();
		session.close();
		return terminalList;
	}
	
	@SuppressWarnings("unchecked")
	public static List<CurrentBatch> searchCurrentBatch(Integer terminalId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<CurrentBatch> currentBatch = (List<CurrentBatch>)session.createQuery("from CurrentBatch as currentBatch where currentBatch.terminal.id = :terminalId")
				.setInteger("terminalId", terminalId)
				.list();
		session.close();
		return currentBatch;
	}

	public static List<BatchHistory> searchBatchHistory(String date){
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<BatchHistory> batchHistory = (List<BatchHistory>)session.createQuery("from BatchHistory as bh where bh.batchHistSynchronized = 0 AND TO_CHAR(bh.closingTimestamp, 'YYYY-MM-DD') = :date")
				.setString("date", date)
				.list();
		session.close();
		return batchHistory;
	}
}
