package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.CardFranchiseList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

@Path("/cardFranchise")
public class CardFranchiseResource extends ResourceManager {

	/**
	 * Listar Franquicias: Construye una lista con todas las entidades de Franquicia (CardFranchise) alamcenadas en la base de datos
	 * 
	 * @return La lista de todas las entidades de Franquicia (CardFranchise)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllCardFranchise() { 
		Session session = null;
		CardFranchiseList carFranchise = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.CardFranchiseMin(id, name) from CardFranchise as cardFranchise";
			carFranchise = new CardFranchiseList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return carFranchise == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de tarjeta", "Recurso no encontrado") : carFranchise;
	}	
}
