package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.BinList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Bin (Bin)
 */
@Path("/bin")
public class BinService extends ResourceManager {
	
	/**
	 * Listar Bines: Construye una lista con todas las entidades de tipo Bin (Bin) alamcenadas en la base de datos
	 * 
	 * @return La lista de todas las entidades de tipo Bin (Bin)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllBins() { 
		Session session = null;
		BinList binList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "from Bin as bin";
			binList = new BinList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return binList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun banco", "Recurso no encontrado") : binList;
	}
}
