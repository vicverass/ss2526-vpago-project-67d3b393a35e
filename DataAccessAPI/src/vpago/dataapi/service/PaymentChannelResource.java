package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.PaymentChannelList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los metodos para los servicios relacionados al recurso Canal de Pago (PaymentChannel)
 */
@Path("/paymentChannel")
public class PaymentChannelResource extends ResourceManager {
	
	/**
	 * Listar Canales de Pago: Construye una lista con todas las entidades de tipo Canal de Pago (PaymentChannel) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Canal de Pago (PaymentChannel)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllPaymentChannel(){
		Session session = null;
		PaymentChannelList paymentChannelList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.PaymentChannelMin(id, name, description, codeName) from PaymentChannel as paymentChannel";
			paymentChannelList = new PaymentChannelList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return paymentChannelList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun canal de pago", "Recurso no encontrado") : paymentChannelList;
	}
}
