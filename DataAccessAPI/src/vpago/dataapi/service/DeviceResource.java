package vpago.dataapi.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Query;
import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.AssignedDevice;
import vpago.dataapi.classes.Device;
import vpago.dataapi.classes.DeviceToAssign;
import vpago.dataapi.classes.DeviceType;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.Terminal;
import vpago.dataapi.classes.minified.DeviceToAssignMin;
import vpago.dataapi.classes.wrappers.DeviceList;
import vpago.dataapi.classes.wrappers.DeviceToAssignList;
import vpago.dataapi.classes.wrappers.DeviceTypeList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados a los recursos Dispositivo (Device), Tipo de Dispositivo (DeviceType), y AssignedDevice (Dispositivo Asignado)
 */
@Path("/device")
public class DeviceResource extends ResourceManager  {
	
	/**
	 * Consultar Tipo de Dispositivo: Consultar los datos de una entidad de tipo Tipo de Dispositivo (DeviceType)
	 * 
	 * @param deviceTypeId Identificador del recurso de tipo Tipo de Dispositivo (DeviceType)
	 * 
	 * @return Datos de la entidad de tipo Tipo de Dispositivo (DeviceType) identificada por <strong>deviceTypeId</strong>
	 */
	@Path("/type/{device_type_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getDeviceType(@PathParam("device_type_id") int deviceTypeId) {
		Session session = null;
		DeviceType type = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			type = (DeviceType)session.get(DeviceType.class, deviceTypeId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return type == null ? new Response(ResponseType.RESOURCENOTFOUND, "El tipo de dispositivo solicitado no existe", "Identificador no encontrado") : type;
	}
	
	/**
	 * Crear Tipo de Dispositivo: Crea una entidad del tipo Tipo de Dispositivo (DeviceType) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param t Objeto de tipo <strong>DeviceType</strong> con los datos del Tipo de Dispositivo (DeviceType)
	 * 
	 * @return Datos de la entidad de tipo Tipo de Dispositivo (DeviceType) que ha sido creada
	 */
	@Path("/type")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createDeviceType(DeviceType t) {
		DeviceType type = new DeviceType(t.getName());
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(type);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return type;
	}
	
	/**
	 * Modificar Tipo de Dispositivo: Permite cambiar un subconjunto de los datos del Tipo de Dispositivo (DeviceType). Estos son: Nombre
	 * 
	 * @param deviceTypeId Identificador del recurso de tipo Tipo de Dispositivo (DeviceType)
	 * 
	 * @param t Objeto de tipo <strong>DeviceType</strong> con los datos del Tipo de Dispositivo (DeviceType)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/type/{device_type_id: \\d+}")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesDeviceType(@PathParam("device_type_id") int deviceTypeId,  DeviceType t) {
		Session session = null;
		DeviceType type = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			type = (DeviceType)session.get(DeviceType.class, deviceTypeId);	
			if(!type.equals(t)) type.update(t);
			session.beginTransaction();
			session.update(type);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return type == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El tipo de dispositivo solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El tipo de dispositivo ha sido actualizado correctamente");
	}
	
	/**
	 * Eliminar Tipo de Dispositivo: Elimina una entidad de tipo Tipo de Dispositivo (DeviceType)
	 * 
	 * @param deviceTypeId Identificador del recurso de tipo Tipo de Dispositivo (DeviceType)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/type/{device_type_id: \\d+}")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteDeviceType(@PathParam("device_type_id") int deviceTypeId) {
		Session session = null;
		DeviceType type = null;
		Long devices = (long)-1;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			type = (DeviceType)session.get(DeviceType.class, deviceTypeId);	
			Iterator<?> result = session.createQuery("select count(device) from Device as device where device.type = :deviceType")
					.setEntity("deviceType", type).iterate();
			if (result.hasNext()) devices = (long) result.next();
			logger.trace("Dispositivos asociados: {}", devices);
			if (devices != 0) return new Response(ResponseType.INVALIDRESOURCE, "No se pudo eliminar el tipo de dispositivo", "Dispositivos asociados"); 
			if(type != null) {
				session.beginTransaction();
				session.delete(type);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return type == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El tipo de dispositivo solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El tipo de dispositivo ha sido eliminado correctamente");
	}
	
	/**
	 * Listar Tipos de Dispositivos: Construye una lista con todas las entidades de tipo Tipo de Dispositivo (DeviceType) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Tipo de Dispositivo (DeviceType)
	 */
	@SuppressWarnings("unchecked")
	@Path("/type")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllDeviceTypes(){
		Session session = null;
		DeviceTypeList deviceTypeList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.DeviceTypeMin(id, name) from DeviceType as deviceType";
			deviceTypeList = new DeviceTypeList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceTypeList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de dispositivo", "Recurso no encontrado") : deviceTypeList;
	}
	
	/**
	 * Consultar Dispositivo: Consultar los datos de una entidad de tipo Dispositivo (Device)
	 * 
	 * @param deviceId Identificador del recurso de tipo Dispositivo (Device)
	 * 
	 * @return Datos de la entidad de tipo Dispositivo (Device) identificada por <strong>deviceId</strong>
	 */
	@Path("/{device_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getDevice(@PathParam("device_id") int deviceId) {
		Session session = null;
		Device type = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			type = (Device)session.get(Device.class, deviceId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return type == null ? new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo solicitado no existe", "Identificador no encontrado") : type;
	}
	
	/**
	 * Crear Dispositivo: Crea una entidad del tipo Dispositivo (Device) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param d Objeto de tipo <strong>Device</strong> con los datos del Dispositivo (Device)
	 * 
	 * @return Datos de la entidad de tipo Dispositivo (Device) que ha sido creada
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createDevice(Device d) {
		Device device = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			DeviceType dt = d.getDeviceTypeId() != null ? (DeviceType)session.get(DeviceType.class, d.getDeviceTypeId()) : null;
			if(dt != null) {
				device = new Device(d.getDisplayName(), 
						d.getDescription(),
						d.getModel(),
						d.getSeries(),
						d.getVendor(),
						dt);
				session.beginTransaction();
				session.save(device);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return device;
	}
	
	/**
	 * Permite cambiar un subconjunto de los datos del Dispositivo (Device). Estos son: Nombre para mostrar, descripcion, modelo, serie, fabricante y tipo de dispositivo
	 * 
	 * @param deviceId Identificador del recurso de tipo Dispositivo (Device)
	 * 
	 * @param d Objeto de tipo <strong>Device</strong> con los datos del Dispositivo (Device)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{device_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesDevice(@PathParam("device_id") int deviceId, Device d) {
		Session session = null;
		Device device = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			device = (Device)session.get(Device.class, deviceId);	
			if(!device.equals(d)) device.update(d);
			logger.trace("{}", device);
			session.beginTransaction();
			session.update(device);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return device == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El dispositivo ha sido actualizado correctamente");
	}
	
	/**
	 * Eliminar Dispositivo:  Elimina una entidad de tipo Dispositivo (Device)
	 * 
	 * @param deviceId Identificador del recurso de tipo Dispositivo (Device)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{device_id: \\d+ }")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteDevice(@PathParam("device_id") int deviceId) {
		Session session = null;
		Device device = null;
		Long assignedDevices = (long) -1;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			device = (Device)session.get(Device.class, deviceId);
			if(device != null) {
				Iterator<?> result = session.createQuery("select count(assignedDevice) from AssignedDevice as assignedDevice where assignedDevice.device = :device")
						.setEntity("device", device).iterate();
				if(result.hasNext()) assignedDevices = (long) result.next();
				if(assignedDevices != 0) return new Response(ResponseType.INVALIDRESOURCE, "El dispositivo solicitado esta asociado a terminales", "Identificador no valido"); 
				session.beginTransaction();
				session.delete(device);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return device == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El dispositivo ha sido eliminado correctamente");
	}
	
	/**
	 * Listar Dispositivos: Construye una lista con todas las entidades de tipo Dispositivo (Device) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Dispositivo (Device)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllDevices(){
		Session session = null;
		DeviceList deviceList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.DeviceMin(id, displayName, deviceTypeId) from Device as device";
			deviceList = new DeviceList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de dispositivo", "Recurso no encontrado") : deviceList;
	}
	
	/**
	 * Asignar Dispositivo a Terminal: Crea una Asignacion de Dispositivo (AssignedDevice) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param terminalGuid Identificador del recurso de tipo Terminal (Terminal) al que sera asignado el dispositivo
	 * 
	 * @param ad Objeto de tipo <strong>AssignedDevice</strong> con los datos de la Asigacion de Dispositivo (AssignedDevice)
	 * 
	 * @return Datos de la entidad de tipo Asignacion de Dispositivo (AssignedDevice) que ha sido creada
	 */
	@SuppressWarnings("unchecked")
	@Path("/assigned/{terminal_guid: [a-fA-F\\d]{8}}")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createAssignedDevice(@PathParam("terminal_guid") String terminalGuid, AssignedDevice ad) {
		Session session = null;
		Device device = null;
		DeviceToAssign deviceToAssign = null;
		Terminal terminal = null;
		List<AssignedDevice> assigned = null;
		List<DeviceToAssign> toAssign = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			terminal = ResourceQuery.searchTerminal(terminalGuid);
			if(terminal != null) {
				if(terminal.getDateAssignment() == null){
					terminal.setDateAssignment(Calendar.getInstance());
				}else{
					terminal.setSerialUpdateDate(Calendar.getInstance());
				}
				assigned = session.createQuery("from AssignedDevice as aDevice where "
						+ "aDevice.serialNumberDevice = :serialNumber and "
						+ "aDevice.deviceId = :deviceId")
						.setString("serialNumber", ad.getSerialNumberDevice())
						.setInteger("deviceId", ad.getDeviceId()).list();
				toAssign = session.createQuery("from DeviceToAssign as aDevice where "
						+ "aDevice.deviceSerial = :serialNumber and "
						+ "aDevice.deviceId = :deviceId and "
						+ "aDevice.available = true and "
						+ "aDevice.active = true")
						.setString("serialNumber", ad.getSerialNumberDevice())
						.setInteger("deviceId", ad.getDeviceId()).list();
				logger.info("ad {}", ad);
				if(assigned.size() != 0) return new Response(ResponseType.INVALIDRESOURCE, "Asignacion erronea", "El dispositivo con el serial indicado ya ha sido asignado a un terminal"); 
				if(toAssign.size() == 0) return new Response(ResponseType.INVALIDRESOURCE, "Asignacion erronea", "El dispositivo con el serial indicado no existe o no se encuentra diponible"); 
				device = (Device)session.get(Device.class, ad.getDeviceId());
				deviceToAssign = toAssign.get(0);
				if((device != null) && (deviceToAssign != null)) {
					ad.setDevice(device);
					
					logger.info("Dispositivo asignado {}",ad);
					deviceToAssign.setAvailable(false);
					session.beginTransaction();
					session.save(ad);
					session.save(deviceToAssign);
					session.getTransaction().commit();
					terminal.setAssignedDevice(ad);
					session.beginTransaction();
					session.update(terminal);
					session.getTransaction().commit();
				}
			}	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(terminal == null) return new Response(ResponseType.RESOURCENOTFOUND, "El terminal a asociar no existe", "Identificador no encontrado"); 
		else if(device == null) return new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo a asignar no existe", "Identificador no encontrado"); 
		else return ad;
	}
	
	/**
	 * Consultar Asignaci�n de Dispositivo a Terminal: Consultar los datos de una entidad de tipo Dispositivo Asignado (AssignedDevice)
	 * 
	 * @param assignedDeviceId Identificador del recurso de tipo Dispositivo Asignado (AssignedDevice)
	 * 
	 * @return Datos de la entidad de tipo Dispositivo Asignado (AssignedDevice) identificada por <strong>assignedDeviceId</strong>
	 */
	@Path("/assigned/{assigned_device_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAssignedDevice(@PathParam("assigned_device_id") int assignedDeviceId) {
		Session session = null;
		AssignedDevice assignedDevice = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			assignedDevice = (AssignedDevice)session.get(AssignedDevice.class, assignedDeviceId);
			return assignedDevice == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el dispositivo asignado", "Recurso no encontrado") : assignedDevice;
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}

	/**
	 * Revocar Dispositivo Asignar: Elimina la asociacion entre un Terminal (Terminal) y un Dispositivo (Device)
	 * 
	 * @param assignedDeviceId Identificador del recurso de tipo Dispositivo Asignado (AssignedDevice)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/assigned/{assigned_device_id: \\d+}")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteAssignedDevice(@PathParam("assigned_device_id") int assignedDeviceId) {
		Session session = null;
		AssignedDevice assigned = null;
		Terminal terminal = null;
		List<?> terminals = null;
		List<?> toAssign = null;
		DeviceToAssign deviceToAssign = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			assigned = (AssignedDevice)session.get(AssignedDevice.class, assignedDeviceId);	
			terminals = session.createQuery("from Terminal as terminal where terminal.assignedDeviceId = :assignedDeviceId")
					.setInteger("assignedDeviceId", assigned.getId()).list();
			if(terminals.size() > 0) terminal = (Terminal)terminals.get(0);
			if(assigned != null && terminal != null) {
				toAssign = session.createQuery("from DeviceToAssign as aDevice where "
						+ "aDevice.deviceSerial = :serialNumber and "
						+ "aDevice.deviceId = :deviceId")
						.setString("serialNumber", assigned.getSerialNumberDevice())
						.setInteger("deviceId", assigned.getDeviceId()).list();
				if(toAssign != null) if(toAssign.size() > 0) {
					deviceToAssign = (DeviceToAssign)toAssign.get(0);
					deviceToAssign.setAvailable(true);
					
					terminal.setAssignedDevice(null);
					session.beginTransaction();
					session.update(deviceToAssign);
					session.update(terminal);
					session.delete(assigned);
					session.getTransaction().commit();
				}
				
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return assigned == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo asignado solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "La asignacion de dispositivo ha sido elimianda correctamente");
	}
	
	/**
	 * Listar Dispositivos a asignar: Construye una lista con todas las entidades de tipo Dispositivo a asignar (DeviceToAssign) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Dispositivo a asignar (DeviceToAssign)
	 */
	@SuppressWarnings("unchecked")
	@Path("/toAssign")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllDeviceToAssign( 
			 @QueryParam("available") Boolean available,
			 @QueryParam("device") Integer device,
			 @QueryParam("deviceType") Integer deviceType){
		logger.debug("Parametros: available=" + available + " device=" + device + " deviceType=" + deviceType);
		Session session = null;
		DeviceToAssignList deviceToAssignList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();			
			String hsqlquery = "select new vpago.dataapi.classes.minified.DeviceToAssignMin("
					+ "deviceToAssign.id, "
					+ "deviceToAssign.device, "
					+ "deviceToAssign.deviceSerial, "
					+ "deviceToAssign.available) from DeviceToAssign as deviceToAssign where "
					+ "deviceToAssign.active = true"
					+ ((available != null) ? " and deviceToAssign.available = " + available : "")
					+ ((device != null) ? " and deviceToAssign.deviceId = " + device : "") 
					+ ((deviceType != null) ? " and deviceToAssign.deviceTypeId = " + deviceType : "")
				    + ") order by deviceToAssign.available asc, deviceToAssign.id asc";
			Query deviceQuery = session.createQuery(hsqlquery);
			deviceToAssignList = new DeviceToAssignList(deviceQuery != null ? deviceQuery.list() : new ArrayList<DeviceToAssignMin>());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceToAssignList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de dispositivo", "Recurso no encontrado") : deviceToAssignList;
	}
	
	/**
	 * Consultar Dispositivo a asignar: Consultar los datos de una entidad de tipo Dispositivo a asignar (DeviceToAssign)
	 * 
	 * @param deviceToAssignId Identificador del recurso de tipo Dispositivo a asignar (DeviceToAssign)
	 * 
	 * @return Datos de la entidad de tipo Dispositivo a asignar (DeviceToAssign) identificada por <strong>deviceToAssignId</strong>
	 */
	@Path("/toAssign/{device_to_assign_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getDeviceToAssign(@PathParam("device_to_assign_id") int deviceToAssignId) {
		Session session = null;
		DeviceToAssign deviceToAssign = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			deviceToAssign = (DeviceToAssign)session.get(DeviceToAssign.class, deviceToAssignId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceToAssign == null ? new Response(ResponseType.RESOURCENOTFOUND, "El dipositivo a asignar solicitado no existe", "Identificador no encontrado") : deviceToAssign;
	}
	
	/**
	 * Crear Dispositivo a asignar: Crea una entidad del tipo Dispositivo a asignar (DeviceToAssign) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param d Objeto de tipo <strong>DeviceToAssign</strong> con los datos del Dispositivo a asignar (DeviceToAssign)
	 * 
	 * @return Datos de la entidad de tipo Dispositivo a asignar (DeviceToAssign) que ha sido creada
	 */
	@Path("/toAssign")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createDeviceToAssign(DeviceToAssign d) {
		DeviceToAssign deviceToAssign = null;
		Session session = null;
		List<?> toAssign = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Device device = d.getDeviceId() != null ? (Device)session.get(Device.class, d.getDeviceId()) : null;
			DeviceType deviceType = device != null ? device.getType() : null;
			logger.info("Dispositivo a asignar {}",d.getDeviceId());
			logger.info("deviceToAssign {}", d.getId());
			if((device != null) && (deviceType != null)) {
				toAssign = session.createQuery("from DeviceToAssign as aDevice where "
						+ "aDevice.deviceSerial = :serialNumber and "
						+ "aDevice.deviceId = :deviceId and "
						+ "aDevice.available = true and "
						+ "aDevice.active = true")
						.setString("serialNumber", d.getDeviceSerial())
						.setInteger("deviceId", d.getDeviceId()).list();
				
				if(toAssign.size() != 0) return new Response(ResponseType.INVALIDRESOURCE, "Asignacion erronea", "El dispositivo con el serial indicado ya se encuentra registrado"); 
				
				if (d.getId() == 0){
					deviceToAssign = new DeviceToAssign(
							device,
							deviceType,
							d.getDeviceSerial(),
							true,
							true
							);
					session.beginTransaction();
					logger.info("deviceToAssign insert {}",deviceToAssign);
					session.save(deviceToAssign);
				}else{
					deviceToAssign = new DeviceToAssign(
							d.getId(),
							device,
							deviceType,
							d.getDeviceSerial(),
							true,
							true
							);
					session.beginTransaction();
					logger.info("deviceToAssign update {}",deviceToAssign);
					session.update(deviceToAssign);
				}
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceToAssign;
	}
	
	/**
	 * Borrar Dispositivo a asignar: Elimina un Dispositivo a asignar
	 * 
	 * @param assignedDeviceId Identificador del recurso de tipo Dispositivo a asignar (AssignedDevice)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/toAssign/{device_to_assign_id: \\d+}")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteDeviceToAssign(@PathParam("device_to_assign_id") int assignedDeviceId) {
		Session session = null;
		DeviceToAssign deviceToAssign = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			deviceToAssign = (DeviceToAssign)session.get(DeviceToAssign.class, assignedDeviceId);	
			if(deviceToAssign == null) new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo a asignar solicitado no existe", "Identificador no encontrado") ;
			if (!deviceToAssign.isAvailable()) return new Response(ResponseType.INVALIDRESOURCE, "No se pudo eliminar el tipo de dispositivo", "Dispositivo asociado"); 
			deviceToAssign.setActive(false);
			session.beginTransaction();
			session.update(deviceToAssign);
			session.getTransaction().commit();
			
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return deviceToAssign == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El dispositivo a asignar solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El dispositivo a asignar ha sido eliminado correctamente");
	}
}
