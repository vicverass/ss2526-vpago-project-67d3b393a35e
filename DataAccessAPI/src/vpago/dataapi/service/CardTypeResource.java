package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.CardTypeList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso tipo de tarjeta (CardType)
 */
@Path("/cardType")
public class CardTypeResource extends ResourceManager {

	/**
	 * Listar tipo de tarjeta: Construye una lista con todas las entidades de tipo de tarjeta (CardType) alamcenadas en la base de datos
	 * 
	 * @return La lista de todas las entidades de tipo de tarjeta (CardType)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllCardType() { 
		Session session = null;
		CardTypeList cardType = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.CardTypeMin(id, name) from CardType as cardType";
			cardType = new CardTypeList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return cardType == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de tarjeta", "Recurso no encontrado") : cardType;
	}	
	
}
