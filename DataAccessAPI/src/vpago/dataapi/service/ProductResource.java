package vpago.dataapi.service;

import java.io.Serializable;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Bank;
import vpago.dataapi.classes.BankCardProduct;
import vpago.dataapi.classes.Bin;
import vpago.dataapi.classes.CardFranchise;
import vpago.dataapi.classes.CardType;
import vpago.dataapi.classes.GeneralCardProduct;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.minified.BankCardProductMin;
import vpago.dataapi.classes.minified.GeneralCardProductMin;
import vpago.dataapi.classes.wrappers.BankCardProductList;
import vpago.dataapi.classes.wrappers.GeneralCardProductList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados a los recursos de productos (Tarjetas)
 */
@Path("/product")
public class ProductResource extends ResourceManager {
	
	@Path("/general")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllGeneralProducts() {
		Session session = null;
		GeneralCardProductList generalCardProductList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.GeneralCardProductMin("
					+ "id, "
					+ "cardFranchise.id, "
					+ "cardFranchise.name, "
					+ "cardType.id, "
					+ "cardType.name, "
					+ "productBin"
					+ ") from GeneralCardProduct as generalCardProduct "
					+ "where product_type='general'";
			generalCardProductList = new GeneralCardProductList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return generalCardProductList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun producto general", "Recurso no encontrado") : generalCardProductList;
	}
	
	@Path("/general/{general_product_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getGeneralProduct(@PathParam("general_product_id") int generalProductId) {
		Session session = null;
		GeneralCardProduct generalCardProduct = null;
		GeneralCardProductMin generalCardProductMin = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			generalCardProduct = (GeneralCardProduct)session.get(GeneralCardProduct.class, generalProductId);
			if(generalCardProduct != null) generalCardProductMin = 
					new GeneralCardProductMin(
							generalCardProduct.getId(), 
							generalCardProduct.getCardFranchise().getId(),
							generalCardProduct.getCardFranchise().getName(), 
							generalCardProduct.getCardType().getId(),
							generalCardProduct.getCardType().getName(),
							generalCardProduct.getProductBin()
							);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return generalCardProductMin == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el producto general solicitado", "Recurso no encontrado") : generalCardProductMin;
	}
	
	@Path("/brand")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllSpecificProducts() {
		Session session = null;
		BankCardProductList bankCardProductList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.BankCardProductMin("
					+ "id, "
					+ "cardFranchise.id, "
					+ "cardFranchise.name, "
					+ "cardType.id, "
					+ "cardType.name, "
					+ "cardBank.id, "
					+ "cardBank.name, "
					+ "productBin"
					+ ") from BankCardProduct as bankCardProduct";
			bankCardProductList = new BankCardProductList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bankCardProductList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun producto de banco", "Recurso no encontrado") : bankCardProductList;
	}
	
	@Path("/brand/{brand_product_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getSpecificProduct(@PathParam("brand_product_id") int brandProductId) {
		Session session = null;
		BankCardProduct bankCardProduct = null;
		BankCardProductMin bankCardProductMin = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			bankCardProduct = (BankCardProduct)session.get(BankCardProduct.class, brandProductId);
			if(bankCardProduct != null) bankCardProductMin = 
					new BankCardProductMin(
							bankCardProduct.getId(), 
							bankCardProduct.getCardFranchise().getId(),
							bankCardProduct.getCardFranchise().getName(), 
							bankCardProduct.getCardType().getId(),
							bankCardProduct.getCardType().getName(),
							bankCardProduct.getCardBank().getId(),
							bankCardProduct.getCardBank().getName(),
							bankCardProduct.getProductBin()
							);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bankCardProductMin == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el producto de banco solicitado", "Recurso no encontrado") : bankCardProductMin;
	}
	
	/**
	 * Crear Producto General: Crea una entidad del tipo Producto General (GeneralCardProduct) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param a Objeto de tipo <strong>GeneralCardProduct</strong> con los datos del Producto General  (GeneralCardProduct)
	 * 
	 * @return Datos de la entidad de tipo Producto General (GeneralCardProduct) que ha sido creada
	 */
	@POST
	@Path("/general")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createGeneralCardProduct(GeneralCardProduct a) {
		logger.debug("createGeneralCardProduct");
		GeneralCardProduct generalCardProduct= null;
		CardFranchise cardFranchise = null;
		CardType cardType= null; 
		Bin productBin= null; 
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			cardFranchise = (CardFranchise)session.get(CardFranchise.class, (Serializable) a.getCardFranchiseId());
			cardType = (CardType)session.get(CardType.class, (Serializable) a.getCardTypeId());
			productBin = (Bin)session.get(Bin.class, (Serializable) a.getProductBinId());
			logger.trace("Captura de datos{}", cardFranchise);

			generalCardProduct = new GeneralCardProduct(cardFranchise,cardType, productBin); 
				session.beginTransaction();
				session.save(generalCardProduct).toString();
				session.getTransaction().commit();
				/*acquirer.setBank(null);*/
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		if(cardFranchise == null) return new Response(ResponseType.RESOURCENOTFOUND, "La franquicia solicitada no existe", "Identificador no encontrado");  
		else {
			generalCardProduct.setCardFranchiseId(null);
			generalCardProduct.setCardTypeId(null);
			generalCardProduct.setProductBinId(null);
			return generalCardProduct;
		}

		
	}
	
	/**
	 * Modificar Producto General: Permite cambiar un subconjunto de los datos del producto general (GeneralCardProduct). 
	 * 
	 * @param general_product_id  Parametro extraido de la URL que representa el identificador del producto general (GeneralCardProduct) a modificar
	 * 
	 * @param c Objeto de tipo <strong>GeneralCardProduct</strong> con los datos del producto general(GeneralCardProduct)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/general/{general_product_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updateGeneralCardProduct(@PathParam("general_product_id") int generalProductId, GeneralCardProduct c){
		GeneralCardProduct generalCardProduct = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			CardFranchise cardFranchise = (CardFranchise) session.get(CardFranchise.class, c.getCardFranchiseId());
			CardType cardType= (CardType) session.get(CardType.class, c.getCardTypeId());
			Bin productBin= (Bin) session.get(Bin.class, c.getProductBinId()); 
			generalCardProduct = (GeneralCardProduct)session.get(GeneralCardProduct.class, generalProductId);
			if (cardFranchise != null && cardType != null && productBin != null && generalCardProduct != null) {
				c.setCardFranchise(cardFranchise);
				c.setCardType(cardType);
				c.setProductBin(productBin);
				generalCardProduct.update(c);
				session.beginTransaction();
				session.update(generalCardProduct);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return generalCardProduct == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el producto solicitado", "Recurso no encontrado") :  
									 new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El producto general ha sido actualizado correctamente");
	}
	
	/**
	 * Crear Producto espec�fico: Crea una entidad del tipo Producto espec�fico (BankCardProduct) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param a Objeto de tipo <strong>BankCardProduct</strong> con los datos del Producto espec�fico  (BankCardProduct)
	 * 
	 * @return Datos de la entidad de tipo Producto espec�fico (BankCardProduct) que ha sido creada
	 */
	@POST
	@Path("/brand")
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createBankCardProduct(BankCardProduct a) {
		logger.debug("createBankCardProduct");
		BankCardProduct bankCardProduct= null;
		CardFranchise cardFranchise = null;
		CardType cardType= null; 
		Bin productBin= null; 
		Bank cardBank = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			cardFranchise = (CardFranchise)session.get(CardFranchise.class, (Serializable) a.getCardFranchiseId());
			cardType = (CardType)session.get(CardType.class, (Serializable) a.getCardTypeId());
			productBin = (Bin)session.get(Bin.class, (Serializable) a.getProductBinId());
			cardBank = (Bank)session.get(Bank.class, (Serializable) a.getCardBankId());	
			logger.trace("Captura de datos{}", cardFranchise);
			/*affiliation = (Affiliation)session.get(Affiliation.class, a.getAffiliationId());*/
			logger.trace("Banco tarjeta {}", cardBank);
			if(cardFranchise != null && cardBank != null && productBin != null) {
				bankCardProduct = new BankCardProduct(
						cardFranchise,
						cardType, 
						productBin,
						cardBank); 
				session.beginTransaction();
				session.save(bankCardProduct).toString();
				session.getTransaction().commit();
				/*acquirer.setBank(null);*/
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		if(cardFranchise == null) return new Response(ResponseType.RESOURCENOTFOUND, "La franquicia solicitads no existe", "Identificador no encontrado"); 
		if(cardBank == null) return new Response(ResponseType.RESOURCENOTFOUND, "El banco solicitado no existe", "Identificador no encontrado"); 
		else {
			bankCardProduct.setCardBankId(null);
			bankCardProduct.setCardFranchiseId(null);
			bankCardProduct.setCardTypeId(null);
			bankCardProduct.setProductBinId(null);
			return bankCardProduct;
		}
	}
	
	/**
	 * Modificar Producto espec�fico: Permite cambiar un subconjunto de los datos de Producto espec�fico (BankCardProduct). Estos son: nombre
	 * 
	 * @param bankId  Parametro extraido de la URL que representa el identificador del Producto espec�fico (BankCardProduct) a modificar
	 * 
	 * @param c Objeto de tipo <strong>BankCardProduct</strong> con los datos del Producto espec�fico (BankCardProduct)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/brand/{brand_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updateBankCardProduct(@PathParam("brand_id") int bankCardProductId, BankCardProduct c){
		BankCardProduct bankCardProduct = null;
		logger.debug("createBankCardProduct");
		CardFranchise cardFranchise = null;
		CardType cardType= null; 
		Bin productBin= null; 
		Bank cardBank = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			cardFranchise = (CardFranchise)session.get(CardFranchise.class, (Serializable) c.getCardFranchiseId());
			cardType = (CardType)session.get(CardType.class, c.getCardTypeId());
			productBin = (Bin)session.get(Bin.class, c.getProductBinId());
			cardBank = (Bank)session.get(Bank.class,  c.getCardBankId());	
			bankCardProduct = (BankCardProduct)session.get(BankCardProduct.class, bankCardProductId);
			if (bankCardProduct != null && cardBank != null && cardFranchise != null && cardType != null && productBin != null) {
				c.setCardBank(cardBank);
				c.setCardFranchise(cardFranchise);
				c.setCardType(cardType);
				c.setProductBin(productBin);
				bankCardProduct.update(c);
				session.beginTransaction();
				session.update(bankCardProduct);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bankCardProduct == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el producto espec�fico solicitado", "Recurso no encontrado") :  
									 new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El producto espec�fico ha sido actualizado correctamente");

	}
}
