package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Parish;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.ParishList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Parish (Parroquia)
 */
@Path("/parish")
public class ParishResource extends APIResponse {
	
	/**
	 * Listar Parroquias por Municipio: Construye una lista con todas las entidades de tipo Parroquia (Parish) asociadas a un municipio almacenadas en la base de datos
	 * 
	 * @param municipalityId El identificador del municipio al que estan asociadas las parroquias
	 * 
	 * @return La lista de todas las entidades de tipo Parroquia (Parish)
	 */
	@Path("/byMunicipality/{municipality_id: \\d+ }")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getParishListByMunicipality(@PathParam("municipality_id") int municipalityId) { 
		Session session = null;
		ParishList parishList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.ParishMin(id, municipalityId, parishName) from Parish as parish where parish.municipalityId = :municipalityId";
			parishList = new ParishList(session.createQuery(hsqlquery).setInteger("municipalityId", municipalityId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return parishList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna parroquia", "Recurso no encontrado") : parishList;
	}
	
	/**
	 * Consultar Parroquia: Consultar los datos de una entidad de tipo Parroquia (Parish)
	 * 
	 * @param parishId Identificador del recurso de tipo Parroquia (Parish)
	 * 
	 * @return Datos de la entidad de tipo Parroquia (Parish) identificada por <strong>parishId</strong>
	 */
	@Path("/{parish_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getParish(@PathParam("parish_id") int parishId) { 
		Session session = null;
		Parish parish = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			parish = (Parish)session.get(Parish.class, parishId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return parish == null ? new Response(ResponseType.RESOURCENOTFOUND, "La parroquia solicitada no existe", "Identificador no encontrado") : parish;
	}
}
