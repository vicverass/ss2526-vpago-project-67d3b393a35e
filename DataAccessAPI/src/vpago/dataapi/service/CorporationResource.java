package vpago.dataapi.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Query;
import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Corporation;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.CorporationList;
import vpago.dataapi.classes.wrappers.MerchantList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Corporacion (Corporation)
 */
@Path("/corporation")
public class CorporationResource extends ResourceManager {
	
	/**
	 * Crear Corporación: Crea una entidad del tipo Corporación (Corporation) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param c Objeto de tipo <strong>Corporation</strong> con los datos de la Corporacion (Corporation)
	 * 
	 * @return Datos de la entidad de tipo Corporacion (Corporation) que ha sido creada
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createCorporation(Corporation c){
		Corporation corporation = null;
		Session session = null;
		try {
			corporation = new Corporation(c.getName(), c.getFantasyName(), c.getRif());
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(corporation);
			session.getTransaction().commit();
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return corporation;
	}
	
	/**
	 * Consultar Corporación: Consultar los datos de una entidad de tipo Corporación (Corporation)
	 * 
	 * @param corporationId Parametro extraido de la URL que representa el identificador de la Corporacion (Corporation) a consultar
	 * 
	 * @return Datos de la entidad de tipo Corporación (Corporation) identificada por <strong>corporationId</strong>
	 */
	@Path("/{corporation_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getCorporation(@PathParam("corporation_id") int corporationId){
		Session session = null;
		Corporation corporation = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			corporation = (Corporation)session.get(Corporation.class, corporationId);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return corporation == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro la corporacion solicitada", "Recurso no encontrado") : corporation;
	}
	
	/**
	 * Listar Corporaciones: Construye una lista con todas las entidades de tipo Corporacion (Corporation) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Corporacion (Corporation)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllCorporations(
			@QueryParam("acquirerBank") Integer acquirerBank
			){
		Session session = null;
		CorporationList corporationList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			String corporationAcqQuery = 
					"select new vpago.dataapi.classes.minified.CorporationMin("
					+ "corporation.id, corporation.name, corporation.fantasyName, corporation.rif) "
					+ "from Corporation as corporation "
					+ ((acquirerBank != null) ? 				
					"inner join corporation.merchants as merchant "
					+ "inner join merchant.affiliations as affiliation "
					+ "inner join affiliation.acquirers as acquirer "
					+ "where acquirer.bankId = :acquirerBank " : "")
					+ "order by corporation.name asc"
					;
			logger.debug("Query: {}", corporationAcqQuery);
			
			Query corporationQuery = session.createQuery(corporationAcqQuery);
			if(acquirerBank != null) corporationQuery.setInteger("acquirerBank", acquirerBank);
			corporationList = new CorporationList(
					corporationQuery
					.list());
			corporationList.getCorporation().forEach(corporation->{
				logger.debug("Corporacion asociada a {}: {}", acquirerBank, corporation);
			});
			
			//String hsqlquery = "select new vpago.dataapi.classes.minified.CorporationMin(id, name, fantasyName, rif) from Corporation as corporation";
			//corporationList = new CorporationList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return corporationList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontr� ninguna corporacion", "Recurso no encontrado") : corporationList;
	}
	
	/**
	 * Modificar datos de Corporación: Permite cambiar un subconjunto de los datos de la Corporacion (Corporation). Estos son: Nombre fantasía, nombre y rif
	 * 
	 * @param corporationId  Parametro extraido de la URL que representa el identificador de la Corporacion (Corporation) a modificar
	 * 
	 * @param c Objeto de tipo <strong>Corporation</strong> con los datos de la Corporacion (Corporation)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/{corporation_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updateCorporation(@PathParam("corporation_id") int corporationId, Corporation c){
		Corporation corporation = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			corporation = (Corporation)session.get(Corporation.class, corporationId);
			if (corporation != null) {
				corporation.update(c);
				session.beginTransaction();
				session.update(corporation);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return corporation == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro la corporacion solicitada", "Recurso no encontrado") :  
									 new Response(ResponseType.SUCCESS, "Solicitud exitosa", "La corporacion ha sido actualizada correctamente");
	}
	
	/**
	 * Listar comercios asociados a Corporación: Construye una lista con todas las entidades de tipo Comercio (Merchant/Multimerchant) asociadas a la entidad Corporación (Corporation) indicada
	 * 
	 * @param corporationId Parametro extraido de la URL que representa el identificador de la Corporacion (Corporation)
	 * 
	 * @return La lista de todas las entidades de tipo Comercio (Merchant) asociadas a la Corporacion (Corporation) identificada por <strong>corporationId</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("/{corporation_id: \\d+ }/merchants")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getCorporationMerchants(@PathParam("corporation_id") int corporationId){
		Session session = null;
		MerchantList merchantList = null;
		Corporation corporation = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			corporation = (Corporation)session.get(Corporation.class, corporationId);
			if (corporation != null) {
				String hsqlquery = "select new vpago.dataapi.classes.minified.MerchantMin(id, merchantGuid, name, rif, fantasyName, active, merchantType) from Merchant as merchant where merchant.corporationId=:corporationId";
				merchantList = new MerchantList(
				session.createQuery(hsqlquery).setInteger("corporationId", corporation.getId()).list());
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(corporation == null) return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro la corporacion solicitada", "Recurso no encontrado");
		return merchantList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun comercio asociado a la corporacion", "Recurso no encontrado") : merchantList;
	}
	
	/**
	 * Asociar Comercio/Multicomercio a Corporación: Relaciona una entidad de tipo Comercio/Multicomercio (Merchant/Multimerchant) con una entidad de tipo Corporacion (Corporation)
	 * 
	 * @param corporationId Parametro extraido de la URL que representa el identificador de la Corporacion (Corporation)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio/Multicomercio (Merchant/Multimerchant)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/{corporation_id: \\d+ }/add/{merchant_guid: [a-fA-F\\d]{16}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse addMerchantToCorporation(@PathParam("corporation_id") int corporationId, @PathParam("merchant_guid") String merchantGuid) {
		Session session = null;
		Merchant merchant = null;
		Corporation corporation = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if (merchant != null) {
				session = HibernateUtil.getSessionFactory().openSession();
				corporation = (Corporation)session.get(Corporation.class, corporationId);
				merchant.setCorporation(corporation != null ? corporation : merchant.getCorporation());
				session.beginTransaction();
				session.update(merchant);
				session.getTransaction().commit();
			} 
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return (merchant != null) && (corporation != null) ? new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Comercio asociado a Corporacion")
				: (merchant == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado") 
						: new Response(ResponseType.RESOURCENOTFOUND, "No se encontro la corporacion solicitada", "Recurso no encontrado"));
	}
}
