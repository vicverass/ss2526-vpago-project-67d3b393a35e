package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.PostalZone;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.PostalZoneList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso PostalZone (Zona Postal)
 */
@Path("/postalZone")
public class PostalZoneResource extends ResourceManager {
	
	/**
	 * Listar Zonas Postales por Parroquia: Construye una lista con todas las entidades de tipo Zona Postal (PostalZone) asociadas a un estado almacenadas en la base de datos
	 * 
	 * @param parishId El identificador de la parroquia a la que estan asociados los codigos postales
	 * 
	 * @return La lista de todas las entidades de tipo Zona Postal (PostalZone)
	 */
	@Path("/byParish/{parish_id: \\d+ }")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getPostalZonesByParish(@PathParam("parish_id") int parishId) { 
		Session session = null;
		PostalZoneList postalZoneList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.PostalZoneMin(id, parishId, townName, postalCode) from PostalZone as postalZone where postalZone.parishId = :parishId";
			postalZoneList = new PostalZoneList(session.createQuery(hsqlquery).setInteger("parishId", parishId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return postalZoneList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna zona postal", "Recurso no encontrado") : postalZoneList;
	}
	
	/**
	 * Consultar Zona Postal: Consultar los datos de una entidad de tipo Zona Postal (PostalZone)
	 * 
	 * @param postalZoneId Identificador del recurso de tipo Zona Postal (PostalZone)
	 * 
	 * @return Datos de la entidad de tipo Zona Postal (PostalZone) identificada por <strong>postalZoneId</strong>
	 */
	@Path("/{postal_zone_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getPostalZone(@PathParam("postal_zone_id") int postalZoneId) { 
		Session session = null;
		PostalZone postalZone = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			postalZone = (PostalZone)session.get(PostalZone.class, postalZoneId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return postalZone == null ? new Response(ResponseType.RESOURCENOTFOUND, "La zona postal solicitada no existe", "Identificador no encontrado") : postalZone;
	}
}
