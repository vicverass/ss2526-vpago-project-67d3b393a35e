package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.State;
import vpago.dataapi.classes.wrappers.StateList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso State (Estado)
 */
@Path("/state")
public class StateResource extends ResourceManager {
	
	/**
	 * Listar Estados por Pais: Construye una lista con todas las entidades de tipo Estado (State) asociadas a un pais almacenadas en la base de datos
	 * 
	 *  @param countryId Identificador del pais al que estan asociados los estados
	 *  
	 * @return La lista de todas las entidades de tipo Estado (State)
	 */
	@Path("/byCountry/{country_id: \\d+ }")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getStatesByCountry(@PathParam("country_id") int countryId) { 
		Session session = null;
		StateList stateList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.StateMin(id, countryId, stateName, iso31662Code) from State as state where state.countryId = :countryId";
			stateList = new StateList(session.createQuery(hsqlquery).setInteger("countryId", countryId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return stateList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun estado", "Recurso no encontrado") : stateList;
	}
	
	/**
	 * Consultar Estado: Consultar los datos de una entidad de tipo Estado (State)
	 * 
	 * @param stateId Identificador del recurso de tipo Estado (State)
	 * 
	 * @return Datos de la entidad de tipo Estado (State) identificada por <strong>stateId</strong>
	 */
	@Path("/{state_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getState(@PathParam("state_id") int stateId) { 
		Session session = null;
		State state = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			state = (State)session.get(State.class, stateId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return state == null ? new Response(ResponseType.RESOURCENOTFOUND, "El estado solicitado no existe", "Identificador no encontrado") : state;
	}
}
