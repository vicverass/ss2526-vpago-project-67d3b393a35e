package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.IndustryType;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.Sector;
import vpago.dataapi.classes.wrappers.IndustryTypeList;
import vpago.dataapi.classes.wrappers.SectorList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los metodos para los servicios relacionados a los recursos Sector (Sector) y Tipo de Industria (IndustryType) 
 */
@Path("/sector")
public class MerchantSectorResource extends ResourceManager {

	/**
	 * Obtener lista de sectores economicos: Construye una lista con todas las entidades de tipo Sector (Sector) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Sector (Sector)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllSectors(){
		Session session = null;
		SectorList sectorList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.Sector(id, number, name) from Sector as sector";
			sectorList = new SectorList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return sectorList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun sector", "Recurso no encontrado") : sectorList;
	}
	
	/**
	 * Obtener tipos de industrias por sector economico: Construye una lista con todas las entidades de tipo Tipo de Industria (IndustryType) asociadas a un Sector (Sector)
	 * 
	 * @param sectorId Identificador del recurso de tipo Sector (Sector)
	 * 
	 * @return La lista de todas las entidades de tipo Tipo de Industria (IndustryType) asociadas al Sector (Sector) identificado por <strong>sectorId</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("/{sector_id: \\d+ }/types")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllIndustryTypes(@PathParam("sector_id") Integer sectorId){
		Session session = null;
		IndustryTypeList industryTypeList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.IndustryType(id, number, name, sector) from IndustryType as industryType where industryType.sectorId = :sectorId";
			industryTypeList = new IndustryTypeList(session.createQuery(hsqlquery).setInteger("sectorId", sectorId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return industryTypeList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun tipo de industria", "Recurso no encontrado") : industryTypeList;
	}
	
	/**
	 * Consultar datos de sector: Consultar los datos de una entidad de tipo Sector (Sector)
	 * 
	 * @param sectorId Identificador del recurso de tipo Sector (Sector)
	 * 
	 * @return Datos de la entidad de tipo Sector (Sector) identificada por <strong>sectorId</strong>
	 */
	@Path("/{sector_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getSector(@PathParam("sector_id") Integer sectorId){
		Session session = null;
		Sector sector = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			sector = (Sector)session.get(Sector.class, sectorId);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return sector == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el sector solicitado", "Recurso no encontrado") : sector;
	}
	
	/**
	 * Consultar datos de tipo de industria: Consultar los datos de una entidad de tipo Tipo de Industria (IndustryType)
	 * 
	 * @param industryId Identificador del recurso de tipo Tipo de Industria (IndustryType)
	 * 
	 * @return Datos de la entidad de tipo Tipo de Industria (IndustryType) identificada por <strong>industryId</strong>
	 */
	@Path("/types/{industry_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getIndustryType(@PathParam("industry_id") Integer industryId){
		Session session = null;
		IndustryType industryType = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			industryType = (IndustryType)session.get(IndustryType.class, industryId);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return industryType == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el tipo de industria solicitado", "Recurso no encontrado") : industryType;
	}
	
	
}
