package vpago.dataapi.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Bank;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.BankList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Bank (Banco)
 */
@Path("/bank")
public class BankResource extends ResourceManager {
	
	/**
	 * Listar Bancos: Construye una lista con todas las entidades de tipo Banco (Bank) alamcenadas en la base de datos
	 * 
	 * @return La lista de todas las entidades de tipo Banco (Bank)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllBanks() { 
		Session session = null;
		BankList bankList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.BankMin(id, name, rif) from Bank as bank";
			bankList = new BankList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bankList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun banco", "Recurso no encontrado") : bankList;
	}
	
	/**
	 * Consultar Banco: Consultar los datos de una entidad de tipo Banco (Bank)
	 * 
	 * @param bankId Identificador del recurso de tipo Banco (Bank)
	 * 
	 * @return Datos de la entidad de tipo Banco (Bank) identificada por <strong>bankId</strong>
	 */
	@Path("/{bank_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getBank(@PathParam("bank_id") int bankId) { 
		Session session = null;
		Bank bank = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			bank = (Bank)session.get(Bank.class, bankId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bank == null ? new Response(ResponseType.RESOURCENOTFOUND, "El banco solicitado no existe", "Identificador no encontrado") : bank;
	}
	
	/**
	 * Crear Banco: Crea una entidad de tipo banco (Bank) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param b Objeto de tipo <strong>Bank</strong> con los datos del Banco (Bank)
	 * 
	 * @return Datos de la entidad de tipo Banco (Bank) que ha sido creada
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createBank(Bank b){
		Bank bank =null;
		Session session = null;
		try {
			bank = new Bank(b.getName(), b.getRif(), b.getAcquirerCode());
			if(bank != null){
				session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				session.save(bank);
				session.getTransaction().commit();
			}			
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bank;
	}
	
	/**
	 * Modificar Banco: Permite cambiar un subconjunto de los datos de Banco (Bank).
	 * 
	 * @param bankId  Parametro extraido de la URL que representa el identificador de la Banco (Bank) a modificar
	 * 
	 * @param c Objeto de tipo <strong>Bank</strong> con los datos de la Banco (Bank)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/{bank_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updateBank(@PathParam("bank_id") int bankId, Bank c){
		Bank bank = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			bank = (Bank)session.get(Bank.class, bankId);
			if (bank != null) {
				bank.update(c);
				session.beginTransaction();
				session.update(bank);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return bank == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el banco solicitado", "Recurso no encontrado") :  
									 new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El banco ha sido actualizado correctamente");
	}
	
}
