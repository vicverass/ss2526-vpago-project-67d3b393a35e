package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Municipality;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.MunicipalityList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Municipality (Municipio)
 */
@Path("/municipality")
public class MunicipalityResource extends ResourceManager {
	
	/**
	 * Listar Municipios por Estado: Construye una lista con todas las entidades de tipo Municipio (Municipality) asociadas a un estado almacenadas en la base de datos
	 * 
	 * @param stateId Identificador del estado al que estan asociados los municipios
	 * 
	 * @return La lista de todas las entidades de tipo Municipio (Municipality)
	 */
	@Path("/byState/{state_id: \\d+ }")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMunicipalitiesByState(@PathParam("state_id") int stateId) { 
		Session session = null;
		MunicipalityList municipalityList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.MunicipalityMin(id, stateId, municipalityName) from Municipality as municipality where municipality.stateId = :stateId";
			municipalityList = new MunicipalityList(session.createQuery(hsqlquery).setInteger("stateId", stateId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return municipalityList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun municipio", "Recurso no encontrado") : municipalityList;
	}
	
	/**
	 * Consultar Municipio: Consultar los datos de una entidad de tipo Municipio (Municipality)
	 * 
	 * @param municipalityId Identificador del recurso de tipo Municipio (Municipality)
	 * 
	 * @return Datos de la entidad de tipo Municipio (Municipality) identificada por <strong>municipalityId</strong>
	 */
	@Path("/{municipality_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMunicipality(@PathParam("municipality_id") int municipalityId) { 
		Session session = null;
		Municipality municipality = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			municipality = (Municipality)session.get(Municipality.class, municipalityId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return municipality == null ? new Response(ResponseType.RESOURCENOTFOUND, "El municipio solicitado no existe", "Identificador no encontrado") : municipality;
	}
}
