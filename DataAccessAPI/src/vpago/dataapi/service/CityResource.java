package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.City;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.CityList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso City (Ciudad)
 */
@Path("/city")
public class CityResource extends ResourceManager {
	
	/**
	 * Listar Ciudades por Estado: Construye una lista con todas las entidades de tipo Ciuda (City) asociadas a un estado almacenadas en la base de datos
	 * 
	 * @param stateId Identificador del estado al que estan asociadas las ciudades
	 * 
	 * @return La lista de todas las entidades de tipo Ciudad (City)
	 */
	@Path("/byState/{state_id: \\d+ }")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getCitiesByState(@PathParam("state_id") int stateId) { 
		Session session = null;
		CityList cityList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.CityMin(id, stateId, cityName, capital) from City as city where city.stateId = :stateId";
			cityList = new CityList(session.createQuery(hsqlquery).setInteger("stateId", stateId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return cityList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna ciudad", "Recurso no encontrado") : cityList;
	}
	
	/**
	 * Consultar Ciudad: Consultar los datos de una entidad de tipo Ciudad (City)
	 * 
	 * @param cityId Identificador del recurso de tipo Ciudad (City)
	 * 
	 * @return Datos de la entidad de tipo Ciudad (City) identificada por <strong>cityId</strong>
	 */
	@Path("/{city_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getCity(@PathParam("city_id") int cityId) { 
		Session session = null;
		City city = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			city = (City)session.get(City.class, cityId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return city == null ? new Response(ResponseType.RESOURCENOTFOUND, "La ciudad solicitada no existe", "Identificador no encontrado") : city;
	}
}
