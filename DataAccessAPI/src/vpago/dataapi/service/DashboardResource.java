package vpago.dataapi.service;

import static java.lang.Math.toIntExact;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.LongType;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.forms.Dashboard;
import vpago.dataapi.classes.forms.DashboardMerchant;
import vpago.dataapi.classes.forms.DashboardTerminal;
import vpago.dataapi.classes.minified.DashboardLatestTicketMin;
import vpago.dataapi.classes.minified.DashboardMerchantByAcquirerMin;
import vpago.dataapi.classes.wrappers.DashboardLatestTicketList;
import vpago.dataapi.classes.wrappers.DashboardMerchantByAcquirerList;
import vpago.dataapi.classes.wrappers.DashboardMerchantByStateList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
/**
 * Contiene todos los metodos para los servicios relacionados al recurso de Transacciones
 */
@Path("/dashboard")
public class DashboardResource extends ResourceManager {
	
	/**
	 * Consultar toda la informaci�n necesaria para las tarjetas de los dashboards del V-Portal
	 * 
	 * @param associateId
	 * @return
	 */
	@Path("/card")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllTransactionsForCard(@QueryParam("associateId") String associateId,
			@QueryParam("rolUser") String rolUser){
		logger.trace("Solicitud de todas las transacciones");
		Session session = null;
		Dashboard dashboardList = new Dashboard();
		Integer terminalCount = null;
		Integer terminalInactiveCount = null;
		Integer merchantCount = null;
		Integer multimerchantCount = null;
		Integer corporationCount = null;
		Integer transactionOfDayCount = null;
		Integer transactionOfWeekCount = null;
		Integer transactionOfMonthCount = null;
		double amountTransactionOfDay = 0.0;
		double amountTransactionOfWeek =0.0;
		double amountTransactionOfMonth = 0.0;
		Date ahora = new Date();
		logger.debug("ahora {}", ahora);
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = formateador.format(ahora);
		logger.debug("Fecha {}",fecha);

		Calendar week = Calendar.getInstance();
		Calendar month = Calendar.getInstance();
		
		week.setTime(ahora);
		month.setTime(ahora);
		
		week.add(Calendar.DATE, -week.get(Calendar.DAY_OF_WEEK)+1);
		month.add(Calendar.DATE, -month.get(Calendar.DAY_OF_MONTH)+1);
		
		String dateWeek = formateador.format(week.getTime());
		String dateMonth = formateador.format(month.getTime());
		
		logger.debug("week {}", dateWeek);
		logger.debug("month {}", dateMonth);
		logger.debug("DAY_OF_WEEK: {}", week.get(Calendar.DAY_OF_WEEK));
		logger.debug("DAY_OF_MONTH: {}", month.get(Calendar.DAY_OF_MONTH));
		
		//Variables para calcular las cantidades de transacciones (dia, semana, mes)
		String queryDay =  "";
		String queryWeek = "";
		String queryMonth = "";
		Query resultDay =  null;
		Query resultWeek =  null;
		Query resultMonth =  null;
		
		//Variables para calcular los montos transados (dia, semana, mes)
		String amountDay = "";
		String amountWeek = "";
		String amountMonth = "";
		try {
			session = HibernateUtil.getSessionFactory().openSession();			
			try {
				switch(rolUser){
					case "A":
						logger.info("Filtro usuarios de tipo adquirientes (Bancos) - rol:{}", rolUser);
						terminalCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=true "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						terminalInactiveCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=false "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						merchantCount = toIntExact((Long)session.createQuery("select count(*) from Merchant as merchant where merchant.active=true"
								+ ((associateId != null) ? " and merchant.id "
								+ "IN (SELECT merchantId FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						multimerchantCount = toIntExact((Long)session.createQuery("select count(*) from Multimerchant as multimerchant").uniqueResult());
						corporationCount = toIntExact((Long)session.createQuery("select count(*) from Corporation as corporation").uniqueResult());
					
						//Cantidad de transacciones por adquiriente (dia, semana, mes)
						queryDay ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
						resultDay = session.createQuery(queryDay);
						
						queryWeek ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultWeek = session.createQuery(queryWeek);
						
						queryMonth ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultMonth = session.createQuery(queryMonth);
						
						transactionOfDayCount = toIntExact((Long) resultDay.uniqueResult());
						transactionOfWeekCount = toIntExact((Long) resultWeek.uniqueResult());			
						transactionOfMonthCount = toIntExact((Long) resultMonth.uniqueResult());
						
						//Monto transado (dia, semana y mes)
						try{
							amountDay = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+"and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
							
							amountWeek = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountMonth = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountTransactionOfDay = (double) session.createQuery(amountDay).uniqueResult();
							amountTransactionOfWeek = (double) session.createQuery(amountWeek).uniqueResult();
							amountTransactionOfMonth = (double) session.createQuery(amountMonth).uniqueResult();
						}catch (Exception e){
							amountTransactionOfDay = 0.0;
							amountTransactionOfWeek = 0.0;
							amountTransactionOfMonth = 0.0;
						}
						break;
					case "a":
						logger.info("Filtro usuarios de tipo adquirientes (Bancos) - rol:{}", rolUser);
						terminalCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=true "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						terminalInactiveCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=false"
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						merchantCount = toIntExact((Long)session.createQuery("select count(*) from Merchant as merchant where merchant.active=true"
								+ ((associateId != null) ? " and merchant.id "
								+ "IN (SELECT merchantId FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))" : "")).uniqueResult());
						multimerchantCount = toIntExact((Long)session.createQuery("select count(*) from Multimerchant as multimerchant").uniqueResult());
						corporationCount = toIntExact((Long)session.createQuery("select count(*) from Corporation as corporation").uniqueResult());
					
						//Cantidad de transacciones por adquiriente
						queryDay ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
						resultDay = session.createQuery(queryDay);
						
						queryWeek ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultWeek = session.createQuery(queryWeek);
						
						queryMonth ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultMonth = session.createQuery(queryMonth);
						
						transactionOfDayCount = toIntExact((Long) resultDay.uniqueResult());
						transactionOfWeekCount = toIntExact((Long) resultWeek.uniqueResult());			
						transactionOfMonthCount = toIntExact((Long) resultMonth.uniqueResult());
						
						//Monto transado (dia, semana y mes)
						try{
							amountDay = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+"and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
							
							amountWeek = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountMonth = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.id "
								+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountTransactionOfDay = (double) session.createQuery(amountDay).uniqueResult();
							amountTransactionOfWeek = (double) session.createQuery(amountWeek).uniqueResult();
							amountTransactionOfMonth = (double) session.createQuery(amountMonth).uniqueResult();
						}catch (Exception e){
							amountTransactionOfDay = 0.0;
							amountTransactionOfWeek = 0.0;
							amountTransactionOfMonth = 0.0;
						}						
						break;
					case "M":
						logger.info("Filtro usuarios de tipo comercio - rol:{}", rolUser);
						terminalCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=true "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))" : "")).uniqueResult());
						terminalInactiveCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=false "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))" : "")).uniqueResult());
						logger.info("Terminal activo {}", terminalCount);
						logger.info("Terminal inactivo {}", terminalInactiveCount);
						merchantCount = 0;
						multimerchantCount = 0;
						corporationCount = 0;
						
						//Cantidad de transacciones por adquiriente (dia, semana, mes)
						queryDay ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
						resultDay = session.createQuery(queryDay);
						
						queryWeek ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultWeek = session.createQuery(queryWeek);
						
						queryMonth ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultMonth = session.createQuery(queryMonth);
						
						transactionOfDayCount = toIntExact((Long) resultDay.uniqueResult());
						transactionOfWeekCount = toIntExact((Long) resultWeek.uniqueResult());			
						transactionOfMonthCount = toIntExact((Long) resultMonth.uniqueResult());
					
						//Monto transado (dia, semana y mes)
						try{
							amountDay = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+"and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
							
							amountWeek = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountMonth = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountTransactionOfDay = (double) session.createQuery(amountDay).uniqueResult();
							amountTransactionOfWeek = (double) session.createQuery(amountWeek).uniqueResult();
							amountTransactionOfMonth = (double) session.createQuery(amountMonth).uniqueResult();
						}catch (Exception e){
							amountTransactionOfDay = 0.0;
							amountTransactionOfWeek = 0.0;
							amountTransactionOfMonth = 0.0;
						}		
						break;
					case "m":
						logger.info("Filtro usuarios de tipo comercio - rol:{}", rolUser);
						terminalCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=true "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id  FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))" : "")).uniqueResult());
						terminalInactiveCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=false "
								+ ((associateId != null) ? " and terminal.affiliationId "
								+ "IN (SELECT id  FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))" : "")).uniqueResult());
						logger.info("Terminal activo {}", terminalCount);
						logger.info("Terminal inactivo {}", terminalInactiveCount);
						merchantCount = 0;
						multimerchantCount = 0;
						corporationCount = 0;
						
						//Cantidad de transacciones por adquiriente (dia, semana, mes)
						queryDay ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
						resultDay = session.createQuery(queryDay);
						
						queryWeek ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultWeek = session.createQuery(queryWeek);
						
						queryMonth ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
						resultMonth = session.createQuery(queryMonth);
						
						transactionOfDayCount = toIntExact((Long) resultDay.uniqueResult());
						transactionOfWeekCount = toIntExact((Long) resultWeek.uniqueResult());			
						transactionOfMonthCount = toIntExact((Long) resultMonth.uniqueResult());
					
						//Monto transado (dia, semana y mes)
						try{
							amountDay = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+"and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
							
							amountWeek = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountMonth = "SELECT sum(transaction.amount) FROM Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
								+ "IN (SELECT id FROM TransactionType as tt where tt.reportValidation=true) "
								+ "and transaction.terminalId "
								+ "IN (SELECT id FROM Terminal as terminal where terminal.affiliationId "
								+ "IN (SELECT id FROM Affiliation as a where a.active=true and a.merchant.id "
								+ "IN (SELECT id FROM Merchant where merchantGuid='"+associateId+"'))) "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' "
								+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
							
							amountTransactionOfDay = (double) session.createQuery(amountDay).uniqueResult();
							amountTransactionOfWeek = (double) session.createQuery(amountWeek).uniqueResult();
							amountTransactionOfMonth = (double) session.createQuery(amountMonth).uniqueResult();
						}catch (Exception e){
							amountTransactionOfDay = 0.0;
							amountTransactionOfWeek = 0.0;
							amountTransactionOfMonth = 0.0;
						}
						
						break;
				}
			}catch (NullPointerException e1) {
				logger.info("Sin filtros Usuarios Administradores/Master");
				terminalCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=true").uniqueResult());
				terminalInactiveCount = toIntExact((Long)session.createQuery("select count(*) from Terminal as terminal where terminal.active=false").uniqueResult());
				merchantCount = toIntExact((Long)session.createQuery("select count(*) from Merchant as merchant where merchant.active=true").uniqueResult());
				multimerchantCount = toIntExact((Long)session.createQuery("select count(*) from Multimerchant as multimerchant").uniqueResult());
				corporationCount = toIntExact((Long)session.createQuery("select count(*) from Corporation as corporation").uniqueResult());
			
				//Cantidad de transacciones (dia, semana, mes)
				queryDay ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
						+ "IN (SELECT id  FROM TransactionType as tt where tt.reportValidation=true) "
						+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
				resultDay = session.createQuery(queryDay);
				
				queryWeek ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
						+ "IN (SELECT id  FROM TransactionType as tt where tt.reportValidation=true) "
						+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
				resultWeek = session.createQuery(queryWeek);
				
				queryMonth ="select count(*) as c from Transaction as transaction where (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) and transaction.transactionTypeId  "
						+ "IN (SELECT id  FROM TransactionType as tt where tt.reportValidation=true) "
						+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
				resultMonth = session.createQuery(queryMonth);
				
				transactionOfDayCount = toIntExact((Long) resultDay.uniqueResult());
				transactionOfWeekCount = toIntExact((Long) resultWeek.uniqueResult());			
				transactionOfMonthCount = toIntExact((Long) resultMonth.uniqueResult());
				
				//Montro transado (dia, semana, mes)
				try{
					amountDay = "select sum(transaction.amount)"
							+ "from Transaction transaction "
							/*+ "inner join transactionType ON tt.id=transaction.transactionTypeId "*/
							+ "where transaction.transactionTypeId "
							+ "IN (select id from TransactionType transactionType where transactionType.reportValidation=true) "
							+ "and (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) "
							+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')='"+fecha+"'";
			
					amountWeek = "select sum(transaction.amount)"
							+ "from Transaction transaction "
							/*+ "inner join TransactionType transactionType ON tt.id=transaction.transactionTypeId "*/
							+ "where transaction.transactionTypeId "
							+ "IN (select id from TransactionType transactionType where transactionType.reportValidation=true) "
							+ "and (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) "
							+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateWeek+"' "
							+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
					
					amountMonth = "select sum(transaction.amount)"
							+ "from Transaction transaction "
							/*+ "inner join TransactionType transactionType ON tt.id=transaction.transactionTypeId "*/
							+ "where transaction.transactionTypeId "
							+ "IN (select id from TransactionType transactionType where transactionType.reportValidation=true) "
							+ "and (transaction.transactionStatus=0 or transaction.transactionStatus=7) and (transaction.transactionResponseId=38) "
							+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')>='"+dateMonth+"' "
							+ "and TO_CHAR(transaction.txnDate, 'YYYY-MM-DD')<='"+fecha+"'";
					logger.info("SQL amountDay {}",amountDay);
					amountTransactionOfDay = (double) session.createQuery(amountDay).uniqueResult();
					amountTransactionOfWeek = (double) session.createQuery(amountWeek).uniqueResult();
					amountTransactionOfMonth = (double) session.createQuery(amountMonth).uniqueResult();
				}catch (Exception e){
					amountTransactionOfDay = 0.0;
					amountTransactionOfWeek = 0.0;
					amountTransactionOfMonth = 0.0;
				}
			}catch (Exception e1) {
				logger.error("{}",e1);
			}
			
			
			logger.debug("Monto tranzado del dia  {}", amountTransactionOfDay);
			logger.debug("Monto tranzado semanal  {}", amountTransactionOfWeek);
			logger.debug("Monto tranzado mensual  {}", amountTransactionOfMonth);
			dashboardList.setCorporation(corporationCount);
			dashboardList.setMerchant(merchantCount);
			dashboardList.setMultimerchant(multimerchantCount);
			dashboardList.setTerminalActive(terminalCount);
			dashboardList.setTerminalInactive(terminalInactiveCount);
			dashboardList.setTransactionDay(transactionOfDayCount);
			dashboardList.setTransactionWeek(transactionOfWeekCount);
			dashboardList.setTransactionMonth(transactionOfMonthCount);
			dashboardList.setAmountTransactionOfDay(amountTransactionOfDay);
			dashboardList.setAmountTransactionOfWeek(amountTransactionOfWeek);
			dashboardList.setAmountTransactionOfMonth(amountTransactionOfMonth);
			
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		logger.debug("Antes de retornar");
		return dashboardList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardList;
	}
	
	/**
	 * Consultar toda la informaci�n para el grafico de Comercios Activos/Inactivos  
	 * para los dashboard del V-Portal
	 * 
	 * @param associateId
	 * @param rolUser
	 * @return
	 */
	@Path("/merchantGraphic")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllActiveAndInactiveMerchants(@QueryParam("associateId") Integer associateId,
			@QueryParam("rolUser") String rolUser){
		Session session = null;
		Integer merchantActiveCount = null;
		Integer merchantInactiveCount = null ;
		Integer allMerchantCount = null;
		Integer allMerchantAffiliationInac = null;
		DashboardMerchant dashboardMerchantList = new DashboardMerchant();
		session = HibernateUtil.getSessionFactory().openSession();	
		
		try {
			if(rolUser.equals("A") || rolUser.equals("a")){
				logger.info("Comercios activos/inactivos por adquiriente");
				String queryActive = "SELECT count(*) "
						+ "FROM Merchant as m "
						+ "WHERE m.active=true and m.id "
						+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true and a.id "
						+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))";
				
				String queryInactive = "SELECT count(*) "
						+ "FROM Merchant as m "
						+ "WHERE m.active=false and m.id "
						+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true and a.id "
						+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))";
				
				String queryAll = "SELECT count(*) "
						+ "FROM Merchant as m "
						+ "WHERE m.id "
						+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true and a.id "
						+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))";
				
				String queryAllInactive =  "SELECT count(*) "
						+ "FROM Merchant as m "
						+ "WHERE m.id "
						+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=false and a.id "
						+ "IN (SELECT affiliationId FROM Acquirer where bankId="+associateId+"))";
				
				merchantActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());
				
				merchantInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());

				allMerchantCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());

				allMerchantAffiliationInac = toIntExact((Long)session.createQuery(queryAllInactive).uniqueResult());
			}			
			
		} catch (NullPointerException e) {
			//Si los parameters son nulos imprime los datos globales de todos los terminales activos e inactivos
			logger.info("-- Imprimir datos de todos los comercios activos e inactivos --");
			String queryActive = "SELECT count(*) "
					+ "FROM Merchant as m "
					+ "WHERE m.active=true and m.id "
					+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true)";
			
			String queryInactive = "SELECT count(*) "
					+ "FROM Merchant as m "
					+ "WHERE m.active=false and m.id "
					+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true)";
			
			String queryAll = "SELECT count(*) "
					+ "FROM Merchant as m "
					+ "WHERE m.id "
					+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=true)";
			
			String queryAllInactive =  "SELECT count(*) "
					+ "FROM Merchant as m "
					+ "WHERE m.id "
					+ "IN (SELECT merchantId  FROM Affiliation as a where a.active=false)";
			
			merchantActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());
			
			merchantInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());

			allMerchantCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());

			allMerchantAffiliationInac = toIntExact((Long)session.createQuery(queryAllInactive).uniqueResult());

        } catch (Exception e) {
			logger.debug("exception {}", e);
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		logger.debug("Comercios Activos {}",merchantActiveCount);
		logger.debug("Comercios Inactivos {}",merchantInactiveCount);
		logger.debug("Todos los comercios con afiliaciones activas {}",allMerchantCount);
		logger.debug("Todos los comercios con afiliaciones inactivas {}",allMerchantAffiliationInac);
		
		dashboardMerchantList.setActive(merchantActiveCount);
		dashboardMerchantList.setInactive(merchantInactiveCount);
		dashboardMerchantList.setAllMerchant(allMerchantCount);
		dashboardMerchantList.setAllMerchantAffiliationInac(allMerchantAffiliationInac);
		logger.debug("dashboardMerchantList {}", dashboardMerchantList);
		return dashboardMerchantList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardMerchantList;
	}
	
	/**
	 * Consultar toda la informaci�n para el grafico de Comercios por Estados
	 * para los dashboard del V-Portal
	 * 
	 * @param associateId
	 * @param rolUser
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Path("/merchantByStateGraphic")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllMerchantsByState(@QueryParam("associateId") Integer associateId, 
			@QueryParam("rolUser") String rolUser){
		Session session = null;
		DashboardMerchantByStateList dashboardMerchanByState = null;
		session = HibernateUtil.getSessionFactory().openSession();	
		try {
			if(rolUser.equals("A") || rolUser.equals("a")){
				logger.debug("Comercios por estados filtrados por adquiriente");
				String query = "select new vpago.dataapi.classes.minified.DashboardMerchantByStateMin(s.stateName,(select count(*) "
						+"	FROM Merchant as m "
						+"	WHERE m.active=true AND m.id IN "
						+" 	(SELECT merchantId  FROM Affiliation as a WHERE a.active=true and a.id IN "
						+"	(SELECT affiliationId FROM Acquirer WHERE bankId="+associateId+"))"
						+"	AND m.location "
						+"	IN ("
						+"		SELECT id FROM Location WHERE address "
						+"		IN ( "
						+"			SELECT id FROM Address WHERE stateId IN (s.id)"
						+"			)"
						+"		)"
						+"	) ) "
						+"FROM State s ORDER BY s.stateName";
				Query merchants =  session.createQuery(query);
				dashboardMerchanByState = new DashboardMerchantByStateList(
						merchants.list()
						);
				logger.debug("dashboardMerchanByState {}", dashboardMerchanByState.getDashboardMerchantByState());
						
			}
			
		} catch (NullPointerException e) {
			//Si los parameters son nulos imprime los datos globales de todos los terminales activos e inactivos
			logger.debug("Comercios por estados sin filtro de adquiriente");
			String query = "select new vpago.dataapi.classes.minified.DashboardMerchantByStateMin(s.stateName,(select count(*) "
					+"	FROM Merchant "
					+"	WHERE active=true AND location "
					+"	IN ("
					+"		SELECT id FROM Location WHERE address "
					+"		IN ( "
					+"			SELECT id FROM Address WHERE stateId IN (s.id)"
					+"			)"
					+"		)"
					+"	) ) "
					+"FROM State s ORDER BY s.stateName";
			Query merchants =  session.createQuery(query);
			dashboardMerchanByState = new DashboardMerchantByStateList(
					merchants.list()
					);
        } catch (Exception e) {
			logger.debug("exception {}", e);
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
				
		return dashboardMerchanByState == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardMerchanByState;
	}
	
	/**
	 * Consultar todos los comercios por adquirientes
	 * 
	 * @param associateId
	 * @param rolUser
	 * @return XML o JSON 
	 */
	@SuppressWarnings("unchecked")
	@Path("/merchantsByAcquirerGraphic")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllMerchantsByAcquirer(){
		Session session = null;
		DashboardMerchantByAcquirerList dashboardMerchanByAcquirer = null;
		session = HibernateUtil.getSessionFactory().openSession();	
	
		try {
			String query = "select new vpago.dataapi.classes.minified.DashboardMerchantByAcquirerMin(b.name,(select count(*) "
					+" FROM Merchant as m "
					+" WHERE m.active=true and  m.id IN "
					+" (SELECT merchantId  FROM Affiliation as a WHERE a.active=true and a.id IN  "
					+" (SELECT affiliationId FROM Acquirer WHERE bankId=b.id)) ) ) "
					+" FROM Bank as b ORDER BY name";
			
			Query merchants =  session.createQuery(query);
			dashboardMerchanByAcquirer = new DashboardMerchantByAcquirerList(
					merchants.list()
					);
			logger.debug("dashboardMerchanByAcquirer {}", dashboardMerchanByAcquirer.getDashboardMerchantByAcquirer());
		} catch (Exception e) {
			logger.debug("exception {}", e);
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		return dashboardMerchanByAcquirer == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardMerchanByAcquirer;
	}
	
	/**
	 * Consultar toda la informaci�n para el grafico de Terminales Activos/Inactivos  
	 * para los dashboard del V-Portal
	 * 
	 * @param associateId
	 * @param rolUser
	 * @return
	 */
	@Path("/terminalGraphic")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllActiveAndInactiveTerminal(@QueryParam("associateId") String associateId, 
			@QueryParam("rolUser") String rolUser){
		Session session = null;
		Integer terminalActiveCount = null;
		Integer terminalInactiveCount = null ;
		DashboardTerminal dashboardTerminalList = new DashboardTerminal();
		Integer allTerminalCount = null;
		session = HibernateUtil.getSessionFactory().openSession();	
		try {
			if(rolUser.equals("A") || rolUser.equals("a")){
				logger.debug("terminales activos/inactivos por adquiriente");
				String queryActive = "SELECT COUNT(*)"
						+ "FROM Terminal as t "
						+ "WHERE t.active=true and t.affiliationId IN"
						+ "(SELECT affiliationId FROM Acquirer as a WHERE a.bankId ="+associateId+")";
				
				String queryInactive = "SELECT COUNT(*)"
						+ "FROM Terminal as t "
						+ "WHERE t.active=false and t.affiliationId IN"
						+ "(SELECT affiliationId FROM Acquirer as a WHERE a.bankId ="+associateId+")";
				
				String queryAll = "SELECT COUNT(*)"
						+ "FROM Terminal as t "
						+ "WHERE t.affiliationId IN"
						+ "(SELECT affiliationId FROM Acquirer as a WHERE a.bankId ="+associateId+")";
				
				terminalActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());
				
				terminalInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());

				allTerminalCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());

			}else if(rolUser.equals("C") || rolUser.equals("c")){
				logger.debug("terminales activos/inactivos por comercio");
				
				Query merchantType = session.createQuery("SELECT merchantType FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'");
				Query merchantId = session.createQuery("SELECT id FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'"); 
				Query affiliationId = session.createQuery("SELECT id FROM Affiliation as a WHERE a.merchantId="+merchantId.uniqueResult());
				
				logger.debug("merchantId {}",merchantId.uniqueResult());
				logger.debug("affiliationId {}", affiliationId.uniqueResult());
				String tipo = "simple";
				if(tipo.equals(merchantType.uniqueResult())){
					logger.debug("Comercio simple");
					String queryActive = "SELECT COUNT(*)"
							+ "FROM Terminal as t "
							+ "WHERE t.active=true and t.affiliationId ="+affiliationId.uniqueResult();			
					
					String queryInactive = "SELECT COUNT(*)"
							+ "FROM Terminal as t "
							+ "WHERE t.active=false and t.affiliationId ="+affiliationId.uniqueResult();	
					
					String queryAll = "SELECT COUNT(*)"
							+ "FROM Terminal as t "
							+ "WHERE t.affiliationId ="+affiliationId.uniqueResult();
					
					terminalActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());
					
					terminalInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());
					
					allTerminalCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());
				}else{
					logger.debug("Comercio multi");
					String queryActive = "SELECT count(*) FROM  Terminal t "
							+ "WHERE t.active=true and t.affiliationId IN "
							+ "(SELECT a.id FROM Affiliation a WHERE a.merchantId IN "
							+ "(SELECT m.id FROM Merchant m WHERE m.parentMerchantId="+merchantId.uniqueResult()+"))";
					
					String queryInactive = "SELECT count(*) FROM  Terminal t "
							+ "WHERE t.active=false and t.affiliationId IN "
							+ "(SELECT a.id FROM Affiliation a WHERE a.merchantId IN "
							+ "(SELECT m.id FROM Merchant m WHERE m.parentMerchantId="+merchantId.uniqueResult()+"))";
					
					String queryAll = "SELECT count(*) FROM  Terminal t "
							+ "WHERE t.affiliationId IN "
							+ "(SELECT a.id FROM Affiliation a WHERE a.merchantId IN "
							+ "(SELECT m.id FROM Merchant m WHERE m.parentMerchantId="+merchantId.uniqueResult()+"))";
					
					terminalActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());

					terminalInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());
					
					allTerminalCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());
				}
				
			}
			
			
		} catch (NullPointerException e) {
			//Si los parameters son nulos imprime los datos globales de todos los terminales activos e inactivos
			logger.info("-- Imprimir datos de todos los terminales activos e inactivos --");
			String queryActive = "SELECT COUNT(*)"
					+ "FROM Terminal as t "
					+ "WHERE t.active=true";
			
			String queryInactive = "SELECT COUNT(*)"
					+ "FROM Terminal as t "
					+ "WHERE t.active=false";
			
			String queryAll = "SELECT COUNT(*)"
					+ "FROM Terminal as t ";
			
			terminalActiveCount = toIntExact((Long)session.createQuery(queryActive).uniqueResult());
			
			terminalInactiveCount = toIntExact((Long)session.createQuery(queryInactive).uniqueResult());

			allTerminalCount = toIntExact((Long)session.createQuery(queryAll).uniqueResult());
        } catch (Exception e) {
			logger.debug("exception {}", e);
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}

		logger.debug("Terminales Activos {}",terminalActiveCount);
		logger.debug("Terminales Inactivos {}",terminalInactiveCount);
		logger.debug("Terminales {}",allTerminalCount);
		
		dashboardTerminalList.setActive(terminalActiveCount);
		dashboardTerminalList.setInactive(terminalInactiveCount);
		dashboardTerminalList.setAllTerminal(allTerminalCount);
		
		logger.debug("dashboardTerminalList {}", dashboardTerminalList);
		return dashboardTerminalList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardTerminalList;
	}
	
	@SuppressWarnings("unchecked")
	@Path("/latestTicket")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getLatestTicket(@QueryParam("associateId") String associateId, 
			@QueryParam("rolUser") String rolUser){
		Session session = null;
		DashboardLatestTicketList dashboardLatestTicket = null;
		session = HibernateUtil.getSessionFactory().openSession();	
		//Generar lista con los pares de A�o-Mes
        List<String> latestMonth = new ArrayList<String>();
        List<DashboardLatestTicketMin> result = new ArrayList<DashboardLatestTicketMin>();
  
        for(int i=0; i<=5 ; i++){
        	Date ahora = new Date();
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM");
    		
            Calendar dateActual = Calendar.getInstance();
            dateActual.add(Calendar.MONTH, -i);
            String fecha = formateador.format(dateActual.getTime());
            Calendar dateStartMonth = dateActual;
            Calendar dateEndMonth = null;
            String month=null;
            int dayStart=0;
            
            month = Integer.toString(dateActual.get(Calendar.MONTH));
 
            logger.debug("MONTH: {}", month);
            dateStartMonth.add(Calendar.DATE, -dateStartMonth.get(Calendar.DAY_OF_MONTH)+1);
            logger.debug("dateStartMonth: {}", dateStartMonth.getTime());
            //month.add(Calendar.DATE, -month.get(Calendar.DAY_OF_MONTH)+1);
            dateEndMonth = dateStartMonth;
           
            if (month=="0" || month=="2" || month=="4" || month=="6" || month=="7" || month=="9" || month=="11"){
            	dateEndMonth.add(Calendar.DATE, 31);
            }else if (month=="1"){
            	dateEndMonth.add(Calendar.DATE, 28);
            }else{
            	dateEndMonth.add(Calendar.DATE, 30);
            }

            latestMonth.add(fecha);//agregar pares de anio-mes a la lista de los ultimos 6 meses
        }
        
         /* Filtro comercio para ticket promedio ultimos 6 meses*/

			try {
				if(!rolUser.equals(null) && (rolUser.equals("C") || rolUser.equals("c"))){
					Query merchantType = session.createQuery("SELECT merchantType FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'");
					Query merchantId = session.createQuery("SELECT id FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'"); 
					Query affiliationId = session.createQuery("SELECT id FROM Affiliation as a WHERE a.merchantId="+merchantId.uniqueResult());
					
					logger.debug("merchantId {}",merchantId.uniqueResult());
					logger.debug("affiliationId {}", affiliationId.uniqueResult());
					String tipo = "simple";
					try {
						if(tipo.equals(merchantType.uniqueResult())){
							//Recorrer la lista con los intervalos anio - mes
							for(String mes : latestMonth){
								logger.debug("periodo de tiempo {}", mes);
								//Query para consultar el promedio
							    String query = "SELECT new vpago.dataapi.classes.minified.DashboardLatestTicketMin((sum(t.amount) /count(*)))"
							    		+ "FROM Transaction t "
							    		+ "WHERE t.transactionTypeId "
							    		+ "IN (SELECT tp.id FROM TransactionType tp WHERE tp.reportValidation=true) "
							    		+ "AND t.id "
							    		+ "IN (SELECT tpacode.transaction FROM TransactionParameter tpacode WHERE tpacode.paramName='ans_code' and tpacode.paramValue='00' and tpacode.paramType='response')"
							    		+ "AND (TO_CHAR(t.txnDate, 'YYYY-MM')='"+mes+"' "
							    		+ "AND t.terminalId IN (SELECT id FROM Terminal as terminal WHERE terminal.affiliationId IN "
							    		+ "(SELECT id FROM Affiliation as a WHERE a.merchantId IN "
							    		+ "(SELECT id FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'))))";

								Query tickets =  session.createQuery(query);
								try {
									result.add((DashboardLatestTicketMin) tickets.uniqueResult());
								} catch (Exception e) {
									logger.debug("Error monto 0.0");

									DashboardLatestTicketMin dashboardLatestTicketMin2 = new DashboardLatestTicketMin();
									dashboardLatestTicketMin2.setDatePeriod(null);
									dashboardLatestTicketMin2.setAverage(0.0);
									result.add(dashboardLatestTicketMin2);
								}   				
								
								dashboardLatestTicket = new DashboardLatestTicketList(
							        	result
										);
							}
						}else{
							logger.debug("Comercio multi");
							for(String mes : latestMonth){
								String query = "SELECT new vpago.dataapi.classes.minified.DashboardLatestTicketMin((sum(t.amount) /count(*)))"
							    		+ "FROM Transaction t "
							    		+ "WHERE t.transactionTypeId "
							    		+ "IN (SELECT tp.id FROM TransactionType tp WHERE tp.reportValidation=true) "
							    		+ "AND t.id "
							    		+ "IN (SELECT tpacode.transaction FROM TransactionParameter tpacode WHERE tpacode.paramName='ans_code' and tpacode.paramValue='00' and tpacode.paramType='response')"
							    		+ "AND (TO_CHAR(t.txnDate, 'YYYY-MM')='"+mes+"' "
							    		+ "AND t.terminalId IN (SELECT id FROM Terminal as terminal WHERE terminal.affiliationId IN "
							    		+ "(SELECT id FROM Affiliation as a WHERE a.merchantId IN "
							    		+ "(SELECT id FROM Merchant as m WHERE m.parentMerchantId="+merchantId.uniqueResult()+"))))";
								
								Query tickets =  session.createQuery(query);
								try {
									result.add((DashboardLatestTicketMin) tickets.uniqueResult());
								} catch (Exception e) {
									logger.debug("Error monto 0.0");

									DashboardLatestTicketMin dashboardLatestTicketMin2 = new DashboardLatestTicketMin();
									dashboardLatestTicketMin2.setDatePeriod(null);
									dashboardLatestTicketMin2.setAverage(0.0);
									result.add(dashboardLatestTicketMin2);
								}   				
								
								dashboardLatestTicket = new DashboardLatestTicketList(
							        	result
										);
							}
						}
					
						int i=0;
						for(DashboardLatestTicketMin ticket : dashboardLatestTicket.getDashboardLatestTicket()){
							ticket.setDatePeriod(latestMonth.get(i));
							i = i + 1;
						}      	    			
						
					} catch (Exception e) {					
						logger.debug("exception {}", e.toString());
						return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
					} 
				   
				    logger.debug("dashboardLatestTicket {}", dashboardLatestTicket);
				}
			} catch (NullPointerException e) {
				for(String mes : latestMonth){
					logger.debug("mes {}", mes);
					//Query para consultar el promedio
					String query = "SELECT new vpago.dataapi.classes.minified.DashboardLatestTicketMin((sum(t.amount) /count(*)))"
				    		+ "FROM Transaction t where t.transactionTypeId IN (SELECT tp.id FROM TransactionType tp WHERE tp.name='CREDIT_SALE') "
				    		+ "AND t.id "
				    		+ "IN (SELECT tpacode.transaction FROM TransactionParameter tpacode WHERE tpacode.paramName='ans_code' and tpacode.paramValue='00' and tpacode.paramType='response') "
				    		+ "AND (TO_CHAR(t.txnDate, 'YYYY-MM')='"+mes+"')";
					Query tickets =  session.createQuery(query);
					
					try {
						result.add((DashboardLatestTicketMin) tickets.uniqueResult());
					} catch (Exception emonto) {
						logger.debug("Error monto 0.0");

						DashboardLatestTicketMin dashboardLatestTicketMin2 = new DashboardLatestTicketMin();
						dashboardLatestTicketMin2.setDatePeriod(null);
						dashboardLatestTicketMin2.setAverage(0.0);
						result.add(dashboardLatestTicketMin2);
					}   				
					
					dashboardLatestTicket = new DashboardLatestTicketList(
				        	result
							);
				}
				int i=0;
				for(DashboardLatestTicketMin ticket : dashboardLatestTicket.getDashboardLatestTicket()){
					ticket.setDatePeriod(latestMonth.get(i));
					i = i + 1;
				}  
			} finally {
				if(session != null) session.close();
			}
		
        
      
        return dashboardLatestTicket == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : dashboardLatestTicket;
	}

}
