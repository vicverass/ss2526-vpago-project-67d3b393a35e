package vpago.dataapi.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Affiliation;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.PaymentChannel;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.AffiliationList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Afiliacion (Affiliation)
 */
@Path("/affiliation")
public class AffiliationResource extends ResourceManager {
	
	/**
	 * Crear Afiliacion: Crea una relacion que asocia un Comercio (Merchant) con un Canal de Pago (PaymentChannel)
	 * 
	 * @param a Objeto de tipo <strong>Affiliation</strong> con los datos de la Afiliacion (Affiliation)
	 * 
	 * @return Datos de la entidad de tipo Afiliacion (Affiliation) que ha sido creada
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createAffiliation(Affiliation a) {
		Session session = null;
		Merchant merchant = null;
		PaymentChannel paymentChannel = null;
		Affiliation affiliation = null;
		try {
			// TODO Validar que el comercio no se encuentre afiliado
			session = HibernateUtil.getSessionFactory().openSession();
			merchant = a.getMerchantId() != null ? (Merchant)session.get(Merchant.class, a.getMerchantId()) : null;
			paymentChannel = a.getPaymentChannelId() != null ? (PaymentChannel)session.get(PaymentChannel.class, a.getPaymentChannelId()) : null;
			if((merchant != null) && (paymentChannel != null)) {
				affiliation = new Affiliation(merchant, paymentChannel);
				session.beginTransaction();
				session.save(affiliation);
				session.getTransaction().commit();
			}
			if(affiliation == null) return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", "No se pudo crear la afiliacion");
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if (merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Identificador no encontrado"); 
		else if (paymentChannel == null) return new Response(ResponseType.RESOURCENOTFOUND, "El canal de pago solicitado no existe", "Identificador no encontrado"); 
		else return affiliation;
	}
	
	/**
	 * Listar Afiliaciones de Comercio: Construye una lista con las entidades de tipo Afiliacion (Affiliation) asociadas a un Comercio (Merchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return La lista de todas las entidades de tipo Afiliacion (Affiliation) asociadas al Comercio (Merchant) identificado por <strong>merchantGuid</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("{merchant_guid: [a-fA-F\\d]{16}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMerchantAffiliation(@PathParam("merchant_guid") String merchantGuid) {
		Session session = null;
		AffiliationList affiliationList = null;
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if(merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Recurso no encontrado");
			logger.trace("{}", merchant);
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.AffiliationMin(id, affiliation, active) from Affiliation as affiliation where affiliation.merchantId = :merchantId order by affiliation.id";
			affiliationList = new AffiliationList(session.createQuery(hsqlquery).setInteger("merchantId", merchant.getId()).list());
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return affiliationList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna afiliacion asociada al comercio", "Recurso no encontrado") : affiliationList;
	}
	
}
