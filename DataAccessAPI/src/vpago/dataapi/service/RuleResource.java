package vpago.dataapi.service;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.Affiliation;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.Rule;
import vpago.dataapi.classes.RuleProduct;
import vpago.dataapi.classes.RuleProductGeneral;
import vpago.dataapi.classes.RuleProportion;
import vpago.dataapi.classes.RuleSet;
import vpago.dataapi.classes.wrappers.RuleList;
import vpago.dataapi.classes.wrappers.RuleSetList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados a los recursos Regla (Rule) y Conjunto de Reglas (RuleSet)
 */
@Path("/rule")
public class RuleResource extends ResourceManager {
	
	/**
	 * Listar Conjuntos de Reglas de Comercio: Construye una lista con todas las entidades de tipo Conjunto de Reglas (RuleSet) asociadas a un Comercio (Merchant) a traves de sus afiliaciones
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return La lista de todas las entidades de tipo Conjunto de Reglas (RuleSet) asociadas al Comercio (Merchant) identificado por <strong>merchantGuid</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("set/{merchant_guid: [a-fA-F\\d]{16}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getRuleSetsByMerchant(@PathParam("merchant_guid") String merchantGuid){
		Session session = null;
		RuleSetList ruleSetList = new RuleSetList();
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if(merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Recurso no encontrado");
			if(!merchant.getAffiliations().isEmpty()) {
				session = HibernateUtil.getSessionFactory().openSession();
				String hsqlquery = "select new vpago.dataapi.classes.minified.RuleSetMin(id, displayName, description, affiliationId) from RuleSet as ruleSet where ruleSet.affiliation in :affiliations ";
				ruleSetList = new RuleSetList(session.createQuery(hsqlquery)
				.setParameterList("affiliations", merchant.getAffiliations()).list());
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleSetList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun conjunto de reglas asociado al comercio", "Recurso no encontrado") : ruleSetList;
	}
	
	/**
	 * Consultar Conjunto de Reglas: Consultar los datos de una entidad de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @return Datos de la entidad de tipo Conjunto de Reglas (RuleSet) identificada por <strong>ruleSetId</strong>
	 */
	@Path("/set/{rule_set_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getRuleSet(@PathParam("rule_set_id") int ruleSetId) { 
		Session session = null;
		RuleSet ruleSet = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);	
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleSet == null ? new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado") : ruleSet;
	}
	
	/**
	 * Crear Conjunto de Reglas: Crea una entidad del tipo Conjunto de Reglas (RuleSet) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param rs Objeto de tipo <strong>RuleSet</strong> con los datos del Conjunto de Reglas (RuleSet)
	 * 
	 * @return Datos de la entidad de tipo Conjunto de Reglas (RuleSet) que ha sido creada
	 */
	@Path("/set")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createRuleSet(RuleSet rs) { 
		RuleSet ruleSet = null;
		Affiliation affiliation = null;
		Session session = null;
		logger.debug("RS {}",rs.getTimeFrame());
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			affiliation = (Affiliation)session.get(Affiliation.class, rs.getAffiliationId());
			if(affiliation != null) {
				ruleSet = new RuleSet(
						rs.getDisplayName(),
						rs.getDescription(),
						rs.getTimeFrame(),
						affiliation);
				session.beginTransaction();
				session.save(ruleSet).toString();
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(affiliation == null) return new Response(ResponseType.RESOURCENOTFOUND, "La afiliacion solicitada no existe", "Identificador no encontrado"); 
		else return ruleSet;
	}
	
	/**
	 * Modificar Conjunto de Reglas: Modifica un subconjunto de los datos de una entidad de tipo Conjunto de Reglas (RuleSet) 
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @param rs Objeto de tipo <strong>RuleSet</strong> con los datos del Conjunto de Reglas (RuleSet)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/set/{rule_set_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesRuleSet(@PathParam("rule_set_id") int ruleSetId, RuleSet rs) { 
		Session session = null;
		RuleSet ruleSet = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			
			if(ruleSet != null) {	
				if(!ruleSet.equals(rs)) ruleSet.update(rs);
				session.beginTransaction();
				session.update(ruleSet);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleSet == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El conjunto de reglas ha sido actualizado correctamente");
	}
	
	/**
	 * Asignar Conjunto de Reglas a Afiliacion de Comercio: Establece un Conjunto de Reglas (RuleSet) como el activo para una Afiliacion (Affiliation)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @param affiliationId Identificador del recurso de tipo Afiliacion (Affiliation)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/merchant/{merchant_guid: [a-fA-F\\d]{16} }/{affiliation_id: \\d+ }/set/{rule_set_id: \\d+ }")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse addRuleSetToAffiliation(@PathParam("merchant_guid") String merchantGuid, @PathParam("affiliation_id") int affiliationId, @PathParam("rule_set_id") int ruleSetId) { 
		Session session = null;
		RuleSet ruleSet = null;
		Merchant merchant = null;
		Affiliation affiliation = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			merchant = (Merchant)session.createQuery("from Merchant as merchant where merchant.merchantGuid = :merchantGuid").setString("merchantGuid", merchantGuid).uniqueResult();
			affiliation = (Affiliation)session.get(Affiliation.class, affiliationId);
			if((ruleSet != null) && (affiliation != null) && (merchant != null)) {
				if(affiliation.getMerchantId() != merchant.getId()) return new Response(ResponseType.INVALIDRESOURCE, "Identificadores no validos", "Los recursos no pertenecen al mismo comercio");
				affiliation.setActiveRuleSet(ruleSet);
				session.beginTransaction();
				session.update(affiliation);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(ruleSet == null) return new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado");
		if(merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Identificador no encontrado");
		if(affiliation == null) return new Response(ResponseType.RESOURCENOTFOUND, "La afiliacion solicitada no existe", "Identificador no encontrado");
		else return new Response(ResponseType.SUCCESS, "Solicitud exitosa","El conjunto de reglas ha sido asignado al comercio");
	}
	
	/**
	 * Eliminar Conjunto de Reglas: Elimina un Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/set/{rule_set_id: \\d+ }")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteRuleSet(@PathParam("rule_set_id") int ruleSetId) { 
		Session session = null;
		RuleSet ruleSet = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			if(ruleSet != null) {
				if(!ruleSet.getAffiliation().getActiveRuleSet().equals(ruleSet)) {
					session.beginTransaction();
					session.delete(ruleSet);
					session.getTransaction().commit();
				} else {
					return new Response(ResponseType.INVALIDRESOURCE, "El conjunto de reglas a eliminar se encuentra activo para una afiliacion", "Identificador invalido");
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleSet == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El conjunto de reglas ha sido eliminado correctamente");
	}
	
	/**
	 * Listar Reglas de Conjunto de Reglas: Construye una lista con todas las entidades de tipo Regla (Rule) asociadas una entidad de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @return La lista con todas las entidades de tipo Regla (Rule) asociadas una la entidad de tipo Conjunto de Reglas (RuleSet) identificada por <strong>ruleSetId</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("/set/{rule_set_id: \\d+ }/list")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getRuleSetRules(@PathParam("rule_set_id") int ruleSetId) { 
		Session session = null;
		RuleSet ruleSet = null;
		RuleList ruleList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			logger.debug("Rules: {}", ruleSet.getRules());
			if(ruleSet != null) {
				if(ruleSet.getRules().size() > 0) {
					String hsqlquery = "select new vpago.dataapi.classes.minified.RuleMin(id, displayName, acquirerId, ruleType) from Rule as rule where rule in :rules";
					ruleList = new RuleList(session.createQuery(hsqlquery).setParameterList("rules", ruleSet.getRules()).list());
					logger.debug("ruleList: {}", ruleList);
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleSet == null ? new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado") : ruleList;
	}
	
	/**
	 * Listar Reglas por Afiliacion: Construye una lista con todas las entidades de tipo Regla (Rule) asociadas a una Afiliacion (Affiliation)
	 * 
	 * @param affiliationId Identificador del recurso Afiliacion (Affiliation)
	 * 
	 * @return La lista con todas las entidades de tipo Regla (Rule) asociadas una la entidad de tipo Afiliacion (Affiliation) identificada por <strong>affiliationId</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("/{affiliation_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getRulesByAffiliation(@PathParam("affiliation_id") int affiliationId) { 
		Session session = null;
		RuleList ruleList = null;
		Affiliation affiliation = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			affiliation = (Affiliation)session.get(Affiliation.class, affiliationId);
			if(affiliation == null) return  new Response(ResponseType.RESOURCENOTFOUND, "No se encontro la afiliacion", "Recurso no encontrado");
			String hsqlquery = "select new vpago.dataapi.classes.minified.RuleMin(id, displayName, acquirerId, ruleType) from Rule as rule where rule.affiliation = :affiliationId";
			ruleList = new RuleList(session.createQuery(hsqlquery).setInteger("affiliationId", affiliationId).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna regla asociada a la afiliacion", "Recurso no encontrado") : ruleList;
	}
	
	/**
	 * Crear Regla: Crea una entidad del tipo Regla (Rule) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param ruleType Tipo de recurso Regla (Rule) a crear
	 * 
	 * @param r Objeto de tipo <strong>APIResponse</strong> con los datos de la Regla (Rule)
	 * 
	 * @return Datos de la entidad de tipo Regla (Rule) que ha sido creada
	 */
	@Path("/{rule_type: product|general|proportion }")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createRule(@PathParam("rule_type") String ruleType, APIResponse r) { 
		logger.trace("Regla recibida {}", r);
		Object ruleS = null;
		int product_id = 0, general_product_id = 0, proportion = 0;
		Class<?> ruleClass = null;
		Affiliation affiliation = null;
		Acquirer acquirer = null;
		Session session = null;
		int affiliationId = ((Rule)r).getAffiliationId();
		int acquirerId = ((Rule)r).getAcquirerId();
		String displayName = ((Rule)r).getDisplayName();
		String description = ((Rule)r).getDescription();
		switch(ruleType) {
			case "rule": ruleClass = Rule.class; break;
			case "product": ruleClass = RuleProduct.class; product_id = ((RuleProduct)r).getProductId(); break;
			case "general": ruleClass = RuleProductGeneral.class; general_product_id = ((RuleProductGeneral)r).getGeneralProductId(); break;
			case "proportion": ruleClass = RuleProportion.class; proportion = ((RuleProportion)r).getProportion(); break;
			default: ruleClass = Rule.class; break;
		}
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			affiliation = (Affiliation)session.get(Affiliation.class, affiliationId);
			if(affiliation != null) {
				String query = "from Acquirer as acquirer where acquirer.id = :acquirerId and acquirer.affiliationId = :affiliationId";
				List<?> result = session.createQuery(query)
						.setInteger("acquirerId", acquirerId)
						.setInteger("affiliationId", affiliationId)
						.list();
				acquirer = result.size() > 0 ? (Acquirer)result.get(0) : null;
			}
			if((affiliation != null) && (acquirer != null)) {
				switch(ruleType) {
					case "rule": ruleS = new Rule(displayName, description, acquirer, affiliation); break;
					case "product": ruleS = new RuleProduct(displayName, description, acquirer, affiliation, product_id); break;
					case "general": ruleS = new RuleProductGeneral(displayName, description, acquirer, affiliation, general_product_id); break;
					case "proportion": ruleS = new RuleProportion(displayName, description, acquirer, affiliation, proportion); break;
					default: ruleS = new Rule(displayName, description, acquirer, affiliation); break;
				}
				session.beginTransaction();
				session.save(ruleClass.cast(ruleS)).toString();
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(affiliation == null) return new Response(ResponseType.RESOURCENOTFOUND, "La afiliacion solicitada no existe", "Identificador no encontrado"); 
		if(acquirer == null) return new Response(ResponseType.RESOURCENOTFOUND, "La informacion de adquiriencia solicitada no existe o no esta asociada al comercio solicitado", "Identificador no encontrado");
		else return (APIResponse) ruleClass.cast(ruleS);
	}
	
	/**
	 * Consultar Regla: Consultar los datos de una entidad de tipo Regla (Rule)
	 * 
	 * @param ruleType Tipo de recurso Regla (Rule) a consultar
	 * 
	 * @param ruleId Identificador del recurso de tipo Reglas (Rule)
	 * 
	 * @return Datos de la entidad de tipo Reglas (Rule) identificada por <strong>ruleId</strong>
	 */
	@Path("/{rule_type: product|general|proportion }/{rule_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getRule(@PathParam("rule_type") String ruleType, @PathParam("rule_id") int ruleId) { 
		Session session = null;
		Class<?> ruleClass = null;
		Object rule = null;
		switch(ruleType) {
			case "rule": ruleClass = Rule.class; break;
			case "product": ruleClass = RuleProduct.class; break;
			case "general": ruleClass = RuleProductGeneral.class; break;
			case "proportion": ruleClass = RuleProportion.class; break;
			default: ruleClass = Rule.class; break;
		}
		try {
			rule = ruleClass.newInstance();
			session = HibernateUtil.getSessionFactory().openSession();
			rule = (ruleClass.cast(session.get(ruleClass, ruleId)));	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return rule == null ? new Response(ResponseType.RESOURCENOTFOUND, "La regla solicitada no existe", "Identificador no encontrado") : (APIResponse)ruleClass.cast(rule);
	}
	
	/**
	 * Modificar Regla: Permite cambiar un subconjunto de los datos de una Regla (Rule). Estos son: Nombre, descripcion y rango de tiempo
	 * 
	 * @param ruleType Tipo de recurso Regla (Rule) a modificar
	 * 
	 * @param ruleId Identificador del recurso de tipo Regla (Rule)
	 * 
	 * @param r Objeto de tipo <strong>APIResponse</strong> con los datos de la Regla (Rule)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{rule_type: product|general|proportion }/{rule_id: \\d+ }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesRule(@PathParam("rule_type") String ruleType, @PathParam("rule_id") int ruleId, APIResponse r) { 
		Object ruleS = null;
		Class<?> ruleClass = null;
		Session session = null;
		switch(ruleType) {
			case "rule": ruleClass = Rule.class; break;
			case "product": ruleClass = RuleProduct.class; break;
			case "general": ruleClass = RuleProductGeneral.class; break;
			case "proportion": ruleClass = RuleProportion.class; break;
			default: ruleClass = Rule.class; break;
		}
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleS = (ruleClass.cast(session.get(ruleClass, ruleId)));	
			if(ruleS != null) {
				switch(ruleType) {
					case "rule": ((Rule)ruleS).update((Rule)r); break;
					case "product":((RuleProduct)ruleS).update((RuleProduct)r); break;
					case "general": ((RuleProductGeneral)ruleS).update((RuleProductGeneral)r); break;
					case "proportion": ((RuleProportion)ruleS).update((RuleProportion)r); break;
					default: ((Rule)ruleS).update((Rule)r); break;
				}
				session.beginTransaction();
				session.update(ruleClass.cast(ruleS));
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return ruleS == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "La regla solicitada no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "La regla ha sido actualizada correctamente");
	}
	
	/**
	 * Eliminar Regla: Elimina una Regla (Rule)
	 * 
	 * @param ruleType Tipo de recurso Regla (Rule) a eliminar
	 * 
	 * @param ruleId Identificador del recurso de tipo Regla (Rule)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{rule_type: product|general|proportion }/{rule_id: \\d+ }")
	@DELETE
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse deleteRule(@PathParam("rule_type") String ruleType, @PathParam("rule_id") int ruleId) { 
		Session session = null;
		Class<?> ruleClass = null;
		Object rule = null;
		boolean inSet = false;
		switch(ruleType) {
			case "rule": ruleClass = Rule.class; break;
			case "product": ruleClass = RuleProduct.class; break;
			case "general": ruleClass = RuleProductGeneral.class; break;
			case "proportion": ruleClass = RuleProportion.class; break;
			default: ruleClass = Rule.class; break;
		}
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			rule = (ruleClass.cast(session.get(ruleClass, ruleId)));
			if(rule != null) inSet = !((Rule)rule).getRuleSets().isEmpty();
			if((rule != null) && (!inSet)) {
				session.beginTransaction();
				session.delete(rule);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(inSet) return new Response(ResponseType.INVALIDRESOURCE, "La regla no puede ser eliminada", "La regla solicitada se encuentra asignada a uno o mas conjuntos de reglas");
		return rule == null ? 
				new Response(ResponseType.RESOURCENOTFOUND, "La regla solicitada no existe", "Identificador no encontrado") 
				: new Response(ResponseType.SUCCESS, "Solicitud exitosa", "La regla ha sido eliminada correctamente");
	}

	/**
	 * Agregar Regla a Conjunto de Reglas: Asocia una entidad de tipo Regla (Rule) a un Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleId Identificador del recurso de tipo Regla (Rule)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/set/{rule_set_id: \\d+ }/add/{rule_id: \\d+ }")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse addRule(@PathParam("rule_set_id") int ruleSetId, @PathParam("rule_id") int ruleId) { 
		Session session = null;
		RuleSet ruleSet = null;
		Rule rule = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			rule = (Rule)session.get(Rule.class, ruleId);
			if((ruleSet != null) && (rule != null)) {
				if(ruleSet.getAffiliationId() != rule.getAffiliationId()) return new Response(ResponseType.INVALIDRESOURCE, "Identificadores no validos", "Los recursos no pertenecen a la misma afiliacion");
				if(ruleSet.hasRule(rule)) return new Response(ResponseType.INVALIDRESOURCE, "Regla existente", "La regla ya se encuentra asociada al conjunto de reglas");
				ruleSet.addRule(rule);
				session.beginTransaction();
				session.update(ruleSet);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(ruleSet == null) return new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado");
		if(rule == null) return new Response(ResponseType.RESOURCENOTFOUND, "La regla solicitada no existe", "Identificador no encontrado");
		else return new Response(ResponseType.SUCCESS, "Solicitud exitosa","La regla ha sido asignada al conjunto de reglas");
	}
	
	/**
	 * Remover Regla de Conjunto de Reglas: Elimina la asociacion entre una entidad de tipo Regla (Rule) y un Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleSetId Identificador del recurso de tipo Conjunto de Reglas (RuleSet)
	 * 
	 * @param ruleId Identificador del recurso de tipo Regla (Rule)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/set/{rule_set_id: \\d+ }/remove/{rule_id: \\d+ }")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse removeRule(@PathParam("rule_set_id") int ruleSetId, @PathParam("rule_id") int ruleId) { 
		Session session = null;
		RuleSet ruleSet = null;
		Rule rule = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			ruleSet = (RuleSet)session.get(RuleSet.class, ruleSetId);
			rule = (Rule)session.get(Rule.class, ruleId);
			if((ruleSet != null) && (rule != null)) {
				if(ruleSet.getAffiliationId() != rule.getAffiliationId()) return new Response(ResponseType.INVALIDRESOURCE, "Identificadores no validos", "Los recursos no pertenecen al mismo comercio");
				if(!ruleSet.hasRule(rule)) return new Response(ResponseType.INVALIDRESOURCE, "Regla inexistente", "La regla no se encuentra asociada al conjunto de reglas");
				ruleSet.deleteRule(rule);
				session.beginTransaction();
				session.update(ruleSet);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(ruleSet == null) return new Response(ResponseType.RESOURCENOTFOUND, "El conjunto de reglas solicitado no existe", "Identificador no encontrado");
		if(rule == null) return new Response(ResponseType.RESOURCENOTFOUND, "La regla solicitada no existe", "Identificador no encontrado");
		else return new Response(ResponseType.SUCCESS, "Solicitud exitosa","La regla ha sido removida del conjunto de reglas");
	}	
}
