package vpago.dataapi.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.sun.istack.internal.Nullable;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.BatchHistory;
import vpago.dataapi.classes.City;
import vpago.dataapi.classes.Corporation;
import vpago.dataapi.classes.Country;
import vpago.dataapi.classes.IndustryType;
import vpago.dataapi.classes.Location;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Multimerchant;
import vpago.dataapi.classes.Municipality;
import vpago.dataapi.classes.Parish;
import vpago.dataapi.classes.PostalZone;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.State;
import vpago.dataapi.classes.forms.BatchHistorySynchronized;
import vpago.dataapi.classes.minified.BatchConsultMin;
import vpago.dataapi.classes.minified.BatchHistoryConsultSynchronizedMin;
import vpago.dataapi.classes.minified.BatchHistorySynchronizedMin;
import vpago.dataapi.classes.SimpleMerchant;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.ResourceQuery;
import vpago.dataapi.classes.wrappers.BatchHistoryConsultSynchronizedList;
import vpago.dataapi.classes.wrappers.BatchHistorySynchronizedList;
import vpago.dataapi.classes.wrappers.MerchantList;
import vpago.dataapi.classes.wrappers.TransactionParametersList;
import vpago.dataapi.config.EngineProperties;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Comercio (Merchant)
 */
@Path("/merchant")
public class MerchantResource extends ResourceManager {
	
	/**
	 * Listar todos los Comercios: Construye una lista con todas las entidades de tipo Comercio (Merchant) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Comercio (Merchant)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllMerchant(
			@QueryParam("acquirerBank") Integer acquirerBank
			){
		logger.info("Solicitud de todos los comercios");
		Session session = null;
		MerchantList merchantList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			String merchantAcqQuery = 
					"select distinct new vpago.dataapi.classes.minified.MerchantMin("
					+ "merchant.id, merchant.merchantGuid, merchant.name, merchant.rif, "
					+ "merchant.fantasyName, merchant.active, merchant.merchantType) "
					+ "from Merchant as merchant "
					+ ((acquirerBank != null) ? 
					  "inner join merchant.affiliations as affiliation "
					+ "inner join affiliation.acquirers as acquirer "
					+ "where acquirer.bankId = :acquirerBank " : "")
					+ "order by merchant.active desc, merchant.name asc"
					;

			logger.debug("Query: {}", merchantAcqQuery);
			
			Query merchantQuery = session.createQuery(merchantAcqQuery);
			if(acquirerBank != null) merchantQuery.setInteger("acquirerBank", acquirerBank);
			merchantList = new MerchantList(
					merchantQuery
					.list());
			merchantList.getMerchant().forEach(merchant->{
				logger.trace("Comercio asociado a {}: {}", acquirerBank, merchant);
			});
			
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return merchantList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun comercio", "Recurso no encontrado") : merchantList;
	}
	
	/**
	 * Listar Comercios de Multicomercio: Construye una lista con todas las entidades de tipo Comercio (Merchant) asociadas a un recurso de tipo Multicomercio (Multimerchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Multicomercio (Multimerchant)
	 *
	 * @return La lista de todas las entidades de tipo Comercio (Merchant) asociadas al Multicomercio (Multimerchant) identificado por <strong>merchantGuid</strong>		
	 */
	@SuppressWarnings("unchecked")
	@Path("/{merchant_guid: [a-fA-F\\d]{16}}/merchants")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMultimerchantMerchants(@PathParam("merchant_guid") String merchantGuid){
		Session session = null;
		MerchantList merchantList = null;
		Multimerchant parentMerchant = ResourceQuery.searchMultimerchant(merchantGuid);
		if(parentMerchant != null) {
			try {
				session = HibernateUtil.getSessionFactory().openSession();
				String hsqlquery = "select new vpago.dataapi.classes.minified.MerchantMin(id, merchantGuid, name, rif, fantasyName, active, merchantType) from Merchant as merchant where merchant.parentMerchantId=:parentMerchantId";
				merchantList = new MerchantList(session.createQuery(hsqlquery).setInteger("parentMerchantId", parentMerchant.getId()).list());
			} catch (Exception e) {
				logger.error(e.toString());
				return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
			} finally {
				if(session != null) session.close();
			}
		} else {
			return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el multicomercio solicitado", "Recurso no encontrado");
		}
		return merchantList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun comercio", "Recurso no encontrado") : merchantList;
	}
	
	
	/**
	 * Consultar Comercio: Consultar los datos de una entidad de tipo Comercio (Merchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Multicomercio (Multimerchant)
	 * 
	 * @return Datos de la entidad de tipo Comercio (Merchant) identificada por <strong>merchantGuid</strong>
	 */
	@Path("/{merchant_guid: [a-fA-F\\d]{16}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMerchant(@PathParam("merchant_guid") String merchantGuid){
		try {
			Merchant merchant = ResourceQuery.searchMerchant(merchantGuid);
			logger.debug(merchant.toString());
			merchant.setIndustryTypeName(merchant.getIndustryType() != null ? merchant.getIndustryType().getName() : "No asociado");
			merchant.setSectorName(merchant.getIndustryType() != null ? merchant.getIndustryType().getSector().getName() : "No asociado");
			merchant.setIndustryType(null);
			return merchant == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el multicomercio solicitado", "Recurso no encontrado") : merchant;
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} 
	}
	
	/**
	 * Afiliar Comercio: Crea una entidad del tipo Comercio (Merchant) o Multicomercio (Multimerchant) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param merchantType Parametro extraido de la URL que indica si el recurso de tipo Comercio es simple (Merchant) o multicomercio (Multimerchant) para realizar la conversion de tipos correspondiente
	 * 
	 * @param m Objeto de tipo <strong>APIResponse</strong> con los datos del Comercio (Merchant)
	 * 
	 * @return Datos de la entidad de tipo Comercio (Merchant) o Multicomercio (Multimerchant) que ha sido creada
	 */
	@Path("/{merchant_type: simple|multi }")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createMerchant(@PathParam("merchant_type") String merchantType, APIResponse m){
		Object merchantO = null;
		Class<?> merchantClass = null;
		Session session = null;
		String name = ((Merchant)m).getName();
		String rif = ((Merchant)m).getRif();
		Location location = ((Merchant)m).getLocation();
		String fantasyName = ((Merchant)m).getFantasyName();
		int industryTypeId = ((Merchant)m).getIndustryTypeId();
		Integer parentMerchantId = ((Merchant)m).getParentMerchantId();
		Integer corporationId = ((Merchant)m).getCorporationId();
		int postalZoneId = ((Merchant)m).getLocation().getAddress().getPostalZoneId();
		int parishId = ((Merchant)m).getLocation().getAddress().getParishId();
		int muncipalityId = ((Merchant)m).getLocation().getAddress().getMunicipalityId();
		int cityId = ((Merchant)m).getLocation().getAddress().getCityId();
		int stateId = ((Merchant)m).getLocation().getAddress().getStateId();
		int countryId = ((Merchant)m).getLocation().getAddress().getCountryId();
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			IndustryType industryType = (IndustryType)session.get(IndustryType.class, industryTypeId);
			location.getAddress().setPostalZone((PostalZone)session.get(PostalZone.class, postalZoneId));
			location.getAddress().setParish((Parish)session.get(Parish.class, parishId));
			location.getAddress().setMunicipality((Municipality)session.get(Municipality.class, muncipalityId));
			location.getAddress().setCity((City)session.get(City.class, cityId));
			location.getAddress().setState((State)session.get(State.class, stateId));
			location.getAddress().setCountry((Country)session.get(Country.class, countryId));
			Multimerchant parentMerchant = parentMerchantId != null ? (Multimerchant)session.get(Multimerchant.class, parentMerchantId) : null;
			Corporation corporation = null;
			if(parentMerchant != null) corporation = parentMerchant.getCorporation() != null ? parentMerchant.getCorporation() : null;
			else corporation = corporationId != null ? (Corporation)session.get(Corporation.class, corporationId) : null;
			switch(merchantType) {
			case "simple": 
				merchantO = new Merchant(name, rif, location, fantasyName, false, industryType, parentMerchant, corporation, Calendar.getInstance());
				merchantClass = Merchant.class; 
				break;
			case "multi":
				merchantO = new Multimerchant(name, rif, location, fantasyName, false, industryType, corporation, Calendar.getInstance());
				merchantClass = Multimerchant.class; 
				break;
			default:
				merchantO = new Merchant(name, rif, location, fantasyName, false, industryType, parentMerchant, corporation, Calendar.getInstance());
				merchantClass = Merchant.class; 
				break;
			}
			logger.debug("Comercio a guardar {}", merchantO);
			session.beginTransaction();
			session.save(merchantClass.cast(merchantO));
			((Merchant)merchantO).generateMerchantGuid();
			session.update(merchantO);
			session.getTransaction().commit();
			
			//Enviar comercio al vadmin
			// TODO Cargar solo al inicio del aplicativo
			EngineProperties.loadProperties();
			if(EngineProperties.getSendVadmin()) {
				Merchant merchantAux = (Merchant)merchantO;
				try {
					logger.debug("Enviar comercio nuevo al vadmin {}", (Merchant)merchantO);
					logger.debug("Construyendo solicitud a enviar al servicio web");
					SimpleMerchant simpleMerchant = new SimpleMerchant(
							merchantAux.getMerchantGuid(), 
							merchantAux.getMerchantType(), 
							merchantAux.getCorporation() != null ? merchantAux.getCorporation().getName() : null, 
							merchantAux.getParentMerchantId(), 
							merchantAux.getIndustryType().getName(), 
							merchantAux.getRif(), 
							merchantAux.getName(), 
							merchantAux.getFantasyName(), 
							merchantAux.isActive(), 
							merchantAux.getLocation().getAddress().getCountry().getCountryName(), 
							merchantAux.getLocation().getAddress().getCity().getCityName(), 
							merchantAux.getLocation().getAddress().getState().getStateName(), 
							merchantAux.getLocation().getAddress().getLocation() + " " + merchantAux.getLocation().getAddress().getProperty());
					logger.debug("Estableciendo conexion con vadmin");
					String vadminTarget = "http://" + EngineProperties.getVadminIp() + ":" + EngineProperties.getVadminPort() + "/vadmin/merchant/add/" + merchantAux.getMerchantGuid();
					logger.debug("vadminTarget: {}", vadminTarget);
					Client gatewayClient = ClientBuilder.newClient();
					WebTarget gatewayWebTarget = gatewayClient.target(vadminTarget);
					Invocation.Builder gInvocationBuilder = gatewayWebTarget.request().accept(MediaType.APPLICATION_JSON);
					logger.debug("Enviando solicitud mediante servicio web");
					javax.ws.rs.core.Response gResponse = gInvocationBuilder.post(Entity.entity(simpleMerchant, MediaType.APPLICATION_JSON));
					logger.debug("Recibiendo respuesta del servicio web");
					SimpleMerchant simpleMerchantAns = gResponse.readEntity(SimpleMerchant.class);
					logger.debug("Respuesta: {}", simpleMerchantAns);
				} catch (Exception e) {
					logger.error("No se pudo enviar el comercio {} al vadmin", merchantAux.getMerchantGuid());
				}
			}
			
			((Merchant)merchantO).setIndustryType(null);
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		}  finally {
			if(session != null) session.close();
		}
		return (APIResponse)merchantClass.cast(merchantO);
	}
	
	/**
	 * Modificar datos de Comercio: Permite cambiar un subconjunto de los datos del Comercio (Merchant/Multimerchant). Estos son: Nombre fantasía, tipo de sector industrial al que pertenece y datos de dirección
	 * 
	 * @param merchantType Parametro extraido de la URL que indica si el recurso de tipo Comercio es simple (Merchant) o multicomercio (Multimerchant) para realizar la conversion de tipos correspondiente
	 * 
	 * @param m Objeto de tipo <strong>APIResponse</strong> con los datos del Comercio (Merchant/Multimerchant)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{merchant_type: simple|multi }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesMerchant(@PathParam("merchant_type") String merchantType, APIResponse m){
		Object merchant = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			merchant = merchantType.equals("simple") 
					? ResourceQuery.searchMerchant(((Merchant)m).getMerchantGuid())
					: ResourceQuery.searchMultimerchant(((Multimerchant)m).getMerchantGuid()) ;
			int postalZoneId = ((Merchant)m).getLocation().getAddress().getPostalZoneId();
			int parishId = ((Merchant)m).getLocation().getAddress().getParishId();
			int muncipalityId = ((Merchant)m).getLocation().getAddress().getMunicipalityId();
			int cityId = ((Merchant)m).getLocation().getAddress().getCityId();
			int stateId = ((Merchant)m).getLocation().getAddress().getStateId();
			int countryId = ((Merchant)m).getLocation().getAddress().getCountryId();
			logger.trace("m: {}", m);
			if(merchant != null) {
				((Merchant)m).getLocation().getAddress().setPostalZone((PostalZone)session.get(PostalZone.class, postalZoneId));
				((Merchant)m).getLocation().getAddress().setParish((Parish)session.get(Parish.class, parishId));
				((Merchant)m).getLocation().getAddress().setMunicipality((Municipality)session.get(Municipality.class, muncipalityId));
				((Merchant)m).getLocation().getAddress().setCity((City)session.get(City.class, cityId));
				((Merchant)m).getLocation().getAddress().setState((State)session.get(State.class, stateId));
				((Merchant)m).getLocation().getAddress().setCountry((Country)session.get(Country.class, countryId));
				((Merchant)merchant).update((Merchant)m);
				session.beginTransaction();
				session.update(merchant);
				session.getTransaction().commit();
				
				//Enviar comercio al vadmin
				// TODO Cargar solo al inicio del aplicativo
				EngineProperties.loadProperties();
				if(EngineProperties.getSendVadmin()) {
					Merchant merchantAux = (Merchant)merchant;
					try {	
						logger.debug("Enviar comercio actualizado al vadmin {}", (Merchant)merchant);
						logger.debug("Construyendo solicitud a enviar al servicio web");
						SimpleMerchant simpleMerchant = new SimpleMerchant(
								merchantAux.getMerchantGuid(), 
								merchantAux.getMerchantType(), 
								merchantAux.getCorporation() != null ? merchantAux.getCorporation().getName() : null, 
								merchantAux.getParentMerchantId(), 
								merchantAux.getIndustryType().getName(), 
								merchantAux.getRif(), 
								merchantAux.getName(), 
								merchantAux.getFantasyName(), 
								merchantAux.isActive(), 
								merchantAux.getLocation().getAddress().getCountry().getCountryName(), 
								merchantAux.getLocation().getAddress().getCity().getCityName(), 
								merchantAux.getLocation().getAddress().getState().getStateName(), 
								merchantAux.getLocation().getAddress().getLocation() + " " + merchantAux.getLocation().getAddress().getProperty());
						logger.debug("Estableciendo conexion con vadmin");
						String vadminTarget = "http://" + EngineProperties.getVadminIp() + ":" + EngineProperties.getVadminPort() + "/vadmin/merchant/add/" + merchantAux.getMerchantGuid();
						logger.debug("vadminTarget: {}", vadminTarget);
						Client gatewayClient = ClientBuilder.newClient();
						WebTarget gatewayWebTarget = gatewayClient.target(vadminTarget);
						Invocation.Builder gInvocationBuilder = gatewayWebTarget.request().accept(MediaType.APPLICATION_JSON);
						logger.debug("Enviando solicitud mediante servicio web");
						javax.ws.rs.core.Response gResponse = gInvocationBuilder.post(Entity.entity(simpleMerchant, MediaType.APPLICATION_JSON));
						logger.debug("Recibiendo respuesta del servicio web");
						SimpleMerchant simpleMerchantAns = gResponse.readEntity(SimpleMerchant.class);
						logger.debug("Respuesta: {}", simpleMerchantAns);
					
					} catch (Exception e) {
						logger.error("No se pudo actualizar el comercio {} en el vadmin", merchantAux.getMerchantGuid());
					}
				
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if (merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado");
		return new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El comercio ha sido actualizado correctamente");
	}
		
	/**
	 * Consultar Comercio por Terminal: Obtener informacion basica del Comercio (Merchant) dado el identificador de Terminal (Terminal)
	 * 
	 * @param terminalGuid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Datos de la entidad de tipo Comercio (Merchant) a la cual esta asociado el Terminal (Terminal) identificado por <strong>terminalGuid</strong>
	 */
	@Path("/terminal/{terminal_guid: [a-fA-F\\d]{8}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getMerchantByTerminal(@PathParam("terminal_guid") String terminalGuid){
		try {
			Merchant merchant = null;
			merchant = ResourceQuery.searchMerchantByTerminal(terminalGuid);
			if(merchant != null) {
				merchant.setIndustryType(null);
				return merchant;
			} else {
				return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado");
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} 
	}
	
	/**
	 * Habilitar Comercio: Habilita la entidad de tipo Comericio (Merchant) para la ejecucion de transacciones
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/enable/{merchant_guid: [a-fA-F\\d]{16}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse enableMerchant(@PathParam("merchant_guid") String merchantGuid) {
		Session session = null;
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if (merchant != null) {
				if (merchant.getMerchantType().equals("simple")) {
					session = HibernateUtil.getSessionFactory().openSession();
				    boolean hasAffiliations = false, hasTerminals = false;
				    hasAffiliations = merchant.getAffiliations() != null ? (merchant.getAffiliations().size() > 0 ? true : false) : false;
				    hasTerminals = hasAffiliations ? 
				    		((long)session.createQuery("select count(terminal) from Terminal as terminal where terminal.affiliation in :affiliations")
				    				.setParameterList("affiliations", merchant.getAffiliations()).iterate().next() > 0 ? true : false)
				    		: false;
				    logger.trace("hasAfiliations: {} - hasTerminals: {}", hasAffiliations, hasTerminals);
				    if (hasAffiliations && hasTerminals) merchant.setActive(true);
				    session.beginTransaction();
				    session.update(merchant);
				    session.getTransaction().commit();
				} else {
					return new Response(ResponseType.GENERALERROR, "No se pudo activar el comercio solicitado", "Comercio no activado");
				}
			} else {
				return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado");
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return merchant.isActive() ? new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Comercio activado exitosamente") 
				: new Response(ResponseType.GENERALERROR, "No se pudo activar el comercio solicitado", "Comercio no activado");
	}
	
	/**
	 * Deshabilitar Comercio: Deshabilita la entidad de tipo Comericio (Merchant) para la ejecucion de transacciones
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/disable/{merchant_guid: [a-fA-F\\d]{16}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse disableMerchant(@PathParam("merchant_guid") String merchantGuid) {
		Session session = null;
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if (merchant != null) {
				if (merchant.getMerchantType().equals("simple")) {
					session = HibernateUtil.getSessionFactory().openSession();
					merchant.setActive(false);
				    session.beginTransaction();
				    session.update(merchant);
				    session.getTransaction().commit();
				} else {
					return new Response(ResponseType.GENERALERROR, "No se pudo desactivar el comercio solicitado", "Comercio no desactivado");
				}
			} else {
				return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado");
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return !merchant.isActive() ? new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Comercio desactivado exitosamente") 
				: new Response(ResponseType.GENERALERROR, "No se pudo desactivar el comercio solicitado", "Comercio no desactivado");
	}
	
	/**
	 * Asociar Comercio a Multicomercio: Relaciona una entidad de tipo Comercio (Merchant) con una entidad de tipo Multicomercio (Multimerchant)
	 * 
	 * @param multimerchantGuid Identificador del recurso de tipo/Multicomercio (Multimerchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return Mensaje con el resultado de la funcion
	 */
	@Path("/{multimerchant_guid: [a-fA-F\\d]{16}}/add/{merchant_guid: [a-fA-F\\d]{16}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse addMerchantToMultimerchant(@PathParam("multimerchant_guid") String multimerchantGuid, @PathParam("merchant_guid") String merchantGuid){
		Session session = null;
		Merchant merchant = null;
		Multimerchant multimerchant;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			multimerchant = ResourceQuery.searchMultimerchant(multimerchantGuid);
			if ((merchant != null) && (multimerchant != null)) {
				if ((merchant.getMerchantType().equals("simple")) && (multimerchant.getMerchantType().equals("multi"))) {
					session = HibernateUtil.getSessionFactory().openSession();
					merchant.setParentMerchant(multimerchant);
					session.beginTransaction();
					session.update(merchant);
					session.getTransaction().commit();
				} else {
					return new Response(ResponseType.GENERALERROR, "No se pudo asociar el comercio solicitado", "Comercio no asociado");
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return (merchant != null) && (multimerchant != null) ? new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Comercio asociado a Multicomercio")
				: (merchant == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado") 
						: new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el multicomercio solicitado", "Recurso no encontrado"));
	}
	
	/**
	 * Listar todos los Multicomercios: Construye una lista con todas las entidades de tipo Multicomercio (Multimerchant) almacenadas
	 * 
	 * @return La lista de todas las entidades de tipo Multicomercio (Multimerchant)
	 */
	@SuppressWarnings("unchecked")
	@Path("/multim")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllMultimerchant(
			@QueryParam("acquirerBank") Integer acquirerBank
			){
		logger.trace("Solicitud de todos los multicomercios");
		Session session = null;
		MerchantList merchantList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			String merchantAcqQuery = 
					"select distinct new vpago.dataapi.classes.minified.MerchantMin("
					+ "multimerchant.id, multimerchant.merchantGuid, multimerchant.name, multimerchant.rif, multimerchant.fantasyName, multimerchant.active, multimerchant.merchantType) "
					+ "from Multimerchant as multimerchant "
					+ ((acquirerBank != null) ? 
					  "inner join multimerchant.merchants as merchant "
					+ "inner join merchant.affiliations as affiliation "
					+ "inner join affiliation.acquirers as acquirer "
					+ "where acquirer.bankId = :acquirerBank " : "")
					+ "order by multimerchant.name asc"
					;
			
			logger.debug("Query: {}", merchantAcqQuery);
			
			Query merchantQuery = session.createQuery(merchantAcqQuery);
			if(acquirerBank != null) merchantQuery.setInteger("acquirerBank", acquirerBank);
			merchantList = new MerchantList(
					merchantQuery
					.list());
			merchantList.getMerchant().forEach(multimerchant->{
				logger.debug("Multicomercio asociado a {}: {}", acquirerBank, multimerchant);
			});
			
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return merchantList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun multicomercio", "Recurso no encontrado") : merchantList;
	}
	
	@Path("/simplemerchant")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public SimpleMerchant getTestSimpleMerchant(){
		logger.debug("Construyendo solicitud a enviar al servicio web");
		SimpleMerchant simpleMerchant = new SimpleMerchant("A2E76E123B556", "SERVICIO", "SS2526", 0, "OUTSORCING", "J122399182", "COMER ARCA", "ARCA 2014", true, "VZLA", "CARACAS", "DTTO CAPITAL", "LA CALIF NORTE");
		logger.debug("Estableciendo conexion con vadmin");
		Client gatewayClient = ClientBuilder.newClient();
		WebTarget gatewayWebTarget = gatewayClient.target("http://localhost:8081/vadmin/merchant/add/" + "A2E76E123B556");
		Invocation.Builder gInvocationBuilder = gatewayWebTarget.request().accept(MediaType.APPLICATION_JSON);
		logger.debug("Enviando solicitud mediante servicio web");
		javax.ws.rs.core.Response gResponse = gInvocationBuilder.post(Entity.entity(simpleMerchant, MediaType.APPLICATION_JSON));
		logger.debug("Recibiendo respuesta del servicio web");
		SimpleMerchant simpleMerchantAns = gResponse.readEntity(SimpleMerchant.class);
		logger.debug("Respuesta: {}", simpleMerchantAns);
		return simpleMerchantAns;
	}
	
	@Path("updateData/{merchant_type: simple|multi }")
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse updatesDataMerchant(@PathParam("merchant_type") String merchantType, APIResponse m){
		Object merchant = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			merchant = merchantType.equals("simple") 
					? ResourceQuery.searchMerchant(((Merchant)m).getMerchantGuid())
					: ResourceQuery.searchMultimerchant(((Multimerchant)m).getMerchantGuid()) ;
			
			if(merchant != null) {				
				((Merchant)merchant).updateData((Merchant)m);
				logger.debug("---- Comercio a actualizar ---- {}",m);
				session.beginTransaction();
				session.update(merchant);
				session.getTransaction().commit();				
				
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if (merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el comercio solicitado", "Recurso no encontrado");
		return new Response(ResponseType.SUCCESS, "Solicitud exitosa", "El comercio ha sido actualizado correctamente");
	}

	@Path("/batchHistorySynchronized")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllPaymentMerchant(
			@QueryParam("date") String date
			){
		logger.info("Solicitud de todos los comercios a domiciliar");
		Session session = null;
		BatchHistorySynchronizedList list = null;
		List<BatchHistorySynchronizedMin> result = new ArrayList<BatchHistorySynchronizedMin>();
		List<BatchHistory> batchHistoryUpdate = new ArrayList<BatchHistory>();
				
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String 	sql = 
					"SELECT bh.id, m.merchant_guid, ac.bank_id, bh.total_amount, tp.PARAMETER_VALUE, bh.batch_number "
					+ "FROM batch_history bh "
					+ "inner join terminal t on t.id=bh.terminal_id "
					+ "inner join affiliation a on a.id=t.affiliation_id "
					+ "inner join acquirer ac on a.id=ac.affiliation_id "
					+ "inner join merchant m on m.id=a.merchant_id "
					+ "inner join terminal_parameter tp on tp.terminal_id=t.id "
					+ "WHERE TO_CHAR(bh.CLOSING_TIMESTAMP, 'YYYY-MM-DD') = '"+date+"' AND bh.SYNCHRONIZED = 0 "
					+ "GROUP BY bh.id, ac.bank_id, m.merchant_guid, bh.total_amount, tp.parameter_value, bh.batch_number "
					+ "ORDER BY ac.bank_id, bh.id"
					;

			logger.debug("Query: {}", sql);
			
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			
			logger.info("Data {}", data);
			data.forEach(elem->{ 
				BatchHistorySynchronizedMin batchHistory = new BatchHistorySynchronizedMin();
				batchHistory.setMerchantGuid((String) elem.get("MERCHANT_GUID"));
				batchHistory.setBankId((Integer) elem.get("BANK_ID"));
				batchHistory.setTotalAmount((double) elem.get("TOTAL_AMOUNT"));
				batchHistory.setTerminalBank((String) elem.get("PARAMETER_VALUE"));
				batchHistory.setBatchNumber((Integer) elem.get("BATCH_NUMBER"));
				
				result.add(batchHistory);
			});
		
			list = new BatchHistorySynchronizedList(
					result
					);
			
			logger.debug("List {}", list.getBatchHistorySynchronized());

			batchHistoryUpdate = ResourceQuery.searchBatchHistory(date);
			for(BatchHistory batchHistory : batchHistoryUpdate){
				batchHistory.setBatchHistSynchronized(true);
				batchHistory.setSynchronizedDate(Calendar.getInstance());
				session.beginTransaction();
				session.update(batchHistory);
				session.getTransaction().commit();
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		logger.debug("Antes de retornar");
		return list == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun comercio", "Recurso no encontrado") : list;
		//return null;
	}
	
	@Path("/allBatchHistoryMerchants")
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllBatchMerchants(
			@QueryParam("date") String date,
			@QueryParam("bankId") Integer bankId,
			@QueryParam("dateb") String dateb
			){
		logger.info("Solicitud de todos los lotes por comercio");
		Session session = null;
		BatchHistoryConsultSynchronizedList list = null;
		List<BatchHistoryConsultSynchronizedMin> result = new ArrayList<BatchHistoryConsultSynchronizedMin>();
		List<BatchHistory> batchHistoryUpdate = new ArrayList<BatchHistory>();
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String 	sql = 
					"SELECT DISTINCT bh.ID AS BATCHID, tr.ID as TERMINALID, b.NAME as BANKNAME, m.MERCHANT_GUID, m.NAME as MERCHANTNAME, m.RIF, "
					+ "SUBSTR(ac.MERCHANT_ID_CODE,3,8) AS MERCHANTIDCODE, SUBSTR(tp.PARAMETER_VALUE,6,3) AS TERMINALBANK, "
					+ "bt.BATCH_TYPE_NAME AS BATCHTYPE, bh.BATCH_NUMBER AS BATCHNUMBER, bh.TOTAL_AMOUNT, "
					+ "(SELECT count(m2.RIF) FROM MERCHANT m2 "
					+ "INNER JOIN AFFILIATION af2 ON m2.ID=af2.MERCHANT_ID "
					+ "INNER JOIN ACQUIRER ac2 ON ac2.AFFILIATION_ID=af2.ID "
					+ "INNER JOIN TERMINAL tr2 ON tr2.AFFILIATION_ID=af2.ID "
					+ "INNER JOIN TERMINAL_PARAMETER tp2 ON tr2.ID=tp2.TERMINAL_ID "
					+ "WHERE ac.MERCHANT_ID_CODE=ac2.MERCHANT_ID_CODE AND tp.PARAMETER_VALUE=tp2.PARAMETER_VALUE AND m2.RIF=m.RIF) AS DUPLICATE "
					+ "FROM BATCH_HISTORY bh "
					+ "INNER JOIN BATCH_TYPE bt ON bt.ID=bh.BATCH_TYPE_ID "
					+ "INNER JOIN TERMINAL tr ON bh.TERMINAL_ID=tr.ID "
					+ "INNER JOIN AFFILIATION af ON af.ID=tr.AFFILIATION_ID "
					+ "INNER JOIN ACQUIRER ac ON af.ID=ac.AFFILIATION_ID "
					+ "INNER JOIN BANK b ON b.ID=ac.BANK_ID "
					+ "INNER JOIN MERCHANT m ON m.ID=af.MERCHANT_ID "
					+ "INNER JOIN TERMINAL_PARAMETER tp ON tr.ID=tp.TERMINAL_ID "
					+ ((bankId != 0) ?
						"WHERE (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>= :date AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<= :dateb AND bh.SYNCHRONIZED = :synchronized) AND b.id= :bankId ORDER BY m.RIF" : "WHERE (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>= :date AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<= :dateb AND bh.SYNCHRONIZED = :synchronized) ORDER BY m.RIF" );
					;

			logger.debug("Query: {}", sql);
			
			
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setParameter("date", date);
			queryParam.setParameter("dateb", dateb);
			queryParam.setParameter("synchronized", 0);
			if(bankId != 0) {
				queryParam.setParameter("bankId", bankId);
			}
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			
			logger.info("Data {}", data);
			data.forEach(elem->{ 
				BatchHistoryConsultSynchronizedMin batchHistory = new BatchHistoryConsultSynchronizedMin();
				batchHistory.setBatchId((BigDecimal) elem.get("BATCHID"));
				batchHistory.setTerminalId((BigDecimal) elem.get("TERMINALID"));
				batchHistory.setMerchantGuid((String) elem.get("MERCHANT_GUID"));
				batchHistory.setBankName((String) elem.get("BANKNAME"));
				batchHistory.setMerchantName((String) elem.get("MERCHANTNAME"));
				batchHistory.setRif((String) elem.get("RIF"));
				batchHistory.setMerchantIdCode((String) elem.get("MERCHANTIDCODE"));
				batchHistory.setTerminalBank((String) elem.get("TERMINALBANK"));
				batchHistory.setBatchType((String) elem.get("BATCHTYPE"));
				batchHistory.setBatchNumber((BigDecimal) elem.get("BATCHNUMBER"));
				batchHistory.setTotalAmount((BigDecimal) elem.get("TOTAL_AMOUNT"));
				batchHistory.setDuplicate((BigDecimal) elem.get("DUPLICATE"));
				
				result.add(batchHistory);
			});
			logger.debug("Result {}", result);
		
			list = new BatchHistoryConsultSynchronizedList(
					result
					);
			
			logger.debug("List {}", list.getBatchHistorySynchronized());

			/*batchHistoryUpdate = ResourceQuery.searchBatchHistory(date);
			for(BatchHistory batchHistory : batchHistoryUpdate){
				batchHistory.setBatchHistSynchronized(true);
				batchHistory.setSynchronizedDate(Calendar.getInstance());
				session.beginTransaction();
				session.update(batchHistory);
				session.getTransaction().commit();
			}	*/
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		logger.debug("Antes de retornar");
		return list == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun comercio", "Recurso no encontrado") : list;
	}
}
