package vpago.dataapi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Agrupa metodos y atributos comunes a las clases que manejan los recursos y servicios de la API
 */
public class ResourceManager {
	protected static final Logger logger = LoggerFactory.getLogger(ResourceManager.class);
}
