package vpago.dataapi.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StandardBasicTypes;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.Affiliation;
import vpago.dataapi.classes.BatchType;
import vpago.dataapi.classes.CurrentBatch;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.PaymentChannel;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.Terminal;
import vpago.dataapi.classes.TerminalParameter;
import vpago.dataapi.classes.forms.MerchantTerminalList;
import vpago.dataapi.classes.forms.NewMerchantTerminal;
import vpago.dataapi.classes.forms.NewMerchantTerminalList;
import vpago.dataapi.classes.minified.BatchSummaryConsultMin;
import vpago.dataapi.classes.minified.TerminalAffiliationMin;
import vpago.dataapi.classes.minified.TerminalDeviceMin;
import vpago.dataapi.classes.minified.TerminalSummaryMin;
import vpago.dataapi.classes.wrappers.BatchSummaryConsultList;
import vpago.dataapi.classes.wrappers.TerminalAffiliationList;
import vpago.dataapi.classes.wrappers.TerminalDeviceList;
import vpago.dataapi.classes.wrappers.TerminalList;
import vpago.dataapi.classes.wrappers.TerminalParametersList;
import vpago.dataapi.classes.wrappers.TerminalSummaryList;
import vpago.dataapi.config.EngineProperties;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.util.AutoAffiliation;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Terminal (Terminal)
 */
@Path("/terminal")
public class TerminalResource extends ResourceManager {
	
	/**
	 * Metodo para retornar todos los terminales almacenados en el sistema
	 * 
	 * @return Lista de terminales
	 */
	@SuppressWarnings({ "unchecked"})
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getTerminal(){
		Session session = null;
		TerminalList terminalList = new TerminalList();
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.TerminalMin(id, terminalGuid, active, affiliationId) from Terminal as terminal ";
			terminalList = new TerminalList(session.createQuery(hsqlquery).list());
			logger.debug("Lista de terminales", terminalList);
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		}finally {
			if(session != null) session.close();
		}
		
		return terminalList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun terminal asociado al comercio", "Recurso no encontrado") : terminalList;
	}
	
	/**
	 * Consultar Terminales de Comercio: Construye una lista con las entidades de tipo Terminal (Terminal) asociadas a un determinado Comercio (Merchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @return La lista de todas las entidades de tipo Terminal (Terminal) asociadas al Comercio (Merchant) identificado por <strong>merchantGuid</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("bymerchant/{merchant_guid: [a-fA-F\\d]{16}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getTerminalsByMerchant(@PathParam("merchant_guid") String merchantGuid){
		Session session = null;
		TerminalList terminalList = new TerminalList();
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if(merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Recurso no encontrado");
			if(!merchant.getAffiliations().isEmpty()) {
				session = HibernateUtil.getSessionFactory().openSession();
				String hsqlquery = "select new vpago.dataapi.classes.minified.TerminalMin(id, terminalGuid, active, affiliationId) from Terminal as terminal where terminal.affiliation in :affiliations ";
				terminalList = new TerminalList(session.createQuery(hsqlquery)
				.setParameterList("affiliations", merchant.getAffiliations()).list());
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return terminalList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun terminal asociado al comercio", "Recurso no encontrado") : terminalList;
	}
	
	/**
	 * Consultar Terminal: Consultar los datos de una entidad de tipo Terminal (Terminal)
	 * 
	 * @param terminalGuid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Datos de la entidad de tipo Terminal (Terminal) identificada por <strong>terminalGuid</strong>
	 */
	@Path("/{terminal_guid: [a-fA-F\\d]{8}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getTerminal(@PathParam("terminal_guid") String terminalGuid) {
		try {
			Terminal terminal = null;
			terminal = ResourceQuery.searchTerminal(terminalGuid);
			if(terminal != null) {
				terminal.setAccessKey(null);
				return terminal;
			}
			else return new Response(ResponseType.RESOURCENOTFOUND, "El terminal a asociar no existe", "Identificador no encontrado"); 
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		}
	}
	
	/**
	 * Crear Terminal: Crea una entidad del tipo Terminal (Terminal) asociada a la afiliación indicada
	 * 
	 * @param affiliationId Identificador del recurso de tipo Afiliacion (Affiliation)
	 * 
	 * @return Datos de la entidad de tipo Terminal (Terminal) que ha sido creada
	 */
	@SuppressWarnings("unchecked")
	@Path("{affiliation_id: \\d+ }")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createTerminal(@PathParam("affiliation_id") int affiliationId){
		Terminal terminal = null;
		Affiliation affiliation = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			affiliation = (Affiliation)session.get(Affiliation.class, affiliationId);
			if(affiliation != null) {
				terminal = new Terminal(affiliation);
				terminal.setTraceCounter(0);
				terminal.setCreateDate(Calendar.getInstance());
				logger.debug("Terminal a crear {}",terminal);
				logger.trace("{}", terminal);
				session.beginTransaction();
				Integer terminalCreated = (Integer)session.save(terminal);
				terminal.generateTerminalGuid();
				session.update(terminal);
				session.getTransaction().commit();
				logger.debug("-- Terminal creado -- {}",terminalCreated);
				
				//Terminal t = (Terminal)session.get(Terminal.class, terminalCreated);//Objecto del Terminal creado
				terminal.setCurrentBatchs(new ArrayList<CurrentBatch>());
				CurrentBatch currentBatch = null;
				for(BatchType batchType : affiliation.getPaymentChannel().getBatchTypes()){
					currentBatch = new CurrentBatch();
					currentBatch.setTerminal(terminal);
					currentBatch.setBatchNumber(1);
					currentBatch.setBatchType(batchType);
					currentBatch.setReferenceCounter(0);
					currentBatch.setTotalAmount(0.0);
					currentBatch.setTotalTrx(0);
					terminal.getCurrentBatchs().add(currentBatch);
					logger.debug("-- CurrentBatch -- {}", currentBatch);
//					session.beginTransaction();
//					session.save(currentBatch);//Inicializar valores de currentBatch con cada tipo de lote
//					session.getTransaction().commit();
				}
				session.beginTransaction();
				session.update(terminal);
				session.getTransaction().commit();
				
				// TODO Colocar el loadProperties solo al arrancar el sistema
				//Agregar un parametro de terminal bancario por cada adquiriente
				EngineProperties.loadProperties();
				boolean autoAssignation = EngineProperties.getAutoAffiliation();
				if(autoAssignation) {
					boolean autoTermSuccess = true;
					String autoTerm;
					ArrayList<NewMerchantTerminal> merchantTermList = new ArrayList<NewMerchantTerminal>();
					for(Acquirer acquirer : affiliation.getAcquirers()) {
						autoTerm = AutoAffiliation.getTerminalNumber();
						if(autoTerm.isEmpty()) autoTermSuccess = false;
						terminal.addTerminalParameter(
								acquirer, 
								affiliation.getPaymentChannel().getTemplateMessage().get(0).getTerminalParamName(), 
								autoTerm);
						merchantTermList.add(new NewMerchantTerminal(
								acquirer.getAffiliation().getMerchant().getMerchantGuid(),
								acquirer.getMerchantIdCode(),
								terminal.getTerminalGuid(),
								autoTerm
								));
					}
					if (autoTermSuccess) {
						session.beginTransaction();
						session.update(terminal);
						session.getTransaction().commit();
					} else {
						return new Response(ResponseType.GENERALERROR, "No se pudo crear el terminal", "El terminal automatico no pudo generarse");  
					}
					
					//Enviar terminal y comercio al autorizador
					MerchantTerminalList merchantTermAns = new MerchantTerminalList();
					try {
						logger.debug("Enviar parametros de terminal nuevo al vautorizador {}", merchantTermList);
						logger.debug("Estableciendo conexion con vautorizador");
						String vauthTarget = "http://" + EngineProperties.getVauthIp() + ":" + EngineProperties.getVauthPort() + "/vauth/service/affiliation/terminal";
						logger.debug("vauthTarget: {}", vauthTarget);
						Client gatewayClient = ClientBuilder.newClient();
						WebTarget gatewayWebTarget = gatewayClient.target(vauthTarget);
						Invocation.Builder gInvocationBuilder = gatewayWebTarget.request().accept(MediaType.APPLICATION_JSON);
						logger.debug("Enviando solicitud mediante servicio web");
						NewMerchantTerminalList termList = new NewMerchantTerminalList();
						termList.termList = merchantTermList;
						javax.ws.rs.core.Response gResponse = gInvocationBuilder.post(Entity.entity(termList, MediaType.APPLICATION_JSON));
						logger.debug("Recibiendo respuesta del servicio web");
						merchantTermAns = gResponse.readEntity(MerchantTerminalList.class);
						logger.debug("Respuesta: {}", merchantTermAns);
					} catch (Exception e) {
						logger.error("No se pudieron enviar los parametros del terminal {} al vautorizador", merchantTermList);
					}
				}
				terminal.setAccessKey(null);
			}
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(affiliation == null) return new Response(ResponseType.RESOURCENOTFOUND, "La afiliacion a asociar no existe", "Identificador no encontrado"); 
		else if (terminal == null) return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", "El terminal no pudo ser creado");
		else return terminal;
	}
	
	/**
	 * Habilitar Terminal: Habilita una entidad de tipo Terminal (Terminal) para realizar transacciones
	 * 
	 * @param terminalGuid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/enable/{terminal_guid: [a-fA-F\\d]{8}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse enableTerminal(@PathParam("terminal_guid") String terminalGuid) {
		Terminal terminal = null;
		Session session = null;
		try {
			terminal = ResourceQuery.searchTerminal(terminalGuid);
			if(terminal != null){
				session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				terminal.setActive(true);
				session.update(terminal);
				session.getTransaction().commit();
				return new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Terminal activado exitosamente");
			}
			return new Response(ResponseType.GENERALERROR, "Solicitud no exitosa", "Terminal no activado");
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}
	
	/**
	 * Deshabilitar Terminal: Deshabilita una entidad de tipo Terminal (Terminal) para realizar transacciones
	 * 
	 * @param terminalGuid Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return Mensaje con el resultado de la función
	 */
	@Path("/disable/{terminal_guid: [a-fA-F\\d]{8}}")
	@POST
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse disableTerminal(@PathParam("terminal_guid") String terminalGuid) {
		Terminal terminal = null;
		Session session = null;
		try {
			terminal = ResourceQuery.searchTerminal(terminalGuid);
			if(terminal != null){
				session = HibernateUtil.getSessionFactory().openSession();
				session.beginTransaction();
				terminal.setActive(false);
				session.update(terminal);
				session.getTransaction().commit();
				return new Response(ResponseType.SUCCESS, "Solicitud exitosa", "Terminal desactivado exitosamente");
			}
			return new Response(ResponseType.GENERALERROR, "Solicitud no exitosa", "Terminal no desactivado");
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}	
	
	/**
	 * Listar parametros de terminal bancario de Terminal: Construye la lista de los parametros de terminal bancario que estan asociados a una entidad de tipo Terminal (Terminal)
	 * 
	 * @param terminalId Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @return La lista de los terminales bancarios asociados al Terminalm (Terminal) identificado por <strong>terminalId</strong>
	 */
	@Path("{terminal_id: \\d+ }/parameter")
	@GET
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse listTerminalParameters(@PathParam("terminal_id") int terminalId){
		Terminal terminal = null;
		TerminalParametersList terminalParamList = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			terminal = (Terminal)session.get(Terminal.class, terminalId);
			if(terminal != null) terminalParamList = new TerminalParametersList(terminal.getTerminalParameters());
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.toString());
		} finally {
			if(session != null) session.close();
		}
		if(terminal == null) return new Response(ResponseType.RESOURCENOTFOUND, "El terminal a asociar no existe", "Identificador no encontrado"); 
		else return terminalParamList;
	}	
	
	/**
	 * Anadir parametro de terminal bancario a Terminal: Asocia un valor de terminal bancario a una entidad de tipo Terminal (Terminal)
	 * 
	 * @param terminalId Identificador del recurso de tipo Terminal (Terminal)
	 * 
	 * @param paymentChannelId: Identificador del recurso de tipo Canal de Pago (PaymentChannel)
	 * 
	 * @param terminalParam Valor de terminal bancario a almacenar
	 * 
	 * @return La lista de los terminales bancarios asociados al Terminalm (Terminal) identificado por <strong>terminalId</strong>
	 */
	@Path("{terminal_id: \\d+ }/{payment_channel_id: \\d+ }/parameter/add")
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse addTerminalParameter(@PathParam("terminal_id") int terminalId, @PathParam("payment_channel_id") int paymentChannelId, TerminalParameter terminalParam){
		logger.trace("TerminalParameter recibido: {}", terminalParam);
		TerminalParametersList terminalParamList = null;
		Terminal terminal = null;
		Acquirer acquirer = null;
		Session session = null;
		PaymentChannel paymentChannel = null;
		try {
			if (terminalParam.getAcquirerId() == null) return new Response(ResponseType.RESOURCENOTFOUND, "El adquiriente a asociar no existe", "Identificador no encontrado"); 
			session = HibernateUtil.getSessionFactory().openSession();
			terminal = (Terminal)session.get(Terminal.class, terminalId);
			acquirer = (Acquirer)session.get(Acquirer.class, terminalParam.getAcquirerId());
			paymentChannel = (PaymentChannel)session.get(PaymentChannel.class, paymentChannelId);
			//Verificamos que el terminal y el adquiriente esten asociados al mismo comercio
			if((terminal != null) && (acquirer != null) && (paymentChannel != null)) {
				if(terminal.getAffiliation().getMerchant().getId() == acquirer.getAffiliation().getMerchant().getId()) {
					terminal.addTerminalParameter(acquirer, paymentChannel.getTemplateMessage().get(0).getTerminalParamName(), terminalParam.getValue());
					// TODO Si el parametro a agregar ya existe para una combinacion de canal de pago - adquiriente, reemplazar
					session.beginTransaction();
					session.update(terminal);
					session.getTransaction().commit();
					terminalParamList = new TerminalParametersList(terminal.getTerminalParameters());
				} else {
					return new Response(ResponseType.INVALIDRESOURCE, "El adquiriente a asociar no pertenece al mismo comercio que el terminal", "Recurso invalido"); 
				}	
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		if(terminal == null) return new Response(ResponseType.RESOURCENOTFOUND, "El terminal a asociar no existe", "Identificador no encontrado"); 
		else if(acquirer == null) return new Response(ResponseType.RESOURCENOTFOUND, "El adquiriente a asociar no existe", "Identificador no encontrado"); 
		else if (paymentChannel == null) return new Response(ResponseType.RESOURCENOTFOUND, "El canal de pago a asociar no existe", "Identificador no encontrado"); 
		else return terminalParamList;
	}

	/**
	 * Lista completa de terminal, terminal bancario, dispositivo asignado, sim card asignado, status
	 * 
	 * @param merchantGuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Path("bymerchantsummary/{merchant_guid: [a-fA-F\\d]{16}}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getTerminalsByMerchantSummary(@PathParam("merchant_guid") String merchantGuid){
		logger.debug("merchantGuid {}",merchantGuid);
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<TerminalSummaryMin> result = new ArrayList<TerminalSummaryMin>();
		TerminalSummaryList listTerminal = null;
		
		try {
			String sql = "SELECT t.id as terminalId, t.terminal_guid as terminalGuid, tp.id as terminalBankId, SUBSTR(PARAMETER_VALUE,6,8) as terminalBank, "
					+ "ad.id as assignedDeviceId, ad.serial_number as serialNumberDevice, ad.serial_number_simcard as serialNumberSimCard, ad.operator_name as operatorNameSimCard, "
					+ "device.id as deviceId, device.display_name as deviceName, "
					+ "tp.acquirer_id as acquirerId, t.active as active, p.id as paymentChannelId, p.name as paymentChannelName "
					+ "FROM terminal t "
					+ "inner join affiliation af on t.affiliation_id=af.id "
					+ "inner join merchant m on af.merchant_id=m.id "
					+ "inner join payment_channel p on af.payment_channel_id=p.id "
					+ "left join assigned_device ad on t.assigned_device_id=ad.id "
					+ "left join device device on ad.device_id=device.id "
					+ "inner join terminal_parameter tp on t.id=tp.terminal_id "
					+ "where m.merchant_guid='"+merchantGuid+"' order by terminalBank asc";
			System.out.println(sql);
			//Ejecutar la consulta sql
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			
			List<Map<String, Object>> data = queryParam.list();
		
			/*
			 * Recorrer la lista que se obtiene desde el query y setear la data al min para generar la lista en XML/JSON
			 */
			data.forEach(elem->{
				TerminalSummaryMin list = new TerminalSummaryMin();
				try {
					list.setTerminalId(((BigDecimal)elem.get("TERMINALID")).intValue());
					list.setTerminalGuid((String) elem.get("TERMINALGUID"));
					list.setTerminalBankId(((BigDecimal) elem.get("TERMINALBANKID")).intValue());
					list.setTerminalBank((String) elem.get("TERMINALBANK"));
					list.setAssignedDeviceId(elem.get("ASSIGNEDDEVICEID") != null ? ((BigDecimal) elem.get("ASSIGNEDDEVICEID")).intValue() : null);
					list.setDeviceId(elem.get("DEVICEID") != null ? ((BigDecimal) elem.get("DEVICEID")).intValue() : null);
					list.setSerialNumberDevice((String) elem.get("SERIALNUMBERDEVICE"));
					list.setDeviceName((String) elem.get("DEVICENAME"));
					list.setAcquirerId(((BigDecimal) elem.get("ACQUIRERID")).intValue());
					list.setActive(((BigDecimal)elem.get("ACTIVE")).intValue() == 1 ? true : false);
					list.setPaymentChannelId(((BigDecimal) elem.get("PAYMENTCHANNELID")).intValue());
					list.setPaymentChannelName((String) elem.get("PAYMENTCHANNELNAME"));
					list.setSerialNumberSimCard((String) elem.get("SERIALNUMBERSIMCARD"));
					list.setOperatorNameSimCard((String) elem.get("OPERATORNAMESIMCARD"));
					result.add(list);
				} catch (Exception e) {
					logger.error(e.toString());
				}
				logger.trace("list {}",list);
			});
			
			/*
			 * Setear la lista del min al wrapper 	
			 */
			listTerminal =  new TerminalSummaryList (
					result
					);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		return listTerminal == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun terminal para ese comercio", "Recurso no encontrado") : listTerminal;
	}
	
	@Path("/affiliations")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getTerminalAffiliation(@QueryParam("affiliationId") Integer affiliationId) throws NullPointerException{
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		List<TerminalAffiliationMin> result = new ArrayList<TerminalAffiliationMin>();
		TerminalAffiliationList consultList = null;
		
		try {
			/*String sql = "SELECT tp.ID, tp.PARAMETER_VALUE FROM TERMINAL t INNER JOIN TERMINAL_PARAMETER tp ON t.ID=tp.TERMINAL_ID WHERE "
					+ "t.AFFILIATION_ID=" + affiliationId + " ORDER BY tp.PARAMETER_VALUE asc";
			logger.info("SQL a ejecutar {}",sql);
			
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			
			data.forEach(elem->{
				TerminalAffiliationMin consult = new TerminalAffiliationMin();
				consult.setTerminalParameterId((Integer) elem.get("ID"));
				consult.setParameterValue(String.valueOf((elem.get("PARAMETER_VALUE"))));
				result.add(consult);
			});
			*/
			List<Terminal> terminalList = ResourceQuery.searchTerminalAf(affiliationId);
			terminalList.forEach(elem->{
				try {					
					TerminalAffiliationMin consult = new TerminalAffiliationMin();
					consult.setTerminalParameterId(elem.getTerminalParameters().get(0).getId());
					consult.setParameterValue(elem.getTerminalParameters().get(0).getValue());
					result.add(consult);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
				logger.debug("Result {}",result);
			});
			/*for (Terminal terminal: terminalList){
				logger.debug("Lista terminal {}", terminal);
				try {					
					TerminalAffiliationMin consult = new TerminalAffiliationMin();
					consult.setTerminalParameterId(terminal.getTerminalParameters().get(0).getId());
					consult.setParameterValue(terminal.getTerminalParameters().get(0).getValue());
					result.add(consult);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}*/
			
			/** Agregar el objeto min al wrapper **/
			consultList =  new TerminalAffiliationList(
					result
					);
			
			logger.debug("consultList {}",consultList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		return consultList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : consultList;
	}
	
	@Path("/updateTerminalParameter")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateTerminalAffiliation(@QueryParam("terminalParameterId") Integer terminalParameterId, @QueryParam("parameterValue") String parameterValue) throws HibernateException, SQLException{
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		TerminalParameter terminalParameter= null;
		terminalParameter = ResourceQuery.searchTerminalParameter(terminalParameterId);
		Response response = new Response();
		try {
			String msg ="El terminal ";
			Boolean resp = true;
			List<CurrentBatch> currentBatchList =  new ArrayList<>();
			logger.info("terminalParameter.getTerminal().getId():==> {}", terminalParameter.getTerminal().getId());
			currentBatchList = ResourceQuery.searchCurrentBatch(terminalParameter.getTerminal().getId());
			logger.info("currentBatchList:==> {}", currentBatchList);
			boolean cbresp = true;
			for (CurrentBatch currentBatch: currentBatchList){
				logger.info("currentBatch:==> Txt: {}; TxtAm: {}", currentBatch.getTotalTrx(), currentBatch.getTotalAmount());
				if (cbresp == true){
					if ((currentBatch.getTotalTrx() > 0) && (currentBatch.getTotalAmount() > 0)){
						logger.info("TerminalParameters:==> {}", terminalParameter.getValue());
						msg = msg + terminalParameter.getValue().substring(terminalParameter.getValue().length()-2,terminalParameter.getValue().length()) + ", ";
						logger.info("msg:==> {}",msg);
						resp = false;
						cbresp = false;
					}
				}
			}
			msg = msg.substring(0,msg.length()-2) + " dispone de transacciones pendientes sin cerrar, por favor generar el cierre de los mismo, para poder procesar el cambio de terminal.";
			if (resp == true){
				logger.info("terminalParameter:==> {}", terminalParameter);
				terminalParameter.setValue(parameterValue);
				session.beginTransaction();
				session.update(terminalParameter);
				session.getTransaction().commit();
				response.setType(ResponseType.SUCCESS);
				response.setDescription("Terminal bancario actualizado.");
				response.setName("Terminal bancario actualizado.");
			}else{
				response.setType(ResponseType.INVALIDRESOURCE);
				response.setDescription(msg);
				response.setName("Actualizacion de terminal bancario.");
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}
	
	/**
     * Consultar Terminales de Comercio: Construye una lista con las entidades de tipo Terminal (Terminal) asociadas a un determinado Comercio (Merchant)
     * 
     * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
     * 
     * @return La lista de todas las entidades de tipo Terminal (Terminal) asociadas al Comercio (Merchant) identificado por <strong>merchantGuid</strong>
     */
    @SuppressWarnings("unchecked")
    @Path("terminalsDeviceList")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public APIResponse getTerminalsDevice(@QueryParam("consultType") String consultType, @QueryParam("dayStart") String dayStart, @QueryParam("dayEnd") String dayEnd){
        logger.debug("terminalsDeviceList ==========> {}",consultType);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<TerminalDeviceMin> result = new ArrayList<TerminalDeviceMin>();
        TerminalDeviceList listTerminal = null;
        
        try {
            String sql = "SELECT DISTINCT m.merchant_guid as merchantGuid, SUBSTR(ac.merchant_id_code, 3, 8) as merchantIdCode, t.id as terminalId, t.terminal_guid as terminalGuid, tp.id as terminalBankId, SUBSTR(PARAMETER_VALUE,6,8) as terminalBank, "
                    + "ad.id as assignedDeviceId, ad.serial_number as serialNumberDevice, ad.serial_number_simcard as serialNumberSimCard, ad.operator_name as operatorNameSimCard, "
                    + "device.id as deviceId, device.display_name as deviceName, "
                    + "tp.acquirer_id as acquirerId, t.active as active, p.id as paymentChannelId, p.name as paymentChannelName, "
                    + "m.rif as merchantRif, m.name as merchantName, bk.name as bankName,  "
                    + "c.PHONE1, (c.LASTNAME || ' ' || c.NAME) AS personaContacto, c.email,  st.STATE, ci.CITY, ad.LOCATION, ad.PROPERTY, ind.INDUSTRY_TYPE_NAME AS categoria,"
                    + "TO_CHAR(t.date_assignment, 'YYYY-MM-DD HH24:MI:SS') as DATE_ASSIGNMENT, TO_CHAR(t.start_date_operations, 'YYYY-MM-DD HH24:MI:SS') as start_date_operations, TO_CHAR(t.serial_update_date, 'YYYY-MM-DD HH24:MI:SS') as serial_update_date, TO_CHAR(t.last_transaction_date, 'YYYY-MM-DD HH24:MI:SS') as last_transaction_date ";
                    if (consultType != null){
                    	if (consultType == "dayFirst" || consultType.equals("dayFirst")){
                    		sql = sql + ", (SELECT min(TO_CHAR(bh.CLOSING_TIMESTAMP, 'YYYY-MM-DD')) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID) AS firstDate"; 
                    	}else if ((consultType == "accumulated" || consultType.equals("accumulated")) && (dayStart != null && dayEnd != null)){
                    		sql = sql + ", (SELECT SUM(TOTAL_TRX) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS accumulatedTrx";
                    		sql = sql + ", (SELECT SUM(TOTAL_AMOUNT) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS accumulatedAmount";
                    		sql = sql + ", (SELECT (SUM(TOTAL_AMOUNT) /count(*)) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS averageTicket";
                    	}else if (consultType == "mixed" || consultType.equals("mixed")){
                    		sql = sql + ", (SELECT min(TO_CHAR(bh.CLOSING_TIMESTAMP, 'YYYY-MM-DD')) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID) AS firstDate";
                    		sql = sql + ", (SELECT SUM(TOTAL_TRX) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS accumulatedTrx";
                    		sql = sql + ", (SELECT SUM(TOTAL_AMOUNT) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS accumulatedAmount";
                    		sql = sql + ", (SELECT (SUM(TOTAL_AMOUNT) /count(*)) FROM ENGINE.BATCH_HISTORY bh WHERE bh.TERMINAL_ID=t.ID AND (TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')>=:dayStart AND TO_CHAR(bh.CLOSING_TIMESTAMP,'YYYY-MM-DD')<=:dayEnd)) AS averageTicket";
                    	}
                    }
                    logger.debug("Sql-1: {}",sql);
                    sql = sql + " FROM terminal t "
                    + "inner join affiliation af on t.affiliation_id=af.id "
                    + "inner join acquirer ac on ac.affiliation_id=af.id "
                    + "inner join bank bk on ac.bank_id=bk.id "
                    + "inner join merchant m on af.merchant_id=m.id "
                    + "inner join payment_channel p on af.payment_channel_id=p.id "
                    + "inner join assigned_device ad on t.assigned_device_id=ad.id "
                    + "inner join device device on ad.device_id=device.id "
                    + "inner join terminal_parameter tp on t.id=tp.terminal_id "
                    + "INNER JOIN ENGINE.LOCATION l ON m.LOCATION_ID=l.ID "
                    + "INNER JOIN ENGINE.ADDRESS ad ON l.ADDRESS_ID=ad.ID "
                    + "INNER JOIN ENGINE.CONTACT c ON l.CONTACT_ID=c.ID "
                    + "INNER JOIN ENGINE.STATE st ON ad.STATE_ID=st.ID "
                    + "INNER JOIN ENGINE.CITY ci ON ad.CITY_ID=ci.ID "
                    + "INNER JOIN ENGINE.INDUSTRY_TYPE ind ON m.INDUSTRY_TYPE_ID=ind.ID "
                    + "order by merchantName asc, terminalBank asc ";
                    logger.debug("Sql-2: {}",sql);
            //Ejecutar la consulta sql
            Query queryParam = session.createSQLQuery(sql);
            if (consultType != null && (consultType == "accumulated" || consultType.equals("accumulated") || consultType == "mixed" || consultType.equals("mixed")) && (dayStart != null && dayEnd != null)){
            	queryParam.setString("dayStart", dayStart)
            	.setString("dayEnd", dayEnd);
            }
            queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
            
            List<Map<String, Object>> data = queryParam.list();
            
            /*
             * Recorrer la lista que se obtiene desde el query y setear la data al min para generar la lista en XML/JSON
             */
            data.forEach(elem->{
                TerminalDeviceMin list = new TerminalDeviceMin();
                try {
                    list.setTerminalId(((BigDecimal)(elem.get("TERMINALID")!=null?elem.get("TERMINALID"):0)).intValue());
                    list.setTerminalGuid((String) (elem.get("TERMINALGUID")!= null?elem.get("TERMINALGUID"):"-"));
                    list.setTerminalBankId(((BigDecimal) (elem.get("TERMINALBANKID")!=null?elem.get("TERMINALBANKID"):0)).intValue());
                    list.setTerminalBank((String) (elem.get("TERMINALBANK")!=null?elem.get("TERMINALBANK"):0));
                    list.setAssignedDeviceId(elem.get("ASSIGNEDDEVICEID") != null ? ((BigDecimal) elem.get("ASSIGNEDDEVICEID")).intValue() : 0);
                    list.setDeviceId(elem.get("DEVICEID") != null ? ((BigDecimal) elem.get("DEVICEID")).intValue() : 0);
                    list.setSerialNumberDevice((String) (elem.get("SERIALNUMBERDEVICE")!=null?elem.get("SERIALNUMBERDEVICE"):"-"));
                    list.setDeviceName((String) (elem.get("DEVICENAME")!=null?elem.get("DEVICENAME"):"-"));
                    list.setAcquirerId(((BigDecimal) (elem.get("ACQUIRERID")!=null?elem.get("ACQUIRERID"):0)).intValue());
                    list.setActive(((BigDecimal)elem.get("ACTIVE")).intValue() == 1 ? true : false);
                    list.setPaymentChannelId(((BigDecimal) (elem.get("PAYMENTCHANNELID")!=null?elem.get("PAYMENTCHANNELID"):0)).intValue());
                    list.setPaymentChannelName((String) (elem.get("PAYMENTCHANNELNAME")!=null?elem.get("PAYMENTCHANNELNAME"):"-"));
                    list.setSerialNumberSimCard((String) (elem.get("SERIALNUMBERSIMCARD")!=null?elem.get("SERIALNUMBERSIMCARD"):"-"));
                    list.setOperatorNameSimCard((String) (elem.get("OPERATORNAMESIMCARD")!=null?elem.get("OPERATORNAMESIMCARD"):"-"));
                    list.setMerchantRif((String) (elem.get("MERCHANTRIF")!=null?elem.get("MERCHANTRIF"):"-"));
                    list.setMerchantName((String) (elem.get("MERCHANTNAME")!=null?elem.get("MERCHANTNAME"):"-"));
                    list.setMerchantGuid((String) (elem.get("MERCHANTGUID")!=null?elem.get("MERCHANTGUID"):"-"));
                    list.setMerchantIdCode((String)(elem.get("MERCHANTIDCODE")!=null?elem.get("MERCHANTIDCODE"):"-"));
                    list.setBankName((String)(elem.get("BANKNAME")!=null?elem.get("BANKNAME"):"-"));
                	try {
	                    if (consultType != null && (consultType == "dayFirst"  || consultType.equals("dayFirst"))){
	                    	list.setDayActive((String)(elem.get("FIRSTDATE")!=null?elem.get("FIRSTDATE"):"-"));
	                    }else if (consultType != null && (consultType == "accumulated" || consultType.equals("accumulated")) && (dayStart != null && dayEnd != null)){ 
							logger.debug("AccumulatedTrx:= {}", elem.get("ACCUMULATEDTRX"));
							logger.debug("AccumulatedAmount:= {}", elem.get("ACCUMULATEDAMOUNT"));
							list.setAccumulatedTrx(BigInteger.valueOf(((BigDecimal) elem.get("ACCUMULATEDTRX")).intValue()));
							list.setAccumulatedAmount(((BigDecimal) elem.get("ACCUMULATEDAMOUNT")));
							list.setAverageTicket((BigDecimal) elem.get("AVERAGETICKET"));
	                	}else if (consultType != null && (consultType == "mixed"  || consultType.equals("mixed"))){
	                		list.setDayActive((String)(elem.get("FIRSTDATE")!=null?elem.get("FIRSTDATE"):"-"));
	                		list.setAccumulatedTrx(BigInteger.valueOf(((BigDecimal) elem.get("ACCUMULATEDTRX")).intValue()));
							list.setAccumulatedAmount(((BigDecimal) elem.get("ACCUMULATEDAMOUNT")));
							list.setAverageTicket((BigDecimal) elem.get("AVERAGETICKET"));
	                	}
                    } catch (Exception e) {
                    	if (consultType != null && (consultType == "mixed"  || consultType.equals("mixed"))){
                    		logger.error("Error en Lectura del Monto {}",e.toString());
                    		list.setDayActive("-");
                    		list.setAccumulatedAmount(new BigDecimal(0));
                    		list.setAverageTicket(new BigDecimal(0));
    						list.setAccumulatedTrx(BigInteger.valueOf(0));
                    	}else{
                    		logger.error("Error en Lectura del Monto {}",e.toString());
    						list.setAccumulatedAmount(new BigDecimal(0));
    						list.setAccumulatedTrx(BigInteger.valueOf(0));
    						list.setAverageTicket(new BigDecimal(0));
                    	}
					}
                	logger.debug("{}",elem.get("PERSONACONTACTO"));
                	 list.setLocation((String)(elem.get("LOCATION")!=null?elem.get("LOCATION"):"-"));
                	 list.setCity((String)(elem.get("CITY")!=null?elem.get("CITY"):"-"));
                	 list.setProperty((String)(elem.get("PROPERTY")!=null?elem.get("PROPERTY"):"-"));
                	 list.setState((String)(elem.get("STATE")!=null?elem.get("STATE"):"-"));
                	 list.setContact((String) elem.get("PERSONACONTACTO"));
                	 list.setPhone((String)(elem.get("PHONE1")!=null?elem.get("PHONE1"):"-"));
                	 list.setMerchantEmail((String)elem.get("EMAIL"));
                	 list.setIndustryType((String)(elem.get("CATEGORIA")!=null?elem.get("CATEGORIA"):"-"));
                	 list.setDateAssignment((String)elem.get("DATE_ASSIGNMENT"));
                	 list.setStartDateOperations((String)elem.get("START_DATE_OPERATIONS"));
                	 list.setLastTransactionDate((String)elem.get("LAST_TRANSACTION_DATE"));
                	 list.setSerialUpdateDate((String)elem.get("SERIAL_UPDATE_DATE"));
                	 String[] rif = list.getMerchantRif().split("-");
                	 if(rif[0]=="G"){
                		 list.setClassification("Publico");
                	 }else{
                		 list.setClassification("Privado");
                	 }
                    result.add(list);
                } catch (Exception e) {
                    
                }
                //logger.trace("list {}",list);
            });
            
            /*
             * Setear la lista del min al wrapper   
             */
            logger.debug("result:==> {}",result);
            listTerminal =  new TerminalDeviceList (
                    result
                    );
            logger.debug("listTerminal:==> {}",listTerminal);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("exception {}", e.toString());
            return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
        } finally {
            if(session != null) session.close();
        }
        
        return listTerminal == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun terminal para ese comercio", "Recurso no encontrado") : listTerminal;
    }
	
}
