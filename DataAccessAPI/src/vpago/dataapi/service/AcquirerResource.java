package vpago.dataapi.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Acquirer;
import vpago.dataapi.classes.Affiliation;
import vpago.dataapi.classes.Bank;
import vpago.dataapi.classes.CurrentBatch;
import vpago.dataapi.classes.Merchant;
import vpago.dataapi.classes.Multimerchant;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.RuleProportion;
import vpago.dataapi.classes.RuleSet;
import vpago.dataapi.classes.Terminal;
import vpago.dataapi.classes.TerminalParameter;
import vpago.dataapi.classes.TimeFrame;
import vpago.dataapi.classes.wrappers.AcquirerList;
import vpago.dataapi.config.EngineProperties;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;
import vpago.dataapi.types.WeekDay;
import vpago.dataapi.util.AutoAffiliation;
import vpago.dataapi.util.ResourceQuery;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Adquiriente (Acquirer)
 */
@Path("/acquirer")
public class AcquirerResource extends ResourceManager {

	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllAcquirers(){
		Session session = null;
		AcquirerList acquirerList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.AcquirerMin("
					+ "id, displayName, bankId, affiliationId, merchantIdCode) "
					+ "from Acquirer as acquirer order by displayName";
			logger.info("Query {}",hsqlquery);
			acquirerList = new AcquirerList(session.createQuery(hsqlquery)
					.list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return acquirerList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun adquiriente asociado al comercio", "Recurso no encontrado") : acquirerList;
	}
	
	/**
	 * Listar Adquirientes de Comercio: Construye una lista con las entidades de tipo Adquiriente (Acquirer) asociadas a un Comercio (Merchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Comercio (Merchant)
	 * 
	 * @param paymentChannelId Identificador de Canal de Pago (PaymentChannel)
	 * 
	 * @return La lista de todas las entidades de tipo Adquiriente (Acquirer) asociadas al Comercio (Merchant) identificado por <strong>merchantGuid</strong> filtradas por el Canal de Pago (PaymentChannel) identificado por <strong>paymentChannelId</strong>
	 */
	@SuppressWarnings("unchecked")
	@Path("merchant/{merchant_guid: [a-fA-F\\d]{16}}/{payment_channel_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAcquirersByMerchant(@PathParam("merchant_guid") String merchantGuid, @PathParam("payment_channel_id") int paymentChannelId){
		Session session = null;
		AcquirerList acquirerList = null;
		Merchant merchant = null;
		try {
			merchant = ResourceQuery.searchMerchant(merchantGuid);
			if(merchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "El comercio solicitado no existe", "Recurso no encontrado");
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.AcquirerMin("
					+ "id, displayName, bankId, affiliationId, merchantIdCode) "
					+ "from Acquirer as acquirer where "
					+ "acquirer.affiliation in :affiliations "
					+ "and acquirer.affiliation.paymentChannelId = :paymentChannelId";
			logger.info("Query {}",hsqlquery);
			acquirerList = new AcquirerList(session.createQuery(hsqlquery)
					.setParameterList("affiliations", merchant.getAffiliations())
					.setInteger("paymentChannelId", paymentChannelId)
					.list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return acquirerList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun adquiriente asociado al comercio", "Recurso no encontrado") : acquirerList;
	}
	
	/**
	 * Listar Adquirientes de Multicomercio: Construye una lista con las entidades de tipo Adquiriente (Acquirer) asociadas a los Comercios del Multicomercio (Multimerchant)
	 * 
	 * @param merchantGuid Identificador del recurso de tipo Multicomercio (Multimerchant)
	 * 
	 * @param paymentChannelId Identificador del recurso de tipo Canal de Pago (PaymentChannel)
	 * 
	 * @return La lista de todas las entidades de tipo Adquiriente (Acquirer) asociadas al Multicomercio (Multimerchant) identificado por <strong>merchantGuid</strong> filtradas por el Canal de Pago (PaymentChannel) identificado por <strong>paymentChannelId</strong>
	 */
	@Path("multimerchant/{merchant_guid: [a-fA-F\\d]{16}}/{payment_channel_id: \\d+}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAcquirersByMultimerchant(@PathParam("merchant_guid") String merchantGuid, @PathParam("payment_channel_id") int paymentChannelId) {
		Multimerchant multimerchant = null;
		List<Acquirer> acquirers = null;
		try {
			multimerchant = ResourceQuery.searchMultimerchant(merchantGuid);
			if(multimerchant != null){
				acquirers = new ArrayList<Acquirer>();
				for (int i = 0; i < multimerchant.getMerchants().size(); i++) {
					logger.trace("{}", multimerchant.getMerchants().get(i));
					for (int j = 0; j < multimerchant.getMerchants().get(i).getAffiliations().size(); j++) {
						logger.trace("{}", multimerchant.getMerchants().get(i).getAffiliations().get(j));
						if(multimerchant.getMerchants().get(i).getAffiliations().get(j).getPaymentChannelId() == paymentChannelId) {
							acquirers.addAll(multimerchant.getMerchants().get(i).getAffiliations().get(j).getAcquirers());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		}
		if(multimerchant == null) return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el multicomercio solicitado", "Recurso no encontrado");
		else {
			if(acquirers != null) {
				AcquirerList acquirerList = new AcquirerList();
				logger.trace("{}", acquirers);
				acquirerList.setFromAcquirerList(acquirers);
				return acquirerList;
			} else {
				return new Response(ResponseType.RESOURCENOTFOUND, "No se encontro el multicomercio solicitado", "Recurso no encontrado");
			}
		}
	}
	
	/**
	 * Consultar Adquiriente: Consultar los datos de una entidad de tipo Adquiriente (Acquirer)
	 * 
	 * @param acquirerId Identificador del recurso de tipo Adquiriente (Acquirer)
	 * 
	 * @return Datos de la entidad de tipo Adquiriente (Acquirer) identificada por <strong>acquirerId</strong>
	 */
	@Path("/{acquirer_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAcquirer(@PathParam("acquirer_id") int acquirerId) {
		Session session = null;
		Acquirer acquirer = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			acquirer = (Acquirer)session.get(Acquirer.class, acquirerId);	
			if(acquirer != null) acquirer.setBank(null);
		} catch (HibernateException e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		return acquirer == null ? new Response(ResponseType.RESOURCENOTFOUND, "La cuenta solicitada no existe", "Identificador no encontrado") : acquirer;
	}
	
	/**
	 * Crear Adquiriente: Crea una entidad del tipo Adquiriente (Acquirer) a partir de los datos enviados en el cuerpo de la solicitud
	 * 
	 * @param a Objeto de tipo <strong>Acquirer</strong> con los datos del Adquiriente (Acquirer)
	 * 
	 * @return Datos de la entidad de tipo Adquiriente (Acquirer) que ha sido creada
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse createAcquirer(Acquirer a) {
		Acquirer acquirer = null;
		Bank bank = null;
		Affiliation affiliation = null;
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			bank = (Bank)session.get(Bank.class, a.getBankId());
			logger.trace("Banco adquiriente {}", bank);
			affiliation = (Affiliation)session.get(Affiliation.class, a.getAffiliationId());
			logger.trace("Comercio {}", affiliation.getMerchant());
			
			if(bank != null && affiliation != null) {
				// TODO Colocar el loadProperties solo al arrancar el sistema
				EngineProperties.loadProperties();
				boolean autoAssignation = EngineProperties.getAutoAffiliation();
				String merchantIdCode = autoAssignation ? AutoAffiliation.getAffiliationNumber() : a.getMerchantIdCode(); 
				
				if (!merchantIdCode.isEmpty()) {
					acquirer = new Acquirer(merchantIdCode, affiliation, bank);
					session.beginTransaction();
					session.save(acquirer).toString();
					session.getTransaction().commit();
					//Conjunto de reglas por defecto
					Calendar cal1 = Calendar.getInstance();
					Calendar cal2 = Calendar.getInstance();
					cal1.set(Calendar.HOUR_OF_DAY, 8);
					cal1.set(Calendar.MINUTE, 0);
					cal1.set(Calendar.SECOND, 0);
					cal2.set(Calendar.HOUR_OF_DAY, 17);
					cal2.set(Calendar.MINUTE, 0);
					cal2.set(Calendar.SECOND, 0);
					RuleSet ruleSet = new RuleSet("Conjunto de reglas", "Conjunto de reglas por defecto",
							new TimeFrame(WeekDay.LUNES, cal1, cal2), affiliation);
					session.beginTransaction();
					session.save(ruleSet).toString();
					session.getTransaction().commit();
					//Reglas por defecto
					RuleProportion ruleS = new RuleProportion("Regla por defecto", "Regla de adquiriencia por defecto ",
							acquirer, affiliation, 100);
					ruleSet.addRule(ruleS);
					session.beginTransaction();
					session.save(ruleS).toString();
					session.update(ruleSet);
					session.getTransaction().commit();
					//Asignar conjunto de reglas a afiliacion
					affiliation.setActiveRuleSet(ruleSet);
					session.beginTransaction();
					session.update(affiliation);
					session.getTransaction().commit();
					acquirer.setBank(null);
				} else {
					return new Response(ResponseType.GENERALERROR, "No se pudo crear el adquiriente", "El afiliado automatico no pudo generarse");  
				}		
			}
		} catch (Exception e) {
			logger.error(e.toString());
			e.printStackTrace();
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if (session != null) session.close();
		}
		if(bank == null) return new Response(ResponseType.RESOURCENOTFOUND, "El banco solicitado no existe", "Identificador no encontrado"); 
		if(affiliation == null) return new Response(ResponseType.RESOURCENOTFOUND, "La afiliacion solicitada no existe", "Identificador no encontrado"); 
		else return acquirer;
	}
	
	@Path("/updateMerchantIdCode")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateMerchantIdCode(@QueryParam("acquirerId") Integer acquirerId, @QueryParam("merchantIdCode") String merchantIdCode) throws HibernateException, SQLException{
		logger.info("<==:updateMerchantIdCode:==>");
		logger.info("acquirerId:==> {}; merchantIdCode:==> {}", acquirerId, merchantIdCode);
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		Acquirer acquirer= null;
		acquirer = ResourceQuery.searchAcquirer(acquirerId);
		logger.info("acquirer:==> {}", acquirer);
		List<Terminal> terminalList = new ArrayList<>();
		terminalList = ResourceQuery.searchTerminalAf(acquirer.getAffiliation().getId());
		logger.info("terminalList:==> {}", terminalList);
		Response response = new Response();
		try {
			Boolean resp = true;
			String msg ="El terminal ";
			for (Terminal term: terminalList){
				List<CurrentBatch> currentBatchList =  new ArrayList<>();
				currentBatchList = ResourceQuery.searchCurrentBatch(term.getId());
				logger.info("term:==> {}", term);
				logger.info("currentBatchList:==> {}", currentBatchList);
				boolean cbresp = true;
				for (CurrentBatch currentBatch: currentBatchList){
					logger.info("currentBatch:==> Txt: {}; TxtAm: {}", currentBatch.getTotalTrx(), currentBatch.getTotalAmount());
					if (cbresp == true){
						if ((currentBatch.getTotalTrx() > 0) && (currentBatch.getTotalAmount() > 0)){
							logger.info("TerminalParameters:==> {}", term.getTerminalParameters().get(0).getValue());
							msg = msg + term.getTerminalParameters().get(0).getValue().substring(term.getTerminalParameters().get(0).getValue().length()-2,term.getTerminalParameters().get(0).getValue().length()) + ", ";
							logger.info("msg:==> {}",msg);
							resp = false;
							cbresp = false;
						}
					}
				}
			}
			msg = msg.substring(0,msg.length()-2) + " disponen de transacciones pendientes sin cerrar, por favor generar el cierre de los mismo, para poder procesar el cambio de afiliado.";
			if (resp == true){
				acquirer.setMerchantIdCode("00" + merchantIdCode + "     ");
				session.beginTransaction();
				session.update(acquirer);
				session.getTransaction().commit();
				response.setType(ResponseType.SUCCESS);
				response.setDescription("Actualizacion de afiliado procesada correctamente.");
				response.setName("Actualizacion de afiliado.");
			}else{
				response.setType(ResponseType.INVALIDRESOURCE);
				response.setDescription(msg);
				response.setName("Actualizacion de afiliado.");
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}

	@Path("/updateBank")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateBank(@QueryParam("acquirerId") Integer acquirerId, @QueryParam("bankId") Integer bankId) throws HibernateException, SQLException{
		logger.info("<==:updateBank:==>");
		logger.info("acquirerId:==> {}; bankId:==> {}", acquirerId, bankId);
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		Acquirer acquirer= null;
		acquirer = ResourceQuery.searchAcquirer(acquirerId);
		logger.info("acquirer:==> {}", acquirer);
		Response response = new Response();
		try {
			String displayNam = acquirer.getDisplayName().replace(acquirer.getBank().getName(), "");
			Bank bank = null;
			bank = ResourceQuery.searchBank(bankId);
			logger.info("bank:==> {}", bank);
			displayNam = displayNam + bank.getName();
			acquirer.setBank(bank);
			acquirer.setDisplayName(displayNam);
			session.beginTransaction();
			session.update(acquirer);
			session.getTransaction().commit();
			response.setType(ResponseType.SUCCESS);
			response.setDescription("Actualizacion banco procesado correctamente.");
			response.setName("Actualizacion de banco.");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
	}
	
}
