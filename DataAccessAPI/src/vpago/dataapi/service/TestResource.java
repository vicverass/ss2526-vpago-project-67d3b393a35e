package vpago.dataapi.service;

import java.util.Calendar;
import vpago.dataapi.classes.TimeFrame;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import vpago.dataapi.config.EngineProperties;
import vpago.dataapi.types.WeekDay;

@Path("/test")
public class TestResource extends ResourceManager {
	
	@GET
	public String testProperties(){
		logger.debug("Test resource");
		EngineProperties.loadProperties();
		logger.debug("getAutoAffiliation: " + EngineProperties.getAutoAffiliation());
		
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.set(Calendar.HOUR_OF_DAY, 8);
		cal2.set(Calendar.HOUR_OF_DAY, 17);
		TimeFrame tf = new TimeFrame(WeekDay.LUNES, cal1, cal2);
		logger.debug("{}", tf.toString());
		return "";
	}
}
