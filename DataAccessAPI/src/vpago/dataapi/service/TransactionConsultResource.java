package vpago.dataapi.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.minified.BatchConsultMin;
import vpago.dataapi.classes.minified.BatchSummaryConsultMin;
import vpago.dataapi.classes.minified.TransactionConsultMin;
import vpago.dataapi.classes.wrappers.BatchConsultList;
import vpago.dataapi.classes.wrappers.BatchSummaryConsultList;
import vpago.dataapi.classes.wrappers.TransactionConsultList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso de Transacciones
 */
@Path("/transaction")
public class TransactionConsultResource extends ResourceManager {

	/**
	 * Funcion para listar las transacciones
	 * 
	 * @param consultType
	 * @param rangeStart
	 * @param rangeEnd
	 * @param associateId
	 * @param rolUser
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@Path("/consult")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllTransactions(@QueryParam("consultType") String consultType,
			@QueryParam("rangeStart") String rangeStart,
			@QueryParam("rangeEnd") String rangeEnd,
			@QueryParam("associateId") String associateId, 
			@QueryParam("rolUser") String rolUser,
			@QueryParam("batchHistory") Integer batchHistory){
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		//TransactionConsultMin consult = new TransactionConsultMin();
		List<TransactionConsultMin> result = new ArrayList<TransactionConsultMin>();
		TransactionConsultList consultList = null;
		/*Filtro fechas: diarias, semanales y mensuales*/
		Date ahora = new Date();
		logger.info("ahora {}", ahora);
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = formateador.format(ahora);
		logger.info("Fecha {}",fecha);

		Calendar week = Calendar.getInstance();
		Calendar month = Calendar.getInstance();
		
		week.setTime(ahora);
		month.setTime(ahora);
		
		week.add(Calendar.DATE, -week.get(Calendar.DAY_OF_WEEK)+1);
		month.add(Calendar.DATE, -month.get(Calendar.DAY_OF_MONTH)+1);
		
		String dateWeek = formateador.format(week.getTime());
		String dateMonth = formateador.format(month.getTime());
		String tipo =null;
		Query merchantType =null;
		Query merchantId = null;
		int merchant_id=0;
		try {
			if(rolUser.equals("C") || rolUser.equals("c")){
				tipo = "simple";
				logger.info("Filtro todas las transacciones del comercio");
				merchantType = session.createQuery("SELECT merchantType FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'");
				merchantId = session.createQuery("SELECT id FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'"); 
				tipo = (String) merchantType.uniqueResult();
				merchant_id = (int) merchantId.uniqueResult();
			}
		} catch (Exception e1) {
			tipo = "none";
		}
		
		try {			
			String sql = "SELECT t.txn_date, bank.name as banco, bank.rif as bankRif, m.name as comercio, m.rif as merchantRif, "
			   	    + "SUBSTR((SELECT tpt.param_value FROM transaction_parameters tpt WHERE tpt.transaction_id=t.id and tpt.param_name='terminal_id' and tpt.param_type='request'),6,8) as terminal,"
			   	    + "(SELECT tpa.param_value FROM transaction_parameters tpa WHERE tpa.transaction_id=t.id and tpa.param_name='affiliated_id' and tpa.param_type='request') as affiliado,"
			   	    + "tp_pan.param_value as pan, "
				    + "(SELECT tpca.param_value FROM transaction_parameters tpca WHERE tpca.transaction_id=t.id and tpca.param_name='auth_code' and tpca.param_type='response') as cod_apro,"
				    + "cast(bh.BATCH_NUMBER as varchar(10)) as number_lote_hist, cast(cb.BATCH_NUMBER as varchar(10)) as number_lote_curr, "
				//    + "btc.BATCH_TYPE_DESC as BATCH_TYPE_DESC_Current, bth.BATCH_TYPE_DESC as BATCH_TYPE_DESC_history, "
				    + "(SELECT tpr.param_value FROM transaction_parameters tpr WHERE tpr.transaction_id=t.id and tpr.param_name='pos_ref' and tpr.param_type='request') as Refe,"
				    + "(SELECT tpt.param_value FROM transaction_parameters tpt WHERE tpt.transaction_id=t.id and tpt.param_name='trace' and tpt.param_type='request') as trace,"
				    + "t.amount,"
				    + "(SELECT tpcr.param_value FROM transaction_parameters tpcr WHERE tpcr.transaction_id=t.id and tpcr.param_name='ans_code' and tpcr.param_type='response') as cod_res"
				    + " FROM transaction t"
				    + " left join BATCH_HISTORY bh on t.BATCH_HISTORY_ID=bh.id "
				    + " left join CURRENT_BATCH cb on t.CURRENT_BATCH_ID=cb.id "
				    + " inner join transaction_parameters tp_pan on t.id=tp_pan.transaction_id "
				    + " INNER JOIN terminal ter ON ter.id=t.terminal_id"
				    + " INNER JOIN affiliation aff ON aff.id=ter.affiliation_id"
				    + " INNER JOIN acquirer ac ON ac.affiliation_id=aff.id"
				    + " INNER JOIN bank bank ON bank.id=ac.bank_id"
				    + " INNER JOIN merchant m ON m.id=aff.merchant_id"
				    + " INNER JOIN transaction_type tpy ON t.transaction_type_id=tpy.id"
				    + " INNER JOIN transaction_parameters tp ON t.id=tp.transaction_id "
				    + "INNER JOIN transaction_parameters tp2 ON t.id=tp2.transaction_id "
				    + "WHERE (t.transaction_status=0 or t.transaction_status=7) and (tpy.report_validation=1) and (tp_pan.param_name='pan' or tp_pan.param_name='track_2')"
				    + "and (tp.param_name='ans_code' and tp.param_value='00') "
				    + "and (tp2.param_name='affiliated_id' and tp2.param_value=ac.merchant_id_code)";
			logger.info("sql antes del if {}", sql);
			//logger.info("merchantId.uniqueResult() {}", merchantId.uniqueResult());
			/** Filtro de los comercios simples o multicomercios **/
			try {
				if(tipo.equals("simple")){
					logger.info("Comercio simple");
					sql= sql + " and (m.id=" + merchant_id + ")";
				}else if(tipo.equals("multi")){
					logger.info("Comercio multi");
					sql= sql + " and (m.parent_merchant_id=" + merchant_id + ")";
				}
			} catch (NullPointerException exNull1) {
				logger.info("tipo de comercio null");
			}
			/** Filtro para las consultas de las transacciones por adquiriente **/
			try {
				/** Filtros para las consultas de las transacciones del adquiriente **/
				if(rolUser.equals("A") || rolUser.equals("a")){
					logger.info("Filtro todas las transacciones por adquiriente");
					sql= sql + " and ac.bank_id="+associateId;
				}
			} catch (NullPointerException exNull2) {
				logger.info("adquiriente null");
			}
			/** Filtros para las consultas de las transacciones **/
			try {
				if(consultType.equals("d") || consultType.equals("D")){
					logger.info("Filtro todas las transacciones diarias");
					sql= sql + " and TO_CHAR(t.txn_date, 'YYYY-MM-DD')='"+fecha+"' order by t.txn_date desc";
				}else if(consultType.equals("s") || consultType.equals("S")){
					logger.info("Filtro todas las transacciones semanales");
					sql = sql + " and TO_CHAR(t.txn_date, 'YYYY-MM-DD')>='"+dateWeek+"' and TO_CHAR(t.txn_date, 'YYYY-MM-DD')<='"+fecha+"' order by t.txn_date desc";
				}else if(consultType.equals("m") || consultType.equals("M")){
					logger.info("Filtro todas las transacciones mensuales");
					sql = sql + " and TO_CHAR(t.txn_date, 'YYYY-MM-DD')>='"+dateMonth+"' and TO_CHAR(t.txn_date, 'YYYY-MM-DD')<='"+fecha+"' order by t.txn_date desc";
				}else if(consultType.equals("im") || consultType.equals("IM") && rangeStart!=null && rangeEnd!=null){
					logger.info("Filtro todas las transacciones intervalos de monto");
					logger.info("Monto inicial: {}", rangeStart);
					logger.info("Monto final: {}", rangeEnd);
					sql = sql + " and t.amount>="+rangeStart+" and t.amount<="+rangeEnd +" order by t.txn_date desc";
				}else if(consultType.equals("id") || consultType.equals("ID") && rangeStart!=null && rangeEnd!=null){
					logger.info("Filtro todas las transacciones intervalos de fechas");
					logger.info("fecha inicial: {}", rangeStart);
					logger.info("fecha final: {}", rangeEnd);
					sql = sql + " and TO_CHAR(t.txn_date, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR(t.txn_date, 'YYYY-MM-DD')<='"+rangeEnd+"' order by t.txn_date desc";
				}else{
					/** Consulta de transacciones sin filtros **/
					sql = sql + " order by t.txn_date desc FETCH NEXT 500 ROWS ONLY";
					
				}
			} catch (NullPointerException exNull2) {
				logger.info("tipo de consulta null");
				//sql = sql + " order by t.txn_date desc FETCH NEXT 500 ROWS ONLY";
				try {
					logger.info("Antes del if batchHistory");
					if(!batchHistory.equals(null) || batchHistory != null){
						sql = sql.replace(" FROM transaction t ", ", bth.BATCH_TYPE_DESC as BATCH_TYPE_DESC_history FROM transaction t ");
						sql = sql.replace(" WHERE ", " inner join batch_type bth on bth.id=bh.batch_type_id WHERE ");
						sql = sql + " and t.BATCH_HISTORY_ID = " + batchHistory + " order by t.txn_date asc";
						logger.info("SQL: {}", sql);
					}
				} catch (Exception e) {
					logger.info("Catch de batchHistory");
					sql = sql + " order by t.txn_date desc FETCH NEXT 500 ROWS ONLY";
				}
			}
			logger.info("sql {}", sql);
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			logger.info("List {}", data);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			/** Setear los datos de la consulta al minifield de las transacciones **/
			data.forEach(elem->{
				TransactionConsultMin consult = new TransactionConsultMin();
				consult.setTxnDate(format.format(((Timestamp)elem.get("TXN_DATE")).getTime()));
				consult.setBankName((String) elem.get("BANCO"));
				consult.setBankRif((String) elem.get("BANKRIF"));
				consult.setMerchantName((String) elem.get("COMERCIO"));
				consult.setMerchantRif((String) elem.get("MERCHANTRIF"));
				consult.setTerminal((String)elem.get("TERMINAL"));
				consult.setAffiliatedId((String)elem.get("AFFILIADO"));
				consult.setPan((String) elem.get("PAN"));
				consult.setAuthCode((String) elem.get("COD_APRO"));
				consult.setLoteHist((String) elem.get("NUMBER_LOTE_HIST"));
				consult.setLoteCurr((String) elem.get("NUMBER_LOTE_CURR"));
				consult.setRef((String) elem.get("REFE"));
				consult.setTrace((String) elem.get("TRACE"));
				consult.setAmount(String.valueOf(elem.get("AMOUNT")));
				consult.setAnsCode((String) elem.get("COD_RES"));
				consult.setCardTypeHist((String) elem.get("BATCH_TYPE_DESC_HISTORY"));
				//consult.setCardTypeCurr((String) elem.get("BATCH_TYPE_DESC_CURRENT"));
				logger.info("consult {}", consult.getAmount());
				result.add(consult);
			});
			
			/** Agregar el objeto min al wrapper **/
			consultList = new TransactionConsultList(
					result
					);
			consultList.getTransactionConsult().forEach(action->{
				logger.info("action {}", action);
			});
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		          
        
        return consultList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : consultList;
    	
	}

	/**
	 * Lista de lotes cerrados 
	 * 
	 * @param rangeStart
	 * @param rangeEnd
	 * @param associateId
	 * @param rolUser
	 * @param batchNumber
	 * @param numberAffiliate
	 * @return
	 */
	@Path("/batchList")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getBatchs(@QueryParam("rangeStart") String rangeStart,
			@QueryParam("rangeEnd") String rangeEnd,
			@QueryParam("associateId") String associateId, 
			@QueryParam("rolUser") String rolUser,
			@QueryParam("consultType") String consultType,
			@QueryParam("batchNumber") Integer batchNumber,
			@QueryParam("affiliateNumber") String affiliateNumber) throws NullPointerException{
		
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		List<BatchConsultMin> result = new ArrayList<BatchConsultMin>();
		BatchConsultList consultList = null;
		String tipo =null;
		Query merchantType =null;
		Query merchantId = null;
		int merchant_id=0;
		
		try {
			
			String sql = "SELECT b.id as id, b.BATCH_NUMBER as batch_number, b.TOTAL_TRX as total_trx,"
					+ "b.TOTAL_AMOUNT as total_amount, TO_CHAR(b.closing_timestamp, 'DD/MM/YYYY HH24:MI:SS.FF' ) as CLOSING_TIMESTAMP, bt.batch_type_name as batch_type, "
					+ "SUBSTR(tp.PARAMETER_VALUE,6,8) as terminal_bank, p.name as payment_channel_name, "
					+ "ac.merchant_id_code as affiliate_num, ac.bank_id as bank_id, bank.name as bank_name, "
					+ "af.merchant_id as merchant_id, m.merchant_guid as merchant_guid, m.rif as merchant_rif, m.name as merchant_name "
					+ "FROM BATCH_HISTORY b "
					+ "inner join batch_type bt on bt.id=b.batch_type_id "
					+ "inner join terminal t on t.id=b.terminal_id "
					+ "inner join TERMINAL_PARAMETER tp on  tp.terminal_id=t.id "
					+ "inner join acquirer ac on ac.id=tp.acquirer_id "
					+ "inner join bank bank on bank.id=ac.bank_id "
					+ "inner join affiliation af on af.id=ac.affiliation_id "
					+ "inner join payment_channel p on p.id=af.payment_channel_id "
					+ "inner join merchant m on m.id=af.merchant_id "
					+ "WHERE TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"' ";
			
			Calendar fecha = Calendar.getInstance();
			String fechaS ="";
			int m = fecha.get(Calendar.MONTH) + 1;
			int y = fecha.get(Calendar.YEAR);
			if (m<10){
				fechaS = String.valueOf(y) + "-0" + String.valueOf(m);
			}else{
				fechaS = String.valueOf(y) + "-" + String.valueOf(m);
			}
			logger.info("fechaS: {}",fechaS);
			
			/** Filtro de los comercios simples o multicomercios **/
			try {
				if(rolUser.equals("C") || rolUser.equals("c")){
					tipo = "simple";
					logger.info("Filtro todas las transacciones del comercio");
					merchantType = session.createQuery("SELECT merchantType FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'");
					merchantId = session.createQuery("SELECT id FROM Merchant as m WHERE m.merchantGuid='"+associateId+"'"); 
					tipo = (String) merchantType.uniqueResult();
					merchant_id = (int) merchantId.uniqueResult();
				}else{
					tipo = "none";
				}
				
				if(tipo.equals("simple")){
					logger.info("Comercio simple");
					sql = sql.replace("WHERE TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"'", "WHERE ");
					sql= sql + " (m.merchant_guid='" + associateId+ "')";
					if(rangeStart!=(null) && rangeEnd!=(null)){
						logger.info("No tiene parametros de lote y afiliado, pero si de fecha");
						sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
					}else{
						sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
					}
				}else if(tipo.equals("multi")){
					logger.info("Comercio multi");
					sql = sql.replace("WHERE TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"'", "WHERE ");
					sql= sql + " (m.parent_merchant_id=" + merchant_id + ")";
					if(rangeStart!=(null) && rangeEnd!=(null)){
						logger.info("No tiene parametros de lote y afiliado, pero si de fecha");
						sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
					}else{
						sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
					}
				}
			} catch (NullPointerException exNull1) {
				logger.info("tipo de comercio null");
			}
			
			/** Filtro para las consultas de las transacciones por adquiriente **/
			try {
				if(rolUser.equals("A") || rolUser.equals("a")){
					logger.info("Filtro todas las transacciones por adquiriente");
					sql = sql.replace("WHERE TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"'", "WHERE ");
					sql= sql + " ac.bank_id="+associateId;
					
					/*
					 * Consulta por numero de lote (debe llevar ademas el numero de afiliado)
					 */
					try {
						if(consultType.equals("lote")){
							logger.info("--- Consultar un lote en especifico de un adquiriente ---");
							if(!batchNumber.equals(null) && !affiliateNumber.equals(null)){
								sql = sql + " and b.BATCH_NUMBER="+batchNumber+" and ac.merchant_id_code='"+affiliateNumber+"'";
							}
						}
						if(consultType.equals("afiliado")){
							logger.info("--- Consultar lotes por afiliado de un adquiriente ---");
							if(affiliateNumber!=(null)){
								sql = sql + " and ac.merchant_id_code='"+affiliateNumber+"'";
								if(rangeStart!=(null) && rangeEnd!=(null)){
									logger.info("Por afiliado y rango de Fechas para el Admin o Master");
									sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
								}else{
									sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
								}
							}
						}
						
					} catch (Exception e) {
						//e.printStackTrace();
						if(rangeStart!=(null) && rangeEnd!=(null)){
							logger.info("No tiene parametros de lote y afiliado, pero si de fecha");
							sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
						}else{
							sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
						}
					}
					
				}
				
			} catch (NullPointerException exNull2) {
				logger.info("adquiriente null");
			}
			
			try {
				if(consultType.equals("lote")){
					logger.info("--- Consultar un lote en especifico usuario administrador o master ---");
					sql = sql.replace("WHERE TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='null' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='null'", "WHERE ");
					if(!batchNumber.equals(null) && !affiliateNumber.equals(null)){
						sql = sql + " b.BATCH_NUMBER="+batchNumber+" and ac.merchant_id_code='"+affiliateNumber+"'";
					}
				}
				
				if(consultType.equals("afiliado")){
					logger.info("--- Consultar lotes por afiliado ---");
					if(affiliateNumber!=(null)){
						sql = sql + " and ac.merchant_id_code='"+affiliateNumber+"'";
						if(rangeStart!=(null) && rangeEnd!=(null)){
							logger.info("Por afiliado y rango de Fechas para el Admin o Master");
							sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
						}else{
							sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
						}
					}
				}
			} catch (Exception e) {
				//e.printStackTrace();
				if(rangeStart!=(null) && rangeEnd!=(null)){
					logger.info("No tiene parametros de lote y afiliado, pero si de fecha");
					sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')>='"+rangeStart+"' and TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM-DD')<='"+rangeEnd+"')";
				}else{
					sql = sql + " and (TO_CHAR( b.CLOSING_TIMESTAMP, 'YYYY-MM')='"+ fechaS +"')";
				}
			}
			
			sql = sql +" order by b.closing_timestamp desc"; //Ordenar la consulta por la fecha
			logger.info("SQL a ejecutar {}",sql);
			
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			
			data.forEach(elem->{
				BatchConsultMin consult = new BatchConsultMin();
				consult.setId(((BigDecimal) elem.get("ID")).intValue());
				consult.setAffiliateNumber((String) elem.get("AFFILIATE_NUM"));
				consult.setBatchNumber(((BigDecimal) elem.get("BATCH_NUMBER")).intValue());
				consult.setTerminalBank((String) elem.get("TERMINAL_BANK"));
				consult.setBatchType((String) elem.get("BATCH_TYPE"));
				consult.setBatchDate((String) elem.get("CLOSING_TIMESTAMP"));
				consult.setPaymentChannel((String) elem.get("PAYMENT_CHANNEL_NAME"));
				consult.setTotalTrx(((BigDecimal) elem.get("TOTAL_TRX")).intValue());
				consult.setBankId(((BigDecimal) elem.get("BANK_ID")).intValue());
				consult.setBank((String) elem.get("BANK_NAME"));
				consult.setMerchantId(((BigDecimal) elem.get("MERCHANT_ID")).intValue());
				consult.setMerchantGuid((String) elem.get("MERCHANT_GUID"));
				consult.setMerchantRif((String) elem.get("MERCHANT_RIF"));
				consult.setMerchantName((String) elem.get("MERCHANT_NAME"));
				consult.setTotalAmount(((BigDecimal) elem.get("TOTAL_AMOUNT")).doubleValue());
				result.add(consult);
			});
			
			/** Agregar el objeto min al wrapper **/
			consultList =  new BatchConsultList(
					result
					);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		return consultList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : consultList;
  
	}
	
	/**
	 * Resumen de lotes
	 * 
	 * @param batchNumber
	 * @return
	 * @throws NullPointerException
	 */
	@Path("/batchSummary")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getBatchSummary(@QueryParam("batchNumber") Integer batchNumber) throws NullPointerException{
		Session session = null;
		session = HibernateUtil.getSessionFactory().openSession();
		List<BatchSummaryConsultMin> result = new ArrayList<BatchSummaryConsultMin>();
		BatchSummaryConsultList consultList = null;
		
		try {
			String sql = "SELECT bt.BATCH_TYPE_DESC as type, count(*) as nro_trx, sum(t.amount) as acum_trx "
						+ "FROM BATCH_HISTORY b "
						+ "inner join batch_type bt on bt.id=b.batch_type_id "
						+ "inner join transaction t on t.batch_history_id=b.id "
						+ "WHERE (t.TRANSACTION_STATUS = 0 or t.TRANSACTION_STATUS = 7) and (t.TRANSACTION_TYPE_ID = 11 or t.TRANSACTION_TYPE_ID = 3) and t.TRANSACTION_RESPONSE_ID=38 and b.id=" + batchNumber + " GROUP BY bt.BATCH_TYPE_DESC ORDER BY bt.BATCH_TYPE_DESC";
			logger.info("SQL a ejecutar {}",sql);
			
			SQLQuery queryParam = session.createSQLQuery(sql);
			queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Map<String, Object>> data = queryParam.list();
			
			data.forEach(elem->{
				BatchSummaryConsultMin consult = new BatchSummaryConsultMin();
				consult.setProduct((String) elem.get("TYPE"));
				consult.setTransactions(BigInteger.valueOf(((BigDecimal) elem.get("NRO_TRX")).intValue()));
				consult.setTotalAmount(((BigDecimal) elem.get("ACUM_TRX")).doubleValue());
				result.add(consult);
			});
			
			/** Agregar el objeto min al wrapper **/
			consultList =  new BatchSummaryConsultList(
					result
					);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("exception {}", e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		
		return consultList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ninguna transaccion", "Recurso no encontrado") : consultList;
	}
}
