package vpago.dataapi.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;

import vpago.dataapi.classes.APIResponse;
import vpago.dataapi.classes.Country;
import vpago.dataapi.classes.Response;
import vpago.dataapi.classes.wrappers.CountryList;
import vpago.dataapi.db.HibernateUtil;
import vpago.dataapi.types.ResponseType;

/**
 * Contiene todos los metodos para los servicios relacionados al recurso Country (Pais)
 */
@Path("/country")
public class CountryResource extends APIResponse {
	
	/**
	 * Listar Paises: Construye una lista con todas las entidades de tipo Pais (Country) almacenadas en la base de datos
	 * 
	 * @return La lista de todas las entidades de tipo Pais (Country)
	 */
	@SuppressWarnings("unchecked")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getAllCountries() { 
		Session session = null;
		CountryList countryList = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String hsqlquery = "select new vpago.dataapi.classes.minified.CountryMin(id, countryName, iso31661Code) from Country as country";
			countryList = new CountryList(session.createQuery(hsqlquery).list());
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return countryList == null ? new Response(ResponseType.RESOURCENOTFOUND, "No se encontro ningun pais", "Recurso no encontrado") : countryList;
	}
	
	/**
	 * Consultar Pais: Consultar los datos de una entidad de tipo Pais (Country)
	 * 
	 * @param countryId Identificador del recurso de tipo Pais (Country)
	 * 
	 * @return Datos de la entidad de tipo Pais (Country) identificada por <strong>countryId</strong>
	 */
	@Path("/{country_id: \\d+ }")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public APIResponse getCountry(@PathParam("country_id") int countryId) { 
		Session session = null;
		Country country = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			country = (Country)session.get(Country.class, countryId);	
		} catch (Exception e) {
			logger.error(e.toString());
			return new Response(ResponseType.GENERALERROR, "Ha ocurrido un error al procesar la solicitud", e.getMessage());
		} finally {
			if(session != null) session.close();
		}
		return country == null ? new Response(ResponseType.RESOURCENOTFOUND, "El pais solicitado no existe", "Identificador no encontrado") : country;
	}
}
