alter table TEMPLATE_FIELD add column encrypted boolean default false;
alter table TRANSACTION_PARAMETERS add column encrypted boolean default false;