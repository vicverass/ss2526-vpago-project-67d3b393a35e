alter table assigned_device drop column serial_number;
alter table assigned_device add column serial_number_device varchar(100);
alter table assigned_device add column serial_number_simcard varchar(100);
alter table assigned_device add column operator_name varchar(200);