alter table template_field add fixed_value varchar(100);
alter table template_field add load_func varchar(100);
alter table template_field add store_func varchar(100);
alter table template_field add returnable boolean default false not null;
alter table template_field add mandatory boolean default true not null;
alter table iso_template add header varchar(255);
alter table iso_template drop column file_name;