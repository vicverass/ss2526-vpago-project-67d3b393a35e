alter table transaction add batch_history_id int default null;
alter table transaction add batch_number varchar(20) default null;
alter table transaction add ref_number varchar(20) default null;
alter table terminal add trace_counter int default null;
alter table terminal add current_batch_id int default null;