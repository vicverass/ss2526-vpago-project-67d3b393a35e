alter table address add name varchar(100);
alter table address add location varchar(100);
alter table address add property varchar(100);
alter table address add postal_zone varchar(50);
alter table address alter column line1 rename to parish;
alter table address alter column line2 rename to municipality;