alter table BATCH_HISTORY add column synchronized boolean default false;
alter table BATCH_HISTORY add column synchronization_date timestamp with time zone;