alter table transaction_parameters alter column param_value varchar(300);
alter table transaction_parameters alter column param_value set not null;