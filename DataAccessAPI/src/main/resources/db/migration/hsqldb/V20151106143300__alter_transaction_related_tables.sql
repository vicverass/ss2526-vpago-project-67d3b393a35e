alter table transaction add sec_token varchar(64) unique not null;
alter table transaction_type add transaction_response_type int;
alter table payment_channel add code_name varchar(128) not null;
alter table transaction_response add description varchar(255);
alter table iso_template add file_name varchar(255) not null;
alter table template_field add encoder varchar(255);
alter table template_field add decoder varchar(255);
alter table template_field add storable boolean default false not null;