create table device_to_assign (
    id int generated by default as identity (start with 1),
    serial_number varchar(255),
    device_id int not null,
    device_type_id int not null,
    device_available boolean default true not null,
    active boolean default true not null
);