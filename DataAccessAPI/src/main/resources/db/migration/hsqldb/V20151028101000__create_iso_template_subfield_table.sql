create table iso_template_subfield (
    iso_template_id int not null,
    template_subfield_id int not null,
    parameter_name varchar(255) not null
);