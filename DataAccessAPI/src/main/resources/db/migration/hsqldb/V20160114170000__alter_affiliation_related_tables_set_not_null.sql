alter table terminal alter column affiliation_id set not null;
alter table acquirer alter column affiliation_id set not null;
alter table transaction_type alter column template_message_id set not null;
alter table rule_set alter column affiliation_id set not null;