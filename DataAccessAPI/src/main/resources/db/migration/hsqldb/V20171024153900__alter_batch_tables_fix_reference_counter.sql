alter table current_batch add reference_counter int default 1;
alter table batch_history drop reference_counter;