alter table ADDITIONAL_VOUCHER_FIELDS add transaction_type_id int default null;
alter table ADDITIONAL_VOUCHER_FIELDS add batch_type_id int default null;
alter table TRANSACTION_TYPE add state_print boolean default false;
alter table TRANSACTION_TYPE add voucher_name varchar(30) default null;