create table terminal_category (
    id int generated by default as identity (start with 1),
    name varchar(255),
    description varchar(512),
);