create table engine_config (
    id int generated by default as identity (start with 1),
    affiliated_counter varchar(50),
    terminal_counter varchar(50)
);
insert into engine_config (id, affiliated_counter, terminal_counter) values (default, '0000000000', '00000000');