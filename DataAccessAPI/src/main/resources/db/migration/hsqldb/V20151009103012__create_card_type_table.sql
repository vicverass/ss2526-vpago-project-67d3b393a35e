create table card_type (
    id int generated by default as identity (start with 1),
    name varchar(100) not null
);