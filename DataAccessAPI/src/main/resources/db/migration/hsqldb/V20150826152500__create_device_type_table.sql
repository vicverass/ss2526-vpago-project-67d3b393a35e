create table device_type (
    id int generated by default as identity (start with 1),
    name varchar(255)
);