alter table template_field alter column account_attr rename to acquirer_attr;
alter table rule alter column account_id rename to acquirer_id;