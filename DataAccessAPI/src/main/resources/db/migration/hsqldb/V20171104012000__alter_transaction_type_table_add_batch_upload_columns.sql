alter table transaction_type add uploadable_type boolean default false;
alter table transaction_type add batch_upload_type int;
alter table transaction_response add unbalance_code boolean default false;