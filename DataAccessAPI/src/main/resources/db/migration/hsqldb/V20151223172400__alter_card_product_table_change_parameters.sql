alter table card_product add bin_id int not null;
alter table card_product add product_type varchar(20) not null;
alter table card_product alter column bank_id set null;