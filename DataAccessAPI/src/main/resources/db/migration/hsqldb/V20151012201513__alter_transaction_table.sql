alter table transaction alter column terminal_bank_id set null;
alter table transaction alter column merchant_id_code set null;
alter table transaction alter column transaction_response_id set null;
alter table transaction alter column card_product_id set null;
alter table transaction drop column txn_time;