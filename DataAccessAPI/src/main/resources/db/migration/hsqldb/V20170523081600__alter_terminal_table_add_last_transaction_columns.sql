alter table terminal add last_appr_trx_id int default null;
alter table terminal add last_exec_trx_id int default null;
alter table terminal add pending_reverse boolean default false;
alter table terminal add pending_confirmation boolean default false;