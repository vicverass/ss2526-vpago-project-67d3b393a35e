create table terminal (
    id serial,
    terminal_guid varchar(16) unique not null,
    access_key varchar(16) unique not null,
    active boolean not null,
    group_index int not null,
    terminal_group_id int not null,
    location_id int not null,
    assigned_device_id int
);