create table address (
    id serial,
    city varchar(100),
    state varchar(100),
    country varchar(100),
    line1 varchar(100),
    line2 varchar(100)
);  