create table terminal_group (
    id serial,
    name varchar(255) not null,
    description varchar(512),
    terminal_category_id int not null,
    merchant_id int not null
);