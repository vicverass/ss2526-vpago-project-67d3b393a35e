create table location (
    id serial,
    address_id int not null,
    contact_id int not null,
    phone varchar(20),
    email varchar(255)
);