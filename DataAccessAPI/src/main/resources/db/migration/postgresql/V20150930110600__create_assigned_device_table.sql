create table assigned_device (
    id serial,
    serial_number varchar(255),
    device_id int not null
);