create table terminal_category (
    id serial,
    name varchar(255),
    description varchar(512)
);