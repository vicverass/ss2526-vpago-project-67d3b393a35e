create table rule (
    id serial,
    display_name varchar(255) not null, 
    description varchar(512),
    time_frame_id int not null,
    rule_type varchar(255) not null
);