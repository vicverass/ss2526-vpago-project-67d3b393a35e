create table rule_set_rule (
    id serial,
    rule_set_id int not null,
    rule_id int not null
);