create table rule_set (
    id serial,
    display_name varchar(255) not null,
    description varchar(512),
    merchant_id int not null
);