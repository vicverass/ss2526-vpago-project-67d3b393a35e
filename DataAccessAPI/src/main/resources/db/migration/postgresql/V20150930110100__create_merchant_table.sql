create table merchant (
    id serial,
    merchant_guid varchar(64) unique not null,
    name varchar(100) not null,
    rif varchar(50) unique not null,
    location_id int not null
);