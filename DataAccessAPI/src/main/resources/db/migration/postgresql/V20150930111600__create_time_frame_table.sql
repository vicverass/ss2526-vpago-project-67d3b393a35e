create table time_frame (
    id serial,
    week_day varchar(16) not null,
    start_time time not null,
    end_time time not null
);