create table contact (
    id serial,
    name varchar(255),
    lastname varchar(255),
  	phone1 varchar(20),
  	phone2 varchar(20),
  	email varchar(255)  
);