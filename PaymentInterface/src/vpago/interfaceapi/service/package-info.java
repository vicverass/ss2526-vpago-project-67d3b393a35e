/**
 * Contiene todas las clases destinadas a implementar los metodos para los diferentes servicios que ofrece la Interfaz de Pago
 */
package vpago.interfaceapi.service;