package vpago.interfaceapi.service;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.transactions.TransactionHandlerLocal;
import vpago.core.transactions.classes.TransactionData;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.exceptions.AlreadyInvertedTransaction;
import vpago.core.transactions.exceptions.CleanBatch;
import vpago.core.transactions.exceptions.ConnectionFailure;
import vpago.core.transactions.exceptions.DisabledMerchant;
import vpago.core.transactions.exceptions.DisabledTerminal;
import vpago.core.transactions.exceptions.DuplicateTransaction;
import vpago.core.transactions.exceptions.InvalidBatch;
import vpago.core.transactions.exceptions.InvalidSerialNumber;
import vpago.core.transactions.exceptions.InvalidTransaction;
import vpago.core.transactions.exceptions.InvalidTransactionType;
import vpago.core.transactions.exceptions.InvertTransaction;
import vpago.core.transactions.exceptions.NoResponse;
import vpago.core.transactions.exceptions.NonExistentPaymentChannel;
import vpago.core.transactions.exceptions.NonExistentTerminal;
import vpago.core.transactions.exceptions.PendingConfirmation;
import vpago.core.transactions.exceptions.PendingInverse;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.exceptions.TransactionException;
import vpago.core.transactions.classes.CoreResponse;
import vpago.core.transactions.classes.FunctionData;
import vpago.core.transactions.classes.FunctionResponse;
import vpago.interfaceapi.classes.FunctionMessage;
import vpago.interfaceapi.classes.PaymentChannel;
import vpago.interfaceapi.classes.PaymentMessage;
import vpago.interfaceapi.classes.StatusMessage;
import vpago.interfaceapi.classes.TransactionStatus;
import vpago.interfaceapi.classes.Transaction;
import vpago.interfaceapi.classes.TransactionMessage;
import vpago.interfaceapi.classes.TransactionTerminal;
import vpago.interfaceapi.types.MessageType;

/**
 * Contiene todos los metodos para los servicios relacionados a la solicitud de transacciones
 */
@Path("/transaction")
public class TransactionResource {
	private static final Logger logger = LoggerFactory.getLogger(TransactionResource.class);
	
	@EJB(beanName="TransactionHandler")
	private TransactionHandlerLocal transactionHandler;
	
	/**
	 * Servicio web que se invoca con los parametros correspondientes para 
	 * solicitar la ejecucion de una transaccion
	 * 
	 * @param transactionReq Objeto de tipo <strong>TransactionMessage</strong> con los parametros requeridos para
	 * la ejecucion de una transaccion
	 * 
	 * @return El mensaje de respuesta de la transaccion en el formato indicado en el encabezado Accept de la 
	 * solicitud
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public PaymentMessage execute(TransactionMessage transactionReq){
		long lStartTime = System.nanoTime();
		logger.trace("#################################################-NUEVA SOLICITUD-#################################################");
		logger.debug("TransactionMessage: {}", transactionReq);
		CoreResponse coreResponse = null;
		//Construimos la estructura con los parametros proporcionados
		//a través del servicio web
		//TransactionHandler transactionHandler = new TransactionHandler();
		TransactionData transactionData = null;
		FunctionData functionData = null;
		if(transactionReq.getFunction() == null) {
			transactionData = new TransactionData(
				transactionReq.getTerminal().getTerminalId(),
				transactionReq.getPaymentChannel().getChannelCode(),
				transactionReq.getTransaction().getType(),
				transactionReq.getTransaction().getControlToken(),
				transactionReq.getTransaction().getSecToken(),
				transactionReq.getTransaction().getTransactionParameters().getOrDefault("pan", null),
				transactionReq.getTransaction().getSerial(),
				transactionReq.getTransaction().getTransactionParameters(),
				transactionReq.getTransaction().getReferenceNum(),
				transactionReq.getTransaction().getTimestamp());
		} else {
			
			try {
				//Llamar a handler con el tipo de funcion
				functionData = new FunctionData(
						transactionReq.getTerminal().getTerminalId(),
						transactionReq.getPaymentChannel().getChannelCode(),
						transactionReq.getFunction().getType(),
						null
						);
				FunctionResponse functionRes = transactionHandler.executeFunction(functionData);
				return new FunctionMessage(functionRes != null ? functionRes.getFunctionParameters() : null);
			} catch (TransactionException e) {
				logger.error("{}", e);
				return new StatusMessage(MessageType.TRANSACTION_ERROR);
			}
		}
				
		//Se solicita la ejecucion de la transaccion 
		try {
			String refNum = transactionReq.getTransaction().getReferenceNum();
			logger.debug("Referencia de transaccion: {}", refNum);
			Integer prevTranId = refNum != null ? 
					transactionHandler.getIdTransactionFromRef(transactionReq.getTerminal().getTerminalId(), 
																refNum,
																transactionData,
																transactionReq.getPaymentChannel().getChannelCode()) :
					null
						;
			coreResponse = refNum == null ?
					transactionHandler.executeTransaction(transactionData) :
					(prevTranId != -1) ? transactionHandler.executeTransaction(transactionData, prevTranId)	: null
						;
			if(coreResponse == null) return new StatusMessage(MessageType.TRANSACTION_ERROR);
		} catch (InvalidTransactionType e) {
			return new StatusMessage(MessageType.INVALID_TRANSACTION_TYPE);
		} catch (NonExistentPaymentChannel e) {
			return new StatusMessage(MessageType.NON_EXISTENT_PAYMENT_CHANNEL);
		} catch (NonExistentTerminal e) {
			return new StatusMessage(MessageType.NON_EXISTENT_TERMINAL);
		} catch (DuplicateTransaction e) {
			return new StatusMessage(MessageType.DUPLICATE_TRANSACTION);
		} catch (ServiceUnavailable e) {
			return new StatusMessage(MessageType.SERVICE_UNAVAILABLE);
		} catch (NoResponse e) {
			try {
				logger.debug("Excepcion de NoResponse");
				TransactionType inverseType = transactionHandler.getTransactionInverse(
						transactionReq.getTransaction().getType(), 
						transactionReq.getPaymentChannel().getChannelCode());
				logger.debug("Inversa: {}", inverseType);
				transactionHandler.executeInverse(transactionData, inverseType);
			} catch (Exception ex) {
				return new StatusMessage(MessageType.TRANSACTION_ERROR);
			}
			return new StatusMessage(MessageType.NO_RESPONSE);
		} catch (ConnectionFailure e) {
			return new StatusMessage(MessageType.CONNECTION_FAILURE);
		} catch (InvalidTransaction e) {
			return new StatusMessage(MessageType.INVALID_TRANSACTION);
		} catch (DisabledMerchant e) {
			return new StatusMessage(MessageType.DISABLED_MERCHANT);
		} catch (DisabledTerminal e) {
			return new StatusMessage(MessageType.DISABLED_TERMINAL);
		} catch (PendingConfirmation e) {
			return new StatusMessage(MessageType.PENDING_CONFIRMATION);
		} catch (PendingInverse e) {
			return new StatusMessage(MessageType.PENDING_INVERSE);
		} catch (InvertTransaction e) {
			try {
				TransactionType inverseType = transactionHandler.getTransactionInverse(
						transactionReq.getTransaction().getType(), 
						transactionReq.getPaymentChannel().getChannelCode());
				logger.trace("Inversa: {}", inverseType);
				transactionHandler.executeInverse(transactionData, inverseType);
				coreResponse = e.getCoreResponse();
			} catch (Exception ex) {
				return new StatusMessage(MessageType.TRANSACTION_ERROR);
			}
			//return new StatusMessage(MessageType.INVERT_TRANSACTION);
		} catch (AlreadyInvertedTransaction e) {
			return new StatusMessage(MessageType.INVERTED_TRANSACTION);
		} catch (InvalidSerialNumber e) {
			return new StatusMessage(MessageType.INVALID_SERIAL_NUMBER);
		} catch (CleanBatch e) {
			return new StatusMessage(MessageType.CLEAN_BATCH);
		} catch (InvalidBatch e) {
			return new StatusMessage(MessageType.INVALID_BATCH);
		} catch (TransactionException e) {
			return new StatusMessage(MessageType.TRANSACTION_ERROR);
		} catch (Exception e) {
			return new StatusMessage(MessageType.UNEXPECTED_EXCEPTION);
		} catch (Error e) {
			return new StatusMessage(MessageType.UNEXPECTED_ERROR);
		}
		logger.debug("Respuesta de la transaccion: {}", coreResponse);
		
		//Eliminamos los parametros que no deben ser devueltos en el mensaje
		transactionReq.getTransaction().setHashCode(coreResponse.getTxnId());
		transactionReq.getTransaction().setTimestamp(coreResponse.getTimestamp());
		TransactionMessage transactionRes = new TransactionMessage(
				transactionReq.getTerminal(), 
				transactionReq.getTransaction(), 
				transactionReq.getPaymentChannel(),
				new TransactionStatus(coreResponse.getCode(),
									  coreResponse.getDescription(),
									  coreResponse.getStatusData(),
									  coreResponse.getAdditionalTopData(),
									  coreResponse.getAdditionalBottomData(),
									  coreResponse.isStatePrint()));
		transactionRes.getTerminal().setTerminalKey(null);
		transactionRes.getTransaction().setTransactionParameters(null);
		long lEndTime = System.nanoTime();
		long output = lEndTime - lStartTime;
        logger.info("Tiempo de la transaccion en motor: {} seg", (double)output / 1000000000);
		return transactionRes;
	}
	 
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public TransactionMessage getTest(){ 
		
		TransactionMessage tm = null;
		try {
			TransactionTerminal tt = new TransactionTerminal("5D5BB258", null);
			PaymentChannel pc = new PaymentChannel("CPAGO_CC");
//			TransactionTerminal tt = new TransactionTerminal("TerminalIDTest",null);
//			PaymentChannel pc = new PaymentChannel("PaymentChannelIDTest", null);
			logger.debug("TransactionTerminal: {}", tt);
			//TransactionStatus rc = new TransactionStatus("42", "Aprobado", null);
			TransactionStatus rc = null; 
			//Map<String,String> sd = new HashMap<String,String>();
			//sd.put("trace", "344534");
			//rc.setStatusData(sd);  
			 
			Map<String, String> testMap = new HashMap<String, String>();
			//Venta credito
			testMap.put("amount","400.50");
			testMap.put("pan","1234567812345678");
			testMap.put("ci","12345678");
			testMap.put("sec_cod","123");
			testMap.put("expires","1119");
			
			//Reverso
			//testMap.put("amount","400.50");
			//testMap.put("pan","1234567812345678");
			
			//EchoTest 
			
			Transaction t = new Transaction("4th8tgfb5gv8db8vn", "CREDIT_SALE", "123456", null, null, testMap, "", "");
			//Transaction t = new Transaction("ECHO_TEST",null, null);
			//t.setTxnId("2434rurhrr37ye3whe");
			
			tm = new TransactionMessage(tt, t, pc, rc);
			logger.debug("TransactionMessage: {}", tm);
			
			return tm;
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return tm;
	}
	
	@GET
	@Path("test")
	@Produces(MediaType.APPLICATION_JSON) 
	public TransactionStatus getString(){
		//TransactionHandler th = new TransactionHandler();
		//th.executeTransaction(new TransactionData());
		TransactionStatus rc = new TransactionStatus("42", "Aprobado", null,null,null, true);
		return rc;
	}
	
	@POST
	@Path("test")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public TransactionStatus postString(String requestData){
		logger.debug("requestData: {}", requestData);
		//TransactionHandler th = new TransactionHandler();
		//th.executeTransaction(new TransactionData(new IsoData(0x200)));
		TransactionStatus rc = new TransactionStatus("42", requestData, null,null,null, true);
		return rc; 
	}
	
}
