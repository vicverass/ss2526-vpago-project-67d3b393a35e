package vpago.interfaceapi.types;

/**
 * Tipo enumerado para representar los diferentes tipos de mensajes no transaccionales que puede retornar el motor
 */
public enum MessageType {

	TRANSACTION_ERROR("0042", "TRANSACTION_ERROR", "Error durante la ejecucion de la transaccion"),
	INVALID_TRANSACTION_TYPE("0043", "INVALID_TRANSACTION_TYPE", "El tipo de transaccion es invalido"),
	NON_EXISTENT_PAYMENT_CHANNEL("0044", "NON_EXISTENT_PAYMENT_CHANNEL", "El canal de pago no existe"),
	NON_EXISTENT_TERMINAL("0045","NON_EXISTENT_TERMINAL", "El terminal solicitado no existe" ),
	DUPLICATE_TRANSACTION("0046", "DUPLICATE_TRANSACTION", "Transaccion duplicada"),
	SERVICE_UNAVAILABLE("0047", "SERVICE_UNAVAILABLE", "El servicio no se encuentra disponible"),
	UNEXPECTED_EXCEPTION("0048", "UNEXPECTED_EXCEPTION", "Excepcion inesperada"),
	UNEXPECTED_ERROR("0049", "UNEXPECTED_ERROR", "Fallo inesperado de la plataforma"),
	NO_RESPONSE("0050", "NO_RESPONSE", "No se recibio respuesta de la transaccion"), 
	CONNECTION_FAILURE("0051", "CONNECTION_FAILURE", "Interrupcion en la conexion con el aprobador"),
	INVALID_TRANSACTION("0052", "INVALID_TRANSACTION", "Transaccion no valida"),
	DISABLED_MERCHANT("0053", "DISABLED_MERCHANT", "Comercio no habilitado para realizar transacciones"),
	DISABLED_TERMINAL("0054", "DISABLED_TERMINAL", "Terminal no habilitado para realizar transacciones"),
	PENDING_CONFIRMATION("0055", "PENDING_CONFIRMATION", "Pendiente la confirmacion de la transaccion"),
	PENDING_INVERSE("0056", "PENDING_INVERSE", "Pendiente la inversa o reverso de la transaccion"),
	INVERT_TRANSACTION("0057", "INVERT_TRANSACTION", "Transaccion invertida/reversada"),
	INVERTED_TRANSACTION("0058", "INVERTED_TRANSACTION", "La inversa de la transaccion a reversar ya fue aprobada"),
	INVALID_SERIAL_NUMBER("0059", "INVALID_SERIAL_NUMBER", "El serial del dispositivo no coincide con el registrado"),
	CLEAN_BATCH("0060", "CLEAN_BATCH", "El lote no tiene transacciones para cerrar"),
	INVALID_BATCH("0061", "INVALID_BATCH", "No se pudo determinar un lote para la transaccion")
	;
	
	/** Codigo numerico del mensaje **/
	private final String messageCode;
	/** Tipo de mensaje **/
	private final String messageTag;
	/** Descripcion del mensaje **/
	private final String messageDescription;
	
	private MessageType(String messageCode, String messageTag, String messageDescription) {
		this.messageCode = messageCode;
		this.messageTag = messageTag;
		this.messageDescription = messageDescription;
	}

	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * @return the messageTag
	 */
	public String getMessageTag() {
		return messageTag;
	}

	/**
	 * @return the messageDescription
	 */
	public String getMessageDescription() {
		return messageDescription;
	}
	
	
}
