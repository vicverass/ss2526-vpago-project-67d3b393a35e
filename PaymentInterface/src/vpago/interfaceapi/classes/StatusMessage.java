package vpago.interfaceapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import vpago.interfaceapi.types.MessageType;

/**
 * Clase para aplicar formato a los mensajes no transaccionales. Informan de condiciones de excepcion, informacion general, entre otros
 */
@XmlRootElement
@XmlType(propOrder={"messageCode", "messageTag", "messageDescription"})
public class StatusMessage extends PaymentMessage {
	
	public StatusMessage() {
		super();
	}
	
	public StatusMessage(MessageType messageType) {
		super();
		this.messageCode = messageType.getMessageCode();
		this.messageTag = messageType.getMessageTag();
		this.messageDescription = messageType.getMessageDescription();
	}

	/** Codigo numerico del mensaje **/
	private String messageCode;
	/** Tipo de mensaje **/
	private String messageTag;
	/** Descripcion del mensaje **/
	private String messageDescription;
	
	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}
	
	/**
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	/**
	 * @return the messageTag
	 */
	public String getMessageTag() {
		return messageTag;
	}
	
	/**
	 * @param messageTag the messageTag to set
	 */
	public void setMessageTag(String messageTag) {
		this.messageTag = messageTag;
	}
	
	/**
	 * @return the messageDescription
	 */
	public String getMessageDescription() {
		return messageDescription;
	}
	
	/**
	 * @param messageDescription the messageDescription to set
	 */
	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StatusMessage [messageCode=" + messageCode + ", messageTag=" + messageTag + ", messageDescription="
				+ messageDescription + "]";
	}

}
