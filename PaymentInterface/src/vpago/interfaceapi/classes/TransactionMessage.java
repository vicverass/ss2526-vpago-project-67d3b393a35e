package vpago.interfaceapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Representacion de un mensaje transaccional
 */
@XmlRootElement
@XmlType(propOrder={"terminal", "paymentChannel", "transaction", "status", "function"})
public class TransactionMessage extends PaymentMessage {
	
	public TransactionMessage() {
		super();
	}
	
	public TransactionMessage(TransactionTerminal terminal, Transaction transaction, PaymentChannel paymentChannel,
			TransactionStatus status) {
		super();
		this.terminal = terminal;
		this.transaction = transaction;
		this.paymentChannel = paymentChannel;
		this.status = status;
	}

	/** Terminal donde se gener� la transaccion */
	protected TransactionTerminal terminal;
	/** Datos de la transaccion asociada al mensaje transaccional */
	protected Transaction transaction;
	/** Datos para identificar el canal de pago utilizado */
	protected PaymentChannel paymentChannel;
	/** Respuesta al mensaje transaccional */
	protected TransactionStatus status;
	/** Funcion no transaccional (No ISO8583) **/
	protected EngineFunction function;
	
	
	/**
	 * @return el terminal
	 */
	public TransactionTerminal getTerminal() {
		return terminal;
	}
	
	
	/**
	 * @param terminal el terminal a fijar
	 */
	public void setTerminal(TransactionTerminal terminal) {
		this.terminal = terminal;
	}
	
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return la transaccion
	 */
	public Transaction getTransaction() {
		return transaction;
	}
	
	/**
	 * @param transaction la transaccion a fijar
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	/**
	 * @return el estado
	 */
	public TransactionStatus getStatus() {
		return status;
	}
	
	/**
	 * @param status el estado a fijar
	 */
	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	/**
	 * @return the function
	 */
	public EngineFunction getFunction() {
		return function;
	}

	/**
	 * @param function the function to set
	 */
	public void setFunction(EngineFunction function) {
		this.function = function;
	}

	@Override
	public String toString() {
		return "TransactionMessage [terminal=" + terminal + ", transaction=" + transaction + ", paymentChannel="
				+ paymentChannel + ", status=" + status + "]";
	}

}
