package vpago.interfaceapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase para aplicar formato a las entidades que seran devueltas por las funciones de la Interfaz de Pago
 */
@XmlRootElement
@XmlSeeAlso({TransactionMessage.class,
			StatusMessage.class,
			FunctionMessage.class})
public class PaymentMessage {
	protected static final Logger logger = LoggerFactory.getLogger(PaymentMessage.class);
}
