package vpago.interfaceapi.classes;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase para representar una Transaccion
 */
@XmlRootElement
@XmlType(propOrder={"type", "functionParameters"})
public class EngineFunction {
	
	public EngineFunction() {
		super();
	}

	public EngineFunction(String type, Map<String, String> functionParameters) {
		super();
		this.type = type;
		this.functionParameters = functionParameters;
	}
	
	/** Tipo de funcion */
	private String type;
	/** Parametros necesarios para el procesamiento de la transaccion solicitada */
	private Map<String, String> functionParameters;
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the functionParameters
	 */
	public Map<String, String> getFunctionParameters() {
		return functionParameters;
	}
	
	/**
	 * @param functionParameters the functionParameters to set
	 */
	public void setFunctionParameters(Map<String, String> functionParameters) {
		this.functionParameters = functionParameters;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EngineFunction [type=" + type + ", functionParameters=" + functionParameters + "]";
	}
	
	
}
