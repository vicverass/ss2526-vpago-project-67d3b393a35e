package vpago.interfaceapi.classes;

import java.util.Map;

public class FunctionMessage extends PaymentMessage {
	
	public FunctionMessage() {
		super();
	}

	public FunctionMessage(Map<String, String> functionParameters) {
		super();
		this.functionParameters = functionParameters;
	}

	private Map<String, String> functionParameters;

	/**
	 * @return the functionParameters
	 */
	public Map<String, String> getFunctionParameters() {
		return functionParameters;
	}

	/**
	 * @param functionParameters the functionParameters to set
	 */
	public void setFunctionParameters(Map<String, String> functionParameters) {
		this.functionParameters = functionParameters;
	}
	
	
}
