package vpago.interfaceapi.classes;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase para representar una Transaccion
 */
@XmlRootElement
@XmlType(propOrder={"hashCode", "type", "controlToken", "secToken", "timestamp", "transactionParameters", "referenceNum", "serial"})
public class Transaction {

	public Transaction() {
		super();
	}

	
	public Transaction(String hashCode, String type, String controlToken, String secToken, String timestamp,
			Map<String, String> transactionParameters, String referenceNum, String serial) {
		super();
		this.hashCode = hashCode;
		this.type = type;
		this.controlToken = controlToken;
		this.secToken = secToken;
		this.timestamp = timestamp;
		this.transactionParameters = transactionParameters;
		this.referenceNum = referenceNum;
		this.serial = serial;
	}

	/** Identificador de la transaccion generado por el motor */
	private String hashCode;
	/** Tipo de transaccion */
	private String type;
	/** Token de control enviado por el solicitante de la transaccion */
	private String controlToken;
	/** Firma de para evitar duplicacion de la transaccion */
	private String secToken;
	/** Marca de tiempo */
	private String timestamp;
	/** Parametros necesarios para el procesamiento de la transaccion solicitada */
	private Map<String, String> transactionParameters;
	/** Referencia de transaccion anterior **/
	private String referenceNum;
	/** Serial del dispositivo**/
	private String serial;
	
	/**
	 * @return the hashCode
	 */
	public String getHashCode() {
		return hashCode;
	}

	/**
	 * @param hashCode the hashCode to set
	 */
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the controlToken
	 */
	public String getControlToken() {
		return controlToken;
	}

	/**
	 * @param controlToken the controlToken to set
	 */
	public void setControlToken(String controlToken) {
		this.controlToken = controlToken;
	}

	/**
	 * @return the transactionParameters
	 */
	public Map<String, String> getTransactionParameters() {
		return transactionParameters;
	}

	/**
	 * @param transactionParameters the transactionParameters to set
	 */
	public void setTransactionParameters(Map<String, String> transactionParameters) {
		this.transactionParameters = transactionParameters;
	}

	/**
	 * @return the secToken
	 */
	public String getSecToken() {
		return secToken;
	}

	/**
	 * @param secToken the secToken to set
	 */
	public void setSecToken(String secToken) {
		this.secToken = secToken;
	}
	
	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * @return the referenceNum
	 */
	public String getReferenceNum() {
		return referenceNum;
	}

	/**
	 * @param referenceNum the referenceNum to set
	 */
	public void setReferenceNum(String referenceNum) {
		this.referenceNum = referenceNum;
	}

	/**
	 * 
	 * @return the serial number
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * 
	 * @param serial
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transaction [hashCode=" + hashCode + ", type=" + type + ", controlToken=" + controlToken + ", secToken="
				+ secToken + ", timestamp=" + timestamp + ", transactionParameters=" + transactionParameters
				+ ", referenceNum=" + referenceNum + ", serial=" + serial + "]";
	}
	

}
