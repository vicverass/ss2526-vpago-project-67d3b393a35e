package vpago.interfaceapi.classes;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Estado de la transaccion
 */
@XmlRootElement
public class TransactionStatus {

	public TransactionStatus() {
		super();
	}

	public TransactionStatus(
			String code, 
			String description, 
			Map<String, String> statusData, 
			Map<Integer, String> additionalTopData, 
			Map<Integer, String> additionalBottomData,
			boolean statePrint) {
		super();
		this.code = code;
		this.description = description;
		this.statusData = statusData;
		this.additionalTopData = additionalTopData;
		this.additionalBottomData = additionalBottomData;
		this.statePrint = statePrint;
	}

	/** Codigo que identifica al estado */
	protected String code;
	/** Descripcion del estado */
	protected String description;
	/** Parametros relacionados al estado de la transaccion */
	protected Map<String,String> statusData;
	/** Parametros relacionados al estado de la transaccion */
	protected Map<Integer,String> additionalTopData;
	/** Parametros relacionados al estado de la transaccion */
	protected Map<Integer,String> additionalBottomData;
	/** Indica si se imprime comprobante  **/
	protected boolean statePrint;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Map<String, String> getStatusData() {
		return statusData;
	}

	public void setStatusData(Map<String, String> statusData) {
		this.statusData = statusData;
	}
	
	public void setStatusParam(String key, String val){
		this.statusData.put(key, val);
	}
	
	public Map<Integer, String> getAdditionalTopData() {
		return additionalTopData;
	}

	public void setAdditionalTopData(Map<Integer, String> additionalTopData) {
		this.additionalTopData = additionalTopData;
	}
	
	public Map<Integer, String> getAdditionalBottomData() {
		return additionalBottomData;
	}

	public void setAdditionalBottomData(Map<Integer, String> additionalBottomData) {
		this.additionalBottomData = additionalBottomData;
	}
	
	/**
	 * @return the statePrint
	 */
	public boolean isStatePrint() {
		return statePrint;
	}

	/**
	 * @param statePrint the statePrint to set
	 */
	public void setStatePrint(boolean statePrint) {
		this.statePrint = statePrint;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionStatus [code=" + code + ", description=" + description + ", statusData=" + statusData
				+ ", additionalTopData=" + additionalTopData + ", additionalBottomData=" + additionalBottomData
				+ ", statePrint=" + statePrint + "]";
	}
	
}
