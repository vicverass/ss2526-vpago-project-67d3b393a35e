package vpago.interfaceapi.classes;

/**
 * Punto terminal donde se origina la transaccion
 */
public class TransactionTerminal {
		
	public TransactionTerminal() {}
	
	public TransactionTerminal(String terminalId, String terminalKey) {
		super();
		this.terminalId = terminalId;
		this.terminalKey = terminalKey;
	}
	
	/** Identificador del terminal donde se genero la transaccion */
	protected String terminalId;
	/** Llave de identificacion del terminal donde se genero la transaccion */
	protected String terminalKey;
		
	/**
	 * @return the terminalId
	 */
	public String getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the terminalKey
	 */
	public String getTerminalKey() {
		return terminalKey;
	}
	
	/**
	 * @param terminalKey the terminalKey to set
	 */
	public void setTerminalKey(String terminalKey) {
		this.terminalKey = terminalKey;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TransactionTerminal [terminalId=" + terminalId + ", terminalKey=" + terminalKey + "]";
	}

}
