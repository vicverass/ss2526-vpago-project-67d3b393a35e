package vpago.interfaceapi.classes;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Clase para representar un Canal de Pago
 */
@XmlRootElement
@XmlType(propOrder={"channelCode"})
public class PaymentChannel {
	
	public PaymentChannel() {
		super();
	}

	public PaymentChannel(String channelCode) {
		super();
		this.channelCode = channelCode;
	}

	/** Codigo identificador que sera enviado por el Terminal */
	private String channelCode;
	
	/**
	 * @return the channelCode
	 */
	public String getChannelCode() {
		return channelCode;
	}

	/**
	 * @param channelCode the channelCode to set
	 */
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentChannel [channelCode=" + channelCode + "]";
	}

}
