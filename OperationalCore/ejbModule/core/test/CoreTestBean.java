package core.test;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class CoreTestBean
 */
@Stateless
@LocalBean
public class CoreTestBean {

    /**
     * Default constructor. 
     */
    public CoreTestBean() {
        super();
    }

}
