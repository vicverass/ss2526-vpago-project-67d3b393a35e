package core.test;

import java.nio.channels.CompletionHandler;

public class MyHandler implements CompletionHandler<Integer,  String> {
	
	@Override
	public void completed(Integer result, String attachment) {
		System.out.println("MyHandler - completed");
	}

	@Override
	public void failed(Throwable exc, String attachment) {
		System.out.println("MyHandler - failed");
	}

}
