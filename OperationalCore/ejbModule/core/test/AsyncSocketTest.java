package core.test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

public class AsyncSocketTest {

	public static void main(String[] args) {
		try {
			AsynchronousSocketChannel clientChannel = AsynchronousSocketChannel.open();
			InetSocketAddress hostAddress = new InetSocketAddress("127.0.0.1", 9115);
//			clientChannel.bind(hostAddress);
			clientChannel.connect(hostAddress);
			
//			String [] messages = new String [] {"Time goes fast.", "What now?", "Bye."};
			String [] messages = new String [] {"Time goes fast."};
			for (int i = 0; i < messages.length; i++) {
				byte [] message = new String(messages [i]).getBytes();
				ByteBuffer buffer = ByteBuffer.wrap(message);
			    clientChannel.write(buffer);
				ByteBuffer bufferR = ByteBuffer.allocate(100);
				clientChannel.read(bufferR, "Attachment ".concat(String.valueOf(i)), new MyHandler());
				
			}
			
			try {
				Thread.sleep(600000000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
