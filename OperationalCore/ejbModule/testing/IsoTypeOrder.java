package testing;

import com.solab.iso8583.IsoType;

/**
 * Imprime las constantes asociadas a cada elemento del tipo enumerado IsoType
 */
public class IsoTypeOrder {

	public static void main(String[] args) {
		int len = IsoType.values().length;
		for(int i = 0; i < len; i++) System.err.println(Integer.toString(IsoType.values()[i].ordinal()) + " " + IsoType.values()[i]  );

	}

}
