package testing;


import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.impl.SimpleTraceGenerator;
import com.solab.iso8583.parse.ConfigParser;
import com.solab.iso8583.util.HexCodec;

/**
 * Metodos para pruebas de construccion y envio de transacciones
 */
public class IsoTest {
	private static final Logger logger = LoggerFactory.getLogger(IsoTest.class);
	
	public static void main(String[] args) {
		creditSale();
		//autoReverse();
		//echoTest();
	
	}

	/**
	 * Imprime los campos de una estructura IsoMessage
	 * 
	 * @param m Estructura IsoMessage que almacena un mensaje
	 */
	public static void print(IsoMessage m) {
		logger.info("{}", String.format("TYPE: %04x\n", m.getType()));
		for (int i = 2; i < 128; i++) {
			if (m.hasField(i)) {
				logger.info("{}", String.format("F %3d(%s): %s -> '%s'\n", i, m.getField(i)
						.getType(), m.getObjectValue(i), m.getField(i)
						.toString()));
			}
		}
	}
	
	/**
	 * Convierte un string al formato "BCD" de Credicard. En este formato se representa un 
	 * numero que esta en representacion String a otro String con su equivalente en hexadecimal.
	 * 
	 * @param tobcd Cadena de texto a convertir
	 * 
	 * @return Cadena de texto convertida
	 */
	@SuppressWarnings("unused")
	private static String toMyBcd(String tobcd){
		String hexs = "";
		int strlen = tobcd.length();
		//char[] charlist = new char [strlen];
		char[] charlist = tobcd.toCharArray();
		for(int i = 0; i < strlen; i++){
			//charlist[i] = tobcd.charAt(i);
			hexs += String.format("%02x", (int) charlist[i]);
		}
		logger.debug("hexs: {}", hexs);
		logger.debug("charlist: {}", charlist);
		return hexs;
	}
	
	/**
	 * Convierte un string a otro con la representacion ascii de cada par de elementos del string
	 * 
	 * @param strtochar Cadena de texto a convertir
	 * 
	 * @return Cadena de texto convertida
	 */
	@SuppressWarnings("unused")
	private static String convertChar(String strtochar){
		String ans = "";
		char aux;
		for(int i = 0; i < strtochar.length(); i+=2){
			aux = (char)Integer.parseInt(strtochar.substring(i, i+2), 16);
			ans += aux;
		}
		return ans;
	}
	
	/**
	 * Metodo para probar mensajes 0200 y 0210
	 */
	private static void creditSale(){
		try {
			MessageFactory<?> mfact = ConfigParser.createFromClasspathConfig("testing/config.xml");
			//mfact.setAssignDate(true);
			/*
				Se usa un generador simple para el valor del campo 11 (trace), es necesario
				implementar uno
			*/
			mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			mfact.setIgnoreLastMissingField(true);
			IsoMessage m = mfact.newMessage(0x200);
			//m.setBinary(true);
			m.setValue(2, "4761739001010119", IsoType.LLVAR, 0);
			m.setValue(3, 3000, IsoType.NUMERIC, 6);
			m.setValue(4, new BigDecimal("501.25"), IsoType.AMOUNT, 0);
			m.setValue(14, 1607, IsoType.NUMERIC, 4);
			m.setValue(24, 003, IsoType.NUMERIC, 3);
			m.setValue(32, "441132", IsoType.LLVAR, 0);
			/*
			m.setValue(41, "00004002", IsoType.BINARY, 8);
			m.setValue(42, "0010000076     ", IsoType.BINARY, 15);
			m.setValue(48, "01230020191602", IsoType.LLLBIN, 0);
			m.setValue(62, "123765", IsoType.LLLBIN, 0);
			*/
			
			//byte [] c41 = {'0','0','0','0','4','0','0','2'};
			//byte [] c42 = {'0','0','1','0','0','0','0','0','2','5',' ',' ',' ',' ',' '};
			String s41 = "00004002";
			byte [] c41 = s41.getBytes();
			String s42 = "0010000025     ";
			byte [] c42 = s42.getBytes();
			m.setValue(41, HexCodec.hexEncode(c41, 0, 8), IsoType.NUMERIC, 16);
			m.setValue(42, HexCodec.hexEncode(c42, 0, 15), IsoType.NUMERIC, 30);
			//m.setValue(41, toMyBcd("00001202"), IsoType.BINARY, 8);
			//m.setValue(42, toMyBcd("0010000025     "), IsoType.BINARY, 15);
			//byte [] ci = {'0', '0', '1', '4', '0','1','2','3','1','2','3','4','5','6','7','8','9','0'};
			byte [] ci = { '0','1','2','3','1','2','3','4','5','6','7','8','9','0'};
			//m.setValue(48, new String(ci), IsoType.NUMERIC, 18);
			m.setValue(48, new String(ci), IsoType.LLLLVAR, 0);
			//m.setValue(48, "014" + toMyBcd("01230020191602"), IsoType.ALPHA, 28+3);
			byte [] ref = {0, 0, 6, '1','2','3','4','5','6'};
			m.setValue(62, HexCodec.hexEncode(ref, 0, 9), IsoType.NUMERIC, 18);
			//m.setValue(62, "006" + toMyBcd("123765"), IsoType.ALPHA, 12+3);
			print(m);
			logger.debug("Debug string: {}", m.debugString());
			//sendMsgMan(m);
			sendMsgAut(m);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo para probar mensajes 0400 y 0410
	 */
	@SuppressWarnings("unused")
	private static void autoReverse(){
		try {			
			MessageFactory<?> mfact = ConfigParser.createFromClasspathConfig("testing/config.xml");
			//mfact.setAssignDate(true);
			/*
				Se usa un generador simple para el valor del campo 11 (trace), es necesario
				implementar uno
			*/
			mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			mfact.setIgnoreLastMissingField(true);
			//mfact.setAssignDate(true);
			IsoMessage m = mfact.newMessage(0x400);
			//m.setBinary(true);
			m.setValue(2, "4761739001010119", IsoType.LLVAR, 0);
			m.setValue(3, 3000, IsoType.NUMERIC, 6);
			m.setValue(4, new BigDecimal("501.25"), IsoType.AMOUNT, 0);
			m.setValue(14, 1607, IsoType.NUMERIC, 4);
			m.setValue(24, 003, IsoType.NUMERIC, 3);
			/*
			m.setValue(41, "00004002", IsoType.BINARY, 8);
			m.setValue(42, "0010000076     ", IsoType.BINARY, 15);
			m.setValue(48, "01230020191602", IsoType.LLLBIN, 0);
			m.setValue(62, "123765", IsoType.LLLBIN, 0);
			*/
			
			byte [] c41 = {'0','0','0','0','1','2','0','2'};
			byte [] c42 = {'0','0','1','0','0','0','0','0','2','5',' ',' ',' ',' ',' '};
			m.setValue(41, HexCodec.hexEncode(c41, 0, 8), IsoType.NUMERIC, 16);
			m.setValue(42, HexCodec.hexEncode(c42, 0, 15), IsoType.NUMERIC, 30);
			
			//m.setValue(41, toMyBcd("00001202"), IsoType.BINARY, 8);
			//m.setValue(42, toMyBcd("0010000025     "), IsoType.BINARY, 15);
			byte [] ref = {0, 0, 6, '1','2','3','4','5','6'};
			m.setValue(62, HexCodec.hexEncode(ref, 0, 9), IsoType.NUMERIC, 18);
			//m.setValue(62, "006" + toMyBcd("123765"), IsoType.ALPHA, 12+3);
			print(m);
			sendMsgMan(m);
			logger.debug("Debug string: {}", m.debugString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Metodo para probar mensajes 0800 y 0810
	 */
	@SuppressWarnings("unused")
	private static void echoTest(){
		try {			
			MessageFactory<?> mfact = ConfigParser.createFromClasspathConfig("testing/config.xml");
			//mfact.setAssignDate(true);
			/*
				Se usa un generador simple para el valor del campo 11 (trace), es necesario
				implementar uno
			*/
			mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			mfact.setIgnoreLastMissingField(true);
			//mfact.setAssignDate(true);
			IsoMessage m = mfact.newMessage(0x800);
			//m.setBinary(true);
			
			//m.setValue(41, toMyBcd("00001202"), IsoType.BINARY, 8);
			//m.setValue(42, toMyBcd("0010000025     "), IsoType.BINARY, 15);
			
			//byte [] c41 = {0,0,0,0,1,2,0,2};
			//byte [] c42 = {0,0,1,0,0,0,0,2,5,' ',' ',' ',' ',' ',' '};
			byte [] c41 = {'0','0','0','0','4','0','0','2'};
			byte [] c42 = {'0','0','1','0','0','0','0','0','2','5',' ',' ',' ',' ',' '};
			m.setValue(41, HexCodec.hexEncode(c41, 0, 8), IsoType.NUMERIC, 16);
			m.setValue(42, HexCodec.hexEncode(c42, 0, 15), IsoType.NUMERIC, 30);
			
			print(m);
			logger.debug("Debug string: {}", m.debugString());
			sendMsgMan(m);
			
			/*
			byte [] arg0 = {88};
			logger.debug("", HexCodec.hexEncode(arg0, 0, 1));
			*/
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Convierte los caracteres de una cadena a hexadecimal
	 * 
	 * @param str Cadena de texto a convertir
	 * 
	 * @return Cadena de texto obtenida
	 */
	private static String convertStringToHex(String str){
	  
		char[] chars = str.toCharArray();
		String middle;
		
		StringBuffer hex = new StringBuffer();
		for(int i = 0; i < chars.length; i++){
			middle = Integer.toHexString((int)chars[i]);
			hex.append(middle);
            logger.debug("{} {}", new Object[]{i, middle});
		}
		
		return hex.toString();
	}
	
	/**
	 * Convierte una cadena de texto hexadecimal a una cadena de texto
	 * 
	 * @param hex Cadena de texto a convertir
	 * 
	 * @return Arreglo de bytes obtenido
	 */
	private static String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();

        StringBuilder temp = new StringBuilder();
        logger.debug("Longitud: {}", hex.length());
        logger.debug("i\toutput decimal (char)dec (byte)dec");
        for( int i=0; i<hex.length()-1; i+=2 ){
            String output = hex.substring(i, (i + 2));
            int decimal = Integer.parseInt(output, 16);
            //sb.append((char)decimal);
            //sb.append((byte)decimal);
            sb.append((char)decimal);    
            logger.debug("{}\t{}\t{}\t{}\t{}", new Object[]{i, output, decimal, (char)decimal, (byte)decimal/* & 0xFF*/});
            temp.append(decimal);
        }
        logger.debug("sb len: {}", sb.toString().length());
        logger.debug("sb.getBytes len: {}", sb.toString().getBytes().length);
        return sb.toString();
  }
	
	/**
	 * Convierte una cadena de texto hexadecimal a un arreglo de bytes sin signo
	 * 
	 * @param hex Cadena de texto a convertir
	 * 
	 * @return Arreglo de bytes obtenido
	 */
	@SuppressWarnings("unused")
	private static byte[] convertHexToStringUnsigned(String hex){
		ByteArrayOutputStream dynByteA = new ByteArrayOutputStream();
        StringBuilder temp = new StringBuilder();
        logger.debug("Longitud: {}", hex.length());
        logger.debug("i\toutput decimal (char)dec (byte)dec");
        for( int i=0; i<hex.length()-1; i+=2 ){
            String output = hex.substring(i, (i + 2));      
            int decimal = Integer.parseInt(output, 16);
            if ((byte)decimal >= 0) {
            	dynByteA.write((byte)decimal);
            	logger.debug("P: {}", (byte)decimal);
            }else{
            	dynByteA.write(0xFF); //Probar 13/11/15
            	dynByteA.write((decimal & 0x00FF));
            	logger.debug("N {} {}", new Object[]{(decimal & 0xFF00), (decimal & 0x00FF)});
            }
            logger.debug("{}\t{}\t{}\t{}\t{}", new Object[]{i, output, decimal, (char)decimal, (byte)decimal} /*& 0xFF*/);
            temp.append(decimal);
        }
        byte[] unsignedA = dynByteA.toByteArray();
        for(int i=0; i < unsignedA.length; i++) {
        	logger.debug("i={} {} {}", new Object[]{i, unsignedA[i], ((unsignedA[i]) < 0 ? "N -> " + ((unsignedA[i] < 0) ? (unsignedA[i] & 0xFF) : "") : "P")});
        }
        return dynByteA.toByteArray();
  }
	
	/**
	 * Convierte una cadena de texto hexadecimal a un arreglo de bytes
	 * 
	 * @param s Cadena de texto a convertir
	 * 
	 * @return Arreglo de bytes obtenido
	 */
	@SuppressWarnings("unused")
	private static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	        logger.debug("{} {}{} {}", new Object[]{i, s.charAt(i), s.charAt(i+1), data[i / 2]});
	    }
	    return data;
	}	
	
	/**
	 * Obtiene un arreglo de bytes sin signo a partir de una cadena de texto
	 * 
	 * @param s Cadena de texto a convertir
	 * 
	 * @return Arreglo de bytes obtenido
	 */
	@SuppressWarnings("unused")
	private static byte[] unsignedBytes(String s) {
		byte[] signedA = s.getBytes();
		byte[] unsignedA = new byte[signedA.length];
		for(int i=0; i< signedA.length; i++) {
			unsignedA[i] = (byte) ((signedA[i]) < 0 ? ((int)signedA[i] & 0xFF) : signedA[i]); 
			logger.debug("i= {} {}", new Object[]{i, unsignedA[i]});
		}
		return unsignedA;
	}
	
	/**
	 * Metodo para enviar mensajes de prueba al switch de desarrollo, la longitud se calcula manual
	 * 
	 * @param m Estructura IsoMessage que contiene el mensaje
	 */
	private static void sendMsgMan(IsoMessage m){
		Socket testSocket;
		DataInputStream dis;
		DataOutputStream dos;
		try {
			/* Creacion del socket */
			
			//testSocket = new Socket("192.168.8.118",8000);
			//testSocket = new Socket("192.168.8.118",9000);
			testSocket = new Socket("192.168.1.235",8000);
			//testSocket = new Socket("137.1.1.12",5020);
			//testSocket = new Socket("localhost",8000);
			
			/* Si el socket esta conectado */
			if(testSocket.isConnected()){
				
				logger.info("Socket conectado. Enviando....");

				/* Arreglos de bytes para los bytes de longitud, en decimal y hexadecimal */
				//byte [] lbytes = {0,44};
				//sbyte [] lbytesx = {0,0x58}; //0800
				//byte [] lbytesx = {0,0x4F}; //0800
				//byte [] lbytesx = {0,0x4D}; //0800
				byte [] lbytesx = {0,0x5F}; //0200
				//byte [] lbytesx = {0,(byte) 0xA8}; //0200
				//byte [] lbytesx = {(byte) 0xFF,(byte) 0xA8}; //0200
				//byte [] lbytesx = {0,0x2C}; //0400
				//byte [] lbytesx = {0,0};
				logger.info("lbytesx[0] {}", lbytesx[0]);
				logger.info("lbytesx[1] {}", lbytesx[1]);
				/* Creamos el DataOutputStream para el envio del mensaje */
				dos = new DataOutputStream(testSocket.getOutputStream());
				
				/* Envio con writeBytes */
				//dos.writeBytes(new String(lbytes) + new String(m.writeData()));
				//dos.writeBytes(new String(lbytesx) + new String(m.writeData()));
				
				/* Envio con writeUTF */
				//dos.writeUTF(new String(m.writeData()));   
				//dos.writeUTF(new String(lbytes) + new String(m.writeData()));
				//dos.writeUTF(new String(lbytesx) + new String(m.writeData()));
				//dos.writeUTF(new String(lbytesx) + m.debugString());
				//dos.writeUTF(new String(lbytesx) + "600007000008002020000000C000009900000447063030303031323032303031303030303032352020202020");
				
				//Falta probar-----------------------------------------------------------------------------
				/* Envio con write concatendo los arreglos de bytes en lugar de convertir a string */
				//byte[] msg = m.writeData();
				

				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes(); //Funciona pero llega el bitmap incorrecto
				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes("UTF-16");
				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes("ISO-8859-1");
				
				//Probar 13/11/15
				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes(); //Works
				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes(StandardCharsets.UTF_8); // No 
				//byte[] msg = convertHexToString("005F" + m.debugString()).getBytes(StandardCharsets.UTF_16); // No llego 
				byte[] msg = convertHexToString("0063" + m.debugString()).getBytes(StandardCharsets.ISO_8859_1); // Caracteres llegan correctos
				//byte[] msg = new BigInteger("005F" + m.debugString(), 16).toByteArray(); // No llego
				//byte[] msg = convertHexToStringUnsigned("005F" + m.debugString()); // Maal sin bitmap no enviar 
				
				//byte[] msg = convertHexToStringUnsigned("005F" + m.debugString()).getBytes(); 
				//byte[] msg = unsignedBytes(convertHexToString("005F" + m.debugString()));
				//byte[] msg = convertStringToHex(m.debugString()).getBytes();
				//byte[] msg = hexStringToByteArray(m.debugString());
				//byte[] msg = convert(m.debugString()).getBytes();

				for(int i=0; i < msg.length; i++)
					logger.info("i={} {} {}", new Object[]{i, msg[i], ((msg[i]) < 0 ? "N -> " + ((msg[i] < 0) ? (msg[i] & 0xFF00) + " " + (msg[i] & 0x00FF) : "") : "P")});	
				logger.info("msg: {}", msg);
				
				int lonmsj = msg.length;
				//lbytesx[1] = ;
				logger.info("Bytes del mensaje: {}", lonmsj);
				byte[] lenplusmsg = new byte[msg.length + lbytesx.length];
				System.arraycopy(lbytesx, 0, lenplusmsg, 0, lbytesx.length);
				System.arraycopy(msg, 0, lenplusmsg, lbytesx.length, msg.length);
				//dos.write(lenplusmsg, 0, lenplusmsg.length);
				//dos.write(msg);
				dos.write(msg, 0, msg.length);
				
				/* Envio de los string's como char's con writeChars */
				//dos.writeChars(new String(lbytesx) + m.debugString());
				
				/* Envio del string convertido con converChar */
				//dos.writeChars(new String(lbytesx) + convertChar(m.debugString()));
				//dos.writeChars(new String(lbytesx) + convertChar(m.debugString()));
				
				/* Envio del string convertido con convertHexToString */
				//dos.write(convertHexToString(m.debugString()).getBytes());
				
				
				//Falta probar-----------------------------------------------------------------------------
				
				//testSocket.getOutputStream().write(middle.getBytes());
				//m.write(testSocket.getOutputStream(),2);
				
				/* debugString del mensaje con longitud en decimal */
				//logger.debug("Debug string con longitud en decimal: {}", new String(lbytes) + m.debugString());
				
				/* debugString del mensaje con longitud en hexadecimal */
				//logger.debug("Debug string con longitud en hexadecimal: {}", new String(lbytesx) + m.debugString());
				
				/* debugString del mensaje sin la longitud */
				//logger.debug("Debug string sin la longitud: {}", m.debugString());
				
				dis  = new DataInputStream(testSocket.getInputStream());
				//String answer = dis.readUTF(); //java.io.UTFDataFormatException
			
				byte[] lenbuf = new byte[2];
				dis.read(lenbuf, 0, 2);
				logger.info("lenbuf[0]= {} lenbuf[1]= {}", (char)lenbuf[0], (char)lenbuf[1]);
				int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
				byte[] buf = new byte[size];
				
				/*
				if (dis.read(buf,0,size) == size){
					logger.debug("Bytes leidos: {}", size);
					logger.debug("Recibido: {}",  new String(buf));
				}
				*/
				logger.info("Leidos en realidad: {}", dis.read(buf,0,size));
				String str = new String(buf, StandardCharsets.ISO_8859_1);
				logger.info("String: {}", str);
				logger.info("Bytes leidos (iso len): {}", size);
				logger.info("Recibido: {}", new String(buf));
				for(int i = 0; i < size; i++)
					logger.debug("buf[i={}]= {} char: {}", new Object[]{i, buf[i], (char)buf[i]});
				
				String hex = convertStringToHex(str);
				logger.info("Hex: ", hex);
				String hex2 = DatatypeConverter.printHexBinary(buf);
				logger.info("DatatypeConverter: ", hex2);
				
				//Parsear mensaje de respuesta
				
				MessageFactory<?> mfact = ConfigParser.createFromClasspathConfig("testing/config.xml");
				mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
				mfact.setIgnoreLastMissingField(true);
				try {
					IsoMessage im = mfact.parseMessage(hex2.getBytes(), 10);
					print(im);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			
				
				//dos.flush();
				
				/*
				byte[] lenbuf = new byte[2];
				testSocket.getInputStream().read(lenbuf);
				int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
				byte[] buf = new byte[size];
					
				//Thread.sleep(5000);
				if (testSocket.getInputStream().read(buf) == size){
					logger.debug("Bytes leidos: {}", size);
					logger.debug("Recibido: {}", new String(buf));
				}
					
				//read = testSocket.getInputStream().read(response);
				/*
				if (sock.getInputStream().read(buf) == size) {
				while(read != 0) {
					logger.info("Esperando respuesta...");
					testSocket.getInputStream().read(response);
				}
				*/
			}
			else{
				logger.warn("Socket no conectado");
			}
			
			//testSocket.close();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Metodo para enviar mensajes de prueba al switch de desarrollo, la longitud se calcula automaticamente
	 * 
	 * @param m Estructura IsoMessage que contiene el mensaje
	 */
	private static void sendMsgAut(IsoMessage m){
		Socket testSocket;
		DataInputStream dis;
		DataOutputStream dos;
		try {
			print(m);
			/* Creacion del socket */
			
			//testSocket = new Socket("192.168.8.118",8000);
			//testSocket = new Socket("192.168.8.118",9000);
			testSocket = new Socket("192.168.1.235",8000);
			//testSocket = new Socket("137.1.1.12",5020);
			//testSocket = new Socket("localhost",8000);
			
			/* Si el socket esta conectado */
			if(testSocket.isConnected()){
				
				logger.info("Socket conectado. Enviando....");

				/* Convertir el mensaje y agregarle la longitud */
				ByteBuffer byteBuf = ByteBuffer.allocate(2);
				byte[] requestMsgCom =  convertHexToString(m.debugString()).getBytes(StandardCharsets.ISO_8859_1);
				logger.info(m.debugString());
				byteBuf.putShort((short)requestMsgCom.length);
				byte[] requestMsgLen = byteBuf.array();
				byte[] requestMsg = new byte[requestMsgLen.length + requestMsgCom.length];
				System.arraycopy(requestMsgLen, 0, requestMsg, 0, requestMsgLen.length);
				System.arraycopy(requestMsgCom, 0, requestMsg, requestMsgLen.length, requestMsgCom.length);
				
				/*
				for(int i=0; i < msg.length; i++)
					logger.info("i={} {} {}", new Object[]{i, msg[i], ((msg[i]) < 0 ? "N -> " + ((msg[i] < 0) ? (msg[i] & 0xFF00) + " " + (msg[i] & 0x00FF) : "") : "P")});	
				logger.info("msg: {}", msg);
				*/
				
				int lonmsj = requestMsg.length;
				logger.info("Bytes del mensaje: {}", lonmsj);
				
				/* Creamos el DataOutputStream para el envio del mensaje y el DataInputStream para la recepcion */
				dos = new DataOutputStream(testSocket.getOutputStream());
				dis  = new DataInputStream(testSocket.getInputStream());
				dos.write(requestMsg);
				byte[] lenbuf = new byte[2];
				dis.read(lenbuf, 0, 2);
				logger.info("lenbuf[0]= {} lenbuf[1]= {}", (char)lenbuf[0], (char)lenbuf[1]);
				int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
				byte[] buf = new byte[size];
				
				logger.info("Leidos en realidad: {}", dis.read(buf,0,size));
				String str = new String(buf, StandardCharsets.ISO_8859_1);
				logger.info("String: {}", str);
				logger.info("Bytes leidos (iso len): {}", size);
				logger.info("Recibido: {}", new String(buf));
				for(int i = 0; i < size; i++)
					logger.debug("buf[i={}]= {} char: {}", new Object[]{i, buf[i], (char)buf[i]});
				
				String hex = convertStringToHex(str);
				logger.info("Hex: ", hex);
				String hex2 = DatatypeConverter.printHexBinary(buf);
				logger.info("DatatypeConverter: ", hex2);
				
				//Parsear mensaje de respuesta
				MessageFactory<?> mfact = ConfigParser.createFromClasspathConfig("testing/config.xml");
				mfact.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
				mfact.setIgnoreLastMissingField(true);
				try {
					IsoMessage im = mfact.parseMessage(hex2.getBytes(), 10);
					print(im);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			
			}
			else{
				logger.warn("Socket no conectado");
			}
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
	
