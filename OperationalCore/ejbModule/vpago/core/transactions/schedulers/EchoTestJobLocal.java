package vpago.core.transactions.schedulers;

import javax.ejb.Local;

@Local
public interface EchoTestJobLocal {
	
    public void sendEchoTest();
}
