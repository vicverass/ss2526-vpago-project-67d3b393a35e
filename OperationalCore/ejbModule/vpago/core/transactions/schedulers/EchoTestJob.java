package vpago.core.transactions.schedulers;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.MessageFactory;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.TransactionFactory;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.connections.AsyncReqQueueLocal;
import vpago.core.transactions.connections.NonClosingAsyncTCPLocal;
import vpago.core.transactions.connections.ReadExecutorServiceLocal;
import vpago.core.transactions.exceptions.ConnectionFailure;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.util.TraceGenerator;

/**
 * Session Bean implementation class CronService
 */
@Startup
@Singleton
@LocalBean
public class EchoTestJob implements EchoTestJobRemote, EchoTestJobLocal {
	private static final Logger logger = LoggerFactory.getLogger(EchoTestJob.class);
	
    @EJB(beanName="NonClosingAsyncTCP")
	private NonClosingAsyncTCPLocal nonClosingAsyncTCP;
    @EJB(beanName="AsyncReqQueue")
	private AsyncReqQueueLocal aReqQueue;
    @EJB(beanName="ReadExecutorService")
	private ReadExecutorServiceLocal asyncExecSer;
    @Resource(lookup = "java:app/AppName")
    String appName;
    @Resource(lookup = "java:module/ModuleName")
    String moduleName;

	
    /**
     * Default constructor. 
     */
    public EchoTestJob() {
    	logger.info("Creada instancia de echo-test job ");
    }
    
    //@Schedule(hour = "*", minute = "*", second = "*/20", persistent = false)
    public void sendEchoTest() {
    	logger.trace("EchoTestJob corriendo en module {}", moduleName);
    		
    	if (moduleName.equals("OperationalCore")) {
			//logger.debug(" runs every 20 seconds ");
			logger.trace("{}", nonClosingAsyncTCP != null ? "Diferente de null" : "igual a null");
			logger.trace("--------------ECHO TEST JOB--------------");
			Session session = null;
			try {
				IsoMessage isoFrame = null;
				try {
					session = HibernateUtil.getSessionFactory().openSession();
				} catch (ExceptionInInitializerError err) {
					logger.error(err.toString());
					throw new ServiceUnavailable();
				}
				/*			
							// TODO Cargar el id desde la configuracion o una tabla 
							TransactionType transactionType = (TransactionType) session.load(TransactionType.class, 19);
							logger.trace("{}", transactionType);
							try {
								// Se invoca la primitiva configurada para el Tipo de Transaccion (TransactionType) indicado
								isoFrame = (IsoMessage) TransactionFactory.class.getDeclaredMethod(transactionType.getMethodName(), new Class[]{IsoTemplate.class, TransactionData.class, Transaction.class})
								.invoke(new TransactionFactory(), transactionType.getIsoTemplate(), new TransactionData(), new Transaction());
							} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
								logger.error("Error en la configuracion de la transaccion: {}", e.toString());
								throw new TransactionException();
							} catch (Exception e) {
								logger.error(e.toString());
								throw e;
							} 
							TransactionFactory.printIsoMessage(isoFrame);
							*/
				TransactionType transactionType = (TransactionType) session.load(TransactionType.class, 22);
				int type = transactionType.getIsoTemplate().getTxnType();
				MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
				//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
				reqMsgFactory.setTraceNumberGenerator(new TraceGenerator());
				reqMsgFactory.setIgnoreLastMissingField(true);
				reqMsgFactory.setIsoHeader(type, transactionType.getIsoTemplate().getHeader());
				reqMsgFactory.setAssignDate(transactionType.getIsoTemplate().isDatetimeField());
				//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
				// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
				IsoMessage isoMessage = reqMsgFactory.newMessage(type);
				isoMessage.setValue(70, "301", IsoType.NUMERIC, 3);
				//TransactionFactory.printIsoMessage(isoMessage);

				//			if(nonCloseTCP.getTcpSocket().isClosed()) nonCloseTCP.reconnectSocket();
				//			TransactionFactory.sendMsgAut(isoMessage, 15, nonCloseTCP.getTcpSocket());
				if (!nonClosingAsyncTCP.getTcpSocket().isConnected()) {
					logger.debug("Reconectando socket asincrono.... ");
					nonClosingAsyncTCP.reconnectSocket();
				}

				TransactionFactory trxFact = new TransactionFactory();
				logger.debug("aReqQueue{}es null", aReqQueue == null ? " " : " no ");
				byte[] byteResponse = trxFact.sendMsgAut(isoMessage, 15, nonClosingAsyncTCP.getTcpSocket(), 0,
						aReqQueue, asyncExecSer, null);
			} catch (ConnectionFailure e) {
				logger.error("Error en la configuracion de la transaccion: {}", e.toString());
				logger.debug("Reconectando socket asincrono.... ");
				nonClosingAsyncTCP.reconnectSocket();
			} catch (Exception e) {
				logger.error("Error en la configuracion de la transaccion: {}", e.toString());
				//if(nonCloseTCP == null) logger.error("nonCloseTCP is null");
				//			throw new TransactionException();
			} finally {
				if (session != null)
					session.close();
			} 
		}
    }


}
