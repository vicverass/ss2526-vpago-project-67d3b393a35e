package vpago.core.transactions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.transactions.classes.Merchant;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.TransactionData;

/**
 * Clase que posee las funciones para validar los elementos de una transaccion
 */
public class TransactionValidator {
	private static final Logger logger = LoggerFactory.getLogger(TransactionValidator.class);
	
	public TransactionValidator(TransactionData transactionData) {
		this.transactionData = transactionData;
	}
	
	private TransactionData transactionData;
	
	/**
	 * Verifica si el comercio esta en capacidad de efectuar transacciones
	 * 
	 * @param merchant Comercio a verificar
	 * 
	 * @return Si el comercio esta en capacidad de efectuar transacciones
	 */
	public boolean merchantCanMakeTransaction(Merchant merchant) {
		return merchant.isActive();
	}
	
	/**
	 * Verifica si el terminal esta en capacidad de efectuar transacciones
	 * 
	 * @param terminal Terminal a verificar
	 * 
	 * @return Si el terminal esta en capacidad de efectuar transacciones
	 */
	public boolean terminalCanMakeTransaction(Terminal terminal) {
		return terminal.isActive();
	}
	
	/**
	 * Verifica que el token de seguridad enviado con la transaccion sea correcto. Para su envio el
	 * token debe ser calculado como MD5(GUID del comercio + "*" + GUID del terminal + "*" + Token de control)
	 * donde (+) representa la funcion de concatenacion de cadenas
	 * 
	 * @param terminal Terminal que origino la transaccion
	 * 
	 * @return Si el token de seguridad enviado por el terminal es valido, es decir, si la cadena enviada es igual
	 * a la calculada
	 */
	public boolean validSecToken(Terminal terminal, String timestamp) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update((terminal.getAffiliation().getMerchant().getMerchantGuid() + "*").getBytes());
			md.update((terminal.getTerminalGuid() + "*").getBytes());
			md.update((transactionData.getControlToken() + "*").getBytes());
			if(timestamp != null) md.update(timestamp.getBytes());
			String secToken = DatatypeConverter.printHexBinary(md.digest());
			logger.debug("MD5 a comparar: {}", secToken);
			return transactionData.getSecToken().equalsIgnoreCase(secToken);
		} catch (NoSuchAlgorithmException e) {
			logger.error(e.toString());
			return false;
		} catch (Exception e) {
			logger.error(e.toString());
			return false;
		}
	}
	
	public boolean deviceValidation(Terminal terminal, PaymentChannel paymentChannel, String serial){
		logger.debug("terminal {} serial {}", terminal, serial);
		switch(paymentChannel.getDeviceValidation()) {
			case NONE:
				return true;
			case SERIAL:
				return ((serial != null) && (terminal.getAssignedDevice() != null)) 
					? serial.equals(terminal.getAssignedDevice().getSerialNumber()) : false;
			default:
				return true;
		}
	}
}
