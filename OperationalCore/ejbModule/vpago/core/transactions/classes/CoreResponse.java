package vpago.core.transactions.classes;

import java.util.Map;

/**
 * Clase para representar la respuesta de una transaccion dentro del sistema
 */
public class CoreResponse {
	
	public CoreResponse(String txnId, String timestamp, String code, String description,
			Map<String, String> statusData, Map<Integer, String> additionalTopData, Map<Integer, String> additionalBottomData) {
		super();
		this.txnId = txnId;
		this.timestamp = timestamp;
		this.code = code;
		this.description = description;
		this.statusData = statusData;
		this.additionalTopData = additionalTopData;
		this.additionalBottomData = additionalBottomData;
	}

	/** Identificador de la Transaccion */
	protected String txnId;
	/** Marca de tiempo */
	protected String timestamp;
	/** Codigo que identifica al estado */
	protected String code;
	/** Descripcion del estado */
	protected String description;
	/** Estado de impresión*/
	protected boolean statePrint;
	/** Parametros relacionados al estado de la transaccion */
	protected Map<String,String> statusData;
	/** Parametros Adicionales de la transaccion */
	protected Map<Integer,String> additionalTopData;
	/** Parametros Adicionales de la transaccion */
	protected Map<Integer,String> additionalBottomData;

	/**
	 * @return the txnId
	 */
	public String getTxnId() {
		return txnId;
	}

	/**
	 * @param txnId the txnId to set
	 */
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isStatePrint() {
		return statePrint;
	}

	public void setStatePrint(boolean statePrint) {
		this.statePrint = statePrint;
	}
	/**
	 * @return the statusData
	 */
	public Map<String, String> getStatusData() {
		return statusData;
	}

	/**
	 * @param statusData the statusData to set
	 */
	public void setStatusData(Map<String, String> statusData) {
		this.statusData = statusData;
	}

	public Map<Integer, String> getAdditionalTopData() {
		return additionalTopData;
	}

	public void setAdditionalTopData(Map<Integer, String> additionalTopData) {
		this.additionalTopData = additionalTopData;
	}

	public Map<Integer, String> getAdditionalBottomData() {
		return additionalBottomData;
	}

	public void setAdditionalBottomData(Map<Integer, String> additionalBottomData) {
		this.additionalBottomData = additionalBottomData;
	}

	@Override
	public String toString() {
		return "CoreResponse [txnId=" + txnId + ", timestamp=" + timestamp + ", code=" + code + ", description="
				+ description + ", statePrint=" + statePrint + ", statusData=" + statusData + ", additionalTopData="
				+ additionalTopData + ", additionalBottomData=" + additionalBottomData + "]";
	}

}
