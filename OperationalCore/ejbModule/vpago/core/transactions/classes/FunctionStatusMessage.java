package vpago.core.transactions.classes;

/**
 * Representacion de un mensaje de estado para una funcion no transaccional
 */
public class FunctionStatusMessage {
	
	public FunctionStatusMessage(String messageCode, String messageTag, String messageDescription) {
		super();
		this.messageCode = messageCode;
		this.messageTag = messageTag;
		this.messageDescription = messageDescription;
	}
	
	/** Codigo del tipo de mensaje de estado **/
	private String messageCode;
	/** Etiqueta del tipo de mensaje de estado **/
	private String messageTag;
	/** Descripcion del tipo de mensaje de estado **/
	private String messageDescription;
	
	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}
	
	/**
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	/**
	 * @return the messageTag
	 */
	public String getMessageTag() {
		return messageTag;
	}
	
	/**
	 * @param messageTag the messageTag to set
	 */
	public void setMessageTag(String messageTag) {
		this.messageTag = messageTag;
	}
	
	/**
	 * @return the messageDescription
	 */
	public String getMessageDescription() {
		return messageDescription;
	}
	
	/**
	 * @param messageDescription the messageDescription to set
	 */
	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FunctionStatusMessage [messageCode=" + messageCode + ", messageTag=" + messageTag
				+ ", messageDescription=" + messageDescription + "]";
	}

}
