package vpago.core.transactions.classes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.DatatypeConverter;

import vpago.core.transactions.classes.types.TransactionStatus;

/**
 * Clase persistente para representar una Transaccion
 *
 */
@Entity
@Table(name="transaction")
public class Transaction {
	
	public Transaction() {
		super();
	}
	
	public Transaction(Terminal terminal,
			String controlToken, TransactionType transactionType, String secToken) {
		super();
		this.terminal = terminal;
		this.amount = 0.0;
		this.txnDate = Calendar.getInstance();
		this.controlToken = controlToken;
		this.transactionStatus = TransactionStatus.PENDING;
		this.transactionType = transactionType;
		this.transactionResponse = null;
		this.cardProduct = null;
		this.retried = false;
		this.retryAttempts = 0;
		this.secToken = secToken;
		this.parameters = new HashSet<TransactionParameter>();
		this.responseParameters = new HashSet<TransactionResponseParameter>();
		this.generateTransactionHash();
	}
	
	public Transaction(Terminal terminal, TransactionType transactionType) {
		super();
		this.terminal = terminal;
		this.amount = 0.0;
		this.txnDate = Calendar.getInstance();
		this.transactionStatus = TransactionStatus.PENDING;
		this.transactionType = transactionType;
		this.transactionResponse = null;
		this.cardProduct = null;
		this.retried = false;
		this.retryAttempts = 0;
		this.parameters = new HashSet<TransactionParameter>();
		this.responseParameters = new HashSet<TransactionResponseParameter>();
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Codigo único generado para la transaccion */
	private String hashCode;
	/** Identificador del terminal que genero la transaccion */
	private Terminal terminal;
	/** Campo 41 ISO8583 */
	private String terminalBankId;
	/** Campo 42 ISO8583 */
	private String merchantIdCode;
	/** Monto de la transacción */
	private double amount;
	/** Fecha en que se realizo la transaccion */
	private Calendar txnDate;
	/** Código de control enviado por el terminal */
	private String controlToken;
	/** Estado de la transaccion */
	private TransactionStatus transactionStatus;
	/** Identificador del tipo de transaccion */
	private TransactionType transactionType;
	/** Respuesta de la transaccion */
	private TransactionResponse transactionResponse;
	/** Marca de tarjeta */
	private GeneralCardProduct cardProduct;
	/** Si se vencio el timeout para recibir respuesta a la transaccion */
	private boolean retried;
	/** Numero de veces que se reintento realizar la transaccion */
	private int retryAttempts;
	/** Firma de seguridad para validacion de integridad y evitar duplicacion */
	private String secToken;
	/** Conjunto de parametros asociados (solicitud) */
	private Set<TransactionParameter> parameters; 
	/** Conjunto de parametros asociados (respuesta) */
	private Set<TransactionResponseParameter> responseParameters; 
	/** Lote cerrado al que pertenece la transaccion **/
	private BatchHistory batchHistory;
	/** Numero de lote **/
	private String batchNumber;
	/** Numero de referencia **/
	private String refNumber;
	/** Lote actual  al que pertenece la transaccion **/
	private CurrentBatch currentBatch;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="transaction_id_seq")
	@SequenceGenerator(name="transaction_id_seq", sequenceName = "transaction_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the hashCode
	 */
	@Column(name="hash_code")
	public String getHashCode() {
		return hashCode;
	}
	
	/**
	 * @param hashCode the hashCode to set
	 */
	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}
	
	/**
	 * @return the terminal
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminal() {
		return terminal;
	}
	
	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}
	
	/**
	 * @return the terminalBankId
	 */
	@Column(name="terminal_bank_id")
	public String getTerminalBankId() {
		return terminalBankId;
	}
	
	/**
	 * @param terminalBankId the terminalBankId to set
	 */
	public void setTerminalBankId(String terminalBankId) {
		this.terminalBankId = terminalBankId;
	}
	
	/**
	 * @return the merchantIdCode
	 */
	@Column(name="merchant_id_code")
	public String getMerchantIdCode() {
		return merchantIdCode;
	}
	
	/**
	 * @param merchantIdCode the merchantIdCode to set
	 */
	public void setMerchantIdCode(String merchantIdCode) {
		this.merchantIdCode = merchantIdCode;
	}
	
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the txnDate
	 */
	@Column(name="txn_date")
	public Calendar getTxnDate() {
		return txnDate;
	}
	/**
	 * @param txnDate the txnDate to set
	 */
	public void setTxnDate(Calendar txnDate) {
		this.txnDate = txnDate;
	}
	
	/**
	 * @return the controlToken
	 */
	@Column(name="control_token")
	public String getControlToken() {
		return controlToken;
	}
	
	/**
	 * @param controlToken the controlToken to set
	 */
	public void setControlToken(String controlToken) {
		this.controlToken = controlToken;
	}
	
	/**
	 * @return the transactionStatus
	 */
	@Enumerated(EnumType.ORDINAL)
	@Column(name="transaction_status")
	public TransactionStatus getTransactionStatus() {
		return transactionStatus;
	}
	
	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(TransactionStatus transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	
	/**
	 * @return the transactionType
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_type_id")
	public TransactionType getTransactionType() {
		return transactionType;
	}
	
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}
	
	/**
	 * @return the transactionResponse
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_response_id")
	public TransactionResponse getTransactionResponse() {
		return transactionResponse;
	}
	
	/**
	 * @param transactionResponse the transactionResponse to set
	 */
	public void setTransactionResponse(TransactionResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}
	
	/**
	 * @return the cardProduct
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "card_product_id")
	public GeneralCardProduct getCardProduct() {
		return cardProduct;
	}
	
	/**
	 * @param cardProduct the cardProduct to set
	 */
	public void setCardProduct(GeneralCardProduct cardProduct) {
		this.cardProduct = cardProduct;
	}
	
	/**
	 * @return the retried
	 */
	public boolean isRetried() {
		return retried;
	}
	
	/**
	 * @param retried the retried to set
	 */
	public void setRetried(boolean retried) {
		this.retried = retried;
	}
	
	/**
	 * @return the retryAttempts
	 */
	@Column(name="retry_attempts")
	public int getRetryAttempts() {
		return retryAttempts;
	}
	
	/**
	 * @param retryAttempts the retryAttempts to set
	 */
	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}
	
	/**
	 * @return the secToken
	 */
	@Column(name="sec_token")
	public String getSecToken() {
		return secToken;
	}

	/**
	 * @param secToken the secToken to set
	 */
	public void setSecToken(String secToken) {
		this.secToken = secToken;
	}
	
	/**
	 * @return the parameters
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="transaction_id")
	public Set<TransactionParameter> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(Set<TransactionParameter> parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * @return the responseParameters
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="transaction_id")
	public Set<TransactionResponseParameter> getResponseParameters() {
		return responseParameters;
	}

	/**
	 * @param responseParameters the responseParameters to set
	 */
	public void setResponseParameters(Set<TransactionResponseParameter> responseParameters) {
		this.responseParameters = responseParameters;
	}
	
	/**
	 * @return the batchHistory
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "batch_history_id")
	public BatchHistory getBatchHistory() {
		return batchHistory;
	}

	/**
	 * @param batchHistory the batchHistory to set
	 */
	public void setBatchHistory(BatchHistory batchHistory) {
		this.batchHistory = batchHistory;
	}

	/**
	 * @return the batchNumber
	 */
	@Column(name="batch_number")
	public String getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber the batchNumber to set
	 */
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	/**
	 * @return the refNumber
	 */
	@Column(name="ref_number")
	public String getRefNumber() {
		return refNumber;
	}

	/**
	 * @param refNumber the refNumber to set
	 */
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	
	/**
	 * @return the currentBatch
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "current_batch_id")
	public CurrentBatch getCurrentBatch() {
		return currentBatch;
	}

	/**
	 * @param currentBatch the currentBatch to set
	 */
	public void setCurrentBatch(CurrentBatch currentBatch) {
		this.currentBatch = currentBatch;
	}

	/**
	 * Genera el hash que identifica de manera unica una transaccion
	 */
	private void generateTransactionHash() {
	MessageDigest md;
	try {
		md = MessageDigest.getInstance("MD5");
		md.update((byte)this.terminal.getId());
		if(this.terminalBankId != null) md.update(this.terminalBankId.getBytes());
		if(this.merchantIdCode != null) md.update(this.merchantIdCode.getBytes());
		md.update((byte)this.amount);
		md.update((byte)this.txnDate.hashCode());
		md.update(this.controlToken.getBytes());
		md.update((byte)ThreadLocalRandom.current().nextInt(1, 1001));
		String guid = DatatypeConverter.printHexBinary(md.digest());
		this.hashCode = guid.substring(0, guid.length()/2);
	} catch (NoSuchAlgorithmException e) {
		
	}
}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((cardProduct == null) ? 0 : cardProduct.hashCode());
		result = prime * result + ((controlToken == null) ? 0 : controlToken.hashCode());
		result = prime * result + ((hashCode == null) ? 0 : hashCode.hashCode());
		result = prime * result + id;
		result = prime * result + ((merchantIdCode == null) ? 0 : merchantIdCode.hashCode());
		result = prime * result + (retried ? 1231 : 1237);
		result = prime * result + retryAttempts;
		result = prime * result + ((secToken == null) ? 0 : secToken.hashCode());
		result = prime * result + ((terminal == null) ? 0 : terminal.hashCode());
		result = prime * result + ((terminalBankId == null) ? 0 : terminalBankId.hashCode());
		result = prime * result + ((transactionResponse == null) ? 0 : transactionResponse.hashCode());
		result = prime * result + ((transactionStatus == null) ? 0 : transactionStatus.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		result = prime * result + ((txnDate == null) ? 0 : txnDate.hashCode());
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if (cardProduct == null) {
			if (other.cardProduct != null)
				return false;
		} else if (!cardProduct.equals(other.cardProduct))
			return false;
		if (controlToken == null) {
			if (other.controlToken != null)
				return false;
		} else if (!controlToken.equals(other.controlToken))
			return false;
		if (hashCode == null) {
			if (other.hashCode != null)
				return false;
		} else if (!hashCode.equals(other.hashCode))
			return false;
		if (id != other.id)
			return false;
		if (merchantIdCode == null) {
			if (other.merchantIdCode != null)
				return false;
		} else if (!merchantIdCode.equals(other.merchantIdCode))
			return false;
		if (retried != other.retried)
			return false;
		if (retryAttempts != other.retryAttempts)
			return false;
		if (secToken == null) {
			if (other.secToken != null)
				return false;
		} else if (!secToken.equals(other.secToken))
			return false;
		if (terminal == null) {
			if (other.terminal != null)
				return false;
		} else if (!terminal.equals(other.terminal))
			return false;
		if (terminalBankId == null) {
			if (other.terminalBankId != null)
				return false;
		} else if (!terminalBankId.equals(other.terminalBankId))
			return false;
		if (transactionResponse == null) {
			if (other.transactionResponse != null)
				return false;
		} else if (!transactionResponse.equals(other.transactionResponse))
			return false;
		if (transactionStatus != other.transactionStatus)
			return false;
		if (transactionType == null) {
			if (other.transactionType != null)
				return false;
		} else if (!transactionType.equals(other.transactionType))
			return false;
		if (txnDate == null) {
			if (other.txnDate != null)
				return false;
		} else if (!txnDate.equals(other.txnDate))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", hashCode=" + hashCode + ", terminal=" + terminal + ", terminalBankId="
				+ terminalBankId + ", merchantIdCode=" + merchantIdCode + ", amount=" + amount + ", txnDate=" + txnDate
				+ ", controlToken=" + controlToken + ", transactionStatus=" + transactionStatus + ", transactionType="
				+ transactionType + ", transactionResponse=" + transactionResponse + ", cardProduct=" + cardProduct
				+ ", retried=" + retried + ", retryAttempts=" + retryAttempts + ", secToken=" + secToken + "]";
	}
	
}
