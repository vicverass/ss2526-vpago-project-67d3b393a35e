package vpago.core.transactions.classes;

import com.solab.iso8583.CustomField;
import com.solab.iso8583.IsoType;
import com.solab.iso8583.IsoValue;
import com.solab.iso8583.codecs.CompositeField;

/**
 * 
 */
public class CompositeFieldInfo {
	
	public CompositeFieldInfo(CompositeField compField) {
		super();
		this.compField = compField;
		this.totalLen = 0;
		this.compFieldSize = 0;
	}
	
	private CompositeField compField;
	private Integer totalLen;
	private Integer compFieldSize;
	
	public <T> CompositeField addValue(T val,
            CustomField<T> encoder,
            IsoType t,
            int length) {
		this.compFieldSize++;
		this.totalLen += length;
		return this.compField.addValue(val, encoder, t, length);
	}
	
	public CompositeField addValue(IsoValue<?> v) {
		this.compFieldSize++;
		this.totalLen += v.getLength();
		return this.compField.addValue(v);
	}
	
	/**
	 * @return the compField
	 */
	public CompositeField getCompField() {
		return compField;
	}
	
	/**
	 * @param compField the compField to set
	 */
	public void setCompField(CompositeField compField) {
		this.compField = compField;
	}
	
	/**
	 * @return the totalLen
	 */
	public Integer getTotalLen() {
		return totalLen;
	}
	
	/**
	 * @param totalLen the totalLen to set
	 */
	public void setTotalLen(Integer totalLen) {
		this.totalLen = totalLen;
	}
	
	/**
	 * @return the compFieldSize
	 */
	public Integer getCompFieldSize() {
		return compFieldSize;
	}
	
	/**
	 * @param compFieldSize the compFieldSize to set
	 */
	public void setCompFieldSize(Integer compFieldSize) {
		this.compFieldSize = compFieldSize;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CompositeFieldInfo [compField=" + compField + ", totalLen=" + totalLen + ", compFieldSize="
				+ compFieldSize + "]";
	}
	
	
}
