package vpago.core.transactions.classes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Clase persistente para representar un Producto de Tarjeta de Banco
 */
@Entity
@DiscriminatorValue("bank")
public class BankCardProduct extends GeneralCardProduct {

	public BankCardProduct() {
		super();
	}

	public BankCardProduct(CardFranchise cardFranchise, CardType cardType, Bin productBin, Bank cardBank) {
		super(cardFranchise, cardType, productBin);
		this.cardBank = cardBank;
	}
	
	/** Banco emisor de la tarjeta */
	private Bank cardBank;
	
	/**
	 * @return the cardBank
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bank_id")
	public Bank getCardBank() {
		return cardBank;
	}

	/**
	 * @param cardBank the cardBank to set
	 */
	public void setCardBank(Bank cardBank) {
		this.cardBank = cardBank;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BankCardProduct [id=" + id + ", cardFranchise=" + cardFranchise + ", cardType=" + cardType
				+ ", productBin=" + productBin + ", cardBank=" + cardBank + "]";
	}

}
