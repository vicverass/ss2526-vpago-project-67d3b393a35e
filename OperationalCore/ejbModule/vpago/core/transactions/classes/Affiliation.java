package vpago.core.transactions.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar una Afiliacion
 */
@Entity
@Table(name="affiliation")
public class Affiliation {
	
	public Affiliation() {
		super();
	}
	
	public Affiliation(Merchant merchant, PaymentChannel paymentChannel) {
		super();
		this.merchant = merchant;
		this.paymentChannel = paymentChannel;
		this.active = false;
		this.activeRuleSet = null;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Comercio afiliado */
	private Merchant merchant;
	/** Identificador del Canal de Pago al cual esta afiliado el Comercio */
	private PaymentChannel paymentChannel;
	/** Indica si el Comercio esta activo para este Canal de Pago */
	private boolean active;
	/** Conjunto de Reglas activo del Comercio para esta Afiliacion */
	private RuleSet activeRuleSet;
	/** Conjunto de Adquirientes asociados a la Afiliacion */
	private List<Acquirer> acquirers;
	/** Conjuntos de Reglas asociados a la Afiliacion */
	private List<RuleSet> ruleSets;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="affiliation_id_seq")
	@SequenceGenerator(name="affiliation_id_seq", sequenceName = "affiliation_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the merchant
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "merchant_id")
	public Merchant getMerchant() {
		return merchant;
	}
	
	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
	/**
	 * @return the paymentChannel
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}
	
	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the active
	 */
	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the activeRuleSet
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "active_rule_set_id")
	public RuleSet getActiveRuleSet() {
		return activeRuleSet;
	}
	
	/**
	 * @param activeRuleSet the activeRuleSet to set
	 */
	public void setActiveRuleSet(RuleSet activeRuleSet) {
		this.activeRuleSet = activeRuleSet;
	}
	
	/**
	 * @return the acquirers
	 */
	@OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="affiliation_id")
	public List<Acquirer> getAcquirers() {
		return acquirers;
	}

	/**
	 * @param acquirers the acquirers to set
	 */
	public void setAcquirers(List<Acquirer> acquirers) {
		this.acquirers = acquirers;
	}
	
	/**
	 * @return the ruleSets
	 */
	@OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="affiliation_id")
	public List<RuleSet> getRuleSets() {
		return ruleSets;
	}

	/**
	 * @param ruleSets the ruleSets to set
	 */
	public void setRuleSets(List<RuleSet> ruleSets) {
		this.ruleSets = ruleSets;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Affiliation [id=" + id + ", merchant=" + merchant.getId() + ", paymentChannel=" + paymentChannel.getId() + ", active="
				+ active + ", activeRuleSet=" + activeRuleSet.getId() + ", acquirers=" + acquirers + "]";
	}


}
