package vpago.core.transactions.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "transaction_header")
public class TransactionHeader {
	
	public TransactionHeader() {
		super();
	}

	public TransactionHeader(Integer id, Integer characterIndex, String tgC0, String tgC1, String tgC4,
			String serialDevice, String messageHeader, String tg9f1c, String tgDf8330) {
		super();
		this.id = id;
		this.characterIndex = characterIndex;
		this.tgC0 = tgC0;
		this.tgC1 = tgC1;
		this.tgC4 = tgC4;
		this.serialDevice = serialDevice;
		this.messageHeader = messageHeader;
		this.tg9f1c = tg9f1c;
		this.tgDf8330 = tgDf8330;
	}

	private Integer id;
	private Integer characterIndex;
	private String tgC0;
	private String tgC1;
	private String tgC4;
	private String serialDevice;
	private String messageHeader;
	private String tg9f1c;
	private String tgDf8330;
	
	@Id @GeneratedValue(generator="transaction_header_id_seq")
	@SequenceGenerator(name="transaction_header_id_seq", sequenceName = "transaction_header_id_seq")
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="character_index")
	public Integer getCharacterIndex() {
		return characterIndex;
	}

	public void setCharacterIndex(Integer characterIndex) {
		this.characterIndex = characterIndex;
	}

	@Column(name="tag_c0")
	public String getTgC0() {
		return tgC0;
	}

	public void setTgC0(String tgC0) {
		this.tgC0 = tgC0;
	}

	@Column(name="tag_c1")
	public String getTgC1() {
		return tgC1;
	}

	public void setTgC1(String tgC1) {
		this.tgC1 = tgC1;
	}

	@Column(name="tag_c4")
	public String getTgC4() {
		return tgC4;
	}

	public void setTgC4(String tgC4) {
		this.tgC4 = tgC4;
	}

	@Column(name="serial_device")
	public String getSerialDevice() {
		return serialDevice;
	}

	public void setSerialDevice(String serialDevice) {
		this.serialDevice = serialDevice;
	}

	@Column(name="message_header")
	public String getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}

	@Column(name="tag_9f1c")
	public String getTg9f1c() {
		return tg9f1c;
	}

	public void setTg9f1c(String tg9f1c) {
		this.tg9f1c = tg9f1c;
	}

	@Column(name="tag_df8330")
	public String getTgDf8330() {
		return tgDf8330;
	}

	public void setTgDf8330(String tgDf8330) {
		this.tgDf8330 = tgDf8330;
	}

	@Override
	public String toString() {
		return "TransactionHeader [id=" + id + ", characterIndex=" + characterIndex + ", tgC0=" + tgC0 + ", tgC1="
				+ tgC1 + ", tgC4=" + tgC4 + ", serialDevice=" + serialDevice + ", messageHeader=" + messageHeader
				+ ", tg9f1c=" + tg9f1c + ", tgDf8330=" + tgDf8330 + "]";
	}

}
