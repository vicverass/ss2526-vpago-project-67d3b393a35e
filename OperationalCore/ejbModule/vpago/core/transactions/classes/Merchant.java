package vpago.core.transactions.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;


/**
 * Clase persistente para representar un Comercio
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
	name="merchant_type",
	discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue("simple")
@Table(name="merchant")
public class Merchant {
	
	public Merchant() {
		super(); 
	}
	
	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Identificador unico del Comercio en la base de datos */
	protected String merchantGuid;
	/** Nombre del comercio */
	protected String name;
	/** Registro de Identificacion Fiscal del Comercio **/
	protected String rif;
	/** Nombre fantasia */
	protected String fantasyName;
	/** Comercio activo */
	protected boolean active;
	/** Afiliaciones del Comercio */
	protected List<Affiliation> affiliations;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="merchant_id_seq")
	@SequenceGenerator(name="merchant_id_seq", sequenceName = "merchant_id_seq")
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the merchantGuid
	 */
	@XmlElement
	@Column(name="merchant_guid")
	public String getMerchantGuid() {
		return merchantGuid;
	}
	/**
	 * @param merchantGuid the merchantGuid to set
	 */
	public void setMerchantGuid(String merchantGuid) {
		this.merchantGuid = merchantGuid;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the rif
	 */
	public String getRif() {
		return rif;
	}
	/**
	 * @param rif the rif to set
	 */
	public void setRif(String rif) {
		this.rif = rif;
	}

	/**
	 * @return the fantasyName
	 */
	@Column(name="fantasy_name")
	public String getFantasyName() {
		return fantasyName;
	}

	/**
	 * @param fantasyName the fantasyName to set
	 */
	public void setFantasyName(String fantasyName) {
		this.fantasyName = fantasyName;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the affiliations
	 */
	@OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name="merchant_id")
	public List<Affiliation> getAffiliations() {
		return affiliations;
	}

	/**
	 * @param affiliations the affiliations to set
	 */
	public void setAffiliations(List<Affiliation> affiliations) {
		this.affiliations = affiliations;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Merchant [id=" + id + ", merchantGuid=" + merchantGuid + ", name=" + name + ", rif=" + rif
				+ ", fantasyName=" + fantasyName + ", active=" + active + "]";
	}

	
}