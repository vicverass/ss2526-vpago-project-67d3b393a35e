package vpago.core.transactions.classes;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="batch_history")
public class BatchHistory {

	public BatchHistory() {
		super();
	}
	
	public BatchHistory(Terminal terminal, Integer batchNumber, Double totalAmount,
			Integer totalTrx, BatchType batchType, Calendar closingTimestamp) {
		super();
		this.terminal = terminal;
		this.batchNumber = batchNumber;
		this.totalAmount = totalAmount;
		this.totalTrx = totalTrx;
		this.batchType = batchType;
		this.closingTimestamp = closingTimestamp;
	}

	/** Identificador generado por el motor base de datos */
	private Integer id;
	/** Terminal **/
	private Terminal terminal;
	/** Numero de lote **/
	private Integer batchNumber;
	/** Monto total **/
	private Double totalAmount;
	/** Numero total de transacciones **/
	private Integer totalTrx;
	/** Tipo de lote **/
	private BatchType batchType;
	/** Marca de tiempo de cierre **/
	private Calendar closingTimestamp;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="batch_history_id_seq")
	@SequenceGenerator(name="batch_history_id_seq", sequenceName = "batch_history_id_seq")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the terminal
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "terminal_id")
	public Terminal getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal the terminal to set
	 */
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	/**
	 * @return the batchNumber
	 */
	@Column(name="batch_number")
	public Integer getBatchNumber() {
		return batchNumber;
	}

	/**
	 * @param batchNumber the batchNumber to set
	 */
	public void setBatchNumber(Integer batchNumber) {
		this.batchNumber = batchNumber;
	}


	/**
	 * @return the totalAmount
	 */
	@Column(name="total_amount")
	public Double getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the totalTrx
	 */
	@Column(name="total_trx")
	public Integer getTotalTrx() {
		return totalTrx;
	}

	/**
	 * @param totalTrx the totalTrx to set
	 */
	public void setTotalTrx(Integer totalTrx) {
		this.totalTrx = totalTrx;
	}

	/**
	 * @return the batchType
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "batch_type_id")
	public BatchType getBatchType() {
		return batchType;
	}

	/**
	 * @param batchType the batchType to set
	 */
	public void setBatchType(BatchType batchType) {
		this.batchType = batchType;
	}

	/**
	 * @return the closingTimestamp
	 */
	@Column(name="closing_timestamp")
	public Calendar getClosingTimestamp() {
		return closingTimestamp;
	}

	/**
	 * @param closingTimestamp the closingTimestamp to set
	 */
	public void setClosingTimestamp(Calendar closingTimestamp) {
		this.closingTimestamp = closingTimestamp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BatchHistory [id=" + id + ", terminal=" + terminal + ", batchNumber=" + batchNumber + ", totalAmount="
				+ totalAmount + ", totalTrx=" + totalTrx + ", batchType=" + batchType + ", closingTimestamp="
				+ closingTimestamp + "]";
	}
    
}
