package vpago.core.transactions.classes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="device_type")
public class DeviceType {

	public DeviceType() { 
		super(); 
	}
	
	public DeviceType(String name) {
		super();
		this.name = name;
	}
	
	public void update(DeviceType other) {
		this.name = this.name != other.name ? other.name : this.name;
	}
	
	/** Id generado por el motor de base de datos */
	private int id;
	/** Nombre del Tipo de Dispositivo */
	private String name;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="device_type_id_seq")
	@SequenceGenerator(name="device_type_id_seq", sequenceName = "device_type_id_seq")
	@NotNull
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceType other = (DeviceType) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DeviceType [id=" + id + ", name=" + name + "]";
	}

	
}

