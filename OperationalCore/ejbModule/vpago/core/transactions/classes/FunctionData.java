package vpago.core.transactions.classes;

import java.util.Map;

/**
 * 
 */
public class FunctionData {
	
	public FunctionData(String terminalId, String channelCode, String functionType,
			Map<String, String> transactionParameters) {
		super();
		this.terminalId = terminalId;
		this.channelCode = channelCode;
		this.functionType = functionType;
		this.transactionParameters = transactionParameters;
	}
	
	/** Identificador generado por el motor base de datos */
	private String terminalId;
	/** Codigo del canal de pago **/
	private String channelCode;
	/** Tipo de funcion **/
	private String functionType;
	/** Argumentos necesarios para la ejecucion de la funcion **/
	private Map<String, String> transactionParameters;
	
	/**
	 * @return the terminalId
	 */
	public String getTerminalId() {
		return terminalId;
	}
	
	/**
	 * @param terminalId the terminalId to set
	 */
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	
	/**
	 * @return the channelCode
	 */
	public String getChannelCode() {
		return channelCode;
	}
	
	/**
	 * @param channelCode the channelCode to set
	 */
	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}
	
	/**
	 * @return the functionType
	 */
	public String getFunctionType() {
		return functionType;
	}
	
	/**
	 * @param functionType the functionType to set
	 */
	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}
	
	/**
	 * @return the transactionParameters
	 */
	public Map<String, String> getTransactionParameters() {
		return transactionParameters;
	}
	
	/**
	 * @param transactionParameters the transactionParameters to set
	 */
	public void setTransactionParameters(Map<String, String> transactionParameters) {
		this.transactionParameters = transactionParameters;
	}
	
}
