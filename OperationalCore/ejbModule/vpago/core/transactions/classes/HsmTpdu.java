package vpago.core.transactions.classes;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar una Afiliacion
 */
@Entity
@Table(name="hsm_tpdu")
public class HsmTpdu {
	
	public HsmTpdu() {
		super();
	}
	

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador del Hsm */
	private Hsm hsm;
	/** Identificador del Canal de Pago*/
	private PaymentChannel paymentChannel;
	/** TPDU del Canal de Pago para operaciones con HSM */
	private String tpdu;
	/** Indica si el Comercio esta activo para este Canal de Pago */
	private boolean active;
	
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="hsm_tpdu_id_seq")
	@SequenceGenerator(name="hsm_tpdu_id_seq", sequenceName = "hsm_tpdu_id_seq")
	@Column(name="id")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the hsm
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hsm_id")
	public Hsm getHsm() {
		return hsm;
	}
	
	/**
	 * @param hsm the hsm to set
	 */
	public void setHsm(Hsm hsm) {
		this.hsm = hsm;
	}
	
	/**
	 * @return the paymentChannel
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "payment_channel_id")
	public PaymentChannel getPaymentChannel() {
		return paymentChannel;
	}
	
	/**
	 * @param paymentChannel the paymentChannel to set
	 */
	public void setPaymentChannel(PaymentChannel paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	
	/**
	 * @return the tpdu
	 */
	@Column(name="tpdu")
	public String getTpdu() {
		return tpdu;
	}

	/**
	 * @param tpdu the tpdu to set
	 */
	public void setTpdu(String tpdu) {
		this.tpdu = tpdu;
	}

	/**
	 * @return the active
	 */
	@Column(name="active")
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HsmTpdu [id=" + id + ", hsm=" + hsm + ", paymentChannel=" + paymentChannel + ", tpdu=" + tpdu
				+ ", active=" + active + "]";
	}
	
}
