package vpago.core.transactions.classes;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="assigned_device")
public class AssignedDevice {

	public AssignedDevice() {
		super();
	}

	public AssignedDevice(Integer id, String serialNumber, Device deviceId) {
		super();
		this.id = id;
		this.serialNumber = serialNumber;
		this.deviceId = deviceId;
	}

	private Integer id;
	private String serialNumber;
	private Device deviceId;
	
	/**
	 * 
	 * @return the id
	 */
	@Id @GeneratedValue(generator="assigned_device_id_seq")
	@SequenceGenerator(name="assigned_device_id_seq", sequenceName = "assigned_device_id_seq")
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return the serialNumber
	 */
	@Column(name="serial_number")
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * 
	 * @param serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * 
	 * @return the device
	 */
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="device_id")
	public Device getDeviceId() {
		return deviceId;
	}

	/**
	 * 
	 * @param deviceId
	 */
	public void setDeviceId(Device deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "AssignedDevice [id=" + id + ", serialNumber=" + serialNumber + ", deviceId=" + deviceId + "]";
	}

	
	
}
