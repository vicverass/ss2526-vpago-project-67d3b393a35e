package vpago.core.transactions.classes;

import java.util.Map;

public class FunctionResponse {
	
	public FunctionResponse(Map<String, String> functionParameters) {
		super();
		this.functionParameters = functionParameters;
	}

	private Map<String, String> functionParameters;

	/**
	 * @return the functionParameters
	 */
	public Map<String, String> getFunctionParameters() {
		return functionParameters;
	}

	/**
	 * @param functionParameters the functionParameters to set
	 */
	public void setFunctionParameters(Map<String, String> functionParameters) {
		this.functionParameters = functionParameters;
	}
	
	
}
