package vpago.core.transactions.classes;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar un Conjunto de Reglas
 */
@Entity
@Table(name="rule_set")
public class RuleSet {
	
	public RuleSet() {
		super();
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Rango de tiempo */
	protected TimeFrame timeFrame;
	/** Afiliacion a la que esta asociado el Conjunto de Reglas */
	private Affiliation affiliation;
	/** Reglas asociadas al Conjunto de Reglas */
	private Set<Rule> rules;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="rule_set_id_seq")
	@SequenceGenerator(name="rule_set_id_seq", sequenceName = "rule_set_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	/**
	 * @return the timeFrame
	 */
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "time_frame_id")
	public TimeFrame getTimeFrame() {
		return timeFrame;
	}
	
	/**
	 * @param timeFrame the timeFrame to set
	 */
	public void setTimeFrame(TimeFrame timeFrame) {
		this.timeFrame = timeFrame;
	}

	/**
	 * @return the affiliation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}
	
	/**
	 * @return the rules
	 */
	@ManyToMany(
		targetEntity=Rule.class,
		cascade={CascadeType.PERSIST, CascadeType.MERGE}
	)
	@JoinTable(
		name="rule_set_rule",
		joinColumns= @JoinColumn(name="rule_set_id"),
		inverseJoinColumns=@JoinColumn(name="rule_id")
	)
	public Set<Rule> getRules() {
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}

//	public Rule currentRule() {
//		
//	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleSet [id=" + id + ", displayName=" + displayName + ", rules=" + rules + "]";
	}
	
}
