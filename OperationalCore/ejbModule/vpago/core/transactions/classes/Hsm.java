package vpago.core.transactions.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Clase persistente para representar un Banco
 */
@Entity
@Table(name="hsm")
public class Hsm {
	
	public Hsm() {
		super();
	}
	
	public Hsm(int id, String ipAddress, Integer port, String schemeHsm, String schemeHsmAcronym, String messageHeader,
			String keyType, String key, String ksnDescriptor, Boolean active, Integer kin) {
		super();
		this.id = id;
		this.ipAddress = ipAddress;
		this.port = port;
		this.schemeHsm = schemeHsm;
		this.schemeHsmAcronym = schemeHsmAcronym;
		this.messageHeader = messageHeader;
		this.keyType = keyType;
		this.key = key;
		this.ksnDescriptor = ksnDescriptor;
		this.active = active;
		this.kin = kin;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Ip de la Conexi�n */
	private String ipAddress;
	/** Puerto de la conexi�n */
	private Integer port;
	/** Esquema del HSM */
	private String schemeHsm;
	/** Acronimo del esquema del HSM */
	private String schemeHsmAcronym;
	/** Encabezado del mensaje para el HSM */
	private String messageHeader;
	/** Tipo de llave */
	private String keyType;
	/** Llave **/
	private String key;
	/** Descriptor del KSN **/
	private String ksnDescriptor;
	/** Estado del KSN **/
	private Boolean active;
	/** KIN **/
	private Integer kin;
	/** Impresion de log activa **/
	private boolean printLog;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="hsm_id_seq")
	@SequenceGenerator(name="hsm_id_seq", sequenceName = "hsm_id_seq")
	@Column(name="id")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the ipAddress
	 */
	@Column(name="ip_address")
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the port
	 */
	@Column(name="port")
	public Integer getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * @return the schemeHsm
	 */
	@Column(name="scheme_hsm")
	public String getSchemeHsm() {
		return schemeHsm;
	}

	/**
	 * @param schemeHsm the schemeHsm to set
	 */
	public void setSchemeHsm(String schemeHsm) {
		this.schemeHsm = schemeHsm;
	}

	/**
	 * @return the schemeHsmAcronym
	 */
	@Column(name="scheme_hsm_acronym")
	public String getSchemeHsmAcronym() {
		return schemeHsmAcronym;
	}

	/**
	 * @param schemeHsmAcronym the schemeHsmAcronym to set
	 */
	public void setSchemeHsmAcronym(String schemeHsmAcronym) {
		this.schemeHsmAcronym = schemeHsmAcronym;
	}
	
	/**
	 * @return the messageHeader
	 */
	@Column(name="message_header")
	public String getMessageHeader() {
		return messageHeader;
	}

	/**
	 * @param messageHeader the messageHeader to set
	 */
	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}

	/**
	 * @return the keyType
	 */
	@Column(name="key_type")
	public String getKeyType() {
		return keyType;
	}

	/**
	 * @param keyType the keyType to set
	 */
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}

	/**
	 * @return the key
	 */
	@Column(name="key")
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the ksnDescriptor
	 */
	@Column(name="ksn_descriptor")
	public String getKsnDescriptor() {
		return ksnDescriptor;
	}

	/**
	 * @param ksnDescriptor the ksnDescriptor to set
	 */
	public void setKsnDescriptor(String ksnDescriptor) {
		this.ksnDescriptor = ksnDescriptor;
	}

	/**
	 * @return the active
	 */
	@Column(name="active")
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the kin
	 */
	public Integer getKin() {
		return kin;
	}

	/**
	 * @param kin the kin to set
	 */
	public void setKin(Integer kin) {
		this.kin = kin;
	}
	
	/**
	 * @return the printLog
	 */
	@Column(name="print_log")
	public boolean isPrintLog() {
		return printLog;
	}

	/**
	 * @param printLog the printLog to set
	 */
	public void setPrintLog(boolean printLog) {
		this.printLog = printLog;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Hsm [id=" + id + ", ipAddress=" + ipAddress + ", port=" + port + ", schemeHsm=" + schemeHsm
				+ ", schemeHsmAcronym=" + schemeHsmAcronym + ", messageHeader=" + messageHeader + ", keyType=" + keyType
				+ ", key=" + key + ", ksnDescriptor=" + ksnDescriptor + ", active=" + active + "]";
	}
	
}
