package vpago.core.transactions.classes.types;

/**
 * Tipo enumerado para representar los Tipos de Carga de Parametros para una transaccion
 */
public enum FieldLoadType {
	/** Obtenido de la base de datos a traves de TransactionParameter y TransactionResponseParameter */
	DATABASE,		
	/** Enviado en la invocacion al servicio web correspondiente */
	WEBSERVICE,		
	/** Almacenado como valor fijo en la base de datos a traves TemplateField o TemplateSubField */
	FIXED,			 
	/** Calculado u obtenido utilizando una funcion determinada */
	COMPUTED,		
	/** Carga delegada a los subcampos */
	SUBFIELD,		
	/** Campos relacionados al manejo de reglas del comercio */
	RULE,			
	/** Ignorar en el momento de la carga */
	NONE,
	/** Se carga de la solicitud de la ultima transaccion aprobada **/
	LASTTRXREQ,
	/** Se carga de la respuesta de la ultima transaccion aprobada **/
	LASTTRXRESP,
	/** Se carga de la solicitud de la ultima transaccion ejecutada **/
	LASTEXECTRXREQ,
	/** Se carga de la respuesta de la ultima transaccion ejecutada **/
	LASTEXECTRXRESP,
	/** Se carga de la solicitud de una transaccion determinada **/
	PREVTRXREQ,
	/** Se carga de la respuesta de una transaccion determinada **/
	PREVTRXRESP,
	/** Representa un parametro de lote **/
	BATCH
}
