package vpago.core.transactions.classes.types;

/**
 * Tipo enumerado para representar los tipos de funciones de envio
 */
public enum SendFuncType {
	/** Transaccion comun **/
	TRANSACTION,
	/** Transaccion de confirmacion **/
	CONFIRMATION,
	/** Transaccion inversa **/
	INVERSE
}
