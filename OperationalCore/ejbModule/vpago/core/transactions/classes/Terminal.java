package vpago.core.transactions.classes;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;


/**
 * Clase persistente para representar un Terminal
 *
 */
@Entity
@Table(name="terminal")
public class Terminal {

	public Terminal() {
		super();
	}
	
	/** Identificador generado por el motor base de datos */
	private int id;
	/** Identificador unico del terminal en la base de datos */
	private String terminalGuid;
	/** Llave para autenticar al terminal */
	private String accessKey;
	/** Terminal activo */
	private boolean active;
	/** Afiliacion a la que esta asociado el Terminal */
	private Affiliation affiliation;
	/** Lista de terminales bancarios */
	private List<TerminalParameter> terminalParameters;
	/** Ultima transaccion aprobada **/
	private Transaction lastApprvTrx;
	/** Ultima transaccion **/
	private Transaction lastExecTrx;
	/** Inversa pendiente **/
	private boolean pendingReverse;
	/** Confirmacion pendiente **/
	private boolean pendingConfirmation;
	/** Ultima transaccion reversible **/
	private Transaction lastRevTrx;
	/** Ultima transaccion confirmable **/
	private Transaction lastConfTrx;
	/** Contador de trace **/
	private Integer traceCounter;
	/** Lotes actuales **/
	private List<CurrentBatch> currentBatchs;
	/** Dispositivo asignado **/
	private AssignedDevice assignedDevice;
	/** Fecha de inicio de operaciones */
	private Calendar startDateOperations;
	/** Fecha de ultima transaccion **/
	private Calendar lastTransactionDate;
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="terminal_id_seq")
	@SequenceGenerator(name="terminal_id_seq", sequenceName = "terminal_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the terminalGuid
	 */
	@Column(name="terminal_guid")
	@NotNull
	public String getTerminalGuid() {
		return terminalGuid;
	}

	/**
	 * @param terminalGuid the terminalGuid to set
	 */
	@SuppressWarnings("unused")
	private void setTerminalGuid(String terminalGuid) {
		this.terminalGuid = terminalGuid;
	}

	/**
	 * @return the accessKey
	 */
	@Column(name="access_key")
	@NotNull
	public String getAccessKey() {
		return accessKey;
	}

	/**
	 * @param accessKey the accessKey to set
	 */
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	/**
	 * @return the active
	 */
	@NotNull
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * @return the affiliation
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "affiliation_id", insertable=false, updatable=false)
	public Affiliation getAffiliation() {
		return affiliation;
	}

	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(Affiliation affiliation) {
		this.affiliation = affiliation;
	}
	
	/**
	 * @return the terminalParameters
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="terminal_id")
	public List<TerminalParameter> getTerminalParameters() {
		return terminalParameters;
	}

	/**
	 * @param terminalParameters the terminalParameters to set
	 */
	public void setTerminalParameters(List<TerminalParameter> terminalParameters) {
		this.terminalParameters = terminalParameters;
	}
	
	/**
	 * @return the lastApprvTrx
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="last_appr_trx_id")
	public Transaction getLastApprvTrx() {
		return lastApprvTrx;
	}

	/**
	 * @param lastApprvTrx the lastApprvTrx to set
	 */
	public void setLastApprvTrx(Transaction lastApprvTrx) {
		this.lastApprvTrx = lastApprvTrx;
	}

	/**
	 * @return the lastExecTrx
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="last_exec_trx_id")
	public Transaction getLastExecTrx() {
		return lastExecTrx;
	}

	/**
	 * @param lastExecTrx the lastExecTrx to set
	 */
	public void setLastExecTrx(Transaction lastExecTrx) {
		this.lastExecTrx = lastExecTrx;
	}

	/**
	 * @return the pendingReverse
	 */
	@Column(name="pending_reverse")
	public boolean isPendingReverse() {
		return pendingReverse;
	}

	/**
	 * @param pendingReverse the pendingReverse to set
	 */
	public void setPendingReverse(boolean pendingReverse) {
		this.pendingReverse = pendingReverse;
	}

	/**
	 * @return the pendingConfirmation
	 */
	@Column(name="pending_confirmation")
	public boolean isPendingConfirmation() {
		return pendingConfirmation;
	}

	/**
	 * @param pendingConfirmation the pendingConfirmation to set
	 */
	public void setPendingConfirmation(boolean pendingConfirmation) {
		this.pendingConfirmation = pendingConfirmation;
	}
	
	/**
	 * @return the lastRevTrx
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="last_rev_trx_id")
	public Transaction getLastRevTrx() {
		return lastRevTrx;
	}

	/**
	 * @param lastRevTrx the lastRevTrx to set
	 */
	public void setLastRevTrx(Transaction lastRevTrx) {
		this.lastRevTrx = lastRevTrx;
	}
	
	/**
	 * @return the lastConfTrx
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="last_conf_trx_id")
	public Transaction getLastConfTrx() {
		return lastConfTrx;
	}

	/**
	 * @param lastConfTrx the lastConfTrx to set
	 */
	public void setLastConfTrx(Transaction lastConfTrx) {
		this.lastConfTrx = lastConfTrx;
	}
	
	/**
	 * @return the traceCounter
	 */
	@Column(name="trace_counter")
	public Integer getTraceCounter() {
		return traceCounter;
	}

	/**
	 * @param traceCounter the traceCounter to set
	 */
	public void setTraceCounter(Integer traceCounter) {
		this.traceCounter = traceCounter;
	}
	
	/**
	 * @return the currentBatchs
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="terminal_id")
	public List<CurrentBatch> getCurrentBatchs() {
		return currentBatchs;
	}

	/**
	 * @param currentBatchs the currentBatchs to set
	 */
	public void setCurrentBatchs(List<CurrentBatch> currentBatchs) {
		this.currentBatchs = currentBatchs;
	}

	/**
	 * 	
	 * @return the assignedDevice
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="assigned_device_id")
	public AssignedDevice getAssignedDevice() {
		return assignedDevice;
	}

	/**
	 * 
	 * @param assignedDevice
	 */
	public void setAssignedDevice(AssignedDevice assignedDevice) {
		this.assignedDevice = assignedDevice;
	}

	/**
	 * 
	 * @return the startDateOperations
	 */
	@Column(name="start_date_operations")
	public Calendar getStartDateOperations() {
		return startDateOperations;
	}

	/**
	 * 
	 * @param startDateOperations
	 */
	public void setStartDateOperations(Calendar startDateOperations) {
		this.startDateOperations = startDateOperations;
	}

	/**
	 * 
	 * @return the lastTransactionDate
	 */
	@Column(name = "last_transaction_date")	
	public Calendar getLastTransactionDate() {
		return lastTransactionDate;
	}

	/**
	 * 
	 * @param lastTransactionDate
	 */
	public void setLastTransactionDate(Calendar lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	@Override
	public String toString() {
		return "Terminal [id=" + id + ", terminalGuid=" + terminalGuid + ", accessKey=" + accessKey + ", active="
				+ active + ", affiliation=" + affiliation + ", terminalParameters=" + terminalParameters
				+ ", lastApprvTrx=" + lastApprvTrx + ", lastExecTrx=" + lastExecTrx + ", pendingReverse="
				+ pendingReverse + ", pendingConfirmation=" + pendingConfirmation + ", lastRevTrx=" + lastRevTrx
				+ ", lastConfTrx=" + lastConfTrx + ", traceCounter=" + traceCounter + ", currentBatchs=" + currentBatchs
				+ ", assignedDevice=" + assignedDevice + ", startDateOperations=" + startDateOperations + "]";
	}
	
	
}
