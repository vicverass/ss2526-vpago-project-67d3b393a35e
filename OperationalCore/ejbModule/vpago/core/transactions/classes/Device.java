package vpago.core.transactions.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="device")
public class Device {
	
	public Device() { 
		super(); 
	}
	
	public Device(String displayName, String description, String model, String series, String vendor, DeviceType type) {
		super();
		this.displayName = displayName;
		this.description = description;
		this.model = model;
		this.series = series;
		this.vendor = vendor;	
		this.type = type;
	}
	
	public void update(Device other) {
		this.displayName = this.displayName != other.displayName ? other.displayName : this.displayName;
		this.description = this.description != other.description ? other.description : this.description;
		this.model = this.model != other.model ? other.model : this.model;
		this.series = this.series != other.series ? other.series : this.series;
		this.vendor = this.vendor != other.vendor ? other.vendor : this.vendor;
		this.setType((!this.type.equals(other.getType()) && other.getType() != null) ?  other.type : this.type);
	}
	
	/** Id en la base de datos */
	private int id;
	/** Nombre para mostrar */
	private String displayName;
	/** Descripci�n del dispositivo */
	private String description;
	/** Modelo del dispositivo */
	private String model;
	/** Serie del dispositivo */
	private String series;
	/** Fabricante del dispositivo */
	private String vendor;
	/** Tipo de Dispositivo */
	private DeviceType type;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="device_id_seq")
	@SequenceGenerator(name="device_id_seq", sequenceName = "device_id_seq")
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the displayName
	 */
	@Column(name="display_name")
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}
	
	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}
	
	/**
	 * @param series the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}
	
	/**
	 * @return the vendor
	 */
	public String getVendor() {
		return vendor;
	}
	
	/**
	 * @param vendor the vendor to set
	 */
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	/**
	 * @return the type
	 */
	@XmlTransient
	@ManyToOne( fetch=FetchType.LAZY )
	@JoinColumn(name = "device_type_id")
	public DeviceType getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(DeviceType type) {
		this.type = type;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + id;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((series == null) ? 0 : series.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (id != other.id)
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (series == null) {
			if (other.series != null)
				return false;
		} else if (!series.equals(other.series))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (vendor == null) {
			if (other.vendor != null)
				return false;
		} else if (!vendor.equals(other.vendor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Device [id=" + id + ", displayName=" + displayName + ", description=" + description + ", model=" + model
				+ ", series=" + series + ", vendor=" + vendor + ", type=" + type + "]";
	}

	

}
