package vpago.core.transactions.classes;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.solab.iso8583.IsoType;

import vpago.core.transactions.classes.types.FieldLoadType;

/**
 * Clase persistente que representa una Plantilla de Subcampo utilizada para almacenar la configuracion de los subcampos
 * privados para los mensajes
 */
@Entity
@DiscriminatorValue("Subfield")
public class TemplateSubField extends TemplateField {

	public TemplateSubField() {
		super();
	}
	
	public TemplateSubField(TemplateField parentField, IsoType fieldType, int fieldLen, FieldLoadType loadType, String encoder,
			String decoder, String loadFunc, String storeFunc, boolean storable, boolean returnable, boolean mandatory,
			String fixedValue, String accountAttr, String className, IsoTemplate isoTemplate, int subfieldId) {
		super(parentField.fieldId, fieldType, fieldLen, loadType, encoder, decoder, loadFunc, storeFunc, storable, returnable,
				mandatory, fixedValue, accountAttr, className, isoTemplate);
		this.subfieldId = subfieldId;
		this.parentField = parentField;
	}
	
	/** Plantilla de campo ISO a la que pertence el subcampo */
	protected TemplateField parentField;
	/** Identificador e indice del subcampo dentro del campo al que pertenece */
	protected int subfieldId;

	/**
	 * @return the parentField
	 */
	@ManyToOne
	@JoinColumn(name="template_field_id")
	public TemplateField getParentField() {
		return parentField;
	}

	/**
	 * @param parentField the parentField to set
	 */
	public void setParentField(TemplateField parentField) {
		this.parentField = parentField;
	}

	/**
	 * @return the subfieldId
	 */
	@Column(name="subfield_id")
	public int getSubfieldId() {
		return subfieldId;
	}

	/**
	 * @param subfieldId the subfieldId to set
	 */
	public void setSubfieldId(int subfieldId) {
		this.subfieldId = subfieldId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TemplateSubField [subfieldId=" + subfieldId + ", id=" + id + ", fieldType=" + fieldType + ", fieldLen="
				+ fieldLen + ", loadType=" + loadType + ", isoTemplate=" + isoTemplate + "]";
	}
	
}
