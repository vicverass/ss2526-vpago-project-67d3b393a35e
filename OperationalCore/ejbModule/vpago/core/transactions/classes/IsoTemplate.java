package vpago.core.transactions.classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.solab.iso8583.IsoType;
import com.solab.iso8583.parse.FieldParseInfo;

import vpago.core.transactions.classes.types.FieldLoadType;

/**
 * Clase persistente para representar una Plantilla ISO que almacena los parametros para mapear campos de una transaccion a campos iso segun el esquema elegido
 */
@Entity
@Table(name="iso_template")
public class IsoTemplate {
	
	public IsoTemplate(){}
		
	public IsoTemplate(int txnType, String header, String subpackageName, String preProcFunction, 
			String className, String valRevFunction, String termUnblockConf, boolean datetimeField, 
			Integer headerLength, Set<BatchType> batchTypes) {
		super();
		this.txnType = txnType;
		this.header = header;
		this.subpackageName = subpackageName;
		this.preProcFunction = preProcFunction;
		this.className = className;
		this.valRevFunction = valRevFunction;
		this.termUnblockConf = termUnblockConf;
		this.datetimeField = datetimeField;
		this.headerLength = headerLength;
		this.batchTypes = batchTypes;
	}
	
	public IsoTemplate(int txnType, String header, String subpackageName, String preProcFunction, 
			String className, String valRevFunction, String termUnblockConf, boolean datetimeField, 
			Integer headerLength) {
		super();
		this.txnType = txnType;
		this.header = header;
		this.subpackageName = subpackageName;
		this.preProcFunction = preProcFunction;
		this.className = className;
		this.valRevFunction = valRevFunction;
		this.termUnblockConf = termUnblockConf;
		this.datetimeField = datetimeField;
		this.headerLength = headerLength;
	}

	/** Identificador generado por el motor base de datos */
	protected int id;
	/** Tipo de operacion iso a la cual esta asociada el template */
	protected int txnType;
	/** Encabezado del formato ISO */
	protected String header;
	/** El key corresponde al valor del key en los parametros de la transaccion enviados mediante el servicio web, el 
	 * value al campo iso del protocolo 8583 */
	protected Map<String, TemplateField> template_field;
	/** El key corresponde al valor del key en los parametros de la transaccion enviados mediante el servicio web, el 
	 * value corresponde al campo privado en la bd */
	protected Map<String, TemplateSubField> template_subfield;
	/** Nombre del paquete donde se encuentran las clases para el manejo de los campos */
	protected String subpackageName;
	/** Nombre de la funcion de pre procesamiento antes del parsing*/
	protected String preProcFunction;
	/** Nombre de la clase dinamica que almacena funciones generales de la plantilla */
	protected String className;
	/** Nombre de la funcion que validara si se genera la invocacion de la inversa de una transaccion **/
	protected String valRevFunction;
	/** Nombre de la funcion que validara si el terminal bloqueado por confirmacion puede desbloquearse **/
	protected String termUnblockConf;
	/** Indica si el campo de fecha (DE7 ISO8583) estara presente **/
	protected boolean datetimeField;
	/** Indica la longitud del header de la trama ISO 8583 **/
	protected Integer headerLength;
	/** Tipos de lotes asociados con la plantilla **/
	protected Set<BatchType> batchTypes;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="iso_template_id_seq")
	@SequenceGenerator(name="iso_template_id_seq", sequenceName = "iso_template_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the txnType
	 */
	@Column(name="txn_type")
	public int getTxnType() {
		return txnType;
	}

	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(int txnType) {
		this.txnType = txnType;
	}
	
	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * @return the template_field
	 */
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY) @JoinTable(name="iso_template_field")
	@MapKeyColumn(name="parameter_name")
	public Map<String, TemplateField> getTemplate_field() {
		return template_field;
	}

	/**
	 * @param template_field the template_field to set
	 */
	public void setTemplate_field(Map<String, TemplateField> template_field) {
		this.template_field = template_field;
	}

	/**
	 * @return the template_subfield
	 */
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY) @JoinTable(name="iso_template_field")
	@MapKeyColumn(name="parameter_name")
	@OrderBy("subfield_id ASC")
	public Map<String, TemplateSubField> getTemplate_subfield() {
		return template_subfield;
	}

	/**
	 * @param template_subfield the template_subfield to set
	 */
	public void setTemplate_subfield(Map<String, TemplateSubField> template_subfield) {
		this.template_subfield = template_subfield;
	}
		
	/**
	 * @return the subpackageName
	 */
	@Column(name="subpackage_name")
	public String getSubpackageName() {
		return subpackageName;
	}

	/**
	 * @param subpackageName the subpackageName to set
	 */
	public void setSubpackageName(String subpackageName) {
		this.subpackageName = subpackageName;
	}
	
	/**
	 * @return the preProcFunction
	 */
	@Column(name="pre_proc_function")
	public String getPreProcFunction() {
		return preProcFunction;
	}

	/**
	 * @param preProcFunction the preProcFunction to set
	 */
	public void setPreProcFunction(String preProcFunction) {
		this.preProcFunction = preProcFunction;
	}
	
	/**
	 * @return the className
	 */
	@Column(name="class_name")
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	
	/**
	 * @return the valRevFunction
	 */
	@Column(name="validate_reversal_func")
	public String getValRevFunction() {
		return valRevFunction;
	}
	
	/**
	 * @param valRevFunction the valRevFunction to set
	 */
	public void setValRevFunction(String valRevFunction) {
		this.valRevFunction = valRevFunction;
	}
	
	/**
	 * @return the termUnblockConf
	 */
	@Column(name="term_conf_unblock_funct")
	public String getTermUnblockConf() {
		return termUnblockConf;
	}

	/**
	 * @param termUnblockConf the termUnblockConf to set
	 */
	public void setTermUnblockConf(String termUnblockConf) {
		this.termUnblockConf = termUnblockConf;
	}
	
	/**
	 * @return the datetimeField
	 */
	@Column(name="datetime_field_present")
	public boolean isDatetimeField() {
		return datetimeField;
	}

	/**
	 * @param datetimeField the datetimeField to set
	 */
	public void setDatetimeField(boolean datetimeField) {
		this.datetimeField = datetimeField;
	}

	/**
	 * @return the headerLength
	 */
	@Column(name="header_length")
	public Integer getHeaderLength() {
		return headerLength;
	}

	/**
	 * @param headerLength the headerLength to set
	 */
	public void setHeaderLength(Integer headerLength) {
		this.headerLength = headerLength;
	}

	/**
	 * Inserta una configuracion de campo iso asociada a un par clave-valor
	 * 
	 * @param key Clave que identifica al campo ISO
	 * @param fieldId Identificador del campo segun el formato ISO
	 * @param fieldType Tipo de subcampo ISO
	 * @param fieldLen Longitud del subcampo ISO
	 * @param loadType Tipo de Carga del parametro
	 * @param encoder Nombre de la funcion codificadora del subcampo
	 * @param decoder Nombre de la funcion decodificadora del campo
	 * @param loadFunc Nombre de la funcion a aplicar durante la carga del parametro
	 * @param storeFunc Nombre de la funcion a aplicar durante el almacenamiento del parametro
	 * @param storable Indica si el parametro debe almacenarse en la base de datos
	 * @param returnable Indica si el parametro debe retornarse en la respuesta de la solicitud
	 * @param mandatory Indica si es obligatoria la presencia del parametro en el mensaje
	 * @param fixedValue Almacena el valor del parametro en caso de que sea tipo FIXED
	 * @param acquirerAttr Nombre del atributo de la clase Acquirer al que esta asociado en caso de ser de tipo RULE
	 * @param className Nombre de la clase que contiene los metodos para manejar el campo
	 */
	public void insertIsoField(String key, int fieldId, IsoType fieldType, int fieldLen, FieldLoadType loadType, String encoder, 
			String decoder, String loadFunc, String storeFunc, boolean storable, boolean returnable, boolean mandatory, 
			String fixedValue, String acquirerAttr, String className){
		this.template_field.put(key, new TemplateField(fieldId, fieldType, fieldLen, loadType, encoder, 
				decoder, loadFunc, storeFunc, storable, returnable, mandatory, fixedValue, acquirerAttr, className, this));
	}
	
	/**
	 * Inserta una configuracion de subcampo iso asociada a un par clave-valor
	 * 
	 * @param key Clave que identifica al campo ISO
	 * @param parentField Plantilla de campo ISO a la que pertence el subcampo
	 * @param fieldType Tipo de subcampo ISO
	 * @param fieldLen Longitud del subcampo ISO
	 * @param loadType Tipo de Carga del parametro
	 * @param encoder Nombre de la funcion codificadora del subcampo
	 * @param decoder Nombre de la funcion decodificadora del campo
	 * @param loadFunc Nombre de la funcion a aplicar durante la carga del parametro
	 * @param storeFunc Nombre de la funcion a aplicar durante el almacenamiento del parametro
	 * @param storable Indica si el parametro debe almacenarse en la base de datos
	 * @param returnable Indica si el parametro debe retornarse en la respuesta de la solicitud
	 * @param mandatory Indica si es obligatoria la presencia del parametro en el mensaje
	 * @param fixedValue Almacena el valor del parametro en caso de que sea tipo FIXED
	 * @param acquirerAttr Nombre del atributo de la clase Acquirer al que esta asociado en caso de ser de tipo RULE
	 * @param className Nombre de la clase que contiene los metodos para manejar el campo
	 * @param subfId Identificador e indice del subcampo dentro del campo al que pertenece
	 */
	public void insertIsoSubField(String key, TemplateField parentField, IsoType fieldType, int fieldLen, FieldLoadType loadType, String encoder, 
			String decoder, String loadFunc, String storeFunc, boolean storable, boolean returnable, boolean mandatory, 
			String fixedValue, String acquirerAttr, String className, int subfId){
		this.template_subfield.put(key, new TemplateSubField(parentField, fieldType, fieldLen, loadType, encoder, 
				decoder, loadFunc, storeFunc, storable, returnable, mandatory, 
				fixedValue, acquirerAttr, className, this, subfId));
	}
	
	/**
	 * Retorna la Plantilla de Campo ISO asociada a la clave indicada
	 * 
	 * @param key Clave asociada al campo
	 * 
	 * @return Plantilla de Campo ISO
	 */
	public TemplateField getTemplateField(String key){
		return this.template_field.get(key);
	}
	
	/**
	 * Retorna la Plantilla de Subcampo ISO asociada a la clave indicada
	 * 
	 * @param key Clave asociada al subcampo
	 * 
	 * @return Plantilla de Subcampo ISO
	 */
	public TemplateSubField getTemplateSubField(String key){
		return this.template_subfield.get(key);
	}
	
	/**
	 * Construye un Map con la informacion de las Plantillas de Campos asociados a esta  Plantilla ISO en el formato FieldParseInfo de la libreria J8583
	 * 
	 * @return Map que contiene los FieldParseInfo indexados por el identificador de campo ISO
	 */
	@Transient
	public Map<Integer, FieldParseInfo> getFieldParseInfoMap() {
		String encoding = System.getProperty("file.encoding");
		Map<Integer, FieldParseInfo> map = new HashMap<Integer, FieldParseInfo>();
		this.template_field.forEach((paramName, templateField) -> {
			map.put(templateField.fieldId, FieldParseInfo.getInstance(templateField.fieldType, templateField.fieldLen, encoding));
		});
		return map;
	}
	
	/**
	 * Construye un Map con la informacion de las Plantillas de Campos asociados a esta  Plantilla ISO mapeadas por el numero de campo
	 * 
	 * @return Map que contiene los TemplateField indexados por el identificador de campo ISO
	 */
	@Transient
	public Map<Integer, TemplateField> getIsoIdMap() {
		Map<Integer, TemplateField> map = new HashMap<Integer, TemplateField>();
		this.template_field.forEach((paramName, templateField) -> {
			map.put(templateField.fieldId, templateField);
		});
		return map;
	}
	
	/**
	 * @return the batchTypes
	 */
	@OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name="iso_template_id")
	public Set<BatchType> getBatchTypes() {
		return batchTypes;
	}

	/**
	 * @param batchTypes the batchTypes to set
	 */
	public void setBatchTypes(Set<BatchType> batchTypes) {
		this.batchTypes = batchTypes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IsoTemplate [id=" + id + ", txnType=" + txnType + ", header=" + header + ", subpackageName="
				+ subpackageName + ", preProcFunction=" + preProcFunction + ", className=" + className + "]";
	}
	
}
