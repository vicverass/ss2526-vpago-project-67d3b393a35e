package vpago.core.transactions.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Clase persistente para representar un Tipo de Funcion no ISO8583
 */
@Entity
@Table(name="function_type")
public class FunctionType {
	
	public FunctionType() {
		super();
	}
	
	public FunctionType(String name, String description, String methodName) {
		super();
		this.name = name;
		this.description = description;
		this.methodName = methodName;
	}

	/** Identificador generado por el motor base de datos */
	private int id;
	/** Nombre del metodo */
	private String name;
	/** Descripcion opcional del metodo */
	private String description;
	/** Nombre de la primitiva a invocar en el codigo */
	private String methodName;
	
	/**
	 * @return the id
	 */
	@Id @GeneratedValue(generator="function_type_id_seq")
	@SequenceGenerator(name="function_type_id_seq", sequenceName = "function_type_id_seq")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the methodName
	 */
	@Column(name="method_name")
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FunctionType [id=" + id + ", name=" + name + ", description=" + description + ", methodName="
				+ methodName + "]";
	}

}
