package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se envia la transaccion con un numero de terminal no existente
 */
public class NonExistentTerminal extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8903638534206928012L;

	public NonExistentTerminal() {
		super();
	}

	public NonExistentTerminal(String message) {
		super(message);
	}

	public NonExistentTerminal(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public NonExistentTerminal(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	
}
