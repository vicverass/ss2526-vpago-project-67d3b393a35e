package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se detecta un inconveniente con la validez de la transaccion
 */
public class InvalidTransaction extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4535295010386361252L;

	public InvalidTransaction() {
		super();
	}

	public InvalidTransaction(String message) {
		super(message);
	}

	public InvalidTransaction(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public InvalidTransaction(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	

}
