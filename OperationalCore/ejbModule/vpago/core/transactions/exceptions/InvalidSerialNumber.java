package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se detecta un inconveniente con la validez de la transaccion
 */
public class InvalidSerialNumber extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1318277349955604212L;

	public InvalidSerialNumber() {
		super();
	}

	public InvalidSerialNumber(String message) {
		super(message);
	}

	public InvalidSerialNumber(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public InvalidSerialNumber(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
}
