package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando no se puede acceder a algun componente de la plataforma
 */
public class ServiceUnavailable extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1332141160840198782L;

	public ServiceUnavailable() {
		super();
	}

	public ServiceUnavailable(String message) {
		super(message);
	}

	public ServiceUnavailable(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public ServiceUnavailable(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
