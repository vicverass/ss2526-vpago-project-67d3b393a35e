package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando no se encuentra una configuracion de campo para un argumento 
 */
public class ArgumentsMismatchTemplateException extends TransactionException {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1655581255504611597L;

	public ArgumentsMismatchTemplateException() {
		super();
	}

    public ArgumentsMismatchTemplateException(String message) {
    	super(message);
    }

	public ArgumentsMismatchTemplateException(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public ArgumentsMismatchTemplateException(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

    
}
