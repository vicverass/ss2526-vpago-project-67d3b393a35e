package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se envia la transaccion pero no se recibe respuesta
 */
public class NoResponse extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4144565988943717767L;

	public NoResponse() {
		super();
	}

	public NoResponse(String message) {
		super(message);
	}

	public NoResponse(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public NoResponse(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	
}
