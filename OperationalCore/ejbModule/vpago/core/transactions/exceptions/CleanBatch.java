package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se intenta enviar un cierre que no tiene transacciones
 */
public class CleanBatch extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CleanBatch() {
		super();
	}

	public CleanBatch(String message) {
		super(message);
	}

	public CleanBatch(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public CleanBatch(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
