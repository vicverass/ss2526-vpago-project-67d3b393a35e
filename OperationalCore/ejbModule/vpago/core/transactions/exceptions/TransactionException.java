package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada por el modulo de transacciones 
 */
public class TransactionException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1554200176744030809L;
	
	protected CoreResponse coreResponse;

	public TransactionException() {
		super();
	}

    public TransactionException(String message) {
        super(message);
    }

	public TransactionException(CoreResponse coreResponse) {
		super();
		this.coreResponse = coreResponse;
	}
    
	public TransactionException(String message, CoreResponse coreResponse) {
	    super(message);
	    this.coreResponse = coreResponse;
	}

	/**
	 * @return the coreResponse
	 */
	public CoreResponse getCoreResponse() {
		return coreResponse;
	}

	/**
	 * @param coreResponse the coreResponse to set
	 */
	public void setCoreResponse(CoreResponse coreResponse) {
		this.coreResponse = coreResponse;
	}
	
	
}
