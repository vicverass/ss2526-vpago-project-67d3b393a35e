package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada al tratar de realizar una transaccion duplicada
 */
public class DuplicateTransaction extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3493039812369177508L;

	public DuplicateTransaction() {
		super();
	}

	public DuplicateTransaction(String message) {
		super(message);
	}

	public DuplicateTransaction(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public DuplicateTransaction(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
