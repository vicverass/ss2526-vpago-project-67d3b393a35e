package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando se intenta invertir una transaccion cuya inversa fue aprobada
 */
public class AlreadyInvertedTransaction extends TransactionException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3990581196639528971L;

	public AlreadyInvertedTransaction() {
		super();
	}

	public AlreadyInvertedTransaction(String message) {
		super(message);
	}

	public AlreadyInvertedTransaction(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public AlreadyInvertedTransaction(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	
}
