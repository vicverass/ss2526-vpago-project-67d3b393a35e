package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

public class InvalidBatch extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidBatch() {
		super();
	}

	public InvalidBatch(String message) {
		super(message);
	}

	public InvalidBatch(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public InvalidBatch(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
