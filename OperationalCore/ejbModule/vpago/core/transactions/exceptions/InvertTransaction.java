package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando la funcion validadora indica que se debe invertir o reversar la transaccion
 */
public class InvertTransaction extends TransactionException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3990581196639528971L;

	public InvertTransaction() {
		super();
	}

	public InvertTransaction(String message) {
		super(message);
	}

	public InvertTransaction(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public InvertTransaction(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	
}
