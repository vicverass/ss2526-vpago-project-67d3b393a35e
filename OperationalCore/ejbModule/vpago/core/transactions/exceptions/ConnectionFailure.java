package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando ocurre una falla en la conexion con el aprobador
 */
public class ConnectionFailure extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3990581196639528971L;

	public ConnectionFailure() {
		super();
	}

	public ConnectionFailure(String message) {
		super(message);
	}

	public ConnectionFailure(CoreResponse coreResponse) {
		super(coreResponse);
		// TODO Auto-generated constructor stub
	}

	public ConnectionFailure(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
		// TODO Auto-generated constructor stub
	}

	
}
