package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando un terminal genera una transaccion y no esta habilitado
 */
public class DisabledTerminal extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -847191878340518002L;

	public DisabledTerminal() {
		super();
	}

	public DisabledTerminal(String message) {
		super(message);
	}

	public DisabledTerminal(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public DisabledTerminal(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
