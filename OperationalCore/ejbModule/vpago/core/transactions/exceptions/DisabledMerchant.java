package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando el terminal de un comercio no habilitado genera una transaccion
 */
public class DisabledMerchant extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2726414856802152347L;

	public DisabledMerchant() {
		super();
	}

	public DisabledMerchant(String message) {
		super(message);
	}

	public DisabledMerchant(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public DisabledMerchant(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
