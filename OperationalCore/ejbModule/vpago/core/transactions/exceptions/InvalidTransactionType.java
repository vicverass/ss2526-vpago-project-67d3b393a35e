package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando el tipo de transaccion solicitada es invalida
 */
public class InvalidTransactionType extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4839328739016926677L;

	public InvalidTransactionType() {
		super();
	}

	public InvalidTransactionType(String message) {
		super(message);
	}

	public InvalidTransactionType(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public InvalidTransactionType(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	
}
