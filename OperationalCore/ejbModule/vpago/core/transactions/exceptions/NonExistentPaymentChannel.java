package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando el canal de pago solicitado no existe
 */
public class NonExistentPaymentChannel extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6929603869199859262L;

	public NonExistentPaymentChannel() {
		super();
	}

	public NonExistentPaymentChannel(String message) {
		super(message);
	}

	public NonExistentPaymentChannel(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public NonExistentPaymentChannel(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	

}
