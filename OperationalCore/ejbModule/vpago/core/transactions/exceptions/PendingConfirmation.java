package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando el terminal esta bloqueado por confirmacion pendiente
 */
public class PendingConfirmation extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1332141160840198782L;

	public PendingConfirmation() {
		super();
	}

	public PendingConfirmation(String message) {
		super(message);
	}

	public PendingConfirmation(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public PendingConfirmation(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}

	
}
