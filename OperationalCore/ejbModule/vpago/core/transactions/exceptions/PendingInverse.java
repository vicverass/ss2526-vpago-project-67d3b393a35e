package vpago.core.transactions.exceptions;

import vpago.core.transactions.classes.CoreResponse;

/**
 * Excepcion generada cuando el terminal esta bloqueado por confirmacion pendiente
 */
public class PendingInverse extends TransactionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1332141160840198782L;

	public PendingInverse() {
		super();
	}

	public PendingInverse(String message) {
		super(message);
	}

	public PendingInverse(CoreResponse coreResponse) {
		super(coreResponse);
	}

	public PendingInverse(String message, CoreResponse coreResponse) {
		super(message, coreResponse);
	}
	
	

}
