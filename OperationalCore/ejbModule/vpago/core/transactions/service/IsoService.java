package vpago.core.transactions.service;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Terminal;

import static java.lang.Math.toIntExact;

/**
 * Servicios para proveer parametros ISO
 */
public class IsoService {
	private static final Logger logger = LoggerFactory.getLogger(IsoService.class);
	
	/**
	 * Calcula un numero de referencia para una transaccion
	 * 
	 * @return Numero de referencia
	 */
	public static String getReference() {
		Session session = null;
		String referenceNum = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Integer transactionCount = toIntExact((Long)session.createQuery("select count(*) from Transaction as transaction").uniqueResult());
			//Integer transactionCount = 0;
			logger.debug("Transacciones registradas: {}", transactionCount);
			if(transactionCount > 0) {
				Integer lastId = (Integer) session.createQuery("select max(transaction.id) from Transaction as transaction").uniqueResult();
				lastId++;
				referenceNum = String.format("%06d", lastId);
				logger.debug("Identificador de la ultima transaccion insertada: {}", referenceNum);
			} else {
				referenceNum = String.format("%06d", 0);
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			logger.debug("Referencia: " + referenceNum);
			if(session != null) session.close();
		}
		return referenceNum != null ? (referenceNum.length() > 6 ? (referenceNum.substring(referenceNum.length()-6, referenceNum.length())) : referenceNum) : "000000";
	}
	
	public static Integer getCurrentBatchNumber(Terminal terminal, String paramVal) { 
		for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
			if(currentBatch.getBatchType().getParamValue().equals(paramVal)) return currentBatch.getBatchNumber();
		}
		return -1;
	}
}
