/**
 * Clases con funciones que proveen servicios a los metodos en 
 * el paquete de extensiones de los canales de pago 
 */
package vpago.core.transactions.service;