package vpago.core.transactions.util;

//import android.nfc.FormatException;
//import android.util.Log;
//
//import com.bbpos.www.cayan.log.CayanLog;
//import com.bbpos.www.cayan.log.CayanLogEngine;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import vpago.core.transactions.classes.Terminal;


public class Utils {

	private static Hashtable<String, String> paymentTypeTable;
	private static Hashtable<String, String[]> TACTable;

	//static constructor
	static
	{
		//Refer to https://en.wikipedia.org/wiki/EMV
		//populate payment table
		paymentTypeTable = new Hashtable<>();
		paymentTypeTable.put("A000000003", "VISA");
		paymentTypeTable.put("A000000004", "MASTERCARD");
		paymentTypeTable.put("A000000005", "MASTERCARD");
		paymentTypeTable.put("A000000025", "AMEX");
		paymentTypeTable.put("A000000029", "LINK");
		paymentTypeTable.put("A000000042", "CB");
		paymentTypeTable.put("A000000065", "JCB");
		paymentTypeTable.put("A000000121", "DANKORT");
		paymentTypeTable.put("A000000141", "COGEBAN");
		paymentTypeTable.put("A000000152", "DINERS CLUB");
		paymentTypeTable.put("A000000154", "BANRISUL");
		paymentTypeTable.put("A000000228", "SPAN2");
		paymentTypeTable.put("A000000277", "INTERAC");
		paymentTypeTable.put("A000000324", "DISCOVER");
		paymentTypeTable.put("A000000333", "UNIONPAY");
		paymentTypeTable.put("A000000359", "ZKA");
		paymentTypeTable.put("A000000371", "VERVE");
		paymentTypeTable.put("A000000439", "THE EXCHANGE NETWORK");
		paymentTypeTable.put("A000000524", "RUPAY");

		TACTable = new Hashtable<>();
		TACTable.put("A0000000031010", new String[]{"584000A800", "0010000000", "584004F800"}); //Visa
		TACTable.put("A0000000041010", new String[]{"FC5080A000", "0000000000", "FC5080F800"}); //Master
		TACTable.put("A0000000032010", new String[]{"584000A800", "0010000000", "584004F800"}); //Visa Electron
		TACTable.put("A0000000043060", new String[]{"FC5080A000", "0000000000", "FC5080F800"}); //Master Maestro
		TACTable.put("A00000002501", new String[]{"CC00000000", "0000000000", "CC00000000"}); //AMEX

	}
	
	public static String toHexString(byte[] b) {
		String result = "";
		for (int i=0; i < b.length; i++) {
			result += Integer.toString( ( b[i] & 0xFF ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
	
	public static byte[] hexToByteArray(String s) {
		if(s == null) {
			s = "";
		}
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		for(int i = 0; i < s.length() - 1; i += 2) {
			String data = s.substring(i, i + 2);
			bout.write(Integer.parseInt(data, 16));
		}
		return bout.toByteArray();
	}

	public static String removePKCS7(String tlv) {
		byte[] tlvBytes = Utils.hexToByteArray(tlv);
		int numberOfPadding = tlvBytes[tlvBytes.length - 1];

		//Make sure the padding bytes consistent
		for (int i=tlvBytes.length-numberOfPadding; i<tlvBytes.length; i++)
		{
			if (tlvBytes[i] != numberOfPadding)
			{
				//CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.UTILS).setMsg("Remove PKCS7 failed. Return original string"));
				return tlv;
			}
		}

		return tlv.substring(0, tlv.length() - numberOfPadding * 2);
	}

	public static String removeNullPadding(String tlv) {
		byte[] tlvBytes = Utils.hexToByteArray(tlv);

		int numberOfPadding = 0;

		for (int i=tlvBytes.length-1; i>=0; i--)
		{
			if (tlvBytes[i] == 0)
				numberOfPadding++;
			else
				break;
		}

		//Log.d("TimChan", "Remove padding len=" + numberOfPadding);

		return tlv.substring(0, tlv.length() - numberOfPadding * 2);
	}

	public static String addTlvTag(String tlvString, String appendTag) /* throws FormatException */
	{
		byte[] bytes = hexToByteArray(tlvString);

		if (bytes[0] != 0x70)
		{
			//CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.UTILS).setMsg("Expected 0x70 got " + bytes[0]));

			//throw new FormatException();
		}


		//Retrieve length
		int len = (bytes[2]<< 8) + bytes[3];

		//update len
		len += appendTag.length() >> 1;

		bytes[2] = (byte)(len >> 8);
		bytes[3] = (byte)(len & 0xff);

		String rtn = toHexString(bytes);
		rtn = rtn.concat(appendTag);

		return rtn;

	}

	public static String removeTlvTag(String tlvString, String removeTag) /* throws FormatException */
	{
		byte[] bytes = hexToByteArray(tlvString);

		if (bytes[0] != 0x70)
		{
			//CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.UTILS).setMsg("Expected 0x70 got " + bytes[0]));
			//throw new FormatException();
		}


		//Retrieve length
		int len = (bytes[2]<< 8) + bytes[3];

		//update len
		len -= removeTag.length() >> 1;

		bytes[2] = (byte)(len >> 8);
		bytes[3] = (byte)(len & 0xff);

		String rtn = toHexString(bytes);
		rtn = rtn.replace(removeTag, "");

		return rtn;

	}

	public static String parseTag(String xml, String tag)
	{
		//Use regex instead of dom for simplicity
		try {
			String searchPattern = "<" + tag + ">(.*?)</" + tag + ">";

			Pattern regex = Pattern.compile(searchPattern, Pattern.DOTALL);
			Matcher matcher = regex.matcher(xml);

			if (matcher.find()) {
				String val = matcher.group(1);
				return val;
			}
		}
		catch (Exception e)
		{
			//CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.UTILS).setMsg("Unable to parse xml"));
		}

		return null;

	}

	public static String genSha1(String text)
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes("iso-8859-1"),0,text.length());
			byte[] sha1hash = md.digest();
			return toHexString(sha1hash);
		}
		catch (Exception e)
		{
			return null;
		}
	}


	public static boolean luhnCheck(String pnr){
		// this only works if you are certain all input will be at least 10 characters
		int checkDigit = pnr.charAt(pnr.length()-1) - '0';
		int sum=0;

		for (int i=0; i<pnr.length()-1; i++)
		{
			int digit = pnr.charAt(i) - '0';
			if ((i%2) == 0)
			{
				digit *= 2;
				if (digit > 9)
					digit -= 9;
			}
			sum += digit;
		}

		sum *= 9;
		sum = sum % 10;

		if (sum == checkDigit)
			return true;
		else
			return false;

	}



	public static String lookupPaymentType(String aid)
	{

		String rtn = "UNKNOWN";

		try
		{
			String rid = aid.substring(0, 10);

			//Log.d("TimChan", "lookup payment type:" + rid);
			rtn = paymentTypeTable.get(rid.toUpperCase());

			if (rtn == null)
				rtn = "UNKNOWN";

		}
		catch (Exception e)
		{

		}
		//Log.d("TimChan", "lookup payment type, return " + rtn);
		return rtn;
	}

	public static String[] lookupTAC(String aid)
	{

		String[] rtn = null;

		try
		{
			rtn = TACTable.get(aid.toUpperCase());
		}
		catch (Exception e)
		{

		}

		return rtn;
	}

	/*
		Convert the amount return from TLV to amount in string
		e.g. 0000320 means 3.20 if decimal place is 2
	 */
	public static String convertTLVAmountToString(String tlvAmount, int decimalPlace)
	{
		String rtn = null;
		if (tlvAmount != null)
		{
			try
			{
				//Add a period
				String x = tlvAmount.substring(0, tlvAmount.length()-decimalPlace) + "." + tlvAmount.substring(tlvAmount.length()-decimalPlace, tlvAmount.length());

				BigDecimal tmp = new BigDecimal(x);


				DecimalFormat df = new DecimalFormat();
				df.setMaximumFractionDigits(2);
				df.setMinimumFractionDigits(2);
				rtn = df.format(tmp);

			}
			catch (Exception e)
			{

			}
		}

		return rtn;
	}

	public static String convertTransactionDate(String date)
	{
		String rtn = null;
		try
		{
			SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			//First of all string after second
			date = date.substring(0, date.indexOf("."));

			//Do the conversion
			rtn = myFormat.format(fromUser.parse(date));

		}
		catch (Exception e)
		{
			//keep original format
			rtn = date;
		}
		return rtn;
	}

	//Put a uniqueTag in any decimal tag in XML
	public static String putUniqueTagInDecimalXml(String input, String uniqueTag)
	{
		String rtn = null;
		try {
			String searchPattern = ">" + "[0-9]*\\.?[0-9]+" +"<" ;
			rtn = input.replaceAll(searchPattern, uniqueTag+ "$0");
			rtn = rtn.replaceAll(uniqueTag+ ">", ">" + uniqueTag);

		}
		catch (Exception e)
		{
			//CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.UTILS).setMsg("Unable to add uniqueTag"));
			rtn = input; //conversion failed, fall back to org input
		}

		return rtn;

	}
	
	public static String getSaltString(int len) {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < len) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
	
	public static String calcSecToken(Terminal terminal, String timestamp, String controlToken) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update((terminal.getAffiliation().getMerchant().getMerchantGuid() + "*").getBytes());
			md.update((terminal.getTerminalGuid() + "*").getBytes());
			md.update((controlToken + "*").getBytes());
			if(timestamp != null) md.update(timestamp.getBytes());
			String secToken = DatatypeConverter.printHexBinary(md.digest());
			return secToken;
		} catch (NoSuchAlgorithmException e) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
}
