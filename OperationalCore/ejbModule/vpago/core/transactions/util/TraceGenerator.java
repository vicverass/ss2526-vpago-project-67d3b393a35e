package vpago.core.transactions.util;

import java.util.ArrayList;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.TraceNumberGenerator;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;

/**
 * Generador de trace
 */
public class TraceGenerator implements TraceNumberGenerator {
	private static final Logger logger = LoggerFactory.getLogger(TraceGenerator.class);
	
	public TraceGenerator() {
		super();
	}
	
	public TraceGenerator(Terminal terminal) {
		super();
		this.terminal = terminal;
	}

	/** Terminal **/
	private Terminal terminal;
	
	@Override
	public int getLastTrace() {
		return terminal.getTraceCounter();
	}

	@Override
	public int nextTrace() {
		Session session = null;
		String referenceNum = null;
		try {
			logger.debug("Terminal en nextTrace {}", terminal);
			session = HibernateUtil.getSessionFactory().openSession();
			if(terminal != null) {
				terminal.setTraceCounter(terminal.getTraceCounter() == 999999 ? 1 : terminal.getTraceCounter()+1);
			} else {
				//Configurado con el terminal 001 de Arca en produccion 77B6A1FC
				//Para pruebas locales DF056554
				terminal = (Terminal) session
						.createQuery("from Terminal as terminal where terminal.terminalGuid = :terminalGuid")
						.setString("terminalGuid", "77B6A1FC").uniqueResult();
				return terminal.getTraceCounter();
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			logger.debug("Referencia: " + referenceNum);
			if(session != null) session.close();
		}
		return terminal.getTraceCounter();
	}

}
