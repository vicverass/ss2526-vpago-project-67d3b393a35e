package vpago.core.transactions.connections;

import java.nio.ByteBuffer;

public class TransactionAttachment {

//	public TransactionAttachment(Integer trxId, String trxTrace, ByteBuffer msgRespB) {
//		super();
//		this.trxId = trxId;
//		this.trxTrace = trxTrace;
//		this.msgRespB = msgRespB;
//	}
	
	public TransactionAttachment(Integer trxId, String trxTrace, ByteBuffer msgRespB, AsyncReqQueueLocal aReqQueue) {
		super();
		this.trxId = trxId;
		this.trxTrace = trxTrace;
		this.msgRespB = msgRespB;
		this.aReqQueue = aReqQueue;
	}

	private Integer trxId;
	private String trxTrace;
	private ByteBuffer msgRespB;
	private AsyncReqQueueLocal aReqQueue;
	
	/**
	 * @return the trxId
	 */
	public Integer getTrxId() {
		return trxId;
	}

	/**
	 * @param trxId the trxId to set
	 */
	public void setTrxId(Integer trxId) {
		this.trxId = trxId;
	}

	/**
	 * @return the trxTrace
	 */
	public String getTrxTrace() {
		return trxTrace;
	}

	/**
	 * @param trxTrace the trxTrace to set
	 */
	public void setTrxTrace(String trxTrace) {
		this.trxTrace = trxTrace;
	}

	/**
	 * @return the msgRespB
	 */
	public ByteBuffer getMsgRespB() {
		return msgRespB;
	}

	/**
	 * @param msgRespB the msgRespB to set
	 */
	public void setMsgRespB(ByteBuffer msgRespB) {
		this.msgRespB = msgRespB;
	}

	/**
	 * @return the aReqQueue
	 */
	public AsyncReqQueueLocal getaReqQueue() {
		return aReqQueue;
	}

	/**
	 * @param aReqQueue the aReqQueue to set
	 */
	public void setaReqQueue(AsyncReqQueueLocal aReqQueue) {
		this.aReqQueue = aReqQueue;
	}
	
	
}
