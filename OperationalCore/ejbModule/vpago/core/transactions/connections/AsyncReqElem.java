package vpago.core.transactions.connections;

import java.nio.ByteBuffer;

import vpago.core.transactions.classes.TransactionData;

public class AsyncReqElem {
	
	public AsyncReqElem(
			Integer trxId, 
			String trxTrace, 
			ByteBuffer trxByteB, 
			boolean timedOut, 
			boolean autoRevSent,
			TransactionData trxData) {
		super();
		this.trxId = trxId;
		this.trxTrace = trxTrace;
		this.trxByteB = trxByteB;
		this.timedOut = timedOut;
		this.autoRevSent = autoRevSent;
		this.trxData = trxData;
	}

	public AsyncReqElem(String trxTrace, ByteBuffer trxByteB) {
		super();
		this.trxTrace = trxTrace;
		this.trxByteB = trxByteB;
	}

	private Integer trxId;
	private String trxTrace;
	private ByteBuffer trxByteB;
	private boolean timedOut;
	private boolean autoRevSent;
	private TransactionData trxData;
	
	/**
	 * @return the trxId
	 */
	public Integer getTrxId() {
		return trxId;
	}
	
	/**
	 * @param trxId the trxId to set
	 */
	public void setTrxId(Integer trxId) {
		this.trxId = trxId;
	}
	
	/**
	 * @return the trxTrace
	 */
	public String getTrxTrace() {
		return trxTrace;
	}
	
	/**
	 * @param trxTrace the trxTrace to set
	 */
	public void setTrxTrace(String trxTrace) {
		this.trxTrace = trxTrace;
	}
	
	/**
	 * @return the trxByteB
	 */
	public ByteBuffer getTrxByteB() {
		return trxByteB;
	}
	
	/**
	 * @param trxByteB the trxByteB to set
	 */
	public void setTrxByteB(ByteBuffer trxByteB) {
		this.trxByteB = trxByteB;
	}
	
	/**
	 * @return the timedOut
	 */
	public boolean isTimedOut() {
		return timedOut;
	}
	
	/**
	 * @param timedOut the timedOut to set
	 */
	public void setTimedOut(boolean timedOut) {
		this.timedOut = timedOut;
	}
	
	/**
	 * @return the autoRevSent
	 */
	public boolean isAutoRevSent() {
		return autoRevSent;
	}
	
	/**
	 * @param autoRevSent the autoRevSent to set
	 */
	public void setAutoRevSent(boolean autoRevSent) {
		this.autoRevSent = autoRevSent;
	}

	/**
	 * @return the trxData
	 */
	public TransactionData getTrxData() {
		return trxData;
	}

	/**
	 * @param trxData the trxData to set
	 */
	public void setTrxData(TransactionData trxData) {
		this.trxData = trxData;
	}
}
