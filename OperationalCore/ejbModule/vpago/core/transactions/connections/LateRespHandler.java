package vpago.core.transactions.connections;

import java.nio.channels.CompletionHandler;

public class LateRespHandler implements CompletionHandler<Long,  TransactionAttachment> {

	@Override
	public void completed(Long result, TransactionAttachment attachment) {
		System.out.println("LateRespHandler - completed");
	}

	@Override
	public void failed(Throwable exc, TransactionAttachment attachment) {
		System.out.println("LateRespHandler - failed");
	}

}
