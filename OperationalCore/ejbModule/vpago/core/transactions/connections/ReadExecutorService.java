package vpago.core.transactions.connections;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.TransactionFactory;
import vpago.core.transactions.TransactionHandlerLocal;
import vpago.core.transactions.classes.AssortedParameters;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionResponseParameter;
import vpago.core.transactions.classes.TransactionType;

/**
 * Session Bean implementation class ReadExecutorService
 */
@Startup
@Singleton
@LocalBean
@DependsOn({"NonClosingAsyncTCP", "AsyncReqQueue"})
public class ReadExecutorService implements ReadExecutorServiceRemote, ReadExecutorServiceLocal {
	private static final Logger logger = LoggerFactory.getLogger(ReadExecutorService.class);
	
    /**
     * Default constructor. 
     */
    public ReadExecutorService() {
//    	logger.trace("ReadExecutorService corriendo en module {}", moduleName);
//    	if (moduleName.equals("OperationalCore")) {
//			initReaderTrhead();
//		}
    }
    
    private ExecutorService asyncExecSer;
    private ExecutorService socketReadExecSer;
    @EJB(beanName="NonClosingAsyncTCP")
    private NonClosingAsyncTCPLocal nonClosingAsyncTCP;
    @EJB(beanName="AsyncReqQueue")
	private AsyncReqQueueLocal aReqQueue;
    @EJB(beanName="TransactionHandler")
	private TransactionHandlerLocal transactionHandler;
    private PaymentChannel paymentChannel; 
    private ArrayList<TransactionType> transactionTypes;
    @Resource(lookup = "java:module/ModuleName")
    String moduleName;
    
    @PostConstruct
    public void init() {
    	logger.debug("NonClosingAsyncTCP: Metodo @PostConstruct");
		logger.trace("NonClosingAsyncTCP corriendo en module {}", moduleName);
		if (moduleName.equals("OperationalCore")) {
			initReaderTrhead();
		}
    }

    private void initReaderTrhead() {
    	Session session = null;
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (ExceptionInInitializerError err) {
			logger.error(err.toString());
		}
    	try {
			paymentChannel = (PaymentChannel) session.createQuery(
					"from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
					.setString("codeName", "REC_MOVISTAR") //TODO Hacer configurable
					.uniqueResult();
			logger.debug(paymentChannel.getCodeName());
			paymentChannel.getTransactionRespTypes().forEach((codeN, trxRespType)->{
				logger.trace("ReadExecutorService RespTypes -> " + codeN + " " + trxRespType.toString());
			});
			paymentChannel.getTemplateMessage().forEach((templMsg)->{
				logger.trace("ReadExecutorService TemplateMsgs -> " + templMsg.toString());
			});
		} catch (Exception e) {
			logger.error(e.toString());
		}
    	transactionTypes = null;
    	try {
    		transactionTypes = (ArrayList<TransactionType>) session.createQuery(
				    "from TransactionType as transactionType where transactionType.templateMessage = :templateMessage")
				    .setEntity("templateMessage", paymentChannel.getTemplateMessage().get(0))
				    .list();
    		transactionTypes.forEach(transactionType->{
    			logger.trace("ReadExecutorService TransactionTypes -> " + transactionType.toString());
    		});
		} catch (Exception e) {
			logger.error(e.toString());
		}
    	
    	logger.debug("Crear hilo que leera de manera asincrona");
    	asyncExecSer = Executors.newSingleThreadExecutor();
    	socketReadExecSer = Executors.newSingleThreadExecutor();
    	//Lanzar hilo que lee de manera asincrona
    	logger.debug("Crear hilo que leera de manera asincrona");
    	Callable<Void> callableRead = new Callable<Void>() {
		    public Void call() throws Exception {
		    	boolean keepReading = true;
		    	while (keepReading) {
		    		try {
						int size = 1000; //TODO Tamano?
						ByteBuffer responseMsgB = ByteBuffer.allocate(size);
						logger.trace("Reader thread: executor callable before while");
						logger.debug("Antes de entrar en while de espera por lectura");
						//Esperar que haya datos que leer en la conexion
						logger.trace("nonClosingAsyncTCP {} null", nonClosingAsyncTCP != null ? "!=" : "==");
						logger.trace("nonClosingAsyncTCP.getTcpSocket() {} null", nonClosingAsyncTCP.getTcpSocket() != null ? "!=" : "==");
						while (nonClosingAsyncTCP.getTcpSocket().read(responseMsgB) < 1)
							;
						//asyncReadF.onReadQueueResult(aReqQueue.getcQueue().get(trxId).getTrxByteB());
						logger.trace("Reader thread: executor callable after while");
						logger.debug("Despues de while de espera, recibiendo datos...");
						logger.debug("Hilo lector recibio: "
								+ (responseMsgB != null ? new String(responseMsgB.array()) : "responseMsgB is null"));
						if(responseMsgB != null) {
							logger.debug("responseMsgB != null");
							TransactionFactory transactionFact = new TransactionFactory();
							IsoMessage isoMessageR = null;
							//isoMessageR = resMsgFactory.parseMessage(responseMsgB.array(), transactionTypes.get(0).getIsoTemplate().getHeader().length());
							logger.debug("Searching parsing type...");
							//Parsear la trama recibida
							isoMessageR = transactionFact.parseMessage(responseMsgB.array(), transactionTypes);
							if(isoMessageR != null) {
								logger.debug("isoMessageR != null");
								TransactionFactory.printIsoMessage(isoMessageR);
								//Determinar el tipo de mensaje
								//Descartar si es una respuesta de un echo request
								Transaction boundTrx;
								switch(isoMessageR.getType()) {
									case 0x210:
										//Respuesta de recarga
										logger.debug("Respuesta de recarga");
										boundTrx = searchTrx(isoMessageR);
										logger.trace("boundTrx: " + boundTrx.toString());
										boolean appTrx = isoMessageR.getField(39).getValue().equals("00") ? true : false;
										logger.debug("Respuesta de transaccion{}aprobada", appTrx ? " " : " no ");
										//Colocar respuesta en la cola
										if(aReqQueue.getcQueue().get(boundTrx.getId()) == null) {
											aReqQueue.getcQueue().put(boundTrx.getId(), new AsyncReqElem(
													boundTrx.getId(),
													"",
													responseMsgB,
													false,
													false,
													aReqQueue.getReqDQueue().get(boundTrx.getId())
													));
										} else {
											aReqQueue.getcQueue().get(boundTrx.getId()).setTrxId(boundTrx.getId());
											aReqQueue.getcQueue().get(boundTrx.getId()).setTrxTrace("");
											aReqQueue.getcQueue().get(boundTrx.getId()).setTrxByteB(responseMsgB);
											//aReqQueue.getcQueue().get(boundTrx.getId()).setTimedOut(aReqQueue.getcQueue().get(boundTrx.getId()).isTimedOut() ? true : false);
											//Si la transaccion fue aprobada fuera de tiempo marcar para enviar reverso
											aReqQueue.getcQueue().get(boundTrx.getId()).setAutoRevSent(aReqQueue.getcQueue().get(boundTrx.getId()).isTimedOut() ? true : false);
											aReqQueue.getcQueue().get(boundTrx.getId()).setTrxData(aReqQueue.getReqDQueue().get(boundTrx.getId()));
										}
										//Guardar parametros de la respuesta
										logger.debug("Guardando parametros de la respuesta...");
										AssortedParameters resParams = null;
										TransactionFactory transactionFactory = new TransactionFactory();
										resParams = transactionFactory.getTransactionParameters(isoMessageR, boundTrx.getTransactionType().getTransactionResponseType().getIsoTemplate(), boundTrx);
										resParams.getStorableParams().forEach((key, param) -> {
											boundTrx.getResponseParameters().add(new TransactionResponseParameter(boundTrx, key, param));
										});
										Session sessionRes = null;
								    	try {
								    		sessionRes = HibernateUtil.getSessionFactory().openSession();
										} catch (ExceptionInInitializerError err) {
											logger.error(err.toString());
										}
								    	sessionRes.beginTransaction();
								    	//sessionRes.update(boundTrx);
								    	sessionRes.merge(boundTrx);
								    	sessionRes.getTransaction().commit();
								    	logger.debug("Parametros de la respuesta guardados");
										//Generar reverso si fue aprobada fuera de tiempo
										boolean timedOut = aReqQueue.getcQueue().get(boundTrx.getId()).isTimedOut();
										logger.debug("Respuesta de transaccion{}aprobada", appTrx ? " " : " no ");
										logger.debug("Respuesta{}tardia", timedOut ? " " : " no ");
								    	if(appTrx && timedOut) {
											logger.debug("Transaccion aprobada fuera de tiempo, generando reverso...");
											TransactionType inverseTransactionType = transactionHandler.getTransactionInverse(
													"MOVISTAR_RECHARGE_REQ", 
													"REC_MOVISTAR"
													);
											logger.trace("Id de transaccion {} - Transaccion inversa de recarga: {}", boundTrx.getId(), inverseTransactionType);
//											transactionHandler.executeInverse(
//													aReqQueue.getcQueue().get(boundTrx.getId()).getTrxData(), 
//													inverseTransactionType);
//											transactionHandler.executeInverse(
//													aReqQueue.getReqDQueue().get(boundTrx.getId()), 
//													inverseTransactionType, 
//													boundTrx.getId());
											transactionHandler.executeLateInverse(
													aReqQueue.getReqDQueue().get(boundTrx.getId()), 
													inverseTransactionType, 
													boundTrx.getId());
										}
								    	
										break;
									case 0x430:
										//Respuesta de reverso
										logger.debug("Respuesta de reverso");
										boundTrx = searchTrx(isoMessageR);
										logger.trace("boundTrx: " + boundTrx.toString());
										break;
									case 0x810:
										//Respuesta de echo
										logger.debug("Respuesta de echo");
										break;
									default:
										//Otro caso
										logger.debug("Default");
										break;
								}
							} else {
								logger.trace("isoMessageR == null");
							}	
						}
					} catch (Exception e) {
						logger.error(e.toString());
						if(e instanceof EJBException) {
							logger.error("Captura de excepcion EJB");
							keepReading = false;
						}
						if(e instanceof ClosedChannelException) {
							logger.error("Captura de excepcion por canal de comunicacion cerrado");
							keepReading = false;
						}
						if(e instanceof IOException) {
							logger.error("Captura de excepcion por canal de comunicacion cerrado (IO)");
							keepReading = false;
						}
						e.printStackTrace();
						if(nonClosingAsyncTCP == null) keepReading = false;
					}
				
		    	}
				return null;
		    }
		};
		
		socketReadExecSer.submit(callableRead);
    }
    
    private Transaction searchTrx(IsoMessage isoMessageR) {
    	//Identificar la transaccion que origino la trama de respuesta
		Transaction prevTrx = null;
    	Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			String trxPrevQuery = 							
					"select transaction "
					+ "from Transaction as transaction "
					+ "inner join transaction.parameters as traceParam "
//					+ "inner join transaction.parameters as dateParam "
					+ "inner join transaction.parameters as localtimeParam "
					+ "inner join transaction.parameters as localdateParam "
					+ "where traceParam.paramName = :traceParamN "
					+ "and traceParam.paramValue = :traceParamV "
//					+ "and dateParam.paramName = :dateTimeParamN "
//					+ "and dateParam.paramValue = :dateTimeParamV "
					+ "and localtimeParam.paramName = :localTimeParamN "
					+ "and localtimeParam.paramValue = :localTimeParamV "
					+ "and localdateParam.paramName = :localDateParamN "
					+ "and localdateParam.paramValue = :localDateParamV "
					;
			logger.trace("traceParamV: " + isoMessageR.getAt(11).getValue().toString().replaceFirst("^0+(?!$)", ""));
			logger.trace("dateTimeParamV: " + isoMessageR.getAt(7).getValue().toString());
			logger.trace("localTimeParamV: " + isoMessageR.getField(12).toString());
			logger.trace("localDateParamV: " + isoMessageR.getField(13).toString());
			ArrayList<Transaction> transList = (ArrayList<Transaction>)session.createQuery(trxPrevQuery)
					.setString("traceParamN", "trace")
					.setString("traceParamV", isoMessageR.getAt(11).getValue().toString().replaceFirst("^0+(?!$)", ""))
//					.setString("dateTimeParamN", "date_time")
//					.setString("dateTimeParamV", isoMessageR.getAt(7).getValue().toString())
					.setString("localTimeParamN", "local_time")
					.setString("localTimeParamV", isoMessageR.getField(12).toString())
					.setString("localDateParamN", "local_date")
					.setString("localDateParamV", isoMessageR.getField(13).toString())
				    .list();
			if(transList != null) {											
				logger.trace("transList != null and has " + String.valueOf(transList.size()) + " elements");
				logger.trace("transList(0) ->  " + transList.get(0).toString());
				transList.forEach(transaction->{
					logger.trace("transaction prev -> " + transaction.toString());
				});
				prevTrx = transList.get(0);
			} else {
				logger.trace("transList == null");
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return prevTrx;
		
    }
    
	/**
	 * @return the asyncExecSer
	 */
	public ExecutorService getAsyncExecSer() {
		return asyncExecSer;
	}

	/**
	 * @param asyncExecSer the asyncExecSer to set
	 */
	public void setAsyncExecSer(ExecutorService asyncExecSer) {
		this.asyncExecSer = asyncExecSer;
	}

	/**
	 * @return the socketReadExecSer
	 */
	public ExecutorService getSocketReadExecSer() {
		return socketReadExecSer;
	}

	/**
	 * @param socketReadExecSer the socketReadExecSer to set
	 */
	public void setSocketReadExecSer(ExecutorService socketReadExecSer) {
		this.socketReadExecSer = socketReadExecSer;
	}

	@PreDestroy
	public void close() {
		logger.debug("ReadExecutorService: Metodo @PreDestroy");
		if (moduleName.equals("OperationalCore")) {
			try {
				socketReadExecSer.shutdown();
			} catch (Exception e) {
				logger.error(e.getMessage());
			} 
		}
	}
}
