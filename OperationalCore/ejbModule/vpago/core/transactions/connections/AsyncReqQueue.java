package vpago.core.transactions.connections;

import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.transactions.classes.TransactionData;

/**
 * Session Bean implementation class AsyncReqQueue
 */
@Singleton
@LocalBean
public class AsyncReqQueue implements AsyncReqQueueRemote, AsyncReqQueueLocal {

	private static final Logger logger = LoggerFactory.getLogger(AsyncReqQueue.class);
	
    /**
     * Default constructor. 
     */
    public AsyncReqQueue() {
//    	logger.trace("AsyncReqQueue corriendo en module {}", moduleName);
//    	if (moduleName.equals("OperationalCore")) {
//			this.cQueue = new ConcurrentHashMap<Integer, AsyncReqElem>();
//			this.reqDQueue = new ConcurrentHashMap<Integer, TransactionData>();
//		}
    }

    /** Estructura para almacenar la informacion de las transacciones que se estan enviando **/
    private ConcurrentHashMap<Integer, AsyncReqElem> cQueue;
    /** Estructura con los datos de las transacciones **/
    private ConcurrentHashMap<Integer, TransactionData> reqDQueue;
    @Resource(lookup = "java:module/ModuleName")
    String moduleName;

	public ConcurrentHashMap<Integer, AsyncReqElem> getcQueue() {
		return cQueue;
	}

	public void setcQueue(ConcurrentHashMap<Integer, AsyncReqElem> cQueue) {
		this.cQueue = cQueue;
	}

	/**
	 * @return the reqDQueue
	 */
	public ConcurrentHashMap<Integer, TransactionData> getReqDQueue() {
		return reqDQueue;
	}

	/**
	 * @param reqDQueue the reqDQueue to set
	 */
	public void setReqDQueue(ConcurrentHashMap<Integer, TransactionData> reqDQueue) {
		this.reqDQueue = reqDQueue;
	}
   
	@PostConstruct
	public void init() {
		logger.trace("AsyncReqQueue corriendo en module {}", moduleName);
    	if (moduleName.equals("OperationalCore")) {
			this.cQueue = new ConcurrentHashMap<Integer, AsyncReqElem>();
			this.reqDQueue = new ConcurrentHashMap<Integer, TransactionData>();
		}
	}
}
