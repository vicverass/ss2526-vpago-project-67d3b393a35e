package vpago.core.transactions.connections;

import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.SocketChannel;

import javax.ejb.Local;

@Local
public interface NonClosingAsyncTCPLocal {
	
	public SocketChannel getTcpSocket();
	public boolean reconnectSocket();
	
}
