package vpago.core.transactions.connections;

import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.sun.media.jfxmedia.logging.Logger;

public class AsyncReadFuture implements Future<ByteBuffer> {
	
	private volatile ByteBuffer result = null;
    private volatile boolean cancelled = false;
    private final CountDownLatch countDownLatch;
	
    public AsyncReadFuture() {
		super();
		System.out.println("AsyncReadFuture -> constructor");
		countDownLatch = new CountDownLatch(1);
	}

	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		System.out.println("AsyncReadFuture -> cancel");
		if (isDone()) {
            return false;
        } else {
            countDownLatch.countDown();
            cancelled = true;
            return !isDone();
        }
	}

	@Override
	public boolean isCancelled() {
		System.out.println("AsyncReadFuture -> isCancelled");
		return cancelled;
	}

	@Override
	public boolean isDone() {
		System.out.println("AsyncReadFuture -> isDone");
		return countDownLatch.getCount() == 0;
	}

	@Override
	public ByteBuffer get() throws InterruptedException, ExecutionException {
		System.out.println("AsyncReadFuture -> get");
		countDownLatch.await();
        return result;
	}

	@Override
	public ByteBuffer get(long timeout, TimeUnit unit) 
			throws InterruptedException, ExecutionException, TimeoutException {
		System.out.println("AsyncReadFuture -> get with timeout -> before await call");
		boolean completedInTime = countDownLatch.await(timeout, unit);
		if(!completedInTime) throw new TimeoutException();
		System.out.println("AsyncReadFuture -> get with timeout -> after await call");
        return result;
	}

	public void onReadQueueResult(final ByteBuffer result) {
		System.out.println("AsyncReadFuture -> onReadQueueResult");
		this.result = result;
        countDownLatch.countDown();
    }
}
