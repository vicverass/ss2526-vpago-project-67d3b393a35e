package vpago.core.transactions.connections;

import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;

import vpago.core.transactions.classes.TransactionData;

@Local
public interface AsyncReqQueueLocal {
	
	public ConcurrentHashMap<Integer, AsyncReqElem> getcQueue();
	public ConcurrentHashMap<Integer, TransactionData> getReqDQueue();
}
