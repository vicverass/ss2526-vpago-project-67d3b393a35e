package vpago.core.transactions.connections;

import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;

import javax.ejb.EJB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LateResponseHandler implements CompletionHandler<Integer,  TransactionAttachment> {
	private static final Logger logger = LoggerFactory.getLogger(LateResponseHandler.class);
	
	@EJB(beanName="AsyncReqQueue")
	private AsyncReqQueueLocal aReqQueue;

	@Override
	public void completed(Integer result, TransactionAttachment attachment) {
		logger.debug("LateResponseHandler - completed");
		logger.debug("TransactionAttachment - trace: {}", attachment.getTrxTrace());
		String respStr = new String(attachment.getMsgRespB().array(), StandardCharsets.ISO_8859_1);
		logger.debug("TransactionAttachment - buf: {}", respStr);
		//aReqQueue.getcQueue().put(attachment.getTrxId(), new AsyncReqElem(attachment.getTrxTrace(), attachment.getMsgRespB()));
		attachment.getaReqQueue().getcQueue().put(attachment.getTrxId(), new AsyncReqElem(attachment.getTrxTrace(), attachment.getMsgRespB()));
	}

	@Override
	public void failed(Throwable exc, TransactionAttachment attachment) {
		logger.debug("LateResponseHandler - failed");
		logger.debug("TransactionAttachment - trace: {}", attachment.getTrxTrace());
		String respStr = new String(attachment.getMsgRespB().array(), StandardCharsets.ISO_8859_1);
		logger.debug("TransactionAttachment - buf: {}", respStr);
	}
	
}
