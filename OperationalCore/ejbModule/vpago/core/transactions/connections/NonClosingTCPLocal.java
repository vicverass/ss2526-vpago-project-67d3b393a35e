package vpago.core.transactions.connections;

import java.net.Socket;

import javax.ejb.Local;

@Local
public interface NonClosingTCPLocal {
	
	public Socket getTcpSocket();
	public boolean reconnectSocket();

}
