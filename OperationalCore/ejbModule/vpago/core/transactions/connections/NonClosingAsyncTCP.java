package vpago.core.transactions.connections;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session Bean implementation class NonClosingAsyncTCP
 */
@Startup
@Singleton
@LocalBean
public class NonClosingAsyncTCP implements NonClosingAsyncTCPRemote, NonClosingAsyncTCPLocal {
	private static final Logger logger = LoggerFactory.getLogger(NonClosingAsyncTCP.class);
    
	/**
     * Default constructor. 
     */
    public NonClosingAsyncTCP() {
    	
    }
    
    /** Host con el que se realizara la conexion **/
    private String authHost;
    /** Puerto en el que se realizara la conexion **/
	private int authPort;
	/** Objeto socket para establecer la conexion **/
	private SocketChannel tcpSocket;
	@Resource(lookup = "java:module/ModuleName")
    String moduleName;
	
	@PostConstruct
	public void init() {
		logger.debug("NonClosingAsyncTCP: Metodo @PostConstruct");
		logger.trace("NonClosingAsyncTCP corriendo en module {}", moduleName);
		
    	if (moduleName.equals("OperationalCore")) {
			//authHost = "10.164.7.134"; 
			authHost = "127.0.0.1";
			//authHost = "192.168.1.219";
			//authHost = "201.249.143.134";
			//authHost = "172.28.111.165"; //Desarrollo y testing
			//authHost = "10.164.15.178"; //Produccion
			//authHost = "200.74.197.113";
			//authPort = 9115;
			authPort = 9000; //Produccion
			//authPort = 8001; //Desarrollo
			//authPort = 8002; //Testing
			//authPort = 9118;
			//authPort = 2526;
			try {
				if (tcpSocket == null) {
					logger.debug("Creando socket asincrono permanente");
					tcpSocket = SocketChannel.open();
					InetSocketAddress hostAddress = new InetSocketAddress(authHost, authPort);
					tcpSocket.connect(hostAddress);

					if (tcpSocket.isOpen()) {
						logger.debug("Conexion con {}:{} establecida", authHost, authPort);
					}
				}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.getMessage());
			} 
		} 
	}
	
	@PreDestroy
	public void close() {
		logger.debug("NonClosingAsyncTCP: Metodo @PreDestroy");
		logger.trace("NonClosingAsyncTCP corriendo en module {}", moduleName);
		if (moduleName.equals("OperationalCore")) {
			try {
				tcpSocket.close();
				logger.debug("Conexion a {}:{} cerrada", authHost, authPort);
			} catch (IOException e) {
				logger.error(e.getMessage());
			} 
		}
	}
	
	/**
	 * @return the authHost
	 */
	public String getAuthHost() {
		return authHost;
	}
	
	/**
	 * @param authHost the authHost to set
	 */
	public void setAuthHost(String authHost) {
		this.authHost = authHost;
	}
	
	/**
	 * @return the authPort
	 */
	public int getAuthPort() {
		return authPort;
	}
	
	/**
	 * @param authPort the authPort to set
	 */
	public void setAuthPort(int authPort) {
		this.authPort = authPort;
	}
	
	/**
	 * @return the tcpSocket
	 */
	public SocketChannel getTcpSocket() {
		return tcpSocket;
	}
	
	/**
	 * @param tcpSocket the tcpSocket to set
	 */
	public void setTcpSocket(SocketChannel tcpSocket) {
		this.tcpSocket = tcpSocket;
	}
	
	public boolean reconnectSocket() {
		logger.trace("NonClosingAsyncTCP corriendo en module {}", moduleName);
		if (moduleName.equals("OperationalCore")) {
			SocketAddress sockaddr = new InetSocketAddress(authHost, authPort);
			try {
				this.tcpSocket.close();
				this.tcpSocket = null;
				tcpSocket = SocketChannel.open();
				logger.debug("reconnectSocket(): Reconectando socket");
				this.tcpSocket.connect(sockaddr);
			} catch (Exception e) {
				logger.debug("reconnectSocket(): Error reconectando socket ");
				e.printStackTrace();
			}
			return tcpSocket.isOpen();
		} else return false;
	}

}
