package vpago.core.transactions.connections;

import java.util.concurrent.ExecutorService;

import javax.ejb.Local;

@Local
public interface ReadExecutorServiceLocal {
	
	public ExecutorService getAsyncExecSer();
	public ExecutorService getSocketReadExecSer();
	
}
