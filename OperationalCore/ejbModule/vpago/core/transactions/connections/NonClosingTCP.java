package vpago.core.transactions.connections;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session Bean implementation class NonClosingTCP
 */
// TODO Condicionar startup segun archivo de configuracion
//@Startup
@Singleton
@LocalBean
public class NonClosingTCP implements NonClosingTCPRemote, NonClosingTCPLocal {
	private static final Logger logger = LoggerFactory.getLogger(NonClosingTCP.class);
	
	/**
     * Default constructor. 
     */
    public NonClosingTCP() {
    	
    }
    
    /** Host con el que se realizara la conexion **/
    private String authHost;
    /** Puerto en el que se realizara la conexion **/
	private int authPort;
	/** Objeto socket para establecer la conexion **/
	private Socket tcpSocket;
	
	@PostConstruct
	public void init() {
		logger.debug("NonClosingTCP: Metodo @PostConstruct");
    	
    	//authHost = "10.164.7.134"; 
    	authHost = "127.0.0.1"; 
    	authPort = 9115;
    	try {
    		if(tcpSocket == null) {
    			logger.debug("Creando socket permanente");
    			tcpSocket = new Socket(authHost, authPort);
    			
    			if((!tcpSocket.isClosed()) && (tcpSocket.isConnected())) {
    				logger.debug("Conexion con {}:{} establecida", authHost, authPort);
    			}
    		}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} 
	}
	
	@PreDestroy
	public void close() {
		logger.debug("NonClosingTCP: Metodo @PreDestroy");
		try {
			tcpSocket.close();
			logger.debug("Conexion a {}:{} cerrada", authHost, authPort);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * @return the authHost
	 */
	public String getAuthHost() {
		return authHost;
	}
	
	/**
	 * @param authHost the authHost to set
	 */
	public void setAuthHost(String authHost) {
		this.authHost = authHost;
	}
	
	/**
	 * @return the authPort
	 */
	public int getAuthPort() {
		return authPort;
	}
	
	/**
	 * @param authPort the authPort to set
	 */
	public void setAuthPort(int authPort) {
		this.authPort = authPort;
	}
	
	/**
	 * @return the tcpSocket
	 */
	public Socket getTcpSocket() {
		return tcpSocket;
	}
	
	/**
	 * @param tcpSocket the tcpSocket to set
	 */
	public void setTcpSocket(Socket tcpSocket) {
		this.tcpSocket = tcpSocket;
	}
	
	public boolean reconnectSocket() {
		SocketAddress sockaddr = new InetSocketAddress(authHost, authPort);
		try {
			tcpSocket.connect(sockaddr);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tcpSocket.isConnected();
	}
}
