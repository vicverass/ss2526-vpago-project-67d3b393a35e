package vpago.core.transactions.ext.recmovistar;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import com.solab.iso8583.IsoType;

import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.service.IsoService;

public class MovistarRecharge {
		
	public static void main(String[] args) {
//		Date date = new Date();
//		Calendar cal = Calendar.getInstance();
//	    cal.setTime(date);
//	    String yearD = String.valueOf(cal.get(Calendar.YEAR)).substring(3);
//	    String julDay = String.valueOf(cal.get(Calendar.DAY_OF_YEAR));
//		String hour = IsoType.DATE10.format(date, null).substring(4, 6);
//		System.err.println(yearD+julDay+hour);
//		System.err.println(encodeRef("& 0000200116!0X000094004148074100                           0101  0000  0600000000000000000015717519-000204901020729"));
//		System.err.println(fillDate("123456"));
	}
	
	public static Object toBigDecimal(Terminal terminal, Object param, Map<?, ?> params) {
		return new BigDecimal((String) param);
	}
	
	public static Object loadField12(Terminal terminal, Map<?, ?> params){
		Date date = new Date();
		//TimeZone gmt = TimeZone.getTimeZone("GMT");
		return IsoType.TIME.format(date, null);
	}
	
	public static Object loadField13(Terminal terminal, Map<?, ?> params){
		Date date = new Date();
		//TimeZone gmt = TimeZone.getTimeZone("GMT");
		return IsoType.DATE4.format(date, null);
	}
	
	public static Object loadField15(Terminal terminal, Map<?, ?> params){
		Date date = new Date();
		//TimeZone gmt = TimeZone.getTimeZone("GMT");
		return IsoType.DATE4.format(date, null);
	}
	
	public static Object loadField17(Terminal terminal, Map<?, ?> params){
		Date date = new Date();
		//TimeZone gmt = TimeZone.getTimeZone("GMT");
		return IsoType.DATE4.format(date, null);
	}
	
	public static Object calculateRRN(Terminal terminal, Map<?, ?> params) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    String yearD = String.valueOf(cal.get(Calendar.YEAR)).substring(3);
	    String julDay = String.valueOf(cal.get(Calendar.DAY_OF_YEAR));
		String hour = IsoType.DATE10.format(date, null).substring(4, 6);		
		String reference = IsoService.getReference();
		return yearD + julDay + hour + reference;
	}
	
	public static Object encodeRef(Terminal terminal, Object param, Map<?, ?> params) {
		String field63 = (String)param;
		String apprCode = field63.substring(90, 100);
		return apprCode;
	}
	
	public static Object fillTime(Terminal terminal, Object param , Map<?, ?> params) {
		return ((String)param).concat("00");
	}
	
	public static Object loadTeminal(Terminal terminal, Object param , Map<?, ?> params) {
		return ((String)param).concat("  ");
	}
	
}
