package vpago.core.transactions.ext.cpagocc;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Random;

import org.hibernate.Session;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.service.IsoService;

public class CpagoCc {

	public static Object toBigDecimal(Terminal terminal, Object param, Map<?, ?> params) {
		return new BigDecimal((String) param);
	}
	
	public static Object strToInteger(Terminal terminal, Object param, Map<?, ?> params) {
		return new Integer((String)param);
	}
	
	public static Object calculateReference(Terminal terminal, Map<?, ?> params) {
		String reference = "";
		try {
			System.out.println("calculateBatchData");
			Map<String, String> paramL = (Map<String, String>)params;
			String cardType = paramL.get("card_type");
			CurrentBatch currentBatchT = null;
			for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
				if(currentBatch.getBatchType().getParamValue().equals(cardType)) currentBatchT = currentBatch;
			}
			if(currentBatchT != null) {
				int refNum = currentBatchT.getReferenceCounter() == 999999 ? 1 : currentBatchT.getReferenceCounter()+1;
				reference = String.format("%06d", refNum);
				//Actualizar referencia
				currentBatchT.setReferenceCounter(refNum);
				Session session = null;
				try {
					session = HibernateUtil.getSessionFactory().openSession();
					session.beginTransaction();
					session.update(currentBatchT);
					session.getTransaction().commit();
				} finally {
					if(session != null) session.close();
				}
			} else {
				reference = IsoService.getReference();
			}
		} catch (Exception e) {
			System.err.println("Excepcion en calculateBatchData");
			e.printStackTrace();
		}
		byte[] refLen = {0, 6}, refStr = reference.getBytes();
		byte[] lenPlusRef = new byte[refLen.length + refStr.length];
		System.arraycopy(refLen, 0, lenPlusRef, 0, refLen.length);
		System.arraycopy(refStr, 0, lenPlusRef, refLen.length, refStr.length);	
		System.out.println("calculateReference: " + new String(lenPlusRef));
		return new String(lenPlusRef);
	}
	
	public static Object encodeBcd(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdAff(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		sParam = String.format("%1$-" + 15 + "s", sParam); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdSec(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		sParam = String.format("%1$" + 4 + "s", sParam).replace(' ', '0'); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdCi(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		sParam = String.format("%1$" + 10 + "s", sParam).replace(' ', '0'); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object fromBigDecimal(Terminal terminal, Object param, Map<?, ?> params) {
		return ((BigDecimal)param).toString();
	}
	
	public static Object decodeBcd(Terminal terminal, Object param, Map<?, ?> params) {
		String hex = (String)param;
		//A null string returns an empty array
		if (hex == null || hex.length() == 0) {
			return new byte[0];
		} else if (hex.length() < 3) {
			return new byte[]{ (byte)(Integer.parseInt(hex, 16) & 0xff) };
		}
		//Adjust accordingly for odd-length strings
		int count = hex.length();
		int nibble = 0;
		if (count % 2 != 0) {
			count++;
			nibble = 1;
		}
		byte[] buf = new byte[count / 2];
		char c = 0;
		int holder = 0;
		int pos = 0;
		for (int i = 0; i < buf.length; i++) {
		    for (int z = 0; z < 2 && pos<hex.length(); z++) {
		        c = hex.charAt(pos++);
		        if (c >= 'A' && c <= 'F') {
		            c -= 55;
		        } else if (c >= '0' && c <= '9') {
		            c -= 48;
		        } else if (c >= 'a' && c <= 'f') {
		            c -= 87;
		        }
		        if (nibble == 0) {
		            holder = c << 4;
		        } else {
		            holder |= c;
		            buf[i] = (byte)holder;
		        }
		        nibble = 1 - nibble;
		    }
		}
		return new String(buf);
	}
	
	public static Object maskPan(Terminal terminal, Object param, Map<?, ?> params) {
		String pan = (String)param;
		int 
		upperLimit = pan.length() - 4,
		maskLen = pan.length() - 10;
		CharSequence originSeq = pan.subSequence(6, upperLimit);
		CharSequence maskSeq = pan.substring(6, upperLimit).replaceAll("\\d", "*").subSequence(0, maskLen);
		return pan.replace(originSeq, maskSeq);
	}
	
	public static Object decodeRef(Terminal terminal, Object param, Map<?, ?> params) {
		String ref = ((String)param).substring(6);
		String decRef = (String)decodeBcd(terminal, ref, params);
		return decRef;
	}
	
	public static Object setSecCiLen(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("setSecCiLen: " +  (String)param); 
		String secCi = (String)param;
		byte[] secCiLen = {0, 14}, secCiStr = secCi.getBytes();
		byte[] lenPlusSecCi = new byte[secCiLen.length + secCiStr.length];
		System.arraycopy(secCiLen, 0, lenPlusSecCi, 0, secCiLen.length);
		System.arraycopy(secCiStr, 0, lenPlusSecCi, secCiLen.length, secCiStr.length);
		return new String(lenPlusSecCi);
	}

}
