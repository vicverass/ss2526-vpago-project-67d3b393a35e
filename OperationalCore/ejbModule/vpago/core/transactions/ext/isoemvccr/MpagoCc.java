package vpago.core.transactions.ext.isoemvccr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.Bank;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionParameter;
import vpago.core.transactions.classes.types.TransactionStatus;
import vpago.core.transactions.encryption.DUKPTServer;
import vpago.core.transactions.encryption.TripleDES;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.service.IsoService;
import vpago.core.transactions.tlv.BerTlv;
import vpago.core.transactions.tlv.BerTlvParser;
import vpago.core.transactions.tlv.BerTlvs;
import vpago.core.transactions.tlv.HexUtil;
import vpago.core.transactions.util.Utils;

public class MpagoCc {
		
	public static void main(String[] args) {
//		System.err.println(parseEncTlv0200("C00A02410309160170E0002EC408454139FFFFFF9019C2820180C270077C4CCC9BC599BEE173A04AE46B0249C9AAC7643AD5F4151FAA5A90EFE6EEE53413DCB5CACA50CA88FC63537915123D57E3F4B387AE1403B30069E36CC31E8CA3D5BF5B44C47097F0818544EEF7FB778E41EB11830883ECFFC1C2BF2C93186BCCD0C429A36DAA449C6CFDD2F5631F7D9A1C8D3A1C6A90B470833FD33B205402940880C7E2381F01A38493A22CD1710EA39533AB1D721A7D9EC25052BBDAF49625F2037B44B717A8193C2DBBF338FBF4F90BF46BAD137B5C67FB3080C907AC6D1AAB387F708D9D3792D323724F15254BC278172576A4D2CE9FC0F3577A8E76B605B6736B98EEFC1F7942B67AB3AEDB345AE91354C89FC45F528596D9A3C328C873D97923D67C51C80DDB0EBE4643AD94419C2DC3107B9395D7B789A7AFA67552E0DE7C1581BA795B0845FCB30F6E4AD5DB7FF872ECDA2B70D7D2F89D0B5F71AD59B55A5D261A0BFB5A724756BFD4F1AEA998BE538D7E884C585A67BB0EFD80B27E3F761CE4215EEF92789BCE107655D8F07507F76DD97D13D769908A8DA94F07A000000003101082025C009505022000E0009A035008309B02E8009C01005F24031902285F2A0200589F02060000001500009F03060000000000009F0607A00000000310109F100706010A03A020009F12074352454449544F9F1A0200589F1C08AC7C0F8D000000009F2608EA04A889370EFE379F2701809F330360F8C89F34031E03009F360200579F3704E518F0B5DF826E0F575050323234363336303130313730DF080A02430309160170E00016CD087E05A805577FB8FC="));
//		System.err.println(parseEncTlv0200("4f07a0000000041010500a4d41535445524341524457105406285657655847d1908201198113625a085406285657655847820218008407a00000000410108e10000000000000000042011e0342031f03950580200080009a035009049b0268009c01005f201a434f52524f2f454d494c592052454245434120202020202020205f24031908315f25031604085f2a0200585f300202015f3401009f01060000000000019f02060000002600009f03060000000000009f0607a00000000410109f0702ff009f090200029f0d05b0509c88009f0e0500000000009f0f05b0709c98009f10120110a00001240000000000000000000000ff9f120a4d4153544552434152449f160f72f53f6023ff5713000000000000009f1a0200589f1c08c452e043000000009f1e0831323334353637389f21030212529f260809785df0d30c8acb9f2701809f330360f8c89f34031e03009f3501219f3602002f9f37048ef86c9b9f3901059f40057e0020b0019f4104000001709f4e0d54657374204d65726368616e74df8330020200df836b050020191602"));
//		System.err.println(encodeBcdSec("    "));
//		formatPan("6799998900000200069");
//		System.out.println("");
//		formatPan("679999890");
//		System.out.println("\n");
//		formatPan("6799");
//		System.out.println("\n");
//		formatPan("679957654");
//		System.out.println("formatRecPan " + formatRecPan("1906799998900000200069"));
//		System.out.println("formatRecPan " + formatRecPan("196799998900000200069"));
//		System.out.println("formatRecPan " + formatRecPan("18679999890000020006"));
//		calcField63Len("00223333313330333031303030313030303030303030303300083337303030303031");
//		System.out.println(new String(preProcAdvc("60000000030230602801000A8000001654133300890200112330000011190804020537323136313730303131313930303030303032323032".getBytes())));
//		System.out.println("-------------------------------------------------------------------------------");
//		System.out.println(new String(preProcAdvc("60000000030230602801000A80000019054133300890200119992330000011190804020537323136313730303131313930303030303032323032".getBytes())));
//		System.out.println("-------------------------------------------------------------------------------");
//		System.out.println(new String(preProcAdvc("60000000030230202801000A8000002330000011190804020537323136313730303131313930303030303032323032".getBytes())));
//		System.out.println(formatTrack2("4427808001112223337D22122011822239289F"));
//		System.out.println("");
//		System.out.println(formatTrack2("2239289F"));
//		System.out.println(formatTrack2("6799998900000200069D2512220085130519"));
//		System.out.println(loadPanFromTrack2("3354133300890200111D1412601079360805F"));
//		System.out.println(storeBatch("001000083337303030303133"));
//		System.out.println(storeBatch("003400223333383838383838383831363032323232303030304200083337303030303031"));
	}
	
	
	public static Object toBigDecimal(Terminal terminal, Object param, Map<?, ?> params) {
		return new BigDecimal((String) param);
	}
	
	public static Object strToInteger(Terminal terminal, Object param, Map<?, ?> params) {
		return new Integer((String)param);
	}
	
	public static Object calculateReference(Terminal terminal, Map<?, ?> params) {		
		String reference = "";
		try {
			System.out.println("calculateReference");
			Map<String, String> paramL = (Map<String, String>)params;
			String cardType = paramL.get("card_type");
			CurrentBatch currentBatchT = null;
			for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
				if(currentBatch.getBatchType().getParamValue().equals(cardType)) currentBatchT = currentBatch;
			}
			if(currentBatchT != null) {
				int refNum = currentBatchT.getReferenceCounter() == 999999 ? 1 : currentBatchT.getReferenceCounter()+1;
				reference = String.format("%06d", refNum);
				//Actualizar referencia
				currentBatchT.setReferenceCounter(refNum);
			} else {
				reference = IsoService.getReference();
			}
		} catch (Exception e) {
			System.err.println("Excepcion en calculateReference");
			e.printStackTrace();
		}
		byte[] refLen = {0, 6}, refStr = reference.getBytes();
		byte[] lenPlusRef = new byte[refLen.length + refStr.length];
		System.arraycopy(refLen, 0, lenPlusRef, 0, refLen.length);
		System.arraycopy(refStr, 0, lenPlusRef, refLen.length, refStr.length);	
		System.out.println("calculateReference: " + new String(lenPlusRef));
		return new String(lenPlusRef);
	}
	
	public static Object encodeloadedRef(Terminal terminal, Object param, Map<?, ?> params) {
		char[] chars = new char[1];
		try {
			String sParam = String.format("%06d", Integer.parseInt((String) param)); 
			System.err.println("encodeloadedRef " + sParam);
			char[] HEX = "0123456789ABCDEF".toCharArray();
			byte[] buffer = sParam.getBytes();
			int start = 0, length = sParam.length();
			if (buffer.length == 0) {
				return "";
			}
			int holder = 0;
			chars = new char[length * 2];
			int pos = -1;
			for (int i = start; i < start+length; i++) {
				holder = (buffer[i] & 0xf0) >> 4;
				chars[++pos * 2] = HEX[holder];
				holder = buffer[i] & 0x0f;
				chars[(pos * 2) + 1] = HEX[holder];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0006".concat(new String(chars));
	}
	
	public static Object encodeBcd(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdAff(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		sParam = String.format("%1$-" + 15 + "s", sParam); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdSec(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		if(sParam.equals("    ")) return "20202020";
		sParam = String.format("%1$" + 4 + "s", sParam).replace(' ', '0'); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdCi(Terminal terminal, Object param, Map<?, ?> params) {
		char[] HEX = "0123456789ABCDEF".toCharArray();
		String sParam = (String)param;
		sParam = String.format("%1$" + 10 + "s", sParam).replace(' ', '0'); 
		byte[] buffer = sParam.getBytes();
		int start = 0, length = sParam.length();
		if (buffer.length == 0) {
			return "";
		}
		int holder = 0;
		char[] chars = new char[length * 2];
        int pos = -1;
		for (int i = start; i < start+length; i++) {
			holder = (buffer[i] & 0xf0) >> 4;
			chars[++pos * 2] = HEX[holder];
			holder = buffer[i] & 0x0f;
			chars[(pos * 2) + 1] = HEX[holder];
		}
		return new String(chars);
	}
	
	public static Object encodeBcdKSN(Terminal terminal, Object param, Map<?, ?> params) {
		String encKsn = ""; 
		try {
			encKsn = "00223333" + encodeBcd(terminal, param, params);			
			System.out.println("KSN = " + encKsn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encKsn;
	}
	
	public static Object calculateTerm(Terminal terminal, Object param, Map<?, ?> params) {
		String term = "";
		term = ((String)param).substring(0, 5) + ((String)param).substring(6, 8);
		return term;
	}
	
	public static Object fromBigDecimal(Terminal terminal, Object param) {
		return ((BigDecimal)param).toString();
	}
	
	public static Object decodeBcd(Terminal terminal, Object param, Map<?, ?> params) {
		String hex = (String)param;
		//A null string returns an empty array
		if (hex == null || hex.length() == 0) {
			return new byte[0];
		} else if (hex.length() < 3) {
			return new byte[]{ (byte)(Integer.parseInt(hex, 16) & 0xff) };
		}
		//Adjust accordingly for odd-length strings
		int count = hex.length();
		int nibble = 0;
		if (count % 2 != 0) {
			count++;
			nibble = 1;
		}
		byte[] buf = new byte[count / 2];
		char c = 0;
		int holder = 0;
		int pos = 0;
		for (int i = 0; i < buf.length; i++) {
		    for (int z = 0; z < 2 && pos<hex.length(); z++) {
		        c = hex.charAt(pos++);
		        if (c >= 'A' && c <= 'F') {
		            c -= 55;
		        } else if (c >= '0' && c <= '9') {
		            c -= 48;
		        } else if (c >= 'a' && c <= 'f') {
		            c -= 87;
		        }
		        if (nibble == 0) {
		            holder = c << 4;
		        } else {
		            holder |= c;
		            buf[i] = (byte)holder;
		        }
		        nibble = 1 - nibble;
		    }
		}
		return new String(buf);
	}
	
	public static Object maskPan(Terminal terminal, Object param, Map<?, ?> params) {
		String pan = (String)param;
		int 
		upperLimit = pan.length() - 4,
		maskLen = pan.length() - 10;
		CharSequence originSeq = pan.subSequence(6, upperLimit);
		CharSequence maskSeq = pan.substring(6, upperLimit).replaceAll("\\d", "*").subSequence(0, maskLen);
		return pan.replace(originSeq, maskSeq);
	}
	
	public static Object decodeRef(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("decodeRef -> " + (String)param);
		String ref = ((String)param).substring(4);
		String decRef = (String)decodeBcd(terminal, ref, params);
		return decRef;
	}
	
	public static Object setSecCiLen(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("setSecCiLen: " +  (String)param); 
		String secCi = (String)param;
		byte[] secCiLen = {0, 14}, secCiStr = secCi.getBytes();
		byte[] lenPlusSecCi = new byte[secCiLen.length + secCiStr.length];
		System.arraycopy(secCiLen, 0, lenPlusSecCi, 0, secCiLen.length);
		System.arraycopy(secCiStr, 0, lenPlusSecCi, secCiLen.length, secCiStr.length);
		return new String(lenPlusSecCi);
	}
	

	public static Object accTypeToPcode(Terminal terminal, Object param, Map<?, ?> params) {
		String accountType = (String)param;
		String pCode = "000000";
		switch(accountType) {
			case "0":
				//Compra credito
				pCode = "000000";
			break;
			case "1":
				//Compra debito cuenta principal 
				pCode = "001000";
			break;
			case "2":
				//Compra debito cuenta ahorro
				pCode = "002000";
			break;
			case "3":
				pCode = "003000";
			break;
			case "4":
				pCode = "004000";
			break;
			default:
				pCode = "000000";
			break;		
		}
		return pCode;
	}
	
	public static Object pCodeToPcodeAdv(Terminal terminal, Object param, Map<?, ?> params) {
		String accountType = (String)param;
		String pCode = "230000";
		switch(accountType) {
			case "000000":
				pCode = "230000";
			break;
			case "001000":
				pCode = "231000";
			break;
			case "002000":
				pCode = "232000";
			break;
			case "003000":
				pCode = "233000";
			break;
			case "004000":
				pCode = "234000";
			break;
			default:
				pCode = "230000";
			break;		
		}
		return pCode;
	}
	
	public static Object accTypeToPcodeAnnul(Terminal terminal, Object param, Map<?, ?> params) {
		String pCode = (String)param;
		return pCode != null ? "02".concat(pCode.substring(2)) : "020000";
	}
	
	public static Object optToPanEntMode(Terminal terminal, Object param, Map<?, ?> params) {
		String accountType = (String)param;
		return "0" + accountType;
	}
	
	public static Object optToPinEntMode(Terminal terminal, Object param, Map<?, ?> params) {
		String accountType = (String)param;
		String pCode = "0";
		switch(accountType) {
			case "0":
				pCode = "0";
			break;
			case "1":
				pCode = "1";
			break;
			case "2":
				pCode = "2";
			break;
			default:
				pCode = "0";
			break;		
		}
		return pCode;
	}
	
	public static Object toTableId03(Terminal terminal, Object param, Map<?, ?> params) {
		String fLen = "0022",
		tableId = "3033",
		deviceSerial = (String)param;
		Integer devSerLen = deviceSerial.length();
		//deviceSerial = (String) encodeBcd(String.format("%020d", Integer.parseInt((String)param)));
		for(int i=0; i<20-devSerLen; i++) deviceSerial = "0"+deviceSerial;
		deviceSerial = (String) encodeBcd(terminal, deviceSerial, params);
		return fLen + tableId + deviceSerial;
	}
	
	
	public static Object formatPan(Terminal terminal, Object param, Map<?, ?> params) {
		String  finalPan = "",
				panLenStr = "",
				panStr = (String)param;
		Integer panLen = panStr.length();
		panLenStr = String.valueOf(panLen); 
		finalPan =  String.format("%1$" + 2 + "s", panLenStr).replace(" ", "0") 
				+ ( (panLen%2 == 0) ? panStr : String.format("%1$-" + (panLen+1) + "s", panStr).replace(" ", "F") );
		//System.out.print("finalPan: " + finalPan);
		return finalPan;
	}
	
	public static Object formatPan(Terminal terminal, Object param) {
		return formatPan(terminal, param, null);
	}
	
	public static Object formatRecPan(Terminal terminal, Object param, Map<?, ?> params) {
		String panRec = (String)param,
			   panLen = panRec.substring(0, 2)
			;
		System.out.println("panRec.substring(2, 3) " + panRec.substring(2, 3));
		return (Integer.parseInt(panLen)%2 == 0) ? 
				panRec.substring(2) : 
				(panRec.substring(2, 3).equals("0") ? panRec.substring(3) : panRec.substring(2));
	}
	
	public static Object calcField63Len(Terminal terminal, Object param, Map<?, ?> params) {
		String field63="", field63Len="";
		try {
			System.out.println("calcField63Len");
			field63 = (String)param;
			field63Len = String.format("%1$" + 4 + "s", Integer.valueOf((field63.length()/2))).replace(" ", "0");		
				   ;
			System.out.println("field63Len+field63: " + field63Len+field63 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return field63Len+field63; 
	}
	
	public static Object parseEncTlv0200(Terminal terminal, Object param, Map<?, ?> params) {
//		System.err.println("parseEncTlv0200");
		String tag55 = "";
		try {
			//Obtener BDK
			String reqTlv = (String)param;
//			System.err.println("reqTlv " + reqTlv);

			byte[] bytesDec; 
			
			BerTlvParser parser = new BerTlvParser();
			
			bytesDec = HexUtil.parseHex(reqTlv);
			BerTlvs tlvsDec = parser.parse(bytesDec, 0, bytesDec.length);
			
			HashMap<String, BerTlv> encTagMap = new HashMap<String, BerTlv>();
			for (BerTlv tlv : tlvsDec.getList()) {
				String tagN = HexUtil.toHexString(tlv.getTag().bytes);
				encTagMap.put(tagN, tlv);
			}
//			System.err.println("reqTlv Flag 3");
			//Obtener campos requeridos
			//Construir campo 55
			tag55 = 		
			((encTagMap.get("9F26") != null) ? ( "9F26" + String.format("%02d", encTagMap.get("9F26").getHexValue().length()/2) + encTagMap.get("9F26").getHexValue() ) : "00000000" ) +
			((encTagMap.get("9F27") != null) ? ( "9F27" + String.format("%02d", encTagMap.get("9F27").getHexValue().length()/2) + encTagMap.get("9F27").getHexValue() ) : "0" ) +
			((encTagMap.get("9F10") != null) ? ( "9F10" + String.format("%02d", Integer.parseInt(Integer.toHexString(encTagMap.get("9F10").getHexValue().length()/2))) + encTagMap.get("9F10").getHexValue() ) : "" ) +
			((encTagMap.get("9F37") != null) ? ( "9F37" + String.format("%02d", encTagMap.get("9F37").getHexValue().length()/2) + encTagMap.get("9F37").getHexValue() ) : "0000" ) +
			((encTagMap.get("9F36") != null) ? ( "9F36" + String.format("%02d", encTagMap.get("9F36").getHexValue().length()/2) + encTagMap.get("9F36").getHexValue() ) : "00" ) +
			((encTagMap.get("95") != null) ? ( "95" + String.format("%02d", encTagMap.get("95").getHexValue().length()/2) + encTagMap.get("95").getHexValue() ) : "00000" ) +
			((encTagMap.get("9A") != null) ? ( "9A" + String.format("%02d", encTagMap.get("9A").getHexValue().length()/2) + encTagMap.get("9A").getHexValue() ) : "000" ) +
			((encTagMap.get("9C") != null) ? ( "9C" + String.format("%02d", encTagMap.get("9C").getHexValue().length()/2) + encTagMap.get("9C").getHexValue() ) : "0" ) +
			((encTagMap.get("9F02") != null) ? ( "9F02" + String.format("%02d", encTagMap.get("9F02").getHexValue().length()/2) + encTagMap.get("9F02").getHexValue() ) : "000000" ) +
			((encTagMap.get("5F2A") != null) ? ( "5F2A" + String.format("%02d", encTagMap.get("5F2A").getHexValue().length()/2) + encTagMap.get("5F2A").getHexValue() ) : "00" ) +
			((encTagMap.get("82") != null) ? ( "82" + String.format("%02d", encTagMap.get("82").getHexValue().length()/2) + encTagMap.get("82").getHexValue() ) : "00" ) +
			((encTagMap.get("9F1A") != null) ? ( "9F1A" + String.format("%02d", encTagMap.get("9F1A").getHexValue().length()/2) + encTagMap.get("9F1A").getHexValue() ) : "00" ) +
			((encTagMap.get("9F34") != null) ? ( "9F34" + String.format("%02d", encTagMap.get("9F34").getHexValue().length()/2) + encTagMap.get("9F34").getHexValue() ) : "000" ) +
			((encTagMap.get("9F33") != null) ? ( "9F33" + String.format("%02d", encTagMap.get("9F33").getHexValue().length()/2) + encTagMap.get("9F33").getHexValue() ) : "000" ) +
			((encTagMap.get("9F03") != null) ? ( "9F03" + String.format("%02d", encTagMap.get("9F03").getHexValue().length()/2) + encTagMap.get("9F03").getHexValue() ) : "000000" ) +
			((encTagMap.get("9F5B") != null) ? ( "9F5B" + String.format("%02d", encTagMap.get("9F5B").getHexValue().length()/2) + encTagMap.get("9F5B").getHexValue() ) : "" ) +
			((encTagMap.get("9F1E") != null) ? ( "9F1E" + String.format("%02d", encTagMap.get("9F1E").getHexValue().length()/2) + encTagMap.get("9F1E").getHexValue() ) : "00000000" ) +
			((encTagMap.get("9F21") != null) ? ( "9F21" + String.format("%02d", encTagMap.get("9F21").getHexValue().length()/2) + encTagMap.get("9F21").getHexValue() ) : "000" ) +
			((encTagMap.get("9F35") != null) ? ( "9F35" + String.format("%02d", encTagMap.get("9F35").getHexValue().length()/2) + encTagMap.get("9F35").getHexValue() ) : "0" )	
			;
			//System.err.println("tag55: " + tag55);
			return tag55;
		} catch (Exception e) {
			System.err.println("Excepcion en parseEncTlv0200");
			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object parseEncTlv0210(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("parseEncTlv0210");
		String tag55 = (String)param;
		//tag55 = ((byte[])param).toString();
		try {
			//System.err.println("tag55 recibido: " + tag55);	
			//tag55 = tag55.substring(4);
			return tag55;
		} catch (Exception e) {
			System.err.println("Excepcion en parseEncTlv0210");
			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object parseEncTlv0200A(Terminal terminal, Object param, Map<?, ?> params) {
//		System.err.println("parseEncTlv0200");
		String tag55 = "";
		try {
			//Obtener BDK
			String reqTlv = (String)param;
//			System.err.println("reqTlv " + reqTlv);

			byte[] bytesDec; 
			
			BerTlvParser parser = new BerTlvParser();
			
			bytesDec = HexUtil.parseHex(reqTlv);
			BerTlvs tlvsDec = parser.parse(bytesDec, 0, bytesDec.length);
			
			HashMap<String, BerTlv> encTagMap = new HashMap<String, BerTlv>();
			for (BerTlv tlv : tlvsDec.getList()) {
				String tagN = HexUtil.toHexString(tlv.getTag().bytes);
				encTagMap.put(tagN, tlv);
			}
//			System.err.println("reqTlv Flag 3");
			//Obtener campos requeridos
			//Construir campo 55
			tag55 = 				
			((encTagMap.get("9F10") != null) ? ( "9F10" + String.format("%02d", Integer.parseInt(Integer.toHexString(encTagMap.get("9F10").getHexValue().length()/2))) + encTagMap.get("9F10").getHexValue() ) : "" ) +			
			((encTagMap.get("95") != null) ? ( "95" + String.format("%02d", encTagMap.get("95").getHexValue().length()/2) + encTagMap.get("95").getHexValue() ) : "00000" ) +	
			((encTagMap.get("9F5B") != null) ? ( "9F5B" + String.format("%02d", encTagMap.get("9F5B").getHexValue().length()/2) + encTagMap.get("9F5B").getHexValue() ) : "" )		
			;
			//System.err.println("tag55: " + tag55);
			return tag55;
		} catch (Exception e) {
			System.err.println("Excepcion en parseEncTlv0200A");
			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object parseEncTlv0210A(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("parseEncTlv0210A");
		String tag55 = (String)param;
		//tag55 = ((byte[])param).toString();
		try {
			//System.err.println("tag55 recibido: " + tag55);	
			//tag55 = tag55.substring(4);
			return tag55;
		} catch (Exception e) {
			System.err.println("Excepcion en parseEncTlv0210A");
			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object parseEncTlv0220(Terminal terminal, Object param, Map<?, ?> params) {
//		System.err.println("parseEncTlv0200");
		String tag55 = "";
		try {
			//Obtener BDK
			String reqTlv = (String)param;
//			System.err.println("reqTlv " + reqTlv);

			byte[] bytesDec; 
			
			BerTlvParser parser = new BerTlvParser();
			
			bytesDec = HexUtil.parseHex(reqTlv);
			BerTlvs tlvsDec = parser.parse(bytesDec, 0, bytesDec.length);
			
			HashMap<String, BerTlv> encTagMap = new HashMap<String, BerTlv>();
			for (BerTlv tlv : tlvsDec.getList()) {
				String tagN = HexUtil.toHexString(tlv.getTag().bytes);
				encTagMap.put(tagN, tlv);
			}
//			System.err.println("reqTlv Flag 3");
			//Obtener campos requeridos
			//Construir campo 55
			tag55 = 
			((encTagMap.get("9F26") != null) ? ( "9F26" + String.format("%02d", encTagMap.get("9F26").getHexValue().length()/2) + encTagMap.get("9F26").getHexValue() ) : "00000000" ) +
			((encTagMap.get("9F27") != null) ? ( "9F27" + String.format("%02d", encTagMap.get("9F27").getHexValue().length()/2) + encTagMap.get("9F27").getHexValue() ) : "0" ) +
			((encTagMap.get("9F10") != null) ? ( "9F10" + String.format("%02d", Integer.parseInt(Integer.toHexString(encTagMap.get("9F10").getHexValue().length()/2))) + encTagMap.get("9F10").getHexValue() ) : "" ) +
			((encTagMap.get("9F37") != null) ? ( "9F37" + String.format("%02d", encTagMap.get("9F37").getHexValue().length()/2) + encTagMap.get("9F37").getHexValue() ) : "0000" ) +
			((encTagMap.get("9F36") != null) ? ( "9F36" + String.format("%02d", encTagMap.get("9F36").getHexValue().length()/2) + encTagMap.get("9F36").getHexValue() ) : "00" ) +
			((encTagMap.get("95") != null) ? ( "95" + String.format("%02d", encTagMap.get("95").getHexValue().length()/2) + encTagMap.get("95").getHexValue() ) : "00000" ) +
			((encTagMap.get("9A") != null) ? ( "9A" + String.format("%02d", encTagMap.get("9A").getHexValue().length()/2) + encTagMap.get("9A").getHexValue() ) : "000" ) +
			((encTagMap.get("9C") != null) ? ( "9C" + String.format("%02d", encTagMap.get("9C").getHexValue().length()/2) + encTagMap.get("9C").getHexValue() ) : "0" ) +
			((encTagMap.get("9F02") != null) ? ( "9F02" + String.format("%02d", encTagMap.get("9F02").getHexValue().length()/2) + encTagMap.get("9F02").getHexValue() ) : "000000" ) +
			((encTagMap.get("5F2A") != null) ? ( "5F2A" + String.format("%02d", encTagMap.get("5F2A").getHexValue().length()/2) + encTagMap.get("5F2A").getHexValue() ) : "00" ) +
			((encTagMap.get("82") != null) ? ( "82" + String.format("%02d", encTagMap.get("82").getHexValue().length()/2) + encTagMap.get("82").getHexValue() ) : "00" ) +
			((encTagMap.get("9F1A") != null) ? ( "9F1A" + String.format("%02d", encTagMap.get("9F1A").getHexValue().length()/2) + encTagMap.get("9F1A").getHexValue() ) : "00" ) +
			((encTagMap.get("9F34") != null) ? ( "9F34" + String.format("%02d", encTagMap.get("9F34").getHexValue().length()/2) + encTagMap.get("9F34").getHexValue() ) : "000" ) +
			((encTagMap.get("9F33") != null) ? ( "9F33" + String.format("%02d", encTagMap.get("9F33").getHexValue().length()/2) + encTagMap.get("9F33").getHexValue() ) : "000" ) +
			((encTagMap.get("9F03") != null) ? ( "9F03" + String.format("%02d", encTagMap.get("9F03").getHexValue().length()/2) + encTagMap.get("9F03").getHexValue() ) : "000000" ) +
			((encTagMap.get("9F5B") != null) ? ( "9F5B" + String.format("%02d", encTagMap.get("9F5B").getHexValue().length()/2) + encTagMap.get("9F5B").getHexValue() ) : "" ) +
			((encTagMap.get("9F1E") != null) ? ( "9F1E" + String.format("%02d", encTagMap.get("9F1E").getHexValue().length()/2) + encTagMap.get("9F1E").getHexValue() ) : "00000000" ) +
			((encTagMap.get("9F21") != null) ? ( "9F21" + String.format("%02d", encTagMap.get("9F21").getHexValue().length()/2) + encTagMap.get("9F21").getHexValue() ) : "000" ) +
			((encTagMap.get("9F35") != null) ? ( "9F35" + String.format("%02d", encTagMap.get("9F35").getHexValue().length()/2) + encTagMap.get("9F35").getHexValue() ) : "0" )	
			;
		
			//System.err.println("tag55: " + tag55);
			return tag55;
		} catch (Exception e) {
			System.err.println("Excepcion en parseEncTlv0220");
			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object ldStoreFormat(Terminal terminal, Object param, Map<?, ?> params) {
		System.err.println("Local time: " + param.toString() + " Clase: " + param.getClass().getName());
		String[] localDate = ((String)param).split(" ");
		System.err.println("1+2: " + localDate[1] + localDate[2]);
		String localMonth = localDate[1];
		switch(localMonth) {
			case "Jan":
				localDate[1] = "01";
				break;
			case "Feb":
				localDate[1] = "02";
				break;
			case "Mar":
				localDate[1] = "03";
				break;
			case "Apr":
				localDate[1] = "04";
				break;
			case "May":
				localDate[1] = "05";
				break;
			case "Jun":
				localDate[1] = "06";
				break;
			case "Jul":
				localDate[1] = "07";
				break;
			case "Aug":
				localDate[1] = "08";
				break;
			case "Sep":
				localDate[1] = "09";
				break;
			case "Oct":
				localDate[1] = "10";
				break;
			case "Nov":
				localDate[1] = "11";
				break;
			case "Dec":
				localDate[1] = "12";
				break;
		}
		return localDate[1] + localDate[2];
	}
	
	public static Object ltStoreFormat(Terminal terminal, Object param, Map<?, ?> params) {
		try {
			//System.err.println("Local date: " + param.toString() + " Clase: " + param.getClass().getName());
			String[] dateStr = ((String)param).split(" ");
			//for(String d : dateStr) System.err.println("d: " + d);
			//System.err.println("dateStr[3]: " + dateStr[3]);
			String[] localTime = dateStr[3].split(":");
			//System.err.println("0+1+2: " + localTime[0] + localTime[1] + localTime[2]);
			return localTime[0] + localTime[1] + localTime[2];
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static Object parseEncTlv0200Back(Terminal terminal, Object param, Map<?, ?> params) {
//		System.err.println("parseEncTlv0200");
		String tag55 = "";
		try {
			//Obtener BDK
			String reqTlv = (String)param;
			System.err.println("reqTlv " + reqTlv);
			String bdk = "0123456789ABCDEFFEDCBA9876543210";
			//Descifrar tlv
			byte[] bytes = HexUtil.parseHex(reqTlv);
			byte[] bytesDec; 
			
			BerTlvParser parser = new BerTlvParser();
			BerTlvs tlvs = parser.parse(bytes, 0, bytes.length);

			HashMap<String, BerTlv> decTagMap = new HashMap<String, BerTlv>();
			for (BerTlv tlv : tlvs.getList()) {
				String tagN = HexUtil.toHexString(tlv.getTag().bytes);
				decTagMap.put(tagN, tlv);
			}
//			System.err.println("reqTlv Flag 1");
			String key = DUKPTServer.GetDataKey(decTagMap.get("C0").getHexValue(), bdk);
			String tmp1 = TripleDES.decrypt_CBC(decTagMap.get("C2").getHexValue(), key);
			String tmp2 = Utils.removePKCS7(tmp1);
//			System.err.println("reqTlv Flag 2");		
			bytesDec = HexUtil.parseHex(tmp2);
			BerTlvs tlvsDec = parser.parse(bytesDec, 0, bytesDec.length);
			
			HashMap<String, BerTlv> encTagMap = new HashMap<String, BerTlv>();
			for (BerTlv tlv : tlvsDec.getList()) {
				String tagN = HexUtil.toHexString(tlv.getTag().bytes);
				encTagMap.put(tagN, tlv);
			}
			System.err.println("reqTlv Flag 3");
			//Obtener campos requeridos
			//Construir campo 55
			tag55 = 
			"9F26" + String.format("%02d", encTagMap.get("9F26").getHexValue().length()) + encTagMap.get("9F26").getHexValue() +
			"9F27" + String.format("%02d", encTagMap.get("9F27").getHexValue().length()) + encTagMap.get("9F27").getHexValue() +
			"9F10" + String.format("%02d", encTagMap.get("9F10").getHexValue().length()) + encTagMap.get("9F10").getHexValue() +
			"9F37" + String.format("%02d", encTagMap.get("9F37").getHexValue().length()) + encTagMap.get("9F37").getHexValue() +
			"9F36" + String.format("%02d", encTagMap.get("9F36").getHexValue().length()) + encTagMap.get("9F36").getHexValue() +
			"95" + String.format("%02d", encTagMap.get("95").getHexValue().length()) + encTagMap.get("95").getHexValue() +
			"9A" + String.format("%02d", encTagMap.get("9A").getHexValue().length()) + encTagMap.get("9A").getHexValue() +
			"9C" + String.format("%02d", encTagMap.get("9C").getHexValue().length()) + encTagMap.get("9C").getHexValue() +
			"9F02" + String.format("%02d", encTagMap.get("9F02").getHexValue().length()) + encTagMap.get("9F02").getHexValue() +
			"5F2A" + String.format("%02d", encTagMap.get("5F2A").getHexValue().length()) + encTagMap.get("5F2A").getHexValue() +
			"82" + String.format("%02d", encTagMap.get("82").getHexValue().length()) + encTagMap.get("82").getHexValue() +
			"9F1A" + String.format("%02d", encTagMap.get("9F1A").getHexValue().length()) + encTagMap.get("9F1A").getHexValue() +
			"9F34" + String.format("%02d", encTagMap.get("9F34").getHexValue().length()) + encTagMap.get("9F34").getHexValue() +
			"9F33" + String.format("%02d", encTagMap.get("9F33").getHexValue().length()) + encTagMap.get("9F33").getHexValue() +
			"9F03" + String.format("%02d", encTagMap.get("9F03").getHexValue().length()) + encTagMap.get("9F03").getHexValue() +
			"9F1E" + String.format("%02d", encTagMap.get("9F1E").getHexValue().length()) + encTagMap.get("9F1E").getHexValue() +
			"9F21" + String.format("%02d", encTagMap.get("9F21").getHexValue().length()) + encTagMap.get("9F21").getHexValue() +
			"9F35" + String.format("%02d", encTagMap.get("9F35").getHexValue().length()) + encTagMap.get("9F35").getHexValue();
//			System.err.println("tag55: " + tag55);
			return tag55;
		} catch (Exception e) {
//			System.err.println("Excepcion en parseEncTlv0200");
//			e.printStackTrace();
		}
		return tag55;
	}
	
	public static Object determineTerminal(Terminal terminal, Object param, Map<?, ?> params) {
		//Este metodo utiliza esta nomenclatura para los terminales 0000bxxx
		//Donde b es el digito que depende del tipo de transaccion y xxx es el consecutivo
		//asignado al comercio
		System.out.println("determineTerminal");
		String term = ((String)param).replace("b", params.get("card_type").toString());
//		System.out.println(
//				"Terminal recibido: " + (String)param 
//				+ "\n - Tipo de tarjeta: " + params.get("card_type") 
//				+ "\n - Terminal a retornar " + terminal);
		return encodeBcd(terminal, term, params);
	}
	
	public static Object determineAffiliation(Terminal terminal, Object param, Map<?, ?> params) {
		
		return param;
	}
	
	public static Object formatTrack2(Terminal terminal, Object param, Map<?, ?> params) {
		String  finalTrack2 = "",
				track2LenStr = "",
				track2Str = (String)param;
		Integer panLen = track2Str.length();
		//track2LenStr = String.valueOf(panLen - (track2Str.contains("F") ? 1 : 0)) ;
		track2LenStr = String.valueOf(panLen - 
				(track2Str.contains("F") ? 1 : 0) - 
//				(track2Str.contains("D") ? 1 : 0) - 
				(track2Str.contains("B") ? 1 : 0)
				) ;
//		track2Str.replace("F", "");
		finalTrack2 =  String.format("%1$" + 2 + "s", track2LenStr).replace(" ", "0") 
				+ track2Str;
		//System.out.println("finalTrack2: " + finalTrack2);
		return finalTrack2;
	}
	
	public static Object formatTrack2(Terminal terminal, Object param) {
		String  finalTrack2 = "",
				track2LenStr = "",
				track2Str = (String)param;
		Integer panLen = track2Str.length();
		track2LenStr = String.valueOf(panLen - (track2Str.contains("F") ? 1 : 0)) ;
		finalTrack2 =  String.format("%1$" + 2 + "s", track2LenStr).replace(" ", "0") 
				+ track2Str;
		//System.out.println("finalTrack2: " + finalTrack2);
		return finalTrack2;
	}
	
	public static Object loadPanFromTrack2(Terminal terminal, Object param, Map<?, ?> params) {
		String track2 = (String)param;
		return formatPan(terminal, track2.substring(2, track2.indexOf('D')));
	}
	
	public static Object encodeStoredRef(Terminal terminal, Object param, Map<?, ?> params) {
		return "0006" + encodeBcd(terminal, param, params);
	}
	
	public static Object encodeBatch(Terminal terminal, Object param, Map<?, ?> params) {
		return "00083337" + encodeBcd(terminal, String.format("%06d", Integer.valueOf((String)param)), params);
	}
	
	public static Object loadBatchNumber(Terminal terminal, Map<?, ?> params) {		
		Map<String, String> paramL = (Map<String, String>)params;
		String cardType = paramL.get("card_type");
		Integer batchNum = IsoService.getCurrentBatchNumber(terminal, cardType);
		String batchNumS = String.format("%06d", batchNum);
		return "0006" + encodeBcd(terminal, batchNumS, params);
	}
	
	public static Object calculateBatchData(Terminal terminal, Map<?, ?> params) {
		Session session = null;
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (ExceptionInInitializerError err) {
			//logger.error(err.toString());
		}
		
    	String batchData = "";
		try {
	    	System.out.println("calculateBatchData");
			Integer purchTotalTrx = 0,  annulTotalTrx = 0;
			BigDecimal purchTotalAmount = new BigDecimal(0), annulTotalAmount = new BigDecimal(0);

			//Obtenemos el tipo lote actual a calcular
			CurrentBatch currentBatchT = null;
			Map<String, String> paramL = (Map<String, String>)params;
			String cardType = paramL.get("card_type");
			String currentBQuery =
					"select cb from CurrentBatch as cb " 
					+ "where cb.terminal.id = :terminalId "
					+ "and cb.batchType.paramValue = :cardType ";
			ArrayList<CurrentBatch> cbList = (ArrayList<CurrentBatch>)session.createQuery(currentBQuery)
					.setInteger("terminalId", terminal.getId())
					.setString("cardType", paramL.get("card_type"))
					.list();
			currentBatchT = cbList.get(0);
			System.err.println("cb: " + currentBatchT);
			
			//Obtenemos las transacciones del lote actual de este tipo para el terminal
			String trxListQuery = "select count(t), sum(t.amount) from Transaction as t "
					+ "where t.currentBatch.id = :currentBatchId "
					+ "and t.transactionResponse.id = :transactionResp "
					+ "and (t.transactionStatus = :tStatus0 or t.transactionStatus = :tStatus7 ) "
					+ "and t.transactionType.id = :transactionType";
			List trxList = session.createQuery(trxListQuery)
							.setInteger("currentBatchId", currentBatchT.getId())
							.setInteger("transactionResp", 38)
							.setInteger("tStatus0", 0)
							.setInteger("tStatus7", 7)
							.setInteger("transactionType", 11)
							.list();
			Integer trxTotal = 0;
		    BigDecimal trxAmount = new BigDecimal(0);
			trxTotal = ((Long)((Object[])trxList.get(0))[0]).intValue();
			trxAmount = ((Object[])trxList.get(0))[1] == null ? new BigDecimal(0) : BigDecimal.valueOf(((Double)((Object[])trxList.get(0))[1]).doubleValue());
		    System.out.println("trxTotal: " + trxTotal + " - trxAmount: " + trxAmount);
			
			purchTotalTrx = trxTotal;
			purchTotalAmount = trxAmount;

			batchData = "";
			purchTotalAmount=purchTotalAmount.movePointRight(2);
			annulTotalAmount=annulTotalAmount.movePointRight(2);
//			System.out.println("purchTotalAmount-> " + purchTotalAmount);
//			System.out.println("annulTotalAmount-> " + annulTotalAmount);
			if(currentBatchT != null) {
				purchTotalTrx -= annulTotalTrx;
				purchTotalAmount=purchTotalAmount.subtract(annulTotalAmount);
				switch(cardType) {
					case "1":
						batchData="0030"
						+ encodeBcd(terminal, String.format("%03d", purchTotalTrx), params)
						+ encodeBcd(terminal, String.format("%012d", purchTotalAmount.longValue()), params)
						+ "303030303030303030303030303030"
						;
						System.out.println("batchData card_type 1 : "+batchData);
						break;
					case "2":
						batchData="0045"
						+ "303030303030303030303030303030303030303030303030303030303030"
						+ encodeBcd(terminal, String.format("%03d", purchTotalTrx), params)
						+ encodeBcd(terminal, String.format("%012d", purchTotalAmount.longValue()), params)
						//+ "303030303030303030303030303030"
						;
						System.out.println("batchData card_type 2 : "+batchData);
						break;
					case "3":
						batchData = "0060303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030";
						break;
					default:
						batchData = "0060303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030303030";
						break;
				}
			}
		} catch (Exception e) {
			System.err.println("Excepcion en calculateBatchData");
			e.printStackTrace();
		} finally {
			if(session != null) session.close();
		}
		return batchData; 
	}
	
	public static Object calcField60Len(Terminal terminal, Object param, Map<?, ?> params) {
		String field60="", field60Len="";
		try {
			System.out.println("calcField60Len");
			field60 = (String)param;
			field60Len = String.format("%1$" + 4 + "s", Integer.valueOf((field60.length()/2))).replace(" ", "0");		
				   ;
			System.out.println("field60Len+field60: " + field60Len+field60 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return field60Len+field60; 
	}
	
	public static Object loadOrigTrace(Terminal terminal, Object param, Map<?, ?> params) {
		String field60="", field60Len="";
		try {
			System.out.println("loadOrigTrace");
			field60 = (String)param;
			field60Len = String.format("%1$" + 4 + "s", Integer.valueOf((field60.length()/2))).replace(" ", "0");		
				   ;
			System.out.println("field60Len+field60: " + field60Len+field60 );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return field60Len+field60; 
	}
	
	public static Object storeBatch(Terminal terminal, Object param, Map<?, ?> params) {
		String batch = (String)param;
		System.out.println("batch " + batch);
		return batch.length() > 24 ? "0010".concat(batch.substring(52)) : batch;
	}
	
	public static Object loadAcqCode(Terminal terminal, Object param, Map<?, ?> params) {
		Bank acqBank = (Bank) param;
		//System.out.println("acquirer code bank: " + acqBank.getName() + " " + acqBank.getAcquirerCode() );
		return acqBank.getAcquirerCode();
	}
	
	//Funciones de preprocesamiento ------------------------------------------------------------
	public static byte[] preProcAdvc(byte[] msgToProc) {
		System.out.println("preProcAdvc");
		String msg = new String(msgToProc),
			   msg1 = msg.substring(30)
				;
		System.out.println(msg);
		//Validar en el bitmap si se encuentra el campo 2
		if(msg.charAt(14) == '6') {
			System.out.println("Campo 2 presente");
			Integer de2Len = Integer.parseInt(msg1.substring(0, 2));
			if(de2Len%2 != 0) msg = msg.substring(0, 32) + msg.substring(33);
		}
		//System.out.println(msg);
		return msg.getBytes();
	}

	//Funciones de validacion de inversa: se debe retornar true si la transaccion es valida
	public static boolean valInverseInvoke(Transaction transaction) {
		return !transaction.getTransactionResponse().getCode().equals("08");
	}
	
	//Funciones de desbloqueo de terminales
	public static boolean unblockAdvice(Terminal terminal) {
		boolean unblock = false;
		Transaction transaction = terminal.getLastConfTrx();
		System.out.println("unblockAdvice: id de transaccion: " + transaction.getId());
		TransactionParameter trxParam = null;
		Iterator paramIter = transaction.getParameters().iterator();
		while(paramIter.hasNext() && !unblock) {
			trxParam = (TransactionParameter) paramIter.next();
			if(trxParam.getParamName().equals("pos_entry"))
				unblock = (trxParam.getParamValue().equals("0901")
						|| trxParam.getParamValue().equals("0801")) ? true : false;
		}
		if(transaction.getTransactionStatus().equals(TransactionStatus.REVERSED)) {
			System.out.println("unblockAdvice: la transaccion " + transaction.getId() + "fue reversada");
			unblock = true;
		}
		return unblock;
	}
}
