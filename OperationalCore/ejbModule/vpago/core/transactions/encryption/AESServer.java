package vpago.core.transactions.encryption;

//import android.util.Log;
//
//import com.bbpos.www.cayan.log.CayanLog;
//import com.bbpos.www.cayan.log.CayanLogEngine;
//
//import org.spongycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Created by Tim.chan on 3/11/2015.
 */
public class AESServer {
    public static byte[] aes256cbcEncrypt(byte[] data, byte[] key, byte[] iv)
    {
        SecretKey sk = new SecretKeySpec(key, "AES");
        IvParameterSpec aesIv = new IvParameterSpec(iv);
        try {

            //Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", new BouncyCastleProvider());
            cipher.init(Cipher.ENCRYPT_MODE, sk, aesIv);
            return cipher.doFinal(data);
        } catch (Exception e) {
            //CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.ENCRYPTION).setMsg("aes256cbcEncrypt ex:" + e.toString()));
        }

        return null;
    }

    public static byte[] aes256cbcDecrypt(byte[] data, byte[] key, byte[] iv)
    {
        SecretKey sk = new SecretKeySpec(key, "AES");
        IvParameterSpec aesIv = new IvParameterSpec(iv);
        try {

            //Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", new BouncyCastleProvider());
            cipher.init(Cipher.DECRYPT_MODE, sk, aesIv);
            return cipher.doFinal(data);
        } catch (Exception e) {
            //CayanLogEngine.log(new CayanLog(CayanLog.Priority.Error, CayanLog.MessageID.ENCRYPTION).setMsg("aes256cbcDecrypt ex:"+e.toString()));
        }

        return null;
    }
}
