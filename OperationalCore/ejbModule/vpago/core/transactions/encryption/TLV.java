package vpago.core.transactions.encryption;

import java.util.Hashtable;

public class TLV {
	
	public String tag;
	public String length;
	public String value;
	
	public boolean isNested;
	public Hashtable<String, TLV> tlvList;
	
}
