package vpago.core.transactions.encryption;

//import com.bbpos.www.cayan.Utils;

import java.util.Hashtable;

import vpago.core.transactions.util.Utils;

public class TLVParser {
	
	public static Hashtable<String, TLV> parse(String tlv) {
		try {
			return getTLVList(Utils.hexToByteArray(tlv));
		} catch(Exception e) {
			return null;
		}
	}
	
	private static Hashtable<String, TLV> getTLVList(byte[] data) {
		int index = 0;
		
		Hashtable<String, TLV> tlvList = new Hashtable<String, TLV>();
		
		while(index < data.length) {
		
			byte[] tag;
			byte[] length;
			byte[] value;
			
			boolean isNested;
			if((data[index] & (byte)0x20) == (byte)(0x20)) {
				isNested = true;
			} else {
				isNested = false;
			}
			
			if((data[index] & (byte)0x1F) == (byte)(0x1F)) {
				int lastByte = index + 1;
				while((data[lastByte] & (byte)0x80) == (byte)0x80) {
					++lastByte;
				}
				tag = new byte[lastByte - index + 1];
				System.arraycopy(data, index, tag, 0, tag.length);
				index += tag.length;
			} else {
				tag = new byte[1];
				tag[0] = data[index];
				++index;
				
				if(tag[0] == 0x00) {
					break;
				}
			}
			
			if((data[index] & (byte)0x80) == (byte)(0x80)) {
				int n = (data[index] & (byte)0x7F) + 1;
				length = new byte[n];
				System.arraycopy(data, index, length, 0, length.length);
				index += length.length;
			} else {
				length = new byte[1];
				length[0] = data[index];
				++index;
			}
			
			int n = getLengthInt(length);
			value = new byte[n];
			System.arraycopy(data, index, value, 0, value.length);
			index += value.length;
			
			TLV tlv = new TLV();
			tlv.tag = Utils.toHexString(tag);
			tlv.length = Utils.toHexString(length);
			tlv.value = Utils.toHexString(value);
			tlv.isNested = isNested;
			
			if(isNested) {
				tlv.tlvList = getTLVList(value);
			}
			
			tlvList.put(tlv.tag, tlv);
		}
		return tlvList;
	}
	
	private static int getLengthInt(byte[] data) {
		if((data[0] & (byte)0x80) == (byte)(0x80)) {
			int n = data[0] & (byte)0x7F;
			int length = 0;
			for(int i = 1; i < n + 1; ++i) {
				length <<= 8; 
				length |= (data[i] & 0xFF);
			}
			return length;
		} else {
			return data[0] & 0xFF;
		}
	}
}
