package vpago.core.transactions;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionData;
import vpago.core.transactions.classes.TransactionParameter;
import vpago.core.transactions.classes.TransactionResponse;
import vpago.core.transactions.classes.TransactionResponseParameter;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.classes.types.BatchOperation;
import vpago.core.transactions.classes.types.SendFuncType;
import vpago.core.transactions.classes.types.TransactionStatus;
import vpago.core.transactions.connections.AsyncReqQueueLocal;
import vpago.core.transactions.connections.NonClosingAsyncTCPLocal;
import vpago.core.transactions.connections.ReadExecutorServiceLocal;
import vpago.core.config.EngineProperties;
import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.AdditionalVoucherFields;
import vpago.core.transactions.classes.AssortedParameters;
import vpago.core.transactions.classes.BatchHistory;
import vpago.core.transactions.classes.BatchType;
import vpago.core.transactions.classes.CoreResponse;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.FunctionData;
import vpago.core.transactions.classes.FunctionResponse;
import vpago.core.transactions.classes.FunctionStatusMessage;
import vpago.core.transactions.classes.IsoTemplate;
import vpago.core.transactions.exceptions.AlreadyInvertedTransaction;
import vpago.core.transactions.exceptions.CleanBatch;
import vpago.core.transactions.exceptions.ConnectionFailure;
import vpago.core.transactions.exceptions.DisabledMerchant;
import vpago.core.transactions.exceptions.DisabledTerminal;
import vpago.core.transactions.exceptions.DuplicateTransaction;
import vpago.core.transactions.exceptions.InvalidBatch;
import vpago.core.transactions.exceptions.InvalidSerialNumber;
import vpago.core.transactions.exceptions.InvalidTransaction;
import vpago.core.transactions.exceptions.InvalidTransactionType;
import vpago.core.transactions.exceptions.InvertTransaction;
import vpago.core.transactions.exceptions.NoResponse;
import vpago.core.transactions.exceptions.NonExistentPaymentChannel;
import vpago.core.transactions.exceptions.NonExistentTerminal;
import vpago.core.transactions.exceptions.PendingConfirmation;
import vpago.core.transactions.exceptions.PendingInverse;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.exceptions.TransactionException;
import vpago.core.transactions.queues.ConfirmationQueueElem;
import vpago.core.transactions.queues.ConfirmationQueueLocal;
import vpago.core.transactions.queues.InverseQueueElem;
import vpago.core.transactions.queues.InverseQueueLocal;
import vpago.core.transactions.util.Utils;

/**
 * Bean de sesion para el manejo de las transacciones
 */
@Stateless
@LocalBean
public class TransactionHandler implements TransactionHandlerRemote, TransactionHandlerLocal {
	private static final Logger logger = LoggerFactory.getLogger(TransactionHandler.class);
	
    /**
     * Constructor por defecto.
     */
    public TransactionHandler() {
    	super();
    }
    
    @EJB(beanName="ConfirmationQueue")
    private ConfirmationQueueLocal confQueue;
    @EJB(beanName="InverseQueue")
    private InverseQueueLocal invQueue;
// TODO Declarar en base a configuracion en archivo (?)
//    @EJB(beanName="NonClosingTCP")
//	private NonClosingTCPLocal nonCloseTCP;
    @EJB(beanName="NonClosingAsyncTCP")
	private NonClosingAsyncTCPLocal nonClosingAsyncTCP;
	@EJB(beanName="AsyncReqQueue")
	private AsyncReqQueueLocal aReqQueue;
	@EJB(beanName="ReadExecutorService")
	private ReadExecutorServiceLocal asyncExecSer;
    
    /**
     * Solicitar el envio de una transaccion
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @throws TransactionException Excepcion en transaccion
     */
    @SuppressWarnings("unchecked")
	public CoreResponse executeTransaction(TransactionData requestData, Integer prevTranId) throws TransactionException {
    	logger.trace("{}", requestData);
    	Session session = null;
    	AssortedParameters reqParams = null, resParams = null;
    	CoreResponse coreResponse = null;
    	PaymentChannel paymentChannel = null; 
    	TransactionType transactionType = null;
    	Terminal terminal = null;
    	AdditionalVoucherFields addVoucherFields = null;
    	Transaction settleTrailerTrx = null;
    	//Transaction transactionToDel = null;
    	try {
			try {
				session = HibernateUtil.getSessionFactory().openSession();
			} catch (ExceptionInInitializerError err) {
				logger.error(err.toString());
				throw new ServiceUnavailable();
			}
			
			//Se obtiene el canal de pago
			try {
				paymentChannel = (PaymentChannel) session.createQuery(
						"from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
						.setString("codeName", requestData.getChannelCode())
						.uniqueResult();
				logger.trace("{}", paymentChannel.getCodeName());
			} catch (Exception e) {
				logger.error(e.toString());
				if(paymentChannel == null) throw new NonExistentPaymentChannel();
			}
			
			//Se obtiene el tipo de transaccion
			try {
				transactionType = (TransactionType) session.createQuery(
					    "from TransactionType as transactionType where transactionType.name = :name and transactionType.templateMessage = :templateMessage")
					    .setString("name", requestData.getTransactionType())
					    .setEntity("templateMessage", paymentChannel.getTemplateMessage().get(0))
					    .uniqueResult();
				logger.trace("{}", transactionType.getDescription());
			} catch (Exception e) {
				logger.error(e.toString());
				if(transactionType == null) throw new InvalidTransactionType();
			}
			
			//Se obtiene el terminal del que procede la transaccion
			try {
				terminal = (Terminal) session.createQuery(
						"from Terminal as terminal where terminal.terminalGuid = :terminalGuid").
						setString("terminalGuid", requestData.getTerminalId())
					    .uniqueResult();
				logger.trace("Terminal affiliation {}", terminal.getAffiliation());
			}  catch (Exception e) {
				logger.error(e.toString());
				logger.debug("NonExistentTerminal");
				if(terminal == null) throw new NonExistentTerminal();
			}
			
			//Si el terminal esta bloqueado por confirmacion, validar si existe alguna funcion de desbloqueo del terminal
			boolean unblockTerm = false;
			try {
				if(terminal.isPendingConfirmation()) {
					ClassLoader classLoader = TransactionFactory.class.getClassLoader();
					IsoTemplate trxIsoTemplate = transactionType.getIsoTemplate();	
					logger.debug("Validando si existe una funcion para desbloquear el terminal por confirmacion...");
					logger.debug("{} {} {}", new Object[] {trxIsoTemplate.getSubpackageName(), trxIsoTemplate.getClassName(), trxIsoTemplate.getTermUnblockConf()});
					Class<?> extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(trxIsoTemplate.getSubpackageName().concat(".").concat(trxIsoTemplate.getClassName())));
					logger.debug(extClass != null ? "extClass != null" : "extClass == null");
					logger.debug(extClass.getMethod(trxIsoTemplate.getTermUnblockConf(), Terminal.class) != null ? "Method not null" : "Method null");
					logger.debug("method.getName()= {}", extClass.getMethod(trxIsoTemplate.getTermUnblockConf(), Terminal.class).getName());
					unblockTerm = (boolean) extClass.getMethod(trxIsoTemplate.getTermUnblockConf(), Terminal.class).invoke(null, terminal);
				}
			} catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException | ClassNotFoundException e) {
				logger.error(e.toString());
			} catch (Exception e) {
				logger.error(e.toString());
			}
			if(unblockTerm) {
				logger.info("Desbloqueando terminal {}", terminal.getTerminalGuid());
				terminal.setPendingConfirmation(false);
				session.beginTransaction();
				session.update(terminal);
				session.getTransaction().commit();
			} else logger.info("El terminal {} no puede ser desbloqueado", terminal.getTerminalGuid());

			//Cargar transaccion previa si es requerida 
			Transaction prevTransaction = null;
			if(prevTranId != null) {
				prevTransaction = (Transaction) session.get(Transaction.class, prevTranId);
			} else {
				//Cargar transaccion previa del terminal dependiendo de la naturaleza del tipo de transaccion
				if(transactionType.isInverseTrxType()) prevTransaction = terminal.getLastRevTrx() != null ? terminal.getLastRevTrx() : null;
				if(transactionType.isConfirmationTrxType()) prevTransaction = terminal.getLastConfTrx() != null ? terminal.getLastConfTrx() : null;
			}
			
			// Validaciones
			TransactionValidator transactionValidator = new TransactionValidator(requestData);
			if(!transactionValidator.merchantCanMakeTransaction(terminal.getAffiliation().getMerchant())) throw new DisabledMerchant();
			if(!transactionValidator.terminalCanMakeTransaction(terminal)) throw new DisabledTerminal();
			if(!transactionValidator.validSecToken(terminal, requestData.getTimestamp())) throw new InvalidTransaction();
			if(!transactionValidator.deviceValidation(terminal, paymentChannel, requestData.getSerial())) throw new InvalidSerialNumber();
			
			//Si la transaccion es de un tipo inversa y la transaccion que quiere 
			//invertir ya ha sido reversada descartar la solicitud
			if(transactionType.isInverseTrxType()) {
				//Obtenemos la ultima transaccion reversible
				Transaction lastRevTrx = terminal.getLastRevTrx();
				if (lastRevTrx != null) {
					//Si la transaccion ya fue reversada no enviar esta inversa
					if (lastRevTrx.getTransactionStatus().equals(TransactionStatus.REVERSED)) {
						logger.info("La transaccion {} ya ha sido reversada", lastRevTrx.getId());
						throw new AlreadyInvertedTransaction();
					} 
				}
			}
			
			//Almacenar el objeto transaccion
			Transaction transaction = new Transaction(terminal, 
										requestData.getControlToken(),
										transactionType,
										requestData.getSecToken()
										);
			logger.debug("Monto de la transaccion {}", requestData.getTransactionParameters().getOrDefault("amount", "0.0"));
			transaction.setAmount(Double.parseDouble(requestData.getTransactionParameters().getOrDefault("amount", "0.0")));
			
			logger.debug("{}", transaction);
			// Se obtienen los diferentes parametros de acuerdo a la plantilla del tipo de transaccion solicitada
			// Si el terminal tiene una confirmacion pendiente, se envia
			TransactionFactory transactionFactory = new TransactionFactory();
			IsoMessage reqIsoMsg = (IsoMessage)transactionFactory.buildTransaction(
					transactionType, 
					paymentChannel , 
					terminal, 
					requestData, 
					transaction,
					prevTransaction);
			
			// Se establecen los parametros a almacenar de la solicitud
			logger.trace("Estableciendo los parametros a almacenar de la solicitud");
			reqParams = transactionFactory.getTransactionParameters(reqIsoMsg, transaction.getTransactionType().getIsoTemplate(), transaction);
			logger.trace("Almacenando los parametros de la solicitud");
			reqParams.getStorableParams().forEach((key, param) -> {
				transaction.getParameters().add(new TransactionParameter(transaction, key, param));
			});
							
			logger.info("Determinar el lote de la transaccion");
			// Se determina el tipo de lote de la transaccion
			CurrentBatch trxCurrentBatch = null;
			BatchType trxBatchType = null;
			
			//Estableciendo datos de lote
			if(!transaction.getTransactionType().getBatchOperation().equals(BatchOperation.NONE)) {
				//Si es una transaccion con parametro de lote se determina el lote actual
				logger.debug("Determinando lote actual de transaccion con parametro de lote");
				trxBatchType = transactionFactory.determineTrxBatchT(transaction, requestData);
				logger.debug("Tipo de lote determinado para la transaccion {}", trxBatchType);
				for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
					//Encontramos el lote actual del terminal cuyo tipo coincide con el determinado
					//para la transaccion
					if(currentBatch.getBatchType().equals(trxBatchType)) {
						//Determinar lote actual
						trxCurrentBatch = currentBatch;
					}
				}
			} else {
				//Si no tiene parametro de lote, establecerle el lote de una transaccion asociada
				logger.debug("Determinando lote actual de transaccion sin parametro de lote");
				//Si es de tipo reverso, asociar al lote de la ultima transaccion reversible del terminanl
				if(transactionType.isInverseTrxType()) {
					logger.debug("Determinando lote actual en base a la ultima reversible");
					trxCurrentBatch = terminal.getLastRevTrx().getCurrentBatch();
				}
				//Si es de tipo confirmacion, asociar al lote de la ultima transacccion confirmable del terminal
				if(transactionType.isConfirmationTrxType()) {
					logger.debug("Determinando lote actual en base a la ultima confirmable");
					trxCurrentBatch = terminal.getLastConfTrx().getCurrentBatch();
				}
				//Si no es de ninguno de los tipo anteriores
				if(!transactionType.isInverseTrxType() && !transactionType.isConfirmationTrxType()) {
					//Tomar de la transaccion previa (prevTranId)
					logger.debug("Determinando lote actual de tipo no inversa ni de confirmacion");
					if(prevTransaction != null) {
						logger.debug("Determinando lote actual en base a transaccion previa enviada");
						trxCurrentBatch = prevTransaction.getCurrentBatch();
					} else  {
						//Si no existe transaccion previa cargar la ultima ejecutada del terminal
						trxCurrentBatch = terminal.getLastExecTrx().getCurrentBatch();
					}	
				}
			}

			logger.debug("El lote determinado para la transaccion es {}", trxCurrentBatch);
			transaction.setCurrentBatch(trxCurrentBatch);
			if(transaction.getTransactionType().isSettlementTrxType()) {
				//Si es un tipo de transaccion de cierre y el lote no tiene transacciones, no enviar el mismo
				if(trxCurrentBatch != null) {
					if(trxCurrentBatch.getTotalTrx() == 0) {
						throw new CleanBatch();
					}
				} else {
					throw new InvalidBatch();
				}
			}
			
			//Se almacena la transaccion
			logger.debug("Se almacena la transaccion {}", transaction);
			try {
				session.beginTransaction();
				session.save(transaction);
				session.getTransaction().commit();
			} catch (ConstraintViolationException e) {
				logger.error(e.toString());
				if(session != null) session.getTransaction().rollback();
				throw new DuplicateTransaction();
			}
			logger.trace("Transaccion almacenada");
			
			//Se marca como ultima ejecutada del terminal 
			// TODO colocar en executeInverse?
			terminal.setLastExecTrx(transaction);
			//Si es de tipo reversible se marca como ultima reversible
			// TODO colocar en executeInverse?
			if(transaction.getTransactionType().isReversableType()) terminal.setLastRevTrx(transaction);
			//Si es de tipo confirmables se marca como ultima configurable
			// TODO colocar en executeInverse?
			if(transaction.getTransactionType().isConfirmableType()) terminal.setLastConfTrx(transaction);
			session.beginTransaction();
			session.update(terminal);
			session.getTransaction().commit();
			logger.trace("Transaccion almacenada como ultima ejecutada");
			// Si el terminal debe bloquearse en inversa, validar que el terminal no tenga reversos pendientes
			if((transactionType.isBlocksOnInv()) && (terminal.isPendingReverse())) { 
				byte[] invByteResponse = null;
				InverseQueueElem invQueueEl = invQueue.getiQueue().get(terminal.getTerminalGuid());
				TransactionResponse transactionResponse = null;
				try {
					// TODO Si tengo inversa pendiente pero nada en la cola
					
					IsoMessage invIsoMsg = invQueueEl != null ? invQueueEl.getIsoMessage() : (IsoMessage)transactionFactory.buildTransaction(
																							transactionType.getTransactionInverseType(), 
																							paymentChannel , 
																							terminal, 
																							requestData, 
																							terminal.getLastRevTrx(),
																							prevTransaction);
					TransactionFactory invTansactionFactory = new TransactionFactory();
					invByteResponse = selectSendMsgFunc(
							paymentChannel, 
							SendFuncType.INVERSE,
							transaction,
							null,
							null,
							invIsoMsg,
							requestData);
					// TODO PREPROCESAMIENTO TRAMA
					IsoMessage invResIsoMsg = invTansactionFactory.parseMessage(invByteResponse, invQueueEl.getTransaction(), true);
					TransactionFactory.printIsoMessage(invResIsoMsg);
					logger.debug("invQueueEl {} null", invQueueEl != null ? "!=" : "==");
					logger.debug("invQueueEl.getTransaction() {} null", invQueueEl.getTransaction() != null ? "!=" : "==");
					logger.debug("invQueueEl.getTransaction().getTransactionType() {} null", invQueueEl.getTransaction().getTransactionType() != null ? "!=" : "==");
					//logger.debug("invQueueEl.getTransaction().getTransactionType().getTransactionInverseType() {} null", invQueueEl.getTransaction().getTransactionType().getTransactionInverseType() != null ? "!=" : "==");
					//logger.debug("invQueueEl.getTransaction().getTransactionType().getTransactionInverseType().getIsoTemplate() {} null", invQueueEl.getTransaction().getTransactionType().getTransactionInverseType().getIsoTemplate() != null ? "!=" : "==");
					logger.debug("invQueueEl.getTransaction().getTransactionType().getTransactionResponseType() {} null", invQueueEl.getTransaction().getTransactionType().getTransactionResponseType() != null ? "!=" : "==");
					logger.debug("invQueueEl.getTransaction().getTransactionType().getTransactionResponseType().getIsoTemplate() {} null", invQueueEl.getTransaction().getTransactionType().getTransactionResponseType().getIsoTemplate() != null ? "!=" : "==");
					transactionResponse = invTansactionFactory.getTransactionResponse(
							invResIsoMsg, 
							//invQueueEl.getTransaction().getTransactionType().getTransactionInverseType().getIsoTemplate(),
							invQueueEl.getTransaction().getTransactionType().getTransactionResponseType().getIsoTemplate(),
							paymentChannel,
							invQueueEl.getTransaction());
					
				} catch(Exception e) {
					// TODO Manejar excepcion con inversa pendiente
					e.printStackTrace();
				}
				
				// TODO Si hay respuesta actualizar transaccion de inversa y continuar con la actual
				// TODO Si no hay respuesta actualizar estado de ambas transacciones y devolver error (agregar status nuevo)
				if(transactionResponse != null) {
					logger.trace("transactionResponse diferente de null");
					if(transactionResponse.isApprovalCode(invQueueEl.getTransaction().getTransactionType().getTransactionInverseType())) {
						logger.trace("transactionResponse es un tipo aprobado");
						terminal.setPendingReverse(false);
						//Transaction invTrx = (Transaction) session.get(Transaction.class, invQueueEl.getTransaction().getId());
						//invTrx.setTransactionStatus(TransactionStatus.COMPLETED);
						//invQueueEl.getTransaction().setTransactionStatus(TransactionStatus.COMPLETED);
						session.beginTransaction();
						session.update(terminal);
						session.getTransaction().commit();
						inverseUpdateProc(terminal, session); // ***
						//session.beginTransaction();
						//session.update(invTrx);
						//session.getTransaction().commit();
						
					} else {
						logger.trace("transactionResponse es un tipo negado");
						session.beginTransaction();
						transaction.setTransactionStatus(TransactionStatus.CANCELED);
						session.update(transaction);
						session.getTransaction().commit();
						throw new PendingInverse();
					}
				} else {
					logger.trace("transactionResponse igual a null");
					session.beginTransaction();
					transaction.setTransactionStatus(TransactionStatus.CANCELED);
					session.save(transaction);
					session.getTransaction().commit();
					throw new PendingInverse();
				}
			}
			// Si se bloquea en confirmacion validar que el terminal no tenga confirmaciones pendientes
			if((transactionType.isBlocksOnConf()) && terminal.isPendingConfirmation() 
					&& (!transaction.getTransactionType().isConfirmationTrxType())) {
				byte[] confByteResponse = null;
				ConfirmationQueueElem confQueueEl = confQueue.getcQueue().get(terminal.getTerminalGuid());
				TransactionResponse transactionResponse = null;
				try {
					// TODO Si tengo confirmacion pendiente pero nada en la cola
					
					IsoMessage confIsoMsg = confQueueEl != null ? confQueueEl.getIsoMessage() : (IsoMessage)transactionFactory.buildTransaction(
																								transactionType.getTransactionConfirmationType(), 
																								paymentChannel , 
																								terminal, 
																								requestData, 
																								transaction,
																								prevTransaction);
					
					TransactionFactory confTansactionFactory = new TransactionFactory();		
					confByteResponse = selectSendMsgFunc(
							paymentChannel, 
							SendFuncType.CONFIRMATION,
							transaction,
							null,
							confIsoMsg,
							null,
							requestData);		
					IsoMessage confResIsoMsg = confTansactionFactory.parseMessage(confByteResponse, confQueueEl.getTransaction(), true);
					TransactionFactory.printIsoMessage(confResIsoMsg);
					transactionResponse = confTansactionFactory.getTransactionResponse(
							confResIsoMsg, 
							confQueueEl.getTransaction().getTransactionType().getTransactionResponseType().getIsoTemplate(), 
							paymentChannel,
							confQueueEl.getTransaction());
					
				} catch (Exception e) {
					// TODO Manejar excepcion con confirmacion pendiente
					e.printStackTrace();
				}
				//Si hay respuesta actualizar transaccion de confirmacion y continuar con la actual
				//Si no hay respuesta actualizar estado de ambas transacciones y devolver error (agregar status nuevo)
				if(transactionResponse != null) {
					logger.trace("transactionResponse diferente de null");
					if(transactionResponse.isApprovalCode(confQueueEl.getTransaction().getTransactionType())) {
						logger.trace("transactionResponse es un tipo aprobado");
						session.beginTransaction();
						confQueueEl.getTransaction().setTransactionStatus(TransactionStatus.COMPLETED);
						session.update(confQueueEl.getTransaction());
						session.getTransaction().commit();
					} else {
						logger.trace("transactionResponse es un tipo negado");
						session.beginTransaction();
						transaction.setTransactionStatus(TransactionStatus.CANCELED);
						session.update(transaction);
						session.getTransaction().commit();
						throw new PendingConfirmation();
					}
				} else {
					logger.trace("transactionResponse igual a null");
					session.beginTransaction();
					transaction.setTransactionStatus(TransactionStatus.CANCELED);
					session.save(transaction);
					session.getTransaction().commit();
					throw new PendingConfirmation();
				}
				
			}
			
			// Solicitud de envio de la transaccion
			byte[] byteResponse = null;
			int retryAttempts = 0;
			while((byteResponse == null) && (retryAttempts <= transactionType.getRetryAttempts())){
				try {
					logger.trace("byteResponse: {} retryAttempts: {} getRetryAttempts: {}", new Object[]{byteResponse, retryAttempts, transactionType.getRetryAttempts()});
					byteResponse = selectSendMsgFunc(
							paymentChannel, 
							SendFuncType.TRANSACTION,
							transaction,
							reqIsoMsg,
							null,
							null,
							requestData);
//					logger.debug("*****byteResponse {}", byteResponse);
					
				} catch (Exception e) {
					logger.error(e.toString());
					if(byteResponse == null) {
						if(retryAttempts >= transactionType.getRetryAttempts()) {
							transaction.setTransactionStatus(TransactionStatus.TIMED_OUT);
							logger.debug("No se recibio respuesta de la transaccion, marcando como TIMED_OUT");
							session.beginTransaction();
							session.update(transaction); 
							session.getTransaction().commit();
							throw new NoResponse();
						} else {
							retryAttempts++;	
							try {
								Thread.sleep((long) (transactionType.getRetryDelay()*10000));
								logger.debug("Reintentar transaccion despues de un delay de {}", (long) (transactionType.getRetryDelay()*10000));
							} catch (InterruptedException e1) {
								logger.error("Error ejecutando delay antes del reintento");
							}
							transaction.setRetried(true);
							transaction.setRetryAttempts(retryAttempts);
						}
						session.beginTransaction();
						// TODO probar que caiga en este update, antes era un save
						session.update(transaction); 
						session.getTransaction().commit();
//						if((retryAttempts > transactionType.getRetryAttempts()) 
//								|| (transactionType.getRetryAttempts() == 0)) throw new NoResponse();
					} else {
						throw e;
					}
				} finally {
					logger.debug("Verificando respuesta si es una confirmacion");
					//Si no hay respuesta y es una confirmacion se encola como pendiente
					if((transaction.getTransactionStatus().equals(TransactionStatus.TIMED_OUT) || (byteResponse == null)) &&
					   transaction.getTransactionType().isConfirmationTrxType()) {
						logger.debug("Encolando confirmacion para enviar posteriormente");
						if(confQueue == null) logger.error("confQueue es null");
						if(confQueue.getcQueue() == null) logger.error("confQueue.getcQueue() es null");
						confQueue.getcQueue().put(terminal.getTerminalGuid(), new ConfirmationQueueElem(reqIsoMsg, transaction.getTransactionType().getTimeout(), transaction));
					}
				}
			}
			
			
			// Procesamiento de la respuesta
			IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction, true);
			//IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction.getTransactionType());
			TransactionFactory.printIsoMessage(resIsoMsg);
			
			resParams = transactionFactory.getTransactionParameters(resIsoMsg, transaction.getTransactionType().getTransactionResponseType().getIsoTemplate(), transaction);	

			// Determinar el tipo de respuesta
			TransactionResponse transactionResponse = transactionFactory.getTransactionResponse(
					resIsoMsg, 
					transaction.getTransactionType().getTransactionResponseType().getIsoTemplate(), 
					paymentChannel,
					transaction);
			logger.debug("Verificando si la respuesta de la transaccion es aprobada {}", transactionResponse);
			if(transactionResponse.isApprovalCode(transaction.getTransactionType())) {
				//Asociar como ultima aprobada del terminal
				terminal.setLastApprvTrx(transaction);
				session.beginTransaction();
				session.update(terminal);
				session.getTransaction().commit();
				//Si requiere confirmacion marcar como pendiente del terminal 
				if(transaction.getTransactionType().getTransactionConfirmationType() != null) {
					//Setear valor a la fecha de inicio de operaciones si ha sido una compra exitosa 
					if(terminal.getStartDateOperations().equals(null) || terminal.getStartDateOperations() == null){
						logger.info("---- Fecha de inicio de operaciones ----");
						terminal.setStartDateOperations(Calendar.getInstance());
					}
					terminal.setLastTransactionDate(Calendar.getInstance());//Fecha de la ultima transaccion aprobada
					terminal.setPendingConfirmation(true);
					session.beginTransaction();
					session.update(terminal);
					session.getTransaction().commit();
				}
				//Si es una transaccion de confirmacion desmarcar como pendiente del terminal y marcar como confirmada la transaccion
				if(transaction.getTransactionType().isConfirmationTrxType()) {
					Transaction lastConf = terminal.getLastConfTrx();
					lastConf.setTransactionStatus(TransactionStatus.CONFIRMED);
					terminal.setPendingConfirmation(false);
					session.beginTransaction();
					session.update(lastConf);
					session.update(terminal);
					session.getTransaction().commit();
				}
				//Si es una transaccion inversa marcar como reversada la transaccion
				//desbloquear terminal por inversa y 
				//disminuir totales de lote
				if(transaction.getTransactionType().isInverseTrxType()) {
					inverseUpdateProc(terminal, session);
				}
				//Si es una transaccion de anulacion marcar como anulada la transaccion previas
				if(transaction.getTransactionType().isAnnulmentType()) {
					prevTransaction.setTransactionStatus(TransactionStatus.ANULLED);
					session.beginTransaction();
					session.update(prevTransaction);
					session.getTransaction().commit();
				}
				
				//Si es un tipo de transaccion que requiera operacion de lote/cierre
				logger.debug("Tipo de operacion de lote/cierre: {}", transaction.getTransactionType().getBatchOperation());
				logger.debug("Lote determinado para esta transaccion {}", trxCurrentBatch);
				if(!transaction.getTransactionType().getBatchOperation().equals(BatchOperation.NONE)) {				
					//trxCurrentBatch.setTotalTrx(trxCurrentBatch.getTotalTrx()+1);
					switch(transaction.getTransactionType().getBatchOperation()) {
						case ADD:
							trxCurrentBatch.setTotalAmount(trxCurrentBatch.getTotalAmount()+transaction.getAmount());
							trxCurrentBatch.setTotalTrx(trxCurrentBatch.getTotalTrx()+1);
							logger.debug("Aplicando operacion ADD a lote actual");
							break;
						case SUBTRACT:
							trxCurrentBatch.setTotalAmount(trxCurrentBatch.getTotalAmount()-transaction.getAmount());
							trxCurrentBatch.setTotalTrx(trxCurrentBatch.getTotalTrx()-1);
							logger.debug("Aplicando operacion SUBTRACT a lote actual");
							break;
						default:
							break;
					}
					logger.debug("Actualizando lote actual");
					session.beginTransaction();
					session.update(trxCurrentBatch);
					session.getTransaction().commit();
				}
				
				//Si es un tipo de transaccion de cierre, crear el batch history, 
				//actualizar las transacciones y actualizar el current batch
				if(transaction.getTransactionType().isSettlementTrxType()) {
					closeBatch(session, terminal, trxCurrentBatch);
				}
			} else {
				//Si la respuesta no es aprobada y es un tipo de transaccion de cierre
				//validar si es un descuadre
				if(transactionResponse.isUnbalanceCode() && 
						transaction.getTransactionType().isSettlementTrxType()) {
					//Si es un descuadre y tiene configurada plantilla de batch upload
					logger.debug("Descuadre de transaccion tipo cierre, enviando batch upload");
					if(transaction.getTransactionType().getTransactionBatchUploadType() != null) {
						//Invocar proceso de batch upload
						batchUpload(trxCurrentBatch, transaction.getTransactionType().getTransactionBatchUploadType());
						//Enviar cierre trailer
						//Almacenar el objeto transaccion
						String controlToken = requestData.getControlToken()+"ST";
						Transaction settleTrailerT = new Transaction(terminal, 
													controlToken,
													transactionType,
													Utils.calcSecToken(terminal, requestData.getTimestamp(),controlToken)
													);
						session.beginTransaction();
						session.save(settleTrailerT);
						session.getTransaction().commit();
								
						TransactionType settleTrailerTt = transaction.getTransactionType().getTransactionTrailerSettleType();
						
						IsoMessage settleTIsoMsg = (IsoMessage)transactionFactory.buildTransaction(
								settleTrailerTt, 
								paymentChannel , 
								terminal, 
								requestData, 
								settleTrailerT,
								transaction);
						
						//Se envia la transaccion
						byteResponse = TransactionFactory.sendMsgAut(settleTIsoMsg, settleTrailerTt.getTimeout(),paymentChannel);
						// Procesamiento de la respuesta
						IsoMessage settleTIsoMsgR = transactionFactory.parseMessage(byteResponse, transaction, true);
						TransactionFactory.printIsoMessage(settleTIsoMsgR);
						// Determinar el tipo de respuesta
						TransactionResponse settleTResponse = transactionFactory.getTransactionResponse(
								settleTIsoMsgR,
								settleTrailerTt.getTransactionResponseType().getIsoTemplate(), paymentChannel,
								transaction);
						logger.debug("Respuesta de cierre trailer {}", settleTResponse);
						if(settleTResponse != null) { 
							if(settleTResponse.isApprovalCode(settleTrailerTt)) {
								//settleTrailerTrx = settleTrailerT;
								transactionResponse = settleTResponse;
								//Actualizar transacciones y lote actual
								closeBatch(session, terminal, trxCurrentBatch);
							}
						}
					}
				}
			}
	
			resParams.getReturnableParams().putAll(reqParams.getReturnableParams());
			SimpleDateFormat simpDateForm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");	
			logger.debug("Marca de tiempo de la transaccion: {}", simpDateForm.format(transaction.getTxnDate().getTime()));
			coreResponse = new CoreResponse(transaction.getHashCode(), simpDateForm.format(transaction.getTxnDate().getTime()), transactionResponse.getCode(), transactionResponse.getName(), resParams.getReturnableParams(), new HashMap<Integer, String>(), new HashMap<Integer, String>());
			
			coreResponse.setStatePrint(transaction.getTransactionType().isStatePrint());
			if(transactionResponse != null) {
				transaction.setTransactionResponse(transactionResponse);
				transaction.setTransactionStatus(TransactionStatus.COMPLETED);
			}
			logger.debug("Tipo de Tarjeta - trxBatchType:==>{}",trxBatchType);
			//Almacenar parametros adicionales:==>
			try{
				logger.debug("Almacenar parametros adicionales:==>");
				for (Iterator<TransactionParameter> iteratorTp = transaction.getParameters().iterator(); iteratorTp.hasNext();){
					TransactionParameter transp = new TransactionParameter();
					transp = (TransactionParameter) iteratorTp.next();
					logger.debug("Transp: ==>{}",transp);
					if (transp.getParamName().equals("affiliated_id")){
						logger.debug(transp.getParamName()+": {}"+ transp.getParamValue());
							boolean active = true;
							String sqlAVF = "SELECT tableName, fieldName, message, startReceipt, endReceipt, orderField from "
									+ "AdditionalVoucherFields as additionalVoucherFields "
									+ "where additionalVoucherFields.active = :active "
									+ "and additionalVoucherFields.batchType = :batchType "
									+ "and additionalVoucherFields.transactionType = :transactionType "
									+ "Order By additionalVoucherFields.startReceipt DESC, additionalVoucherFields.orderField";
							logger.debug("sqlAVF:==>{}",sqlAVF);
							ArrayList<AdditionalVoucherFields> results = new ArrayList<AdditionalVoucherFields>();
							Query query = session.createQuery(sqlAVF);
							query.setBoolean("active", active)
							.setEntity("batchType", trxBatchType)
							.setEntity("transactionType", transaction.getTransactionType());
							results = new ArrayList<AdditionalVoucherFields>(
									query
									.list());
							logger.debug("results:==>{}",results);
							 for (Iterator iterator = results.iterator(); iterator.hasNext();){
								 Object[] obj = (Object[]) iterator.next();
								 
								 logger.debug("{}","tableName:" + String.valueOf(obj[0]) + ", fieldName:" + String.valueOf(obj[1])+ ", Order:" + String.valueOf(obj[5]));
								 String paramValue, abrev = "";
								 if (String.valueOf(obj[0]).equals("") && String.valueOf(obj[1]).equals("")){
									 paramValue = String.valueOf(obj[2]);
								 }else{
									 if (String.valueOf(obj[0]).equals("TRANSACTION") || String.valueOf(obj[0]).equals("transaction") || String.valueOf(obj[0]).equals("Transaction")){
										 abrev = "t.";
									 } else if (String.valueOf(obj[0]).equals("TRANSACTION_PARAMETERS") || String.valueOf(obj[0]).equals("transaction_parameters") || String.valueOf(obj[0]).equals("Transaction_Parameters")){
										 abrev = "tp.";
									 }else if (String.valueOf(obj[0]).equals("ACQUIRER") || String.valueOf(obj[0]).equals("acquirer") || String.valueOf(obj[0]).equals("Acquirer")){
										 abrev = "a.";
									 }else if (String.valueOf(obj[0]).equals("BANK") || String.valueOf(obj[0]).equals("bank") || String.valueOf(obj[0]).equals("Bank")){
										 abrev = "b.";
									 }else if (String.valueOf(obj[0]).equals("AFFILIATION") || String.valueOf(obj[0]).equals("affiliation") || String.valueOf(obj[0]).equals("Affiliation")){
										 abrev = "af.";
									 }else if (String.valueOf(obj[0]).equals("MERCHANT") || String.valueOf(obj[0]).equals("merchant") || String.valueOf(obj[0]).equals("Merchant")){
										 abrev = "m.";
									 }else if (String.valueOf(obj[0]).equals("TRANSACTION_TYPE") || String.valueOf(obj[0]).equals("transaction_type") || String.valueOf(obj[0]).equals("Transaction_type")){
										 abrev = "tpy.";
									 }
									 Query queryParam = null;
									 String sqlParam="" ;
									 if (abrev=="tpy."){
										 sqlParam = "SELECT " + abrev + String.valueOf(obj[1]) + " FROM TRANSACTION t "
											 		+ "inner join TRANSACTION_TYPE tpy on t.TRANSACTION_TYPE_ID=tpy.id "
											 		+ "where t.ID=:tid";
										 queryParam = session.createSQLQuery(sqlParam)
												 .setBigDecimal("tid", new BigDecimal(transaction.getId()));
									 }else if (abrev=="tp."){
										 sqlParam = "SELECT DISTINCT tp.PARAM_VALUE FROM TRANSACTION t inner join TRANSACTION_PARAMETERS tp on t.ID=tp.TRANSACTION_ID "
											 		+ "where (tp.PARAM_NAME=:paramName) and t.ID=:tid";
										 queryParam = session.createSQLQuery(sqlParam)
												 .setString("paramName", String.valueOf(obj[1]))
												 .setBigDecimal("tid", new BigDecimal(transaction.getId()));
									 }else{
										 sqlParam = "SELECT " + abrev + String.valueOf(obj[1]) //+ " FROM TRANSACTION t inner join TRANSACTION_PARAMETERS tp on t.ID=tp.TRANSACTION_ID "
											 		+ " FROM ACQUIRER ac inner join BANK b on ac.BANK_ID=b.ID "
											 		+ "inner join AFFILIATION af on ac.AFFILIATION_ID=af.ID inner join MERCHANT m on af.MERCHANT_ID=m.ID "
											 		+ "where (ac.MERCHANT_ID_CODE=:paramValue)";
										 queryParam = session.createSQLQuery(sqlParam)
												 .setString("paramValue", transp.getParamValue());
												 //.setBigDecimal("tid", new BigDecimal(transaction.getId()));
									 }
									 logger.debug("sqlParam ==> {}", sqlParam );
									 paramValue="";
									 logger.debug("paramValue Antes del IF ANS_CODE ==> {}", coreResponse.getCode() + ", " + coreResponse.getDescription());
									 logger.debug("IF String.valueOf(obj[1])={}, abrev={}", String.valueOf(obj[1]),abrev );
									 if ((String.valueOf(obj[1]).equals("ans_code") || String.valueOf(obj[1]).equals("Ans_Code") || String.valueOf(obj[1]).equals("ANS_CODE")) && abrev == "tp."){
										 logger.debug("paramValue ANS_CODE ==> {}", coreResponse.getCode() + ", " + coreResponse.getDescription());
										 paramValue= coreResponse.getCode() + ", " + coreResponse.getDescription();
									 }else if ((String.valueOf(obj[0]).equals("") && String.valueOf(obj[1]).equals("")) ||(String.valueOf(obj[0]).equals("\"\"") && String.valueOf(obj[1]).equals("\"\"")) ){
										 paramValue = String.valueOf(obj[2]);
									 }else{
										 logger.debug("queryParam - Result ==> {}", sqlParam );
										 //SQLQuery queryParam = session.createSQLQuery(sqlParam);
										 queryParam.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
										 List data = queryParam.list();
								         for(Object object : data)
								         {
								            Map row = (Map)object;
								            System.out.print("First Name: " + row.get(String.valueOf(obj[1])));
											paramValue = (String) row.get(String.valueOf(obj[1]));	
								         }
									 }
									 
							         logger.debug("order: {}", String.valueOf(obj[5]));
							         logger.debug("paramValue: {}", paramValue);
								 }
								 try{
									 logger.debug("Asignar Param {} , {}", new Object[] {String.valueOf((Integer)obj[5]), paramValue});
									 if (String.valueOf(obj[3]).equals("true")){
										 coreResponse.getAdditionalTopData().put((Integer) obj[5], paramValue);
									 }else if (String.valueOf(obj[4]).equals("true")){
										 coreResponse.getAdditionalBottomData().put((Integer) obj[5], paramValue);
									 }
								 }catch(Exception e) {
									 logger.debug("Error en la asignacion de los Param: {}", e.toString());
								 }
							 }
					}
				};	
			}catch (Exception e){
				logger.debug("No hay parametros en la lista AdditionalVoucherFields.");
			}
			
			try {
				resParams.getStorableParams().forEach((key, param) -> {
					transaction.getResponseParameters().add(new TransactionResponseParameter(transaction, key, param));
				});
				session.beginTransaction();
				session.update(transaction);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				logger.error(e.toString());
				if(session != null) session.getTransaction().rollback();
				throw new TransactionException();
			}
			
			//Validamos la transaccion para determinar si generara la invocacion de una inversa
			boolean validTrx = true;
			try {
				ClassLoader classLoader = TransactionFactory.class.getClassLoader();
				IsoTemplate trxIsoTemplate = transaction.getTransactionType().getIsoTemplate();	
				logger.debug("Validando si generara transaccion inversa...");
				logger.debug("{} {} {}", new Object[] {trxIsoTemplate.getSubpackageName(), trxIsoTemplate.getClassName(), trxIsoTemplate.getValRevFunction()});
				Class<?> extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(trxIsoTemplate.getSubpackageName().concat(".").concat(trxIsoTemplate.getClassName())));
				logger.debug(extClass != null ? "extClass != null" : "extClass == null");
				logger.debug(extClass.getMethod(trxIsoTemplate.getValRevFunction(), Transaction.class) != null ? "Method not null" : "Method null");
				logger.debug("method.getName()= {}", extClass.getMethod(trxIsoTemplate.getValRevFunction(), Transaction.class).getName());
				validTrx = (boolean) extClass.getMethod(trxIsoTemplate.getValRevFunction(), Transaction.class).invoke(null, transaction);
			} catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException | ClassNotFoundException e) {
				logger.error(e.toString());
			} catch (Exception e) {
				logger.error(e.toString());
			}
			if(!validTrx) {
				logger.info("Transaccion requiere invocar la inversa");
				throw new InvertTransaction(coreResponse);
			}
			
			//transactionToDel = transaction;
		} catch (Exception e) {
			logger.error(e.toString());
			logger.debug("Catch final de executeTransaction");
			throw e;
		} finally {
			//session.beginTransaction();
			//session.delete(transactionToDel);
			//session.getTransaction().commit();
			if(session != null) session.close();
		}
    	logger.debug("coreResponse: ==>{}", coreResponse);
    	return coreResponse;
    }
    
    /**
     * Solicitar el envio de una transaccion
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @throws TransactionException Excepcion en transaccion
     */
    public CoreResponse executeTransaction(TransactionData requestData) throws TransactionException {
    	return executeTransaction(requestData, null);
    }
    
    /**
     * Solicitar el envio de la inversa de una transaccion
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @param inverseTransactionType Tipo de transaccion inversa
     * 
     * @throws Exception Excepcion general en el curso de la ejecucion de la aplicacion
     */
    public void executeInverse(
    		TransactionData requestData, 
    		TransactionType inverseTransactionType, 
    		Integer prevTranId) throws Exception {
    	Session session = null;
    	Transaction transactionToInvert = null;
    	PaymentChannel paymentChannel = null; 
    	Terminal terminal = null;
    	try {
    		try {
				session = HibernateUtil.getSessionFactory().openSession();
			} catch (ExceptionInInitializerError err) {
				logger.error(err.toString());
				throw new ServiceUnavailable();
			}
    		
    		//Se obtiene la transaccion
    		transactionToInvert = (Transaction) session.createQuery(
    				"from Transaction as transaction where transaction.secToken = :secToken")
    				.setString("secToken", requestData.getSecToken())
    				.uniqueResult();
    		logger.trace("{}", transactionToInvert);
    		
    		//Si el tipo de transaccion permite la ejecucion de inversas de manera automatica
    		if (transactionToInvert.getTransactionType().isAutoInverse()) {
				logger.trace("Si el tipo de transaccion permite la ejecucion de inversas de manera automatica");
    			//Se obtiene el canal de pago
				paymentChannel = (PaymentChannel) session
						.createQuery("from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
						.setString("codeName", requestData.getChannelCode()).uniqueResult();
				logger.trace("{}", paymentChannel);
				//Se obtien el terminal
				terminal = (Terminal) session
						.createQuery("from Terminal as terminal where terminal.terminalGuid = :terminalGuid")
						.setString("terminalGuid", requestData.getTerminalId()).uniqueResult();
				logger.trace("{}", terminal);
				//Cargar transaccion previa si es requerida 
				Transaction prevTransaction = null;
				if (prevTranId != null) {
					prevTransaction = (Transaction) session.get(Transaction.class, prevTranId);
				}
				// Bloqueo de terminal por inversa pendiente
				terminal.setPendingReverse(true);
				session.beginTransaction();
				session.update(terminal);
				session.getTransaction().commit();
				//Crear un objeto intermedio de transaccion
				Transaction transaction = new Transaction(terminal, requestData.getControlToken(),
						inverseTransactionType, requestData.getSecToken());
				// Se obtienen los diferentes parametros de acuerdo a la plantilla del tipo de transaccion inversa
				TransactionFactory transactionFactory = new TransactionFactory();
				IsoMessage reqIsoMsg = (IsoMessage) transactionFactory.buildTransaction(inverseTransactionType,
						paymentChannel, terminal, requestData, transaction, prevTransaction);
				// Solicitud de envio de la transaccion inversa
				byte[] byteResponse = null;
				int retryAttempts = 0;
				logger.debug("inverseTransactionType.getRetryAttempts() {}", inverseTransactionType.getRetryAttempts());
				while ((byteResponse == null) && (retryAttempts <= inverseTransactionType.getRetryAttempts())) {
					try {
						logger.trace(" inverse retryAttempts: {}", retryAttempts);
						byteResponse = selectSendMsgFunc(
									paymentChannel, 
									SendFuncType.TRANSACTION,
									transaction,
									reqIsoMsg,
									null,
									null,
									requestData);

					} catch (Exception e) {
						logger.error(e.toString());
						if (byteResponse == null) {
							if (retryAttempts >= inverseTransactionType.getRetryAttempts()) {
								transaction.setTransactionStatus(TransactionStatus.TIMED_OUT); // TODO Cambiar por la transaccion inversa cuando se guarde
								logger.debug("No se recibio respuesta de la transaccion, marcando como TIMED_OUT");
								throw new NoResponse();
							} else {
								retryAttempts++;
								try {
									Thread.sleep((long) (inverseTransactionType.getRetryDelay() * 1000));
									logger.debug("Reintentar transaccion inversa despues de un delay de {}",
											(long) (inverseTransactionType.getRetryDelay() * 1000));
								} catch (InterruptedException e1) {
									logger.error("Error ejecutando delay antes del reintento");
								}
								transaction.setRetried(true); // TODO Cambiar por la transaccion inversa cuando se guarde
								transaction.setRetryAttempts(retryAttempts); // TODO Cambiar por la transaccion inversa cuando se guarde
							}
							//session.beginTransaction();
							//session.update(transaction); 
							//session.getTransaction().commit();
							//Si el numero de reintentos ha superado la configuracion o si no se debe reintentar la transaccion
							//						if((retryAttempts >= inverseTransactionType.getRetryAttempts()) 
							//								|| (inverseTransactionType.getRetryAttempts() == 0)) throw new NoResponse();
						} else {
							throw e;
						}
					} finally {
						logger.debug("Verificando respuesta de inversa");
						//Si no hay respuesta se encola como pendiente
						// TODO Cambiar el objeto transaction por el que se cree para almacenar la transaccion de reverso
						if (transaction.getTransactionStatus().equals(TransactionStatus.TIMED_OUT)
								|| (byteResponse == null)) {
							logger.debug("Encolando inversa para enviar posteriormente");
							if (invQueue == null)
								logger.error("invQueue es null");
							if (invQueue.getiQueue() == null)
								logger.error("invQueue.getiQueue() es null");
							invQueue.getiQueue().put(terminal.getTerminalGuid(), new InverseQueueElem(reqIsoMsg,
									transaction.getTransactionType().getTimeout(), transaction));
						}
					}
				}
				if (byteResponse == null)
					throw new ServiceUnavailable();
				// Procesamiento de la respuesta
				IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction, true);
				//IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction.getTransactionType());
				TransactionFactory.printIsoMessage(resIsoMsg);
				// Determinar el tipo de respuesta
				TransactionResponse transactionResponse = transactionFactory.getTransactionResponse(resIsoMsg,
						transaction.getTransactionType().getTransactionResponseType().getIsoTemplate(), paymentChannel,
						transaction);
				if (transactionResponse != null) {
					// TODO Duda de si el reverso debe ser solo aprobado para desbloquear o con la respuesta basta
					transactionToInvert.setTransactionStatus(TransactionStatus.REVERSED);
					terminal.setPendingReverse(false);
					session.beginTransaction();
					session.save(transactionToInvert);
					session.update(terminal);
					session.getTransaction().commit();
				} 
			}
    		logger.trace("Final de executeInverse");
    	} catch (Exception e) {
			logger.error(e.toString());
			throw e;
		} finally {
			if(session != null) session.close();
		}
    }
    
    /**
     * Solicitar el envio de la inversa de una transaccion
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @param inverseTransactionType Tipo de transaccion inversa
     * 
     * @throws Exception Excepcion general en el curso de la ejecucion de la aplicacion
     */
    public void executeLateInverse(
    		TransactionData requestData, 
    		TransactionType inverseTransactionType, 
    		Integer prevTranId) throws Exception {
    	Session session = null;
    	Transaction transactionToInvert = null;
    	PaymentChannel paymentChannel = null; 
    	Terminal terminal = null;
    	try {
    		try {
				session = HibernateUtil.getSessionFactory().openSession();
			} catch (ExceptionInInitializerError err) {
				logger.error(err.toString());
				throw new ServiceUnavailable();
			}
    		
    		//Se obtiene la transaccion
    		transactionToInvert = (Transaction) session.createQuery(
    				"from Transaction as transaction where transaction.secToken = :secToken")
    				.setString("secToken", requestData.getSecToken())
    				.uniqueResult();
    		logger.trace("{}", transactionToInvert);
    		
    		
				logger.trace("Si el tipo de transaccion permite la ejecucion de inversas de manera automatica");
    			//Se obtiene el canal de pago
				paymentChannel = (PaymentChannel) session
						.createQuery("from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
						.setString("codeName", requestData.getChannelCode()).uniqueResult();
				logger.trace("{}", paymentChannel);
				//Se obtien el terminal
				terminal = (Terminal) session
						.createQuery("from Terminal as terminal where terminal.terminalGuid = :terminalGuid")
						.setString("terminalGuid", requestData.getTerminalId()).uniqueResult();
				logger.trace("{}", terminal);
				//Cargar transaccion previa si es requerida 
				Transaction prevTransaction = null;
				if (prevTranId != null) {
					prevTransaction = (Transaction) session.get(Transaction.class, prevTranId);
				}
				// Bloqueo de terminal por inversa pendiente
				terminal.setPendingReverse(true);
				session.beginTransaction();
				session.update(terminal);
				session.getTransaction().commit();
				//Crear un objeto intermedio de transaccion
				Transaction transaction = new Transaction(terminal, requestData.getControlToken(),
						inverseTransactionType, requestData.getSecToken());
				// Se obtienen los diferentes parametros de acuerdo a la plantilla del tipo de transaccion inversa
				TransactionFactory transactionFactory = new TransactionFactory();
				IsoMessage reqIsoMsg = (IsoMessage) transactionFactory.buildTransaction(inverseTransactionType,
						paymentChannel, terminal, requestData, transaction, prevTransaction);
				// Solicitud de envio de la transaccion inversa
				byte[] byteResponse = null;
				int retryAttempts = 0;
				logger.debug("inverseTransactionType.getRetryAttempts() {}", inverseTransactionType.getRetryAttempts());
				while ((byteResponse == null) && (retryAttempts <= inverseTransactionType.getRetryAttempts())) {
					try {
						logger.trace(" inverse retryAttempts: {}", retryAttempts);
						byteResponse = selectSendMsgFunc(
									paymentChannel, 
									SendFuncType.TRANSACTION,
									transaction,
									reqIsoMsg,
									null,
									null,
									requestData);

					} catch (Exception e) {
						logger.error(e.toString());
						if (byteResponse == null) {
							if (retryAttempts >= inverseTransactionType.getRetryAttempts()) {
								transaction.setTransactionStatus(TransactionStatus.TIMED_OUT); // TODO Cambiar por la transaccion inversa cuando se guarde
								logger.debug("No se recibio respuesta de la transaccion, marcando como TIMED_OUT");
								throw new NoResponse();
							} else {
								retryAttempts++;
								try {
									Thread.sleep((long) (inverseTransactionType.getRetryDelay() * 1000));
									logger.debug("Reintentar transaccion inversa despues de un delay de {}",
											(long) (inverseTransactionType.getRetryDelay() * 1000));
								} catch (InterruptedException e1) {
									logger.error("Error ejecutando delay antes del reintento");
								}
								transaction.setRetried(true); // TODO Cambiar por la transaccion inversa cuando se guarde
								transaction.setRetryAttempts(retryAttempts); // TODO Cambiar por la transaccion inversa cuando se guarde
							}
							//session.beginTransaction();
							//session.update(transaction); 
							//session.getTransaction().commit();
							//Si el numero de reintentos ha superado la configuracion o si no se debe reintentar la transaccion
							//						if((retryAttempts >= inverseTransactionType.getRetryAttempts()) 
							//								|| (inverseTransactionType.getRetryAttempts() == 0)) throw new NoResponse();
						} else {
							throw e;
						}
					} finally {
						logger.debug("Verificando respuesta de inversa");
						//Si no hay respuesta se encola como pendiente
						// TODO Cambiar el objeto transaction por el que se cree para almacenar la transaccion de reverso
						if (transaction.getTransactionStatus().equals(TransactionStatus.TIMED_OUT)
								|| (byteResponse == null)) {
							logger.debug("Encolando inversa para enviar posteriormente");
							if (invQueue == null)
								logger.error("invQueue es null");
							if (invQueue.getiQueue() == null)
								logger.error("invQueue.getiQueue() es null");
							invQueue.getiQueue().put(terminal.getTerminalGuid(), new InverseQueueElem(reqIsoMsg,
									transaction.getTransactionType().getTimeout(), transaction));
						}
					}
				}
				if (byteResponse == null)
					throw new ServiceUnavailable();
				// Procesamiento de la respuesta
				IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction, true);
				//IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction.getTransactionType());
				TransactionFactory.printIsoMessage(resIsoMsg);
				// Determinar el tipo de respuesta
				TransactionResponse transactionResponse = transactionFactory.getTransactionResponse(resIsoMsg,
						transaction.getTransactionType().getTransactionResponseType().getIsoTemplate(), paymentChannel,
						transaction);
				if (transactionResponse != null) {
					// TODO Duda de si el reverso debe ser solo aprobado para desbloquear o con la respuesta basta
					transactionToInvert.setTransactionStatus(TransactionStatus.REVERSED);
					terminal.setPendingReverse(false);
					session.beginTransaction();
					session.save(transactionToInvert);
					session.update(terminal);
					session.getTransaction().commit();
				} 
			
    		logger.trace("Final de executeInverse");
    	} catch (Exception e) {
			logger.error(e.toString());
			throw e;
		} finally {
			if(session != null) session.close();
		}
    }
    
    /**
     * Solicitar el envio de la inversa de una transaccion
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @param inverseTransactionType Tipo de transaccion inversa
     * 
     * @throws Exception Excepcion general en el curso de la ejecucion de la aplicacion
     */
    public void executeInverse(TransactionData requestData, TransactionType inverseTransactionType) throws Exception {
    	executeInverse(requestData, inverseTransactionType, null);
    }
    
    public FunctionResponse executeFunction(FunctionData functionData) throws TransactionException {
    	Session session = null;
    	FunctionResponse funcResp = null;
    	try {
			FunctionFactory functionFactory = new FunctionFactory();	
			Terminal terminal = null;

			try {
				session = HibernateUtil.getSessionFactory().openSession();
			} catch (ExceptionInInitializerError err) {
				logger.error(err.toString());
				throw new ServiceUnavailable();
			}
			
			//Se obtiene el terminal del que procede la transaccion
			try {
				terminal = (Terminal) session.createQuery(
						"from Terminal as terminal where terminal.terminalGuid = :terminalGuid").
						setString("terminalGuid", functionData.getTerminalId())
					    .uniqueResult();
				logger.trace("Terminal affiliation {}", terminal.getAffiliation());
			}  catch (Exception e) {
				logger.error(e.toString());
				logger.debug("NonExistentTerminal");
				if(terminal == null) throw new NonExistentTerminal();
			}

			switch(functionData.getFunctionType()) {
				case "CCR_SUMMARY_REQ":
					funcResp =  functionFactory.summaryFunction(terminal, functionData, session);
					break;
				default:
					funcResp = null;
					break;
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			if(session != null) session.close();
		}
    	return funcResp;
    }
    
    /**
     * Determina cual es la inversa de un tipo de transaccion
     * 
     * @param transactionTypeName Nombre del tipo de transaccion
     * 
     * @param channelCode Canal de pago al que esta asociado el tipo de transaccion
     * 
     * @return Inversa del tipo de transaccion determinado por <strong>transactionTypeName</strong> y <strong>channelCode</strong>
     * 
     * @throws NonExistentPaymentChannel Excepcion generada cuando el canal de pago solicitado no existe
     * 
     * @throws InvalidTransactionType Excepcion generada cuando el tipo de transaccion solicitada es invalida
     * 
     * @throws ServiceUnavailable Excepcion generada cuando no se puede acceder a algun componente de la plataforma
     */
    public TransactionType getTransactionInverse(String transactionTypeName, String channelCode) throws NonExistentPaymentChannel, InvalidTransactionType, ServiceUnavailable {
    	PaymentChannel paymentChannel = null;
    	TransactionType transactionType = null;
    	Session session = null;
    	
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (ExceptionInInitializerError err) {
			logger.error(err.toString());
			throw new ServiceUnavailable();
		}
    	
    	//Se obtiene el canal de pago
		try {
			paymentChannel = (PaymentChannel) session.createQuery(
					"from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
					.setString("codeName", channelCode)
					.uniqueResult();
			logger.debug("{}", paymentChannel);
		} catch (Exception e) {
			logger.error(e.toString());
			if(paymentChannel == null) throw new NonExistentPaymentChannel();
		}
		
		//Se obtiene el tipo de transaccion
		try {
			transactionType = (TransactionType) session.createQuery(
				    "from TransactionType as transactionType where transactionType.name = :name and transactionType.templateMessage = :templateMessage")
				    .setString("name", transactionTypeName)
				    .setEntity("templateMessage", paymentChannel.getTemplateMessage().get(0))
				    .uniqueResult();
			logger.debug("{}", transactionType.getDescription());
		} catch (Exception e) {
			logger.error(e.toString());
			if(transactionType == null) throw new InvalidTransactionType();
		}
	
    	return transactionType.getTransactionInverseType();
    }
    
    public Integer getIdTransactionFromRef(
    		String terminalGuid, 
    		String referenceNum, 
    		TransactionData transactionData,
    		String channelCode) throws NonExistentPaymentChannel, NonExistentTerminal, ServiceUnavailable {
    	
    	Session session = null;
    	PaymentChannel paymentChannel = null;
    	Terminal terminal = null;
    	
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (ExceptionInInitializerError err) {
			logger.error(err.toString());
			throw new ServiceUnavailable();
		}
    	
    	//Se obtiene el canal de pago
		try {
			paymentChannel = (PaymentChannel) session.createQuery(
					"from PaymentChannel as paymentChannel where paymentChannel.codeName = :codeName")
					.setString("codeName", channelCode)
					.uniqueResult();
			logger.debug("{}", paymentChannel);
		} catch (Exception e) {
			logger.error(e.toString());
			if(paymentChannel == null) throw new NonExistentPaymentChannel();
		}
		
		//Se obtiene el terminal
		try {
			terminal = (Terminal) session
					.createQuery("from Terminal as terminal where terminal.terminalGuid = :terminalGuid")
					.setString("terminalGuid", terminalGuid).uniqueResult();
			logger.trace("{}", terminal);
		} catch (Exception e) {
			logger.error(e.toString());
			if(terminal == null) throw new NonExistentTerminal();
		}
    	
    	//Determinar el lote de la transaccion
    	TransactionFactory transactionFactory = new TransactionFactory();
    	BatchType batchType = transactionFactory.determineTrxBatchT(paymentChannel, transactionData);
    	CurrentBatch currentBatch = null;
    	for(CurrentBatch currBatch : terminal.getCurrentBatchs()) {
    		if(currBatch.getBatchType().equals(batchType)) currentBatch = currBatch;
    	}
    	logger.debug("getIdTransactionFromRef currentBatch {}", currentBatch);
    	// Nombre del parametro de la transaccion que representa la referencia
    	String referenceParam = "pos_ref"; // TODO parametrizar
    	Transaction prevTrx = null;
  
    	if(currentBatch != null) 
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
			String trxPrevQuery = 							
					"select transaction "
					+ "from Transaction as transaction "
					+ "inner join transaction.parameters as refParam "
					+ "where refParam.paramName = :refParamN "
					+ "and refParam.paramValue = :refParamV "
					+ "and transaction.currentBatch = :currentBatch "
					+ "and transaction.terminal = :terminal "
					;
			logger.trace("refParamN: " + referenceParam);
			logger.trace("refParamV: " + referenceNum);
			ArrayList<Transaction> transList = (ArrayList<Transaction>)session.createQuery(trxPrevQuery)
				.setString("refParamN", referenceParam)
				.setString("refParamV", referenceNum)
				.setEntity("currentBatch", currentBatch)
				.setEntity("terminal", terminal)
				.list();
			if(transList != null) {											
				if (transList.size() > 0) {
					logger.trace("transList != null and has " + String.valueOf(transList.size()) + " elements");
					logger.trace("transList(0) ->  " + transList.get(0).toString());
					transList.forEach(transaction -> {
						logger.trace("transaction prev -> " + transaction.toString());
					});
					prevTrx = transList.get(0);
				} else return -1;
			} else {
				logger.trace("transList == null");
			}
		} catch (Exception e) {
			logger.error(e.toString());
			return -1;
		}
    	return prevTrx != null ? prevTrx.getId() : -1;
    }
 
    /**
     * Solicitar la ejecucion de una funcion no transaccional
     * 
     * @param requestData Datos necesarios para la ejecucion de la transaccion incluyendo su construccion y posterior envio
     * 
     * @throws TransactionException Excepcion en transaccion
     */
    public FunctionStatusMessage executeFunction() {
    	return null;
    }
    
    private byte[] selectSendMsgFunc(
    		PaymentChannel paymentChannel,
    		SendFuncType sendFuncType,
    		Transaction transaction,
    		IsoMessage reqIsoMsg,
    		IsoMessage confIsoMsg,
    		IsoMessage invIsoMsg,
    		TransactionData requestData		 
    		) throws ServiceUnavailable, ConnectionFailure, NoResponse {  	
    	
    	byte[] invByteResponse = null;
    	byte[] confByteResponse = null;
    	byte[] byteResponse = null;
    	TransactionFactory trxFact = new TransactionFactory();
    	
    	// TODO Cargar solo al inicio del aplicativo
    	EngineProperties.loadProperties();
    	String codeName = EngineProperties.getVauthOn() ? "FINANPLUS" : paymentChannel.getCodeName();
    	
    	//Determinamos el canal de pago
    	switch(codeName) {
	    	case "FINANPLUS":
	    		switch(sendFuncType) {
		    		case TRANSACTION:
		    			// VAutorizacion HTTP POST
		    			byteResponse = TransactionFactory.sendMsgAutPost(reqIsoMsg, transaction.getTransactionType().getTimeout());
		    			break;
		    		case CONFIRMATION:
		    			// VAutorizacion HTTP POST
		    			confByteResponse = TransactionFactory.sendMsgAutPost(confIsoMsg, transaction.getTransactionType().getTimeout());
		    			break;
		    		case INVERSE:
		    			// VAutorizacion HTTP POST
		    			invByteResponse = TransactionFactory.sendMsgAutPost(invIsoMsg, transaction.getTransactionType().getTimeout());
		    			break;
		    		default:
		    			break;
	    		}
	    		break;
	    	case "CPAGO_CC":
	       		switch(sendFuncType) {
		    		case TRANSACTION:
		    			// Credicard socket basico	
		    			byteResponse = TransactionFactory.sendMsgAut(reqIsoMsg, transaction.getTransactionType().getTimeout(),paymentChannel);
		    			break;
		    		case CONFIRMATION:
		    			// Credicard socket basico
		    			confByteResponse = TransactionFactory.sendMsgAut(confIsoMsg, transaction.getTransactionType().getTransactionConfirmationType().getTimeout(),paymentChannel);
		    			break;
		    		case INVERSE:
		    			// Credicard socket basico
		    			invByteResponse = TransactionFactory.sendMsgAut(invIsoMsg, transaction.getTransactionType().getTransactionInverseType().getTimeout(),paymentChannel);
		    			break;
		    		default:
		    			break;
	       		}
	    		break;
	    	case "MENSAJERIA_CCR":
	       		switch(sendFuncType) {
		    		case TRANSACTION:
		    			// Credicard socket basico	
		    			byteResponse = TransactionFactory.sendMsgAut(reqIsoMsg, transaction.getTransactionType().getTimeout(), paymentChannel);
		    			break;
		    		case CONFIRMATION:
		    			// Credicard socket basico
		    			confByteResponse = TransactionFactory.sendMsgAut(confIsoMsg, transaction.getTransactionType().getTransactionConfirmationType().getTimeout(), paymentChannel);
		    			break;
		    		case INVERSE:
		    			// Credicard socket basico
		    			invByteResponse = TransactionFactory.sendMsgAut(invIsoMsg, transaction.getTransactionType().getTransactionInverseType().getTimeout(), paymentChannel);
		    			break;
		    		default:
		    			break;
	       		}
	    		break;
	    	case "REC_MOVISTAR":
	    		switch(sendFuncType) {
		    		case TRANSACTION:
		    			//Movistar socket asincrono	    	
		    			logger.debug("aReqQueue{}es null", aReqQueue == null ? " " : " no ");
		    			byteResponse = trxFact.sendMsgAut(
		    					reqIsoMsg, 
		    					transaction.getTransactionType().getTimeout(), 
		    					nonClosingAsyncTCP.getTcpSocket(), 
		    					transaction.getId(), 
		    					aReqQueue,
		    					asyncExecSer,
		    					requestData);
		    			break;
		    		case CONFIRMATION:
		    			//Movistar socket asincrono
		    			logger.debug("aReqQueue{}es null", aReqQueue == null ? " " : " no ");
		    			confByteResponse = trxFact.sendMsgAut(
		    					confIsoMsg, 
		    					transaction.getTransactionType().getTransactionConfirmationType().getTimeout(), 
		    					nonClosingAsyncTCP.getTcpSocket(), 
		    					transaction.getId(), 
		    					aReqQueue,
		    					asyncExecSer,
		    					requestData);
		    			break;
		    		case INVERSE:
		    			// Movistar socket asincrono
		    			logger.debug("aReqQueue{}es null", aReqQueue == null ? " " : " no ");
		    			invByteResponse = trxFact.sendMsgAut(
		    					invIsoMsg, 
		    					transaction.getTransactionType().getTransactionInverseType().getTimeout(), 
		    					nonClosingAsyncTCP.getTcpSocket(), 
		    					transaction.getId(), 
		    					aReqQueue,
		    					asyncExecSer,
		    					requestData);
		    			break;
		    		default:
		    			break;
	       		}
	    		break;
	    	default:
	    		
    		break;
    	}
    	
	    switch(sendFuncType) {
	    	case TRANSACTION: return byteResponse;
	    	case CONFIRMATION: return confByteResponse;
	    	case INVERSE: return invByteResponse;
	    	default: return byteResponse;
	    }
    }
    
    private void batchUpload(CurrentBatch currentBatch, TransactionType batchUploadTrxType) throws TransactionException {
    	byte[] byteResponse = null;
    	Session session = null;
    	
    	try {
			session = HibernateUtil.getSessionFactory().openSession();
		} catch (ExceptionInInitializerError err) {
			logger.error(err.toString());
			throw new ServiceUnavailable();
		}
    	
    	//Se obtiene la lista de transacciones del lote
    	String batchUploadTrxs = 							
				"from Transaction as t "
				+ "where t.currentBatch.id = :currentBatch "
				+ "and t.transactionType.uploadableType = :uploadable "
				+ "and (t.transactionStatus = :tStatus0 or t.transactionStatus = :tStatus7)"
				//y no reversadas
				;
		ArrayList<Transaction> transList = (ArrayList<Transaction>)session.createQuery(batchUploadTrxs)
			.setInteger("currentBatch", currentBatch.getId())
			.setBoolean("uploadable", true)
			.setInteger("tStatus0", 0)
			.setInteger("tStatus7", 7)
			.list();
    	//Set<Transaction> transSet = currentBatch.getTransactions();
    	//ArrayList<Transaction> transList =  new ArrayList<Transaction>();
    	//transSet.toArray(new Transaction[transSet.size()]);
    	//transList.addAll(transSet);
		logger.debug("Lista de transacciones para batch upload es de tamano {}", transList.size());
//		transList.forEach(trx->{
//			logger.debug("{}", trx);		
//		});
    	if (transList.size() > 0) {
			TransactionFactory transactionFactory = new TransactionFactory();
			Transaction transactionAux = transList.get(0);
			TransactionType transactionType = transactionAux != null ? transactionAux.getTransactionType() : null;
			PaymentChannel paymentChannel = transactionType != null
					? transactionType.getTemplateMessage().getPaymentChannel() : null;
			Terminal terminal = transactionAux != null ? transactionAux.getTerminal() : null;
			TransactionData requestData = new TransactionData();
			Transaction transaction = null;
			for (Transaction batchTrx : transList) {
				//AssortedParameters resParams = transactionFactory.getTransactionParameters(resIsoMsg, batchUploadTrxType.getTransactionResponseType().getIsoTemplate(), transaction);	
				if ((batchTrx.getTransactionType().isUploadableType()) 
						&& (batchTrx.getTransactionStatus().equals(TransactionStatus.CONFIRMED) || batchTrx.getTransactionStatus().equals(TransactionStatus.COMPLETED))
						&& (batchTrx.getTransactionResponse() != null)) {
					if (batchTrx.getTransactionResponse().isApprovalCode(batchTrx.getTransactionType())) {
						//Se crea el objeto transaccion para la funcion, aunque no se almacenara
						transaction = new Transaction(terminal, batchUploadTrxType);
						//Se crea el objeto iso
						IsoMessage reqIsoMsg = (IsoMessage) transactionFactory.buildTransaction(batchUploadTrxType,
								paymentChannel, transactionAux.getTerminal(), requestData, transaction, batchTrx);
						logger.debug("Batch upload de transaccion {}", batchTrx.getId());
						//TransactionFactory.printIsoMessage(reqIsoMsg);
						//Se envia la transaccion
						byteResponse = TransactionFactory.sendMsgAut(reqIsoMsg, batchUploadTrxType.getTimeout(),paymentChannel);
						// Procesamiento de la respuesta
						IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction, true);
						//IsoMessage resIsoMsg = transactionFactory.parseMessage(byteResponse, transaction.getTransactionType());
						TransactionFactory.printIsoMessage(resIsoMsg);
						// Determinar el tipo de respuesta
						TransactionResponse transactionResponse = transactionFactory.getTransactionResponse(resIsoMsg,
								batchUploadTrxType.getTransactionResponseType().getIsoTemplate(), paymentChannel,
								transaction);
						logger.debug("Respuesta de batch upload {} {}", batchTrx.getId(), transactionResponse);
					}
				}
			} 
		}
    	
    }
    
    public void closeBatch(Session session, Terminal terminal, CurrentBatch trxCurrentBatch) {
		//Crear el batch history
		logger.debug("Crear el batch history");
		Integer refCount = trxCurrentBatch.getReferenceCounter();
		BatchHistory batchHistory = new BatchHistory(
				terminal, 
				trxCurrentBatch.getBatchNumber(),
				trxCurrentBatch.getTotalAmount(),
				trxCurrentBatch.getTotalTrx(),
				trxCurrentBatch.getBatchType(),
				Calendar.getInstance());
		logger.debug("Almacenar el batch history");
		session.beginTransaction();
		session.save(batchHistory);
		session.getTransaction().commit();
		
		//Actualizar las transacciones
		logger.debug("Actualizar las transacciones");
		String trxPrevQuery = 							
		"select transaction "
		+ "from Transaction as transaction "
		+ "where transaction.currentBatch = :currentBatch "
		;
		ArrayList<Transaction> transList = (ArrayList<Transaction>)session.createQuery(trxPrevQuery)
		.setEntity("currentBatch", trxCurrentBatch)
		.list();
		transList.forEach(trx->{
			trx.setBatchHistory(batchHistory);
			trx.setCurrentBatch(null);
		});
		
		//Actualizar el current batch
		logger.debug("Actualizar el lote actual");
		trxCurrentBatch.setBatchNumber(trxCurrentBatch.getBatchNumber()+1);
		trxCurrentBatch.setReferenceCounter(refCount);
		trxCurrentBatch.setTotalAmount(0.0);
		trxCurrentBatch.setTotalTrx(0);
		
		session.beginTransaction();
		session.update(trxCurrentBatch);
		session.getTransaction().commit();
    }
    
    public void inverseUpdateProc(Terminal terminal, Session session) {
    	Transaction lastConf = terminal.getLastConfTrx();
		if(!lastConf.getTransactionStatus().equals(TransactionStatus.CONFIRMED)) {
			boolean firstInverted = lastConf.getTransactionStatus().equals(TransactionStatus.REVERSED) ? false : true;
			lastConf.setTransactionStatus(TransactionStatus.REVERSED);
			terminal.setPendingReverse(false);
			session.beginTransaction();
			session.update(lastConf);
			session.update(terminal);
			session.getTransaction().commit();
			if(lastConf.getTransactionResponse() != null && lastConf.getTransactionResponse().isApprovedCode() && firstInverted) {
				CurrentBatch cb = lastConf.getCurrentBatch();
				cb.setTotalAmount(cb.getTotalAmount()-lastConf.getAmount());
				cb.setTotalTrx(cb.getTotalTrx()-1);
				session.beginTransaction();
				session.update(cb);
				session.getTransaction().commit();
			}
		}
    }
}

