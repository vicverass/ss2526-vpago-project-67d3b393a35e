package vpago.core.transactions;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.FunctionData;
import vpago.core.transactions.classes.FunctionResponse;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.types.TransactionStatus;
import vpago.core.transactions.exceptions.ServiceUnavailable;

/**
 * Clase que posee funciones no transaccionales
 */
public class FunctionFactory {
	private static final Logger logger = LoggerFactory.getLogger(FunctionFactory.class);
	
	
	public FunctionResponse summaryFunction(Terminal terminal, FunctionData functionData, Session session) throws ServiceUnavailable {
		HashMap<String, String> functionParams = new HashMap<String, String>();
		Calendar calNow = Calendar.getInstance();
		DateFormat timeStamp = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

		for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
			functionParams.put(
					currentBatch.getBatchType().getTypeName().toLowerCase() + "_total_amount",
					String.valueOf(currentBatch.getTotalAmount()));
			functionParams.put(
					currentBatch.getBatchType().getTypeName().toLowerCase() + "_total_trx",
					String.valueOf(currentBatch.getTotalTrx()));
			functionParams.put(
					currentBatch.getBatchType().getTypeName().toLowerCase() + "_batch_number",
					String.valueOf(currentBatch.getBatchNumber()));
		}
		functionParams.put("merchant_name", terminal.getAffiliation().getMerchant().getName());
		functionParams.put("merchant_rif", terminal.getAffiliation().getMerchant().getRif());
		functionParams.put("timestamp", timeStamp.format(calNow.getTime()));
		return new FunctionResponse(functionParams);
	}
}
