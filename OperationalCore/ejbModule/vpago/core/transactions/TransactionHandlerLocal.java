package vpago.core.transactions;

import javax.ejb.Local;

import vpago.core.transactions.classes.TransactionData;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.exceptions.InvalidTransactionType;
import vpago.core.transactions.exceptions.NonExistentPaymentChannel;
import vpago.core.transactions.exceptions.NonExistentTerminal;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.exceptions.TransactionException;
import vpago.core.transactions.classes.CoreResponse;
import vpago.core.transactions.classes.FunctionData;
import vpago.core.transactions.classes.FunctionResponse;

/**
 * Interfaz local para el bean de sesion TransactionHandler
 */
@Local
public interface TransactionHandlerLocal {
	
	public CoreResponse executeTransaction(TransactionData requestData) throws TransactionException;
	public CoreResponse executeTransaction(TransactionData requestData, Integer prevTranId) throws TransactionException;
	public void executeInverse(TransactionData requestData, TransactionType inverseTransactionType) throws Exception;
	public void executeLateInverse(TransactionData requestData, TransactionType inverseTransactionType, Integer prevTranId) throws Exception;
	public void executeInverse(TransactionData requestData, TransactionType inverseTransactionType, Integer prevTranId) throws Exception;
    public TransactionType getTransactionInverse(String transactionTypeName, String channelCode) throws NonExistentPaymentChannel, InvalidTransactionType, ServiceUnavailable;
    public Integer getIdTransactionFromRef(String terminalGuid, String referenceNum, TransactionData transactionData, String channelCode) throws NonExistentPaymentChannel, NonExistentTerminal, ServiceUnavailable;
    public FunctionResponse executeFunction(FunctionData functionData) throws TransactionException;
}
