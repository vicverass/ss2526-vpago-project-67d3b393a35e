package vpago.core.transactions.queues;

import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Local;

@Local
public interface ConfirmationQueueLocal {

	public ConcurrentHashMap<String, ConfirmationQueueElem> getcQueue();

}
