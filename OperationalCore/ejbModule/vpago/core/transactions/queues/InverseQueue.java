package vpago.core.transactions.queues;

import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class InverseQueue
 */
@Singleton
@LocalBean
public class InverseQueue implements InverseQueueRemote, InverseQueueLocal {

    /**
     * Default constructor. 
     */
    public InverseQueue() {
        this.iQueue = new ConcurrentHashMap<String, InverseQueueElem>();
    }
    
    private ConcurrentHashMap<String, InverseQueueElem> iQueue;
    
	/**
	 * @return the iQueue
	 */
	public ConcurrentHashMap<String, InverseQueueElem> getiQueue() {
		return iQueue;
	}

	/**
	 * @param iQueue the iQueue to set
	 */
	public void setiQueue(ConcurrentHashMap<String, InverseQueueElem> iQueue) {
		this.iQueue = iQueue;
	}

    
}
