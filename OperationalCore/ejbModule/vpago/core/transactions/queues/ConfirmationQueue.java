package vpago.core.transactions.queues;

import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class ConfirmationQueue
 */
@Singleton
@LocalBean
public class ConfirmationQueue implements ConfirmationQueueRemote, ConfirmationQueueLocal {

    /**
     * Default constructor. 
     */
    public ConfirmationQueue() {
    	this.cQueue = new ConcurrentHashMap<String, ConfirmationQueueElem>();
    }
    
    private ConcurrentHashMap<String, ConfirmationQueueElem> cQueue;

	/**
	 * @return the cQueue
	 */
	public ConcurrentHashMap<String, ConfirmationQueueElem> getcQueue() {
		return cQueue;
	}

	/**
	 * @param cQueue the cQueue to set
	 */
	public void setcQueue(ConcurrentHashMap<String, ConfirmationQueueElem> cQueue) {
		this.cQueue = cQueue;
	}
    
    

}
