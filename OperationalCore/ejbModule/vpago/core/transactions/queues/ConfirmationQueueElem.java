package vpago.core.transactions.queues;

import com.solab.iso8583.IsoMessage;

import vpago.core.transactions.classes.Transaction;

public class ConfirmationQueueElem {
	
	public ConfirmationQueueElem(IsoMessage isoMessage, double timeout, Transaction transaction) {
		super();
		this.isoMessage = isoMessage;
		this.timeout = timeout;
		this.transaction = transaction;
	}

	private IsoMessage isoMessage;
	
	private double timeout;
	
	private Transaction transaction;

	/**
	 * @return the isoMessage
	 */
	public IsoMessage getIsoMessage() {
		return isoMessage;
	}

	/**
	 * @param isoMessage the isoMessage to set
	 */
	public void setIsoMessage(IsoMessage isoMessage) {
		this.isoMessage = isoMessage;
	}

	/**
	 * @return the timeout
	 */
	public double getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(double timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	
}
