package vpago.core.transactions;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.Checksum;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoValue;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.codecs.CompositeField;

import cryptography.bookstore.DataBlock;
import vpago.core.config.EngineProperties;
import vpago.core.db.HibernateUtil;
import vpago.core.transactions.classes.Acquirer;
import vpago.core.transactions.classes.IsoTemplate;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.Rule;
import vpago.core.transactions.classes.RuleProduct;
import vpago.core.transactions.classes.RuleProductGeneral;
import vpago.core.transactions.classes.RuleSet;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionData;
import vpago.core.transactions.classes.TransactionParameter;
import vpago.core.transactions.classes.TransactionResponse;
import vpago.core.transactions.classes.TransactionResponseParameter;
import vpago.core.transactions.classes.AssortedParameters;
import vpago.core.transactions.classes.BatchType;
import vpago.core.transactions.classes.CompositeFieldInfo;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Hsm;
import vpago.core.transactions.classes.HsmTpdu;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.classes.types.FieldLoadType;
import vpago.core.transactions.connections.AsyncReadFuture;
import vpago.core.transactions.connections.AsyncReqElem;
import vpago.core.transactions.connections.AsyncReqQueueLocal;
import vpago.core.transactions.connections.ReadExecutorServiceLocal;
import vpago.core.transactions.exceptions.ConnectionFailure;
import vpago.core.transactions.exceptions.NoResponse;
import vpago.core.transactions.exceptions.ServiceUnavailable;
import vpago.core.transactions.exceptions.TransactionException;
import vpago.core.transactions.util.TraceGenerator;
import vpago.core.transactions.classes.TemplateField;
import vpago.core.transactions.classes.TemplateSubField;

/**
 * Clase que posee las funciones para construir y analizar tramas de transacciones
 */
public class TransactionFactory {
	private static final Logger logger = LoggerFactory.getLogger(TransactionFactory.class);
	
	/** Objeto de tipo IsoMessage que representa un mensaje ISO8583 */
	private IsoMessage isoMessage;
	/** Estructura que almacena los subcampos de un campo ISO */
	private CompositeField compField;
	/** Almacena informacion sobre el campo compuesto **/
	private CompositeFieldInfo compFieldI;
	/** Conjunto de reglas que aplican en un rango de tiempo determinado */
	private Set<Rule> chosenRules;
	/** Regla elegida para ser aplicada a un campo */
	private Rule chosenRule;
	/** Variable para almacenar un valor temporal */
	private String tempValue;
	/** Variable para almacenar la configuración del HSM */
	private static Hsm hsm;
	/** Lista de TPDU acordes al HSM */
	private static List<HsmTpdu> hsmTpdu;
	
		
	/**
	 * Construye una transaccion en formato ISO8583
	 * 
	 * @param transactionType Tipo de Transaccion 
	 * 
	 * @param paymentChannel Canal de Pago al que esta asociada la transaccion
	 * 
	 * @param terminal Terminal que origino la Transaccion
	 * 
	 * @param transactionData Datos de la solicitud de ejecucion de transaccion
	 * 
	 * @param transaction Transaccion
	 * 
	 * @return IsoMessage como Object con el resultado de armar la transaccion
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	public Object buildTransaction(TransactionType transactionType, 
								PaymentChannel paymentChannel,
								Terminal terminal,
								TransactionData transactionData, 
								Transaction transaction, 
								Transaction prevTransaction) throws TransactionException {
		Object isoFrame = null;
		try {
			//Si no se recibio una transaccion previa utilizar la ultima reversible del terminal
			if(prevTransaction == null) {
				logger.debug("buildTransaction: No se recibio una transaccion previa utilizar la ultima reversible del terminal");
				prevTransaction = transaction.getTerminal().getLastRevTrx();
				logger.debug("prevTransaction {}", prevTransaction);
			}
			// Se invoca la primitiva configurada para el Tipo de Transaccion (TransactionType) indicado
			isoFrame = TransactionFactory.class.getDeclaredMethod(transactionType.getMethodName(), new Class[]{IsoTemplate.class, TransactionData.class, Transaction.class, Transaction.class})
			.invoke(new TransactionFactory(), transactionType.getIsoTemplate(), transactionData, transaction, prevTransaction);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		} catch ( Exception e) {
			logger.error(e.toString());
			throw e;
		} 
		return isoFrame;
	}
	
	/**
	 * Primitiva para transacciones de compra a credito
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de 
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage creditSale(
				IsoTemplate isoTemplate, 
				TransactionData transactionData, 
				Transaction transaction,
				Transaction prevTransaction) throws TransactionException {
			try {
				int type = isoTemplate.getTxnType();
				Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
				
				MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
				//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
				reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
				reqMsgFactory.setIgnoreLastMissingField(true);
				reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
				reqMsgFactory.setAssignDate(isoTemplate.isDatetimeField()); 
				//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
				// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
				isoMessage = reqMsgFactory.newMessage(type);
				isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
					isoMessage = setMessageField(
							paramName, 
							templateField, 
							isoMessage, 
							isoTemplate, 
							transactionData, 
							transaction,
							prevTransaction);
				});
			} catch (Exception e) {
				logger.error("Error en la configuracion de la transaccion: {}", e.toString());
				throw new TransactionException();
			}
			return isoMessage;
	}
		
	/**
	 * Primitiva para transacciones de Test de Conexion
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de 
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage echoTest(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
		try {
			int type = isoTemplate.getTxnType();
			Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
			
			MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
			//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
			reqMsgFactory.setIgnoreLastMissingField(true);
			reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
			//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
			// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
			isoMessage = reqMsgFactory.newMessage(type);
			isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
				isoMessage = setMessageField(
						paramName, 
						templateField, 
						isoMessage, 
						isoTemplate, 
						transactionData, 
						transaction,
						prevTransaction);
			});
		} catch (Exception e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessage;
	}
	
	/**
	 * Primitiva para transacciones de Reverso automatico
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de 
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage reverse(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
			try {
				
				int type = isoTemplate.getTxnType();
				Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
				
				MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
				//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
				reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
				reqMsgFactory.setIgnoreLastMissingField(true);
				reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
				reqMsgFactory.setAssignDate(isoTemplate.isDatetimeField());
				//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
				// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
				isoMessage = reqMsgFactory.newMessage(type);
				isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
					isoMessage = setMessageField(
							paramName, 
							templateField, 
							isoMessage, 
							isoTemplate, 
							transactionData, 
							transaction,
							prevTransaction);
				});
			} catch (Exception e) {
				logger.error("Error en la configuracion de la transaccion: {}", e.toString());
				throw new TransactionException();
			}
			return isoMessage;
	}
	
	/**
	 * Primitiva para transacciones Advice
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de transacciones
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage advice(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
		try {
			int type = isoTemplate.getTxnType();
			Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
			
			MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
			//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			//reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
			reqMsgFactory.setIgnoreLastMissingField(true);
			reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
			//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
			// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
			isoMessage = reqMsgFactory.newMessage(type);
			isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
				isoMessage = setMessageField(
						paramName, 
						templateField, 
						isoMessage, 
						isoTemplate, 
						transactionData, 
						transaction,
						prevTransaction);
			});
		} catch (Exception e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessage;
	}
	
	/**
	 * Primitiva para transacciones de anulacion
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de transacciones
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage annulment(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
		try {
			int type = isoTemplate.getTxnType();
			Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
			transaction.setAmount(prevTransaction != null ? prevTransaction.getAmount() : 0.0);
			
			MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
			//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			//reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
			reqMsgFactory.setIgnoreLastMissingField(true);
			reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
			//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
			// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
			isoMessage = reqMsgFactory.newMessage(type);
			isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
				isoMessage = setMessageField(
						paramName, 
						templateField, 
						isoMessage, 
						isoTemplate, 
						transactionData, 
						transaction,
						prevTransaction);
			});
		} catch (Exception e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessage;
	}
	
	@SuppressWarnings("unused")
	private IsoMessage settlement(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
		try {
			int type = isoTemplate.getTxnType();
			Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
			
			MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
			//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
			reqMsgFactory.setIgnoreLastMissingField(true);
			reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
			//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
			// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
			isoMessage = reqMsgFactory.newMessage(type);
			isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
				isoMessage = setMessageField(
						paramName, 
						templateField, 
						isoMessage, 
						isoTemplate, 
						transactionData, 
						transaction,
						prevTransaction);
			});
		} catch (Exception e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessage;
	}
	
	/**
	 * Primitiva para transacciones Batch Upload
	 * 
	 * @param isoTemplate Plantilla ISO utilizada para construir la trama para este tipo de transacciones
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Objeto que representa la transaccion actual
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	@SuppressWarnings("unused")
	private IsoMessage batchUpload(
			IsoTemplate isoTemplate, 
			TransactionData transactionData, 
			Transaction transaction,
			Transaction prevTransaction) throws TransactionException {
		try {
			int type = isoTemplate.getTxnType();
			Map<String, TemplateField> wsFields = isoTemplate.getTemplate_field();
			
			MessageFactory<IsoMessage> reqMsgFactory = new MessageFactory<IsoMessage>();
			//reqMsgFactory.setTraceNumberGenerator(new SimpleTraceGenerator((int) (System.currentTimeMillis() % 100000)));
			reqMsgFactory.setTraceNumberGenerator(new TraceGenerator(transaction.getTerminal()));
			reqMsgFactory.setIgnoreLastMissingField(true);
			reqMsgFactory.setIsoHeader(type, isoTemplate.getHeader());
			//messageFactory.setParseMap(type, isoTemplate.getFieldParseInfoMap());
			// Se configura un IsoMessage a partir de los datos contenidos en un IsoTemplate
			isoMessage = reqMsgFactory.newMessage(type);
			isoTemplate.getTemplate_field().forEach((paramName, templateField) -> { 
				isoMessage = setMessageField(
						paramName, 
						templateField, 
						isoMessage, 
						isoTemplate, 
						transactionData, 
						transaction,
						prevTransaction);
			});
		} catch (Exception e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessage;
	}
	
	/**
	 * Convierte la trama asociada a una transaccion a una estructura que con los campos ISO8583
	 * 
	 * @param msgResponse Trama a analizar
	 * 
	 * @param transaction Transaccion a la que esta asociada la trama
	 * 
	 * @param preProcess Indica si la trama debe ser preprocesada antes del analisis
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	public IsoMessage parseMessage(byte[] msgResponse, Transaction transaction, boolean preProcess) throws TransactionException {
		//Parsear la respuesta
		IsoMessage isoMessageR = null;
		MessageFactory<IsoMessage> resMsgFactory = new MessageFactory<IsoMessage>();
		TransactionType responseType = transaction.getTransactionType().getTransactionResponseType();
		IsoTemplate responseTemplate = responseType.getIsoTemplate();
		logger.debug("responseTemplate {}", responseTemplate);
		resMsgFactory.setIsoHeader(responseTemplate.getTxnType(), responseTemplate.getHeader());
		resMsgFactory.setParseMap(responseTemplate.getTxnType(), responseTemplate.getFieldParseInfoMap());
		// PREPROCESAMIENTO TRAMA
		if(preProcess) msgResponse = preProcessingMsg(msgResponse, responseTemplate);
		
		try {
			logger.trace("msgResponse: {}", new String(msgResponse));
			isoMessageR = resMsgFactory.parseMessage(msgResponse, responseTemplate.getHeaderLength());
			//printIsoMessage(isoMessageR);
		} catch (UnsupportedEncodingException | ParseException e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessageR;
	}
	
	public IsoMessage parseMessage(byte[] msgResponse, TransactionType transactionType, boolean preProcess) throws TransactionException {
		logger.debug("parseMessage(byte[] msgResponse, TransactionType transactionType)");
		//Parsear la respuesta
		IsoMessage isoMessageR = null;
		MessageFactory<IsoMessage> resMsgFactory = new MessageFactory<IsoMessage>();
		IsoTemplate responseTemplate = transactionType.getIsoTemplate();
		logger.debug("responseTemplate {}", responseTemplate);
		resMsgFactory.setIsoHeader(responseTemplate.getTxnType(), responseTemplate.getHeader());
		resMsgFactory.setParseMap(responseTemplate.getTxnType(), responseTemplate.getFieldParseInfoMap());
		// PREPROCESAMIENTO TRAMA
		if(preProcess) msgResponse = preProcessingMsg(msgResponse, responseTemplate);
		
		try {
			logger.trace("msgResponse: {}", new String(msgResponse));
			isoMessageR = resMsgFactory.parseMessage(msgResponse, responseTemplate.getHeaderLength());
			//printIsoMessage(isoMessageR);
		} catch (UnsupportedEncodingException | ParseException e) {
			logger.error("Error en la configuracion de la transaccion: {}", e.toString());
			throw new TransactionException();
		}
		return isoMessageR;
	}
	
	public IsoMessage parseMessage(byte[] msgResponse, ArrayList<TransactionType> transactionTypeL) throws TransactionException {
		IsoMessage isoMessage = null;
		for(TransactionType transactionType : transactionTypeL) {
			logger.debug("Parsing with transactionType {}", transactionType.getName());
			try {
				isoMessage = parseMessage(msgResponse, transactionType, false);
			} catch (TransactionException e) {
				logger.debug("Not the parsing template, testing next...");;
			} catch (Exception e) {
				logger.error("Exception parsing in reader thread");
			}
			if(isoMessage != null) {
				logger.debug("After parsing with transactionType {}", isoMessage.debugString());
				return isoMessage;
			}
		}
		return isoMessage;
	}
	
	/**
	 * Construye una estructura donde se asocia el nombre de un campo ISO segun la plantilla con su valor en una 
	 * transaccion determinada
	 * 
	 * @param trxIso Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @param template Plantilla asociada a la transaccion representada por trxIso
	 * 
	 * @return Estructura con los parametros clasificados de acuerdo a los nombres de los campos
	 * 
	 * @throws TransactionException Excepcion en transaccion
	 */
	public AssortedParameters getTransactionParameters(IsoMessage trxIso, IsoTemplate template, Transaction transaction) throws TransactionException {
		AssortedParameters parameters = new AssortedParameters();
		ClassLoader classLoader = TransactionFactory.class.getClassLoader();
		//Set<TransactionParameter> parameters = new HashSet<TransactionParameter>(); 
		Set<Map.Entry<String, TemplateField>> templateFields = template.getTemplate_field().entrySet();
		Set<Map.Entry<String, TemplateSubField>> templateSubFields = template.getTemplate_subfield().entrySet();
		logger.debug("templateSubFields.size() {}", templateSubFields.size());
		Iterator<Entry<String, TemplateField>> templateFieldIt = templateFields.iterator();
		Iterator<Entry<String, TemplateSubField>> templateSFieldIt = templateSubFields.iterator();
		Map.Entry<String, TemplateField> templateEntry;
		Map.Entry<String, TemplateSubField> templateSEntry;
		Object value = null;
		int isoId;
		String isoKey, isoValue = null;
		TemplateField field;
		TemplateSubField sField;
		Class<?> extClass = null;
		while(templateFieldIt.hasNext()) {
			templateEntry = templateFieldIt.next();
			field = templateEntry.getValue();
			if((field.isStorable() || field.isReturnable()) && (trxIso.getField(field.getFieldId()) != null)) {
				try {
					extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(template.getSubpackageName().concat(".").concat(field.getClassName())));
				} catch (ClassNotFoundException e) {
					logger.error("Error en la configuracion de la transaccion: {}", e.toString());
					throw new TransactionException();
				}
				isoId = field.getFieldId();
				isoKey = templateEntry.getKey();
				value = trxIso.getField(isoId).getValue();
				logger.trace("isoId: {} - isoKey: {} - value: {}", new Object[]{isoId, isoKey, value});
				try {
					//Aplica el metodo decodificador configurado para el campo
					isoValue = templateEntry.getValue().getDecoder() == null ? value.toString() : 
																			   extClass.getMethod(templateEntry.getValue().getDecoder(), Terminal.class, Object.class, Map.class)
																			   .invoke(null, transaction.getTerminal(), value, null).toString();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					logger.error("Error en la configuracion de la transaccion: {}", e.toString());
					e.printStackTrace();
					throw new TransactionException();
				}
				if(field.isStorable()) {
					try {
						//Si tiene subcampos
						if(field.getLoadType().equals(FieldLoadType.SUBFIELD)) {
							isoValue=(String) value;
							// TODO Pruebas Movistar: por que comentado??? Porque da error D: Arreglalo!!!!
//							CompositeField compField = (CompositeField) value;
//							logger.debug("Almacenamiento de subcampos de {}", field.getFieldId());
//							for(IsoValue subValue : compField.getValues()) {
//								logger.debug("{} ", subValue.getValue().toString());
//								isoValue+= subValue.getValue().toString();
//							}
							//Comentado hasta aca
							logger.trace("Campo {} compuesto: {}", field.getFieldId(), isoValue);
						}
						logger.trace("Funcion de almacenamiento: {}", templateEntry.getValue().getStoreFunc());
						//Aplica la funcion de almacenamiento configurada para el campo
						isoValue = templateEntry.getValue().getStoreFunc() == null ? isoValue.toString() : 																			   
								extClass.getMethod(templateEntry.getValue().getStoreFunc(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), isoValue, null).toString();				
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						logger.error("Error en la configuracion de la transaccion: {}", e.toString());
						throw new TransactionException();
					}
					parameters.getStorableParams().put(isoKey, isoValue);
				} 
				if(field.isReturnable()) {
					parameters.getReturnableParams().put(isoKey, isoValue);
				}
			}
		}
		//Subcampos
		while(templateSFieldIt.hasNext()) {
			templateSEntry = templateSFieldIt.next();
			sField = templateSEntry.getValue();
			if((sField.isStorable() || sField.isReturnable()) && (trxIso.getField(sField.getFieldId()) != null)) {
				try {
					extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(template.getSubpackageName().concat(".").concat(sField.getClassName())));
				} catch (ClassNotFoundException e) {
					logger.error("Error en la configuracion de la transaccion: {}", e.toString());
					throw new TransactionException();
				}
				isoId = sField.getFieldId();
				isoKey = templateSEntry.getKey();
				value = trxIso.getField(isoId).getValue();
				logger.debug("Subcampo isoId: {} - isoKey: {} - value: {}", new Object[]{isoId, isoKey, value});
				try {
					//Aplica el metodo decodificador configurado para el campo
					isoValue = templateSEntry.getValue().getDecoder() == null ? value.toString() : 
																			   extClass.getMethod(templateSEntry.getValue().getDecoder(), Terminal.class, Object.class, Map.class)
																			   .invoke(null, transaction.getTerminal(), value, null).toString();
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					logger.error("Error en la configuracion de la transaccion: {}", e.toString());
					e.printStackTrace();
					throw new TransactionException();
				}
				if(sField.isStorable()) {
					try {
						logger.trace("Funcion de almacenamiento: {}", templateSEntry.getValue().getStoreFunc());
						//Aplica la funcion de almacenamiento configurada para el campo
						isoValue = templateSEntry.getValue().getStoreFunc() == null ? isoValue.toString() : 																			   
								extClass.getMethod(templateSEntry.getValue().getStoreFunc(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), isoValue, null).toString();				
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						logger.error("Error en la configuracion de la transaccion: {}", e.toString());
						throw new TransactionException();
					}
					parameters.getStorableParams().put(isoKey, isoValue);
				} 
				if(sField.isReturnable()) {
					parameters.getReturnableParams().put(isoKey, isoValue);
				}
			}
		}
		parameters.getStorableParams().forEach((key, parameter) -> { logger.debug("S {} {}", key, parameter);});
		parameters.getReturnableParams().forEach((key, parameter) -> { logger.debug("R {} {}", key, parameter);});
		return parameters;
	}
	
	/**
	 * Resuelve el tipo de respuesta recibido por una transaccion a partir de la plantilla asociada al tipo de 
	 * transaccion enviada
	 * 
	 * @param trxIso Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @param template Plantilla asociada a la transaccion representada por trxIso
	 * 
	 * @param paymentChannel Canal de pago al que esta asociada la transaccion
	 * 
	 * @return Tipo de respuesta recibida para la transaccion
	 * 
	 * @throws TransactionException  Excepcion en transaccion
	 */
	public TransactionResponse getTransactionResponse(
			IsoMessage trxIso, 
			IsoTemplate template, 
			PaymentChannel paymentChannel, 
			Transaction transaction) throws TransactionException {
		ClassLoader classLoader = TransactionFactory.class.getClassLoader();
		Class<?> extClass = null;
		String isoValue = null;
		logger.debug("trxIso de getTransactionResponse {}", trxIso.debugString());
		Object value = trxIso.getField(39).getValue();
		TemplateField responseField = template.getIsoIdMap().getOrDefault(39, null);
		TransactionResponse transactionResponse = null;
		logger.trace("getTransactionResponse value {}", value);
		logger.trace("getTransactionResponse responseField {}", responseField);
		if((responseField != null) && (value != null)) {
			try {
				extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(template.getSubpackageName().concat(".").concat(responseField.getClassName())));
				try {
					//Aplica el metodo decodificador configurado para el campo
					isoValue = responseField.getDecoder() == null ? value.toString() : 
						   extClass.getMethod(responseField.getDecoder(), Terminal.class, Object.class, Map.class)
						   .invoke(null, transaction.getTerminal(), value, null).toString();
					transactionResponse = paymentChannel.getTransactionRespTypes().getOrDefault(isoValue, null);
					logger.trace("getTransactionResponse value {}", value);
					logger.trace("getTransactionResponse responseField {}", responseField);
					if(transactionResponse == null) paymentChannel.getTransactionRespTypes().getOrDefault("", null); // TODO (?)		
					logger.trace("Tipo de respuesta: {}", transactionResponse);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
						| NoSuchMethodException | SecurityException e) {
					logger.error("Error en la configuracion de la transaccion: {}", e.toString());
					throw new TransactionException();
				}
			} catch (ClassNotFoundException e) {
				logger.error("Error en la configuracion de la transaccion: {}", e.toString());
				throw new TransactionException();
			}
			
		}
		return transactionResponse;
	}
	
	/**
	 * Establece un campo de la estructura que sera transformada en la trama, esto se realiza basandose en la 
	 * configuracion presente para dicho campo
	 * 
	 * @param paramName Nombre del campo
	 * 
	 * @param field Plantilla de campo
	 * 
	 * @param isoMessage Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 * 
	 * @param isoTemplate Plantilla de la transaccion a construir
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Transaccion
	 * 
	 * @return Un objeto de tipo IsoMessage que representa un mensaje ISO8583
	 */
	private IsoMessage setMessageField(String paramName, 
									   TemplateField field, 
									   IsoMessage isoMessage, 
									   IsoTemplate isoTemplate, 
									   TransactionData transactionData,
									   Transaction transaction,
									   Transaction prevTransaction){
		try {
			ClassLoader classLoader = TransactionFactory.class.getClassLoader();
			Class<?> extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(isoTemplate.getSubpackageName().concat(".").concat(field.getClassName())));
			Map<String, String> transactionParam = transactionData.getTransactionParameters();
			logger.trace("{}: {}", paramName, field);
			Object value = null;
			//Cargar ultima transaccion aprobada
			Terminal terminal = transaction.getTerminal();
			Transaction lastApprv = terminal.getLastApprvTrx();
			Transaction lastExec = terminal.getLastExecTrx();
			logger.trace("{}: {} {}", new Object[]{paramName, field.getFieldId(), field.getLoadType()});
			switch(field.getLoadType()) {
			case DATABASE:
				try {
					String paramValue = null;
					TransactionParameter transactionParameter = null;
					Iterator<TransactionParameter> iterator = transaction.getParameters().iterator();
					while((iterator.hasNext()) && (paramValue == null)) {
						transactionParameter = iterator.next();
						logger.trace("{}", transactionParameter);
						paramValue = transactionParameter.getParamName() == paramName ? transactionParameter.getParamValue() : null;
					}
					//Aplicar el metodo codificador configurado para el campo
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case WEBSERVICE:
				try {
						//Aplicar el metodo codificador configurado para el campo
						value = transactionParam.containsKey(paramName) ? 
								field.getEncoder() == null ? 
								transactionParam.get(paramName)
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), transactionParam.get(paramName), transactionParam) 
								: null;
						isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case FIXED:
				try {
					//Aplicar el metodo codificador configurado para el campo
						value = field.getEncoder() == null ? 
								field.getFixedValue()
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), field.getFixedValue(), transactionParam);
						isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case COMPUTED:
				try {
						//Aplicar la funcion de carga para obtener el valor del campo
						value = field.getLoadFunc() != null ? 
								extClass.getMethod(field.getLoadFunc(), Terminal.class, Map.class)
								.invoke(null, transaction.getTerminal(), transactionParam)
								: null;
						//Aplicar la funcion codificadora configurada para el campo
						value = field.getEncoder() == null ? value 
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), value, transactionParam); 
						isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case SUBFIELD:
				try {
					// Se obtiene las plantillas de subcampos para un campo ISO
					List<TemplateSubField> subList = field.getSubFields();
					logger.trace("subList -> {}", subList);
					compField = null;
					compFieldI = null;
					// Si la lista de subcampos no esta vacia se procede a establecerlos en el mensaje
					if (!subList.isEmpty()) {
						logger.trace("Lista no vacia");
						compField = compField == null ? new CompositeField() : compField;
						compFieldI = compFieldI == null ? new CompositeFieldInfo(compField) : compFieldI;
						// Se verifica si una plantilla iso contiene una plantilla de subcampo para obtener
						// la clave que permita consultar su valor 
						isoTemplate.getTemplate_subfield().forEach((fieldName, templateSubField) -> {
							if (subList.contains(templateSubField)) {
								logger.trace("Establecer subcampo {}", fieldName);
								compFieldI = setMessageSubField(
										fieldName, 
										templateSubField, 
										compFieldI, 
										isoTemplate, 
										transactionData, 
										transaction,
										prevTransaction);
							}
						});
					}
					logger.debug("Campo compuesto {} con {} elementos y longitud {}", new Object[]{field.getFieldId(), compFieldI.getCompFieldSize(), compFieldI.getTotalLen()});
					isoMessage.setValue(
							field.getFieldId(), 
							compFieldI.getCompField(), 
							compFieldI.getCompField(), 
							field.getFieldType(), 
							compFieldI.getTotalLen());
					value = isoMessage.getField(field.getFieldId()).toString();
					value = field.getEncoder() == null ? value 
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), value, transactionParam); 
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (Exception  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case RULE:
				RuleSet activeRuleSet = transaction.getTerminal().getAffiliation().getActiveRuleSet();
				Calendar currentDate = Calendar.getInstance();
				logger.trace("currentDate: {}", currentDate);
				chosenRules = new HashSet<Rule>();
				transaction.getTerminal().getAffiliation().getRuleSets().forEach((curRuleSet) -> {
					logger.trace("{}", curRuleSet);
					RuleSet chosenRuleSet;
					logger.trace("{}", curRuleSet.getTimeFrame().getStart().getTime());
					logger.trace("{}", curRuleSet.getTimeFrame().getEnd().getTime());
					chosenRuleSet = (currentDate.get(Calendar.DAY_OF_WEEK) == curRuleSet.getTimeFrame().getWeekDay().getDayNumber())
						&& currentDate.after(curRuleSet.getTimeFrame().getStart())
						&& currentDate.before(curRuleSet.getTimeFrame().getEnd()) ? curRuleSet : null;
					if(chosenRuleSet != null) chosenRules = curRuleSet.getRules();
					else chosenRules = activeRuleSet.getRules();
				});
				if((chosenRules.size() == 0)) chosenRules.add((Rule)activeRuleSet.getRules().toArray()[0]); 
				logger.trace("transactionData: {}", transactionData);
				chosenRules.forEach((currentRule) -> {
					logger.trace("- {}", currentRule);
					Integer cardBin = null, startBin = null, endBin = null;
					boolean matchBin = false, foundRule = false;
					if(!foundRule) {
						switch(currentRule.getRuleType()) {
						case "product":
							RuleProduct ruleProduct = (RuleProduct) currentRule;
							cardBin = extractBin(transactionData.getCardNumber());
							startBin = Integer.parseInt(ruleProduct.getBankProduct().getProductBin().getBinStart());
							endBin = Integer.parseInt(ruleProduct.getBankProduct().getProductBin().getBinEnd());
							matchBin = (Integer.compare(cardBin, startBin) >= 0 && Integer.compare(cardBin, endBin) <= 0) ? true : false;
							logger.trace("Card bin: {} - Start bin: {} - End bin: {}", new Object[]{cardBin, startBin, endBin});
							if(matchBin) {
								foundRule = true;
								chosenRule = currentRule;
								logger.trace("Bank Bin match");
							}
							break;
						case "general":
							RuleProductGeneral ruleGeneral = (RuleProductGeneral) currentRule;
							cardBin = extractBin(transactionData.getCardNumber());
							startBin = Integer.parseInt(ruleGeneral.getGeneralProduct().getProductBin().getBinStart());
							endBin = Integer.parseInt(ruleGeneral.getGeneralProduct().getProductBin().getBinEnd());
							matchBin = (Integer.compare(cardBin, startBin) >= 0 && Integer.compare(cardBin, endBin) <= 0) ? true : false;
							logger.trace("Card bin: {} - Start bin: {} - End bin: {}", new Object[]{cardBin, startBin, endBin});
							if(matchBin) {
								foundRule = true;
								chosenRule = currentRule;
								logger.trace("General Bin match");
							}
							break;
						case "proportion":
							//RuleProportion ruleProportion = (RuleProportion) currentRule;
							chosenRule = currentRule;
							break;
						default:
							
							break;
						}
					}
					
				});
				if (chosenRule == null) chosenRule = (Rule) chosenRules.toArray()[0];
				String fieldName= field.getAcquirerAttr();
				try {
					logger.trace("Chosen rule: {}", chosenRule);
					value = Acquirer.class.getDeclaredField(fieldName).get(chosenRule.getAcquirer());	
				} catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
					/* Cuando el atributo no es publico se intenta con el metodo get del atributo bajo las convenciones 
					 * de JavaBeans (las mismas de Hibernate) */
					logger.warn("El atributo {} no existe o no se tienen los permisos para su acceso", fieldName);
					String methodName = "get".concat(fieldName.substring(0, 1).toUpperCase()).concat(fieldName.substring(1));
					logger.trace("Utilizar el metodo get del atributo: {}", methodName);
					try {
						value = Acquirer.class.getDeclaredMethod(methodName, (Class<?>[])null).invoke(chosenRule.getAcquirer(), (Object[])null);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e1) {
						/* Si no se encuentra como propiedad de la clase Adquiriente se busca en los Parametros de Terminal */
						logger.warn("El metodo {} no existe o no se tienen los permisos para su acceso", methodName);
						if (value == null) {
							logger.trace("getTerminal: {}", transaction.getTerminal());
							//logger.trace("getTerminalParameters: {}", transaction.getTerminal().getTerminalParameters());
							tempValue = null;
							transaction.getTerminal().getTerminalParameters().forEach((terminalParameter) -> {
								//logger.trace("{}", terminalParameter);
								tempValue = ((terminalParameter.getName().equals(fieldName)) && 
										(terminalParameter.getAcquirer().equals(chosenRule.getAcquirer()))) ? terminalParameter.getValue(): null;
							});
							value = tempValue != null ? tempValue : null;
						}
					}
				}
				try {
					logger.trace("value: {}", value);
					//Aplicar el metodo codificador configurado para el campo
					value = field.getEncoder() == null ? 
							value
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), value, transactionParam);
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case LASTTRXREQ:
				// TODO validar que la ultima transaccion aprobada no sea null
				try {
					//Aplicar el metodo codificador configurado para el campo
					String paramValue = getSetParameter(lastApprv, paramName);
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case LASTTRXRESP:
				// TODO validar que la ultima transaccion aprobada no sea null
				try {
					//Aplicar el metodo codificador configurado para el campo
					String paramValue = getSetResParameter(lastApprv, paramName);
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case LASTEXECTRXREQ:
				// TODO validar que la ultima transaccion ejecutada no sea null
				try {
					String paramValue = getSetParameter(lastExec, paramName);
					//Aplicar el metodo codificador configurado para el campo 
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case LASTEXECTRXRESP:
				// TODO validar que la ultima transaccion ejecutada no sea null
				try {
					String paramValue = getSetResParameter(lastExec, paramName);
					//Aplicar el metodo codificador configurado para el campo
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case PREVTRXREQ:
				try {
					String paramValue = getSetParameter(prevTransaction, paramName);
					logger.trace("TRX: {} PREVTRXREQ: {} - {}", new Object[]{prevTransaction.getId(), paramName, paramValue});
					//Aplicar el metodo codificador configurado para el campo
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case PREVTRXRESP:
				try {
					String paramValue = getSetResParameter(prevTransaction, paramName);
					//Aplicar el metodo codificador configurado para el campo 
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case BATCH:
				try {
					logger.trace("Tipo de parametro BATCH");
					String paramValue = null;
					terminal = transaction.getTerminal();
					//Para todos los tipos de lotes asociados a la plantilla
					//determinar cual de los pares parametro-valor coinciden con 
					//un parametro presente en la transaccion para asi determinar el tipo de lote
					BatchType trxBatchType = null;
					logger.trace("Numero de batch types {}", transaction.getTransactionType().getTemplateMessage().getPaymentChannel().getBatchTypes().size());
					logger.trace("Numero de parametros {}", transactionData.getTransactionParameters().size());
					trxBatchType = determineTrxBatchT(transaction, transactionData);
					//Si se encuentra un tipo de lote se debe cargar ese tipo de entre los lotes actuales del terminal
					paramValue = loadBatchValue(trxBatchType, terminal);
					//Aplicar el metodo codificador configurado para el campo
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
					logger.trace("BATCH value {}", value);
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			default:
				break;
			}
		
		} catch (ClassNotFoundException e) {
			logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
		}
		return isoMessage;
	}
	
	/**
	 * Establece un subcampo de la estructura que sera transformada en la trama, esto se realiza basandose en la 
	 * configuracion presente para dicho subcampo dentro del campo correspondiente
	 * 
	 * @param paramName Nombre del subcampo
	 * 
	 * @param field Plantilla de subcampo
	 * 
	 * @param compositeField Estructura que almacena los subcampos de un campo ISO
	 * 
	 * @param isoTemplate Plantilla de la transaccion a construir
	 * 
	 * @param transactionData Parametros de la transaccion
	 * 
	 * @param transaction Transaccion
	 * 
	 * @return Estructura que almacena los subcampos de un campo ISO
	 */
	private CompositeFieldInfo setMessageSubField(String paramName, 
													TemplateSubField field, 
													CompositeFieldInfo compositeFieldI, 
													IsoTemplate isoTemplate, 
													TransactionData transactionData,
													Transaction transaction,
													Transaction prevTransaction) {
		try {
			logger.trace("SUBFIELD: {} {}", field.getFieldId(), field);
			ClassLoader classLoader = TransactionFactory.class.getClassLoader();
			Class<?> extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(isoTemplate.getSubpackageName().concat(".").concat(field.getClassName())));
			Map<String, String> transactionParam = transactionData.getTransactionParameters();
			Object value = null;
			logger.trace("{}: {} {}", new Object[]{paramName, field.getFieldId(), field.getLoadType()});
			switch(field.getLoadType()) {
			case DATABASE:
				try {
					String paramValue = null;
					TransactionParameter transactionParameter = null;
					Iterator<TransactionParameter> iterator = transaction.getParameters().iterator();
					while((iterator.hasNext()) && (paramValue == null)) {
						transactionParameter = iterator.next();
						logger.trace("{}", transactionParameter);
						paramValue = transactionParameter.getParamName() == paramName ? transactionParameter.getParamValue() : null;
					}
					//Aplicar el metodo codificador configurado para el campo
					value = paramValue != null ? 
							field.getEncoder() == null ? paramValue
							: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
							.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
							: null;
					isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case WEBSERVICE:
				try {
					//Aplicar el metodo codificador configurado para el campo	
					value = transactionParam.containsKey(paramName) ? 
								field.getEncoder() == null ? 
								transactionParam.get(paramName)
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), transactionParam.get(paramName), transactionParam) 
								: null;
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
					//e.printStackTrace();
				}
				break;
			case FIXED:
				try {
						//Aplicar el metodo codificador configurado para el campo
						value = field.getEncoder() == null ? 
								field.getFixedValue()
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), field.getFixedValue(), transactionParam);
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case COMPUTED:
				try {
						// Aplicar la funcion de carga configurada para obtener el valor del campo
						value = field.getLoadFunc() != null ? 
								extClass.getMethod(field.getLoadFunc(), Terminal.class, Map.class)
								.invoke(null, transaction.getTerminal(), transactionParam)
								: null;
						// Aplicar la funcion de codificacion configurada para el campo
						value = field.getEncoder() == null ? value 
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), value, transactionParam);
				} catch (IllegalAccessException | IllegalArgumentException | 
						InvocationTargetException | NoSuchMethodException | SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
				}
				break;
			case RULE:
				break;
			case PREVTRXREQ:
				try {
					String paramValue = getSetParameter(prevTransaction, paramName);
					logger.trace("TRX: {} PREVTRXREQ SUB: {} - {}", new Object[]{prevTransaction.getId(), paramName, paramValue}); 
					value = paramValue;
					if(field.getEncoder() != null) {
						try {
							value = extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
									.invoke(null, transaction.getTerminal(), paramValue, transactionParam);
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (NoSuchMethodException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
//					if(paramValue != null ) {
//						//Aplicar el metodo codificador configurado para el campo	
//						value = transactionParam.containsKey(paramName) ? 
//									field.getEncoder() == null ? 
//									transactionParam.get(paramName)
//									: extClass.getMethod(field.getEncoder(), Object.class, Map.class)
//									.invoke(null, transactionParam.get(paramName), transactionParam) 
//									: null;
//					}
				} catch (  IllegalArgumentException | 
						    SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
					//e.printStackTrace();
				}
				break;
			case PREVTRXRESP:
				try {
					String paramValue = getSetResParameter(prevTransaction, paramName);
					logger.trace("TRX: {} PREVTRXRESP SUB: {} - {}", new Object[]{prevTransaction.getId(), paramName, paramValue}); 
					value = paramValue;
					if(field.getEncoder() != null) {
						try {
							value = extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
									.invoke(null, transaction.getTerminal(), paramValue, transactionParam);
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (NoSuchMethodException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
//					if(paramValue != null ) {
//						//Aplicar el metodo codificador configurado para el campo	
//						value = transactionParam.containsKey(paramName) ? 
//									field.getEncoder() == null ? 
//									transactionParam.get(paramName)
//									: extClass.getMethod(field.getEncoder(), Object.class, Map.class)
//									.invoke(null, transactionParam.get(paramName), transactionParam) 
//									: null;
//					}
				} catch ( IllegalArgumentException | 
						   SecurityException  e) {
					logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
					//e.printStackTrace();
				}
				break;
				case BATCH:
					try {
						logger.trace("Tipo de parametro BATCH");
						String paramValue = null;
						Terminal terminal = transaction.getTerminal();
						//Para todos los tipos de lotes asociados a la plantilla
						//determinar cual de los pares parametro-valor coinciden con 
						//un parametro presente en la transaccion para asi determinar el tipo de lote
						boolean batchTypeFound = false;
						BatchType trxBatchType = null;
						logger.trace("Numero de batch types {}", transaction.getTransactionType().getTemplateMessage().getPaymentChannel().getBatchTypes().size());
						logger.trace("Numero de parametros {}", transactionData.getTransactionParameters().size());
						trxBatchType = determineTrxBatchT(transaction, transactionData);
						//Si se encuentra un tipo de lote se debe cargar ese tipo de entre los lotes actuales del terminal
						paramValue = loadBatchValue(trxBatchType, terminal);
						//Aplicar el metodo codificador configurado para el campo
						value = paramValue != null ? 
								field.getEncoder() == null ? paramValue
								: extClass.getMethod(field.getEncoder(), Terminal.class, Object.class, Map.class)
								.invoke(null, transaction.getTerminal(), paramValue, transactionParam) 
								: null;
						isoMessage.setValue(field.getFieldId(), value, field.getFieldType(), field.getFieldLen());
						logger.trace("BATCH value {}", value);
					} catch (IllegalAccessException | IllegalArgumentException | 
							InvocationTargetException | NoSuchMethodException | SecurityException  e) {
						logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
					}
				break;
			default:
				break;
				
			}
			// TODO CCR Credito: si no se encuentra el valor y no es mandatorio ignorar
			// TODO CCR Credito: si no se encuentra el valor y es mandatorio generar excepcion y abortar proceso
			if(value != null){
				compositeFieldI.addValue(new IsoValue<Object>(field.getFieldType(), value, field.getFieldLen()));
			} else {
				if(field.isMandatory()) {
					logger.error("El subcampo {} es mandatorio y no se encuentra", paramName);
				} else {
					logger.debug("El subcampo {} no es mandatorio y no se encuentra", paramName);
				}
			}
		} catch (ClassNotFoundException e) {
			logger.error("Error en la configuracion de los campos de la transaccion: {}", e.toString());
		}
		return compositeFieldI;
	}
	
	private static int extractBin(String cardNumber) {
		return Integer.parseInt(cardNumber.substring(0, 6));
	}
	
	/**
	 * Imprime los campos de una estructura IsoMessage
	 * 
	 * @param m Estructura IsoMessage que almacena un mensaje
	 */
	public static void printIsoMessage(IsoMessage m) {
		logger.debug("{}", String.format("TYPE: %04x\n", m.getType()));
		for (int i = 2; i < 128; i++) {
			if (m.hasField(i)) {
				logger.debug("{}", String.format("F %3d(%s): %s -> '%s'\n", i, m.getField(i)
						.getType(), m.getObjectValue(i), m.getField(i)
						.toString()));
			}
		}
	}
	
	/**
	 * Convierte los caracteres de una cadena a hexadecimal
	 * 
	 * @param str Cadena de texto a convertir
	 * 
	 * @return Cadena de texto obtenida
	 */
	private static String convertStringToHex(String str){
		char[] chars = str.toCharArray();
		String middle;
		StringBuffer hex = new StringBuffer();
		for(int i = 0; i < chars.length; i++){
			middle = Integer.toHexString((int)chars[i]);
			hex.append(middle);
		}
		return hex.toString();
	}
	
	/**
	 * Convierte una cadena de texto hexadecimal a una cadena de texto
	 * 
	 * @param hex Cadena de texto a convertir
	 * 
	 * @return Arreglo de bytes obtenido
	 */
	private static String convertHexToString(String hex){
        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        for( int i=0; i<hex.length()-1; i+=2 ){
            String output = hex.substring(i, (i + 2));
            int decimal = Integer.parseInt(output, 16);
            sb.append((char)decimal);    
            temp.append(decimal);
        }
        return sb.toString();
  }
	
	/**
	 * Metodo para enviar mensajes de prueba al switch de desarrollo, la longitud se calcula automaticamente
	 * 
	 * @param m Estructura IsoMessage que contiene el mensaje
	 * 
	 * @param timeout Tiempo maximo de espera de respuesta del mensaje
	 *
	 * @return Respuesta del mensaje
	 * 
	 * @throws ServiceUnavailable No se puede acceder a algun componente de la plataforma
	 * 
	 * @throws ConnectionFailure Falla en la conexion con el aprobador
	 * 
	 * @throws NoResponse Sin respuesta de la transaccion
	 * 
	 * Movistar socket asincrono
	 */
	public byte[] sendMsgAut(
			IsoMessage m, 
			double timeout, 
			SocketChannel testSocket, 
			Integer trxId,
			AsyncReqQueueLocal aReqQueue,
			ReadExecutorServiceLocal readExecSer,
			TransactionData trxData) throws ServiceUnavailable, ConnectionFailure, NoResponse {
		// TODO Mover este metodo a su paquete correspondiente
		logger.debug("Movistar socket asincrono");
		byte[] respArr = null;
		try {
			logger.debug("Trx frame {}", m.debugString());
			printIsoMessage(m);

			/* Si el socket esta conectado */
			if(testSocket.isOpen()){
				
				logger.info("Socket conectado en {}:{}. Enviando....", 
						((InetSocketAddress)(testSocket.getRemoteAddress())).getAddress(), 
						((InetSocketAddress)(testSocket.getRemoteAddress())).getPort());

				/* Convertir el mensaje y agregarle la longitud */
				ByteBuffer byteBuf = ByteBuffer.allocate(2);
				byte[] requestMsgCom =  m.debugString().getBytes(StandardCharsets.ISO_8859_1);
				logger.trace(m.debugString());
				byteBuf.putShort((short)requestMsgCom.length);
				byte[] requestMsgLen = byteBuf.array();
				byte[] requestMsg = new byte[requestMsgLen.length + requestMsgCom.length];
				System.arraycopy(requestMsgLen, 0, requestMsg, 0, requestMsgLen.length);
				System.arraycopy(requestMsgCom, 0, requestMsg, requestMsgLen.length, requestMsgCom.length);
								
				int lonmsj = requestMsg.length;
				logger.trace("Bytes del mensaje: {}", lonmsj);
				ByteBuffer requestMsgB = ByteBuffer.wrap(requestMsg);
				logger.debug("After allocate: requestMsgB.capacity() {} - requestMsgB.limit() {} - ", new Object[]{requestMsgB.capacity(), requestMsgB.limit()});
				
				//Escribimos el mensaje en el socket
				testSocket.write(requestMsgB);
				int size = 1000; //TODO Tamano?
				ByteBuffer responseMsgB = ByteBuffer.allocate(size);
				logger.debug("After allocate: responseMsg.capacity() {} - responseMsg.limit() {} - ", new Object[]{responseMsgB.capacity(), responseMsgB.limit()});
				responseMsgB.clear();
				logger.debug("After clear: responseMsg.capacity() {} - responseMsg.limit() {} - ", new Object[]{responseMsgB.capacity(), responseMsgB.limit()});
				logger.debug("responseMsgB.position(): {}", responseMsgB.position());
				logger.trace("Size allocated: {}", size);

				logger.debug("Timeout of {} {}", (long)timeout,TimeUnit.SECONDS.toString());
				try {
					logger.debug("aReqQueue{}es null", aReqQueue == null ? " " : " no ");
					//Colocar el request data de la transaccion en la cola
					aReqQueue.getReqDQueue().put(trxId, trxData);
					//Leer de la cola ya que es un hilo el que realiza las lecturas del socket y las coloca en la cola
					responseMsgB = this.readQueueBlock(trxId ,(long)timeout, aReqQueue, readExecSer.getAsyncExecSer());
					logger.debug("responseMsgB from readQueueBlock: {}", responseMsgB != null ? new String(responseMsgB.array()) : "null");
				} catch (InterruptedException e) {
					logger.error(e.toString());
				} catch (ExecutionException e) {
					logger.error(e.toString());
				} catch (TimeoutException e) {
					logger.debug("*******************Timeout de metodo asincrono****************");
					//Marcar la transaccion como fuera de tiempo
					if(aReqQueue.getcQueue().get(trxId) != null) {
						aReqQueue.getcQueue().get(trxId).setTimedOut(true);
					} else {
						aReqQueue.getcQueue().put(trxId, new AsyncReqElem(
								trxId,
								null,
								null,
								true,
								false,
								null
								));
					}
					throw new NoResponse();
				}

				logger.debug("responseMsgB: {}", responseMsgB != null ? new String(responseMsgB.array()) : "null");
				logger.debug("responseMsgB.position: {}", responseMsgB != null ? responseMsgB.position() : "null");
    

				logger.debug("Response size: {}", responseMsgB != null ? responseMsgB.array().length : "null");
				String str = responseMsgB != null ? new String(responseMsgB.array(), StandardCharsets.ISO_8859_1) : null;
				logger.trace("String: {}", str);
				logger.trace("Bytes leidos (iso len): {}", size);
				logger.trace("Recibido: {}", responseMsgB != null ? new String(responseMsgB.array()) : "null");
				//for(int i = 0; i < size; i++)
				//	logger.debug("buf[i={}]= {} char: {}", new Object[]{i, buf[i], (char)buf[i]});
				
//				String hex = str != null ? convertStringToHex(str) : null;
//				logger.trace("Hex: ", hex);
//				String hex2 = responseMsgB != null ? DatatypeConverter.printHexBinary(responseMsgB.array()) : null;
//				logger.trace("DatatypeConverter: ", hex2);
//				
//				respArr = hex2 != null ? hex2.getBytes() : null;
				respArr = responseMsgB != null ? responseMsgB.array() : null;
				if(respArr == null) throw new NoResponse(); 
				
			}
			else{
				logger.warn("Socket no conectado.");
			}
			
		} catch (UnknownHostException e) {
			logger.error(e.toString());
			throw new ServiceUnavailable();
		} catch (IOException e) {
			logger.error(e.toString());
			e.printStackTrace();
			throw new ConnectionFailure();
		}
		return respArr;
	}
	
	private Future<ByteBuffer> readQueue(
			Integer trxId, 
			AsyncReqQueueLocal aReqQueue,
			ExecutorService asyncExecSer) throws InterruptedException {
		//Metodo asincrono para leer de la cola, se lanza en otro hilo
		AsyncReadFuture asyncReadF = new AsyncReadFuture();
		logger.debug("Creating callable");
		Callable<ByteBuffer> callableRead = new Callable<ByteBuffer>() {
		    public ByteBuffer call() throws Exception {
		    	System.out.println("Executor callable before while with trxId: " + trxId);
		    	while(aReqQueue.getcQueue().get(trxId) == null);
		    	asyncReadF.onReadQueueResult(aReqQueue.getcQueue().get(trxId).getTrxByteB());
		    	System.out.println("Executor callable after while with trxId: " + trxId);
		    	return aReqQueue.getcQueue().get(trxId).getTrxByteB();
		    }
		};
		logger.debug("Before invoking callables");
		asyncExecSer.submit(callableRead);
		logger.debug("After invoking callables");
//		asyncReadF.onReadQueueResult(aReqQueue.getcQueue().get(trxId) != null ? 
//				aReqQueue.getcQueue().get(trxId).getTrxByteB() : null);
		//Leer de la cola
		return asyncReadF;
	}
	
	private ByteBuffer readQueueBlock(
			Integer trxId, 
			Long timeout,
			AsyncReqQueueLocal aReqQueue,
			ExecutorService asyncExecSer) 
					throws InterruptedException, ExecutionException, TimeoutException {
		logger.debug("readQueueBlock timeout: {}", timeout);
		//ExecutorService asyncExecSer = Executors.newSingleThreadExecutor(); //newFixedThreadPool(1)
		return readQueue(trxId, aReqQueue, asyncExecSer).get(timeout, TimeUnit.SECONDS);	
	}
	
	/**
	 * Metodo para enviar mensajes de prueba al switch de desarrollo, la longitud se calcula automaticamente
	 * 
	 * @param m Estructura IsoMessage que contiene el mensaje
	 * 
	 * @param timeout Tiempo maximo de espera de respuesta del mensaje
	 *
	 * @return Respuesta del mensaje
	 * 
	 * @throws ServiceUnavailable No se puede acceder a algun componente de la plataforma
	 * 
	 * @throws ConnectionFailure Falla en la conexion con el aprobador
	 * 
	 * @throws NoResponse Sin respuesta de la transaccion
	 * 
	 * CCR Socket basico
	 */
	@SuppressWarnings("unchecked")
	public static byte[] sendMsgAut(IsoMessage m, double timeout, PaymentChannel paymentChannel) throws ServiceUnavailable, ConnectionFailure, NoResponse{
		// TODO Mover este metodo a su paquete correspondiente	
				logger.info("CCR Socket basico");
				byte[] respArr = null;
				byte[] eds = null;
				Socket testSocket;
				DataInputStream dis;
				DataOutputStream dos;
				try {
					logger.debug("Trx frame {}", m.debugString());		
					printIsoMessage(m);
					/* Creacion del socket */
					
					String authHost;
					int authPort;
					//authHost = "10.164.7.134"; authPort = 9115;
					//authHost = "localhost"; authPort = 8000;
					//authHost = "localhost"; authPort = 5020;
					//authHost = "137.1.1.12"; authPort = 5020;
					//authHost = "201.249.143.134"; authPort = 8000;	
					//authHost = "credicard.donweb-homeip.net"; authPort = 8002;
					//authHost = "192.168.1.10"; authPort = 8000;
					//authHost = "137.1.1.12"; authPort = 5020;
					//authHost = "201.249.143.134"; authPort = 8000;	
					//authHost = "credicard.donweb-homeip.net"; authPort = 8002;
					authHost = paymentChannel.getPaymentProcessor().getConnectionIp(); 
					authPort = Integer.parseInt(paymentChannel.getPaymentProcessor().getConnectionPort());
					logger.info("Conectandose a {} en el puerto {}", authHost, authPort);
					logger.debug("Timeout de la conexion {}", (int)(timeout*1000));		
					testSocket = new Socket(authHost, authPort);
					testSocket.setSoTimeout((int) (timeout*1000));
					//testSocket.setSoTimeout((int) (30*1000));
					
					/* Si el socket esta conectado */
					if(testSocket.isConnected()){
						Session session = null;
						logger.info("Socket conectado. Enviando trama ISO8583....");
						if (hsm==null){
							//Se obtiene el canal de pago
							try {
								session = HibernateUtil.getSessionFactory().openSession();
								if(session == null) logger.debug("session de Hibernate es null");
								hsm = (Hsm)session.get(Hsm.class, 1);
								logger.debug("hsm: ==>{}", hsm.toString());
								hsmTpdu = (List<HsmTpdu>) session.createQuery(
                                        "from HsmTpdu as hsmTpdu where hsm.id = :hsmId and hsmTpdu.active = :active")
                                        .setInteger("hsmId", hsm.getId())
                                        .setBoolean("active", true)
                                        .list();
                                logger.debug("hsmTpdu: ==>{}", hsmTpdu.toString());
                                logger.debug("hsmTpdu-size: ==>{}", hsmTpdu.size());
							} catch (Exception e) {
								logger.error("Error en cargar el Objeto HSM.");
								e.printStackTrace();
							} finally {
								if(session != null) session.close();
							}
						}
						DataBlock dataBlock = new DataBlock();
						//Convertir el mensaje y agregarle la longitud 
						String tpdu = m.getIsoHeader();
						m.setIsoHeader("");
						String plainFrame = m.debugString().replace(" ", "");  //Trama en claro
						String finalFrame = "";
						logger.debug("Objeto HSM {}", hsm);
						if (hsm == (null) || hsm.getActive()==false) {
							finalFrame = plainFrame;
							logger.debug("Trama en claro sin TPDU: {}", plainFrame);
							logger.debug("Trama en claro con TPDU: {}", tpdu + finalFrame);
						}else{
							//HsmTpdu hsmTpduO = hsmTpdu.size() > 0 ? hsmTpdu.get(0) : null;
                            for (HsmTpdu hsmTpduO: hsmTpdu){
                                if (hsmTpduO.getPaymentChannel() == paymentChannel || hsmTpduO.getPaymentChannel().equals(paymentChannel)){
                                    tpdu =  hsmTpduO.getTpdu();
                                }
                            }
							eds = calculateEds(plainFrame, (short)hsm.getKin().intValue());
							plainFrame = padPlainFrame(plainFrame, 16);
							logger.debug("Trama en claro sin TPDU: {}", plainFrame);
							logger.debug("Trama en claro con TPDU de cifrado: {}", tpdu + " - " + plainFrame);
							logger.debug("Cifrando trama EFTSec");
							//Cifrando trama
							Map<String,String> hsmResult = dataBlock.processDataBlock(hsm.getIpAddress(), hsm.getPort(), hsm.getSchemeHsmAcronym(), "E", hsm.getMessageHeader(), "M0", "01", "1", "1", hsm.getKeyType(), hsm.getKey(), "", "", "00 00 00 00 00 00 00 00", dataBlock.AsciiDecToHex(plainFrame), plainFrame,"","","","", hsm.isPrintLog());
							if (hsmResult.get("errorCode") == "00" || hsmResult.get("errorCode").equals("00")) {
								finalFrame = hsmResult.get("message");
								logger.debug("Trama encriptada sin TPDU: {}", hsmResult.get("message"));
								logger.debug("Trama encriptada con TPDU: {}", tpdu + finalFrame);
							} else {
								testSocket.close();
								throw new NoResponse();
							}
						}				
						
						byte[] requestMsgCom =null;
						byte[] tpduBytes = null;
						requestMsgCom =  convertHexToString(finalFrame).getBytes(StandardCharsets.ISO_8859_1);
						tpduBytes = convertHexToString(tpdu).getBytes(StandardCharsets.ISO_8859_1);
						short msgTotalL = (short) (tpduBytes.length + (eds != null ? eds.length : 0 ) + requestMsgCom.length);
						ByteBuffer byteBuf = ByteBuffer.allocate(2);
						
						logger.debug("TPDU final a enviar {}", tpdu);
						logger.debug("Trama final a enviar {}", finalFrame);
						byteBuf.putShort((short)msgTotalL);
						byte[] requestMsgLen = byteBuf.array();
						byte[] requestMsg = new byte[requestMsgLen.length +  msgTotalL];
						System.arraycopy(requestMsgLen, 0, requestMsg, 0, requestMsgLen.length); //Longitud
						System.arraycopy(tpduBytes, 0, requestMsg, requestMsgLen.length, tpduBytes.length); //TPDU
						if(eds != null) System.arraycopy(eds, 0, requestMsg, requestMsgLen.length + tpduBytes.length, eds.length);
						Integer msgPosDest = eds == null ? requestMsgLen.length + tpduBytes.length 
								: requestMsgLen.length + tpduBytes.length + eds.length;
						System.arraycopy(requestMsgCom, 0, requestMsg, msgPosDest, requestMsgCom.length); //Mensaje sin TPDU ni EDS
										
						int lonmsj = requestMsg.length;
						logger.trace("Numero de bytes del mensaje: {}", lonmsj);
						logger.debug("Bytes del mensaje {}", new String(requestMsg));
						
						/* Creamos el DataOutputStream para el envio del mensaje y el DataInputStream para la recepcion */
						dos = new DataOutputStream(testSocket.getOutputStream());
						dis  = new DataInputStream(testSocket.getInputStream());
						long lStartTime = System.nanoTime();
						dos.write(requestMsg);
						byte[] lenbuf = new byte[2];
						dis.read(lenbuf, 0, 2);
						long lEndTime = System.nanoTime();
						long output = lEndTime - lStartTime;
				        logger.info("Tiempo de la transaccion en procesador: {} seg", (double)output / 1000000000);
						logger.trace("lenbuf[0]= {} lenbuf[1]= {}", (char)lenbuf[0], (char)lenbuf[1]);
						int size = ((lenbuf[0] & 0xff) << 8) | (lenbuf[1] & 0xff);
						byte[] buf = new byte[size];
						int readed = dis.read(buf,0,size);
						logger.trace("Leidos en realidad: {}", readed);
						if(readed < 1) {
							testSocket.close();
							throw new NoResponse();
						}
						String str = new String(buf, StandardCharsets.ISO_8859_1);
						logger.debug("String: {}", str);
						logger.debug("Bytes leidos (iso len): {}", size);
						logger.debug("Recibido: {}", new String(buf));
						//for(int i = 0; i < size; i++)
						//	logger.debug("buf[i={}]= {} char: {}", new Object[]{i, buf[i], (char)buf[i]});
						
						String hex = convertStringToHex(str);
						logger.debug("Hex: ", hex);
						String hex2 = DatatypeConverter.printHexBinary(buf);
						logger.debug("DatatypeConverter: ", hex2);
						
						respArr = hex2.getBytes();
					}
					else{
						logger.warn("Socket no conectado");
					}
					testSocket.close();
					
				} catch (UnknownHostException e) {
					logger.error(e.toString());
					throw new ServiceUnavailable();
				} catch (IOException e) {
					logger.error(e.toString());
					e.printStackTrace();
					logger.error("Lanzando excepcion ConnectionFailure");
					throw new ConnectionFailure();
				}
				return respArr;
	}
	
	public static byte[] sendMsgAutPost(IsoMessage m, double timeout) throws ServiceUnavailable, ConnectionFailure, NoResponse{
		// TODO Mover este metodo a su paquete correspondiente
		byte[] respArr = null;
		try {
			logger.debug("Trx frame {}", m.debugString());
			printIsoMessage(m);
			Client gatewayClient = ClientBuilder.newClient();
			// TODO Cargar solo al inicio del aplicativo
			EngineProperties.loadProperties();
			String authTarget = "http://" + EngineProperties.getVauthIp() + ":" + EngineProperties.getVauthPort() + "/" + EngineProperties.getVauthTrxPath();
			logger.debug("authTarget: {}", authTarget);
			WebTarget gatewayWebTarget = gatewayClient.target(authTarget);
			Invocation.Builder gInvocationBuilder = gatewayWebTarget.request();
			logger.debug("Enviando solicitud al autorizador");
			Response gResponse = gInvocationBuilder.post(Entity.text(m.debugString()));
			String authResp = gResponse.readEntity(String.class);
			logger.debug("Respuesta del autorizador {}", authResp);
			respArr = authResp.getBytes();
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return respArr;
	}
	
	public String getSetParameter(Transaction transaction, String paramName) {
		String paramValue = null;
		TransactionParameter transactionParameter = null;
//		logger.debug("Transaccion id: {} - Num elem getParameters {} - getParameters {}", transaction.getId(), transaction.getParameters().size(), transaction.getParameters());
		Iterator<TransactionParameter> iterator = transaction.getParameters().iterator();
		while((iterator.hasNext()) && (paramValue == null)) {
			transactionParameter = iterator.next();
			paramValue = (transactionParameter.getParamName() == paramName) || (transactionParameter.getParamName().equals(paramName)) ? transactionParameter.getParamValue() : null;
//			logger.debug("transactionParameter.getParamName() {} - paramName {} == {}", transactionParameter.getParamName(), paramName, transactionParameter.getParamName() == paramName);
//			logger.debug("transactionParameter.getParamName() {} - paramName {} equals {}", transactionParameter.getParamName(), paramName, transactionParameter.getParamName().equals(paramName));
		}
		logger.debug("getSetParameter {}: {} - Transaccion {}", paramName, paramValue, transaction);
		return paramValue;
	}
	
	public String getSetResParameter(Transaction transaction, String paramName) {
		String paramValue = null;
		TransactionParameter transactionParameter = null;
		Iterator<TransactionResponseParameter> iterator = transaction.getResponseParameters().iterator();
		while((iterator.hasNext()) && (paramValue == null)) {
			transactionParameter = iterator.next();
			paramValue = (transactionParameter.getParamName() == paramName) || (transactionParameter.getParamName().equals(paramName))  ? transactionParameter.getParamValue() : null;
		}
		logger.debug("getSetResParameter {}: {} - Transaccion {}", paramName, paramValue, transaction);
		return paramValue;
	}
	
	/**
	 * Funcion para pre procesar tramas antes del parsing 
	 * 
	 * @param trxFrame Trama recibida del procesador y/o autorizador
	 * 
	 * @param isoTemplate Plantilla ISO correspondiente al tipo de transaccion
	 * 
	 * @return Trama resultante del pre procesamiento
	 */
	public byte[] preProcessingMsg(byte[] trxFrame, IsoTemplate isoTemplate) {
		ClassLoader classLoader = TransactionFactory.class.getClassLoader();
		try {
			if((isoTemplate.getSubpackageName() != null) && 
				(isoTemplate.getClassName() != null) && 
				(isoTemplate.getPreProcFunction() != null)) {
				logger.debug("Pre procesamiento de la trama recibida");
				logger.debug("{} {} {}", new Object[] {isoTemplate.getSubpackageName(), isoTemplate.getClassName(), isoTemplate.getPreProcFunction()});
				Class<?> extClass = classLoader.loadClass("vpago.core.transactions.ext.".concat(isoTemplate.getSubpackageName().concat(".").concat(isoTemplate.getClassName())));
				logger.debug(extClass != null ? "extClass != null" : "extClass == null");
				logger.debug(extClass.getMethod(isoTemplate.getPreProcFunction(), byte[].class) != null ? "Method not null" : "Method null");
				logger.debug("method.getName()= {}", extClass.getMethod(isoTemplate.getPreProcFunction(), byte[].class).getName());
				trxFrame = (byte[]) extClass.getMethod(isoTemplate.getPreProcFunction(), byte[].class).invoke(null, trxFrame);
			}
			
		} catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			logger.error("Error pre procesando trama recibida {}", e);
		} catch (Exception e) {
			logger.error("Error pre procesando trama recibida {}", e);
		}
		return trxFrame;
	}
	
	/**
	 * 
	 * @param transaction
	 * @return
	 */
	public IsoMessage isoMsgFromExistentTrx(Transaction transaction) {
		return new IsoMessage();
	}
	
	/**
	 * Determina el lote al que pertenece una transaccion (Metodo sobrecargado)
	 * @param transaction
	 * @param transactionData
	 * @return
	 */
	public BatchType determineTrxBatchT(Transaction transaction, TransactionData transactionData) {
		return determineTrxBatchT(transaction.getTransactionType().getTemplateMessage().getPaymentChannel(), transactionData);
	}
	
	/**
	 * Determina el lote al que pertenece una transaccion
	 * @param paymentChannel
	 * @param transactionData
	 * @return
	 */
	public BatchType determineTrxBatchT(PaymentChannel paymentChannel, TransactionData transactionData) {
		logger.debug("determineTrxBatchT");
		BatchType trxBatchType = null;
		boolean batchTypeFound = false;
		logger.debug("paymentChannel getBatchTypes size {}", paymentChannel.getBatchTypes().size());
		for(Entry<String, String> trxParam: transactionData.getTransactionParameters().entrySet()) {
			for(BatchType batchType : paymentChannel.getBatchTypes()) {
//				logger.debug("Tipo de lote - BatchType {} ", batchType);
//				logger.debug("trxParam paramName {} paramValue {} batchType paramName {} paramValue {}", 
//						new Object[]{ trxParam.getKey(),
//						trxParam.getValue(),
//						batchType.getParamName(),
//						batchType.getParamValue()});
				if(trxParam.getKey().equals(batchType.getParamName()) 
						&& trxParam.getValue().equals(batchType.getParamValue())) {
					batchTypeFound = true;
					trxBatchType = batchType;
					logger.trace("El tipo de lote coincide con el valor del parametro {} {}", trxParam.getKey(), trxParam.getValue());
				}
				if(batchTypeFound) break; 
			}
			if(batchTypeFound) break;
		}
		return trxBatchType;
	}

	private String loadBatchValue(BatchType trxBatchType, Terminal terminal) {
		String paramValue = null;
		if(trxBatchType != null) {
			for(CurrentBatch currentBatch : terminal.getCurrentBatchs()) {
				//Encontramos el lote actual del terminal cuyo tipo coincide con el determinado
				//para la transaccion
				if(currentBatch.getBatchType().equals(trxBatchType)) {
					//Cargamos el valor del lote para esa transaccion
					paramValue = Integer.toString(currentBatch.getBatchNumber());
					logger.trace("Numero de lote de la transaccion {}", paramValue);
				}
			}
		}
		return paramValue;
	}
	
	/**
	 * Rellena la trama a una longitu multiplo del numero indicado
	 * @param plainFrame
	 * @return
	 */
	private static String padPlainFrame(String plainFrame, int multiple) {
		Integer lenMod = plainFrame.length() % multiple;
		Integer extraBytes = multiple - lenMod;
		if (lenMod > 0) {
			for(int i=0; i<extraBytes; i++) plainFrame = plainFrame.concat("0");
			logger.trace("Padding a longitud multiplo de {}", multiple);
			logger.trace("length {} extraBytes {}", plainFrame.length(), extraBytes);
			logger.trace("{}", String.format("%1$"+extraBytes+ "s", plainFrame));
		}
		return plainFrame;
	}
	
	/**
	 * Construye el encabezado de la trama EFTSec
	 * @param plainFrame
	 * @return
	 */
	private static byte[] calculateEds(String plainFrame, short kin) {
		ByteBuffer ctrKinBuf = ByteBuffer.allocate(2),
				   fStartBuf = ByteBuffer.allocate(2),
				   fLenBuf = ByteBuffer.allocate(2),
				   fCheckBuf = ByteBuffer.allocate(1),
				   edsBuf = ByteBuffer.allocate(7)
				   ;
		ctrKinBuf.putShort(0, (short)kin);
		fStartBuf.putShort((short) 0);
		fLenBuf.putShort((short) ((short)plainFrame.length()/2));
		fCheckBuf.put(calculateChecksum(plainFrame));
		logger.debug("EDS Length {}", (short) ((short)plainFrame.length()/2));
		edsBuf.putShort(0, (short)kin);
		edsBuf.putShort(2, (short) 0);
		edsBuf.putShort(4, (short) ((short)plainFrame.length()/2));
		edsBuf.put(6, calculateChecksum(plainFrame));
		
		String ctrlKin = new String(ctrKinBuf.array()),
			   fStart = new String(fStartBuf.array()),
			   fLen = new String(fLenBuf.array()),
			   fCheck = new String(fCheckBuf.array()),
			   eds = ""
		;
		eds = ctrlKin + fStart + fLen + fCheck;
		logger.debug("Longitud de la trama {}", plainFrame.length());
		logger.debug("KIN Hexa {} ", Integer.toHexString(kin));
		logger.debug("EDS {}", eds);
		return edsBuf.array();
	}
	
	/**
	 * Calcula el checksum que estara presente en el encabezado de la trama EFTsec
	 * @param plainFrame
	 * @return
	 */
	private static byte calculateChecksum(String plainFrame) {
		int checksum = 0;
		String output = "";
		int ini = plainFrame.length() % 2 == 0 ? 0 : 1;
		if(ini == 1) checksum = Integer.parseInt(plainFrame.substring(0, 1));
		for(int i=ini; i<plainFrame.length()-1; i+=2 ){
			output = plainFrame.substring(i, (i + 2));
			checksum += Integer.parseInt(output, 16);   
		}
		checksum = (short) (checksum % 256);
		logger.debug("Checksum dec {} - hexa {}", checksum, Integer.toHexString(checksum));
		return (byte) checksum;
	}
}
