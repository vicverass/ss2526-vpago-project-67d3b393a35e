package vpago.core.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vpago.core.transactions.classes.Acquirer;
import vpago.core.transactions.classes.AdditionalVoucherFields;
import vpago.core.transactions.classes.Affiliation;
import vpago.core.transactions.classes.AssignedDevice;
import vpago.core.transactions.classes.Bank;
import vpago.core.transactions.classes.BankCardProduct;
import vpago.core.transactions.classes.BatchHistory;
import vpago.core.transactions.classes.BatchType;
import vpago.core.transactions.classes.Bin;
import vpago.core.transactions.classes.CardFranchise;
import vpago.core.transactions.classes.GeneralCardProduct;
import vpago.core.transactions.classes.Hsm;
import vpago.core.transactions.classes.HsmTpdu;
import vpago.core.transactions.classes.CardType;
import vpago.core.transactions.classes.CurrentBatch;
import vpago.core.transactions.classes.Device;
import vpago.core.transactions.classes.DeviceType;
import vpago.core.transactions.classes.FunctionType;
import vpago.core.transactions.classes.IsoTemplate;
import vpago.core.transactions.classes.Merchant;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.PaymentProcessor;
import vpago.core.transactions.classes.Rule;
import vpago.core.transactions.classes.RuleProduct;
import vpago.core.transactions.classes.RuleProductGeneral;
import vpago.core.transactions.classes.RuleProportion;
import vpago.core.transactions.classes.RuleSet;
import vpago.core.transactions.classes.TemplateField;
import vpago.core.transactions.classes.TemplateMessage;
import vpago.core.transactions.classes.TemplateSubField;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.TerminalParameter;
import vpago.core.transactions.classes.TimeFrame;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionParameter;
import vpago.core.transactions.classes.TransactionResponse;
import vpago.core.transactions.classes.TransactionResponseParameter;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.classes.types.TransactionStatus;

/**
 * Clase para el manejo de la configuracion y funcionamiento del framework Hibernate
 */
public class HibernateUtil
{
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	
    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    static
    {
        try
        {
//          Configuration configuration = new Configuration();
//            Configuration configuration = new Configuration().configure();
            Configuration cfg = new Configuration()
       				//HSQLDB
            		.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect")
    				.setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver")
    				.setProperty("hibernate.connection.username", "SA")
    				.setProperty("hibernate.connection.password", "")
    				.setProperty("hibernate.connection.url", "jdbc:hsqldb:hsql://localhost/engine_db")
    				.setProperty("hibernate.show_sql", "true")
    				.setProperty("hibernate.format_sql.password", "true")
    				.setProperty("hibernate.generate_statistics", "true")
    				
    				.setProperty("hibernate.c3p0.min_size", "1")
    				.setProperty("hibernate.c3p0.max_size", "100")
    				.setProperty("hibernate.c3p0.timeout", "0")
    				.setProperty("hibernate.c3p0.max_statements", "50")
    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
    				
	    			//ORACLE PRODUCCION
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINETRANSACTION")
//    				.setProperty("hibernate.connection.password", "AGng2011__")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.98:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "100")
//    				.setProperty("hibernate.c3p0.max_size", "300")
//    				.setProperty("hibernate.c3p0.timeout", "3000")
//    				.setProperty("hibernate.c3p0.max_statements","100")
//    				.setProperty("hibernate.c3p0.idle_test_period", "20")
    				
    				//ORACLE QA
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINETRANSACTION")
//    				.setProperty("hibernate.connection.password", "enginetransactionss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@10.20.184.172:1521:PROD")
////    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "50")
//    				.setProperty("hibernate.c3p0.max_size", "10000")
//    				//.setProperty("hibernate.c3p0.timeout", "60")
//    				.setProperty("hibernate.c3p0.max_statements", "2000")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "3600")
    				
    				//ORACLE DESARROLLO
//            		.setProperty("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect")
//    				.setProperty("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver")
//    				.setProperty("hibernate.connection.username", "ENGINEDEVELOPMENT")
//    				.setProperty("hibernate.connection.password", "enginedevelopmentss2526")
//    				.setProperty("hibernate.connection.url", "jdbc:oracle:thin:@172.28.111.165:1521:PROD")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "false")
//            		
//    				.setProperty("hibernate.c3p0.min_size", "1")
//    				.setProperty("hibernate.c3p0.max_size", "100")
//    				.setProperty("hibernate.c3p0.timeout", "0")
//    				.setProperty("hibernate.c3p0.max_statements", "50")
//    				.setProperty("hibernate.c3p0.maxConnectionAge", "43200")
            		
    				//POSTGRES
//					.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect")
//    				.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver")
////    				.setProperty("hibernate.connection.username", "postgres")
////    				.setProperty("hibernate.connection.password", "vpagoss2526qa")
////    				.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost/engine_db")
//    				.setProperty("hibernate.connection.username", "vfinancialu")
//    				.setProperty("hibernate.connection.password", "vfinancialu")
//    				.setProperty("hibernate.connection.url", "jdbc:postgresql://192.168.1.120/vfinancialenginedb")
//    				.setProperty("hibernate.show_sql", "false")
//    				.setProperty("hibernate.format_sql.password", "true")
    				
		
    				.addAnnotatedClass(Transaction.class)
    				.addAnnotatedClass(Terminal.class)
    				.addAnnotatedClass(Merchant.class)
    				.addAnnotatedClass(Rule.class)
    				.addAnnotatedClass(RuleSet.class)
    				.addAnnotatedClass(Acquirer.class)
    				.addAnnotatedClass(TimeFrame.class)
    				.addAnnotatedClass(CardFranchise.class)
    				.addAnnotatedClass(CardType.class)
    				.addAnnotatedClass(GeneralCardProduct.class)
    				.addAnnotatedClass(BankCardProduct.class)
    				.addAnnotatedClass(Bank.class)
    				.addAnnotatedClass(Bin.class)
    				.addAnnotatedClass(PaymentChannel.class)
    				.addAnnotatedClass(TransactionType.class)
    				.addAnnotatedClass(IsoTemplate.class)
    				.addAnnotatedClass(TemplateField.class)
    				.addAnnotatedClass(TemplateSubField.class)
    				.addAnnotatedClass(TransactionResponse.class)
    				.addAnnotatedClass(TransactionParameter.class)
    				.addAnnotatedClass(TransactionResponseParameter.class)
    				.addAnnotatedClass(RuleProduct.class)
    				.addAnnotatedClass(RuleProductGeneral.class)
    				.addAnnotatedClass(RuleProportion.class)
    				.addAnnotatedClass(TerminalParameter.class)
    				.addAnnotatedClass(Affiliation.class)
    				.addAnnotatedClass(TemplateMessage.class)
    				.addAnnotatedClass(FunctionType.class)
    				.addAnnotatedClass(PaymentProcessor.class)
    				.addAnnotatedClass(AdditionalVoucherFields.class)
    				.addAnnotatedClass(CurrentBatch.class)
    				.addAnnotatedClass(BatchHistory.class)
    				.addAnnotatedClass(BatchType.class)
    				.addAnnotatedClass(Device.class)
    				.addAnnotatedClass(DeviceType.class)
    				.addAnnotatedClass(AssignedDevice.class)
    				.addAnnotatedClass(Hsm.class)
    				.addAnnotatedClass(HsmTpdu.class)
    				;

            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
    				cfg.getProperties()).build();
            sessionFactory = cfg.buildSessionFactory(serviceRegistry);
        } catch (Exception he) {
            logger.error("Error creando sesion de Hibernate: {}", he.getMessage());
            throw new ExceptionInInitializerError(he);
        }
    }

    /**
     * Retorna una entidad necesaria para construir una sesion de Hibernate
     * 
     * @return La entidad SessionFactory
     */
    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    } 
}