package vpago.core.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoType;

import vpago.core.transactions.classes.AdditionalVoucherFields;
import vpago.core.transactions.classes.BatchType;
import vpago.core.transactions.classes.Bin;
import vpago.core.transactions.classes.CardFranchise;
import vpago.core.transactions.classes.CardType;
import vpago.core.transactions.classes.FunctionType;
import vpago.core.transactions.classes.GeneralCardProduct;
import vpago.core.transactions.classes.IsoTemplate;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.PaymentProcessor;
import vpago.core.transactions.classes.Rule;
import vpago.core.transactions.classes.TemplateField;
import vpago.core.transactions.classes.TemplateMessage;
import vpago.core.transactions.classes.Terminal;
import vpago.core.transactions.classes.Transaction;
import vpago.core.transactions.classes.TransactionParameter;
import vpago.core.transactions.classes.TransactionResponse;
import vpago.core.transactions.classes.TransactionResponseParameter;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.classes.types.BatchOperation;
import vpago.core.transactions.classes.types.DeviceValidation;
import vpago.core.transactions.classes.types.FieldLoadType;

/**
 * Metodos para pruebas con persistencia de objetos en la base de datos
 */
public class DBTest {
	private static final Logger logger = LoggerFactory.getLogger(DBTest.class);
	
	public static void main(String[] args) {
		//Almacenar canal de pago
		//paymentChannelTest();
		//Almacenar plantilla de mensaje
		//templateMsgTest();
		//Guardar Plantilla ISO (IsoTemplate)
		//isoTemplateTest();
		//Guardar Tipo de transaccion (TransactionType)
		//transactionTypeTest();
		//Guardar Campos y subcampos ISO (TemplateField y TemplateSubfield)
		//creditSale0210();
		//echoTest0800();
		//echoTest0810();
		//autoReverse0400();
		//autoReverse0410();
		//ccrPurchase0200();
		//ccrPurchase0210();
		//ccrAdvice0220();
		//ccrAdvice0230();
		//ccrAutoReverse0400();
		//ccrAutoReverse0410();
		//ccrReverse0400();
		//ccrReverse0410();
		//ccrAnnulment0210();
		//ccrAnnulment0200();
		//ccrClosing0510();
		//ccrClosing0500();
		//ccrBatchUpload320();
		//ccrBatchUpload330();
		//ccr2Closing0510();
		//ccr2Closing0500();
		//movistarFinancialReq0200();
		//movistarFinancialReq0210();
		//movistarReversalReq0420();
		//movistarReversalReq0430();
		//movistarEchoTest0800();
		//movistarEchoTest0810();
		//Almacenar respuestas de transacciones
		//transactionResponseTest();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
//		TemplateField parentField48 = isoTemplate.getTemplate_field().get("sec_cod_ci");
//		session.beginTransaction();
//		isoTemplate.insertIsoSubField("sec_ci_len", parentField48, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0014", null, "MpagoCc", 1);
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
//		TemplateField parentField57 = isoTemplate.getTemplate_field().get("pos_add_data");
//		session.beginTransaction();
//		isoTemplate.insertIsoSubField("pos_add_data_len", parentField57, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0029", null, "MpagoCc", 1);
//		session.getTransaction().commit();
		
//		for(IsoType it: IsoType.values()) {
//			System.err.println(it.toString() + ": " + it.ordinal());
//		}
//		
//		for(FieldLoadType flt: FieldLoadType.values()) {
//			System.err.println(flt.toString() + ": " + flt.ordinal());
//		}
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
//		session.beginTransaction();
//		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 23, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 13);
//		session.beginTransaction();
//		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 19, FieldLoadType.NONE, null, null, null, null, false, false, false, null, null, "MpagoCc");
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 15);
//		session.beginTransaction();
//		TemplateField parentField03 = isoTemplate.getTemplate_field().get("pcode");
//		isoTemplate.insertIsoSubField("pcode_fixed", parentField03, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MovistarRecharge", 3);
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
//		session.beginTransaction();
//		TemplateField parentField63 = isoTemplate.getTemplate_field().get("field_63");
//		isoTemplate.insertIsoSubField("ksn", parentField63, IsoType.ALPHA, 52, FieldLoadType.WEBSERVICE, "encodeBcdKSN", null, null, null, false, false, false, null, null, "MpagoCc", 2);
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
//		session.beginTransaction();
//		isoTemplate.insertIsoField("card_type", 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 24);
//		session.beginTransaction();
//		isoTemplate.insertIsoField("field_7", 7, IsoType.DATE10, 10, FieldLoadType.NONE, null, null, null, null, false, false, false, null, null, "MpagoCc");
//		session.getTransaction().commit();
		
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 3);
//		session.beginTransaction();
//		isoTemplate.insertIsoField("card_type", 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
//		session.getTransaction().commit();
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 3);
		session.beginTransaction();
		isoTemplate.insertIsoField("currency_code", 49, IsoType.ALPHA, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "840", null, "CpagoCc");
		session.getTransaction().commit();
		
		//circuitoMovistar();
		//circuitoMpagoCCR();
	}
	
	/**
	 * Almacenamiento de funciones no ISO
	 */
	public static void functionTest() {
		FunctionType lastAppFunc = new FunctionType("LAST_APPROVED", "Ultima transaccion aprobada", "lastApproved");
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(lastAppFunc);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de Canal de Pago
	 */
	public static void paymentChannelTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		// Boton de pago Credicard ---------------------------------------------------------------------------------------------
		//PaymentChannel paymentChannel = new PaymentChannel("CPAGO Credicard","Boton de pago Credicard","CPAGO_CC");
		
		// Pagos movistar ------------------------------------------------------------------------------------------------------
		PaymentChannel paymentChannel = new PaymentChannel("Pagos Movistar","Recargas y pagos movistar","REC_MOVISTAR", new PaymentProcessor(), DeviceValidation.SERIAL);
		
		// Mensajeria Credicard ------------------------------------------------------------------------------------------------------
		//PaymentChannel paymentChannel = new PaymentChannel("MPAGO Credicard","Mensajerķa transaccional Credicard","MENSAJERIA_CCR");
				
		// Almacenado ----------------------------------------------------------------------------------------------------------
		session.beginTransaction();
		session.save(paymentChannel);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de plantilla de mensaje
	 */
	public static void templateMsgTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		// Pagos Movistar ------------------------------------------------------------------------------------------------------
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 5);
		TemplateMessage templateMessage = new TemplateMessage(paymentChannel,"terminalBankId");
		
		// Mensajeria Credicard ------------------------------------------------------------------------------------------------------
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
//		TemplateMessage templateMessage = new TemplateMessage(paymentChannel,"terminalBankId");
		
		session.beginTransaction();
		session.save(templateMessage);
		paymentChannel.getTemplateMessage().add(templateMessage);
		session.update(paymentChannel);
		session.update(templateMessage);
		session.getTransaction().commit();
		session.close();
	}

	/**
	 * Almacenamiento de una Plantilla ISO
	 */
	public static void isoTemplateTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		// Boton de pago Credicard ---------------------------------------------------
		//IsoTemplate isoTemplate = new IsoTemplate(0x200, "6000070000", "cpagocc", null, null, null, null);
		//IsoTemplate isoTemplate = new IsoTemplate(0x210, "6000070000", "cpagocc", null, null, null, null);
		//IsoTemplate isoTemplate = new IsoTemplate(0x400, "6000070000", "cpagocc", null, null, null, null);
		//IsoTemplate isoTemplate = new IsoTemplate(0x410, "6000070000", "cpagocc", null, null, null, null);
		//IsoTemplate isoTemplate = new IsoTemplate(0x800, "6000070000", "cpagocc", null, null, null, null);
		//IsoTemplate isoTemplate = new IsoTemplate(0x810, "6000070000", "cpagocc", null, null, null, null);
		
		// Pagos Movistar ------------------------------------------------------------
//		IsoTemplate isoTemplate = new IsoTemplate(0x200, "ISO025000000", "recmovistar", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x210, "ISO025000000", "recmovistar", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x420, "ISO025000000", "recmovistar", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x430, "ISO025000000", "recmovistar", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x800, "ISO025000000", "recmovistar", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x810, "ISO025000000", "recmovistar", null, null, null, null);
		
		// Mensajeria Credicard ------------------------------------------------------------------------------------------------------
//		IsoTemplate isoTemplate = new IsoTemplate(0x200, "6000030000", "isoemvccr", null, "MpagoCc", "valInverseInvoke", "unblockAdvice");
//		IsoTemplate isoTemplate = new IsoTemplate(0x210, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x220, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x230, "6000030000", "isoemvccr", "preProcAdvc", "MpagoCc", null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x320, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x330, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x400, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x410, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x500, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x510, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x400, "6000030000", "isoemvccr", null, null, null, null);
//		IsoTemplate isoTemplate = new IsoTemplate(0x410, "6000030000", "isoemvccr", null, null, null, null, true, 10);
//		IsoTemplate isoTemplate = new IsoTemplate(0x500, "6000030000", "isoemvccr", null, null, null, null, false, 10);
//		IsoTemplate isoTemplate = new IsoTemplate(0x510, "6000030000", "isoemvccr", null, null, null, null, false, 10);
		IsoTemplate isoTemplate1 = new IsoTemplate(0x320, "6000030000", "isoemvccr", null, null, null, null, false, 10, null);
		IsoTemplate isoTemplate2 = new IsoTemplate(0x330, "6000030000", "isoemvccr", null, null, null, null, false, 10, null);

		// Almacenado --------------------------------------------------------------
		logger.debug("IsoTemplate: {}", isoTemplate1);
		session.beginTransaction();
		session.save(isoTemplate1);
		session.getTransaction().commit();
		
		logger.debug("IsoTemplate: {}", isoTemplate2);
		session.beginTransaction();
		session.save(isoTemplate2);
		session.getTransaction().commit();
		
		session.close();
	}
	
	/**
	 * Almacenamiento de un Tipo de Transaccion
	 */
	public static void transactionTypeTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		// Boton de pago Credicard ------------------------------------------------------------------------
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 2);
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 7);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 4);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 8);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 9);
//		TransactionType tansactionType = new TransactionType("CREDIT_SALE",
//															"Compra credito", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"creditSale",
//															1.5,
//															true,
//															2,
//															null);
//		TransactionType tansactionType = new TransactionType("CREDIT_SALE_RESP",
//				"Respuesta Compra credito", 
//				paymentChannel.getTemplateMessage().get(0), 
//				isoTemplate, 
//				"creditSaleResp",
//				1.5,
//				false,
//				0,
//				null);
//		TransactionType tansactionType = new TransactionType("ECHO_TEST_RESP",
//				"Respuesta Echo Test", 
//				paymentChannel.getTemplateMessage().get(0), 
//				isoTemplate, 
//				"echoTestResp",
//				1.5,
//				false,
//				0,
//				null);
//		TransactionType tansactionType = new TransactionType("ECHO_TEST",
//				"Echo Test", 
//				paymentChannel.getTemplateMessage().get(0), 
//				isoTemplate, 
//				"echoTest",
//				1.5,
//				false,
//				0,
//				transactionResponseType);
//		TransactionType tansactionType = new TransactionType("AUTO_REVERSE_RESP",
//				"Respuesta Reverso", 
//				paymentChannel.getTemplateMessage().get(0), 
//				isoTemplate, 
//				"reverseResp",
//				1.5,
//				false,
//				0,
//				null,
//				null);
//		TransactionType tansactionType = new TransactionType("AUTO_REVERSE",
//				"Reverse", 
//				paymentChannel.getTemplateMessage().get(0), 
//				isoTemplate, 
//				"reverse",
//				1.5,
//				false,
//				0,
//				transactionResponseType,
//				null);
		
		// Pagos Movistar ---------------------------------------------------------------------------------
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 5);
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 7);
//		TransactionType transactionInverseType = (TransactionType)session.load(TransactionType.class, 7);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 16);
		
//		TransactionType tansactionType = new TransactionType("MOVISTAR_RECHARGE_RESP",
//															"Mensaje de Respuesta - Recarga Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"creditSaleResp",
//															15,
//															false,
//															0,
//															null,
//															null);	
	
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 14);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 15);
//		TransactionType tansactionType = new TransactionType("MOVISTAR_RECHARGE_REQ",
//															"Mensaje de Requerimiento - Recarga Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"creditSale",
//															15,
//															false,
//															0,
//															transactionResponseType,
//															null);

//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 5);	
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 18);		
//		TransactionType tansactionType = new TransactionType("MOVISTAR_REVERSAL_RESP",
//															"Mensaje de Respuesta - Reverso Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverseResp",
//															15,
//															false,
//															0,
//															null,
//															null);	
	
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 16);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 17);
//		TransactionType tansactionType = new TransactionType("MOVISTAR_REVERSAL_REQ",
//															"Mensaje de Solicitud - Reverso Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverse",
//															15,
//															false,
//															0,
//															transactionResponseType,
//															null);
		
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 5);	
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 20);		
//		TransactionType tansactionType = new TransactionType("MOVISTAR_ECHO_TEST_RESP",
//															"Mensaje de Respuesta - Echo Test Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"echoTestResp",
//															15,
//															false,
//															0,
//															null,
//															null);	
	
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 18);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 19);
//		TransactionType tansactionType = new TransactionType("MOVISTAR_ECHO_TEST_REQ",
//															"Mensaje de Solicitud - Echo Test Movistar", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"echoTest",
//															15,
//															false,
//															0,
//															transactionResponseType,
//															false,
//															false,
//															false,
//															null,
//															false,
//															null,
//															false);
		
		// Mensajeria Credicard ------------------------------------------------------------------------------------------------------
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 10);
//		TransactionType transactionInverseType = (TransactionType)session.load(TransactionType.class, 7);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 11);
		
		//Mensaje 0210
//		TransactionType tansactionType = new TransactionType("CCR_PURCHASE_RESP",
//															"Mensaje de Respuesta - Compra Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"creditSaleResp",
//															30,
//															false,
//															0,
//															null,
//															null);	
		//Mensaje 0200
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 12);
//		TransactionType tansactionType = new TransactionType("CCR_PURCHASE_REQ",
//															"Mensaje de Requerimiento - Compra Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"creditSale",
//															30,
//															false,
//															0,
//															transactionResponseType,
//															null);
		
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 13);
		//Mensaje 0230
//		TransactionType tansactionType = new TransactionType("CCR_ADVICE_RESP",
//															"Mensaje de Respuesta - Advice Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"adviceResp",
//															30,
//															false,
//															0,
//															null,
//															null);	

//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 12);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 12);
//		
		//Mensaje 0220
//		TransactionType tansactionType = new TransactionType("CCR_ADVICE_REQ",
//															"Mensaje de Requerimiento - Advice Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"advice",
//															30,
//															false,
//															0,
//															transactionResponseType,
//															null);
		
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 13);
		//Mensaje 0230
//		TransactionType tansactionType = new TransactionType("CCR_ADVICE_RESP",
//															"Mensaje de Respuesta - Advice Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"adviceResp",
//															30,
//															false,
//															0,
//															null,
//															null);	
		
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 14);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 14);
		
//		//Mensaje 0400
//		TransactionType tansactionType = new TransactionType("CCR_REVERSE_REQ",
//															"Mensaje de Requerimiento - Reverso Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverse",
//															30,
//															true,
//															2,
//															transactionResponseType,
//															null);
		
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 15);
//		//Mensaje 0410
//		TransactionType tansactionType = new TransactionType("CCR_REVERSE_RESP",
//															"Mensaje de Respuesta - Reverso Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverseResp",
//															30,
//															false,
//															0,
//															null,
//															null,
//															BatchOperation.NONE);
		
//		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
//		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, 16);
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 16);
//		
//		//Mensaje 0400
//		TransactionType tansactionType = new TransactionType("CCR_DEV_REVERSAL_REQ",
//															"Mensaje de Requerimiento - Reverso Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverse",
//															30,
//															true,
//															2,
//															transactionResponseType,
//															false,
//															false,
//															false,
//															null,
//															false,
//															null,
//															false,
//															15,
//															false,
//															false,
//															BatchOperation.NONE,
//															false,
//															false,
//															null);	
//		
//		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 17);
//		//Mensaje 0410
//		TransactionType tansactionType = new TransactionType("CCR_DEV_REVERSAL_RESP",
//															"Mensaje de Respuesta - Reverso Credicard", 
//															paymentChannel.getTemplateMessage().get(0), 
//															isoTemplate, 
//															"reverseResp",
//															30,
//															false,
//															0,
//															null,
//															false,
//															false,
//															false,
//															null,
//															false,
//															null,
//															false,
//															0
//															);	
//		

		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		IsoTemplate isoTemplateAns = (IsoTemplate)session.load(IsoTemplate.class, 29);
		
		//Mensaje 0330
		TransactionType tansactionTypeAns = new TransactionType("CCR_BATCH_UPLOAD_RESP",
															"Mensaje de Solicitud - Batch Upload Credicard", 
															paymentChannel.getTemplateMessage().get(0), 
															isoTemplateAns, 
															"batchUploadResp",
															33,
															false,
															0,
															null,
															false,
															false,
															false,
															null,
															false,
															null,
															false,
															0,
															false,
															false,
															BatchOperation.NONE,
															false,
															false,
															false,
															null,
															null,
															false);	
		
		session.beginTransaction();
		Integer trxTypeId = (Integer) session.save(tansactionTypeAns);
		session.getTransaction().commit();
		
		TransactionType transactionResponseType = (TransactionType)session.load(TransactionType.class, trxTypeId);
		IsoTemplate isoTemplateReq = (IsoTemplate)session.load(IsoTemplate.class, 28);
		//Mensaje 320
		TransactionType tansactionTypeReq = new TransactionType("CCR_BATCH_UPLOAD_REQ",
															"Mensaje de Solicitud - Batch Upload Credicard", 
															paymentChannel.getTemplateMessage().get(0), 
															isoTemplateReq, 
															"batchUpload",
															33,
															false,
															0,
															transactionResponseType,
															false,
															false,
															false,
															null,
															false,
															null,
															false,
															0, 
															false, 
															false, 
															null, 
															false, 
															false,
															false,
															null,
															null,
															false
															);	
		
		session.beginTransaction();
		session.save(tansactionTypeReq);
		session.getTransaction().commit();
		session.close();
		
		// Almacenado -------------------------------------------------------------------------------------
//		session.beginTransaction();
//		session.save(tansactionType);
//		session.getTransaction().commit();
//		session.close();
	}
	
	/**
	 * Almacenamiento de una Respuesta de Transaccion
	 */
	public static void transactionResponseTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 5);
		
		// Boton de pago de Credicard ----------------------------------------------------------------------------------------------
//		TransactionResponse transactionResponse = new TransactionResponse("00", "Aprobado", paymentChannel, "Transaccion aprobada");
//		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
//		transactionResponse.add(new TransactionResponse("01", "Llamar al banco", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("19", "Trate de nuevo", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("67", "Retenga y llame", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("85", "No aprobado", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null));
//		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null)); //Default
		
		// Pagos Movistar ----------------------------------------------------------------------------------------------------------
		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
		transactionResponse.add(new TransactionResponse("00", "Aprobada con exito", paymentChannel, null, true, null, false));
		transactionResponse.add(new TransactionResponse("01", "Llame al emisor de la tarjeta", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("06", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("07", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("09", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("11", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("15", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("30", "Trate de nuevo", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("31", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("33", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("34", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("35", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("36", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("37", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("38", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("39", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("56", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("57", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("65", "Retenga y llame", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("79", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("81", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("83", "No aprobado", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("84", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("85", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("89", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("90", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("99", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("N0", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("N1", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("N2", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("Q5", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("Q6", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("Q7", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("Q8", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("Q9", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R1", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R2", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R3", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R4", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R5", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R6", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R7", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("R8", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S4", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S5", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S6", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S7", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S8", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("S9", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T1", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T2", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T3", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T4", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T5", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T6", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("T7", "Emisor no responde", paymentChannel, null, false, null, false));
		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null, false, null, false)); //Default
		
		// Mensajeria Credicard ----------------------------------------------------------------------------------------------
//		TransactionResponse transactionResponse = new TransactionResponse("00", "Aprobado", paymentChannel, "Transaccion aprobada");
//		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
//		transactionResponse.add(new TransactionResponse("00", "Aprobado", paymentChannel, "Transaccion aprobada", true));
//		transactionResponse.add(new TransactionResponse("01", "Llamar al banco", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("19", "Trate de nuevo", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("67", "Retenga y llame", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("85", "No aprobado", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null, false));
//		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null, false)); //Default
		
		transactionResponse.forEach((transactionR) -> {
			session.beginTransaction();
			session.save(transactionR);
			session.getTransaction().commit();
			
		});
		session.close();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 200 de ISO Pagos Movistar
	 */
	public static void movistarFinancialReq0200() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 15);
		//**
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		//**
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.COMPUTED, null, null, "loadField12", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, null, "loadField13", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_date", 15, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, "loadField15", null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("capture_date", 17, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, null, "loadField17", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "010", null, "MovistarRecharge");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "729", null, "MovistarRecharge");
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0007299999999999999=0000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 12, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, true, false, true, "                ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("carda_name_loc", 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null, null, null, null, true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS     DC VE", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("retailer_data", 48, IsoType.LLLVAR, 27, FieldLoadType.COMPUTED, null, null, null, null, false, false, true, "1234567890                 ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("currency_code", 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "937", null, "MovistarRecharge");
		isoTemplate.insertIsoField("additional_amounts", 54, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_data", 60, IsoType.LLLVAR, 16, FieldLoadType.FIXED, null, null, null, null, false, false, true, "G729TES10000    ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("category_response", 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0729TES100000000000", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("additional_data", 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("crt_auth_data", 121, IsoType.LLLVAR, 20, FieldLoadType.FIXED, null, null, null, null, false, false, true, "12345600000000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("batch_shift_data", 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_data", 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "P 0729TELC00", null, "MovistarRecharge");
		isoTemplate.insertIsoField("preauth_data", 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00000000000000000000000000000000000000", null, "MovistarRecharge");

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField03 = isoTemplate.getTemplate_field().get("pcode");
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("additional_data");
		session.beginTransaction();
		
		isoTemplate.insertIsoSubField("operation_type", parentField03, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("account_type", parentField03, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("pcode_fixed", parentField03, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MovistarRecharge", 3);
		
		isoTemplate.insertIsoSubField("token_header", parentField63, IsoType.ALPHA, 22, FieldLoadType.FIXED, null, null, null, null, false, false, true, "& 0000200116!0X0000940", null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("account_number", parentField63, IsoType.ALPHA, 19, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("filler_one", parentField63, IsoType.ALPHA, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "                   ", null, "MovistarRecharge", 3);
		isoTemplate.insertIsoSubField("product_code", parentField63, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 4);
		isoTemplate.insertIsoSubField("payment_mode", parentField63, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 5);
		isoTemplate.insertIsoSubField("adjusment_reason", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "  ", null, "MovistarRecharge", 6);
		isoTemplate.insertIsoSubField("filler_two", parentField63, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000", null, "MovistarRecharge", 7);
		isoTemplate.insertIsoSubField("filler_three", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "  ", null, "MovistarRecharge", 8);
		isoTemplate.insertIsoSubField("payment_channel", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "06", null, "MovistarRecharge", 9);
		isoTemplate.insertIsoSubField("commission_amount", parentField63, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000000000000000", null, "MovistarRecharge", 10);
		isoTemplate.insertIsoSubField("approved_code", parentField63, IsoType.ALPHA, 10, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000000000", null, "MovistarRecharge", 11);
		isoTemplate.insertIsoSubField("client_balance", parentField63, IsoType.ALPHA, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000000", null, "MovistarRecharge", 12);
		isoTemplate.insertIsoSubField("retail_code", parentField63, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0123", null, "MovistarRecharge", 13);
	
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 210 de ISO Pagos Movistar
	 */
	public static void movistarFinancialReq0210() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 16);
		//**
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		//**
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.COMPUTED, null, null, "loadField12", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, null, "loadField13", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_date", 15, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, "loadField15", null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("capture_date", 17, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null, null, "loadField17", null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "010", null, "MovistarRecharge");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "729", null, "MovistarRecharge");
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0007299999999999999=0000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 12, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 6, FieldLoadType.NONE, null, null, null, null, true, true, false, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 2, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, true, false, true, "                ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("carda_name_loc", 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null, null, null, null, true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS     DC VE", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("retailer_data", 48, IsoType.LLLVAR, 27, FieldLoadType.COMPUTED, null, null, null, null, false, false, true, "1234567890                 ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("currency_code", 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "937", null, "MovistarRecharge");
		isoTemplate.insertIsoField("additional_amounts", 54, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_data", 60, IsoType.LLLVAR, 16, FieldLoadType.FIXED, null, null, null, null, false, false, true, "G729TES10000    ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("category_response", 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0729TES100000000000", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("additional_data", 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("crt_auth_data", 121, IsoType.LLLVAR, 20, FieldLoadType.FIXED, null, null, null, null, false, false, true, "12345600000000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("batch_shift_data", 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_data", 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null, null, null, null, false, false, false, "P 0729TELC00", null, "MovistarRecharge");
		isoTemplate.insertIsoField("preauth_data", 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null, null, null, null, false, false, false, "00000000000000000000000000000000000000", null, "MovistarRecharge");

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField03 = isoTemplate.getTemplate_field().get("pcode");
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("additional_data");
		session.beginTransaction();
		
		isoTemplate.insertIsoSubField("operation_type", parentField03, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("account_type", parentField03, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("pcode_fixed", parentField03, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MovistarRecharge", 3);
		
		isoTemplate.insertIsoSubField("token_header", parentField63, IsoType.ALPHA, 22, FieldLoadType.FIXED, null, null, null, null, false, false, true, "& 0000200116!0X0000940", null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("account_number", parentField63, IsoType.ALPHA, 19, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("filler_one", parentField63, IsoType.ALPHA, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "                   ", null, "MovistarRecharge", 3);
		isoTemplate.insertIsoSubField("product_code", parentField63, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 4);
		isoTemplate.insertIsoSubField("payment_mode", parentField63, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 5);
		isoTemplate.insertIsoSubField("adjusment_reason", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "  ", null, "MovistarRecharge", 6);
		isoTemplate.insertIsoSubField("filler_two", parentField63, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000", null, "MovistarRecharge", 7);
		isoTemplate.insertIsoSubField("filler_three", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "  ", null, "MovistarRecharge", 8);
		isoTemplate.insertIsoSubField("payment_channel", parentField63, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "06", null, "MovistarRecharge", 9);
		isoTemplate.insertIsoSubField("commission_amount", parentField63, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000000000000000", null, "MovistarRecharge", 10);
		isoTemplate.insertIsoSubField("approved_code", parentField63, IsoType.ALPHA, 10, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0000000000", null, "MovistarRecharge", 11);
		isoTemplate.insertIsoSubField("client_balance", parentField63, IsoType.ALPHA, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000000", null, "MovistarRecharge", 12);
		isoTemplate.insertIsoSubField("retail_code", parentField63, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0123", null, "MovistarRecharge", 13);
	
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 420 de ISO Pagos Movistar
	 */
	public static void movistarReversalReq0420(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 17);
		//**
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal", null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		//**
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_date", 15, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("capture_date", 17, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "010", null, "MovistarRecharge");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "729", null, "MovistarRecharge");
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0007299999999999999=0000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 12, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 6, FieldLoadType.PREVTRXRESP, null, null, null, null, true, true, false, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 2, FieldLoadType.FIXED, null, null, null, null, true, false, true, "68", null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, true, false, true, "                ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("carda_name_loc", 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null, null, null, null, true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS     DC VE", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("retailer_data", 48, IsoType.LLLVAR, 27, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, "1234567890                 ", null, "MovistarRecharge");
		isoTemplate.insertIsoField("currency_code", 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "937", null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_data", 60, IsoType.LLLVAR, 16, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("category_response", 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0729TES100000000000", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("additional_data", 63, IsoType.LLLVAR, 60, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("orig_data_elem", 90, IsoType.ALPHA, 42, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("rec_institution_code", 100, IsoType.LLVAR, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "729", null, "MovistarRecharge");
		isoTemplate.insertIsoField("crt_auth_data", 121, IsoType.LLLVAR, 20, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, "12345600000000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("invoice_data", 123, IsoType.LLLVAR, 10, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, "0000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("batch_shift_data", 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null, null, null, null, false, false, true, "000000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("settlement_data", 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null, null, null, null, false, false, true, "P 0729TELC00", null, "MovistarRecharge");
		isoTemplate.insertIsoField("preauth_data", 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00000000000000000000000000000000000000", null, "MovistarRecharge");

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField90 = isoTemplate.getTemplate_field().get("orig_data_elem");
		session.beginTransaction();
		
		isoTemplate.insertIsoSubField("orig_trans_type", parentField90, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0200", null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("orig_ref_code", parentField90, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("orig_date", parentField90, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 3);
		isoTemplate.insertIsoSubField("orig_time", parentField90, IsoType.ALPHA, 8, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 4);
		isoTemplate.insertIsoSubField("orig_capt_date", parentField90, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 5);
		isoTemplate.insertIsoSubField("orig_appr_code", parentField90, IsoType.ALPHA, 10, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 6);
		
	
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 430 de ISO Pagos Movistar
	 */
	public static void movistarReversalReq0430(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 18);
		//**
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal", null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		//**
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		isoTemplate.insertIsoField("settlement_date", 15, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null, "loadField15", null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "010", null, "MovistarRecharge");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "729", null, "MovistarRecharge");
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0007299999999999999=0000000", null, "MovistarRecharge");
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 12, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null, null, null, null, true, false, true, "                ", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("currency_code", 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "937", null, "MovistarRecharge");
		isoTemplate.insertIsoField("category_response", 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0729TES100000000000", null, "MovistarRecharge");
		//**
		isoTemplate.insertIsoField("additional_data", 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("orig_data_elem", 90, IsoType.ALPHA, 42, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MovistarRecharge");

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField90 = isoTemplate.getTemplate_field().get("orig_data_elem");
		session.beginTransaction();
		
		isoTemplate.insertIsoSubField("orig_trans_type", parentField90, IsoType.ALPHA, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0200", null, "MovistarRecharge", 1);
		isoTemplate.insertIsoSubField("orig_ref_code", parentField90, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 2);
		isoTemplate.insertIsoSubField("orig_date", parentField90, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 3);
		isoTemplate.insertIsoSubField("orig_time", parentField90, IsoType.ALPHA, 8, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 4);
		isoTemplate.insertIsoSubField("orig_capt_date", parentField90, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 5);
		isoTemplate.insertIsoSubField("orig_appr_code", parentField90, IsoType.ALPHA, 10, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MovistarRecharge", 6);
		
	
		session.getTransaction().commit();
	}
	
	public static void movistarEchoTest0800() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 19);
	
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("net_management_code", 70, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "301", null, "MovistarRecharge");
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void movistarEchoTest0810() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 20);
	
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 10, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");	
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 2, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MovistarRecharge");
		isoTemplate.insertIsoField("net_management_code", 70, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "301", null, "MovistarRecharge");
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 200 de ISO Boton de Pago Credicard
	 */
	public static void creditSale0200() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 3);
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 0, FieldLoadType.WEBSERVICE, null, null, null, "maskPan", true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, "strToInteger", null, null, null, false, false, true, "3000", null, "CpagoCc");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0812", null, "CpagoCc");
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null, null, null, null, false, false, true, "003", null, "CpagoCc");
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "CpagoCc");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 0, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "CpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "CpagoCc");
		isoTemplate.insertIsoField("sec_cod_ci", 48, IsoType.LLLLVAR, 14, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC,  18, FieldLoadType.COMPUTED, "encodeBcd", null, "calculateReference", "decodeRef", true, true, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("batch", 63, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001000083337303030303031", null, "CpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField48 = isoTemplate.getTemplate_field().get("sec_cod_ci");
		session.beginTransaction();
		isoTemplate.insertIsoSubField("sec_cod", parentField48, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "CpagoCc", 1);
		isoTemplate.insertIsoSubField("ci", parentField48, IsoType.NUMERIC, 10, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "CpagoCc", 2);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 210 de ISO Boton de Pago Credicard
	 */
	public static void creditSale0210() { 
		Session session = HibernateUtil.getSessionFactory().openSession(); 
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 11);
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.NONE, null, null, "fromBigDecimal", null, true, true, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, true, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("rrn", 37, IsoType.ALPHA, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null, "decodeBcd", null, null, true, true, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.NONE, null, "decodeBcd", null, null, false, false, true, null, null, "CpagoCc");
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
		
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 400 de ISO Boton de Pago Credicard
	 */
	public static void autoReverse0400() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 8);
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 0, FieldLoadType.PREVTRXREQ, null, null, null, "maskPan", true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, "strToInteger", null, null, null, false, false, true, "3000", null, "CpagoCc");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0812", null, "CpagoCc");
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003", null, "CpagoCc");
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "CpagoCc");
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 0, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "CpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "CpagoCc");
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC,  16, FieldLoadType.COMPUTED, "encodeBcd", null, "calculateReference", "decodeRef", true, true, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("batch", 63, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001000083337303030303031", null, "CpagoCc");
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 410 de ISO Boton de Pago Credicard
	 */
	public static void autoReverse0410() {
		Session session = HibernateUtil.getSessionFactory().openSession(); 
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 9);
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.NONE, "strToInteger", null, null, null, false, false, true, "3000", null, "CpagoCc");
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.NONE, "toBigDecimal", null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("rrn", 37, IsoType.ALPHA, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.NONE, null, "decodeBcd", null, null, false, false, true, null, null, "CpagoCc");
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 800 de ISO Boton de Pago Credicard
	 */
	public static void echoTest0800() {
		Session session = HibernateUtil.getSessionFactory().openSession(); 
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 6);
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, "strToInteger", null, null, null, false, false, true, "990000", null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "CpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "CpagoCc");
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 810 de ISO Boton de Pago Credicard
	 */
	public static void echoTest0810() {
		Session session = HibernateUtil.getSessionFactory().openSession(); 
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 7);
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, "strToInteger", null, null, null, false, false, true, "990000", null, "CpagoCc");
		isoTemplate.insertIsoField("date_time", 7, IsoType.DATE10, 0, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "CpagoCc");
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "CpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "CpagoCc");
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "CpagoCc");
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0200 de Mensajeria Credicard (Solicitud compra)
	 */
	public static void ccrPurchase0200() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 10);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 19, FieldLoadType.WEBSERVICE, null, null, null, "maskPan", true, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode", null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 14 Fecha de vencimiento
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 22 Modo de ingreso POS
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, true, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MpagoCc");
		// 32 Codigo adquiriente
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "MpagoCc");
		// 35 Track 2
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 37, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 40 Serial del terminal (Opcional si se envia el 57)
		//isoTemplate.insertIsoField("terminal_serial", 40, IsoType.ALPHA, 8, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 42 Numero de afiliado
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		// 45 Track 1
		isoTemplate.insertIsoField("track_1", 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 46 Data adicional (*Que lleva, descripcion?)
		//isoTemplate.insertIsoField("additional_data", 46, IsoType.LLLVAR, 15, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 47 Version aplicacion (*Asignada en conjunto con Credicard, extraer de la base de datos)
		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		// 48 CVC2/CVV2/Codigo alpha - CI (CVV??)
		isoTemplate.insertIsoField("sec_cod_ci", 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 52 PIN Ecriptado
		isoTemplate.insertIsoField("enc_pin", 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, false, true, null, null, "MpagoCc");
		// 56 Literal de moneda
		//isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, false, "BSF.", null, "MpagoCc");
		// 57 Dato adicional POS (*Subcampos)
		isoTemplate.insertIsoField("pos_add_data", 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 62 Referencia del POS
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC, 18, FieldLoadType.COMPUTED, "encodeBcd", null, "calculateReference", "decodeRef", true, true, true, null, null, "MpagoCc");
		// 63 KSN/Nro Lote (*Subcampos)
		isoTemplate.insertIsoField("field_63", 63, IsoType.ALPHA, 72, FieldLoadType.SUBFIELD, "calcField63Len", null, null, null, true, false, true, null, null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField22 = isoTemplate.getTemplate_field().get("pos_entry");
		TemplateField parentField48 = isoTemplate.getTemplate_field().get("sec_cod_ci");
		TemplateField parentField57 = isoTemplate.getTemplate_field().get("pos_add_data");
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("field_63");
		session.beginTransaction();
		// Subcampos de 22
		// Modo de entrada del PAN en el POS
		isoTemplate.insertIsoSubField("entry_mode_pan", parentField22, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, "optToPanEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 1);
		// Modo de entrada del PIN en el POS
		isoTemplate.insertIsoSubField("entry_mode_pin", parentField22, IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 2);

		// Subcampos de 48
		// Longitud fija
		isoTemplate.insertIsoSubField("sec_ci_len", parentField48, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0014", null, "MpagoCc", 1);
		// CVC2/CVV2/Codigo alpha
		isoTemplate.insertIsoSubField("sec_cod", parentField48, IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec", null, null, null, false, false, true, null, null, "MpagoCc", 2);
		// CI
		isoTemplate.insertIsoSubField("ci", parentField48, IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi", null, null, null, false, false, true, null, null, "MpagoCc", 3);
		
		// Subcampos de 57
		// Capacidad EMV
		isoTemplate.insertIsoSubField("pos_add_data_len", parentField57, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0029", null, "MpagoCc", 1);
		// Capacidad EMV
		isoTemplate.insertIsoSubField("emv_cap", parentField57, IsoType.NUMERIC, 9, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003303231", null, "MpagoCc", 2);
		// Serial del dispositivo 
		isoTemplate.insertIsoSubField("device_serial", parentField57, IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03", null, null, null, false, false, true, null, null, "MpagoCc", 3);
		
		
		// TODO Subcampos de 63 (Fijos temporalmente)
		// Numero de lote (*Sacar de la base de datos)
		// TODO Lote dinamico y no fixed 
		isoTemplate.insertIsoSubField("batch", parentField63, IsoType.ALPHA, 20, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00083337303030303031", null, "MpagoCc", 2);
		// KSN (*Con que se cifra el pinblock) Temporalmente comentado 
		isoTemplate.insertIsoSubField("ksn", parentField63, IsoType.ALPHA, 48, FieldLoadType.WEBSERVICE, "encodeBcdKSN", null, null, null, false, false, false, null, null, "MpagoCc", 1);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0210 de Mensajeria Credicard (Respuesta compra)
	 */
	public static void ccrPurchase0210() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 11);
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode", null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.NONE, null, null, null, "ltStoreFormat", true, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.NONE, null, null, null, "ldStoreFormat", true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, false, "00", null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "MpagoCc");
		// 38 Codigo de autorizacion
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null, null, "decodeBcd", null, true, true, false, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, null, "decodeBcd", null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, true, true, null, null, "MpagoCc");
		// 56 Literal de moneda
		isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, false, "BSF.", null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0220 de Mensajeria Credicard (Advice)
	 */
	public static void ccrAdvice0220() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 12);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan", null, null, "maskPan", true, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.LASTTRXREQ, "pCodeToPcodeAdv", null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.LASTTRXREQ, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.LASTTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 14 Fecha de vencimiento
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 22 Modo de ingreso POS
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MpagoCc");
		// 32 Codigo adquiriente
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "MpagoCc");
		// 38 Codigo de autorizacion
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 12, FieldLoadType.LASTTRXRESP, "encodeBcd", null, "decodeBcd", null, true, true, false, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.LASTTRXRESP, "encodeBcd", null, "decodeBcd", null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.LASTTRXREQ, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 42 Numero de afiliado
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.LASTTRXREQ, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		// 45 Track 1
		isoTemplate.insertIsoField("track_1", 45, IsoType.LLVAR, 76, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 47 Version aplicacion (*Asignada en conjunto con Credicard, extraer de la base de datos)
		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0220", null, null, null, false, false, true, null, null, "MpagoCc");
		// 57 Dato adicional POS (*Subcampos)
		isoTemplate.insertIsoField("pos_add_data", 57, IsoType.NUMERIC, 62, FieldLoadType.LASTTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 62 Referencia del POS
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC, 16, FieldLoadType.LASTTRXREQ, "encodeloadedRef", null, null, "decodeRef", true, true, true, null, null, "MpagoCc");
		// 63 KSN/Nro Lote (*Subcampos)
		isoTemplate.insertIsoField("field_63", 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("field_63");
		session.beginTransaction();
		
		// Subcampos de 63 (Fijos temporalmente)
		// KSN (*Con que se cifra el pinblock) Temporalmente comentado 
		//isoTemplate.insertIsoSubField("ksn", parentField63, IsoType.LLLLVAR, 22, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc", 1);
		// Numero de lote (*Sacar de la base de datos)
		isoTemplate.insertIsoSubField("batch", parentField63, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001000083337303030303031", null, "MpagoCc", 2);
		
		session.getTransaction().commit();
	}

	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0230 de Mensajeria Credicard (Respuesta advice)
	 */
	public static void ccrAdvice0230() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 13);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 19, FieldLoadType.WEBSERVICE, "formatRecPan", null, null, null, false, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode", null, null, null, false, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, false, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, false, "00", null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "MpagoCc");
		// 38 Codigo de autorizacion
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null, "decodeBcd", null, null, true, true, false, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, false, false, true, null, "terminalBankId", "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, true, false, null, null, "MpagoCc");
		// 56 Literal de moneda
		isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "BSF.", null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0400 de Mensajeria Credicard (Solicitud reverso automatico interno)
	 */
	public static void ccrAutoReverse0400() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 14);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 19, FieldLoadType.WEBSERVICE, null, null, null, "maskPan", true, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.LASTEXECTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 14 Fecha de vencimiento
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 22 Modo de ingreso POS
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, true, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MpagoCc");
		// 32 Codigo adquiriente
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "MpagoCc");
		// 35 Track 2
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 37, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 40 Serial del terminal (Opcional si se envia el 57)
		//isoTemplate.insertIsoField("terminal_serial", 40, IsoType.ALPHA, 8, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 42 Numero de afiliado
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		// 45 Track 1
		isoTemplate.insertIsoField("track_1", 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 46 Data adicional (*Que lleva, descripcion?)
		//isoTemplate.insertIsoField("additional_data", 46, IsoType.LLLVAR, 15, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 47 Version aplicacion (*Asignada en conjunto con Credicard, extraer de la base de datos)
		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		// 48 CVC2/CVV2/Codigo alpha - CI (CVV??)
		isoTemplate.insertIsoField("sec_cod_ci", 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 52 PIN Ecriptado
		isoTemplate.insertIsoField("enc_pin", 52, IsoType.BINARY, 64, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, false, true, null, null, "MpagoCc");
		// 56 Literal de moneda
		//isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, false, "BSF.", null, "MpagoCc");
		// 57 Dato adicional POS (*Subcampos)
		isoTemplate.insertIsoField("pos_add_data", 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 62 Referencia del POS
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC, 18, FieldLoadType.COMPUTED, "encodeBcd", null, "calculateReference", "decodeRef", true, true, true, null, null, "MpagoCc");
		// 63 KSN/Nro Lote (*Subcampos)
		isoTemplate.insertIsoField("field_63", 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
//		TemplateField parentField22 = isoTemplate.getTemplate_field().get("pos_entry");
		TemplateField parentField48 = isoTemplate.getTemplate_field().get("sec_cod_ci");
		TemplateField parentField57 = isoTemplate.getTemplate_field().get("pos_add_data");
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("field_63");
		session.beginTransaction();
		// Subcampos de 22
		// Modo de entrada del PAN en el POS
//		isoTemplate.insertIsoSubField("entry_mode_pan", parentField22, IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, "optToPanEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 1);
		// Modo de entrada del PIN en el POS
//		isoTemplate.insertIsoSubField("entry_mode_pin", parentField22, IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 2);

		// Subcampos de 48
		// Longitud fija
		isoTemplate.insertIsoSubField("sec_ci_len", parentField48, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0014", null, "MpagoCc", 1);
		// CVC2/CVV2/Codigo alpha
		isoTemplate.insertIsoSubField("sec_cod", parentField48, IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec", null, null, null, false, false, true, null, null, "MpagoCc", 2);
		// CI
		isoTemplate.insertIsoSubField("ci", parentField48, IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi", null, null, null, false, false, true, null, null, "MpagoCc", 3);
		
		// Subcampos de 57
		// Capacidad EMV
		isoTemplate.insertIsoSubField("pos_add_data_len", parentField57, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0029", null, "MpagoCc", 1);
		// Capacidad EMV
		isoTemplate.insertIsoSubField("emv_cap", parentField57, IsoType.NUMERIC, 10, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003303231", null, "MpagoCc", 2);
		// Serial del dispositivo 
		isoTemplate.insertIsoSubField("device_serial", parentField57, IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03", null, null, null, false, false, true, null, null, "MpagoCc", 3);
		
		
		// Subcampos de 63
		// KSN (*Con que se cifra el pinblock) Temporalmente comentado 
		isoTemplate.insertIsoSubField("ksn", parentField63, IsoType.LLLLVAR, 22, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc", 1);
		isoTemplate.insertIsoSubField("batch", parentField63, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001000083337303030303031", null, "MpagoCc", 2);
		
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0410 de Mensajeria Credicard (Respuesta reverso automatico interno)
	 */
	public static void ccrAutoReverse0410() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 15);
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.NONE, null, null, null, "ltStoreFormat", true, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.NONE, null, null, null, "ldStoreFormat", true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", "decodeBcd", null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, true, true, null, null, "MpagoCc");
		// 56 Literal de moneda
		isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, false, "BSF.", null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0400 de Mensajeria Credicard (Solicitud reverso automatico desde el dispositivo)
	 */
	public static void ccrReverse0400() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 16);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.LLVAR, 19, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 14 Fecha de vencimiento
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 22 Modo de ingreso POS
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, true, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MpagoCc");
		// 32 Codigo adquiriente
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "MpagoCc");
		// 35 Track 2
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 37, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 42 Numero de afiliado
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		// 45 Track 1
		isoTemplate.insertIsoField("track_1", 45, IsoType.LLVAR, 76, FieldLoadType.PREVTRXREQ, null, null, null, null, true, false, false, null, null, "MpagoCc");
		// 47 Version aplicacion (*Asignada en conjunto con Credicard, extraer de la base de datos)
		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001052503030303130313030", null, "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, false, true, null, null, "MpagoCc");
		// 57 Dato adicional POS (*Subcampos)
		isoTemplate.insertIsoField("pos_add_data", 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 62 Referencia del POS
		isoTemplate.insertIsoField("pos_ref", 62, IsoType.NUMERIC, 18, FieldLoadType.PREVTRXREQ, "encodeBcd", null, null, null, true, true, true, null, null, "MpagoCc");
		// 63 KSN/Nro Lote (*Subcampos)
		isoTemplate.insertIsoField("field_63", 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null, null, null, null, true, false, true, null, null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField22 = isoTemplate.getTemplate_field().get("pos_entry");
		TemplateField parentField57 = isoTemplate.getTemplate_field().get("pos_add_data");
		TemplateField parentField63 = isoTemplate.getTemplate_field().get("field_63");
		session.beginTransaction();
		// Subcampos de 22
		// Modo de entrada del PAN en el POS
		isoTemplate.insertIsoSubField("entry_mode_pan", parentField22, IsoType.NUMERIC, 2, FieldLoadType.PREVTRXREQ, "optToPanEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 1);
		// Modo de entrada del PIN en el POS
		isoTemplate.insertIsoSubField("entry_mode_pin", parentField22, IsoType.NUMERIC, 1, FieldLoadType.PREVTRXREQ, "optToPinEntMode", null, null, null, false, false, true, null, null, "MpagoCc", 2);

		// Subcampos de 57
		// Capacidad EMV
		isoTemplate.insertIsoSubField("pos_add_data_len", parentField57, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0029", null, "MpagoCc", 1);
		// Capacidad EMV
		isoTemplate.insertIsoSubField("emv_cap", parentField57, IsoType.NUMERIC, 10, FieldLoadType.FIXED, null, null, null, null, false, false, true, "0003303231", null, "MpagoCc", 2);
		// Serial del dispositivo 
		isoTemplate.insertIsoSubField("device_serial", parentField57, IsoType.NUMERIC, 48, FieldLoadType.PREVTRXREQ, "toTableId03", null, null, null, false, false, true, null, null, "MpagoCc", 3);
		
		//Subcampos de 63
		isoTemplate.insertIsoSubField("batch", parentField63, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001000083337303030303031", null, "MpagoCc", 1);
		
		session.getTransaction().commit();
	}
	
	/**
	 * Almacenamiento de una Plantilla ISO para un mensaje 0410 de Mensajeria Credicard (Respuesta reverso automatico desde el dispositivo)
	 */
	public static void ccrReverse0410() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 17);
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, true, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, true, false, true, null, null, "MpagoCc");
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.NONE, null, null, null, "ltStoreFormat", true, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.NONE, null, null, null, "ldStoreFormat", true, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, true, false, true, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", "decodeBcd", null, true, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, true, true, null, null, "MpagoCc");
		// 56 Literal de moneda
		isoTemplate.insertIsoField("currency_lit", 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null, null, null, null, false, false, false, "BSF.", null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void ccrAnnulment0200() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x200, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType =  (TransactionType)session.load(TransactionType.class, 24);
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_REQ" , "Mensaje de Solicitud - Anulacion Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "annulment" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.SUBTRACT, false, false, true, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcodeAnnul" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null , null , null , "ltStoreFormat" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null , null , null , "ldStoreFormat" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "441132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "determineTerminal" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200A" , "parseEncTlv0210A" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );		
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , true, false, true, null , null , "MpagoCc" , 3);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 1);
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();


	}
	
	public static void ccrAnnulment0210() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x210, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null ); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_RESP" , "Mensaje de Respuesta - Anulacion Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "annulmentResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, true, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

	}
	
	public static void ccrClosing0500() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null ); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType =  (TransactionType)session.load(TransactionType.class, 26);
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_REQ" , "Mensaje de Solicitud - Cierre Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "920000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal", "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff", "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		isoTemplate.insertIsoField( "app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		isoTemplate.insertIsoField( "batch_number", 60, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, null, null, "loadBatchNumber", null, true, true, true, null, null, "MpagoCc");
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.COMPUTED, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void ccrClosing0510() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null ); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);
		
		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_RESP" , "Mensaje de Respuesta - Cierre Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, true, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void ccrBatchUpload320() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 28);
		// 02 Numero de tarjeta
		isoTemplate.insertIsoField("pan", 2, IsoType.ALPHA, 22, FieldLoadType.PREVTRXREQ, "formatPan", null, null, "maskPan", false, false, false, null, null, "MpagoCc");
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal", null, null, null, false, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 14 Fecha de vencimiento
		isoTemplate.insertIsoField("expires", 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 22 Modo de ingreso POS
		isoTemplate.insertIsoField("pos_entry", 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 24 NII
		isoTemplate.insertIsoField("nii", 24, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, "0003", null, "MpagoCc");
		// 25 POS Condition Code
		isoTemplate.insertIsoField("pos_cond", 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null, null, null, null, false, false, true, "00", null, "MpagoCc");
		// 32 Codigo adquiriente
		isoTemplate.insertIsoField("acquirer_code", 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null, null, null, null, false, false, true, "441132", null, "MpagoCc");
		// 35 Track 2
		isoTemplate.insertIsoField("track_2", 35, IsoType.LLVAR, 37, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.ALPHA, 24, FieldLoadType.PREVTRXRESP, "encodeBcd", "decodeBcd", null, null, false, false, true, null, null, "MpagoCc");
		// 38 Codigo de autorizacion
		isoTemplate.insertIsoField("auth_code", 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd", null, "decodeBcd", null, false, false, true, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, "encodeBcd", null, "decodeBcd", null, false, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd", "decodeBcd", null, null, false, false, true, null, "terminalBankId", "MpagoCc");
		// 42 Numero de afiliado
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff", "decodeBcd", null, null, false, false, true, null, "merchantIdCode", "MpagoCc");
		// 47 Version aplicacion (*Asignada en conjunto con Credicard, extraer de la base de datos)
		isoTemplate.insertIsoField("app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.PREVTRXREQ, "parseEncTlv0220", null, null, null, false, false, true, null, null, "MpagoCc");
		// 57 Dato adicional POS (*Subcampos)
		isoTemplate.insertIsoField("pos_add_data", 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 60 Datos originales de la transaccion
		isoTemplate.insertIsoField("orig_data", 60, IsoType.ALPHA, 48, FieldLoadType.SUBFIELD, "calcField60Len", null, null, null, false, false, true, null, null, "MpagoCc");
		// 62 Referencia del POS
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , false, false, true, null , null , "MpagoCc" );
		// 63 Lote
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
		TemplateField parentField60 = isoTemplate.getTemplate_field().get("orig_data");
		session.beginTransaction();
		// Subcampos de 60
		// Tipo de mensaje original
		isoTemplate.insertIsoSubField("orig_type", parentField60, IsoType.ALPHA, 8, FieldLoadType.FIXED, null, null, null, null, false, false, true, "30323030", null, "MpagoCc", 1);
		// Trace original
		isoTemplate.insertIsoSubField("trace", parentField60, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd", null, null, null, false, false, true, null, null, "MpagoCc", 2);
		// Reservado
		isoTemplate.insertIsoSubField("orig_reserv", parentField60, IsoType.ALPHA, 24, FieldLoadType.FIXED, null, null, null, null, false, false, true, "202020202020202020202020", null, "MpagoCc", 3);
		session.getTransaction().commit();
	}
	
	public static void ccrBatchUpload330() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, 29);
		// 03 Codigo de proceso
		isoTemplate.insertIsoField("pcode", 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode", null, null, null, false, false, true, null, null, "MpagoCc");
		// 04 Monto
		isoTemplate.insertIsoField("amount", 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal", null, null, null, false, false, true, null, null, "MpagoCc");
		// 11 Trace
		isoTemplate.insertIsoField("trace", 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 12 Hora local
		isoTemplate.insertIsoField("local_time", 12, IsoType.TIME, 6, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 13 Fecha local
		isoTemplate.insertIsoField("local_date", 13, IsoType.DATE4, 4, FieldLoadType.NONE, null, null, null, null, false, false, true, null, null, "MpagoCc");
		// 23 Card sequence number
		isoTemplate.insertIsoField("card_seq_numb", 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null, null, null, null, false, false, false, null, null, "MpagoCc");
		// 37 RRN
		isoTemplate.insertIsoField("rrn", 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null, "decodeBcd", null, null, false, false, true, null, null, "MpagoCc");
		// 39 Codigo de respuesta
		isoTemplate.insertIsoField("ans_code", 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null, "decodeBcd", null, null, false, false, true, null, null, "MpagoCc");
		// 41 Numero terminal
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd", "decodeBcd", null, null, false, false, true, null, "terminalBankId", "MpagoCc");
		// 55 Data Chip (En el servicio se envia el tlv cifrado como un parametro) (*Longitud) 
		isoTemplate.insertIsoField("chip_data", 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200", "parseEncTlv0210", null, null, false, false, false, null, null, "MpagoCc");
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void ccr2Closing0500() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null ); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType =  (TransactionType)session.load(TransactionType.class, 30);
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_REQ" , "Mensaje de Solicitud - Cierre Trailer Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "960000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField("terminal_id", 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, null, "decodeBcd", null, null, true, false, true, null, "terminalBankId", "MpagoCc");
		isoTemplate.insertIsoField("affiliated_id", 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, null, "decodeBcd", null, null, true, false, true, null, "merchantIdCode", "MpagoCc");
		isoTemplate.insertIsoField( "app_version", 47, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXREQ, null, null, null, null, false, false, true, "001043413030303130323132", null, "MpagoCc");
		isoTemplate.insertIsoField( "batch_number", 60, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, null, null, "loadBatchNumber", null, true, true, true, null, null, "MpagoCc");
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.PREVTRXREQ, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		
		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	public static void ccr2Closing0510() {
		IsoTemplate isoTemplate = null;
		Session session = null;
		Integer isoTemplateId = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null ;
		TransactionType transactionConfirmationType = null ;
		TransactionType transactionType = null ;
		Integer transactionTypeId = null;
		
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10, null ); 
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);
		
		session = HibernateUtil.getSessionFactory().openSession();
		PaymentChannel paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, 4);
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_RESP" , "Mensaje de Respuesta - Cierre Trailer Credicard" , paymentChannel.getTemplateMessage().get(0), isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, true, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();
	}
	
	//--------------------------------------------------------------------------------------------------------------------
	

	
	/**
	 * Escribe en el log la lista de los parametros de una transaccion
	 */
	public static void listTransactionParameters() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = (Transaction)session.get(Transaction.class, 3);
		logger.debug("Transaction: {}", transaction);
		transaction.getParameters().forEach((transactionParameter) -> {
			logger.debug("TransactionParameter: {}", transactionParameter);
		});
	}
	
	/**
	 * Almacenamiento de un Parametro de una Transaccion
	 */
	public static void transactionParameterTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = (Transaction)session.get(Transaction.class, 3);
		TransactionParameter transactionParameter = new TransactionParameter(transaction, "pcode", "3000");
		session.beginTransaction();
		session.save(transactionParameter);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de un parametro de Respuesta de una Transaccion
	 */
	public static void transactionResponseParameterTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = (Transaction)session.get(Transaction.class, 3);
		TransactionResponseParameter transactionResponseParameter = new TransactionResponseParameter(transaction, "auth_code", "123456");
		session.beginTransaction();
		session.save(transactionResponseParameter);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de una Plantilla de Campo
	 */
	public static void templateFieldTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		IsoTemplate isoTemplate = (IsoTemplate)session.load(IsoTemplate.class, 2);
		//isoTemplate.insertIsoField("terminalCcr", 41, IsoType.ALPHA, 16, FieldLoadType.DATABASE, "", "", false);
		//isoTemplate.insertIsoField("affiliated", 42, IsoType.ALPHA, 30, FieldLoadType.DATABASE, "", "", false);
		session.beginTransaction();
		session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de una Transaccion
	 */
	public static void transactionTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Terminal terminal = (Terminal)session.load(Terminal.class, 18);
		TransactionType transactionType = (TransactionType)session.load(TransactionType.class, 2);
		//GeneralCardProduct cardProduct = (GeneralCardProduct)session.load(GeneralCardProduct.class, 1);
		Transaction transaction = new Transaction(
									terminal,
									"145dff44gdh54j4j",
									transactionType,
									"54f5e4gfreg5rgg87"
									);
		logger.debug("Transaction: {}", transaction);
		session.beginTransaction();
		session.save(transaction);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de un Tipo de Tarjeta
	 */
	public static void cardTypeTest() {
		CardType cardType = new CardType("DEBITO");
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(cardType);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de una Franquicia de Tarjeta
	 */
	public static void cardFranchiseTest() {
		CardFranchise cardFranchise = new CardFranchise("VISA");
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(cardFranchise);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de un Producto de Tarjeta
	 */
	public static void cardProductTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		CardFranchise cardFranchise = (CardFranchise)session.load(CardFranchise.class, 1);
		//Bank cardBank = (Bank)session.load(Bank.class, 1);
		CardType cardType = (CardType)session.load(CardType.class, 1);
		Bin bin = new Bin("123456", "1234569");
		GeneralCardProduct cardProduct = new GeneralCardProduct(cardFranchise, cardType, bin);
		
		session.beginTransaction();
		session.save(cardProduct);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Almacenamiento de Bin
	 */
	public static void binTest() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Bin bin = new Bin("123456", "1234569");
		
		session.beginTransaction();
		session.save(bin);
		session.getTransaction().commit();
		session.close();
	}
	
	/**
	 * Listar todas las Reglas almacenadas
	 */
	public static void listRules() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		@SuppressWarnings("unchecked")
		List<Rule> ruleList = session.createQuery("from Rule").list();
		ruleList.forEach((rule) -> {
			logger.debug("Rule: {}", rule);
		});
	}
	
	public static void circuitoMovistar() {
		//Procesador
		Session session = HibernateUtil.getSessionFactory().openSession();
		PaymentProcessor paymentProcessor = (PaymentProcessor)session.load(PaymentProcessor.class, 1);
		session.close();
		
		//Canal de pago
		PaymentChannel paymentChannel = new PaymentChannel("Pagos Movistar", "Recargas y pagos movistar", "REC_MOVISTAR", paymentProcessor, DeviceValidation.SERIAL);
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Integer paymentChannelId = (Integer) session.save(paymentChannel);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nuevo canal de pago: " + paymentChannelId);

		//Plantilla de mensaje
		session = HibernateUtil.getSessionFactory().openSession();
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		TemplateMessage templateMessage = new TemplateMessage(paymentChannel,"terminalBankId");
		session.beginTransaction();
		Integer templateMessageId = (Integer) session.save(templateMessage);
		paymentChannel.getTemplateMessage().add(templateMessage);
		session.update(paymentChannel);
		session.update(templateMessage);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla de mensaje: " + templateMessageId);

		//Guardar Tipos de transaccion con parametros
		Integer transactionTypeId = 0;
		Integer isoTemplateId = 0;
		TransactionType transactionType = null;
		IsoTemplate isoTemplate = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null;
		TransactionType transactionConfirmationType = null;

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x210, "ISO025000005" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_RECHARGE_RESP" , "Mensaje de Respuesta - Recarga Movistar" , templateMessage, isoTemplate, "creditSaleResp" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 2, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "batch_shift_data" , 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null , null , null , null , true, false, true, "000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "729" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "preauth_data" , 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null , null , null , null , true, false, false, "00000000000000000000000000000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "currency_code" , 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "937" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "date_time" , 7, IsoType.DATE10, 10, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.COMPUTED, null , null , "loadField12" , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "010" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_data" , 60, IsoType.LLLVAR, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, "RPGOTES10000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0007299999999999999=0000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "retailer_data" , 48, IsoType.LLLVAR, 27, FieldLoadType.COMPUTED, null , null , null , null , true, false, true, "1234567890 " , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_date" , 15, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_data" , 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null , null , null , null , true, false, false, "P 0729TELC00" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "category_response" , 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0729TES100000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , "loadField13" , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 12, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 6, FieldLoadType.NONE, null , null , null , null , true, true, false, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "additional_data" , 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "crt_auth_data" , 121, IsoType.LLLVAR, 20, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00000000000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "carda_name_loc" , 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null , null , null , null , true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS DC VE" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "capture_date" , 17, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , "loadField17" , null , false, false, true, null , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "operation_type" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "token_header" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 22, FieldLoadType.FIXED, null , null , null , null , true, false, true, "& 0000200116!0X0000940" , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "account_type" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "account_number" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 19, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "pcode_fixed" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00" , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "filler_one" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 19, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "product_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 4);
		isoTemplate.insertIsoSubField( "payment_mode" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 5);
		isoTemplate.insertIsoSubField( "adjusment_reason" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 6);
		isoTemplate.insertIsoSubField( "filler_two" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000" , null , "MovistarRecharge" , 7);
		isoTemplate.insertIsoSubField( "filler_three" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 8);
		isoTemplate.insertIsoSubField( "payment_channel" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "06" , null , "MovistarRecharge" , 9);
		isoTemplate.insertIsoSubField( "commission_amount" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000000000000000" , null , "MovistarRecharge" , 10);
		isoTemplate.insertIsoSubField( "approved_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 10, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000000000" , null , "MovistarRecharge" , 11);
		isoTemplate.insertIsoSubField( "client_balance" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 12, FieldLoadType.FIXED, null , null , null , null , true, false, true, "000000000000" , null , "MovistarRecharge" , 12);
		isoTemplate.insertIsoSubField( "retail_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0729" , null , "MovistarRecharge" , 13);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x430, "ISO025000000" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_REVERSAL_RESP" , "Mensaje de Respuesta - Reverso Movistar" , templateMessage, isoTemplate, "reverseResp" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_date" , 15, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "category_response" , 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0729TES100000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "729" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "currency_code" , 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "937" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 12, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "additional_data" , 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "orig_data_elem" , 90, IsoType.ALPHA, 42, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "date_time" , 7, IsoType.DATE10, 10, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "010" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0007299999999999999=0000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "orig_trans_type" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0200" , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "orig_ref_code" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "orig_date" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "orig_time" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 8, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 4);
		isoTemplate.insertIsoSubField( "orig_capt_date" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 5);
		isoTemplate.insertIsoSubField( "orig_appr_code" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 10, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 6);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x420, "ISO025000000" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = (TransactionType) session.get(TransactionType.class, 16) ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_REVERSAL_REQ" , "Mensaje de Solicitud - Reverso Movistar" , templateMessage, isoTemplate, "reverse" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "68" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "batch_shift_data" , 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null , null , null , null , false, false, true, "000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "729" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "preauth_data" , 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00000000000000000000000000000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "currency_code" , 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "937" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXREQ, null , null , "loadField12" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "010" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_data" , 60, IsoType.LLLVAR, 16, FieldLoadType.PREVTRXRESP, "loadTeminal" , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "rec_institution_code" , 100, IsoType.LLVAR, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "729" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0007299999999999999=0000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "retailer_data" , 48, IsoType.LLLVAR, 27, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "1234567890 " , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_date" , 15, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_data" , 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null , null , null , null , false, false, true, "P 0729TELC00" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "category_response" , 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0729TES100000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 12, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 6, FieldLoadType.PREVTRXRESP, null , null , null , null , true, true, false, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "additional_data" , 63, IsoType.LLLVAR, 60, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "crt_auth_data" , 121, IsoType.LLLVAR, 20, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "12345600000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "orig_data_elem" , 90, IsoType.ALPHA, 42, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "invoice_data" , 123, IsoType.LLLVAR, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "carda_name_loc" , 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null , null , null , null , true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS DC VE" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "capture_date" , 17, IsoType.DATE4, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "orig_trans_type" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0200" , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "rrn" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 12, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "local_date" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "local_time" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 8, FieldLoadType.PREVTRXREQ, "fillTime" , null , null , null , false, false, true, null , null , "MovistarRecharge" , 4);
		isoTemplate.insertIsoSubField( "capture_date" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MovistarRecharge" , 5);
		isoTemplate.insertIsoSubField( "additional_data" , isoTemplate.getIsoIdMap().get(90), IsoType.ALPHA, 10, FieldLoadType.PREVTRXRESP, "encodeRef" , null , null , null , false, false, true, null , null , "MovistarRecharge" , 6);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x810, "ISO025000000" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_ECHO_TEST_RESP" , "Mensaje de Respuesta - Echo Test Movistar" , templateMessage, isoTemplate, "echoTestResp" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 2, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "date_time" , 7, IsoType.DATE10, 10, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "net_management_code" , 70, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "301" , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x800, "ISO025000000" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = (TransactionType) session.get(TransactionType.class, 18) ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_ECHO_TEST_REQ" , "Mensaje de Solicitud - Echo Test Movistar" , templateMessage, isoTemplate, "echoTest" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "date_time" , 7, IsoType.DATE10, 10, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "net_management_code" , 70, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , false, false, true, "301" , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x200, "ISO025000000" , "recmovistar" , null , null , null , null, true, 14, null );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = (TransactionType) session.get(TransactionType.class, 14) ;
		transactionInverseType = (TransactionType) session.get(TransactionType.class, 17) ;
		transactionConfirmationType = null ;
		transactionType = new TransactionType( "MOVISTAR_RECHARGE_REQ" , "Mensaje de Requerimiento - Recarga Movistar" , templateMessage, isoTemplate, "creditSale" , 15.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, true, 0.0, false, false, BatchOperation.NONE, false, false, false, null, null, false);
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "batch_shift_data" , 124, IsoType.LLLVAR, 9, FieldLoadType.FIXED, null , null , null , null , true, false, true, "000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "retailer_data" , 48, IsoType.LLLVAR, 27, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, "0010020658 " , "terminalBankId" , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_date" , 15, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , "loadField15" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "settlement_data" , 125, IsoType.LLLVAR, 12, FieldLoadType.FIXED, null , null , null , null , true, false, true, "P 0729TELC00" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "category_response" , 61, IsoType.LLLVAR, 19, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0729TES100000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "729" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "preauth_data" , 126, IsoType.LLLVAR, 38, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00000000000000000000000000000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "currency_code" , 49, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "937" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , "loadField13" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 12, FieldLoadType.COMPUTED, null , null , "calculateRRN" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "additional_data" , 63, IsoType.LLLVAR, 60, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "crt_auth_data" , 121, IsoType.LLLVAR, 20, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00000000000000000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.COMPUTED, null , null , "loadField12" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "carda_name_loc" , 43, IsoType.ALPHA, 40, FieldLoadType.FIXED, null , null , null , null , true, false, true, "KIOSC RAPIDPAGO CHACAO CARACAS DC VE" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "capture_date" , 17, IsoType.DATE4, 4, FieldLoadType.COMPUTED, null , null , "loadField17" , null , true, false, true, null , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 3, FieldLoadType.FIXED, null , null , null , null , true, false, true, "010" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_data" , 60, IsoType.LLLVAR, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, "RPGOTES10000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 27, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0007299999999999999=0000000" , null , "MovistarRecharge" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "operation_type" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "token_header" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 22, FieldLoadType.FIXED, null , null , null , null , true, false, true, "& 0000200116!0X0000940" , null , "MovistarRecharge" , 1);
		isoTemplate.insertIsoSubField( "account_type" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00" , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "account_number" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 19, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 2);
		isoTemplate.insertIsoSubField( "filler_one" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 19, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "pcode_fixed" , isoTemplate.getIsoIdMap().get(3), IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "00" , null , "MovistarRecharge" , 3);
		isoTemplate.insertIsoSubField( "product_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 4);
		isoTemplate.insertIsoSubField( "payment_mode" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MovistarRecharge" , 5);
		isoTemplate.insertIsoSubField( "adjusment_reason" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 6);
		isoTemplate.insertIsoSubField( "filler_two" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000" , null , "MovistarRecharge" , 7);
		isoTemplate.insertIsoSubField( "filler_three" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, " " , null , "MovistarRecharge" , 8);
		isoTemplate.insertIsoSubField( "payment_channel" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 2, FieldLoadType.FIXED, null , null , null , null , true, false, true, "06" , null , "MovistarRecharge" , 9);
		isoTemplate.insertIsoSubField( "commission_amount" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 16, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000000000000000" , null , "MovistarRecharge" , 10);
		isoTemplate.insertIsoSubField( "approved_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 10, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0000000000" , null , "MovistarRecharge" , 11);
		isoTemplate.insertIsoSubField( "client_balance" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 12, FieldLoadType.FIXED, null , null , null , null , true, false, true, "000000000000" , null , "MovistarRecharge" , 12);
		isoTemplate.insertIsoSubField( "retail_code" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0729" , null , "MovistarRecharge" , 13);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Respuestas de transacciones
		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
		transactionResponse.add(new TransactionResponse("00", "Aprobada con exito", paymentChannel, null , true, null, false));
		transactionResponse.add(new TransactionResponse("01", "Llame al emisor de la tarjeta", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("06", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("07", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("09", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("11", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("15", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("30", "Trate de nuevo", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("31", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("33", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("34", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("35", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("36", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("37", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("38", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("39", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("56", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("57", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("65", "Retenga y llame", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("79", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("81", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("83", "No aprobado", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("84", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("85", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("89", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("90", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("99", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("N0", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("N1", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("N2", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("Q5", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("Q6", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("Q7", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("Q8", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("Q9", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R1", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R2", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R3", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R4", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R5", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R6", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R7", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("R8", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S4", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S5", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S6", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S7", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S8", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("S9", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T1", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T2", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T3", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T4", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T5", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T6", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("T7", "Emisor no responde", paymentChannel, null , false, null, false));
		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null , false, null, false));
		transactionResponse.forEach((transactionR) -> {
		Session sessionr = HibernateUtil.getSessionFactory().openSession();
		sessionr.beginTransaction();
		sessionr.save(transactionR);
		sessionr.getTransaction().commit();
		sessionr.close();
		});
	}
	
	public static void circuitoMpagoCCR() {
		
		//Procesador
		Integer paymentProcessorId = 1;
		System.out.println("Id de procesador: " + paymentProcessorId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		PaymentProcessor paymentProcessor = (PaymentProcessor)session.get(PaymentProcessor.class, paymentProcessorId);
		if(paymentProcessor == null) {
		paymentProcessor = new PaymentProcessor("Consorcio Credicard", "socketCcr", "137.1.1.12", "5020");
		session.beginTransaction();
		paymentProcessorId = (Integer) session.save(paymentProcessor);
		session.getTransaction().commit();
		System.out.println("Id de nuevo procesador: " + paymentProcessorId);
		} session.close();

		//Canal de pago
		PaymentChannel paymentChannel = new PaymentChannel( "MPAGO Credicard", "Mensajeria transaccional Credicard", "MENSAJERIA_CCR", paymentProcessor, DeviceValidation.SERIAL);
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Integer paymentChannelId = (Integer) session.save(paymentChannel);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nuevo canal de pago: " + paymentChannelId);

		//Guardar Tipos de lotes
		session = HibernateUtil.getSessionFactory().openSession(); List<BatchType> batchTypes = new ArrayList<BatchType>();
		HashMap<String, BatchType> batchTypesMap = new HashMap<String, BatchType>();
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "1" , "Credito" , "Credito" ));
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "3" , "Amex" , "American Express" ));
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "2" , "Debito" , "Debito" ));
		for(BatchType batchTypeE : batchTypes){
		Session sessionbt = HibernateUtil.getSessionFactory().openSession();
		sessionbt.beginTransaction();
		sessionbt.save(batchTypeE);
		sessionbt.getTransaction().commit();
		sessionbt.close();
		batchTypesMap.put(batchTypeE.getTypeName(), batchTypeE);
		};
		session.close();

		//Plantilla de mensaje
		session = HibernateUtil.getSessionFactory().openSession();
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		TemplateMessage templateMessage = new TemplateMessage(paymentChannel,"terminalBankId");
		session.beginTransaction();
		Integer templateMessageId = (Integer) session.save(templateMessage);
		paymentChannel.getTemplateMessage().add(templateMessage);
		session.update(paymentChannel);
		session.update(templateMessage);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla de mensaje: " + templateMessageId);

		//Guardar Tipos de transaccion con parametros
		Integer transactionTypeId = 0;
		Integer isoTemplateId = 0;
		TransactionType transactionType = null;
		IsoTemplate isoTemplate = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null;
		TransactionType transactionConfirmationType = null;
		TransactionType transactionBatchUploadType = null;
		TransactionType transactionTrailerSettleType = null;
		AdditionalVoucherFields addVoucherF = null;
		TransactionType voucherTransactionType = null;
		BatchType voucherBatchType = null;
		HashMap<String, TransactionType> transactionTypeMap = new HashMap<String, TransactionType>();
		HashMap<String, IsoTemplate> isoTemplateMap = new HashMap<String, IsoTemplate>();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x210, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_PURCHASE_RESP" , "Mensaje de Respuesta - Compra Credicard" , templateMessage, isoTemplate, "creditSaleResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, false, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x230, "6000030000" , "isoemvccr" , "preProcAdvc" , "MpagoCc" , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ADVICE_RESP" , "Mensaje de Respuesta - Advice Credicard" , templateMessage, isoTemplate, "adviceResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, true, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, false, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.LLVAR, 19, FieldLoadType.NONE, null , "formatRecPan" , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x220, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_ADVICE_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ADVICE_REQ" , "Mensaje de Requerimiento - Advice Credicard" , templateMessage, isoTemplate, "advice" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, true, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, "pCodeToPcodeAdv" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeloadedRef" , null , null , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0220" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , "maskPan" , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.BATCH, "encodeBatch" , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 2);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x410, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_REVERSE_RESP" , "Mensaje de Respuesta - Reverso Credicard" , templateMessage, isoTemplate, "reverseResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x410, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_DEV_REVERSAL_RESP" , "Mensaje de Respuesta - Reverso Credicard" , templateMessage, isoTemplate, "reverseResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x400, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_DEV_REVERSAL_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_DEV_REVERSAL_REQ" , "Mensaje de Requerimiento - Reverso Credicard" , templateMessage, isoTemplate, "reverse" , 33.0, true, 1, transactionResponseType, true, false, false, transactionInverseType, false, transactionConfirmationType, false, 15.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 2, IsoType.ALPHA, 22, FieldLoadType.PREVTRXREQ, "loadPanFromTrack2" , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 1);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x400, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_REVERSE_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_REVERSE_REQ" , "Mensaje de Requerimiento - Reverso Credicard" , templateMessage, isoTemplate, "reverse" , 33.0, true, 1, transactionResponseType, true, false, false, transactionInverseType, false, transactionConfirmationType, false, 15.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.LASTEXECTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "sec_cod_ci" , 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, "encodeBcd" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.LLVAR, 19, FieldLoadType.WEBSERVICE, null , null , null , "maskPan" , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , false, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "sec_ci_len" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0014" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "sec_cod" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "ci" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x210, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_RESP" , "Mensaje de Respuesta - Anulacion Credicard" , templateMessage, isoTemplate, "annulmentResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_RESP" , "Mensaje de Respuesta - Cierre Credicard" , templateMessage, isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, true, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x330, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_BATCH_UPLOAD_RESP" , "Mensaje de Solicitud - Batch Upload Credicard" , templateMessage, isoTemplate, "batchUploadResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_RESP" , "Mensaje de Respuesta - Cierre Trailer Credicard" , templateMessage, isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, true, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_SETTLEMENT_TRAILER_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_REQ" , "Mensaje de Solicitud - Cierre Trailer Credicard" , templateMessage, isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.PREVTRXREQ, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "001043413030303130323132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "batch_number" , 60, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, null , null , "loadBatchNumber" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "960000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x320, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_BATCH_UPLOAD_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_BATCH_UPLOAD_REQ" , "Mensaje de Solicitud - Batch Upload Credicard" , templateMessage, isoTemplate, "batchUpload" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001043413030303130323132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "orig_data" , 60, IsoType.ALPHA, 48, FieldLoadType.SUBFIELD, "calcField60Len" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "441132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.ALPHA, 24, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , false, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.PREVTRXREQ, "formatPan" , null , null , "maskPan" , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 37, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "orig_type" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 8, FieldLoadType.FIXED, null , null , null , null , false, false, true, "30323030" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "trace" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "orig_reserv" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "202020202020202020202020" , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_SETTLEMENT_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = transactionTypeMap.get("CCR_BATCH_UPLOAD_REQ") ;
		transactionTrailerSettleType = transactionTypeMap.get("CCR_SETTLEMENT_TRAILER_REQ") ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_REQ" , "Mensaje de Solicitud - Cierre Credicard" , templateMessage, isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.SETTLEMENT, true, true, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.COMPUTED, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001043413030303130323132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "batch_number" , 60, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, null , null , "loadBatchNumber" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "920000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x200, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_ANNULMENT_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_REQ" , "Mensaje de Solicitud - Anulacion Credicard" , templateMessage, isoTemplate, "annulment" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.SUBTRACT, false, true, false, transactionBatchUploadType, transactionTrailerSettleType, true );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcodeAnnul" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null , null , null , "ltStoreFormat" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200A" , "parseEncTlv0210A" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null , null , null , "ldStoreFormat" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , "decodeBcd" , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.BATCH, "encodeBatch" , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , true, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "" , "" , "REQUIERE FIRMA_________" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x200, "6000030000" , "isoemvccr" , null , "MpagoCc" , "valInverseInvoke" , "unblockAdvice" , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_PURCHASE_RESP") ;
		transactionInverseType = transactionTypeMap.get("CCR_REVERSE_REQ") ;
		transactionConfirmationType = transactionTypeMap.get("CCR_ADVICE_REQ") ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_PURCHASE_REQ" , "Mensaje de Requerimiento - Compra Credicard" , templateMessage, isoTemplate, "creditSale" , 33.0, false, 0, transactionResponseType, false, true, true, transactionInverseType, false, transactionConfirmationType, true, 0.0, true, true, BatchOperation.ADD, false, true, true, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 72, FieldLoadType.SUBFIELD, "calcField63Len" , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "sec_cod_ci" , 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, "encodeBcd" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "sec_ci_len" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0014" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "ksn" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 48, FieldLoadType.WEBSERVICE, "encodeBcdKSN" , null , null , null , false, false, false, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "sec_cod" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 20, FieldLoadType.BATCH, "encodeBatch" , null , null , null , true, true, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "ci" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , true, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "" , "" , "REQUIERE FIRMA_________" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();

		//Respuestas de transacciones
		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
		TransactionType transactionRType = null;
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("00", "Aprobado", paymentChannel, "Transaccion aprobada" , true, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("01", "Llamar al banco", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("19", "Trate de nuevo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("67", "Retenga y llame", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("85", "No aprobado", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = (TransactionType)session.load(TransactionType.class, 25);
		transactionResponse.add(new TransactionResponse("98", "Aprobada", paymentChannel, "Aprobada - Anulacion" , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("95", "Descuadre", paymentChannel, "Descuadre en cierre" , false, transactionRType, true ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("56", "Error", paymentChannel, "" , false, transactionRType, false ));
		transactionResponse.forEach((transactionR) -> {
		Session sessionr = HibernateUtil.getSessionFactory().openSession();
		sessionr.beginTransaction();
		sessionr.save(transactionR);
		sessionr.getTransaction().commit();
		sessionr.close();
		});

	}
}
