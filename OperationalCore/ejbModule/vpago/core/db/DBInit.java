package vpago.core.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.solab.iso8583.IsoType;

import vpago.core.transactions.classes.AdditionalVoucherFields;
import vpago.core.transactions.classes.Bank;
import vpago.core.transactions.classes.BankCardProduct;
import vpago.core.transactions.classes.BatchType;
import vpago.core.transactions.classes.Bin;
import vpago.core.transactions.classes.CardFranchise;
import vpago.core.transactions.classes.CardType;
import vpago.core.transactions.classes.GeneralCardProduct;
import vpago.core.transactions.classes.IsoTemplate;
import vpago.core.transactions.classes.PaymentChannel;
import vpago.core.transactions.classes.PaymentProcessor;
import vpago.core.transactions.classes.TemplateMessage;
import vpago.core.transactions.classes.TransactionResponse;
import vpago.core.transactions.classes.TransactionType;
import vpago.core.transactions.classes.types.BatchOperation;
import vpago.core.transactions.classes.types.DeviceValidation;
import vpago.core.transactions.classes.types.FieldLoadType;

/**
 * Metodos para inicializar tablas de la base de datos
 */
public class DBInit {
	private static final Logger logger = LoggerFactory.getLogger(DBInit.class);
	
	public static void main(String[] args) {
		initCredicardMpago();
	}
	
	public static void initCredicardMpago() {
		//Procesador
		Integer paymentProcessorId = 1;
		System.out.println("Id de procesador: " + paymentProcessorId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		PaymentProcessor paymentProcessor = (PaymentProcessor)session.get(PaymentProcessor.class, paymentProcessorId);
		if(paymentProcessor == null) {
		paymentProcessor = new PaymentProcessor("Consorcio Credicard", "socketCcr", "192.168.1.248", "8000");
		session.beginTransaction();
		paymentProcessorId = (Integer) session.save(paymentProcessor);
		session.getTransaction().commit();
		System.out.println("Id de nuevo procesador: " + paymentProcessorId);
		} session.close();

		//Canal de pago
		PaymentChannel paymentChannel = new PaymentChannel( "MPAGO Credicard", "Mensajeria transaccional Credicard", "MENSAJERIA_CCR", paymentProcessor, DeviceValidation.SERIAL);
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Integer paymentChannelId = (Integer) session.save(paymentChannel);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nuevo canal de pago: " + paymentChannelId);

		//Guardar Tipos de lotes
		session = HibernateUtil.getSessionFactory().openSession(); List<BatchType> batchTypes = new ArrayList<BatchType>();
		HashMap<String, BatchType> batchTypesMap = new HashMap<String, BatchType>();
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "2" , "Debito" , "Debito" ));
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "1" , "Credito" , "Credito" ));
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		batchTypes.add( new BatchType( paymentChannel, null, "card_type" , "3" , "Amex" , "American Express" ));
		for(BatchType batchTypeE : batchTypes){
		Session sessionbt = HibernateUtil.getSessionFactory().openSession();
		sessionbt.beginTransaction();
		sessionbt.save(batchTypeE);
		sessionbt.getTransaction().commit();
		sessionbt.close();
		batchTypesMap.put(batchTypeE.getTypeName(), batchTypeE);
		};
		session.close();

		//Plantilla de mensaje
		session = HibernateUtil.getSessionFactory().openSession();
		System.out.println("D:");
		paymentChannel = (PaymentChannel)session.load(PaymentChannel.class, paymentChannelId);
		System.out.println("PaymentChannel cargado: ");
		TemplateMessage templateMessage = new TemplateMessage(paymentChannel,"terminalBankId");
		session.beginTransaction();
		Integer templateMessageId = (Integer) session.save(templateMessage);
		paymentChannel.getTemplateMessage().add(templateMessage);
		session.update(paymentChannel);
		session.update(templateMessage);
		session.getTransaction().commit();
		session.close();
		System.out.println("Id de nueva plantilla de mensaje: " + templateMessageId);

		//Guardar Tipos de transaccion con parametros
		Integer transactionTypeId = 0;
		Integer isoTemplateId = 0;
		TransactionType transactionType = null;
		IsoTemplate isoTemplate = null;
		TransactionType transactionResponseType = null;
		TransactionType transactionInverseType = null;
		TransactionType transactionConfirmationType = null;
		TransactionType transactionBatchUploadType = null;
		TransactionType transactionTrailerSettleType = null;
		AdditionalVoucherFields addVoucherF = null;
		TransactionType voucherTransactionType = null;
		BatchType voucherBatchType = null;
		HashMap<String, TransactionType> transactionTypeMap = new HashMap<String, TransactionType>();
		HashMap<String, IsoTemplate> isoTemplateMap = new HashMap<String, IsoTemplate>();

		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x210, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_PURCHASE_RESP" , "Mensaje de Respuesta - Compra Credicard" , templateMessage, isoTemplate, "creditSaleResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, false, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x230, "6000030000" , "isoemvccr" , "preProcAdvc" , "MpagoCc" , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ADVICE_RESP" , "Mensaje de Respuesta - Advice Credicard" , templateMessage, isoTemplate, "adviceResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, true, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, false, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.LLVAR, 19, FieldLoadType.NONE, null , "formatRecPan" , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x220, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_ADVICE_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ADVICE_REQ" , "Mensaje de Requerimiento - Advice Credicard" , templateMessage, isoTemplate, "advice" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, true, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, "pCodeToPcodeAdv" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeloadedRef" , null , null , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0220" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , "maskPan" , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.BATCH, "encodeBatch" , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 2);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x410, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_REVERSE_RESP" , "Mensaje de Respuesta - Reverso Credicard" , templateMessage, isoTemplate, "reverseResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x410, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_DEV_REVERSAL_RESP" , "Mensaje de Respuesta - Reverso Credicard" , templateMessage, isoTemplate, "reverseResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , "decodeBcd" , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x400, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_REVERSE_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_REVERSE_REQ" , "Mensaje de Requerimiento - Reverso Credicard" , templateMessage, isoTemplate, "reverse" , 33.0, true, 1, transactionResponseType, true, false, false, transactionInverseType, false, transactionConfirmationType, false, 15.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.LASTEXECTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "sec_cod_ci" , 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, "encodeBcd" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.LLVAR, 19, FieldLoadType.WEBSERVICE, null , null , null , "maskPan" , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , false, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "sec_ci_len" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0014" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "sec_cod" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "ci" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x210, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_RESP" , "Mensaje de Respuesta - Anulacion Credicard" , templateMessage, isoTemplate, "annulmentResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_7" , 7, IsoType.DATE10, 10, FieldLoadType.NONE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "currency_lit" , 56, IsoType.LLLVAR, 4, FieldLoadType.FIXED, null , null , null , null , false, false, false, "BSF." , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLVAR, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_RESP" , "Mensaje de Respuesta - Cierre Credicard" , templateMessage, isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, true, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x330, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_BATCH_UPLOAD_RESP" , "Mensaje de Solicitud - Batch Upload Credicard" , templateMessage, isoTemplate, "batchUploadResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x320, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_BATCH_UPLOAD_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_BATCH_UPLOAD_REQ" , "Mensaje de Solicitud - Batch Upload Credicard" , templateMessage, isoTemplate, "batchUpload" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "orig_data" , 60, IsoType.ALPHA, 48, FieldLoadType.SUBFIELD, "calcField60Len" , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "441132" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.ALPHA, 24, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , "decodeBcd" , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , false, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.PREVTRXREQ, "formatPan" , null , null , "maskPan" , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.LLVAR, 37, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , false, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "orig_type" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 8, FieldLoadType.FIXED, null , null , null , null , false, false, true, "30323030" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "trace" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "orig_reserv" , isoTemplate.getIsoIdMap().get(60), IsoType.ALPHA, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "202020202020202020202020" , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x510, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = null ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_RESP" , "Mensaje de Respuesta - Cierre Trailer Credicard" , templateMessage, isoTemplate, "settlementResp" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, true, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "ans_code" , 39, IsoType.ALPHA, 4, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.NONE, null , null , null , "ltStoreFormat" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.NONE, null , null , null , "ldStoreFormat" , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.NONE, null , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_SETTLEMENT_TRAILER_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_TRAILER_REQ" , "Mensaje de Solicitud - Cierre Trailer Credicard" , templateMessage, isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.PREVTRXREQ, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "batch_number" , 60, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, null , null , "loadBatchNumber" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "960000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x500, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_SETTLEMENT_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = transactionTypeMap.get("CCR_BATCH_UPLOAD_REQ") ;
		transactionTrailerSettleType = transactionTypeMap.get("CCR_SETTLEMENT_TRAILER_REQ") ;
		transactionType = new TransactionType( "CCR_SETTLEMENT_REQ" , "Mensaje de Solicitud - Cierre Credicard" , templateMessage, isoTemplate, "settlement" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.SETTLEMENT, true, true, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 124, FieldLoadType.COMPUTED, null , null , "calculateBatchData" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "batch_number" , 60, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, null , null , "loadBatchNumber" , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.FIXED, null , null , null , null , true, false, true, "920000" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_SETTLEMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x200, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_ANNULMENT_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_ANNULMENT_REQ" , "Mensaje de Solicitud - Anulacion Credicard" , templateMessage, isoTemplate, "annulment" , 33.0, false, 0, transactionResponseType, false, false, false, transactionInverseType, false, transactionConfirmationType, false, 0.0, false, false, BatchOperation.SUBTRACT, false, true, false, transactionBatchUploadType, transactionTrailerSettleType, true );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, "accTypeToPcodeAnnul" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_time" , 12, IsoType.TIME, 6, FieldLoadType.PREVTRXRESP, null , null , null , "ltStoreFormat" , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200A" , "parseEncTlv0210A" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.PREVTRXREQ, "toBigDecimal" , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "local_date" , 13, IsoType.DATE4, 4, FieldLoadType.PREVTRXRESP, null , null , null , "ldStoreFormat" , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "rrn" , 37, IsoType.NUMERIC, 24, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "auth_code" , 38, IsoType.ALPHA, 12, FieldLoadType.PREVTRXRESP, "encodeBcd" , "decodeBcd" , "decodeBcd" , null , true, true, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.BATCH, "encodeBatch" , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , true, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "" , "" , "REQUIERE FIRMA_________" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_ANNULMENT_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x200, "6000030000" , "isoemvccr" , null , "MpagoCc" , "valInverseInvoke" , "unblockAdvice" , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_PURCHASE_RESP") ;
		transactionInverseType = transactionTypeMap.get("CCR_REVERSE_REQ") ;
		transactionConfirmationType = transactionTypeMap.get("CCR_ADVICE_REQ") ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_PURCHASE_REQ" , "Mensaje de Requerimiento - Compra Credicard" , templateMessage, isoTemplate, "creditSale" , 33.0, false, 0, transactionResponseType, false, true, true, transactionInverseType, false, transactionConfirmationType, true, 0.0, true, true, BatchOperation.ADD, false, true, true, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 72, FieldLoadType.SUBFIELD, "calcField63Len" , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.WEBSERVICE, "accTypeToPcode" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "sec_cod_ci" , 48, IsoType.NUMERIC, 32, FieldLoadType.SUBFIELD, null , null , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_type" , 0, IsoType.ALPHA, 1, FieldLoadType.NONE, null , null , null , null , false, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.COMPUTED, "encodeBcd" , null , "calculateReference" , "decodeRef" , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.RULE, "encodeBcdAff" , "decodeBcd" , null , null , true, true, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "enc_pin" , 52, IsoType.BINARY, 8, FieldLoadType.WEBSERVICE, null , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pan" , 2, IsoType.ALPHA, 22, FieldLoadType.WEBSERVICE, "formatPan" , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.WEBSERVICE, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 35, IsoType.ALPHA, 40, FieldLoadType.WEBSERVICE, "formatTrack2" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.RULE, "determineTerminal" , "decodeBcd" , null , null , true, true, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "entry_mode_pan" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 3, FieldLoadType.WEBSERVICE, "optToPanEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "sec_ci_len" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0014" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "pos_add_data_len" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0029" , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "ksn" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 48, FieldLoadType.WEBSERVICE, "encodeBcdKSN" , null , null , null , false, false, false, null , null , "MpagoCc" , 1);
		isoTemplate.insertIsoSubField( "entry_mode_pin" , isoTemplate.getIsoIdMap().get(22), IsoType.NUMERIC, 1, FieldLoadType.WEBSERVICE, "optToPinEntMode" , null , null , null , true, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "sec_cod" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 8, FieldLoadType.WEBSERVICE, "encodeBcdSec" , null , null , null , false, false, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "emv_cap" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 10, FieldLoadType.FIXED, null , null , null , null , false, false, true, "0003303231" , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 20, FieldLoadType.BATCH, "encodeBatch" , null , null , null , true, true, true, null , null , "MpagoCc" , 2);
		isoTemplate.insertIsoSubField( "ci" , isoTemplate.getIsoIdMap().get(48), IsoType.NUMERIC, 20, FieldLoadType.WEBSERVICE, "encodeBcdCi" , null , null , null , false, false, true, null , null , "MpagoCc" , 3);
		isoTemplate.insertIsoSubField( "device_serial" , isoTemplate.getIsoIdMap().get(57), IsoType.NUMERIC, 48, FieldLoadType.WEBSERVICE, "toTableId03" , null , null , null , true, false, true, null , null , "MpagoCc" , 3);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "" , "" , "REQUIERE FIRMA_________" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 1);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "NAME" , "" , true, false, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "BANK" , "RIF" , "" , true, false, 2, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_TYPE" , "VOUCHER_NAME" , "" , true, false, 3, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "NAME" , "" , true, false, 4, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "MERCHANT" , "RIF" , "" , true, false, 5, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		voucherTransactionType = transactionTypeMap.get("CCR_PURCHASE_REQ");
		voucherBatchType = (BatchType)session.load(BatchType.class, 2);
		addVoucherF = new AdditionalVoucherFields( "TRANSACTION_PARAMETERS" , "ans_code" , "" , false, true, 1, voucherTransactionType, voucherBatchType, true );
		session.beginTransaction();
		session.save(addVoucherF);
		session.getTransaction().commit();
		//####################################################################################################
		isoTemplate = new IsoTemplate( 0x400, "6000030000" , "isoemvccr" , null , null , null , null , false, 10 );
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		isoTemplateId = (Integer) session.save(isoTemplate);
		session.getTransaction().commit();
		session.close();
		isoTemplateMap.put(String.valueOf(isoTemplateId), isoTemplate);
		System.out.println("Id de nueva plantilla iso: " + isoTemplateId);

		session = HibernateUtil.getSessionFactory().openSession();
		transactionResponseType = transactionTypeMap.get("CCR_DEV_REVERSAL_RESP") ;
		transactionInverseType = null ;
		transactionConfirmationType = null ;
		transactionBatchUploadType = null ;
		transactionTrailerSettleType = null ;
		transactionType = new TransactionType( "CCR_DEV_REVERSAL_REQ" , "Mensaje de Requerimiento - Reverso Credicard" , templateMessage, isoTemplate, "reverse" , 33.0, false, 0, transactionResponseType, true, false, false, transactionInverseType, false, transactionConfirmationType, false, 15.0, false, false, BatchOperation.NONE, false, false, false, transactionBatchUploadType, transactionTrailerSettleType, false );
		session.beginTransaction();
		transactionTypeId = (Integer) session.save(transactionType);
		session.getTransaction().commit();
		session.close();
		transactionTypeMap.put(transactionType.getName(), transactionType);

		//Campos de la plantilla ISO
		session = HibernateUtil.getSessionFactory().openSession();
		isoTemplate = (IsoTemplate)session.get(IsoTemplate.class, isoTemplateId);
		isoTemplate.insertIsoField( "amount" , 4, IsoType.AMOUNT, 0, FieldLoadType.WEBSERVICE, "toBigDecimal" , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "expires" , 14, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "field_63" , 63, IsoType.ALPHA, 24, FieldLoadType.SUBFIELD, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "app_version" , 47, IsoType.NUMERIC, 24, FieldLoadType.FIXED, null , null , null , null , false, false, true, "001052503030303130313030" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pcode" , 3, IsoType.NUMERIC, 6, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_add_data" , 57, IsoType.NUMERIC, 62, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "acquirer_code" , 32, IsoType.LLVAR, 11, FieldLoadType.FIXED, null , null , null , null , false, false, true, "455612" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "card_seq_numb" , 23, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_ref" , 62, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeStoredRef" , null , null , null , true, true, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "nii" , 24, IsoType.NUMERIC, 4, FieldLoadType.FIXED, null , null , null , null , true, false, true, "0003" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "affiliated_id" , 42, IsoType.NUMERIC, 30, FieldLoadType.PREVTRXREQ, "encodeBcdAff" , "decodeBcd" , null , null , true, false, true, null , "merchantIdCode" , "MpagoCc" );
		isoTemplate.insertIsoField( "trace" , 11, IsoType.NUMERIC, 6, FieldLoadType.NONE, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_cond" , 25, IsoType.NUMERIC, 2, FieldLoadType.FIXED, null , null , null , null , false, false, true, "00" , null , "MpagoCc" );
		isoTemplate.insertIsoField( "pos_entry" , 22, IsoType.NUMERIC, 4, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "chip_data" , 55, IsoType.LLLLBIN, 255, FieldLoadType.WEBSERVICE, "parseEncTlv0200" , "parseEncTlv0210" , null , null , false, false, true, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_1" , 45, IsoType.LLVAR, 76, FieldLoadType.PREVTRXREQ, null , null , null , null , true, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "track_2" , 2, IsoType.ALPHA, 22, FieldLoadType.PREVTRXREQ, "loadPanFromTrack2" , null , null , null , false, false, false, null , null , "MpagoCc" );
		isoTemplate.insertIsoField( "terminal_id" , 41, IsoType.NUMERIC, 16, FieldLoadType.PREVTRXREQ, "encodeBcd" , "decodeBcd" , null , null , true, false, true, null , "terminalBankId" , "MpagoCc" );

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//SubCampos de la plantilla ISO
		isoTemplate.insertIsoSubField( "batch" , isoTemplate.getIsoIdMap().get(63), IsoType.ALPHA, 24, FieldLoadType.PREVTRXREQ, null , null , null , null , false, false, true, "001000083337303030303031" , null , "MpagoCc" , 1);

		session.beginTransaction();
		session.update(isoTemplate);
		session.getTransaction().commit();

		//Elementos de voucher adicionales
		addVoucherF = null;
		voucherTransactionType = null;
		voucherBatchType = null;

		//Respuestas de transacciones
		List<TransactionResponse> transactionResponse = new ArrayList<TransactionResponse>();
		TransactionType transactionRType = null;
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("00", "Aprobado", paymentChannel, "Transaccion aprobada" , true, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("01", "Llamar al banco", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("02", "Contacte al banco", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("03", "Comercio invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("04", "Retener y llamar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("05", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("08", "Emisor no responde", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("12", "Transaccion invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("13", "Monto invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("14", "Tarjeta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("19", "Trate de nuevo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("41", "Retener y llamar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("43", "Retenga y llame", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("48", "Imposible procesar", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("51", "Rechazada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("54", "Tarjeta vencida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("55", "Clave invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("58", "Transaccion no permitida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("61", "Excede limite diario", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("62", "Tarjeta restringida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("67", "Retenga y llame", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("68", "Emisor no responde", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("75", "Tarjeta bloqueada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("76", "Cuenta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("77", "Transaccion invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("78", "Cuenta invalida", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("80", "Dato invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("82", "Dato invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("85", "No aprobado", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("86", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("87", "Error en llave", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("88", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("91", "Emisor no activo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("92", "Emisor invalido", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("94", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("96", "Emisor no activo", paymentChannel, null , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("", "Negada", paymentChannel, null , false, transactionRType, false ));
		transactionRType = (TransactionType)session.load(TransactionType.class, 25);
		transactionResponse.add(new TransactionResponse("98", "Aprobada", paymentChannel, "Aprobada - Anulacion" , false, transactionRType, false ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("95", "Descuadre", paymentChannel, "Descuadre en cierre" , false, transactionRType, true ));
		transactionRType = null;
		transactionResponse.add(new TransactionResponse("56", "Error", paymentChannel, "" , false, transactionRType, false ));
		transactionResponse.forEach((transactionR) -> {
		Session sessionr = HibernateUtil.getSessionFactory().openSession();
		sessionr.beginTransaction();
		sessionr.save(transactionR);
		sessionr.getTransaction().commit();
		sessionr.close();
		});

	}
	
	/**
	 * Inicializa las Franquicias de Tarjetas
	 */
	public static void initCardFranchise() {
		List<CardFranchise> franchiseList = new ArrayList<CardFranchise>();
		franchiseList.add(new CardFranchise("Maestro"));
		franchiseList.add(new CardFranchise("Visa"));
		franchiseList.add(new CardFranchise("MasterCard"));
		franchiseList.add(new CardFranchise("American Express"));
		franchiseList.add(new CardFranchise("Privada"));
		Session session = HibernateUtil.getSessionFactory().openSession();
		franchiseList.forEach((franchise) -> {
			session.beginTransaction();
			session.save(franchise);
			session.getTransaction().commit();
		});
	}
	
	/**
	 * Inicializa los Bancos
	 */
	public static void initBanks() {
		List<Bank> bankList = new ArrayList<Bank>();
		bankList.add(new Bank("100BANCO","J-085007768","471240"));
		bankList.add(new Bank("BANCAMIGA","J-316287599","455612"));
		bankList.add(new Bank("BANCARIBE","J-000029490","454137"));
		bankList.add(new Bank("BANCO ACTIVO","J-080066227","474496"));
		bankList.add(new Bank("BANCO AGRICOLA DE VENEZUELA","G-200057955","454188"));
		bankList.add(new Bank("BANCO CARONI","J-095048551","434615"));
		bankList.add(new Bank("BANCO CONFEDERADO","J-000000000","455612"));
		bankList.add(new Bank("BANCO DE VENEZUELA","G-200099976","455612"));
		bankList.add(new Bank("BANCO DEL PUEBLO SOBERANO","G-200078588","454188"));
		bankList.add(new Bank("BANCO DEL TESORO","G-200051876","422260"));
		bankList.add(new Bank("BANCO ESPIRITO SANTO","J-308918644","454188"));
		bankList.add(new Bank("BANCO EXTERIOR","J-000029504","456035"));
		bankList.add(new Bank("BANCO GUAYANA","J-308058238","455612"));
		bankList.add(new Bank("BANCO INDUSTRIAL","J-000029571","457380"));
		bankList.add(new Bank("BANCO MERCANTIL","J-000000000","455612"));
		bankList.add(new Bank("BANCO PLAZA","J-002970553","441785"));
		bankList.add(new Bank("BANCO PROVINCIAL","J-000000000","455612"));
		bankList.add(new Bank("BANCO SOFITASA","J-000000000","455612"));
		bankList.add(new Bank("BANCOEX","J-000000000","455612"));
		bankList.add(new Bank("BANCORO","J-070001729","455612"));
		bankList.add(new Bank("BANCRECER","J-316374173","455612"));
		bankList.add(new Bank("BANESCO","J-000000000","455612"));
		bankList.add(new Bank("BANFANB","G-200106573","455612"));
		bankList.add(new Bank("BANGENTE","J-000000000","455612"));
		bankList.add(new Bank("BANPLUS","J-000423032","441059"));
		bankList.add(new Bank("BFC","J-000723060","476560"));
		bankList.add(new Bank("BICENTENARIO","G-200091487","491608"));
		bankList.add(new Bank("BNC","J-000000000","455612"));
		bankList.add(new Bank("BOD BANCO UNIVERSAL","J-300619460","441132"));
		bankList.add(new Bank("CASA PROPIA","J-000928630","455612"));
		bankList.add(new Bank("CITIBANK","J-000000000","455612"));
		bankList.add(new Bank("CORPBANCA","J-000643598","455612"));
		bankList.add(new Bank("DEL SUR","J-000797234","433485"));
		bankList.add(new Bank("MIBANCO","J-000000000","455612"));
		bankList.add(new Bank("TOTALBANK","J-000000000","455612"));
		bankList.add(new Bank("VENEZOLANO DE CREDITO","J-000000000","455612"));
		//bankList.add(new Bank("","",""));
		Session session = HibernateUtil.getSessionFactory().openSession();
		bankList.forEach((bank) -> {
			session.beginTransaction();
			session.save(bank);
			session.getTransaction().commit();
		});
	}
	
	/**
	 * Inicializa los Tipos de Tarjetas
	 */
	public static void initCardType() {
		List<CardType> cardTypeList = new ArrayList<CardType>();
		cardTypeList.add(new CardType("Debito"));
		cardTypeList.add(new CardType("Credito"));
		Session session = HibernateUtil.getSessionFactory().openSession();
		cardTypeList.forEach((cardtype) -> {
			session.beginTransaction();
			session.save(cardtype);
			session.getTransaction().commit();
		});
	}
	
	/**
	 * Inicializa los Productos de Tarjetas Generales
	 */
	public static void initGeneralCardProducts() {
		CardFranchise cardFranchise = null;
		CardType cardType = null;
		List<GeneralCardProduct> generalProductList = new ArrayList<GeneralCardProduct>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "American Express").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("300000","399999")));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("500000","509999")));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("510000","559999")));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("560000","699999")));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("400000","499999")));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		generalProductList.add(new GeneralCardProduct(cardFranchise, cardType, new Bin("540142","540142")));
		
		generalProductList.forEach((generalProduct) -> {
			session.beginTransaction();
			session.save(generalProduct);
			session.getTransaction().commit();
		});
	}
	
	/**
	 * Inicializa los Productos de Tarjetas por Banco
	 */
	public static void initBankCardProducts() {
		CardFranchise cardFranchise = null;
		CardType cardType = null;
		Bank bank = null;
		List<BankCardProduct> bankProductList = new ArrayList<BankCardProduct>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603071","603071"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANESCO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601285","601285"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601288","601288"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603693","603693"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601685","601685"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("628155","628155"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CARONI").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603684","603684"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("500074","500074"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("500784","500784"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601418","601418"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("589941","589941"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601705","601705"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601759","601759"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCARIBE").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603644","603644"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "DEL SUR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601582","601582"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO EXTERIOR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("627534","627534"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO GUAYANA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("602695","602695"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO MERCANTIL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("501877","501878"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("588891","588891"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PLAZA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("621999","621999"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PROVINCIAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("589524","589524"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601686","601686"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "TOTALBANK").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("627101","627101"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517859","517859"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("543212","543212"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552265","552265"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554395","554395"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554665","554665"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554912","554912"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558844","558844"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553424","553424"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANESCO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540167","540167"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542094","542094"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542172","542172"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542218","542218"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542660","542660"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542695","542695"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("544312","544312"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("544338","544338"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("545049","545049"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("545939","545939"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546492","546492"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546704","546704"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547289","547289"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547887","547887"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554729","554729"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540139","540141"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546465","546466"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546752","546753"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554642","554643"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("539658","539658"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("543682","543682"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549191","549191"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("543694","543694"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540133","540133"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540932","540932"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("548505","548505"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554937","554937"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558753","558753"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552292","552292"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553447","553447"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("512399","512399"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518031","518031"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518152","518152"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("525739","525739"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("521877","521877"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540019","540019"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540084","540084"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540131","540131"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541459","541459"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541463","541463"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546088","546088"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546690","546690"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549192","549192"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554770","554770"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554790","554790"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558491","558491"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558834","558834"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558841","558841"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540144","540145"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547290","547291"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547552","547552"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCARIBE").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552284","552284"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("533416","533416"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540085","540085"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540086","540086"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540132","540132"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549193","549193"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "DEL SUR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540739","540739"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541841","541842"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO EXTERIOR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("525973","525973"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518093","518093"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("543725","543725"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547032","547032"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547469","547469"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549196","549196"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552325","552325"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO MERCANTIL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517758","517758"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541247","541247"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541844","541844"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("544680","544680"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549190","549190"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("530470","530472"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("548851","548852"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PROVINCIAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540009","540009"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540628","540628"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542007","542007"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546890","546890"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549197","549197"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547326","547327"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552283","552283"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PLAZA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554798","554798"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554831","554831"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554914","554914"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558886","558886"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Privada").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990101","990101"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990111","990111"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990114","990114"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANESCO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411016","411016"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("450966","450966"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454599","454599"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("491952","491952"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("496638","496638"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("451384","451385"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("451721","451722"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454500","454502"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454511","454515"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454517","454520"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454530","454534"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454560","454561"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454573","454580"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422169","422169"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("498487","498487"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407607","407607"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("409709","409709"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("441132","441133"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("446844","446844"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("467478","467478"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("474154","474154"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407427","407427"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("439475","439476"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("415762","415762"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("414754","414754"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("627166","627166"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "CASA PROPIA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421035","421036"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("406260","406260"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411047","411047"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454131","454136"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("476064","476064"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("448174","448174"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("455612","455615"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCARIBE").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454137","454139"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("451325","451325"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("451315","451315"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("489995","489995"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411019","411019"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("452225","452225"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "DEL SUR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("433485","433486"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO EXTERIOR").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411018","411018"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454732","454732"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("456032","456035"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("404849","404849"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407602","407602"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421882","421882"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("433121","433121"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("440827","440827"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("415229","415229"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("414786","414786"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO GUAYANA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("439482","439483"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO MERCANTIL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411096","411096"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421930","421930"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422099","422099"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("452517","452517"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("453230","453233"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454104","454104"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("455640","455641"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("456336","456336"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("467126","467126"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("479320","479321"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("498870","498871"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PROVINCIAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("402765","402765"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411097","411097"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421177","421178"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("437774","437774"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("446870","446870"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("450604","450604"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("450630","450630"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454041","454042"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("454439","454440"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422242","422242"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("491508","491509"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("402686","402686"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("493752","493753"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407616","407616"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522836","522836"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546774","546774"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546803","546803"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CARONI").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518067","518067"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554972","554972"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("545174","545174"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CARONI").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407457","407457"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("434613","434613"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO SOFITASA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522963","522963"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("548849","548850"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552287","552287"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO SOFITASA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422039","422039"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("457999","457999"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("490112","490113"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BFC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522270","522270"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554735","554735"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("539667","539667"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554646","554646"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BFC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411858","411858"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("476560","476562"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "CITIBANK").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546489","546489"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546490","546490"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554929","554929"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "CITIBANK").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("448741","448742"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("451450","451450"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("479340","479340"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("485801","485801"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CONFEDERADO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517677","517677"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522847","522847"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("524369","524369"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553480","553480"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554210","554211"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CONFEDERADO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421444","421444"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("473292","473292"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("499941","499941"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "VENEZOLANO DE CREDITO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518269","518269"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517707","517707"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("518310","518310"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "VENEZOLANO DE CREDITO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("406267","406267"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("411850","411851"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("415366","415366"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422050","422050"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("423691","423691"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("476515","476515"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("486520","486520"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("494170","494170"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("499929","499930"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BNC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("520509","520509"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("515673","515673"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("515892","515892"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("520048","520048"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("520148","520148"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("520154","520154"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("528004","528004"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("528008","528008"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BNC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("446334","446334"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422044","422046"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("425881","425881"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO SOFITASA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("601618","601618"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "CITIBANK").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("508111","508111"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CONFEDERADO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603208","603208"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "VENEZOLANO DE CREDITO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("621984","621984"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BFC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("602954","602954"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("589950","589950"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603124","603124"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603216","603216"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DEL TESORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407556","407556"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422260","422263"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("512776","512776"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DEL TESORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("512742","512742"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("524339","524339"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("539660","539660"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553438","553438"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DEL TESORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("520783","520783"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("639489","639489"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO GUAYANA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("558762","558762"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554920","554920"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541434","541434"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541431","541431"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522824","522824"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603693","603693"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Privada").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990118","990118"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990119","990119"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PLAZA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("441784","441786"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("434967","434967"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("541689","541689"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("515659","515659"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("628155","628155"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCRECER").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("604961","604961"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BNC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("627609","627609"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ACTIVO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("474496","474499"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ACTIVO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517953","517953"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("521041","521041"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("530147","530147"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("546629","546629"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ACTIVO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("606061","606061"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "MIBANCO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("606401","606401"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("486648","486648"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "American Express").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("300000","399999"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BICENTENARIO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("414747","414747"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("441040","441041"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("444806","444806"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("491608","491609"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BICENTENARIO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("521305","521305"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("544807","544807"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("544909","544909"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("545193","545193"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BICENTENARIO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("603122","603122"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("512460","512460"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("542037","542037"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540142","540142"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("521778","521778"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("459347","459347"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("462229","462229"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("405890","405890"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("406261","406261"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407407","407408"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("407511","407511"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("421884","421886"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Privada").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("990120","990120"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BFC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("405890","405890"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("414640","414640"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCARIBE").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422225","422225"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("486622","486622"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO CARONI").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422268","422268"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("427170","427170"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BFC").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("422281","422281"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("475783","475784"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("425478","425479"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("480433","480433"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("493802","493804"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ACTIVO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("427172","427172"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("441058","441059"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("476812","476814"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DEL PUEBLO SOBERANO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("504157","504157"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("531241","531241"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("531707","531707"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547153","547153"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553635","553635"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCAMIGA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("512391","512391"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("516668","516668"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("523739","523739"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANPLUS").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("515835","515835"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522206","522206"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("527356","527356"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("548586","548586"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517648","517648"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517657","517657"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("540143","540143"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("549115","549115"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("552786","552786"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "CASA PROPIA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("517981","517981"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO DEL TESORO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("522782","522782"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ACTIVO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("547554","547554"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BOD BANCO UNIVERSAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("554388","554388"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO AGRICOLA DE VENEZUELA").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("639781","639781"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ESPIRITO SANTO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("505826","505826"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "MasterCard").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO ESPIRITO SANTO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("528521","528521"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("528940","528940"), bank));
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("553513","553513"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Maestro").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Debito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "100BANCO").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("639589","639589"), bank));
		
		cardFranchise = (CardFranchise) session.createQuery("From CardFranchise as cardFranchise where cardFranchise.name = ?")
				.setString(0, "Visa").setMaxResults(1).list().get(0);
		cardType = (CardType) session.createQuery("From CardType as cardType where cardType.name = ?")
				.setString(0, "Credito").setMaxResults(1).list().get(0);
		bank = (Bank) session.createQuery("From Bank as bank where bank.name = ?")
				.setString(0, "BANCO PROVINCIAL").setMaxResults(1).list().get(0);
		bankProductList.add(new BankCardProduct(cardFranchise, cardType, new Bin("123456","123456"), bank));
		
		bankProductList.forEach((bankProduct) -> {
			//logger.debug("BankProduct: {}", bankProduct);
			session.beginTransaction();
			session.save(bankProduct);
			session.getTransaction().commit();
		});
	}
}
