package vpago.core.config;

import javax.ejb.Local;

@Local
public interface ConfigLoaderLocal {
	
	public boolean isActiveEchoTest();
	public void setActiveEchoTest(boolean activeEchoTest);

}
