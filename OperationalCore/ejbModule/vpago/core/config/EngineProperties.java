package vpago.core.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EngineProperties {
	protected static final Logger logger = LoggerFactory.getLogger(EngineProperties.class);
	 
	private static Boolean vauthOn;
	private static String vauthIp;
	private static String vauthPort;
	private static String vauthTrxPath;
	private static String propertyFile = "/main/resources/operationalcore-development.properties";
	
	public static void loadProperties() {
		Properties engProperties = new Properties();
		InputStream inputStream = null;
		
		try {	
			inputStream = EngineProperties.class.getResourceAsStream(propertyFile);
			// load a properties file
			if(inputStream != null) engProperties.load(inputStream);
			// get the property value and print it out
			vauthOn = engProperties.getProperty("engine.transactions.vauth-on", "false").equals("true") ? true : false;
			logger.debug("engine.transactions.vauth-on={}", vauthOn.toString());
			vauthIp = engProperties.getProperty("engine.connections.vauth-ip", "localhost");
			logger.debug("engine.connections.vauth-ip={}", vauthIp);
			vauthPort = engProperties.getProperty("engine.connections.vauth-port", "8083");
			logger.debug("engine.connections.vauth-port={}", vauthPort);
			vauthTrxPath = engProperties.getProperty("engine.connections.vauth-transaction-path", "/transaction/postNew");
			logger.debug("engine.connections.vauth-transaction-path={}", vauthTrxPath);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @return the vauthOn
	 */
	public static Boolean getVauthOn() {
		return vauthOn;
	}

	/**
	 * @param vauthOn the vauthOn to set
	 */
	public static void setVauthOn(Boolean vauthOn) {
		EngineProperties.vauthOn = vauthOn;
	}

	/**
	 * @return the vauthIp
	 */
	public static String getVauthIp() {
		return vauthIp;
	}

	/**
	 * @param vauthIp the vauthIp to set
	 */
	public static void setVauthIp(String vauthIp) {
		EngineProperties.vauthIp = vauthIp;
	}

	/**
	 * @return the vauthPort
	 */
	public static String getVauthPort() {
		return vauthPort;
	}

	/**
	 * @param vauthPort the vauthPort to set
	 */
	public static void setVauthPort(String vauthPort) {
		EngineProperties.vauthPort = vauthPort;
	}

	/**
	 * @return the vauthTrxPath
	 */
	public static String getVauthTrxPath() {
		return vauthTrxPath;
	}

	/**
	 * @param vauthTrxPath the vauthTrxPath to set
	 */
	public static void setVauthTrxPath(String vauthTrxPath) {
		EngineProperties.vauthTrxPath = vauthTrxPath;
	}

	
}
