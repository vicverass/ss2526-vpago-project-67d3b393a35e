package vpago.core.config;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Session Bean implementation class ConfigLoader
 */
@Startup
@Singleton
@LocalBean
public class ConfigLoader implements ConfigLoaderRemote, ConfigLoaderLocal {

    /**
     * Default constructor. 
     */
    public ConfigLoader() {
        
    }
    
    private boolean activeEchoTest;

	/**
	 * @return the activeEchoTest
	 */
	public boolean isActiveEchoTest() {
		return activeEchoTest;
	}

	/**
	 * @param activeEchoTest the activeEchoTest to set
	 */
	public void setActiveEchoTest(boolean activeEchoTest) {
		this.activeEchoTest = activeEchoTest;
	}
    
    

}
