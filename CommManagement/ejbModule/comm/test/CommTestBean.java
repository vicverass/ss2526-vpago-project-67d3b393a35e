package comm.test;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class CommTestBean
 */
@Stateless
@LocalBean
public class CommTestBean implements CommTestBeanLocal {

    /**
     * Default constructor. 
     */
    public CommTestBean() {
    	super();
    }

}
