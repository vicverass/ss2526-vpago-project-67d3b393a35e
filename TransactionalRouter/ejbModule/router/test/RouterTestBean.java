package router.test;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class RouterTestBean
 */
@Stateless
@LocalBean
public class RouterTestBean implements RouterTestBeanLocal {

    /**
     * Default constructor. 
     */
    public RouterTestBean() {
        super();
    }

}
